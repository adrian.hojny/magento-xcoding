<?php declare(strict_types=1);

namespace Xcoding\StockGraphQl\Model\Resolver;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\CatalogInventory\Model\StockRegistry;

/**
 * @inheritdoc
 */
class SalableQty implements ResolverInterface
{
    /**
     * @var StockRegistry
     */
    private $stockRegistry;

    /**
     * @param StockRegistry $stockRegistry
     */
    public function __construct(
        StockRegistry $stockRegistry
    ) {
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!array_key_exists('model', $value) || !$value['model'] instanceof \Magento\Catalog\Model\Product) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $value['model'];
        return $this->stockRegistry->getStockStatus(
            $product->getId(),
            $product->getStore()->getWebsiteId()
        )->getQty();
    }
}

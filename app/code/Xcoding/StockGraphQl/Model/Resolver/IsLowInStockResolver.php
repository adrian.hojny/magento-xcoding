<?php declare(strict_types=1);

namespace Xcoding\StockGraphQl\Model\Resolver;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Store\Model\ScopeInterface;

/**
 * @inheritdoc
 */
class IsLowInStockResolver implements ResolverInterface
{
    const XML_PATH_LOW_STOCK_THRESHOLD_QTY = 'cataloginventory/options/low_qty_threshold';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\CatalogInventory\Model\StockRegistry
     */
    private $stockRegistry;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StockRegistryInterface $stockRegistry
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StockRegistryInterface $stockRegistry
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->stockRegistry = $stockRegistry;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!array_key_exists('model', $value) || !$value['model'] instanceof ProductInterface) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /** @var ProductInterface $product */
        $product = $value['model'];
        $lowInStock = $this->isLowInStock($product);

        return $lowInStock;
    }

    /**
     * Determine if is in low qty
     *
     * @param ProductInterface $product
     *
     * @return bool
     */
    private function isLowInStock(ProductInterface $product): bool
    {
        $stockItem = $this->stockRegistry->getStockItem($product->getId());

        $useConfigThreshold = $stockItem->getData('use_config_low_qty_threshold');
        $productThresholdQty = $stockItem->getData('low_qty_threshold');
        $globalThresholdQty = (int)$this->scopeConfig->getValue(
            self::XML_PATH_LOW_STOCK_THRESHOLD_QTY,
            ScopeInterface::SCOPE_STORE
        );

        $thresholdQty = $useConfigThreshold ?
            $globalThresholdQty :
            $productThresholdQty;

        if ($thresholdQty > 0) {
            $stockCurrentQty = $this->stockRegistry->getStockStatus(
                $product->getId(),
                $product->getStore()->getWebsiteId()
            )->getQty();

            return $stockCurrentQty !== null && $stockCurrentQty <= $thresholdQty;
        }

        return false;
    }
}

# Xcoding_ReCaptchaGraphQl

### Konfiguracja
Dodanie kluczy oraz wybranie typu zabezpieczenia:
`Stores -> Configuration -> Security -> Google reCaptcha`

Należy ustawić:
- Google API website key
- Google API secret key
- reCaptcha type
- Frontend > Enable [Yes]
- Frontend > Enable Create [Yes] (dla tworzenia konta)

Pozostałe ustawienia nie są obsłużone przez GraphQl

Klucze można wygeneować pod adresem: https://www.google.com/recaptcha/admin/

### GraphQl

#### Pobranie klucza publicznego oraz typu zabezpieczenia

```graphql
query {
  storeConfig {
    recaptcha_public_key
    recaptcha_type
    recaptcha_enabled
    recaptcha_enabled_create_user
  }
}
```

Odpowiedź:
```json
{
  "data": {
    "storeConfig": {
      "recaptcha_public_key": "[klucz]",
      "recaptcha_type": "recaptcha_v3",
      "recaptcha_enabled": true,
      "recaptcha_enabled_create_user": true
    }
  }
}
```
Możliwe wartości `recaptcha_type` to: 
- `recaptcha_v3` - Invisible reCaptcha v3
- `invisible` - Invisible reCaptcha v2
- `recaptcha` - reCaptcha v2

Rejestracja:

```graphql
mutation {
  createCustomer(input: {
    email: "[email]",
    firstname: "[firstname]",
    lastname: "[lastname]",
    gRecaptchaResponse: "[wygenerowany przez reCaptcha token]"
  }) {
    customer {
      email
    }
  }
}
```
Przykładowa odpowiedź w przypadku błędu:
```json
{
  "errors": [
    {
      "message": "Google recaptcha verification failed",
      "extensions": {
        "category": "graphql-input"
      },
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": [
        "createCustomer"
      ]
    }
  ],
  "data": {
    "createCustomer": null
  }
}
```
<?php
declare(strict_types=1);

namespace Xcoding\ReCaptchaGraphQl\Plugin\Magento\CustomerGraphQl\Model\Resolver;

use Magento\CustomerGraphQl\Model\Resolver\CreateCustomer as CreateCustomerBase;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use MSP\ReCaptcha\Api\ValidateInterface;
use MSP\ReCaptcha\Model\Config as ReCaptchaConfig;

class CreateCustomer
{
    /**
     * @var ValidateInterface
     */
    protected $reCaptchaValidate;
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;
    /**
     * @var ReCaptchaConfig
     */
    private $reCaptchaConfig;

    /**
     * CreateCustomer constructor.
     * @param ValidateInterface $reCaptchaValidate
     * @param RemoteAddress $remoteAddress
     * @param ReCaptchaConfig $reCaptchaConfig
     */
    public function __construct(
        ValidateInterface $reCaptchaValidate,
        RemoteAddress $remoteAddress,
        ReCaptchaConfig $reCaptchaConfig
    ) {
        $this->reCaptchaValidate = $reCaptchaValidate;
        $this->remoteAddress = $remoteAddress;
        $this->reCaptchaConfig = $reCaptchaConfig;
    }

    /**
     * @param CreateCustomerBase $subject
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @throws GraphQlInputException
     */
    public function beforeResolve(
        CreateCustomerBase $subject,
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if ($this->reCaptchaConfig->isEnabledFrontend() && $this->reCaptchaConfig->isEnabledFrontendCreate()) {
            $gRecaptchaResponse = $args['input']['gRecaptchaResponse'] ?? null;
            $requestValid = false;
            if ($gRecaptchaResponse) {
                $remoteIp = $this->remoteAddress->getRemoteAddress();
                $requestValid = $this->reCaptchaValidate->validate($gRecaptchaResponse, $remoteIp);
            }

            if (!$requestValid) {
                throw new GraphQlInputException(__('Google recaptcha verification failed'));
            }
        }
    }
}

<?php
declare(strict_types=1);

namespace Xcoding\ProductSizingGraphQl\Model\Resolver;

use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing\CollectionFactory as ProductSizingCollectionFactory;

class ProductSizing implements \Magento\Framework\GraphQl\Query\ResolverInterface
{
    /** @var ProductSizingCollectionFactory */
    protected $productSizingCollectionFactory;

    /**
     * @param ProductSizingCollectionFactory $productSizingCollectionFactory
     */
    public function __construct(
        ProductSizingCollectionFactory $productSizingCollectionFactory
    ) {
        $this->productSizingCollectionFactory = $productSizingCollectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        \Magento\Framework\GraphQl\Config\Element\Field $field,
        $context,
        \Magento\Framework\GraphQl\Schema\Type\ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $ids = $args['ids'];
        $collection = $this->productSizingCollectionFactory->create();
        $collection->addFieldToFilter('productsizing_id', ['in' => $ids]);
        $result = [];
        foreach ($collection as $model) {
            /** @var \Xcoding\ProductSizing\Model\ProductSizing $model */
            $data = $model->getData();
            $data['id'] = $model->getId();
            $data['content_json'] = $model->getTranslatedContentJson();
            $result[] = $data;
        }
        return $result;
    }
}


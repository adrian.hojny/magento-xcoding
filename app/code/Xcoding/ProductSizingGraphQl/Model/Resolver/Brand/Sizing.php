<?php

namespace Xcoding\ProductSizingGraphQl\Model\Resolver\Brand;

use Magento\Store\Model\StoreManagerInterface;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing\CollectionFactory as ProductSizingCollectionFactory;

class Sizing implements \Magento\Framework\GraphQl\Query\ResolverInterface
{
    /** @var \Magento\Store\Model\StoreManager */
    protected $storeManager;

    /** @var ProductSizingCollectionFactory */
    protected $productSizingCollectionFactory;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ProductSizingCollectionFactory $productSizingCollectionFactory
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ProductSizingCollectionFactory $productSizingCollectionFactory
    ) {
        $this->storeManager = $storeManager;
        $this->productSizingCollectionFactory = $productSizingCollectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        \Magento\Framework\GraphQl\Config\Element\Field $field,
        $context,
        \Magento\Framework\GraphQl\Schema\Type\ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var \Xcoding\ShopbyBrand\Model\OptionSetting $model */
        $model = $value['model'] ?? null;
        if (!$model) {
            throw new \Magento\Framework\Exception\LocalizedException(__('"model" value should be specified'));
        }
        $collection = $this->productSizingCollectionFactory->create()
            ->addFieldToFilter('brand_id', $model->getId());
        $result = [];
        foreach ($collection as $model) {
            /** @var \Xcoding\ProductSizing\Model\ProductSizing $model */
            $data = $model->getData();
            $data['id'] = $model->getId();
            $data['content_json'] = $model->getTranslatedContentJson();
            $result[] = $data;
        }
        return $result;
    }
}

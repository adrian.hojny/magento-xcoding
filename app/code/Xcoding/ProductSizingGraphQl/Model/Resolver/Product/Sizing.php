<?php
declare(strict_types=1);

namespace Xcoding\ProductSizingGraphQl\Model\Resolver\Product;

use Amasty\ShopbyBase\Helper\OptionSetting as SettingHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Xcoding\ProductSizing\Api\Data\ProductSizingInterface;
use Xcoding\ProductSizing\Api\ProductSizingRepositoryInterface;
use Xcoding\ProductSizing\Model\ProductSizing;

class Sizing implements ResolverInterface
{
    /**
     * @var SettingHelper
     */
    protected $settingHelper;

    /**
     * @var ProductSizingRepositoryInterface
     */
    protected $productSizingRepository;

    /**
     * @var array
     */
    protected $sizingCache = [];

    /**
     * @param SettingHelper $settingHelper
     * @param ProductSizingRepositoryInterface $productSizingRepository
     */
    public function __construct(
        SettingHelper $settingHelper,
        ProductSizingRepositoryInterface $productSizingRepository
    ) {
        $this->settingHelper = $settingHelper;
        $this->productSizingRepository = $productSizingRepository;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $value['model'];
        $sizingId = $product->getSizing();
        $sizing = null;
        if ($sizingId) {
            $sizing = $this->getSizingById($sizingId);
        } else {
            $brand = null;
            if ($id = (int) $product->getBrand()) {
                $brand = $this->settingHelper->getSettingByValue($id, 'attr_brand', $product->getStoreId());
            }
            if ($brand && $brand->getSizing()) {
                $sizing = $this->getSizingById($brand->getSizing());
            }
        }

        return null !== $sizing ? [
            'title' => $sizing->getTitle(),
            'content' => $sizing->getContent()
        ] : null;
    }

    /**
     * @param $sizingId
     * @return ProductSizingInterface|null
     * @throws LocalizedException
     */
    protected function getSizingById($sizingId)
    {
        if (!isset($this->sizingCache[$sizingId])) {
            $sizing = $this->productSizingRepository->get($sizingId);
            if ($sizing->getCode() === ProductSizing::EMPTY_SIZING_CODE) {
                $sizing = null;
            }

            $this->sizingCache[$sizingId] = $sizing;
        }

        return $this->sizingCache[$sizingId];
    }
}


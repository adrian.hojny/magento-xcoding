# Xcoding_ProductSizingGraphQl

Moduł rozszerzający `Xcoding_ProductSizing` o GraphQl

Zarządzanie rozmiarówkami opisano w README modułu `Xcoding_ProductSizing`

#### Pobieranie rozmiarówki produktu

Przykładowe zapytanie:
```graphql
query {
  products(
    pageSize: 10,
    currentPage: 1,
    filter: {
      brand_url_alias: { 
        eq: "url2.html"
      }
    }
  ) {
    items {
      sku
      name
      
      sizing_details {
        content
        title
      }
    }
  }
}
```

Przykładowa odpowiedź:

```json
{
  "data": {
    "products": {
      "items": [
        {
          "sku": "n31091479",
          "name": "People of Shibuya Transitional Jacket",
          "sizing_details": {
            "content": "content2 content2 content2 content2 content2 content2 content2 ",
            "title": "title 2"
          }
        }
      ]
    }
  }
}
```

Content może zawierać HTML.
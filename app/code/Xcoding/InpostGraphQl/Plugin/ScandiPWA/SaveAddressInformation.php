<?php declare(strict_types=1);

namespace Xcoding\InpostGraphQl\Plugin\ScandiPWA;

use Closure;
use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\QuoteIdMask;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Quote\Model\Webapi\ParamOverriderCartId;
use Psr\Log\LoggerInterface;
use ScandiPWA\QuoteGraphQl\Model\Resolver\SaveAddressInformation as SaveAddressInformationCore;
use Xcoding\InStorePickup\Helper\Data as InStorePickupHelper;

class SaveAddressInformation
{
    /**
     * @var ParamOverriderCartId
     */
    protected $overriderCartId;

    /**
     * @var QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var Psr\Log\LoggerInterface
     */
    protected $logger;
    /**
     * @var InStorePickupHelper
     */
    private $inStorePickupHelper;

    /**
     * SaveAddressInformation constructor.
     * @param ParamOverriderCartId $overriderCartId
     * @param QuoteIdMaskFactory $quoteIdMaskFactory
     * @param CartRepositoryInterface $quoteRepository
     * @param InStorePickupHelper $inStorePickupHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        ParamOverriderCartId $overriderCartId,
        QuoteIdMaskFactory $quoteIdMaskFactory,
        CartRepositoryInterface $quoteRepository,
        InStorePickupHelper $inStorePickupHelper,
        LoggerInterface $logger
    ) {
        $this->overriderCartId = $overriderCartId;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;
        $this->inStorePickupHelper = $inStorePickupHelper;
    }

    /**
     * @param SaveAddressInformationCore $subject
     * @param callable $proceed
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed
     * @throws Exception
     */
    public function aroundResolve(
        SaveAddressInformationCore $subject,
        callable $proceed,
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $result = $proceed($field, $context, $info, $value, $args);
        try {
            if (isset($args['guestCartId'])) {
                /** @var $quoteIdMask QuoteIdMask */
                $quoteIdMask = $this->quoteIdMaskFactory->create()->load($args['guestCartId'], 'masked_id');
                $quoteId = $quoteIdMask->getQuoteId();
            } else {
                $quoteId = $this->overriderCartId->getOverriddenValue();
            }
            $this->saveInpost($args, $quoteId);
            $this->saveStorePickupPoint($args, $quoteId);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw new Exception('Something went wrong');
        }
        return $result;
    }

    /**
     * @param array $requestAddressInformation
     * @param $quoteId
     * @throws NoSuchEntityException
     */
    protected function saveInpost(array $requestAddressInformation, $quoteId)
    {
        $inpostMachineId = $requestAddressInformation['addressInformation']['inpost_machine_id'] ?? null;
        $quote = $this->quoteRepository->getActive($quoteId);
        if ($quote->getShippingAddress()->getShippingMethod() == 'inpost_inpostmachine') {
            if ($quote->getInpostMachineId() !== $inpostMachineId) {
                $quote->setInpostMachineId($inpostMachineId);
                $this->quoteRepository->save($quote);
            }
        } else {
            if ($quote->getInpostMachineId()) {
                $quote->setInpostMachineId(null);
                $this->quoteRepository->save($quote);
            }
        }
    }

    /**
     * @param array $requestAddressInformation
     * @param $quoteId
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function saveStorePickupPoint(array $requestAddressInformation, $quoteId)
    {
        $pickupStoreCode = $requestAddressInformation['addressInformation']['pickup_store_code'] ?? null;
        $quote = $this->quoteRepository->getActive($quoteId);
        if ($quote->getShippingAddress()->getShippingMethod() == 'pickup_instorepickup') {
            if ($quote->getInStorePickupStoreCode() !== $pickupStoreCode) {
                if (!$this->inStorePickupHelper->getAddressByCode($pickupStoreCode)) {
                    throw new LocalizedException(__('Invalid pickup point'));
                }
                $quote->setData('instorepickup_store_code', $pickupStoreCode);
                $this->quoteRepository->save($quote);
            }
        } else {
            if ($quote->getData('instorepickup_store_code')) {
                $quote->setData('instorepickup_store_code', null);
                $this->quoteRepository->save($quote);
            }
        }
    }
}
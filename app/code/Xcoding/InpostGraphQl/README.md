# Xcoding_InpostGraphQl

#### Wybór paczkomatu

Wybór paczkomatu odbywa się przez mutację ScandiPWA `SaveAddressInformation` przez pole `inpost_machine_id` w inpucie `SaveAddressInformation`

Przykładowe zapytanie:
```graphql
mutation SaveAddressInformation(
  	$addressInformation: SaveAddressInformation!
  	$guestCartId: String
) {
	saveAddressInformation(
		addressInformation: $addressInformation,
    	guestCartId: $guestCartId
  	) {
  		payment_methods {
    		code
    		title
  		}
    	totals {
			grand_total
      		items {
        		name
        		qty
      		}
    	}
	}
}
```
parametry:
```json
{
	"guestCartId": "HxoxfM6MlFm6EKd0ndnyQrs9RURYbAmE",
	"addressInformation": {
		"shipping_address": {
			"region": "New Yorkeeeeer",
			"region_id": 43,
			"region_code": "NY",
			"country_id": "US",
			"street": [
				"123 Oak Ave"
			],
			"postcode": "10577",
			"city": "Purchase",
			"firstname": "Jane",
			"lastname": "Doe",
			"email": "jdoe@example.com",
			"telephone": "512-555-1111"
		},
		"billing_address": {
			"region": "New York",
			"region_id": 43,
			"region_code": "NY",
			"country_id": "US",
			"street": [
				"123 Oak Ave"
			],
			"postcode": "10577",
			"city": "Purchase",
			"firstname": "Jane",
			"lastname": "Doe",
			"email": "jdoe@example.com",
			"telephone": "512-555-1111"
		},
		"shipping_carrier_code": "flatrate",
		"shipping_method_code": "flatrate",
		"inpost_machine_id": "JGO03N"
	}
}
```

przykładowa odpowierdź:

```json
{
  "data": {
    "saveAddressInformation": {
      "payment_methods": [
        {
          "code": "checkmo",
          "title": "Check \/ Money order"
        },
        {
          "code": "dialcom_przelewy",
          "title": "Przelewy24"
        }
      ],
      "totals": {
        "grand_total": 122.94,
        "items": [
          {
            "name": "aaaaaaan",
            "qty": 1
          }
        ]
      }
    }
  }
}
```
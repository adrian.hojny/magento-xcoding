<?php declare(strict_types=1);

namespace Xcoding\OfflinePaymentsExtend\Plugin\QuoteGraphQl\Model\Resolver;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class AvailablePaymentMethods
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param $subject
     * @param $result
     * @return mixed
     */
    public function afterResolve(
        $subject,
        $result
    ) {
        if (isset($result['payment_methods'])) {
            $result['payment_methods'] = $this->addInstructions($result['payment_methods']);
        } else {
            $result = $this->addInstructions($result);
        }

        return $result;
    }

    protected function getInstructions($item)
    {
        return $this->scopeConfig->getValue(
            sprintf('payment/%s/instructions', $item['code'] ?? ''),
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $result
     * @return mixed
     */
    protected function addInstructions($result)
    {
        foreach ($result as &$item) {
            $instructions = $this->getInstructions($item);
            if ($instructions) {
                $item['instructions'] = $instructions;
            }
        }

        return $result;
    }
}
# Xcoding_ContactGraphQl

#### Konfiguracja

Konfiguracja odbywa się w panelu admina (`stores > configuration > general > contacts`)

#### Pobranie listy tematów kontaktu

```graphql
query {
  storeConfig {
    contact_form_topics {
      code
      label
    }
  }
}
```

przykładowa odpowierdź:

```json
{
  "data": {
    "storeConfig": {
      "contact_form_topics": [
        {
          "code": "t1",
          "label": "label1"
        },
        {
          "code": "t2",
          "label": "label2"
        },
        {
          "code": "t3",
          "label": "label3"
        }
      ]
    }
  }
}
```

#### Wysłanie wiadomości:
```graphql
mutation {
  contactFormSend(
    name: "name"
    comment: "comment"
    topic: "t2"
    email: "faraon5593@gmail.com"
    gRecaptchaResponse: "[wygenerowany przez reCaptcha token]"
  )
}
```
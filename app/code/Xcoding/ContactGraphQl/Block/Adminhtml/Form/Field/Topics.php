<?php

namespace Xcoding\ContactGraphQl\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class Topics extends AbstractFieldArray
{
    /**
     * {@inheritdoc}
     */
    protected function _prepareToRender()
    {
        $this->addColumn('code', ['label' => __('Code'), 'class' => 'required-entry', 'style' => 'width: 70px;']);
        $this->addColumn('label', ['label' => __('Label'), 'class' => 'required-entry', 'style' => 'width: 300px;']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add subject');
    }
}
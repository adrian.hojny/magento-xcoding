<?php
declare(strict_types=1);

namespace Xcoding\ContactGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Xcoding\ContactGraphQl\Helper\Data as ModuleHelper;

class Topics implements ResolverInterface
{
    /**
     * @var ModuleHelper
     */
    private $moduleHelper;

    public function __construct(
        ModuleHelper $moduleHelper
    ) {
        $this->moduleHelper = $moduleHelper;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        return $this->moduleHelper->getTopics();
    }
}

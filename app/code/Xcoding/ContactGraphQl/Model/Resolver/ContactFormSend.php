<?php
declare(strict_types=1);

namespace Xcoding\ContactGraphQl\Model\Resolver;

use Magento\Contact\Model\MailInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use MSP\ReCaptcha\Api\ValidateInterface;
use Psr\Log\LoggerInterface;
use Xcoding\ContactGraphQl\Helper\Data as ModuleHelper;
use MSP\ReCaptcha\Model\Config as ReCaptchaConfig;

class ContactFormSend implements ResolverInterface
{
    /**
     * @var MailInterface
     */
    protected $mail;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ModuleHelper
     */
    protected $moduleHelper;

    /**
     * @var ReCaptchaConfig
     */
    protected $reCaptchaConfig;

    /**
     * @var ValidateInterface
     */
    protected $reCaptchaValidate;

    /**
     * @var RemoteAddress
     */
    protected $remoteAddress;

    /**
     * @param MailInterface $mail
     * @param LoggerInterface $logger
     * @param ModuleHelper $moduleHelper
     * @param ReCaptchaConfig $reCaptchaConfig
     * @param ValidateInterface $reCaptchaValidate
     * @param RemoteAddress $remoteAddress
     */
    public function __construct(
        MailInterface $mail,
        LoggerInterface $logger,
        ModuleHelper $moduleHelper,
        ReCaptchaConfig $reCaptchaConfig,
        ValidateInterface $reCaptchaValidate,
        RemoteAddress $remoteAddress
    ) {
        $this->mail = $mail;
        $this->logger = $logger;
        $this->moduleHelper = $moduleHelper;
        $this->reCaptchaConfig = $reCaptchaConfig;
        $this->reCaptchaValidate = $reCaptchaValidate;
        $this->remoteAddress = $remoteAddress;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $args = $this->validatedParams($args);
        $args = $this->setTopic($args);
        $this->sendEmail($args);
        return true;
    }

    /**
     * @param array $args Post data from contact form
     * @return void
     */
    protected function sendEmail($args)
    {
        $this->mail->send(
            $args['email'],
            ['data' => new DataObject($args)]
        );
    }

    /**
     * @param array $args
     * @return array
     * @throws LocalizedException
     */
    protected function validatedParams($args)
    {
        if (trim($args['name']) === '') {
            throw new GraphQlInputException(__('Enter the Name and try again.'));
        }

        if (trim($args['comment']) === '') {
            throw new GraphQlInputException(__('Enter the comment and try again.'));
        }

        if (trim($args['topic']) === '') {
            throw new GraphQlInputException(__('Enter the topic and try again.'));
        }

        if (false === strpos($args['email'], '@')) {
            throw new GraphQlInputException(__('The email address is invalid. Verify the email address and try again.'));
        }

        if ($this->reCaptchaConfig->isEnabledFrontendContact()) {
            $gRecaptchaResponse = $args['gRecaptchaResponse'] ?? null;
            $requestValid = false;
            if ($gRecaptchaResponse) {
                $remoteIp = $this->remoteAddress->getRemoteAddress();
                $requestValid = $this->reCaptchaValidate->validate($gRecaptchaResponse, $remoteIp);
            }

            if (!$requestValid) {
                throw new GraphQlInputException(__('Google recaptcha verification failed'));
            }
        }

        return $args;
    }

    protected function setTopic(array $args)
    {
        $topic = $this->moduleHelper->getTopicByCode($args['topic']);
        $args['topic'] = $topic !== null ? $topic['label'] : '';

        return $args;
    }
}

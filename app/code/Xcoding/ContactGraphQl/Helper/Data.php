<?php declare(strict_types=1);

namespace Xcoding\ContactGraphQl\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;

    /**
     * Data constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param JsonSerializer $jsonSerializer
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        JsonSerializer $jsonSerializer
    ) {
        parent::__construct($context);
        $this->jsonSerializer = $jsonSerializer;
        $this->scopeConfig = $scopeConfig;
    }

    public function getTopics()
    {
        $topics = $this->scopeConfig->getValue(
            'contact/email/topics',
            ScopeInterface::SCOPE_STORE
        );

        try {
            $mapping = $this->jsonSerializer->unserialize($topics);
        } catch (Exception $e) {
            $mapping = [];
        }

        return $mapping;
    }

    public function getTopicByCode(string $code)
    {
        $topics = $this->getTopics();
        foreach ($topics as $topic) {
            if ($topic['code'] === $code) {
                return $topic;
            }
        }

        return null;
    }
}

<?php

namespace Xcoding\BrandGraphQl\Model\Resolver;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class BrandAlternateHreflang implements \Magento\Framework\GraphQl\Query\ResolverInterface
{
    /** @var \Magento\Store\Model\StoreManager */
    protected $storeManager;

    /** @var \Magento\Framework\App\Config */
    protected $scopeConfig;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        /** @var \Xcoding\ShopbyBrand\Model\OptionSetting $model */
        $model = $value['model'];
        try {
            $items = [];
            foreach ($this->storeManager->getStores() as $store) {
                /** @var \Magento\Store\Model\Store $store */
                $storeId = $store->getId();
                $locale = $this->scopeConfig->getValue(
                    'general/locale/code',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                    $storeId
                );
                $languageCode = explode('_', $locale)[0];
                if (empty($urls[$languageCode])) {
                    $urlKey = $this->scopeConfig->getValue(
                        'amshopby_brand/general/url_key',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                        $storeId
                    );
                    $url = $store->getBaseUrl() . $urlKey . '/' . $model->getUrlAlias();
    
                    if ($url) {
                        $urls[$languageCode] = $url;
                    }
                }
            }
            foreach ($urls as $languageCode => $url) {
                $items[] = ['hreflang' => $languageCode, 'href' => $url];
            }
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            throw new \Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }

        return $items;
    }
}

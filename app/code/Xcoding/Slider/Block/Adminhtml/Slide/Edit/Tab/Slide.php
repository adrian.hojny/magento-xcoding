<?php
namespace Xcoding\Slider\Block\Adminhtml\Slide\Edit\Tab;

class Slide extends \Scandiweb\Slider\Block\Adminhtml\Slide\Edit\Tab\Slide
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $form = $this->getForm();

        $block1 = $form->getElement('block_1_fieldset');
        $block1->addField(
            'tablet_image',
            'image',
            [
                'name' => 'tablet_image',
                'label' => __('Tablet Image'),
                'title' => __('Tablet Image'),
                'required' => true
            ], 'mobile_image'
        );

        $block2 = $form->getElement('block_2_fieldset');
        $block2->addField(
            'tablet_image_2',
            'image',
            [
                'name' => 'tablet_image_2',
                'label' => __('Tablet Image'),
                'title' => __('Tablet Image'),
                'required' => true
            ], 'mobile_image_2'
        );

        $block2 = $form->getElement('block_3_fieldset');
        $block2->addField(
            'tablet_image_3',
            'image',
            [
                'name' => 'tablet_image_3',
                'label' => __('Tablet Image'),
                'title' => __('Tablet Image'),
                'required' => true
            ], 'mobile_image_3'
        );

        /** @var \Xcoding\Slider\Model\Slide $model */
        $model = $this->_coreRegistry->registry('slide');

        $values = $model->getData();
        $values['tablet_image'] = $model->getTabletImageUrl();
        $values['tablet_image_2'] = $model->getTabletImageUrl2();
        $values['tablet_image_3'] = $model->getTabletImageUrl3();

        $form->setValues($values);

        return $this;
    }
}

<?php
namespace Xcoding\Slider\Model\Api;

use Magento\Framework\Api\ImageProcessorInterface;
use Magento\Framework\Api\Data\ImageContentInterfaceFactory;
use Scandiweb\Slider\Model\ResourceModel\Slide as SlideResource;
use Scandiweb\Slider\Model\SlideFactory;
use Xcoding\Slider\Model\Data\SlideFactory as SlideDataFactory;

class Slide
{
    /** @var ImageProcessorInterface */
    public $imageProcessor;

    /** @var ImageContentInterfaceFactory */
    public $imageContentFactory;

    /** @var \Magento\Framework\Filesystem */
    public $filesystem;

    /** @var SlideResource */
    public $resource;

    /** @var SlideFactory */
    public $factory;

    /** @var SlideDataFactory */
    public $dataFactory;
 
    public function __construct(
        ImageProcessorInterface $imageProcessor,
        ImageContentInterfaceFactory $imageContentFactory,
        \Magento\Framework\Filesystem $filesystem,
        SlideResource $resource,
        SlideFactory $factory,
        SlideDataFactory $dataFactory
    ) {
        $this->imageProcessor = $imageProcessor;
        $this->imageContentFactory = $imageContentFactory;
        $this->filesystem = $filesystem;
        $this->resource = $resource;
        $this->factory = $factory;
        $this->dataFactory = $dataFactory;
    }

    /**
     * GET for Slider API
     * @param int $id
     * @return \Xcoding\Slider\Model\Data\Slide
     */
    public function get($id)
    {
        $dataModel = $this->dataFactory->create();
        $slide = $this->factory->create();
        $this->resource->load($slide, $id);
        $dataModel->setSlide($slide);
        return $dataModel;
    }

    /**
     * PUT for Slide API
     * @param int   $id
     * @param mixed $data Array of data
     * @return \Xcoding\Slider\Model\Data\Slide
     */
    public function put($id, $data)
    {
        $slide = $this->factory->create();
        $this->resource->load($slide, $id);

        $this->setData($slide, $data);
        $this->resource->save($slide);

        $dataModel = $this->dataFactory->create();
        $dataModel->setSlide($slide);
        return $dataModel;
    }

    /**
     * POST for Slide API
     * @param mixed $data Array of data
     * @return \Xcoding\Slider\Model\Data\Slide
     */
    public function post($data)
    {
        $slide = $this->factory->create();

        $this->setData($slide, $data);
        $this->resource->save($slide);

        $dataModel = $this->dataFactory->create();
        $dataModel->setSlide($slide);
        return $dataModel;
    }

    /**
     * @param \Scandiweb\Slider\Model\Slide $slide
     * @param mixed $data
     * 
     * @return void
     */
    private function setData($slide, $data)
    {
        $fields = [
            'slider_id',
            'is_active',
            'title',
            'status',
            'start_time',
            'end_time',
            'slide_text'
        ];
        foreach ($fields as $field) {
            if ($value = $data[$field] ?? null) {
                $slide->setData($field, $value);
            }
        }

        $imageFields = [
            'desktop_image',
            'mobile_image',
            'desktop_image_2',
            'mobile_image_2',
            'desktop_image_3',
            'mobile_image_3'
        ];
        foreach ($imageFields as $field) {
            $imageData = $data[$field] ?? null;
            if (!$imageData) {
                continue;
            }
            $delete = $imageData['delete'] ?? null;
            if ($delete == 1) {
                $slide->setData($field, null);
                continue;
            }
            $imageContent = $this->imageContentFactory->create();
            $imageContent->setBase64EncodedData($imageData['base64_encoded_data']);
            $imageContent->setType($imageData['type']);
            $imageContent->setName($imageData['name']);
            $mediapath = $this->filesystem
                ->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA)
                ->getAbsolutePath(\Scandiweb\Slider\Model\Slider::MEDIA_PATH);

            $filename = $this->imageProcessor->processImageContent($mediapath, $imageContent);
            $slide->prepareImagesForSrcset($mediapath . $filename);
            $slide->setData($field, \Scandiweb\Slider\Model\Slider::MEDIA_PATH . $filename);
        }
    }
}

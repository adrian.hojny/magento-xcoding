<?php
namespace Xcoding\Slider\Model\Api;

use Scandiweb\Slider\Model\SliderFactory;
use Scandiweb\Slider\Model\ResourceModel\Slider as SliderResource;
use Xcoding\Slider\Model\Data\SliderFactory as SliderDataFactory;

class Slider
{
    /** @var SliderResource */
    public $resource;

    /** @var SliderFactory */
    public $factory;

    /** @var SliderDataFactory */
    public $dataFactory;
 
    public function __construct(
        SliderResource $resource,
        SliderFactory $factory,
        SliderDataFactory $dataFactory
    ) {
        $this->resource = $resource;
        $this->factory = $factory;
        $this->dataFactory = $dataFactory;
    }

    /**
     * GET for Slider API
     * @param int $id
     * @return \Xcoding\Slider\Model\Data\Slider
     */
    public function get($id)
    {
        $dataModel = $this->dataFactory->create();
        $slider = $this->factory->create();
        $this->resource->load($slider, $id);
        $dataModel->setSlider($slider);
        return $dataModel;
    }
}

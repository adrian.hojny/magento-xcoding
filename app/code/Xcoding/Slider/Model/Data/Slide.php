<?php
declare(strict_types=1);

namespace Xcoding\Slider\Model\Data;

class Slide extends \Magento\Framework\Api\AbstractSimpleObject
{
    /** @var \Scandiweb\Slider\Model\Slide */
    protected $slide;

    /**
     * @param \Scandiweb\Slider\Model\Slide $slide
     * 
     * @return $this
     */
    public function setSlide($slide)
    {
        $this->slide = $slide;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->slide->getId();
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->slide->getIsActive();
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->slide->getTitle();
    }

    /**
     * @return string|null
     */
    public function getStartTime()
    {
        return $this->slide->getStartTime();
    }

    /**
     * @return string|null
     */
    public function getEndTime()
    {
        return $this->slide->getEndTime();
    }

    /**
     * @return string|null
     */
    public function getPosition()
    {
        return $this->slide->getPosition();
    }

    /**
     * @return string|null
     */
    public function getMobileImage()
    {
        return $this->slide->getMobileImage();
    }

    /**
     * @return string|null
     */
    public function getMobileImage2()
    {
        return $this->slide->getMobileImage2();
    }

    /**
     * @return string|null
     */
    public function getMobileImage3()
    {
        return $this->slide->getMobileImage3();
    }

    /**
     * @return string|null
     */
    public function getDesktopImage()
    {
        return $this->slide->getDesktopImage();
    }

    /**
     * @return string|null
     */
    public function getDesktopImage2()
    {
        return $this->slide->getDesktopImage2();
    }

    /**
     * @return string|null
     */
    public function getDesktopImage3()
    {
        return $this->slide->getDesktopImage3();
    }

    /**
     * @return string|null
     */
    public function getSlideText()
    {
        return $this->slide->getSlideText();
    }

    /**
     * @return string|null
     */
    public function getSlideText2()
    {
        return $this->slide->getSlideText2();
    }

    /**
     * @return string|null
     */
    public function getSlideText3()
    {
        return $this->slide->getSlideText3();
    }
}


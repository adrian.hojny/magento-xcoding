<?php
declare(strict_types=1);

namespace Xcoding\Slider\Model\Data;

use Scandiweb\Slider\Model\ResourceModel\Slide\CollectionFactory;
use Xcoding\Slider\Model\Data\SlideFactory as SlideDataFactory;

class Slider extends \Magento\Framework\Api\AbstractSimpleObject
{
    /** @var CollectionFactory */
    protected $slideCollection;

    /** @var SlideDataFactory */
    public $slideDataFactory;

    public function __construct(
        CollectionFactory $slideCollection,
        SlideDataFactory $slideDataFactory
    ) {
        $this->slideCollection = $slideCollection;
        $this->slideDataFactory = $slideDataFactory;
    }

    /**
     * @param \Scandiweb\Slider\Model\Slider $slider
     * 
     * @return $this
     */
    public function setSlider($slider)
    {
        $this->slider = $slider;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->slider->getId();
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->slider->getTitle();
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->slider->getIsActive();
    }

    /**
     * @return \Xcoding\Slider\Model\Data\Slide[]
     */
    public function getSlides()
    {
        $collection = $this->slideCollection->create();
        $slides = [];
        foreach ($collection->addSliderFilter($this->slider->getId()) as $slide) {
            $slides[] = $this->slideDataFactory->create()->setSlide($slide);
        }
        return $slides;
    }
}


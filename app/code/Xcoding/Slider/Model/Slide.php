<?php
namespace Xcoding\Slider\Model;

class Slide extends \Scandiweb\Slider\Model\Slide
{
    /**
     * @param  bool $secure
     * @return string|bool
     */
    public function getTabletImageUrl($secure = false)
    {
        $path = $this->getTabletImage();
        if (!$path) {
            return false;
        }

        /** @var \Magento\Store\Model\Store */
        $store = $this->_storeManager->getStore();
        $base = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA, $secure);

        return $base . $path;
    }

    /**
     * @param  bool $secure
     * @return string|bool
     */
    public function getTabletImageUrl2($secure = false)
    {
        $path = $this->getTabletImage2();
        if (!$path) {
            return false;
        }

        /** @var \Magento\Store\Model\Store */
        $store = $this->_storeManager->getStore();
        $base = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA, $secure);

        return $base . $path;
    }

    /**
     * @param  bool $secure
     * @return string|bool
     */
    public function getTabletImageUrl3($secure = false)
    {
        $path = $this->getTabletImage3();
        if (!$path) {
            return false;
        }

        /** @var \Magento\Store\Model\Store */
        $store = $this->_storeManager->getStore();
        $base = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA, $secure);

        return $base . $path;
    }
}

<?php
namespace Xcoding\Slider\Setup\Patch\Schema;

use Magento\Framework\Setup\SchemaSetupInterface;

class AddTabletImage implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var SchemaSetupInterface
     */
    protected $setup;

    public function __construct(
        SchemaSetupInterface $setup
    ) {
        $this->setup = $setup;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function apply()
    {
        $setup = $this->setup;
        $setup->startSetup();
        $table = $setup->getTable('scandiweb_slider_slide');
        $connection = $setup->getConnection();
        $connection->addColumn(
            $table,
            'tablet_image',
            [
                'type' => 'text',
                'length' => 255,
                'nullable' => true,
                'comment' => 'Tablet Image location',
                'after' => 'desktop_image'
            ]
        );
        $connection->addColumn(
            $table,
            'tablet_image_2',
            [
                'type' => 'text',
                'length' => 255,
                'nullable' => true,
                'comment' => 'Tablet Image location',
                'after' => 'desktop_image_2'
            ]
        );
        $connection->addColumn(
            $table,
            'tablet_image_3',
            [
                'type' => 'text',
                'length' => 255,
                'nullable' => true,
                'comment' => 'Tablet Image location',
                'after' => 'desktop_image_3'
            ]
        );
        $setup->endSetup();
    }

    public function getAliases()
    {
        return [];
    }
}
<?php declare(strict_types=1);

namespace Xcoding\DeliveryDateGraphQl\Model\Resolver;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Xcoding\DeliveryDate\Api\FormatDatesInterface;
use Xcoding\DeliveryDate\Api\GetDeliveryTimeInterface;

/**
 * Class DeliveryDate
 * @package Xcoding\DeliveryDateGraphQl\Model\Resolver
 */
class DeliveryDate implements ResolverInterface
{
    /**
     * @var GetDeliveryTimeInterface
     */
    private $getDeliveryTime;

    /**
     * @var FormatDatesInterface
     */
    private $formatDates;

    /**
     * DeliveryDate constructor.
     * @param GetDeliveryTimeInterface $getDeliveryTime
     * @param FormatDatesInterface $formatDates
     */
    public function __construct(
        GetDeliveryTimeInterface $getDeliveryTime,
        FormatDatesInterface $formatDates
    ) {
        $this->getDeliveryTime = $getDeliveryTime;
        $this->formatDates = $formatDates;
    }

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed|Value
     * @throws Exception
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        /** @var ProductInterface|null $product */
        $product = $value['model'];
        $dates = null;
        $result = [];

        $formats = $args['formats'] ?? [FormatDatesInterface::DEFAULT_FORMAT];
        if ($product) {
            $dates = $this->getDeliveryTime->execute($product);
        }

        foreach ($formats as $format) {
            $result[] = [
                'format' => $format,
                'value' => $this->formatDates->execute($dates, $format)
            ];
        }

        return $result;
    }
}

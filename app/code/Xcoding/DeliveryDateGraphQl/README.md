# Xcoding_DeliveryDateGraphQl

Moduł rozszerzający Xcoding_DeliveryDate o GraphQl

Zapytanie o domyślny typ (iso date):

```graphql
{
  products(filter: { sku: { eq: "n31253492" } }) {
    items {
      delivery_date {
        value
      }
    }
  }
}
```

odpowiedź:
```graphql
{
  "data": {
    "products": {
      "items": [
        {
          "delivery_date": [
            {
              "value": [
                "2020-06-08T00:00:00+02:00",
                "2020-06-09T00:00:00+02:00"
              ]
            }
          ]
        }
      ]
    }
  }
}
```


Przykładowe zapytanie o wiele formatów:
```graphql
{
  products(filter: { sku: { eq: "n31253492" } }) {
    items {   
      delivery_date(formats:[DATE, WEEKDAY, DAYS_FROM_NOW, ISO_DATE]) {
        format
        value
      }
    }
  }
}

```
Przykładowa odpowiedź:
```graphql
{
  "data": {
    "products": {
      "items": [
        {
          "delivery_date": [
            {
              "format": "DATE",
              "value": [
                "08.06.2020",
                "09.06.2020"
              ]
            },
            {
              "format": "WEEKDAY",
              "value": [
                "Monday",
                "Tuesday"
              ]
            },
            {
              "format": "DAYS_FROM_NOW",
              "value": [
                "08.06.2020",
                "09.06.2020"
              ]
            },
            {
              "format": "ISO_DATE",
              "value": [
                "2020-06-08T00:00:00+02:00",
                "2020-06-09T00:00:00+02:00"
              ]
            }
          ]
        }
      ]
    }
  }
}
```
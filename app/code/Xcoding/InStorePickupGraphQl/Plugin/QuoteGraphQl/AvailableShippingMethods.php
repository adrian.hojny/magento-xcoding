<?php

namespace Xcoding\InStorePickupGraphQl\Plugin\QuoteGraphQl;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Xcoding\InStorePickup\Helper\Data as ModuleHelper;

class AvailableShippingMethods
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;

    /**
     * @var ModuleHelper
     */
    protected $moduleHelper;

    /**
     * AvailableShippingMethods constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param ModuleHelper $moduleHelper
     * @param JsonSerializer $jsonSerializer
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ModuleHelper $moduleHelper,
        JsonSerializer $jsonSerializer
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->jsonSerializer = $jsonSerializer;
        $this->moduleHelper = $moduleHelper;
    }

    public function afterResolve(
        $subject,
        $result
    ) {
        foreach ($result as &$item) {
            if ($item['carrier_code'] === 'pickup' && $item['method_code'] === 'instorepickup') {
                $item['pickup_points'] = $this->getInStorePickupPoints();
            }
        }

        return $result;
    }

    public function getInStorePickupPoints()
    {
        return $this->moduleHelper->getPickupPoints();
    }
}
# Xcoding_InStorePickupGraphQl

#### Lista punktów odbioru osobistego

Przykładowe zapytanie (flow Scandi):
```graphql
mutation EstimateShippingCosts(
    $guestCartId: String!
    $address: EstimateShippingCostsAddress!
) {
    estimateShippingCosts(address: $address, guestCartId: $guestCartId) {
        carrier_code
        method_code
        carrier_title
        method_title
        error_message
        amount
        base_amount
        price_excl_tax
        price_incl_tax
        available
        pickup_points {
            address
            code
        }
    }
}
```
Parametry:

```json
{
	"guestCartId": "A8SP2P5mF9NyKKhECzMwLytIj3gqrflc",
	"address": {
		"region": "New York",
		"region_id": 43,
		"region_code": "NY",
		"country_id": "US",
		"street": [
			"123 Oak Ave"
		],
		"postcode": "10577",
		"city": "Purchase",
		"firstname": "Jane",
		"lastname": "Doe",
		"customer_id": 4,
		"email": "jdoe@example.com",
		"telephone": "(512) 555-1111",
		"same_as_billing": 1
	}
}
```

Przykładowa odpowiedź:
```json
{
  "data": {
    "estimateShippingCosts": [
      {
        "carrier_code": "pickup",
        "method_code": "instorepickup",
        "carrier_title": "In store pickup",
        "method_title": "In store pickup",
        "error_message": "false",
        "amount": 2.2,
        "base_amount": 2.2,
        "price_excl_tax": 2.2,
        "price_incl_tax": 2.2,
        "available": true,
        "pickup_points": [
          {
            "address": "Testowa 12, 12-345 Warszawa, koło lidla",
            "code": "p1"
          },
          {
            "address": "Testowa 12, 12-345 Warszawa",
            "code": "p2"
          },
          {
            "address": "Inna testowa 12, 12-345 Warszawa",
            "code": "p3"
          }
        ]
      },
      {
        "carrier_code": "flatrate",
        "method_code": "flatrate",
        "carrier_title": "Flat Rate",
        "method_title": "Fixed",
        "error_message": "false",
        "amount": 5,
        "base_amount": 5,
        "price_excl_tax": 5,
        "price_incl_tax": 5,
        "available": true,
        "pickup_points": null
      },
      {
        "carrier_code": "dpd",
        "method_code": "dpdcourier",
        "carrier_title": "DPD",
        "method_title": "Kurier",
        "error_message": "false",
        "amount": 12,
        "base_amount": 12,
        "price_excl_tax": 12,
        "price_incl_tax": 12,
        "available": true,
        "pickup_points": null
      },
      {
        "carrier_code": "inpost",
        "method_code": "inpostmachine",
        "carrier_title": "InPost",
        "method_title": "Paczkomaty",
        "error_message": "false",
        "amount": 7,
        "base_amount": 7,
        "price_excl_tax": 7,
        "price_incl_tax": 7,
        "available": true,
        "pickup_points": null
      }
    ]
  }
}
```

Przykładowe zapytanie (core Magento):
```graphql
mutation {
  setShippingAddressesOnCart(
    input: {
      cart_id: "l1Oz15e4fAlomB7GD4usdBqOGW120zNB"
      shipping_addresses: [
        {
          address: {
            firstname: "S SSSJohn"
            lastname: " SSSS Doe"
            company: "Company Name"
            street: ["320 N Crescent Dr", "Beverly Hills"]
            city: "Los Angeles"
            region: "CA"
            postcode: "90210"
            country_code: "US"
            telephone: "123-456-0000"
            save_in_address_book: false
          }
        }
      ]
    }
  ) {
    cart {
      shipping_addresses {
        available_shipping_methods {
          carrier_code
          method_code
          carrier_title
          available
          pickup_points {
            code
            address
          }
          amount {
            currency
            value
          }
        }
      }
    }
  }
}
```
Przykładowa odpowiedź:
```json
{
  "data": {
    "setShippingAddressesOnCart": {
      "cart": {
        "shipping_addresses": [
          {
            "available_shipping_methods": [
              {
                "carrier_code": "pickup",
                "method_code": "instorepickup",
                "carrier_title": "In store pickup",
                "available": true,
                "pickup_points": [
                  {
                    "code": "p1",
                    "address": "f Testowa 12, 12-345 Warszawa, koło lidla"
                  },
                  {
                    "code": "p2",
                    "address": "f Inna tstowa 12, 12-345 Warszawa"
                  },
                  {
                    "code": "p3",
                    "address": "f jeszcze Inna tstowa 12, 12-345 Warszawa"
                  }
                ],
                "amount": {
                  "currency": "USD",
                  "value": 2.2
                }
              },
              {
                "carrier_code": "flatrate",
                "method_code": "flatrate",
                "carrier_title": "Flat Rate",
                "available": true,
                "pickup_points": null,
                "amount": {
                  "currency": "USD",
                  "value": 5
                }
              },
              {
                "carrier_code": "dpd",
                "method_code": "dpdcourier",
                "carrier_title": "DPD",
                "available": true,
                "pickup_points": null,
                "amount": {
                  "currency": "USD",
                  "value": 12
                }
              },
              {
                "carrier_code": "inpost",
                "method_code": "inpostmachine",
                "carrier_title": "InPost",
                "available": true,
                "pickup_points": null,
                "amount": {
                  "currency": "USD",
                  "value": 7
                }
              }
            ]
          }
        ]
      }
    }
  }
}
```

#### Wybór punktu odbioru

Wybór punktu odbioru odbywa się przez mutację ScandiPWA `SaveAddressInformation`, pole `pickup_store_code` w inpucie `SaveAddressInformation`

Przykładowe zapytanie:
```graphql
mutation SaveAddressInformation(
  	$addressInformation: SaveAddressInformation!
  	$guestCartId: String
) {
	saveAddressInformation(
		addressInformation: $addressInformation,
    	guestCartId: $guestCartId
  	) {
  		payment_methods {
    		code
    		title
  		}
    	totals {
			grand_total
      		items {
        		name
        		qty
      		}
    	}
	}
}
```
parametry:
```json
{
	"guestCartId": "HquCVnwvRIg8IrDInNPdZmsdagYDVOol",
	"addressInformation": {
		"shipping_address": {
			"region": "New Yorkeeeeer",
			"region_id": 43,
			"region_code": "NY",
			"country_id": "US",
			"street": [
				"123 Oak Ave"
			],
			"postcode": "10577",
			"city": "Purchase",
			"firstname": "Jane",
			"lastname": "Doe",
			"email": "jdoe@example.com",
			"telephone": "512-555-1111"
		},
		"billing_address": {
			"region": "New York",
			"region_id": 43,
			"region_code": "NY",
			"country_id": "US",
			"street": [
				"123 Oak Ave"
			],
			"postcode": "10577",
			"city": "Purchase",
			"firstname": "Jane",
			"lastname": "Doe",
			"email": "jdoe@example.com",
			"telephone": "512-555-1111"
		},
		"shipping_carrier_code": "pickup",
		"shipping_method_code": "instorepickup",
		"pickup_store_code": "p3"
	}
}
```

przykładowa odpowierdź:

```json
{
  "data": {
    "saveAddressInformation": {
      "payment_methods": [
        {
          "code": "checkmo",
          "title": "Check \/ Money order"
        },
        {
          "code": "dialcom_przelewy",
          "title": "Przelewy24"
        },
        {
          "code": "banktransfer",
          "title": "Bank Transfer Payment"
        }
      ],
      "totals": {
        "grand_total": 120.14,
        "items": [
          {
            "name": "aaaaaaan",
            "qty": 1
          }
        ]
      }
    }
  }
}
```
<?php

namespace Xcoding\Cms\Setup;

use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Xcoding\Cms\Helper\Data;
use Xcoding\Cms\Model\BlockManager;
use Xcoding\Cms\Model\PageManager;

/**
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
    * @var blockManager
    */
    protected $blockManager;

    /**
    * @var PageManager
    */
    protected $pageManager;

    /**
    * @var Data
    */
    protected $helper;

    /**
    * UpgradeData constructor.
    * @param BlockManager $blockManager
    * @param PageManager $pageManager
    * @param Data $helper
    */
    public function __construct(
        BlockManager $blockManager,
        PageManager $pageManager,
        Data $helper
    ) {
        $this->blockManager = $blockManager;
        $this->pageManager = $pageManager;
        $this->helper = $helper;
    }

    /**
    * @param ModuleDataSetupInterface $setup
    * @param ModuleContextInterface $context
    */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->createBlocks($context);
        $this->createPages($context);

        $setup->endSetup();
    }

    /**
    * The main method for installing CMS Blocks.
    * The name of file should be equal like identifier.
    * @param ModuleContextInterface $context
    * @return void
    */
    private function createBlocks($context)
    {
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->createFooterBlocks();
        }
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->createAdvertisingTiles();
        }
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $this->createHeaderBlocks();
        }
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $this->createProductBlocks();
        }
        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            $this->createDeliveryBlock();
        }
        if (version_compare($context->getVersion(), '1.0.7', '<')) {
            $this->createDoubleboxBlock();
        }
        if (version_compare($context->getVersion(), '1.0.8', '<')) {
            $this->createHelpLineBlock();
        }
        if (version_compare($context->getVersion(), '1.0.9', '<')) {
            $this->createHomepageDiscountBlock();
        }
        if (version_compare($context->getVersion(), '1.0.10', '<')) {
            $this->createCheckoutBlock();
        }
        if (version_compare($context->getVersion(), '1.0.12', '<')) {
            $this->createRegisterBlocks();
        }
        if (version_compare($context->getVersion(), '1.0.11', '<')) {
            $this->createContactBlock();
        }
        if (version_compare($context->getVersion(), '1.0.13', '<')) {
            $this->createWeekBrandBlock();
        }

        if (version_compare($context->getVersion(), '1.0.16', '<')) {
            $this->createBrandsMegaMenuBlock();
        }

        if (version_compare($context->getVersion(), '1.0.17', '<')) {
            $this->createPremiumBrandSliderWrapBlock();
            $this->createPremiumBrandsListBlocks();
            $this->createPremiumBrandsDescription();
            $this->createPremiumBrandsFeatured();
            $this->createPremiumBrandsFeaturedWithDescription();
            $this->createPremiumBrandsFeaturedWithDescription();
            $this->createHeroAndSeeProducts();
        }

        if (version_compare($context->getVersion(), '1.0.18', '<')) {
            $this->createHelpAndAgreementsBlocks();
        }
        if (version_compare($context->getVersion(), '1.0.19', '<')) {
            $this->createAgreementsBlocks();
        }
        if (version_compare($context->getVersion(), '1.0.20', '<')) {
            $this->createSaleBlock();
        }
        if (version_compare($context->getVersion(), '1.0.21', '<')) {
            $this->createComingSoonBlocks();
        }
        if (version_compare($context->getVersion(), '1.0.22', '<')) {
            $this->createMethodsIconsBlock();
        }
        if (version_compare($context->getVersion(), '1.0.23', '<')) {
            $this->createPopularBrandsBlock();
        }
        if (version_compare($context->getVersion(), '1.0.24', '<')) {
            $this->createPradaBlock();
            $this->createHomepageNewProductsBlock();
            $this->createTripleImageBlock();
            $this->createImageLinksBlock();
        }
        if (version_compare($context->getVersion(), '1.0.25', '<')) {
            $this->createPrmHomepageRecommendedProductsBlock();
            $this->createSaleBlockPRM();
        }
        if (version_compare($context->getVersion(), '1.0.26', '<')) {
            $this->createSneakerstudioTermsCheckbox();
        }
        if (version_compare($context->getVersion(), '1.0.27', '<')) {
            $this->createSaleBlockPRM();
        }
        if (version_compare($context->getVersion(), '1.0.28', '<')) {
            $this->createBrandOfTheMonthBlock();
        }
        if (version_compare($context->getVersion(), '1.0.29', '<')) {
            $this->createSneakerstudioRegisterTermsCheckbox();
        }
        if (version_compare($context->getVersion(), '1.0.30', '<')) {
            $this->createFeaturedBrandsButtonBelow();
            $this->createPremiumBrandsFeaturedWithDescription();
            $this->createPrmPremiumBrandsSeeProducts();
        }
        if (version_compare($context->getVersion(), '1.0.31', '<')) {
            $this->createPremiumBrandsFeaturedWithDescription();
        }
        if (version_compare($context->getVersion(), '1.0.32', '<')) {
            $this->createPremiumBrandsBrandsListHeader();
        }
        if (version_compare($context->getVersion(), '1.0.33', '<')) {
            $this->createBrandPageBlock();
        }
        if (version_compare($context->getVersion(), '1.0.34', '<')) {
            $this->createBrandNewProductsBlock();
        }
        if (version_compare($context->getVersion(), '1.0.35', '<')) {
            $this->createPrmContactPage();
        }
        if (version_compare($context->getVersion(), '1.0.36', '<')) {
            $this->createSizingLinksBlocks();
        }
    }

    /**
    * The main method for installing CMS Pages.
    * The name of file should be equal like identifier.
    * @param ModuleContextInterface $context
    */
    private function createPages($context)
    {
        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            //            $this->createExamplePage();
        }
    }

    /**
    * Create example static block.
    */
    private function createFooterBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Footer/footer-contact.phtml');
        $this->blockManager->createBlock('footer-contact', 'Footer contact', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Footer/footer-info.phtml');
        $this->blockManager->createBlock('footer-info', 'Footer info', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Footer/footer-help.phtml');
        $this->blockManager->createBlock('footer-help', 'Footer Help', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Footer/footer-socials.phtml');
        $this->blockManager->createBlock('footer-socials', 'Footer Socials', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Footer/footer-icons.phtml');
        $this->blockManager->createBlock('footer-icons', 'Footer Icons', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Footer/footer-bottom-links.phtml');
        $this->blockManager->createBlock('footer-bottom-links', 'Footer Bottom Links', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Category/category-bottom-fullwidth-right.phtml');
        $this->blockManager->createBlock('category-bottom-fullwidth-right', 'Category bottom full width right', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Category/category-bottom-fullwidth-left.phtml');
        $this->blockManager->createBlock('category-bottom-fullwidth-left', 'Category bottom full width left', $content);
    }

    /**
     * Create example static block.
     */
    private function createAdvertisingTiles()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Category/category-tile-example.phtml');
        $this->blockManager->createBlock('category-tile/20', 'Kafelki reklamowe category/men', $content);
    }

    private function createHeaderBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Header/header-info-bar.phtml');
        $this->blockManager->createBlock('header-info-bar', 'Header Info Bar', $content);
    }

    private function createProductBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Product/club-points-info.phtml');
        $this->blockManager->createBlock('club-points-info', 'Club Points Information', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Product/product-info-list.phtml');
        $this->blockManager->createBlock('product-info-list', 'Product Information List', $content);
    }

    private function createDeliveryBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Product/delivery.phtml');
        $this->blockManager->createBlock('delivery', 'Dostawa i płatność', $content);
    }

    private function createDoubleboxBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Cart/cart-doublebox.phtml');
        $this->blockManager->createBlock('doublebox', 'Doublebox', $content);
    }

    private function createHelpLineBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Cart/cart-helpline.phtml');
        $this->blockManager->createBlock('helpline', 'Cart Helpline', $content);
    }

    private function createHomepageDiscountBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/home-discount.phtml');
        $this->blockManager->createBlock('home-discount', 'Home Discount', $content);
    }

    private function createCheckoutBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Checkout/checkout-success-message.phtml');
        $this->blockManager->createBlock('checkout-success-message', 'Checkout Success Message', $content);
    }

    private function createRegisterBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Register/register-info-block.phtml');
        $this->blockManager->createBlock('register-info-block', 'Rejestracja - blok informacyjny', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Register/register-data-compliance.phtml');
        $this->blockManager->createBlock('register-data-compliance', 'Rejestracja - przetwarzanie danych', $content);
    }

    private function createContactBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Contact/vcard.phtml');
        $this->blockManager->createBlock('contact-vcard', 'Contact vCard', $content);
    }

    private function createWeekBrandBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/home-week-brand.phtml');
        $this->blockManager->createBlock('home-week-brand', 'Brand of the Week', $content);
    }

    private function createPopularBrandsBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/home-popular-brands.phtml');
        $this->blockManager->createBlock('home-popular-brands', 'Popular brands', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/home-brands-tiles.phtml');
        $this->blockManager->createBlock('home-brands-tiles', 'Brands Tiles', $content);
    }

    private function createSaleBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/homepage-sale-block-1.phtml');
        $this->blockManager->createBlock('homepage-sale-block-1', 'Homepage Sale Block 1', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/homepage-sale-block-2.phtml');
        $this->blockManager->createBlock('homepage-sale-block-2', 'Homepage Sale Block 2', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/homepage-sale-block-3.phtml');
        $this->blockManager->createBlock('homepage-sale-block-3', 'Homepage Sale Block 3', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/homepage-sale-block-4.phtml');
        $this->blockManager->createBlock('homepage-sale-block-4', 'Homepage Sale Block 4', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/homepage-sale-block-title.phtml');
        $this->blockManager->createBlock('homepage-sale-block-15', 'Homepage Sale Block Title', $content);
    }

    private function createBrandsMegaMenuBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Header/brand-menu-left-featured-brand');
        $this->blockManager->createBlock('brand-menu-left-featured-brand', 'Brand Menu Left - Featured Brand', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Header/brand-menu-left');
        $this->blockManager->createBlock('brand-menu-left', 'Brand Menu Left', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Header/brand-menu-right');
        $this->blockManager->createBlock('brand-menu-right', 'Brand Menu Right', $content);
    }


    /* Premium brands blocks */
    private function createPremiumBrandsDescription()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/brand-description.phtml');
        $this->blockManager->createBlock('premium-brands-description', 'Premium brands description', $content);
    }
    private function createPremiumBrandSliderWrapBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/slider-wrap.phtml');
        $this->blockManager->createBlock('premium-brands-slider-wrap', 'Premium brands slider wrap', $content);
    }

    private function createPremiumBrandsListBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/brands-list1.phtml');
        $this->blockManager->createBlock('brands-list1', 'Premium brands list column and slide 1', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/brands-list2.phtml');
        $this->blockManager->createBlock('brands-list2', 'Premium brands list column and slide 2', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/brands-list3.phtml');
        $this->blockManager->createBlock('brands-list3', 'Premium brands list column and slide 3', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/brands-list4.phtml');
        $this->blockManager->createBlock('brands-list4', 'Premium brands list column and slide 4', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/brands-list5.phtml');
        $this->blockManager->createBlock('brands-list5', 'Premium brands list column and slide 5', $content);
    }


    private function createPremiumBrandsFeatured()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/featured-brands-top.phtml');
        $this->blockManager->createBlock('premium-brands-feature-top', 'Featured brands top', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/featured-brands-middle.phtml');
        $this->blockManager->createBlock('premium-brands-feature-middle', 'Featured brands middle', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/featured-brands-bottom.phtml');
        $this->blockManager->createBlock('premium-brands-feature-bottom', 'Featured brands bottom', $content);
    }


    private function createPremiumBrandsFeaturedWithDescription()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/featured-with-description.phtml');
        $this->blockManager->createBlock('featured-with-desc', 'Featured with desc wrap', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/featured-with-description-slide1.phtml');
        $this->blockManager->createBlock('featured-with-desc-slide1', 'Featured with description: slide 1', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/featured-with-description-slide2.phtml');
        $this->blockManager->createBlock('featured-with-desc-slide2', 'Featured with description: slide 2', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/featured-with-description-slide3.phtml');
        $this->blockManager->createBlock('featured-with-desc-slide2', 'Featured with description: slide 3', $content);
    }

    private function createHeroAndSeeProducts()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/hero.phtml');
        $this->blockManager->createBlock('premium-brands-hero', 'Premium brands hero', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/see-products.phtml');
        $this->blockManager->createBlock('premium-brands-see-products', 'Premium brands see products', $content);
    }

    private function createHelpAndAgreementsBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/complaints.phtml');
        $this->blockManager->createBlock('qa-complaints', 'Q&A Complaints page', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/delivery-methods.phtml');
        $this->blockManager->createBlock('qa-delivery-methods', 'Q&A Delivery methods page', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/fullpage.phtml');
        $this->blockManager->createBlock('qa-fullpage-example', 'Q&A FullPage example', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/menu.phtml');
        $this->blockManager->createBlock('qa-menu', 'Q&A Menu block', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/payment-methods.phtml');
        $this->blockManager->createBlock('qa-payment-methods', 'Q&A Payment methods page', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/qanda.phtml');
        $this->blockManager->createBlock('qa-qanda', 'Q&A Help page', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/returns.phtml');
        $this->blockManager->createBlock('qa-returns', 'Q&A Returns page', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/shoe-care.phtml');
        $this->blockManager->createBlock('qa-shoe-care', 'Q&A Shoe care page', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/warranty.phtml');
        $this->blockManager->createBlock('qa-warranty', 'Q&A Warranty page', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/delivery-map-legend-table.phtml');
        $this->blockManager->createBlock('qa-delivery-map-table-legend', 'Q&A Delivery map tables legend', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/couriers.phtml');
        $this->blockManager->createBlock('qa-couriers', 'Q&A Couriers block', $content);
    }

    private function createAgreementsBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'QAandHelp/aggrements.phtml');
        $this->blockManager->createBlock('aggrements', 'Aggrements page', $content);
    }

    private function createComingSoonBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/home-coming-soon.phtml');
        $this->blockManager->createBlock('homepage-coming-soon', 'Homepage Coming Soon', $content);
    }

    private function createMethodsIconsBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/home-methods-icons.phtml');
        $this->blockManager->createBlock('home-methods-icons', 'Shipping and payment methods icons', $content);
    }

    private function createPradaBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/prm-prada-block.phtml');
        $this->blockManager->createBlock('prm-prada-block', 'Prm prada block', $content);
    }

    private function createSaleBlockPRM()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/prm-homepage-sale-block-1.phtml');
        $this->blockManager->createBlock('prm-homepage-sale-block-1', 'PRM Homepage Sale Block 1', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/prm-homepage-sale-block-2.phtml');
        $this->blockManager->createBlock('prm-homepage-sale-block-2', 'PRM Homepage Sale Block 2', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/prm-homepage-sale-block-3.phtml');
        $this->blockManager->createBlock('prm-homepage-sale-block-3', 'PRM Homepage Sale Block 3', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/prm-homepage-sale-block-4.phtml');
        $this->blockManager->createBlock('prm-homepage-sale-block-4', 'PRM Homepage Sale Block 4', $content);

        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/prm-homepage-sale-block-title.phtml');
        $this->blockManager->createBlock('prm-homepage-sale-block-15', 'PRM Homepage Sale Block Title', $content);
    }

    private function createHomepageNewProductsBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/homepage-new-products.phtml');
        $this->blockManager->createBlock('homepage-new-products', 'Homepage new products slider', $content);
    }

    private function createPrmHomepageRecommendedProductsBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/prm-homepage-recommended-products.phtml');
        $this->blockManager->createBlock('prm-homepage-recommended-products', 'PRM Homepage recommended products slider', $content);
    }

    private function createSneakerstudioTermsCheckbox()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Checkout/sneakerstudio-terms-checkbox.phtml');
        $this->blockManager->createBlock('sneakerstudio-terms-checkbox', 'Sneakerstudio Terms Checkbox', $content);
    }

    private function createSneakerstudioRegisterTermsCheckbox()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Register/sneakerstudio-register-terms-checkbox.phtml');
        $this->blockManager->createBlock('sneakerstudio-register-terms-checkbox', 'Sneakerstudio Register Terms Checkbox', $content);
    }

    private function createTripleImageBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/homepage-triple-image.phtml');
        $this->blockManager->createBlock('homepage-triple-image', 'Homepage triple-image', $content);
    }

    private function createImageLinksBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/homepage-image-links.phtml');
        $this->blockManager->createBlock('homepage-image-links', 'Homepage image links', $content);
    }

    private function createBrandOfTheMonthBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Home/home-month-brand.phtml');
        $this->blockManager->createBlock('home-month-brand', 'Brand of the month', $content);
    }

    private function createFeaturedBrandsButtonBelow()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/featured-brands-button-below.phtml');
        $this->blockManager->createBlock('featured-brands-button-below', 'Featured brands button below', $content);
    }

    private function createPrmPremiumBrandsSeeProducts()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/prm-see-products.phtml');
        $this->blockManager->createBlock('prm-see-products', 'PRM Premium brands see products', $content);
    }

    private function createPremiumBrandsBrandsListHeader()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'PremiumBrands/premium-brands-brands-list-header.phtml');
        $this->blockManager->createBlock('premium-brands-brands-list-header', 'Premium Brands - Brands List Header', $content);
    }

    private function createBrandPageBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Brand/brand_gender_buttons.phtml');
        $this->blockManager->createBlock('brand_gender_buttons', 'Brand - gender buttons', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Brand/brand_discover.phtml');
        $this->blockManager->createBlock('brand_discover', 'Brand - discover', $content);
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Brand/brand_sneakerstudio.phtml');
        $this->blockManager->createBlock('brand_sneakerstudio', 'Brand - sneakerstudio', $content);
    }

    private function createBrandNewProductsBlock()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Brand/brand-new-products-example.phtml');
        $this->blockManager->createBlock('brand-new-products-example', 'Brand new products', $content);
    }

    private function createPrmContactPage()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Contact/prm_contact_page.phtml');
        $this->blockManager->createBlock('prm_contact_page', 'Prm contact page', $content);
    }

    private function createSizingLinksBlocks()
    {
        $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'Sizing/sizing_links.phtml');
        $this->blockManager->createBlock('sizing_links', 'Sizing page links', $content);
    }

    /**
    * Create example CMS Page
    */
    private function createExamplePage()
    {
        $options = [
            'identifier' => 'example_identifier',
            'title' => 'Example Title',
            'content_heading' => 'Example Content Heading',
            'content' => $this->helper->getFileContent(Data::PATH_TO_PAGES . 'example.phtml'),
            'page_layout' => 'cms-nav'
        ];

        $this->pageManager->createPage($options);
    }
}

<?php

namespace Xcoding\Cms\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        if (version_compare($context->getVersion(), '1.0.15', '<')) {
            $installer = $setup;
            $installer->startSetup();
            $connection = $installer->getConnection();

            $connection->addColumn(
                'cms_page',
                'scandipwa_modifiers',
                [
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Modifiers used by scandipwa frontend theme'
                ]
            );
            $installer->endSetup();
        }
    }
}

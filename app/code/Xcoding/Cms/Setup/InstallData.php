<?php

namespace Xcoding\Cms\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Xcoding\Cms\Model\BlockManager;

class InstallData implements InstallDataInterface
{
    /** @var blockManager */
    protected $blockManager;

    /**
     * InstallData constructor.
     * @param BlockManager $blockManager
     */
    public function __construct(
        BlockManager $blockManager
    ) {
        $this->blockManager = $blockManager;
    }

    /**
    * @param ModuleDataSetupInterface $setup
    * @param ModuleContextInterface $context
    */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
    }
}

<?php

namespace Xcoding\Cms\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\Dir\Reader;

/**
 * Class Data
 * @package Xcoding\Cms\Helper
 */
class Data extends AbstractHelper
{
    /**
    * Layout type
    */
    const LAYOUT_1COLUMN = '1column';

    /**
    * Name of this module
    */
    const MODULE_NAME = 'Xcoding_Cms';

    /**
    * Path to files which contents html for cms pages/blocks
    */
    const PATH_TO_PAGES = '/Content/Page/';
    const PATH_TO_BLOCKS = '/Content/Block/';

    /**
    * @var Reader
    */
    protected $reader;

    /**
    * Data constructor.
    * @param Context $context
    * @param Reader $reader
    */
    public function __construct(
        Context $context,
        Reader $reader
    ) {
        $this->reader = $reader;
        parent::__construct($context);
    }

    /**
    * @param $filename
    * @return false|string
    */
    public function getFileContent($filename)
    {
        $content = '';

        $file = $this->getModuleDir(Data::MODULE_NAME) . $filename;

        if (file_exists($file)) {
            $content = file_get_contents($file);
        }

        return $content;
    }

    /**
    * @param        $moduleName
    * @param string $type
    * @return string
    */
    public function getModuleDir($moduleName, $type = '')
    {
        return $this->reader->getModuleDir($type, $moduleName);
    }
}

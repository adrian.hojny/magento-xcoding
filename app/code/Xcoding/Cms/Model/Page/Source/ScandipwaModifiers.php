<?php

namespace Xcoding\Cms\Model\Page\Source;

use Magento\Framework\Data\OptionSourceInterface;

class ScandipwaModifiers implements OptionSourceInterface
{
    public function toOptionArray()
    {
        // don't use comma in value
        return [
            [
                'value' => 'hide-breadcrumbs',
                'label' => 'Hide breadcrumbs'
            ],
            [
                'value' => 'hide-page-title',
                'label' => 'Hide page title'
            ],
            [
                'value' => 'transparent-header',
                'label' => 'Transparent header'
            ]
        ];
    }
}
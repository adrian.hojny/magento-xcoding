<?php

namespace Xcoding\Cms\Model;

use Exception;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Api\Data\BlockInterfaceFactory;
use Magento\Cms\Model\Block;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;

/**
 * Class BlockManager
 * @package Xcoding\Cms\Model
 */
class BlockManager
{
    /** @var    BlockRepositoryInterface */
    protected $blockRepository;

    /** @var BlockInterfaceFactory */
    protected $blockInterfaceFactory;

    /** @var LoggerInterface */
    protected $logger;

    /**
    * InstallData constructor.
    * @param BlockRepositoryInterface $blockRepository
    * @param BlockInterfaceFactory $blockInterfaceFactory
    */
    public function __construct(
        BlockRepositoryInterface $blockRepository,
        BlockInterfaceFactory $blockInterfaceFactory,
        LoggerInterface $logger
    ) {
        $this->blockRepository = $blockRepository;
        $this->blockInterfaceFactory = $blockInterfaceFactory;
        $this->logger = $logger;
    }

    /**
    * @param string $blockIdentifier
    * @param string $blockTitle
    * @param string $content
    * @param array $stores
    * @return $this
    */
    public function createBlock($blockIdentifier = '', $blockTitle = '', $content = '', $stores = [])
    {
        if (!$stores) {
            $stores = [Store::DEFAULT_STORE_ID];
        }
        /** @var Block $cmsBlock */
        $cmsBlock = $this->blockInterfaceFactory->create();

        try {
            $blockId = $this->blockRepository->getById($blockIdentifier)->getId();
            if ($blockId) {
                $cmsBlock->setId($blockId);
            }
        } catch (NoSuchEntityException $e) {
            /** @var Block $cmsBlock */
            $this->logger->critical($e->getMessage());
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        $cmsBlock->setIdentifier($blockIdentifier);
        $cmsBlock->setTitle($blockTitle);
        $cmsBlock->setContent($content);
        $cmsBlock->setData('stores', $stores);

        try {
            $this->blockRepository->save($cmsBlock);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return $this;
    }
}

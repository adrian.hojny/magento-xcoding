<?php
/**
 * @author      Maciej Duda <m.duda@xcitstudio.com>
 * @copyright   Copyright (c) 2019 X-coding IT Studio
 */

namespace Xcoding\Cms\Model;

use Exception;
use Magento\Cms\Api\Data\PageInterfaceFactory;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Cms\Model\Page;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;

/**
 * Class PageManager
 * @package Xcoding\Cms\Model
 */
class PageManager
{
    /**
    * @var LoggerInterface
    */
    public $logger;

    /**
    * @var  PageRepositoryInterface
    */
    protected $pageRepository;

    /**
    * @var PageInterfaceFactory
    */
    protected $pageInterfaceFactory;

    /**
    * PageManager constructor.
    * @param PageRepositoryInterface $pageRepository
    * @param PageInterfaceFactory $pageInterfaceFactory
    * @param LoggerInterface $logger
    */
    public function __construct(
        PageRepositoryInterface $pageRepository,
        PageInterfaceFactory $pageInterfaceFactory,
        LoggerInterface $logger
    ) {
        $this->pageRepository = $pageRepository;
        $this->pageInterfaceFactory = $pageInterfaceFactory;
        $this->logger = $logger;
    }

    /**
    * @param array $options
    * @return $this
    */
    public function createPage($options)
    {
        if (!isset($options['stores'])) {
            $options['stores'] = [Store::DEFAULT_STORE_ID];
        }

        /** @var Page $cmsPage */
        $cmsPage = $this->pageInterfaceFactory->create();
        $cmsPage->setData($options);

        try {
            $this->logger->log('info', 'Saving the CMS PAGE with identifier ' . $options['identifier']);
            $this->pageRepository->save($cmsPage);
            $this->logger->log('info', 'Saved the CMS Page.');
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return $this;
    }
}

<?php

namespace Xcoding\Cms\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Magento\Catalog\Model\ResourceModel\Attribute as AttributeResource;
use Magento\Cms\Model\ResourceModel\Block as BlockResource;
use Magento\Cms\Model\ResourceModel\Block\CollectionFactory as BlockCollectionFactory;
use Magento\Cms\Model\ResourceModel\Page as PageResource;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory as PageCollectionFactory;

/**
 * Class ReplaceString
 */
class ReplaceString extends \Symfony\Component\Console\Command\Command
{
    /** @var AttributeResource */
    private $attributeResource;

    /** @var BlockResource */
    private $blockResource;

    /** @var BlockCollectionFactory */
    private $blockCollectionFactory;

    /** @var PageResource */
    private $pageResource;

    /** @var PageCollectionFactory */
    private $pageCollectionFactory;

    /**
     * @param AttributeResource $attributeResource
     * @param BlockResource $blockResource
     * @param BlockCollectionFactory $blockCollectionFactory
     * @param PageResource $pageResource
     * @param PageCollectionFactory $pageCollectionFactory
     */
    public function __construct(
        AttributeResource $attributeResource,
        BlockResource $blockResource,
        BlockCollectionFactory $blockCollectionFactory,
        PageResource $pageResource,
        PageCollectionFactory $pageCollectionFactory
    ) {
        $this->attributeResource = $attributeResource;
        $this->blockResource = $blockResource;
        $this->blockCollectionFactory = $blockCollectionFactory;
        $this->pageResource = $pageResource;
        $this->pageCollectionFactory = $pageCollectionFactory;

        parent::__construct('xcoding:cms:replacestring');
    }

    /**
     * Configure command options
     */
    protected function configure()
    {
        $definition = [
            new InputOption('from', null, InputOption::VALUE_REQUIRED, 'From'),
            new InputOption('to', null, InputOption::VALUE_REQUIRED, 'To'),
        ];
        $this->setName('xcoding:cms:replacestring');
        $this->setDescription('Replace string in cms');
        $this->setDefinition($definition);
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $from = $input->getOption('from');
        $to = $input->getOption('to');

        // CMS Blocks
        $blockCollection = $this->blockCollectionFactory->create();
        $count = $blockCollection->count();
        $output->writeln('<comment>Checking ' . $count . ' blocks.</comment>');
        foreach ($blockCollection as $block) {
            /** @var \Magento\Cms\Model\Block $block */
            $content = $block->getContent();
            $newContent = str_ireplace($from, $to, $content);
            if ($newContent != $content) {
                $block->setContent($newContent);
                $this->blockResource->save($block);
                $output->writeln('<info>Success replace in block ID ' . $block->getId() . ': ' . $block->getIdentifier() . '.</info>');
            }
            unset($block);
        }
        unset($blockCollection);

        // CMS Pages
        $pageCollection = $this->pageCollectionFactory->create();
        $count = $pageCollection->count();
        $output->writeln('<comment>Checking ' . $count . ' pages.</comment>');
        foreach ($pageCollection as $page) {
            /** @var \Magento\Cms\Model\Page $page */
            $content = $page->getContent();
            $newContent = str_ireplace($from, $to, $content);
            if ($newContent != $content) {
                $page->setContent($newContent);
                $this->pageResource->save($page);
                $output->writeln('<info>Success replace in page ID ' . $page->getId() . ': ' . $page->getIdentifier() . '.</info>');
            }
            unset($page);
        }
        unset($pageCollection);

        // Product varchar attributes
        $connection = $this->attributeResource->getConnection();
        $table = $this->attributeResource->getTable('catalog_product_entity_varchar');
        $select = $connection->select();
        $select->from($table);
        $attributes = $connection->fetchAll($select);
        $count = count($attributes);
        $output->writeln('<comment>Checking ' . $count . ' product varchar attribute values.</comment>');

        foreach ($attributes as $attribute) {
            $value = $attribute['value'];
            $newValue = str_ireplace($from, $to, $value);
            if ($newValue != $value) {
                $output->writeln('<info>Attribute ID ' . $attribute['value_id'] . ': "' . $value . '"</info>');
                $connection->update($table, ['value' => $newValue], 'value_id = ' . $attribute['value_id']);
            }
        }
        unset($attributes);

        // Product text attributes
        $connection = $this->attributeResource->getConnection();
        $table = $this->attributeResource->getTable('catalog_product_entity_text');
        $select = $connection->select();
        $select->from($table);
        $attributes = $connection->fetchAll($select);
        $count = count($attributes);
        $output->writeln('<comment>Checking ' . $count . ' product text attribute values.</comment>');

        foreach ($attributes as $attribute) {
            $value = $attribute['value'];
            $newValue = str_ireplace($from, $to, $value);
            if ($newValue != $value) {
                $output->writeln('<info>Attribute ID ' . $attribute['value_id'] . ': "' . $value . '"</info>');
                $connection->update($table, ['value' => $newValue], 'value_id = ' . $attribute['value_id']);
            }
        }
        unset($attributes);
        $output->writeln('<info>Done! :)</info>');
    }
}

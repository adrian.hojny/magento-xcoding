# Xcoding_CMS

Moduł zawiera treści dla stron i bloków CMS wraz z ich instalatorami

#### Instalacja nowego bloku / strony CMS

1. dodatnie pliku `.phtml` w `Content/Block` lub `Content/Page` (zalecane jest używanie podfolderów, np. `Content/Block/homepage/slider.phtml`)
2. podniesienie `setup_version` w `etc/module.xml`
3. dodanie `if` porównujący wersje modułu w metodzie `createBlocks` lub `createPages` klasy `UpgradeData` np. 

    ```php
    if (version_compare($context->getVersion(), '1.0.0', '<')) {
        $this->createExamplePage();
    }
    ```
    gdzie `1.0.0` zastępujemy nową wartością `setup_version` modułu
    
4. dodatnie metody instalującej konkretny blok/stronę, np.

    ```php
        /**
        * Create example static block.
        */
        private function createExampleBlock()
        {
            $content = $this->helper->getFileContent(Data::PATH_TO_BLOCKS . 'examplesubdir/example.phtml');
            $this->blockManager->createBlock('example_block_id', 'Example block name', $content);
        }
    
        /**
        * Create example CMS Page
        */
        private function createExamplePage()
        {
            $options = [
                'identifier' => 'example_identifier',
                'title' => 'Example Title',
                'content_heading' => 'Example Content Heading',
                'content' => $this->helper->getFileContent(Data::PATH_TO_PAGES . 'example.phtml'),
                'page_layout' => 'cms-nav'
            ];
    
            $this->pageManager->createPage($options);
        }
    ```

    opcje dostępne w instalatorze stron odpowiadają stałym zdefiniowanym w `\Magento\Cms\Api\Data\PageInterface` 
5. `bin/magento setup:upgrade`

#### Aktualizacja bloku / strony CMS
Aktualizacja bloku analogicznie do instalacji, jeśli `identifier` już istnieje w bazie, Magento zaktualizuje blok.
Aktualizacja stron CMS nie jest obsługiwana, z powodu zależności z `url_rewrites`. Stronę należy usunąć i dodać na nowo.

#### Wymuszenie ponownej instalacji
Jeśli podczas pracy na lokalnym środowisku jest potrzeba przeinstalowania treści i nie chcemy ponownej zmiany wersji modułu (np. zmieniła się treść CMS przed wdrożeniem na inne środowiska),
wystarczy w tabeli `setup_module` w bazie danych zmienić wersję modułu na tę sprzed aktualizacji i wywołać `bin/magento setup:upgrade` 

#### Dodatkowe modyfikatory stron statycznych
Dodano możliwość wyboru dodatkowych modyfikatorów wyglądu (np. ukrywanie breadcrumbs) w zakładce `design` w edycji strony.
Zarządzanie opcjami dostępnymi w multiselect odbywa się w metodzie:

`\Xcoding\Cms\Model\Page\Source\ScandipwaModifiers::toOptionArray`
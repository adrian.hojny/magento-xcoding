<?php
declare(strict_types=1);

namespace Xcoding\Cms\Observer\Cms;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class PagePrepareSave implements ObserverInterface
{
    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(
        Observer $observer
    ) {
        $page = $observer->getEvent()->getPage();
        $page->setScandipwaModifiers(
            implode(',', ($page->getScandipwaModifiers() ? $page->getScandipwaModifiers() : []))
        );
    }
}
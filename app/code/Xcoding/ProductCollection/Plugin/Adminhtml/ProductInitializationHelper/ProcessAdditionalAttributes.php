<?php

declare(strict_types=1);

namespace Xcoding\ProductCollection\Plugin\Adminhtml\ProductInitializationHelper;

use Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\RequestInterface;

/**
 * Handles additional attributes data initialization.
 */
class ProcessAdditionalAttributes
{
    /**
     * @var array
     */
    private static $attributeCodes = ['look', 'product_collection'];

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @param RequestInterface $request
     */
    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * Handles product look attributes data initialization.
     *
     * @param Helper $subject
     * @param Product $result
     * @param Product $product
     * @param array $productData
     * @return Product
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterInitializeFromData(
        Helper $subject,
        Product $result,
        Product $product,
        array $productData
    ): Product {
        foreach (self::$attributeCodes as $attributeCode) {
            if (!isset($productData[$attributeCode])) {
                $result->setCustomAttribute($attributeCode, null);
            }
        }
        return $result;
    }
}

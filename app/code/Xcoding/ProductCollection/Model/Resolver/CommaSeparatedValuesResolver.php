<?php

namespace Xcoding\ProductCollection\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class CommaSeparatedValuesResolver
 * @package Xcoding\ProductCollection\Model\Resolver
 */
class CommaSeparatedValuesResolver implements ResolverInterface
{
    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        return isset($value[$field->getName()]) ? explode(',', $value[$field->getName()]) : [];
    }
}

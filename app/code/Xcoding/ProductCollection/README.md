# Xcoding_ProductCollection

Moduł wiązania produktów w kolekcje i looki na podstawie atrybutów

#### Pobieranie opcji atrybutu nieoznaczone jako używany w filtrach

```graphql
query {
  customAttributeMetadata(
    attributes: [
      {
        attribute_code: "product_collection"
        entity_type: "4"
      }
    ]
  ) {
    items {
      attribute_code
      attribute_type
      attribute_options {
       value
       label
     }
    }
  }
}

```

przykład odpowiedzi:
```json
{
  "data": {
    "products": {
      "items": [
        {
          "sku": "n31176055",
          "look": [
            "5510",
            "5511"
          ]
        },
        {
          "sku": "n31226241",
          "look": [
            "5511"
          ]
        }
      ]
    }
  }
}
```

#### Pobieranie produków z danego looku

```graphql
{
  products(
    filter: {
        look: {
          finset: "5511"
        }
      }
  ) {
    items {
      sku
      look
    }
  }
}
```
gdzie "5510" zastępujemy `value` atrybutu `look`

przykład odpowiedzi:
```json
{
  "data": {
    "products": {
      "items": [
        {
          "sku": "n31176055",
          "look": [
            "5510",
            "5511"
          ]
        },
        {
          "sku": "n31226241",
          "look": [
            "5511"
          ]
        }
      ]
    }
  }
}
```

analogicznie korzystamy z kolekcji (atrybut `product_collection`)

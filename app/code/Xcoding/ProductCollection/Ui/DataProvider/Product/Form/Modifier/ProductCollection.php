<?php

namespace Xcoding\ProductCollection\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form\Field;

class ProductCollection extends AbstractModifier
{
    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * ProductCollection constructor.
     * @param LocatorInterface $locator
     */
    public function __construct(
        LocatorInterface $locator
    ) {
        $this->locator = $locator;
    }

    /**
     * @param array $data
     * @return array
     * @since 100.1.0
     */
    public function modifyData(array $data)
    {
        return array_replace_recursive(
            $data,
            [
                $this->locator->getProduct()->getId() => [
                    self::DATA_SOURCE_DEFAULT => [
                        'product_collection' => explode(
                            ',',
                            $this->locator->getProduct()->getCustomAttribute('product_collection') ?
                                $this->locator->getProduct()->getCustomAttribute('product_collection')->getValue() :
                                ''
                        )
                    ]
                ]
            ]
        );
    }

    /**
     * @param array $meta
     * @return array
     * @since 100.1.0
     */
    public function modifyMeta(array $meta)
    {
        if ($name = $this->getGeneralPanelName($meta)) {
            $config = $meta[$name]['children']['container_product_collection']['children']['product_collection']['arguments']['data']['config'] ?? null;

            if ($config !== null) {
                $opts = [];
                if (isset($config['options'])) {
                    foreach ($config['options'] as $option) {
                        if (isset($option['value']) && $option['value']) {
                            $opts[] = $option;
                        }
                    }
                }

                $config = [
                    'label' => __('Product collection'),
                    'component' => 'Magento_Ui/js/form/element/ui-select',
                    'disableLabel' => true,
                    'filterOptions' => true,
                    'elementTmpl' => 'ui/grid/filters/elements/ui-select',
                    'formElement' => 'select',
                    'componentType' => Field::NAME,
                    'options' => $opts,
                    'visible' => true,
                    'required' => false,
                    'source' => $name,
                    'dataScope' => 'product_collection',
                    'multiple' => true,
                    'isDisplayMissingValuePlaceholder' => true
                ];

                $meta[$name]['children']['container_product_collection']['children']['product_collection']['arguments']['data']['config'] = $config;
            }
        }

        return $meta;
    }
}

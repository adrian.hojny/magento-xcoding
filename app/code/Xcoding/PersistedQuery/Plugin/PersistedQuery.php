<?php
namespace Xcoding\PersistedQuery\Plugin;

class PersistedQuery extends \ScandiPWA\PersistedQuery\Plugin\PersistedQuery
{
    /** @var array Unsupported chars string map */
    private const UNSUPPORTED_MAP = [
        'escaped' => [
            '\,',
            '\:',
        ],
        'tmp' => [
            '%2C',
            '%3A',
        ],
        'unescaped' => [
            ',',
            ':',
        ],
    ];

    /**
     * @param $args
     * @return array
     * @throws \InvalidArgumentException
     */
    protected function processVariables($args): array
    {
        unset($args['hash']);

        return array_map(function ($item) {
            // Check for complex JSON structures
            if (preg_match('/^.*:?[{|}].*$/', $item)) {
                return $this->processNested($item);
            }

            // Check for encoded array
            if (preg_match('/,/', $item)) {
                $item = explode(',', $item);

                return $item;
            }

            // String to bool if bool
            if ($item === 'true' || $item === 'false') {
                return filter_var($item, FILTER_VALIDATE_BOOLEAN);
            }

            // String to int if number
            if (is_int($item)) {
                return (int)$item;
            }

            // String to float if number with decimals
            if (is_float($item)) {
                return (float)$item;
            }

            return $item;
        }, $args);
    }

    /**
     * @param string $item Nested arguments
     * 
     * @return array
     */
    private function processNested($item)
    {
        $item = str_replace(self::UNSUPPORTED_MAP['escaped'], self::UNSUPPORTED_MAP['tmp'], $item);
        $rawKeys = str_replace(['{', '}', '[', ']'], '', $item);
        $unifiedString = str_replace(":", ',', $rawKeys);
        $valueList = explode(',', $unifiedString);
        foreach ($valueList as $value) {
            if (strpos($value, '"') !== false || !$value) {
                continue;
            }
            $item = preg_replace("|(?<![\"\w])" . preg_quote($value) . "(?![\"\w])|", "\"$value\"", $item);
        }

        $item = $this->serializer->unserialize($item);
        array_walk_recursive($item, function(&$string) {
            $string = str_replace(self::UNSUPPORTED_MAP['tmp'], self::UNSUPPORTED_MAP['unescaped'], $string);
        });
        return $item;
    }
}

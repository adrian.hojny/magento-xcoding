<?php

namespace Xcoding\PaymentMethodDependency\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Xcoding\PaymentMethodDependency\Block\Adminhtml\Form\Field\PaymentMethods as PaymentMethodsRenderer;
use Xcoding\PaymentMethodDependency\Block\Adminhtml\Form\Field\ShippingMethods as ShippingMethodsRenderer;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;

class Dependency extends AbstractFieldArray
{
    protected $paymentMethodsRenderer;
    protected $shippingMethodsRenderer;

    /**
     * {@inheritdoc}
     */
    protected function _prepareToRender()
    {
        $this->addColumn('shipping_method', [
            'label' => __('Shipping method'),
            'renderer' => $this->getShippingMethodsRenderer(),
            'class' => 'required-entry'
        ]);

        $this->addColumn('payment_method', [
            'label' => __('Disabled payment method'),
            'renderer' => $this->getPaymentMethodsRenderer(),
            'class' => 'required-entry'
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add mapping');
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @throws LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row): void
    {
        $options = [];

        $shippingMethod = $row->getShippingMethod();
        if ($shippingMethod !== null) {
            $options['option_' . $this->getShippingMethodsRenderer()->calcOptionHash($shippingMethod)] = 'selected="selected"';
        }

        $paymentMethod = $row->getPaymentMethod();
        if ($paymentMethod !== null) {
            $options['option_' . $this->getPaymentMethodsRenderer()->calcOptionHash($paymentMethod)] = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }

    protected function getPaymentMethodsRenderer()
    {
        if (!$this->paymentMethodsRenderer) {
            $this->paymentMethodsRenderer = $this->getLayout()->createBlock(
                PaymentMethodsRenderer::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->paymentMethodsRenderer;
    }

    protected function getShippingMethodsRenderer()
    {
        if (!$this->shippingMethodsRenderer) {
            $this->shippingMethodsRenderer = $this->getLayout()->createBlock(
                ShippingMethodsRenderer::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }
        return $this->shippingMethodsRenderer;
    }
}
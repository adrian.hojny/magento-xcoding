<?php

namespace Xcoding\PaymentMethodDependency\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;
use Magento\Payment\Helper\Data as PaymentHelper;

class PaymentMethods extends Select
{
    /**
     * @var PaymentHelper
     */
    protected $paymentHelper;

    public function __construct(
        Context $context,
        PaymentHelper $paymentHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->paymentHelper = $paymentHelper;
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

    public function _toHtml(): string
    {
        $availableMethods = [];
        $allMethods = $this->paymentHelper->getPaymentMethods();
        foreach ($allMethods as $key => $method) {
            $availableMethods[] = [
              'value' => $key,
              'label' => $method['title'] ?? $key
            ];
        }
        if (!$this->getOptions()) {
            $this->setOptions($availableMethods);
        }

        return parent::_toHtml();
    }
}
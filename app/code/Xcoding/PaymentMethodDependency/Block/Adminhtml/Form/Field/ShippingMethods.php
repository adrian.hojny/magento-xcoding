<?php

namespace Xcoding\PaymentMethodDependency\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;
use Magento\Shipping\Model\Config\Source\Allmethods;

class ShippingMethods extends Select
{
    /**
     * @var Allmethods
     */
    protected $allShippingMethods;

    /**
     * ShippingMethods constructor.
     * @param Context $context
     * @param Allmethods $allShippingMethods
     * @param array $data
     */
    public function __construct(
        Context $context,
        Allmethods $allShippingMethods,
        array $data = []
    ) {
        $this->allShippingMethods = $allShippingMethods;
        parent::__construct($context, $data);
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

    public function _toHtml(): string
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getMethods());
        }

        return parent::_toHtml();
    }

    protected function getMethods()
    {
        $methods = $this->allShippingMethods->toOptionArray(true);
        foreach ($methods as $code => &$method) {
            if ($code === 'pickup') {
                foreach ($method['value'] as &$rate) {
                    if ($rate['value'] === 'pickup_pickup') {
                        $rate['value'] = 'pickup_instorepickup';
                    }
                }
            }
        }

        return $methods;
    }
}
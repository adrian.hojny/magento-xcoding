<?php declare(strict_types=1);

namespace Xcoding\PaymentMethodDependency\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const CONFIG_PATH_SHIPPING_PAYMENT_DEPENDENCY_MAPPING = 'shipping/payment_dependency/mapping';

    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;

    /**
     * Data constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param JsonSerializer $jsonSerializer
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        JsonSerializer $jsonSerializer
    )
    {
        $this->jsonSerializer = $jsonSerializer;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @param string $paymentMethodCode
     * @param string $shippingMethodCode
     * @return bool
     */
    public function isPaymentDisabled(string $paymentMethodCode, string $shippingMethodCode)
    {
        $mapping = $this->scopeConfig->getValue(
            self::CONFIG_PATH_SHIPPING_PAYMENT_DEPENDENCY_MAPPING,
            ScopeInterface::SCOPE_STORE
        );
        try {
            $mapping = $this->jsonSerializer->unserialize($mapping);
        } catch (\Exception $e) {
            return false;
        }
        if (!$mapping) {
            return false;
        }
        foreach ($mapping as $row) {
            $rowShipping = $row['shipping_method'] ?? false;
            $rowDisabledPayment = $row['payment_method'] ?? false;
            if ($rowShipping && $rowShipping == $shippingMethodCode && $rowDisabledPayment == $paymentMethodCode) {
                return true;
            }
        }
        return false;
    }
}

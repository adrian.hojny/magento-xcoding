<?php
declare(strict_types=1);

namespace Xcoding\PaymentMethodDependency\Observer\Payment;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Api\Data\CartInterface;
use Xcoding\PaymentMethodDependency\Helper\Data as ModuleHelper;

class MethodIsActive implements ObserverInterface
{
    /**
     * @var ModuleHelper
     */
    protected $moduleHelper;

    /**
     * MethodIsActive constructor.
     * @param ModuleHelper $moduleHelper
     */
    public function __construct(
        ModuleHelper $moduleHelper
    ) {
        $this->moduleHelper = $moduleHelper;
    }

    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(
        Observer $observer
    ) {
        $paymentMethod = $observer->getEvent()->getMethodInstance()->getCode();
        $result = $observer->getEvent()->getResult();

        /** @var CartInterface $quote */
        $quote = $observer->getEvent()->getQuote();
        $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
        if ($this->isPaymentDisabled($paymentMethod, $shippingMethod)) {
            $result->setData('is_available', false);
        }
    }

    protected function isPaymentDisabled($paymentMethod, $shippingMethod)
    {
        return $this->moduleHelper->isPaymentDisabled($paymentMethod, $shippingMethod);
    }
}


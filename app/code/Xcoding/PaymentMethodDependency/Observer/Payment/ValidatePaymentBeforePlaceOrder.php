<?php

namespace Xcoding\PaymentMethodDependency\Observer\Payment;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Xcoding\PaymentMethodDependency\Helper\Data as ModuleHelper;

class ValidatePaymentBeforePlaceOrder implements ObserverInterface
{
    /**
     * @var ModuleHelper
     */
    protected $moduleHelper;

    /**
     * @param ModuleHelper $moduleHelper
     */
    public function __construct(
        ModuleHelper $moduleHelper
    ) {
        $this->moduleHelper = $moduleHelper;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getOrder();
        $paymentMethod = $order->getPayment()->getMethod();
        $shippingMethod = $order->getShippingMethod();

        if ($this->moduleHelper->isPaymentDisabled($paymentMethod, $shippingMethod)) {
            throw new LocalizedException(__("Payment method is not available"));
        }
    }
}
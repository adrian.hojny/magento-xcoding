<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface ProductSizingInterface extends ExtensibleDataInterface
{
    const PRODUCTSIZING_ID = 'productsizing_id';
    const CODE = 'code';
    const TITLE = 'title';
    const CONTENT = 'content';

    /**
     * Get productsizing_id
     * @return string|null
     */
    public function getProductsizingId();

    /**
     * Set productsizing_id
     * @param string $productsizingId
     * @return ProductSizingInterface
     */
    public function setProductsizingId($productsizingId);

    /**
     * Get content
     * @return string|null
     */
    public function getContent();

    /**
     * Set content
     * @param string $content
     * @return ProductSizingInterface
     */
    public function setContent($content);


    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return ProductSizingInterface
     */
    public function setTitle($title);


    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return ProductSizingInterface
     */
    public function setCode($code);
}


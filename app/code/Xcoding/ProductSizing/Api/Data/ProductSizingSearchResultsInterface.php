<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface ProductSizingSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get ProductSizing list.
     * @return ProductSizingInterface[]
     */
    public function getItems();

    /**
     * Set content list.
     * @param ProductSizingInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}


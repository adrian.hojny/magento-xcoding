<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Xcoding\ProductSizing\Api\Data\ProductSizingInterface;
use Xcoding\ProductSizing\Api\Data\ProductSizingSearchResultsInterface;

interface ProductSizingRepositoryInterface
{

    /**
     * Save ProductSizing
     * @param ProductSizingInterface $productSizing
     * @return ProductSizingInterface
     * @throws LocalizedException
     */
    public function save(
        ProductSizingInterface $productSizing
    );

    /**
     * Retrieve ProductSizing
     * @param string $productsizingId
     * @return ProductSizingInterface
     * @throws LocalizedException
     */
    public function get($productsizingId);

    /**
     * Retrieve ProductSizing by code
     * @param string $productsizingCode
     * @return ProductSizingInterface
     * @throws LocalizedException
     */
    public function getByCode($productsizingCode);

    /**
     * Retrieve ProductSizing matching the specified criteria.
     * @param SearchCriteriaInterface $searchCriteria
     * @return ProductSizingSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete ProductSizing
     * @param ProductSizingInterface $productSizing
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        ProductSizingInterface $productSizing
    );

    /**
     * Delete ProductSizing by ID
     * @param string $productsizingId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($productsizingId);
}


# Xcoding_ProductSizing

### Zarządzanie rozmiarówkami

W panelu admina: `content > Product sizing` 

Rozmiarówka `[empty]` jest dodawana automatycznie i nie może zostać usunięta. 

### Przypisywanie rozmiarówki do produktu

Rozmiarówka może być przypisana do produktu oraz marki.  
W atrybucie produktu `Sizing` możemy wybrać jedną ze stworzonych przez nas rozmiarówek, opcję `Use brand setting` albo `Force empty`, która wymuni puste pole dla danego produktu, 
niezależnie od rozmiarówki marki.  

Przypisywanie rozmiarówki dla marki:

W panelu admina `amasty > Improved layered navigation: Brands > Brand Management > [edit] > Product sizing`.

### Tłumaczenia rozmiarówek

Tłumaczenie odbywa się przez stworzenie osobnej rozmiarówki z przetłumaczoną zawartością (np. z kodem `nike_sizing_de`, `nike_sizing_en`, `nike_sizing_pl`).

Następnie produktu/marki wybieramy widok sklepu (np. `MAIN_SHOP_DE`) i przypisujemy rozmiarówkę z sufiksem `_de` w kodzie.

Rozmiarówka `[empty]` nie potrzebuje tłumaczenia, ponieważ nie jest w ogóle zwracana na frontend.  
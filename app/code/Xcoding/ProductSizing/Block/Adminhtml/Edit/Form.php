<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Block\Adminhtml\Edit;

class Form extends \Magento\Framework\View\Element\Template
{    
    public const COLUMN_QTY = 5;

    /** @var Xcoding\ProductSizing\Model\ProductSizing */
    protected $model;
    
    /**
     * @param  \Xcoding\ProductSizing\Model\ProductSizing $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }
    
    /**
     * @return \Xcoding\ProductSizing\Model\ProductSizing
     */
    public function getModel()
    {
        return $this->model;
    }
    
    /**
     * @return string
     */
    public function getConfigJson()
    {
        return $this->getModel()->getContentJson();
    }
}


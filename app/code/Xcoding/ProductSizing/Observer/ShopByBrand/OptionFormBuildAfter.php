<?php

namespace Xcoding\ProductSizing\Observer\ShopByBrand;

use Magento\Framework\Data\Form;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Ui\Component\Form\Field;
use Xcoding\ProductSizing\Model\Product\Attribute\Source\Sizing as SizingAttribute;

class OptionFormBuildAfter implements ObserverInterface
{
    /**
     * @var SizingAttribute
     */
    private $sizingAttribute;

    public function __construct(
        SizingAttribute $sizingAttribute
    ) {
        $this->sizingAttribute = $sizingAttribute;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Form $form */
        $form = $observer->getData('form');

        $fieldset = $form->addFieldset('additional_info',
            ['legend' => __('Additional information'), 'class' => 'form-inline']);

        $fieldset->addField(
            'sizing',
            'select',
            [
                'name' => 'sizing',
                'label' => __('Product sizing'),
                'values' => $this->sizingAttribute->getAllOptions(false),
                'required' => false
            ]
        );
    }
}

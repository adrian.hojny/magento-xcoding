<?php


namespace Xcoding\ProductSizing\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Xcoding\ProductSizing\Model\ProductSizing;

class AddEmptySizing implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $data[] = ['code' => ProductSizing::EMPTY_SIZING_CODE, 'title' => 'Force empty', 'content' => ''];

        $this->moduleDataSetup->getConnection()->insertArray(
            $this->moduleDataSetup->getTable('xcoding_productsizing_productsizing'),
            ['code', 'title', 'content'],
            $data
        );
        $this->moduleDataSetup->endSetup();
    }

    public function getAliases()
    {
        return [];
    }
}
<?php

namespace Xcoding\ProductSizing\Setup\Patch\Schema;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class AddSizingColumnToAmastyBrand implements SchemaPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $sizingColumnExists = $this->moduleDataSetup
            ->getConnection()
            ->tableColumnExists('amasty_amshopby_option_setting', 'sizing');

        if (!$sizingColumnExists) {
            $this->moduleDataSetup->getConnection()->addColumn(
                $this->moduleDataSetup->getTable('amasty_amshopby_option_setting'),
                'sizing',
                [
                    'type' => Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'Product sizing',
                ]
            );
        }

        $this->moduleDataSetup->endSetup();
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }
}
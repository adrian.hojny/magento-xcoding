<?php

namespace Xcoding\ProductSizing\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form\Field;

class ProductSizing extends AbstractModifier
{
    /**
     * @var LocatorInterface
     */
    private $locator;

    /**
     * ProductCollection constructor.
     * @param LocatorInterface $locator
     */
    public function __construct(
        LocatorInterface $locator
    ) {
        $this->locator = $locator;
    }

    /**
     * @param array $data
     * @return array
     * @since 100.1.0
     */
    public function modifyData(array $data)
    {
        return array_replace_recursive(
            $data,
            [
                $this->locator->getProduct()->getId() => [
                    self::DATA_SOURCE_DEFAULT => [
                        'sizing' =>
                            $this->locator->getProduct()->getCustomAttribute('sizing') ?
                                $this->locator->getProduct()->getCustomAttribute('sizing')->getValue() :
                                ''
                    ]
                ]
            ]
        );
    }

    /**
     * @param array $meta
     * @return array
     * @since 100.1.0
     */
    public function modifyMeta(array $meta)
    {
        if ($name = $this->getGeneralPanelName($meta)) {
            $config = $meta[$name]['children']['container_sizing']['children']['sizing']['arguments']['data']['config'] ?? null;

            if ($config !== null) {
                $opts = $config['options'];
                $config = [
                    'label' => __('Sizing'),
                    'component' => 'Magento_Ui/js/form/element/ui-select',
                    'disableLabel' => true,
                    'filterOptions' => true,
                    'elementTmpl' => 'ui/grid/filters/elements/ui-select',
                    'formElement' => 'select',
                    'componentType' => Field::NAME,
                    'options' => $opts,
                    'visible' => true,
                    'required' => false,
                    'source' => $name,
                    'dataScope' => 'sizing',
                    'multiple' => false,
                    'isDisplayMissingValuePlaceholder' => true,
                    'chipsEnabled' => true,
                    'missingValuePlaceholder' => __('Sizing with ID: %s doesn\'t exist'),
                    'isDisplayEmptyPlaceholder' => true,
                    'caption' => __('Use brand settings'),
                ];

                $meta[$name]['children']['container_sizing']['children']['sizing']['arguments']['data']['config'] = $config;
            }
        }

        return $meta;
    }
}

<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Model\Data;

use Magento\Framework\Api\AbstractExtensibleObject;
use Xcoding\ProductSizing\Api\Data\ProductSizingExtensionInterface;
use Xcoding\ProductSizing\Api\Data\ProductSizingInterface;

class ProductSizing extends AbstractExtensibleObject implements ProductSizingInterface
{
    /**
     * Get productsizing_id
     * @return string|null
     */
    public function getProductsizingId()
    {
        return $this->_get(self::PRODUCTSIZING_ID);
    }

    /**
     * Set productsizing_id
     * @param string $productsizingId
     * @return ProductSizingInterface
     */
    public function setProductsizingId($productsizingId)
    {
        return $this->setData(self::PRODUCTSIZING_ID, $productsizingId);
    }

    /**
     * Get content
     * @return string|null
     */
    public function getContent()
    {
        return $this->_get(self::CONTENT);
    }

    /**
     * Set content
     * @param string $content
     * @return ProductSizingInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Get code
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code
     * @param string $code
     * @return ProductSizingInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }


    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return ProductSizingInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return ProductSizingExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param ProductSizingExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        ProductSizingExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}


<?php
namespace Xcoding\ProductSizing\Model\Api;

use Xcoding\ProductSizing\Model\ProductSizingFactory;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing as ProductSizingResourceModel;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing\CollectionFactory as ProductSizingCollectionFactory;

class Productsizing
{
    /**
     * @var ProductSizingResourceModel
     */
    public $resource;

    /**
     * @var ProductSizingFactory
     */
    public $factory;

    /**
     * @var ProductSizingCollectionFactory
     */
    public $collectionFactory;
 
    public function __construct(
        ProductSizingCollectionFactory $collectionFactory,
        ProductSizingResourceModel $resource,
        ProductSizingFactory $factory
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->resource = $resource;
        $this->factory = $factory;
    }

    /**
     * GET for ProductSizing API
     * @param int $id
     * @return \Xcoding\ProductSizing\Api\Data\ProductSizingInterface[]
     */
    public function getAll()
    {
        $collection = $this->collectionFactory->create();
        $result = [];
        foreach ($collection as $model) {
            $result[] = $model->getDataModel();
        }
        return $result;
    }

    /**
     * GET for ProductSizing API
     * @param int $id
     * @return \Xcoding\ProductSizing\Api\Data\ProductSizingInterface
     */
    public function getSingle($id)
    {
        $model = $this->factory->create();
        $this->resource->load($model, $id);
        return $model->getDataModel();
    }
}

<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Xcoding\ProductSizing\Api\Data\ProductSizingInterface;
use Xcoding\ProductSizing\Api\Data\ProductSizingInterfaceFactory;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing\Collection;

class ProductSizing extends AbstractModel
{
    public const EMPTY_SIZING_CODE = 'empty';

    public const HEADER_SIZE_ID = 1;
    public const HEADER_US_ID = 2;
    public const HEADER_UK_ID = 3;
    public const HEADER_EU_ID = 4;
    public const HEADER_CM_ID = 5;
    public const HEADER_HIPS_ID = 6;
    public const HEADER_WAIST_ID = 7;
    public const HEADER_LEG_LENGTH_ID = 8;
    public const HEADER_HEIGHT_ID = 9;
    public const HEADER_CHEST_ID = 10;
    public const HEADER_SLEEVE_LENGTH_ID = 11;

    public const HEADERS = [
        self::HEADER_SIZE_ID => 'Size',
        self::HEADER_US_ID => 'US',
        self::HEADER_UK_ID => 'UK',
        self::HEADER_EU_ID => 'EU',
        self::HEADER_CM_ID => 'CM',
        self::HEADER_HIPS_ID => 'Hips',
        self::HEADER_WAIST_ID => 'Waist',
        self::HEADER_LEG_LENGTH_ID => 'Leg Length',
        self::HEADER_HEIGHT_ID => 'Height',
        self::HEADER_CHEST_ID => 'Chest',
        self::HEADER_SLEEVE_LENGTH_ID => 'Sleeve Length',
    ];

    protected $productsizingDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'xcoding_productsizing_productsizing';

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ProductSizingInterfaceFactory $productsizingDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param ResourceModel\ProductSizing $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ProductSizingInterfaceFactory $productsizingDataFactory,
        DataObjectHelper $dataObjectHelper,
        ResourceModel\ProductSizing $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        $this->productsizingDataFactory = $productsizingDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve productsizing model with productsizing data
     * @return ProductSizingInterface
     */
    public function getDataModel()
    {
        $productsizingData = $this->getData();

        $productsizingDataObject = $this->productsizingDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $productsizingDataObject,
            $productsizingData,
            ProductSizingInterface::class
        );

        return $productsizingDataObject;
    }
    
    /**
     * @return string
     */
    public function getTranslatedContentJson()
    {
        $content = $this->getContentJson();
        if (!$content) {
            return '';
        }
        try {
            $decoded = json_decode($content, true);
            $headers = $decoded['headers'] ?? null;
        } catch (\InvalidArgumentException $e) {
            return $content;
        }
        if (!$headers) {
            return $content;
        }
        foreach ($headers as &$value) {
            $value = __(self::HEADERS[$value] ?? '');
        }
        $decoded['headers'] = $headers;
        return json_encode($decoded);
    }
}

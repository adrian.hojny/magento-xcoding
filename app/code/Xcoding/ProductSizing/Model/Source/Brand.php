<?php

namespace Xcoding\ProductSizing\Model\Source;

use Amasty\ShopbyBase\Model\ResourceModel\OptionSetting\CollectionFactory as OptionSettingCollectionFactory;

class Brand implements \Magento\Framework\Data\OptionSourceInterface
{
    /** @var OptionSettingCollectionFactory */
    private $optionSettingCollectionFactory;

    public function __construct(
        OptionSettingCollectionFactory $optionSettingCollectionFactory
    ) {
        $this->optionSettingCollectionFactory = $optionSettingCollectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $collection = $this->optionSettingCollectionFactory->create()
            ->addOrder('url_alias', 'ASC');
        $collection->getSelect()->joinLeft(
            array("s" => 'store'),
            "main_table.store_id = s.store_id",
            array("store_code" => "s.code")
        );
        $result = [];
        $result[] = ['value' => 0, 'label' => '-'];
        foreach ($collection as $model) {
            $result[] = [
                'value' => $model->getId(),
                'label' => $model->getUrlAlias() . ' (' . $model->getStoreCode() . ')'
            ];
        }
        return $result;
    }
}

<?php

namespace Xcoding\ProductSizing\Model\Source;

class Type implements \Magento\Framework\Data\OptionSourceInterface
{
    public const TYPE_MEN = 1;
    public const TYPE_WOMEN = 2;
    public const TYPE_CHILDREN = 3;
    public const TYPE_UNISEX = 4;

    public const TYPES = [
        self::TYPE_MEN => 'Men',
        self::TYPE_WOMEN => 'Women',
        self::TYPE_CHILDREN => 'Children',
        self::TYPE_UNISEX => 'Unisex',
    ];

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
        foreach (self::TYPES as $key => $label) {
            $result[] = ['value' => $key, 'label' => __($label)];
        }
        return $result;
    }
}

<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Model\ProductSizing;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing\CollectionFactory;

class DataProvider extends AbstractDataProvider
{

    protected $collection;

    protected $dataPersistor;

    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            /** @var \Xcoding\ProductSizing\Model\ProductSizing $model */
            $data = $model->getData();
            $data['block_image'] = $this->prepareImageForImageUploader($data['block_image']);
            $data['block_mobile_image'] = $this->prepareImageForImageUploader($data['block_mobile_image']);
            $this->loadedData[$model->getId()] = $data;
        }
        $data = $this->dataPersistor->get('xcoding_productsizing_productsizing');

        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('xcoding_productsizing_productsizing');
        }

        return $this->loadedData;
    }
    
    /**
     * @param  string $image
     * @return array
     */
    private function prepareImageForImageUploader($imgPath) {
        if (!$imgPath) {
            return [];
        }
        /** @var \Magento\Catalog\Model\Category\FileInfo $fileInfo */
        $fileInfo = \Magento\Framework\App\ObjectManager::getInstance()->get(
            \Magento\Catalog\Model\Category\FileInfo::class
        );
        if (!$fileInfo->isExist($imgPath)) {
            return [];
        }
        $stat = $fileInfo->getStat($imgPath);
        $mime = $fileInfo->getMimeType($imgPath);

        $imageData = [];
        $imageData[0]['name'] = basename($imgPath);
        $imageData[0]['url'] = $imgPath;
        $imageData[0]['size'] = isset($stat) ? $stat['size'] : 0;
        $imageData[0]['type'] = $mime;
        return $imageData;
    }
}


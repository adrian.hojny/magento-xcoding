<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Model\ResourceModel\ProductSizing;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing;

class Collection extends AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'productsizing_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Xcoding\ProductSizing\Model\ProductSizing::class,
            ProductSizing::class
        );
    }
}


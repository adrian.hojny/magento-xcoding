<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class ProductSizing extends AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('xcoding_productsizing_productsizing', 'productsizing_id');
    }
}


<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Model;

use Exception;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Xcoding\ProductSizing\Api\Data\ProductSizingInterface;
use Xcoding\ProductSizing\Api\Data\ProductSizingInterfaceFactory;
use Xcoding\ProductSizing\Api\Data\ProductSizingSearchResultsInterfaceFactory;
use Xcoding\ProductSizing\Api\ProductSizingRepositoryInterface;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing as ResourceProductSizing;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing\CollectionFactory as ProductSizingCollectionFactory;

class ProductSizingRepository implements ProductSizingRepositoryInterface
{

    protected $resource;

    protected $productSizingFactory;

    protected $productSizingCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataProductSizingFactory;

    protected $extensionAttributesJoinProcessor;
    protected $extensibleDataObjectConverter;
    private $storeManager;
    private $collectionProcessor;

    /**
     * @param ResourceProductSizing $resource
     * @param ProductSizingFactory $productSizingFactory
     * @param ProductSizingInterfaceFactory $dataProductSizingFactory
     * @param ProductSizingCollectionFactory $productSizingCollectionFactory
     * @param ProductSizingSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceProductSizing $resource,
        ProductSizingFactory $productSizingFactory,
        ProductSizingInterfaceFactory $dataProductSizingFactory,
        ProductSizingCollectionFactory $productSizingCollectionFactory,
        ProductSizingSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->productSizingFactory = $productSizingFactory;
        $this->productSizingCollectionFactory = $productSizingCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataProductSizingFactory = $dataProductSizingFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        ProductSizingInterface $productSizing
    ) {
        /* if (empty($productSizing->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $productSizing->setStoreId($storeId);
        } */

        $productSizingData = $this->extensibleDataObjectConverter->toNestedArray(
            $productSizing,
            [],
            ProductSizingInterface::class
        );

        $productSizingModel = $this->productSizingFactory->create()->setData($productSizingData);

        try {
            $this->resource->save($productSizingModel);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the productSizing: %1',
                $exception->getMessage()
            ));
        }
        return $productSizingModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->productSizingCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            ProductSizingInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($productSizingId)
    {
        return $this->delete($this->get($productSizingId));
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        ProductSizingInterface $productSizing
    ) {
        try {
            $productSizingModel = $this->productSizingFactory->create();
            $this->resource->load($productSizingModel, $productSizing->getProductsizingId());
            $this->resource->delete($productSizingModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the ProductSizing: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function get($productSizingId)
    {
        $productSizing = $this->productSizingFactory->create();
        $this->resource->load($productSizing, $productSizingId);
        if (!$productSizing->getId()) {
            throw new NoSuchEntityException(__('ProductSizing with id "%1" does not exist.', $productSizingId));
        }
        return $productSizing->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getByCode($productSizingCode)
    {
        $productSizing = $this->productSizingFactory->create();
        $this->resource->load($productSizing, $productSizingCode, 'code');
        if (!$productSizing->getId()) {
            throw new NoSuchEntityException(__('ProductSizing with code "%1" does not exist.', $productSizingCode));
        }
        return $productSizing->getDataModel();
    }
}


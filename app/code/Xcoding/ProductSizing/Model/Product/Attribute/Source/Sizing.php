<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Model\Product\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing\Collection as ProductSizingCollection;
use Xcoding\ProductSizing\Model\ResourceModel\ProductSizing\CollectionFactory as ProductSizingCollectionFactory;

class Sizing extends AbstractSource
{
    /**
     * @var ProductSizingCollectionFactory
     */
    protected $sizingCollectionFactory;

    public function __construct(
        ProductSizingCollectionFactory $sizingCollectionFactory
    ) {
        $this->sizingCollectionFactory = $sizingCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllOptions($addBrandLabel = true)
    {
        if (!$this->_options) {
            $collection = $this->sizingCollectionFactory->create();
            foreach ($collection as $item) {
                $this->_options[] = [
                    'value' => $item->getId(),
                    'label' => sprintf('[%s] %s', $item->getCode(), $item->getTitle())
                ];
            }
            $label = $addBrandLabel ?  __('Use brand settings') : __('None');
            array_unshift($this->_options, ['value' => 0, 'label' => $label]);
        }
        return $this->_options;
    }
}


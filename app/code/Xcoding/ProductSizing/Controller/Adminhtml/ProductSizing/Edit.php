<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Controller\Adminhtml\ProductSizing;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Xcoding\ProductSizing\Model\ProductSizing;

class Edit extends \Xcoding\ProductSizing\Controller\Adminhtml\ProductSizing
{

    protected $resultPageFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('productsizing_id');
        $model = $this->_objectManager->create(ProductSizing::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Product sizing no longer exists.'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
            if ($model->getCode() === ProductSizing::EMPTY_SIZING_CODE) {
                $this->messageManager->addErrorMessage(__('Sizing with code "empty" cannot be edited'));
                /** @var Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('xcoding_productsizing_productsizing', $model);

        // 3. Build edit form
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        /** @var \Xcoding\ProductSizing\Block\Adminhtml\Edit\Form $editFormBlock */
        $editFormBlock = $resultPage->getLayout()->getBlock('productsizing_edit_form');
        $editFormBlock->setModel($model);
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit product sizing') : __('New product sizing'),
            $id ? __('Edit product sizing') : __('New product sizing')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Product sizings'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Product sizing %1',
            $model->getId()) : __('New Product sizing'));
        return $resultPage;
    }
}


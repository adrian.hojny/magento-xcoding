<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Controller\Adminhtml\ProductSizing;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Xcoding\ProductSizing\Model\ProductSizing;

class InlineEdit extends Action
{

    protected $jsonFactory;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Inline edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $modelid) {
                    /** @var ProductSizing $model */
                    $model = $this->_objectManager->create(ProductSizing::class)->load($modelid);
                    if ($model->getCode() === ProductSizing::EMPTY_SIZING_CODE) {
                        $messages[] = __('Sizing with code "empty" cannot be edited');
                        $error = true;
                    } else {
                        try {
                            $model->setData(array_merge($model->getData(), $postItems[$modelid]));
                            $model->save();
                        } catch (Exception $e) {
                            $messages[] = "[Productsizing ID: {$modelid}]  {$e->getMessage()}";
                            $error = true;
                        }
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}


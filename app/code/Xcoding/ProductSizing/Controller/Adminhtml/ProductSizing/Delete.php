<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Controller\Adminhtml\ProductSizing;

use Exception;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Xcoding\ProductSizing\Model\ProductSizing;

class Delete extends \Xcoding\ProductSizing\Controller\Adminhtml\ProductSizing
{

    /**
     * Delete action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('productsizing_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(ProductSizing::class);
                $model->load($id);
                if ($model->getCode() === ProductSizing::EMPTY_SIZING_CODE) {
                    throw new Exception(__('Sizing with code "empty" cannot be deleted'));
                }
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Productsizing.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['productsizing_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Productsizing to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}


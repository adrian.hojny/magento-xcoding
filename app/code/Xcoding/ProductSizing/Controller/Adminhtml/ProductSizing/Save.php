<?php
declare(strict_types=1);

namespace Xcoding\ProductSizing\Controller\Adminhtml\ProductSizing;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Xcoding\ProductSizing\Model\ProductSizing;
use Magento\Store\Model\StoreManagerInterface;

class Save extends Action
{
    /** @var ImageUploader */
    private $imageUploader;

    /** @var StoreManagerInterface */
    private $storeManager;

    /** @var DataPersistorInterface */
    protected $dataPersistor;

    /** @var Filesystem */
    protected $filesystem;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface $storeManager
     * @param ImageUploader $imageUploader
     * @param Filesystem $filesystem
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        ImageUploader $imageUploader,
        Filesystem $filesystem
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
        $this->imageUploader = $imageUploader;
        $this->filesystem = $filesystem;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\App\Request\Http $request */
        $request = $this->getRequest();
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $request->getPostValue();
        if ($data) {
            $id = $request->getParam('productsizing_id');

            /** @var ProductSizing $model */
            $model = $this->_objectManager->create(ProductSizing::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Productsizing no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            try {
                $this->processImageUpload($model, $data, 'block_image');
                $this->processImageUpload($model, $data, 'block_mobile_image');
                $model->setData($data);
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the product sizing.'));
                $this->dataPersistor->clear('xcoding_productsizing_productsizing');

                if ($request->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['productsizing_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (Exception $e) {
                $this->messageManager->addExceptionMessage($e,
                    __('Something went wrong while saving the Productsizing.'));
            }

            $this->dataPersistor->set('xcoding_productsizing_productsizing', $data);
            return $resultRedirect->setPath('*/*/edit',
                ['productsizing_id' => $request->getParam('productsizing_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
    
    /**
     * @param  ProductSizing $model
     * @param  array $data
     * @param  string $key
     * @return void
     */
    private function processImageUpload($model, &$data, $key)
    {
        $imageData = $data[$key] ?? null;

        // upload new image
        if (is_array($imageData) && isset($imageData[0]['tmp_name']) && $file = $imageData[0]['file']) {
            /** @var StoreInterface $store */
            $store = $this->storeManager->getStore();
            $baseMediaDir = $store->getBaseMediaDir();
            $newImgRelativePath = $this->imageUploader->moveFileFromTmp($file, true);
            $data[$key] = '/' . $baseMediaDir . '/' . $newImgRelativePath;
            // remove previous image
            if ($model->getData($key)) {
                $this->removeFile($model->getData($key));
            }
            return;
        }

        // remove image
        if (!$imageData && $model->getData($key)) {
            $this->removeFile($model->getData($key));
            $data[$key] = '';
            return;
        }

        unset($data[$key]);
    }

    private function removeFile($path)
    {
        $mediaDirectory = $this->filesystem->getDirectoryWrite(
            \Magento\Framework\App\Filesystem\DirectoryList::PUB
        );
        $mediaDirectory->delete($path);
    }
}


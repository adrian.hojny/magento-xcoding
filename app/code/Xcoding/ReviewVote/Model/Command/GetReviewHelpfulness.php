<?php declare(strict_types=1);

namespace Xcoding\ReviewVote\Model\Command;

use Xcoding\ReviewVote\Api\Data\ReviewVoteInterface;
use Xcoding\ReviewVote\Api\ReviewVoteRepositoryInterface;

/**
 * Class GetReviewHelpfulness
 * @package Xcoding\ReviewVote\Model\Command
 */
class GetReviewHelpfulness
{
    /**
     * @var ReviewVoteRepositoryInterface
     */
    private $reviewVoteRepository;

    /**
     * GetReviewHelpfulness constructor.
     * @param ReviewVoteRepositoryInterface $reviewVoteRepository
     */
    public function __construct(
        ReviewVoteRepositoryInterface $reviewVoteRepository
    ) {
        $this->reviewVoteRepository = $reviewVoteRepository;
    }

    /**
     * @param int $reviewId
     * @return array
     */
    public function execute(int $reviewId): array
    {
        $yesCount = 0;
        $noCount = 0;
        if ($reviewId) {
            $reviewVotes = $this->reviewVoteRepository->getByReviewId((int)$reviewId);
            foreach ($reviewVotes as $reviewVote) {
                if ($reviewVote->getIsHelpful() == ReviewVoteInterface::VOTE_TYPE_VALUES[ReviewVoteInterface::VOTE_UP]) {
                    $yesCount++;
                } elseif ($reviewVote->getIsHelpful() == ReviewVoteInterface::VOTE_TYPE_VALUES[ReviewVoteInterface::VOTE_DOWN]) {
                    $noCount++;
                }
            }
        }

        return [
            'yes' => $yesCount,
            'no' => $noCount
        ];
    }
}

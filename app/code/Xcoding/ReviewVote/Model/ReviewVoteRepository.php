<?php

namespace Xcoding\ReviewVote\Model;

use Xcoding\ReviewVote\Api\Data\ReviewVoteInterface;
use Xcoding\ReviewVote\Api\Data\ReviewVoteInterfaceFactory;
use Xcoding\ReviewVote\Api\ReviewVoteRepositoryInterface;
use Xcoding\ReviewVote\Model\ResourceModel\ReviewVote as ReviewVoteResource;
use Xcoding\ReviewVote\Model\ResourceModel\ReviewVote\CollectionFactory as ReviewVoteCollectionFactory;

/**
 * Class ReviewVoteRepository
 * @package Xcoding\ReviewVote\Model
 */
class ReviewVoteRepository implements ReviewVoteRepositoryInterface
{
    /**
     * @var ReviewVoteCollectionFactory
     */
    private $reviewVoteCollectionFactory;
    /**
     * @var ReviewVoteResource
     */
    private $reviewVoteResource;
    /**
     * @var ReviewVoteInterfaceFactory
     */
    private $reviewVoteInterfaceFactory;

    /**
     * ReviewVoteRepository constructor.
     * @param ReviewVoteResource $reviewVoteResource
     * @param ReviewVoteCollectionFactory $reviewVoteCollectionFactory
     * @param ReviewVoteInterfaceFactory $reviewVoteInterfaceFactory
     */
    public function __construct(
        ReviewVoteResource $reviewVoteResource,
        ReviewVoteCollectionFactory $reviewVoteCollectionFactory,
        ReviewVoteInterfaceFactory $reviewVoteInterfaceFactory
    ) {
        $this->reviewVoteResource = $reviewVoteResource;
        $this->reviewVoteCollectionFactory = $reviewVoteCollectionFactory;
        $this->reviewVoteInterfaceFactory = $reviewVoteInterfaceFactory;
    }

    /**
     * @inheritDoc
     */
    public function vote(string $type, int $customerId, int $reviewId): bool
    {
        if (isset(ReviewVoteInterface::VOTE_TYPE_VALUES[$type])) {
            $reviewVote = $this->getByCustomerIdAndReviewId($customerId, $reviewId);
            $reviewVote->setIsHelpful(ReviewVoteInterface::VOTE_TYPE_VALUES[$type]);
            $this->reviewVoteResource->save($reviewVote);

            return true;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getByCustomerIdAndReviewId(int $customerId, int $reviewId): ReviewVoteInterface
    {
        $reviewVote = $this->reviewVoteCollectionFactory->create()
            ->addFieldToFilter('review_id', $reviewId)
            ->addFieldToFilter('customer_id', $customerId)
            ->getFirstItem();

        if ($reviewVote->getId() === null) {
            $reviewVote = $this->reviewVoteInterfaceFactory->create();
            $reviewVote->setCustomerId($customerId);
            $reviewVote->setReviewId($reviewId);
        }

        return $reviewVote;
    }

    /**
     * @inheritDoc
     */
    public function getByReviewId(int $reviewId): array
    {
        return $this->reviewVoteCollectionFactory->create()
            ->addFieldToFilter('review_id', $reviewId)
            ->getItems();
    }
}

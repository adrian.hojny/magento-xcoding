<?php

namespace Xcoding\ReviewVote\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Xcoding\ReviewVote\Api\Data\ReviewVoteExtensionInterface;
use Xcoding\ReviewVote\Api\Data\ReviewVoteInterface;
use Xcoding\ReviewVote\Model\ResourceModel\ReviewVote as ReviewVoteResourceModel;

/**
 * Class ReviewVote
 * @package Xcoding\ReviewVote\Model
 */
class ReviewVote extends AbstractExtensibleModel implements ReviewVoteInterface
{
    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(ReviewVoteResourceModel::class);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId(): ?int
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId(int $customerId): ReviewVoteInterface
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * @inheritDoc
     */
    public function getReviewId(): ?int
    {
        return $this->getData(self::REVIEW_ID);
    }

    /**
     * @inheritDoc
     */
    public function setReviewId(int $reviewId): ReviewVoteInterface
    {
        return $this->setData(self::REVIEW_ID, $reviewId);
    }

    /**
     * @inheritDoc
     */
    public function getIsHelpful(): ?bool
    {
        return $this->getData(self::IS_HELPFUL);
    }

    /**
     * @inheritDoc
     */
    public function setIsHelpful(bool $isHelpful): ReviewVoteInterface
    {
        return $this->setData(self::IS_HELPFUL, $isHelpful);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes(): ?ReviewVoteExtensionInterface
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(ReviewVoteExtensionInterface $extensionAttributes): ReviewVoteInterface
    {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}

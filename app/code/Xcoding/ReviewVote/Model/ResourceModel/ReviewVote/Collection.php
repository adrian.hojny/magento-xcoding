<?php

namespace Xcoding\ReviewVote\Model\ResourceModel\ReviewVote;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Xcoding\ReviewVote\Model\ResourceModel\ReviewVote as ReviewVoteResource;
use Xcoding\ReviewVote\Model\ReviewVote;

/**
 * Class Collection
 * @package Xcoding\ReviewVote\Model\ResourceModel\ReviewVote
 */
class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            ReviewVote::class,
            ReviewVoteResource::class
        );
    }
}

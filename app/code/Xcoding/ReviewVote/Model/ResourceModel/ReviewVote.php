<?php

namespace Xcoding\ReviewVote\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Xcoding\ReviewVote\Api\Data\ReviewVoteInterface;

/**
 * Class ReviewVote
 * @package Xcoding\ReviewVote\Model\ResourceModel
 */
class ReviewVote extends AbstractDb
{
    /**
     * Constants related to specific db layer
     */
    const TABLE_NAME_SOURCE = 'xcoding_review_vote';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME_SOURCE, ReviewVoteInterface::ID);
    }
}

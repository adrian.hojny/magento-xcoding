<?php

namespace Xcoding\ReviewVote\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;
use Xcoding\ReviewVote\Api\Data\ReviewVoteExtensionInterface;

/**
 * Interface ReviewVoteInterface
 * @package Xcoding\ReviewVote\Api\Data
 */
interface ReviewVoteInterface extends ExtensibleDataInterface
{
    /**#@+
     * @var string
     */
    const ID = 'id';
    const CUSTOMER_ID = 'customer_id';
    const REVIEW_ID = 'review_id';
    const IS_HELPFUL = 'is_helpful';
    const VOTE_UP = 'VOTE_UP';
    const VOTE_DOWN = 'VOTE_DOWN';
    /**#@-*/

    /**
     * @var array
     */
    const VOTE_TYPE_VALUES = [
        'VOTE_UP' => 1,
        'VOTE_DOWN' => 0
    ];

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @param int $id
     * @return int|null
     */
    public function setId($id);

    /**
     * @return int|null
     */
    public function getCustomerId(): ?int;

    /**
     * @param int $customerId
     * @return \Xcoding\ReviewVote\Api\Data\ReviewVoteInterface
     */
    public function setCustomerId(int $customerId): ReviewVoteInterface;

    /**
     * @return int|null
     */
    public function getReviewId(): ?int;

    /**
     * @param int $reviewId
     * @return \Xcoding\ReviewVote\Api\Data\ReviewVoteInterface
     */
    public function setReviewId(int $reviewId): ReviewVoteInterface;

    /**
     * @return bool|null
     */
    public function getIsHelpful(): ?bool;

    /**
     * @param bool $isHelpful
     * @return \Xcoding\ReviewVote\Api\Data\ReviewVoteInterface
     */
    public function setIsHelpful(bool $isHelpful): ReviewVoteInterface;

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Xcoding\ReviewVote\Api\Data\ReviewVoteExtensionInterface|null
     */
    public function getExtensionAttributes(): ?ReviewVoteExtensionInterface;

    /**
     * Set an extension attributes object.
     * @param \Xcoding\ReviewVote\Api\Data\ReviewVoteExtensionInterface $extensionAttributes
     * @return \Xcoding\ReviewVote\Api\Data\ReviewVoteInterface
     */
    public function setExtensionAttributes(
        ReviewVoteExtensionInterface $extensionAttributes
    ): ReviewVoteInterface;
}

<?php

namespace Xcoding\ReviewVote\Api;

use Xcoding\ReviewVote\Api\Data\ReviewVoteInterface;

/**
 * Interface ReviewVoteRepositoryInterface
 * @package Xcoding\ReviewVote\Api
 */
interface ReviewVoteRepositoryInterface
{
    /**
     * @param string $type
     * @param int $customerId
     * @param int $reviewId
     * @return bool
     */
    public function vote(string $type, int $customerId, int $reviewId): bool;

    /**
     * @param int $customerId
     * @param int $reviewId
     * @return \Xcoding\ReviewVote\Api\Data\ReviewVoteInterface
     */
    public function getByCustomerIdAndReviewId(int $customerId, int $reviewId): ReviewVoteInterface;

    /**
     * @param int $reviewId
     * @return \Xcoding\ReviewVote\Api\Data\ReviewVoteInterface[]
     */
    public function getByReviewId(int $reviewId): array;
}

<?php

namespace Xcoding\ProductWidgetExtend\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Images extends AbstractFieldArray
{
    /**
     * {@inheritdoc}
     */
    protected function _prepareToRender()
    {
        $this->addColumn('image_url', ['label' => __('Image URL'), 'class' => 'required-entry', 'style' => 'width: 200px;']);
        $this->addColumn('redirect_url', ['label' => __('Redirect URL'), 'style' => 'width: 200px;']);
        $this->addColumn('position', ['label' => __('Position')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add image');
    }

    public function prepareElementHtml(AbstractElement $element)
    {
        $this->setElement($element);
        $element->setData('after_element_html', $this->toHtml());

        return $element;
    }
}
<?php

namespace Xcoding\ProductWidgetExtend\Model\Resolver\Attribute;

use Magento\Widget\Helper\Conditions as ConditionsHelper;
use ScandiPWA\CmsGraphQl\Api\AttributeHandlerInterface;

class ConditionsEncodedToJson implements AttributeHandlerInterface
{
    /**
     * @var ConditionsHelper
     */
    private $conditionsHelper;

    public function __construct(
        ConditionsHelper $conditionsHelper
    ) {
        $this->conditionsHelper = $conditionsHelper;
    }

    public function resolve(string $value): string
    {
        if ($value) {
            $value = $this->conditionsHelper->decode($value);
            $value = json_encode($value);
            return base64_encode($value);
        }

        return '';
    }
}

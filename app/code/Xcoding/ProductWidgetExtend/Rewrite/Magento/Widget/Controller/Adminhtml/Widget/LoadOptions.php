<?php
declare(strict_types=1);

namespace Xcoding\ProductWidgetExtend\Rewrite\Magento\Widget\Controller\Adminhtml\Widget;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\Helper\Data;
use Magento\Widget\Helper\Conditions;

class LoadOptions extends \Magento\Widget\Controller\Adminhtml\Widget\LoadOptions
{
    /**
     * @var Conditions
     */
    private $conditionsHelper;

    public function __construct(
        Action\Context $context,
        Conditions $conditionsHelper
    ) {
        $this->conditionsHelper = $conditionsHelper;
        parent::__construct($context);
    }

    public function execute()
    {
        try {
            $this->_view->loadLayout();
            if ($paramsJson = $this->getRequest()->getParam('widget')) {
                $request = $this->_objectManager->get(Data::class)
                    ->jsonDecode($paramsJson);
                if (is_array($request)) {
                    $optionsBlock = $this->_view->getLayout()->getBlock('wysiwyg_widget.options');
                    if (isset($request['widget_type'])) {
                        $optionsBlock->setWidgetType($request['widget_type']);
                    }
                    if (isset($request['values'])) {
                        $request['values'] = array_map('htmlspecialchars_decode', $request['values']);
                        if (isset($request['values']['conditions_encoded'])) {
                            $request['values']['conditions'] =
                                $this->getConditionsHelper()->decode($request['values']['conditions_encoded']);
                        }
                        if (isset($request['values']['static_images'])) {
                            $request['values']['static_images'] =
                                $this->getConditionsHelper()->decode($request['values']['static_images']);
                            foreach($request['values']['static_images'] as $key => $img) {
                                if (!is_array($img)) {
                                    $request['values']['static_images'][$key] = [];
                                }
                            }
                        }
                        $optionsBlock->setWidgetValues($request['values']);
                    }
                }
                $this->_view->renderLayout();
            }
        } catch (LocalizedException $e) {
            $result = ['error' => true, 'message' => $e->getMessage()];
            $this->getResponse()->representJson(
                $this->_objectManager->get(Data::class)->jsonEncode($result)
            );
        }
    }

    private function getConditionsHelper()
    {
        return $this->conditionsHelper;
    }
}
<?php
declare(strict_types=1);

namespace Xcoding\ProductWidgetExtend\Rewrite\Magento\Widget\Model;

use Magento\Framework\Escaper;
use Magento\Framework\Math\Random;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Asset\Source;
use Magento\Framework\View\FileSystem;
use Magento\Widget\Helper\Conditions;
use Magento\Widget\Model\Config\Data;

class Widget extends \Magento\Widget\Model\Widget
{
    /**
     * @var Random
     */
    private $mathRandom;

    public function __construct(
        Escaper $escaper,
        Data $dataStorage,
        Repository $assetRepo,
        Source $assetSource,
        FileSystem $viewFileSystem,
        Conditions $conditionsHelper,
        Random $mathRandom
    ) {
        parent::__construct($escaper, $dataStorage, $assetRepo, $assetSource, $viewFileSystem, $conditionsHelper);
        $this->mathRandom = $mathRandom;
    }

    public function getWidgetDeclaration($type, $params = [], $asIs = true)
    {
        $directive = '{{widget type="' . $type . '"';
        $widget = $this->getConfigAsObject($type);
        foreach ($params as $name => $value) {
            // Retrieve default option value if pre-configured
            if ($name == 'conditions') {
                $name = 'conditions_encoded';
                $value = $this->conditionsHelper->encode($value);
            } elseif (is_array($value)) {
                if (count($value) == count($value, COUNT_RECURSIVE)) {
                    $value = implode(',', $value);
                } else {
                    $array = [];
                    foreach ($value as $item) {
                        if (!empty($item)) {
                            $array[] = $item;
                        }
                    }
                    $value = $this->conditionsHelper->encode($array);
                }
            } elseif (trim($value) == '') {
                $parameters = $widget->getParameters();
                if (isset($parameters[$name]) && is_object($parameters[$name])) {
                    $value = $parameters[$name]->getValue();
                }
            }
            if (isset($value)) {
                $directive .= sprintf(' %s="%s"', $name, $this->escaper->escapeHtmlAttr($value, false));
            }
        }

        $directive .= $this->getWidgetPageVarName($params);

        $directive .= '}}';

        if ($asIs) {
            return $directive;
        }

        $html = sprintf(
            '<img id="%s" src="%s" title="%s">',
            $this->idEncode($directive),
            $this->getPlaceholderImageUrl($type),
            $this->escaper->escapeUrl($directive)
        );
        return $html;
    }

    private function getWidgetPageVarName($params = [])
    {
        $pageVarName = '';
        if (array_key_exists('show_pager', $params) && (bool)$params['show_pager']) {
            $pageVarName = sprintf(
                ' %s="%s"',
                'page_var_name',
                'p' . $this->getMathRandom()->getRandomString(5, \Magento\Framework\Math\Random::CHARS_LOWERS)
            );
        }
        return $pageVarName;
    }

    private function getMathRandom()
    {
        return $this->mathRandom;
    }
}
# Xcoding_Stock

#### Flaga małej dostępności w magazynie
- Próg ustawiany jest w `Stores > Configuration > Catalog > Inventory > Low qty threshold`
- Próg może być nadpisany per produkt w `Advanced Inventory > Low qty threshold (use config settings: no)`
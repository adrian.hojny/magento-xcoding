<?php
declare(strict_types=1);

namespace Xcoding\Paypal\Model\Cart\SalesModel;

/**
 * Class Order
 * @package Xcoding\Paypal\Model\Cart\SalesModel
 */
class Order extends \Magento\Payment\Model\Cart\SalesModel\Order implements SalesModelInterface
{
    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->_salesModel->getSubtotal();
    }

    /**
     * @return float
     */
    public function getTaxAmount()
    {
        return $this->_salesModel->getTaxAmount();
    }

    /**
     * @return float
     */
    public function getShippingAmount()
    {
        return $this->_salesModel->getShippingAmount();
    }

    /**
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->_salesModel->getDiscountAmount();
    }

    /**
     * {@inheritdoc}
     */
    public function getAllItems()
    {
        $resultItems = [];

        foreach ($this->_salesModel->getAllItems() as $item) {
            $resultItems[] = new \Magento\Framework\DataObject(
                [
                    'parent_item' => $item->getParentItem(),
                    'name' => $item->getName(),
                    'qty' => (int)$item->getQtyOrdered(),
                    'price' => (double)$item->getPrice(),
                    'original_item' => $item,
                ]
            );
        }

        return $resultItems;
    }
}
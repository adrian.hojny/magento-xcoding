<?php
declare(strict_types=1);

namespace Xcoding\Paypal\Model\Cart\SalesModel;

/**
 * Class Quote
 * @package Xcoding\Paypal\Model\Cart\SalesModel
 */
class Quote extends \Magento\Payment\Model\Cart\SalesModel\Quote implements SalesModelInterface
{
    /**
     * @return float
     */
    public function getSubtotal()
    {
        return $this->_salesModel->getSubtotal();
    }

    /**
     * @return float
     */
    public function getTaxAmount()
    {
        return $this->_address->getTaxAmount();
    }

    /**
     * @return float
     */
    public function getShippingAmount()
    {
        return $this->_address->getShippingAmount();
    }

    /**
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->_address->getDiscountAmount();
    }
}
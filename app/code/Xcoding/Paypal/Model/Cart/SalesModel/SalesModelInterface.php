<?php
declare(strict_types=1);

namespace Xcoding\Paypal\Model\Cart\SalesModel;

/**
 * Interface SalesModelInterface
 * @package Xcoding\Paypal\Model\Cart\SalesModel
 */
interface SalesModelInterface extends \Magento\Payment\Model\Cart\SalesModel\SalesModelInterface
{
    public function getSubtotal();
    public function getTaxAmount();
    public function getShippingAmount();
    public function getDiscountAmount();
}

<?php
declare(strict_types=1);

namespace Xcoding\Paypal\Model;

/**
 * Class Cart
 * @package Xcoding\Paypal\Model
 */
class Cart extends \Magento\Paypal\Model\Cart
{
    /** @var \Xcoding\Paypal\Model\Cart\SalesModel\SalesModelInterface */
    protected $_salesModel;

    protected function _importItemsFromSalesModel()
    {
        $this->_salesModelItems = [];

        foreach ($this->_salesModel->getAllItems() as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $amount = $item->getPrice();
            $qty = $item->getQty();

            $subAggregatedLabel = '';

            // workaround in case if item subtotal precision is not compatible with PayPal (.2)
            if ($amount - round($amount, 2)) {
                $amount = $amount * $qty;
                $subAggregatedLabel = ' x' . $qty;
                $qty = 1;
            }

            // aggregate item price if item qty * price does not match row total
            $itemBaseRowTotal = $item->getOriginalItem()->getRowTotal();
            if ($amount * $qty != $itemBaseRowTotal) {
                $amount = (double)$itemBaseRowTotal;
                $subAggregatedLabel = ' x' . $qty;
                $qty = 1;
            }

            $this->_salesModelItems[] = $this->_createItemFromData(
                $item->getName() . $subAggregatedLabel,
                $qty,
                $amount
            );
        }

        $this->addSubtotal($this->_salesModel->getSubtotal());
        $this->addTax($this->_salesModel->getTaxAmount());
        $this->addShipping($this->_salesModel->getShippingAmount());
        $this->addDiscount(abs($this->_salesModel->getDiscountAmount()));
    }
}

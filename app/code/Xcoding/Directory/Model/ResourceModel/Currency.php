<?php
namespace Xcoding\Directory\Model\ResourceModel;

class Currency extends \Magento\Directory\Model\ResourceModel\Currency
{
    /**
    * @var \Magento\Framework\Mail\Template\TransportBuilder
    */
    protected $transportBuilder;

    public function __construct(
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Model\ResourceModel\Db\Context $context
    ) {
        parent::__construct($context);
        $this->transportBuilder = $transportBuilder;
    }

    /**
     * Saving currency rates
     *
     * @param array $rates
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function saveRates($rates)
    {
        try {
            $this->notifyChanges($rates);
        } catch (\Exception $e) {}
        if (is_array($rates) && count($rates) > 0) {
            $connection = $this->getConnection();
            $data = [];
            foreach ($rates as $currencyCode => $rate) {
                foreach ($rate as $currencyTo => $value) {
                    $value = abs($value);
                    if ($value == 0) {
                        continue;
                    }
                    $data[] = ['currency_from' => $currencyCode, 'currency_to' => $currencyTo, 'rate' => $value];
                }
            }
            if ($data) {
                $connection->insertOnDuplicate($this->_currencyRateTable, $data, ['rate']);
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(__('Please correct the rates received'));
        }
    }

    /**
     * @param array $rates
     *
     * @return void
     * @throws \Magento\Framework\Exception\MailException
     */
    private function notifyChanges($rates)
    {
        $html = 'Zmieniono kursy walut<br/>';
        foreach ($rates as $baseCurrency => $rates) {
            $html .= '<h3>' . $baseCurrency . ':</h3>| ';
            foreach ($rates as $currency => $value) {
                $html .= $currency . ' => <b>' . $value . '</b> | ';
            }
        }
        $sender = [
            'name' => 'SneakerStudio',
            'email' => 'info@sneakerstudioprm.com',
        ];
        $transport = $this->transportBuilder
            ->setTemplateModel('\Xcoding\Directory\Model\PlainEmailTemplate')
            ->setTemplateOptions(['store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])
            ->setTemplateVars([
                'html' => $html,
                'subject' => 'Zmieniono kursy walut',
            ])
            ->setFrom($sender)
            ->addTo('jacek@ftggroup.com')
            ->addCc('adrian.hojny@ftggroup.com', 'lukasz.zakrzewski@ftggroup.com', 'g.wroblewski@xcitstudio.com')
            ->getTransport();
        $transport->sendMessage();
    }
}

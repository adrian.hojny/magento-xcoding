<?php
namespace Xcoding\Directory\Model;

class PlainEmailTemplate implements \Magento\Framework\Mail\TemplateInterface
{
    /**
     * @var array
     */
    private $vars;

    /**
     * Get processed template
     *
     * @return string
     */
    public function processTemplate()
    {
        return $this->vars['html'] ?? '';
    }

    /**
     * Get processed subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->vars['subject'] ?? 'No subject';
    }

    /**
     * Set template variables
     *
     * @param array $vars
     * @return $this
     */
    public function setVars(array $vars)
    {
        $this->vars = $vars;
        return $this;
    }

    /**
     * Set template options
     *
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options)
    {
        return $this;
    }

    /**
     * Return true if template type eq text
     *
     * @return boolean
     */
    public function isPlain()
    {
        return true;
    }

    /**
     * Getter for template type
     *
     * @return int
     */
    public function getType()
    {
        return \Magento\Framework\App\TemplateTypesInterface::TYPE_HTML;
    }
}

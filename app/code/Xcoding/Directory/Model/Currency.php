<?php
namespace Xcoding\Directory\Model;

/**
 * Currency model
 */
class Currency extends \Magento\Directory\Model\Currency
{
    /**
     * Convert price to currency format
     *
     * @param   float $price
     * @param   \Xcoding\Directory\Model\Currency|string $toCurrency
     * @return  float
     * @throws \Exception
     */
    public function convert($price, $toCurrency = null)
    {
        if ($toCurrency === null) {
            return $price;
        } elseif ($rate = $this->getRate($toCurrency)) {
            if ($rate == 1) {
                return (float)$price;
            } else {
                $currencyCode = is_object($toCurrency) ? $toCurrency->getCurrencyCode() : $toCurrency;
                if ($currencyCode == 'EUR') {
                    return (float)$price * (float)$rate;
                }
                return (float)round((float)$price * (float)$rate);
            }
        }

        throw new \Exception(__(
            'Undefined rate from "%1-%2".',
            $this->getCode(),
            $this->getCurrencyCodeFromToCurrency($toCurrency)
        ));
    }
}

<?php

namespace Xcoding\Catalog\Helper;

use Magento\CatalogInventory\Model\StockRegistry;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ResourceModel\Attribute as AttributeResource;
use Magento\ConfigurableProductGraphQl\Model\Variant\Collection as VariantCollection;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable as ConfigurableProductResource;
use Magento\InventoryReservations\Model\ResourceModel\GetReservationsQuantityCache;

/**
 * Helper AvailableSizes
 * @package Xcoding\Catalog\Helper
 */
class AvailableSizes extends \Magento\Framework\App\Helper\AbstractHelper
{
    /** @var StockRegistry */
    public $stockRegistry;

    /** @var ProductRepository */
    public $productRepository;

    /** @var AttributeResource */
    public $attributeResource;

    /** @var VariantCollection */
    public $variantCollection;

    /** @var ConfigurableProductResource */
    public $configurableProductResource;

    /** @var GetReservationsQuantityCache */
    public $getReservationsQuantityCache;

    public function __construct(
        StockRegistry $stockRegistry,
        ProductRepository $productRepository,
        AttributeResource $attributeResource,
        VariantCollection $variantCollection,
        ConfigurableProductResource $configurableProductResource,
        GetReservationsQuantityCache $getReservationsQuantityCache
    ) {
        $this->stockRegistry = $stockRegistry;
        $this->productRepository = $productRepository;
        $this->attributeResource = $attributeResource;
        $this->variantCollection = $variantCollection;
        $this->configurableProductResource = $configurableProductResource;
        $this->getReservationsQuantityCache = $getReservationsQuantityCache;
    }
     
    /**
     * Handle configurable product for first available_sizes creation
     *
     * @param  Product|int $product Product model or ID
     * @param  bool        $commit  Commit attribute update in database
     * @return string
     */
    public function handleProduct($product, $commit = false)
    {
        if (is_numeric($product)) {
            /** @var Product $product */
            $product = $this->productRepository->getById($product);
        }
        if ($product->getTypeId() != 'configurable') { return; }
        /** @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable $type */
        $type = $product->getTypeInstance();
        $childs = $type->getUsedProducts($product);
        $result = [];
        foreach ($childs as $child) {
            /** @var Product $child */
            /** @var \Magento\CatalogInventory\Model\Stock\Status $stockStatus */
            $stockStatus = $this->stockRegistry->getStockStatus($child->getId());
            $manage = $stockStatus->getStockItem()->getManageStock();
            $qty = $stockStatus->getQty();
            $result[$child->getAttributeText('shoes_size')] = $manage ? $qty : '-';
        }
        $encoded = json_encode($result);
        $product->setCustomAttribute('available_sizes', $encoded);
        if ($commit) {
            $this->commit($product->getId(), $encoded);
        }
        return $encoded;
    }

    /**
     * Handle stock item value change
     *
     * @param  \Magento\CatalogInventory\Model\Adminhtml\Stock\Item $item
     */
    public function handleStockItemChange($item)
    {
        $productId = $item->getProductId();
        if (!$productId) { return; }
        /** @var Product $product */
        $product = $this->productRepository->getById($productId);
        if ($product->getTypeId() != 'simple') { return; }

        $size = $product->getAttributeText('shoes_size');
        $parentIds = $this->configurableProductResource->getParentIdsByChild($product->getId());
        foreach ($parentIds as $parentId) {
            /** @var Product $parent */
            $parent = $this->productRepository->getById($parentId);
            $attribute = $parent->getCustomAttribute('available_sizes');
            if (!$attribute) { return; }
            try {
                $sizes = json_decode($attribute->getValue(), true);
            } catch (\Exception $e) {
                $this->handleProduct($parent, true);
                continue;
            }
            $sizes = json_decode($attribute->getValue(), true);
            if (!$sizes) { return; }
            $currentQty = $sizes[$size] ?? null;
            $reservations = $this->getReservationsQuantityCache->execute($product->getSku(), $item->getStockId());
            $newQty = $item->getQty() + $reservations;
            if ($currentQty != $newQty) {
                $sizes[$size] = ($newQty > 0) ? $newQty : 0;
                $parent->setCustomAttribute('available_sizes', json_encode($sizes));
                $encoded = json_encode($sizes);
                $this->commit($parent->getId(), $encoded);
            }
        }
    }

    /**
     * Handle inventory reservation change (on sale)
     *
     * @param  \Magento\InventoryReservations\Model\Reservation $reservation
     */
    public function handleReservationChange($reservation)
    {
        $sku = $reservation->getSku();
        /** @var Product $product */
        $product = $this->productRepository->get($sku);

        $size = $product->getAttributeText('shoes_size');
        $parentIds = $this->configurableProductResource->getParentIdsByChild($product->getId());
        foreach ($parentIds as $parentId) {
            /** @var Product $parent */
            $parent = $this->productRepository->getById($parentId);
            $attribute = $parent->getCustomAttribute('available_sizes');
            if (!$attribute) { return; }
            try {
                $sizes = json_decode($attribute->getValue(), true);
            } catch (\Exception $e) {
                $this->handleProduct($parent, true);
                continue;
            }
            $sizes = json_decode($attribute->getValue(), true);
            if (!$sizes) { return; }
            $currentQty = $sizes[$size] ?? null;
            $qty = $currentQty + $reservation->getQuantity();
            $sizes[$size] = ($qty > 0) ? $qty : 0;
            $parent->setCustomAttribute('available_sizes', json_encode($sizes));
            $encoded = json_encode($sizes);
            $this->commit($parent->getId(), $encoded);
        }
    }

    /**
     * Commit available_sizes product attribute value save, directly on database.
     *
     * @param  int $productId
     * @param  string $value
     */
    private function commit($productId, $value)
    {
        /** @var \Magento\Framework\DB\Adapter\Pdo\Mysql $connection */
        $connection = $this->attributeResource->getConnection();
        $table = 'catalog_product_entity_varchar';
        $attributeId = $this->attributeResource->getIdByCode('catalog_product', 'available_sizes');
        $select = $connection->select()
            ->from($table, 'value_id')
            ->where("entity_id = $productId AND attribute_id = $attributeId");
        $row = $connection->fetchRow($select);
        $data = [
            'attribute_id' => $attributeId,
            'entity_id' => $productId,
            'store_id' => 0,
            'value' => $value,
        ];
        if ($row && ($id = $row['value_id'])) {
            $connection->update($table, $data, 'value_id = ' . $id);
        } else {
            $connection->insert($table, $data);
        }
    }
}

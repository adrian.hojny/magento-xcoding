<?php
namespace Xcoding\Catalog\Cron;

use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class Daily
{
    /**
     * @var ProductResourceModel
     */
    protected $productResourceModel;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    public function __construct(
        ProductResourceModel $productResourceModel,
        ProductCollectionFactory $productCollectionFactory
    ) {
        $this->productResourceModel = $productResourceModel;
        $this->productCollectionFactory = $productCollectionFactory;
    }

   /**
    * @return void
    */
    public function execute() {
        $this->handleNewsCategoryAssign();
    }

    private function handleNewsCategoryAssign()
    {
        $collection = $this->productCollectionFactory->create();
        $newsCategoryId = \Xcoding\Catalog\Observer\ProductSaveBefore::NEWS_CATEGORY_ID;
        $collection->addCategoriesFilter(['eq' => $newsCategoryId])
            ->addFieldToFilter('type_id', 'configurable');
        $collection->addAttributeToSelect('news_to_date');
        foreach ($collection as $product) {
            /** @var \Magento\Catalog\Model\Product $product */
            if ($product->getNewsToDate()) {
                $newTo = strtotime($product->getNewsToDate());
                $now = time();
                if ($now < $newTo) {
                    // Ommiting product. Should be still on NEWS category
                    continue;
                }
            }
            // Unpinning product from NEWS category.
            $categories = (array) $product->getCategoryIds();
            if (($key = array_search($newsCategoryId, $categories)) !== false) {
                unset($categories[$key]);
            }
            $product->setCategoryIds($categories);
            $this->productResourceModel->save($product);
        }
    }
}
<?php
declare(strict_types=1);

namespace Xcoding\Catalog\Observer;

use Magento\CatalogInventory\Model\Adminhtml\Stock\Item;
use Xcoding\Catalog\Helper\AvailableSizes;

class StockItemSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    /** @var AvailableSizes */
    public $availableSizesHelper;
 
    public function __construct(
        AvailableSizes $availableSizesHelper
    ) {
        $this->availableSizesHelper = $availableSizesHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\CatalogInventory\Model\Adminhtml\Stock\Item $product */
        $item = $observer->getEvent()->getItem();
        $this->handleAvailableSizes($item);
        return $this;
    }

    /**
     * Handle Stock Item Save after changing simple product stock
     * 
     * @param Item $item
     * 
     * @return void
     */
    private function handleAvailableSizes($item)
    {
        $this->availableSizesHelper->handleStockItemChange($item);
    }
}

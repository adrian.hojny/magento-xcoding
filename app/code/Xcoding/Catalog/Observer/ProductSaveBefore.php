<?php
declare(strict_types=1);

namespace Xcoding\Catalog\Observer;

use Magento\Catalog\Model\Product;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Xcoding\Catalog\Helper\AvailableSizes;

class ProductSaveBefore implements \Magento\Framework\Event\ObserverInterface
{
    public const NEWS_CATEGORY_ID = 335;

    /** @var WebsiteRepositoryInterface */
    public $websiteRepository;

    /** @var AvailableSizes */
    public $availableSizesHelper;
 
    public function __construct(
        WebsiteRepositoryInterface $websiteRepository,
        AvailableSizes $availableSizesHelper
    ) {
        $this->websiteRepository = $websiteRepository;
        $this->availableSizesHelper = $availableSizesHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Product $product */
        $product = $observer->getEvent()->getProduct();
        $this->handleWebsiteSwitchers($product);
        $this->handleDisable($product);
        $this->handleNewsCategoryAssign($product);
        $this->handleZeroPrice($product);
        $this->handleAvailableSizes($product);
        return $this;
    }

    /**
     * @param Product $product
     * 
     * @return void
     */
    private function handleWebsiteSwitchers($product)
    {
        $websites = $product->getWebsiteIds();
        if ($product->getIsPrmWebsite()) {
            $website = $this->websiteRepository->get('prm');
            $websites[$website->getId()] = (int)$website->getId();
            $website = $this->websiteRepository->get('PRM_PL');
            $websites[$website->getId()] = (int)$website->getId();
        }
        if ($product->getIsBaseWebsite()) {
            $website = $this->websiteRepository->get('base');
            $websites[$website->getId()] = (int)$website->getId();
        }
        $product->setWebsiteIds($websites);
    }

    /**
     * @param Product $product
     * 
     * @return void
     */
    private function handleDisable($product)
    {
        if (!$product->getId()) {
            $product->setStatus(Product\Attribute\Source\Status::STATUS_DISABLED);
        }
    }

    /**
     * @param Product $product
     * 
     * @return void
     */
    private function handleNewsCategoryAssign($product)
    {
        if (!$product->getNewsToDate()) {
            return;
        }
        $newTo = strtotime($product->getNewsToDate());
        $now = time();
        if ($now > $newTo) {
            return;
        }
        $categories = (array) $product->getCategoryIds();
        if (in_array(self::NEWS_CATEGORY_ID, $categories)) {
            return;
        }
        $categories[] = self::NEWS_CATEGORY_ID;
        $product->setCategoryIds($categories);
    }

    /**
     * @param Product $product
     * 
     * @return void
     */
    private function handleZeroPrice($product)
    {
        if (($product->getTypeId() != 'simple') || ($product->getPrice() > 0)) {
            return;
        }
        $product->setStatus(Product\Attribute\Source\Status::STATUS_DISABLED);
    }

    /**
     * Handle configurable product before save, to process their child
     * 
     * @param Product $product
     * 
     * @return void
     */
    private function handleAvailableSizes($product)
    {
        $attribute = $product->getCustomAttribute('available_sizes');
        if (!$product->getId() || ($product->getTypeId() != 'configurable') || ($attribute && $attribute->getValue())) {
            return;
        }
        $this->availableSizesHelper->handleProduct($product);
    }
}

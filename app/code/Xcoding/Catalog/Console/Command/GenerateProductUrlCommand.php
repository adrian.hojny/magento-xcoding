<?php
namespace Xcoding\Catalog\Console\Command;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Product\Action as ProductResourceAction;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\State;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateProductUrlCommand extends \Symfony\Component\Console\Command\Command
{
    /** @var \Magento\Catalog\Model\ProductRepository */
    private $productRepository;

    /** @var State */
    private $state;

    /** @var StoreManagerInterface */
    private $storeManager;

    /** @var ProductCollectionFactory */
    private $productCollectionFactory;

    /** @var ProductResourceAction */
    private $productResourceAction;

    /**
     * GenerateProductUrlCommand constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param State $state
     * @param StoreManagerInterface $storeManager
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductResourceAction $productResourceAction
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        State $state,
        StoreManagerInterface $storeManager,
        ProductCollectionFactory $productCollectionFactory,
        ProductResourceAction $productResourceAction
    ) {
        $this->productRepository = $productRepository;
        $this->state = $state;
        $this->storeManager = $storeManager;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productResourceAction = $productResourceAction;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('generate:product:url')
            ->setDescription('Generate url for given products')
            ->addOption(
                'store', 's',
                InputOption::VALUE_REQUIRED,
                'Generate for one specific store view',
                Store::DEFAULT_STORE_ID
            )
            ->addArgument(
                'pids',
                InputArgument::IS_ARRAY,
                'Product IDs to generate',
                []
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->state->setAreaCode('adminhtml');
        }

        $storeId = $input->getOption('store');

        try {
            $this->perform((int) $storeId, $output, $input->getArgument('pids'));
        } catch (NoSuchEntityException $e) {
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @param int $storeId
     * @param int[] $productIds
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function perform(int $storeId, $output, array $productIds = [])
    {
        $generated = 0;
        $ommited = 0;
        $store = $this->storeManager->getStore($storeId);
        $storeId = $store->getId();
        $output->writeln('Generating urls for store ' . $store->getName());

        $collection = $this->productCollectionFactory->create();
        $collection
            ->setStoreId($storeId)
            ->addStoreFilter($store)
            ->addFieldToFilter('status', ['eq' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED])
            ->addFieldToFilter('visibility', ['gt' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE]);

        if (!empty($productIds)) {
            $collection->addIdFilter($productIds);
        }

        $collection->addAttributeToSelect(['name', 'url_path', 'url_key']);
        $list = $collection->load();

        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($list as $product) {
            $product->setStoreId($storeId);
            $urlKey = $this->slug($product->getName());
            if ($product->getUrlKey() == $urlKey) {
                $output->writeln('(ID ' . $product->getId() . ') Ommiting.');
                $ommited++;
                continue;
            }
            $output->writeln('(ID ' . $product->getId() . ') Replacing "' . $product->getUrlKey() . '" with "' . $urlKey . '"');
            $this->productResourceAction->updateAttributes(
                [$product->getId()],
                ['url_key' => $urlKey],
                $storeId
            );
            $generated++;
        }
        $output->writeln('<info>Done. Generated: ' . $generated . ', ommited: ' . $ommited . ' products in store ' . $store->getName() . '.</info>');
    }

    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @param  string|null  $language
     * @return string
     */
    public static function slug($title, $separator = '-')
    {
        $from = ['Ę', 'Ó', 'Ą', 'Ś', 'Ł', 'Ż', 'Ź', 'Ć', 'Ń', 'ę', 'ó', 'ą', 'ś', 'ł', 'ż', 'ź', 'ć', 'ń'];
        $to = ['E', 'O', 'A', 'S', 'L', 'Z', 'Z', 'C', 'N', 'e', 'o', 'a', 's', 'l', 'z', 'z', 'c', 'n'];
        $title = str_replace($from, $to, $title);

        // Convert all dashes/underscores into separator
        $flip = $separator === '-' ? '_' : '-';

        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);

        // Replace @ with the word 'at'
        $title = str_replace('@', $separator.'at'.$separator, $title);

        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title, 'UTF-8'));

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);

        return trim($title, $separator);
    }
}

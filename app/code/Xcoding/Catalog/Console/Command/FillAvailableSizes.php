<?php
namespace Xcoding\Catalog\Console\Command;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\App\State;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Xcoding\Catalog\Helper\AvailableSizes;

class FillAvailableSizes extends \Symfony\Component\Console\Command\Command
{
    /** @var ProductCollectionFactory */
    public $productCollectionFactory;

    /** @var State */
    private $state;

    /** @var AvailableSizes */
    public $availableSizesHelper;

    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        State $state,
        AvailableSizes $availableSizesHelper
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->state = $state;
        $this->availableSizesHelper = $availableSizesHelper;
        parent::__construct('xcoding:product:fillavailablesizes');
    }

    protected function configure()
    {
        $definition = [
            new InputOption('size', 's', InputOption::VALUE_OPTIONAL, 'Size of batch', 500),
        ];
        $this->setName('xcoding:product:fillavailablesizes')
            ->setDescription('Fill available_sizes attribute')
            ->setDefinition($definition);
    }

    /**
     * @param InputInterface $input
     * @param \Symfony\Component\Console\Output\ConsoleOutput $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->state->setAreaCode('adminhtml');
        }
        $collection = $this->productCollectionFactory->create();
        $size = $input->getOption('size');
        $collection->addFieldToFilter('type_id', 'configurable');
        $collection->addAttributeToFilter('available_sizes', ['null' => true]);
        $collection->getSelect()->limit($size);
        $collection->load();
        foreach ($collection as $product) {
            $result = $this->availableSizesHelper->handleProduct($product, true);
            $output->writeln('Product ' . $product->getId() . ': ' . $result);
        }
        $output->writeln('<info>Done! Processed ' . $collection->count() . ' products</info>');
    }
}

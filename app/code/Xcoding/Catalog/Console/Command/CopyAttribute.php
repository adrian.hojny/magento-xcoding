<?php
namespace Xcoding\Catalog\Console\Command;

use Magento\Catalog\Model\ResourceModel\Attribute as AttributeResource;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CopyAttribute extends \Symfony\Component\Console\Command\Command
{
    /** @var AttributeResource */
    private $attributeResource;

    /**
     * @param AttributeResource $attributeResource
     */
    public function __construct(
        AttributeResource $attributeResource
    ) {
        $this->attributeResource = $attributeResource;

        parent::__construct('xcoding:product:copyattribute');
    }

    protected function configure()
    {
        $definition = [
            new InputOption('from', 'f', InputOption::VALUE_REQUIRED, 'Store to get values from'),
            new InputOption('to', 't', InputOption::VALUE_REQUIRED, 'Store to copy values to'),
            new InputArgument('attribute', InputArgument::REQUIRED, 'Product attribute code'),
        ];
        $this->setName('xcoding:product:copyattribute')
            ->setDescription('Copy product attribute values to another store')
            ->setDefinition($definition);
    }

    /**
     * @param InputInterface $input
     * @param \Symfony\Component\Console\Output\ConsoleOutput $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fromStore = $input->getOption('from');
        $toStore = $input->getOption('to');
        $attributeCode = $input->getArgument('attribute');

        /** @var \Magento\Framework\DB\Adapter\Pdo\Mysql $connection */
        $connection = $this->attributeResource->getConnection();
        $table = $this->attributeResource->getTable('eav_attribute');
        $select = $connection->select();
        $select->from($table);
        $select->where('attribute_code = ?', $attributeCode);
        $select->where('entity_type_id = 4');
        $attribute = $connection->fetchAll($select)[0] ?? null;
        if (!$attribute) {
            $output->writeln('<comment>There is no product attribute named "' . $attributeCode . '"</comment>');
            return;
        }
        if ($attribute['backend_type'] == 'static') {
            $output->writeln('<comment>There is no support for static attributes</comment>');
            return;
        }
        $attributeId = $attribute['attribute_id'];
        $table = 'catalog_product_entity_' . $attribute['backend_type'];

        $table = $this->attributeResource->getTable($table);
        $select = $connection->select();
        $select->from($table);
        $select->where('attribute_id = ?', $attributeId);
        $stores = implode(',', [0, $fromStore, $toStore]);
        $select->where("store_id IN ($stores)");
        $result = $connection->fetchAll($select);

        $mapped = [];
        foreach ($result as $row) {
            $mapped[$row['entity_id']][$row['store_id']] = $row;
        }
        unset($result);

        foreach ($mapped as $productId => $data) {
            if (count($data) == 1 && isset($data[0])) {
                unset($mapped[$productId]);
            }
        }
        $output->writeln('<comment>' . count($mapped) . ' products to process</comment>');

        foreach ($mapped as $productId => $data) {
            $output->writeln('<comment>Processing product ID ' . $productId . '</comment>');
            $fromValue = $data[$fromStore]['value'] ?? null;
            $defaultValue = $data[0]['value'] ?? null;
            if (is_null($fromValue)) {
                $fromValue = $defaultValue;
            }
            $toValue = $data[$toStore]['value'] ?? null;
            if (is_null($toValue)) {
                $toValue = $defaultValue;
            }
            if ($fromValue == $toValue) {
                $output->writeln("Ommiting. Same value: '$fromValue'");
                continue;
            }
            if ($fromValue == $defaultValue) {
                $output->writeln("Seting default value on store $toStore. Change from: '$toValue' to '$defaultValue'");
                $connection->delete($table, "entity_id = $productId AND attribute_id = $attributeId AND store_id = $toStore");
                continue;
            }
            // fromStore value is not default. We need to create custom value for toStore
            $output->writeln("Seting custom value on store $toStore. Change from: '$toValue' to '$fromValue'");
            $connection->delete($table, "entity_id = $productId AND attribute_id = $attributeId AND store_id = $toStore");
            $dataToInsert = [
                'attribute_id' => $attributeId,
                'entity_id' => $productId,
                'store_id' => $toStore,
                'value' => $data[$fromStore]['value'],
            ];
            $connection->insert($table, $dataToInsert);
        }

        $output->writeln('<info>Done! :)</info>');
    }
}

<?php
namespace Xcoding\Catalog\Console\Command;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Framework\App\State;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RegenerateProductUrlCommand extends \Symfony\Component\Console\Command\Command
{
    /** @var \Magento\Framework\App\State */
    private $state;

    /** @var StoreManagerInterface\Proxy */
    private $storeManager;

    /** @var ProductCollectionFactory */
    private $productCollectionFactory;

    /** @var ProductUrlRewriteGenerator */
    private $urlRewriteGenerator;

    /** @var UrlPersistInterface */
    private $urlPersist;

    /**
     * RegenerateProductUrlCommand constructor.
     * @param State $state
     * @param StoreManagerInterface\Proxy $storeManager
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductUrlRewriteGenerator $urlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     */
    public function __construct(
        State $state,
        StoreManagerInterface\Proxy $storeManager,
        ProductCollectionFactory $productCollectionFactory,
        ProductUrlRewriteGenerator $urlRewriteGenerator,
        UrlPersistInterface $urlPersist
    ) {
        $this->state = $state;
        $this->storeManager = $storeManager;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->urlRewriteGenerator = $urlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('regenerate:product:url')
            ->setDescription('Regenerate url for given products')
            ->addOption(
                'store', 's',
                InputOption::VALUE_REQUIRED,
                'Regenerate for one specific store view',
                Store::DEFAULT_STORE_ID
            )
            ->addArgument(
                'pids',
                InputArgument::IS_ARRAY,
                'Product IDs to regenerate',
                []
            );
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->state->setAreaCode('adminhtml');
        }

        $storeId = $input->getOption('store');

        try {
            $this->perform((int) $storeId, $output, $input->getArgument('pids'));
        } catch (NoSuchEntityException $e) {
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }

    /**
     * @param int $storeId
     * @param int[] $productIds
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function perform(int $storeId, $output, array $productIds = [])
    {
        $generated = 0;

        $store = $this->storeManager->getStore($storeId);
        $output->writeln('Regenerating urls for store ' . $store->getName());

        $collection = $this->productCollectionFactory->create();
        $collection
            ->setStoreId($store->getId())
            ->addStoreFilter($store->getId())
            ->addAttributeToSelect('name')
            ->addFieldToFilter('status', ['eq' => \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED])
            ->addFieldToFilter('visibility', ['gt' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE]);

        if (!empty($productIds)) {
            $collection->addIdFilter($productIds);
        }

        $collection->addAttributeToSelect(['url_path', 'url_key']);
        $list = $collection->load();

        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($list as $product) {
            $output->writeln('Regenerating urls for ' . $product->getSku() . ' (' . $product->getId() . ')');
            $product->setStoreId($store->getId());

            $this->urlPersist->deleteByData([
                \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::ENTITY_ID => $product->getId(),
                \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::ENTITY_TYPE => 'product',
                \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::REDIRECT_TYPE => 0,
                \Magento\UrlRewrite\Service\V1\Data\UrlRewrite::STORE_ID => $store->getId()
            ]);

            $newUrls = $this->urlRewriteGenerator->generate($product);
            try {
                $this->urlPersist->replace($newUrls);
                $generated++;
            } catch (\Exception $e) {
                $output->writeln('<error>Duplicated url for store ID %d, product %d (%s) - %s Generated URLs:' . PHP_EOL . '%s</error>' . PHP_EOL, $store->getId(), $product->getId(), $product->getSku(), $e->getMessage(), implode(PHP_EOL, array_keys($newUrls)));
            }
        }
        $output->writeln('<info>Done. Regenerated ' . $generated . ' products in store ' . $store->getName() . '.</info>');
    }
}

<?php
namespace Xcoding\Catalog\Model\Api;

class Currency
{
    /**
     * @var \Xcoding\Directory\Model\ResourceModel\Currency
     */
    public $currencyResource;
 
    public function __construct(
        \Xcoding\Directory\Model\ResourceModel\Currency $currencyResource
    ) {
        $this->currencyResource = $currencyResource;
    }

    /**
     * POST for currency update API
     * @param string $baseCode Base currency code
     * @param mixed  $rates    Array of [currency_code => rate]
     * @return bool
     */
    public function post($baseCode, $rates)
    {
        try {
            foreach ($rates as &$rate) {
                $rate = floatval($rate);
            }
            $this->currencyResource->saveRates([$baseCode => $rates]);
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }

    /**
     * GET for currency API
     * @param string $from From currency code
     * @param string $to   To currency code
     * @return string rate
     */
    public function get($from, $to)
    {
        $rates = $this->currencyResource->getCurrencyRates($from, $to);
        return array_shift($rates);
    }
}

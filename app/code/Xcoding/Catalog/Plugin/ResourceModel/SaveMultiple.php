<?php
namespace Xcoding\Catalog\Plugin\ResourceModel;

use Xcoding\Catalog\Helper\AvailableSizes;

class SaveMultiple
{
    /** @var AvailableSizes */
    public $availableSizesHelper;
 
    public function __construct(
        AvailableSizes $availableSizesHelper
    ) {
        $this->availableSizesHelper = $availableSizesHelper;
    }

    public function afterExecute($subject, $result, array $reservations)
    {
        foreach ($reservations as $reservation) {
            $this->availableSizesHelper->handleReservationChange($reservation);
        }
        return $result;
    }
}
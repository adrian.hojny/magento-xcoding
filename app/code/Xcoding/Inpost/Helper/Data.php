<?php declare(strict_types=1);

namespace Xcoding\Inpost\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

class Data extends AbstractHelper
{
    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;

    /**
     * Data constructor.
     * @param Context $context
     * @param JsonSerializer $jsonSerializer
     */
    public function __construct(
        Context $context,
        JsonSerializer $jsonSerializer
    )
    {
        parent::__construct($context);
        $this->jsonSerializer = $jsonSerializer;
    }

    public function renderMachineAddress($machineInfoJson)
    {
        try {
            $data = $this->jsonSerializer->unserialize($machineInfoJson);
        } catch (Exception $e) {
            return '';
        }

        $address = __('Adres paczkomatu: %1, %2.', $data['address']['line1'] ?? '', $data['address']['line2'] ?? '');
        if ($data['location_description_1']) {
            $address .= '<br>' . __('(%1)', $data['location_description_1']);
        }

        if ($data['location_description_2']) {
            $address .= '<br>' . __('(%1)', $data['location_description_2']);
        }

        return $address;
    }
}

<?php

namespace Xcoding\Inpost\Observer;

use Exception;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\Client\Curl;
use Psr\Log\LoggerInterface;

class QuoteSubmitBefore  implements ObserverInterface
{
    const POINT_INFO_ENDPOINT = 'https://api-shipx-pl.easypack24.net/v1/points/';

    /**
     * @var Curl
     */
    protected $curl;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(
        Curl $curl,
        LoggerInterface $logger
    ) {

        $this->curl = $curl;
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();

        $machineId = $quote->getInpostMachineId();
        if ($machineId) {
            try {
                $data = $this->getMachineInfo($machineId);
                $order->setInpostMachineId($machineId);
                $order->setInpostMachineInfo($data);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }

    protected function getMachineInfo($machineId)
    {
        $this->curl->get(self::POINT_INFO_ENDPOINT . $machineId);
        return $this->curl->getBody();
    }
}
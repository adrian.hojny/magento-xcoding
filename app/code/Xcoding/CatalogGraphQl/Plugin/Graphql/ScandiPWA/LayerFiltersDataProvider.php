<?php

namespace Xcoding\CatalogGraphQl\Plugin\Graphql\ScandiPWA;

use Magento\Catalog\Model\Layer\Filter\AbstractFilter;
use Magento\CatalogGraphQl\Model\Resolver\Layer\FiltersProvider;
use Magento\Framework\Exception\LocalizedException;
use ScandiPWA\CatalogGraphQl\Model\Resolver\Layer\DataProvider\Filters;

/**
 * Class LayerFiltersDataProvider
 * @package Xcoding\CatalogGraphQl\Plugin\Graphql\ScandiPWA
 */
class LayerFiltersDataProvider
{
    /**
     * @var FiltersProvider
     */
    protected $filtersProvider;

    /**
     * Filters constructor.
     * @param FiltersProvider $filtersProvider
     */
    public function __construct(
        FiltersProvider $filtersProvider
    ) {
        $this->filtersProvider = $filtersProvider;
    }

    /**
     * @param Filters $subject
     * @param $result
     * @param string $layerType
     * @return mixed
     */
    public function afterGetData(
        Filters $subject,
        $result,
        string $layerType
    ) {
        $inputTypes = [];
        /** @var AbstractFilter $filter */
        foreach ($this->filtersProvider->getFilters($layerType) as $filter) {
            try {
                $attrModel = $filter->getAttributeModel();
            } catch (LocalizedException $e) {
                // skip filters without attribute model (i.e. "cat")
                continue;
            }

            $inputTypes[$filter->getRequestVar()] = $attrModel->getFrontendInput();
        }
        foreach ($result as $key => $filter) {
            $filter['input_type'] = isset($filter['request_var']) && isset($inputTypes[$filter['request_var']]) ?
                $inputTypes[$filter['request_var']] :
                null;

            $result[$key] = $filter;
        }

        foreach ($result as $key => $filter) {
            if (!$this->canShowFilter($filter)) {
                unset($result[$key]);
            }
        }

        return $result;
    }

    /**
     * @param array $filter
     * @return bool
     */
    protected function canShowFilter(array $filter): bool
    {
        $canShow = true;

        if ($filter['input_type'] === 'boolean') {
            $hasYesOpt = false;
            foreach ($filter['filter_items'] as $opt) {
                if ($opt['value_string'] == 1 && $opt['items_count'] > 0) {
                    $hasYesOpt = true;
                    break;
                }
            }
            $canShow = $hasYesOpt;
        }

        return $canShow;
    }
}

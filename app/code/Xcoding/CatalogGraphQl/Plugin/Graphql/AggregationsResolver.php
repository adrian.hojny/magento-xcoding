<?php declare(strict_types=1);

namespace Xcoding\CatalogGraphQl\Plugin\Graphql;

use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class AggregationsResolver
{
    const SKIPPED_ATTRIBUTES = ['product_collection', 'look'];

    /**
     * @var ProductAttributeRepositoryInterface
     */
    protected $productAttributeRepository;

    /**
     * AggregationsResolver constructor.
     * @param ProductAttributeRepositoryInterface $productAttributeRepository
     */
    public function __construct(
        ProductAttributeRepositoryInterface $productAttributeRepository
    ) {
        $this->productAttributeRepository = $productAttributeRepository;
    }

    /**
     * @param $subject
     * @param $result
     * @return mixed
     */
    public function afterResolve(
        $subject,
        $result
    ) {
        foreach ($result as $key => $value) {
            if (!isset($value['attribute_code'])) {
                continue;
            }

            $value['input_type'] = $this->getInputType($value['attribute_code']);
            $result[$key] = $value;

            if ($this->shouldSkipAttribute($value)) {
                unset($result[$key]);
            }
        }

        return $result;
    }

    protected function getInputType(string $attributeCode): ?string
    {
        try {
            $attribute = $this->productAttributeRepository->get($attributeCode);
        } catch (NoSuchEntityException $e) {
            return null;
        }

        return $attribute->getFrontendInput();
    }

    protected function shouldSkipAttribute(array $value): bool
    {
        if (in_array($value['attribute_code'], self::SKIPPED_ATTRIBUTES)) {
           return true;
        }

        if ($value['input_type'] === 'boolean') {
            return !$this->hasYesOpt($value);
        }

        return false;
    }

    protected function hasYesOpt(array $value): bool
    {
        $hasYesOpt = false;

        foreach ($value['options'] as $option) {
            if ($option['value'] == 1 && $option['count'] > 0) {
                $hasYesOpt = true;
                break;
            }
        }

        return $hasYesOpt;
    }
}
<?php

namespace Xcoding\CatalogGraphQl\Model\Resolver;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class MaxPrice implements ResolverInterface
{
    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!isset($value['search_result'])) {
            throw new LocalizedException(__('"search_result" value should be specified'));
        }

        $searchResult = $value['search_result'];

        return $searchResult->getMaxPrice() ?? 0;
    }
}

<?php

namespace Xcoding\CatalogGraphQl\Model\Resolver;

use Magento\Catalog\Model\ResourceModel\Product as ProductResourceModel;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class CommaSeparatedValuesResolver
 * @package Xcoding\ProductCollection\Model\Resolver
 */
class IsNew implements ResolverInterface
{
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var ProductResourceModel
     */
    private $productResourceModel;

    /**
     * IsNew constructor.
     * @param ProductResourceModel $productResourceModel
     * @param DateTime $date
     */
    public function __construct(
        ProductResourceModel $productResourceModel,
        DateTime $date
    ) {
        $this->date = $date;
        $this->productResourceModel = $productResourceModel;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $now = $this->date->gmtDate();
        $newFromDate = $value['news_from_date'] ?? null;
        $newToDate = $value['news_to_date'] ?? null;

        if ($newFromDate) {
            $isNew = $newFromDate < $now && (!$newToDate || $newToDate > $now);
        } else {
            $isNew = $newToDate && $newToDate > $now;
        }

        return $isNew;
    }
}

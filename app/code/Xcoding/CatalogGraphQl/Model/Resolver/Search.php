<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Xcoding\CatalogGraphQl\Model\Resolver;

use Amasty\ShopbyBase\Model\ResourceModel\OptionSetting\CollectionFactory as OptionSettingCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\CatalogGraphQl\Model\Resolver\Products\Query\ProductQueryInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Catalog\Model\Layer\Resolver;

/**
 * Products field resolver, used for GraphQL request processing.
 */
class Search implements ResolverInterface
{
    /**
     * @var ProductQueryInterface
     */
    private $searchQuery;

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var OptionSettingCollectionFactory
     */
    private $optionSettingCollectionFactory;

    /**
     * @param ProductQueryInterface $searchQuery
     * @param OptionSettingCollectionFactory $optionSettingCollectionFactory
     */
    public function __construct(
        ProductQueryInterface $searchQuery,
        CategoryCollectionFactory $categoryCollectionFactory,
        OptionSettingCollectionFactory $optionSettingCollectionFactory
    ) {
        $this->searchQuery = $searchQuery;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->optionSettingCollectionFactory = $optionSettingCollectionFactory;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if ($args['currentPage'] < 1) {
            throw new GraphQlInputException(__('currentPage value must be greater than 0.'));
        }
        if ($args['pageSize'] < 1) {
            throw new GraphQlInputException(__('pageSize value must be greater than 0.'));
        }
        if (!isset($args['search']) && !isset($args['filter'])) {
            throw new GraphQlInputException(
                __("'search' or 'filter' input argument is required.")
            );
        }

        $searchResult = $this->searchQuery->getResult($args, $info);

        if ($searchResult->getCurrentPage() > $searchResult->getTotalPages() && $searchResult->getTotalCount() > 0) {
            throw new GraphQlInputException(
                __(
                    'currentPage value %1 specified is greater than the %2 page(s) available.',
                    [$searchResult->getCurrentPage(), $searchResult->getTotalPages()]
                )
            );
        }

        $result = [
            'products' => [
                'total_count' => $searchResult->getTotalCount(),
                'items' => $searchResult->getProductsSearchResult(),
                'page_info' => [
                    'page_size' => $searchResult->getPageSize(),
                    'current_page' => $searchResult->getCurrentPage(),
                    'total_pages' => $searchResult->getTotalPages()
                ],
                'search_result' => $searchResult,
                'layer_type' => isset($args['search']) ? Resolver::CATALOG_LAYER_SEARCH : Resolver::CATALOG_LAYER_CATEGORY,
            ]
        ];

        if (array_key_exists('brands', $info->getFieldSelection())) {
            $collection = $this->optionSettingCollectionFactory->create();
            $collection->addFieldToFilter('url_alias', ['like' => $args['search'] . '%']);

            $brands = [];
            foreach ($collection as $item) {
                /** @var \Amasty\ShopbyBase\Model\OptionSetting $item */
                $data = $item->getData();
                $data['slider_image'] = $item->getSliderImageUrl(true);
                $data['image_url'] = $item->getImageUrl();
                $brands[] = $data;
            }
            $result['brands'] = $brands;
        }

        if (array_key_exists('categories', $info->getFieldSelection())) {
            $collection = $this->categoryCollectionFactory->create();
            $collection->addAttributeToFilter('name', ['like' => $args['search'] . '%']);

            $categories = [];
            foreach ($collection as $category) {
                /** @var \Magento\Catalog\Model\Category $category */
                $data = $category->getData();
                $data['model'] = $category;
                $categories[] = $data;
            }
            $result['categories'] = $categories;
        }

        return $result;
    }
}

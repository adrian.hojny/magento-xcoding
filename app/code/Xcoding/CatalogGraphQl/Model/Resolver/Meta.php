<?php

namespace Xcoding\CatalogGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class Meta
 */
class Meta implements \Magento\Framework\GraphQl\Query\ResolverInterface
{
    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        if (!isset($value['model'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('"model" value should be specified'));
        }
        /** @var \Magento\Catalog\Model\Product|\Magento\Catalog\Model\Category $model */
        $model = $value['model'];
        $field = $field->getName();
        $meta = $model->getData($field);
        if ($meta) {
            return $meta;
        }
        return $model->getName();
    }
}

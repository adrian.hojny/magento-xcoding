<?php

namespace Xcoding\CatalogGraphQl\Model\Resolver\Products\DataProvider\Product\CollectionProcessor;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\CatalogGraphQl\Model\Resolver\Products\DataProvider\Product\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

class AdditionalAttributes implements CollectionProcessorInterface
{
    /**
     * @inheritdoc
     */
    public function process(
        Collection $collection,
        SearchCriteriaInterface $searchCriteria,
        array $attributeNames
    ): Collection {
        if (in_array('is_new', $attributeNames)) {
            $collection->addAttributeToSelect('news_from_date');
            $collection->addAttributeToSelect('news_to_date');
        }

        if (in_array('brand_details', $attributeNames)) {
            $collection->addAttributeToSelect('brand');
        }

        if (in_array('sizing_details', $attributeNames)) {
            $collection->addAttributeToSelect('sizing');
            $collection->addAttributeToSelect('brand');
        }

        return $collection;
    }
}

<?php
declare(strict_types=1);

namespace Xcoding\CatalogGraphQl\Model\Resolver\Product;

use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\Value;

/**
 * Format a product's media gallery information to conform to GraphQL schema representation
 */
class MediaGalleryEntries extends \ScandiPWA\CatalogGraphQl\Model\Resolver\Product\MediaGalleryEntries
{
    /**
     * Format product's media gallery entry data to conform to GraphQL schema
     *
     * {@inheritdoc}
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ): Value {
        if (!isset($value['model'])) {
            $result = function () {
                return null;
            };

            return $this->valueFactory->create($result);
        }

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $value['model'];
        $mediaGalleryEntries = [];

        if (!empty($product->getMediaGalleryEntries())) {
            foreach ($product->getMediaGalleryEntries() as $key => $entry) {
                /** @var \Magento\Catalog\Model\Product\Gallery\Entry $entry */
                $thumbnail = $this->getImageOfType($entry, 'scandipwa_media_thumbnail', 'thumbnail');
                $small = $this->getImageOfType($entry, 'xcoding_medium_image', 'small');
                $base = $this->getImageOfType($entry, 'scandipwa_media_base', 'base');
                $mediaGalleryEntries[$key] = $entry->getData() + [
                    'thumbnail' => $thumbnail,
                    'base' => $base,
                    'small' => $small,
                ];

                /** @var \Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryExtension $extensionAttributes */
                $extensionAttributes = $entry->getExtensionAttributes();
                if ($extensionAttributes && ($videoContent = $extensionAttributes->getVideoContent())) {
                    /** @var \Magento\ProductVideo\Model\Product\Attribute\Media\VideoEntry $videoContent */
                    $mediaGalleryEntries[$key]['video_content'] = $videoContent->getData();
                }
            }
        }

        $result = function () use ($mediaGalleryEntries) {
            return $mediaGalleryEntries;
        };

        return $this->valueFactory->create($result);
    }
}

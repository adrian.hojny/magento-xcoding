<?php
declare(strict_types=1);

namespace Xcoding\CatalogGraphQl\Model\Variant;

/**
 * Collection for fetching configurable child product data.
 */
class Collection extends \ScandiPWA\CatalogGraphQl\Model\Variant\Collection
{
    /**
     * Get if we should return only one product, or we need to process them all
     *
     * @param bool $includeFilters
     * @return bool
     */
    protected function getIsReturnSingleChild($includeFilters = false)
    {
        return false;
    }
}

<?php

namespace Xcoding\CatalogGraphQl\Rewrite\Scandipwa\Model\Resolver\Products\Query;

use Exception;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\CatalogGraphQl\DataProvider\Product\SearchCriteriaBuilder;
use Magento\CatalogGraphQl\Model\Resolver\Products\DataProvider\ProductSearch;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Api\Search\SearchCriteriaInterface;
use Magento\CatalogGraphQl\Model\Resolver\Products\SearchResult;
use Magento\CatalogGraphQl\Model\Resolver\Products\SearchResultFactory;
use Magento\Search\Api\SearchInterface;
use Magento\Framework\Api\Search\SearchCriteriaInterfaceFactory;
use Magento\Search\Model\Search\PageSizeProvider;

use Magento\CatalogGraphQl\Model\Resolver\Products\Query\FieldSelection;
use Magento\Store\Model\StoreManagerInterface;
use ScandiPWA\CatalogGraphQl\Model\Resolver\Products\DataProvider\Product\CriteriaCheck;
use ScandiPWA\Performance\Model\Resolver\Products\DataPostProcessor;

class Search extends \ScandiPWA\CatalogGraphQl\Model\Resolver\Products\Query\Search
{
    /**
     * @var SearchInterface
     */
    private $search;
    /**
     * @var SearchResultFactory
     */
    private $searchResultFactory;
    /**
     * @var PageSizeProvider
     */
    private $pageSizeProvider;
    /**
     * @var SearchCriteriaInterfaceFactory
     */
    private $searchCriteriaFactory;
    /**
     * @var FieldSelection
     */
    private $fieldSelection;
    /**
     * @var ProductSearch
     */
    private $productsProvider;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var ProductResource
     */
    private $productResource;

    public function __construct(
        SearchInterface $search,
        SearchResultFactory $searchResultFactory,
        PageSizeProvider $pageSizeProvider,
        SearchCriteriaInterfaceFactory $searchCriteriaFactory,
        FieldSelection $fieldSelection,
        ProductSearch $productsProvider,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        DataPostProcessor $productPostProcessor,
        StoreManagerInterface $storeManager,
        ProductResource $productResource
    ) {
        parent::__construct($search, $searchResultFactory, $pageSizeProvider, $searchCriteriaFactory, $fieldSelection,
            $productsProvider, $searchCriteriaBuilder, $productPostProcessor);
        $this->search = $search;
        $this->searchResultFactory = $searchResultFactory;
        $this->pageSizeProvider = $pageSizeProvider;
        $this->searchCriteriaFactory = $searchCriteriaFactory;
        $this->fieldSelection = $fieldSelection;
        $this->productsProvider = $productsProvider;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->storeManager = $storeManager;
        $this->productResource = $productResource;
    }

    /**
     * Return product search results using Search API
     *
     * @param array $args
     * @param ResolveInfo $info
     * @return SearchResult
     * @throws Exception
     */
    public function getResult(
        array $args,
        ResolveInfo $info
    ): SearchResult {
        $queryFields = $this->fieldSelection->getProductsFieldSelection($info);

        /* xcoding extra price multipler below */
        if ($priceFilter = $args['filter']['price'] ?? null) {
            $currencyRate = $this->storeManager->getStore()->getCurrentCurrencyRate();
            foreach ($priceFilter as &$value) {
                $value /= $currencyRate;
            }
            $args['filter']['price'] = $priceFilter;
        }
        /* xcoding extra price multipler above */

        $searchCriteria = $this->buildSearchCriteria($args, $info);
        $realPageSize = $searchCriteria->getPageSize();
        $realCurrentPage = $searchCriteria->getCurrentPage();

        // Current page must be set to 0 and page size to max for search to grab all ID's as temporary workaround
        $pageSize = $this->pageSizeProvider->getMaxPageSize();
        $searchCriteria->setPageSize($pageSize);
        $searchCriteria->setCurrentPage(0);
        $itemsResults = $this->search->search($searchCriteria);

        $allIds = array_map(function ($elem) {
            return $elem->getId();
        }, $itemsResults->getItems());

        [
            $minPrice,
            $maxPrice
        ] = $this->getMinMaxPrice($allIds);

        //Create copy of search criteria without conditions (conditions will be applied by joining search result)
        $searchCriteriaCopy = $this->searchCriteriaFactory->create()
            ->setSortOrders($searchCriteria->getSortOrders())
            ->setPageSize($realPageSize)
            ->setCurrentPage($realCurrentPage);

        $searchResults = $this->productsProvider->getList($searchCriteriaCopy, $itemsResults, $queryFields);

        //possible division by 0
        if ($realPageSize) {
            $maxPages = (int)ceil($searchResults->getTotalCount() / $realPageSize);
        } else {
            $maxPages = 0;
        }

        $searchCriteria->setPageSize($realPageSize);
        $searchCriteria->setCurrentPage($realCurrentPage);

        // Following lines are changed
        if (count($queryFields) > 0) {
            $productArray = $this->productPostProcessor->process(
                $searchResults->getItems(),
                'products/items',
                $info,
                ['isSingleProduct' => CriteriaCheck::isSingleProductFilter($searchCriteria)]
            );
        } else {
            $productArray = array_map(function ($product) {
                return $product->getData() + ['model' => $product];
            }, $searchResults->getItems());
        }

        return $this->searchResultFactory->create(
            [
                'totalCount' => $searchResults->getTotalCount(),
                'productsSearchResult' => $productArray,
                'searchAggregation' => $itemsResults->getAggregations(),
                'pageSize' => $realPageSize,
                'currentPage' => $realCurrentPage,
                'totalPages' => $maxPages,
                'minPrice' => $minPrice,
                'maxPrice' => $maxPrice
            ]
        );
    }

    /**
     * Build search criteria from query input args
     *
     * @param array $args
     * @param ResolveInfo $info
     * @return SearchCriteriaInterface
     */
    private function buildSearchCriteria(array $args, ResolveInfo $info): SearchCriteriaInterface
    {
        $productFields = (array)$info->getFieldSelection(1);
        $includeAggregations = isset($productFields['filters']) || isset($productFields['aggregations']);
        $searchCriteria = $this->searchCriteriaBuilder->build($args, $includeAggregations);

        return $searchCriteria;
    }

    /**
     * @param array $entityIds
     * @return array
     * @throws NoSuchEntityException
     */
    public function getMinMaxPrice(array $entityIds)
    {
        $connection = $this->productResource->getConnection();
        $currencyRate = $this->storeManager->getStore()->getCurrentCurrencyRate();
        $query = sprintf(
            'SELECT MIN(min_price) as min_price, MAX(max_price) as max_price FROM catalog_product_index_price WHERE entity_id IN ("%s") AND website_id = %d',
            implode('","', $entityIds),
            $this->storeManager->getStore()->getWebsiteId()
        );
        /*
        // Modified version - taking price from index based on store : catalog_product_index_price_store
        // This is for BSS MultiStoreViewPricing module
        $query = sprintf(
            'SELECT MIN(min_price) as min_price, MAX(max_price) as max_price FROM catalog_product_index_price_store WHERE entity_id IN ("%s") AND store_id = %d',
            implode('","', $entityIds),
            $this->storeManager->getStore()->getId()
        );
        */
        $row = $connection->fetchRow($query);
        return [
            (float) $row['min_price'] * $currencyRate,
            (float) $row['max_price'] * $currencyRate
        ];
    }
}
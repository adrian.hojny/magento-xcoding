# Xcoding_CatalogGraphQl

Moduł rozszerza GraphlQL dla katalogu.
 
 #### Zmiany
dodany `input_type` w LayerFilter - fronend_input_type dla atrybutu filtra
dodany atrybut `is_new` bazujący na `news_from_date` i `news_to_date`

#### Stawka podatku dla slidera w filtrze cen

Ustawiana jest w adminie `store > configuration > catalog > layered navigation > Price slider vat rate`

Pobieranie:

```graphql
query {
  storeConfig {
    price_slider_vat_rate
  }
}
```

Odpowiedź:
```json
{
  "data": {
    "storeConfig": {
      "price_slider_vat_rate": 1.23
    }
  }
}
```
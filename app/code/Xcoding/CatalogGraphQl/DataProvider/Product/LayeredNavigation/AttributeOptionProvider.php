<?php
declare(strict_types=1);

namespace Xcoding\CatalogGraphQl\DataProvider\Product\LayeredNavigation;

use Magento\Catalog\Model\ResourceModel\ProductFactory as ProductResourceFactory;
use Magento\Framework\App\ResourceConnection;

/**
 * Rewrited to use StoreId to get attribute label for specific scope
 * Fetch product attribute option data including attribute info
 * Return data in format:
 * [
 *  attribute_code => [
 *      attribute_code => code,
 *      attribute_label => attribute label,
 *      option_label => option label,
 *      options => [option_id => 'option label', ...],
 *  ]
 * ...
 * ]
 */
class AttributeOptionProvider
{
    /** @var ResourceConnection */
    private $resourceConnection;

    /** @var ProductResourceFactory */
    private $productResourceFactory;

    /**
     * @param ResourceConnection $resourceConnection
     * @param ProductResourceFactory $productResourceFactory
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        ProductResourceFactory $productResourceFactory
    ) {
        $this->resourceConnection = $resourceConnection;
        $this->productResourceFactory = $productResourceFactory;
    }

    /**
     * Get option data. Return list of attributes with option data
     *
     * @param array $optionIds
     * @param array $attributeCodes
     * @param int   $storeId
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    public function getOptions(array $optionIds, array $attributeCodes = [], $storeId = 0): array
    {
        if (!$optionIds) {
            return [];
        }

        $connection = $this->resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                ['a' => $this->resourceConnection->getTableName('eav_attribute')],
                [
                    'attribute_id' => 'a.attribute_id',
                    'attribute_code' => 'a.attribute_code',
                    'attribute_label' => 'a.frontend_label',
                ]
            )
            ->joinLeft(
                ['options' => $this->resourceConnection->getTableName('eav_attribute_option')],
                'a.attribute_id = options.attribute_id',
                []
            )
            ->joinLeft(
                ['option_value' => $this->resourceConnection->getTableName('eav_attribute_option_value')],
                'options.option_id = option_value.option_id',
                [
                    'option_label' => 'option_value.value',
                    'option_id' => 'option_value.option_id',
                ]
            );

        $select->where('option_value.option_id IN (?)', $optionIds);

        if (!empty($attributeCodes)) {
            $select->orWhere(
                'a.attribute_code in (?) AND a.frontend_input = \'boolean\'',
                $attributeCodes
            );
        }

        return $this->formatResult($select, $storeId);
    }

    /**
     * Format result
     *
     * @param \Magento\Framework\DB\Select $select
     * @param int                          $storeId
     * @return array
     * @throws \Zend_Db_Statement_Exception
     */
    private function formatResult(\Magento\Framework\DB\Select $select, $storeId): array
    {
        $statement = $this->resourceConnection->getConnection()->query($select);
        $result = [];
        $poductReource = $this->productResourceFactory->create();
        while ($option = $statement->fetch()) {
            $attributeLabel = $poductReource->getAttribute($option['attribute_code'])->getStoreLabel($storeId);
            if (!$attributeLabel) {
                $attributeLabel = $option['attribute_label'];
            }
            
            $attribute = $poductReource->getAttribute($option['attribute_code']);
            if ($attribute->usesSource()) {
                $optionText = $attribute->getSource()->getOptionText($option['option_id']);
            }
            if (!isset($result[$option['attribute_code']])) {
                $result[$option['attribute_code']] = [
                    'attribute_id' => $option['attribute_id'],
                    'attribute_code' => $option['attribute_code'],
                    'attribute_label' => $attributeLabel,
                    'options' => [],
                ];
            }
            $result[$option['attribute_code']]['options'][$option['option_id']] =  $optionText;
        }

        return $result;
    }
}

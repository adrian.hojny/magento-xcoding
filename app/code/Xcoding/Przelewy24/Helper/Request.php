<?php

namespace Xcoding\Przelewy24\Helper;

use Magento\Framework\HTTP\Client\Curl;
use Xcoding\Przelewy24\Helper\Config as Przelewy24ConfigHelper;

class Request extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Curl
     */
    protected $curl;

    /**
     * @var Przelewy24ConfigHelper
     */
    protected $configHelper;

    /**
     * @var \Dialcom\Przelewy\Przelewy24Class|null
     */
    protected $p24 = null;

    /**
     * Config constructor.
     * @param Curl $curl
     * @param Przelewy24ConfigHelper $configHelper
     */
    public function __construct(
        Curl $curl,
        Przelewy24ConfigHelper $configHelper
    ) {
        $this->curl = $curl;
        $this->configHelper = $configHelper;
    }

    /**
     * @return \Dialcom\Przelewy\Przelewy24Class
     */
    public function getPrzelewy24Class()
    {
        if (!$this->p24) {
            $this->p24 = new \Dialcom\Przelewy\Przelewy24Class(
                $this->configHelper->getValue('merchant_id'),
                $this->configHelper->getValue('shop_id'),
                $this->configHelper->getValue('salt'),
                ($this->configHelper->getValue('mode') == '1')
            );
        }
        return $this->p24;
    }

    public function getConfigHelper()
    {
        return $this->configHelper;
    }

    /**
     * @param string        $method POST / GET
     * @param string        $url    Przelewy24 method @example 'payment/methods'
     * @param array|string  $params POST params
     * @return mixed
     */
    public function makeRequest($method, $url, $params = null)
    {
        $sellerId = $this->configHelper->getValue('merchant_id');
        $password = $this->configHelper->getApiKey();
        $url = $this->getPrzelewy24Class()->getHost() . 'api/v1/' . $url;
        $this->curl->setCredentials($sellerId, $password);
        if (strtolower($method) == 'get') {
            $this->curl->get($url);
        } else {
            $this->curl->post($url, $params);
        }
        return $this->curl->getBody();
    }
}
<?php

namespace Xcoding\Przelewy24\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Config extends AbstractHelper
{
    /**#@+
     * @var string
     */
    const XML_PRZELEWY24_PREFIX = 'payment/dialcom_przelewy/';
    const XML_PRZELEWY24_ADDITIONAL_PREFIX = 'przelewy_settings/additionall/';
    /**#@- */

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Context $context
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Context $context
    ) {
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @param string $value
     * @return string
     */
    public function getValue(string $value)
    {
        return $this->scopeConfig->getValue(self::XML_PRZELEWY24_PREFIX . $value, ScopeInterface::SCOPE_WEBSITE) ??
            $this->scopeConfig->getValue(self::XML_PRZELEWY24_ADDITIONAL_PREFIX . $value, ScopeInterface::SCOPE_WEBSITE);
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->scopeConfig->getValue('przelewy_settings/keys/api_key', ScopeInterface::SCOPE_WEBSITE);
    }
}
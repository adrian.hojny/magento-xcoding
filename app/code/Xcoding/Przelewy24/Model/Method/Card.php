<?php

namespace Xcoding\Przelewy24\Model\Method;

class Card extends \Magento\Payment\Model\Method\AbstractMethod
{
    const PAYMENT_METHOD_CODE = 'przelewy_card';
    protected $_code = self::PAYMENT_METHOD_CODE;
}

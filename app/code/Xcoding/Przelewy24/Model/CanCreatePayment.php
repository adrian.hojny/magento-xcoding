<?php

namespace Xcoding\Przelewy24\Model;

use Dialcom\Przelewy\Przelewy24Class;
use Magento\Sales\Api\Data\OrderInterface;

class CanCreatePayment
{
    /**
     * @param OrderInterface $order
     * @return bool
     */
    public function execute(OrderInterface $order)
    {
        return $order->getEntityId() &&
            $order->getBaseTotalDue() > 0 &&
            in_array(
                $order->getStatus(),
                ['pending', Przelewy24Class::PENDING_PAYMENT_CUSTOM_STATUS]
            );
    }
}
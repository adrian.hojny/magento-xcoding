<?php

namespace Xcoding\Przelewy24\Model;

use Magento\Framework\UrlInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\StoreManagerInterface;
use Xcoding\Przelewy24\Helper\Request as RequestHelper;

/**
 * Class CreatePayment
 */
class CreatePayment
{
    /**
     * @var \Magento\Framework\Locale\Resolver
     */
    protected $localeResolver;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var RequestHelper
     */
    protected $requestHelper;

    /**
     * CreatePayment constructor.
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\Locale\Resolver $localeResolver,
        StoreManagerInterface $storeManager,
        RequestHelper $requestHelper
    ) {
        $this->localeResolver = $localeResolver;
        $this->storeManager = $storeManager;
        $this->requestHelper = $requestHelper;
    }

    /**
     * @param Order $order
     * @return string|null
     */
    public function execute(OrderInterface $order)
    {
        if ($order->getIncrementId()) {
            $order->addStatusToHistory(Order::STATE_PENDING_PAYMENT, __('Waiting for payment.'));
            $order->setSendEmail(true);
            $order->save();
        }

        $configHelper = $this->requestHelper->getConfigHelper();
        $merchantId = $configHelper->getValue('merchant_id');
        /** @var \Magento\Store\Model\Store $store */
        $store = $this->storeManager->getStore();
        $baseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_WEB, true);
        $amount = (int) round($order->getGrandTotal() * 100);
        $currencyCode = $order->getOrderCurrencyCode();
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $order->getPayment();
        $localeCode = $this->localeResolver->getLocale();
        $params = [
            'merchantId' => $merchantId,
            'posId' => $merchantId,
            'sessionId' => $order->getId(),
            'amount' => $amount,
            'currency' => $currencyCode,
            'description' => $order->getIncrementId(),
            'email' => $order->getCustomerEmail(),
            'client' => $order->getCustomerFirstname(),
            'country' => $order->getBillingAddress()->getCountryId(),
            'language' => (strpos($localeCode, 'pl') === false) ? 'en' : 'pl',
            'regulationAccept' => true,
            'urlStatus' => $baseUrl . 'rest/V1/przelewy24/status',
            'urlReturn' => $baseUrl . 'checkout/success?order_id=' . $order->getIncrementId(),
        ];
        $json = json_encode([
            'sessionId' => $order->getId(),
            'merchantId' => (int) $merchantId,
            'amount' => (int) $amount,
            'currency' => $currencyCode,
            'crc' => $configHelper->getValue('salt'),
        ], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $hash = hash('sha384', $json);
        $params['sign'] = $hash;

        if ($method = $payment->getAdditionalInformation('method') ?? null) {
            $params['method'] = $method;
        }

        $response = $this->requestHelper->makeRequest('post', 'transaction/register', $params);
        $response = json_decode($response, true);
        $token = $response['data']['token'] ?? null;
        if (!$token) {
            return;
        }
        $blik = $payment->getAdditionalInformation('blik');
        if (!$blik) {
            $url = $this->requestHelper->getPrzelewy24Class()->getHost() . "trnRequest/$token";
            $payment->setAdditionalInformation('payment_redirect_url', $url)->save();
            return $url;
        }

        $params = [
            'token' => $token,
            'blikCode' => $blik,
        ];
        $this->requestHelper->makeRequest('post', 'paymentMethod/blik/chargeByCode', $params);
    }
}

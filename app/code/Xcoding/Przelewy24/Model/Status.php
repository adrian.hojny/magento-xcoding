<?php

namespace Xcoding\Przelewy24\Model;

use Dialcom\Przelewy\Helper\Data;
use Dialcom\Przelewy\Model\Config\Waluty;
use Dialcom\Przelewy\Przelewy24Class;
use Exception;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\Adapter\Curl as CurlAdapter;
use Magento\Framework\HTTP\Adapter\CurlFactory;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Store\Model\ScopeInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Xcoding\Przelewy24\Helper\Config as ConfigHelper;
use Zend_Http_Response;

class Status
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var Json
     */
    private $jsonSerializer;
    /**
     * @var CurlFactory
     */
    private $curlFactory;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var string
     */
    private $sellerId;
    /**
     * @var string
     */
    private $host;

    /**
     * Status constructor.
     * @param LoggerInterface $logger
     * @param RequestInterface $request
     * @param Data $helper
     * @param ScopeConfigInterface $scopeConfig
     * @param OrderRepositoryInterface $orderRepository
     * @param Curl $curl
     * @param CurlFactory $curlFactory
     * @param ConfigHelper $configHelper
     * @param Json $jsonSerializer
     */
    public function __construct(
        LoggerInterface $logger,
        RequestInterface $request,
        Data $helper,
        ScopeConfigInterface $scopeConfig,
        OrderRepositoryInterface $orderRepository,
        Curl $curl,
        CurlFactory $curlFactory,
        ConfigHelper $configHelper,
        Json $jsonSerializer
    )
    {
        $this->logger = $logger;
        $this->request = $request;
        $this->helper = $helper;
        $this->scopeConfig = $scopeConfig;
        $this->orderRepository = $orderRepository;
        $this->curl = $curl;
        $this->jsonSerializer = $jsonSerializer;
        $this->curlFactory = $curlFactory;
        $this->configHelper = $configHelper;
    }

    /**
     * @return string
     */
    public function setStatus()
    {
        $post = json_decode($this->request->getContent(), true);
        $result = false;
        $sessionId = substr($post['sessionId'] ?? null, 0, 100);
        $this->logger->info(__('[P24] Setting status for order with session_id %1', $sessionId));
        try {
            $this->sellerId = $this->configHelper->getValue('merchant_id');

            $P24 = new Przelewy24Class(
                $this->sellerId,
                $this->configHelper->getValue('shop_id'),
                $this->configHelper->getValue('salt'),
                ($this->configHelper->getValue('mode') == '1')
            );
            $this->p24Class = $P24;
            $this->host = $P24->getHost();

            if (isset($sessionId)) {
                $sa_sid = explode('|', $sessionId);
                $orderId = isset($sa_sid[0]) ? (int) $sa_sid[0] : null;
                $this->logger->info(__('[P24] Order id = %1', $orderId));

                if (!is_null($orderId)) {
                    $result = $this->verifyTransaction($orderId, $post);
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), ['exception' => $e]);
        }


        echo $result ? 'OK' : 'ERROR';
        exit;
    }

    protected function verifyTransaction($orderId, $post)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderRepository->get($orderId);

        $payment = $order->getPayment();
        $storeId = $order->getStoreId();

        if ($payment) {
            $payment->setData('transaction_id', (int)$post['orderId']);
            $payment->addTransaction(Transaction::TYPE_ORDER);
        }
        $currency = $order->getOrderCurrencyCode();

        $fullConfig = Waluty::getFullConfig($currency, null, $storeId);

        $data = array('amount' => number_format($order->getGrandTotal() * 100, 0, "", ""), 'currency' => $currency);
        $ret = $this->trnVerify($post, $order, $data, $fullConfig);

        if ($ret) {
            $sendOrderUpdateEmail = false;

            if ($ret === true) {
                $chgState = (int)$this->scopeConfig->getValue(Data::XML_PATH_CHG_STATE, ScopeInterface::SCOPE_STORE, $storeId);

                if ($chgState == 1) {
                    if ($order->getState() != Order::STATE_PROCESSING) {
                        $sendOrderUpdateEmail = true;
                    }
                    $order->addStatusToHistory(Order::STATE_PROCESSING, __('The payment has been accepted.'), true);
                    $order->setState(Order::STATE_PROCESSING, true);
                    $order->save();
                    $order->setTotalPaid($order->getGrandTotal());
                }
                $order->save();
            } else {
                if ($order->getState() != Order::STATE_HOLDED) {
                    $sendOrderUpdateEmail = true;
                }

                $order->addStatusToHistory(Order::STATE_HOLDED, __('Payment error.') . ' ' . $ret['errorMessage'], true);
                $order->setState(Order::STATE_HOLDED, true);
            }

            if ($sendOrderUpdateEmail == true) {
                $order->setSendEmail(true);
            }
            $order->save();

            return $ret === true;
        } else {
            $this->logger->info(__('[P24] Transaction not verified'));
        }

        return false;
    }

    private function trnVerify(array $data, $order, $orderData, $config)
    {
        $response = $this->validateResponse($data, $config);
        $this->logger->info(__('[P24] Is request valid: %1', (bool) $response ? 'true' : 'false'));

        foreach ($orderData as $field => $value) {
            if (!isset($response[$field]) || $response[$field] != $value) {
                return false;
            }
        }
        $requestData = [
            'merchantId' => $config['merchant_id'],
            'posId' => $config['shop_id'],
            'sessionId' => $data['sessionId'],
            'amount' => $data['amount'],
            'currency' => $data['currency'],
            'orderId' => $data['orderId'],
            'sign' => $this->getSign($data)
        ];

        $this->logger->info(__('[P24] Making request'));
        $response = $this->makeRequest($requestData);
        $this->logger->info(__('Result: %1', $response['data']['status'] ?? 'invalid response'));

        return isset($response['data']['status']) && $response['data']['status'] === 'success';
    }

    private function validateResponse(array $data, $config)
    {
        // todo filterValue z klasy p24
        if (isset($data['sessionId'], $data['orderId'], $data['merchantId'], $data['posId'], $data['amount'], $data['currency'],  $data['sign'])) {
            if (
                ($data['merchantId'] != $config['merchant_id'] || $data['posId'] != $config['shop_id']) ||
                $this->getNotificationSign($data) !== $data['sign']
            ) {
                return false;
            }

            return [
                'sessionId' => $data['sessionId'],
                'orderId' => $data['orderId'],
                'amount' => $data['amount'],
                'currency' => $data['currency'],
                'method' => $data['methodId'],
                'statement' => $data['statement']
            ];
        }

        return null;
    }

    /**
     * @param array $dataToPost
     * @return string
     * @throws Exception
     */
    private function getSign(array $dataToPost)
    {
        // zle
        $keys = ['sessionId', 'orderId', 'amount', 'currency'];
        $signData = [];
        foreach ($keys as $key) {
            $signData[$key] = $dataToPost[$key];
        }

        $signData['crc'] = $this->configHelper->getValue('crc');
        $signData['amount'] = (int)$signData['amount'];

        $encodedData = json_encode($signData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        if (false === $encodedData) {
            throw new Exception(__("Something went wrong while creating payment."));
        }

        return hash('sha384', $encodedData);
    }


    /**
     * @param array $dataToPost
     * @return string
     * @throws Exception
     */
    private function getNotificationSign(array $dataToPost)
    {
        $keys = ['merchantId', 'posId', 'sessionId', 'amount', 'originAmount', 'currency', 'orderId', 'methodId', 'statement'];
        $signData = [];
        foreach ($keys as $key) {
            $signData[$key] = $dataToPost[$key];
        }

        $signData['crc'] = $this->configHelper->getValue('crc');
        $signData['amount'] = (int)$signData['amount'];

        $encodedData = json_encode($signData, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        if (false === $encodedData) {
            throw new Exception("Something went wrong while creating payment.");
        }

        return hash('sha384', $encodedData);
    }

    protected function makeRequest(array $dataToPost)
    {
        $url = $this->host . 'api/v1/transaction/verify';
        $basicAuth = base64_encode("{$this->configHelper->getValue('shop_id')}:{$this->configHelper->getValue('password')}");

        /* Create curl factory */
        /** @var CurlAdapter $httpAdapter */
        $httpAdapter = $this->curlFactory->create();
        /* Forth parameter is POST body */
        $httpAdapter->write(
            \Zend_Http_Client::PUT,
            $url,
            '1.1',
            [
                "Content-Type:application/json",
                "Authorization: Basic {$basicAuth}"
            ],
            json_encode($dataToPost)
        );
        $result = $httpAdapter->read();
        $body = Zend_Http_Response::extractBody($result);

        return $this->jsonSerializer->unserialize($body);
    }
}
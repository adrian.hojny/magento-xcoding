<?php

namespace Xcoding\Przelewy24\Plugin\Order;

use Dialcom\Przelewy\Model\Recurring;
use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order\Interceptor;
use Psr\Log\LoggerInterface;
use Xcoding\Przelewy24\Model\CreatePayment;

class CreateP24PaymentAfterPlaceOrder
{
    const P24_PAYMENT_METHOD_CODE = 'dialcom_przelewy';
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var CreatePayment
     */
    protected $createPayment;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * CreateP24PaymentAfterPlaceOrder constructor.
     * @param CheckoutSession $checkoutSession
     * @param CreatePayment $createPayment
     * @param LoggerInterface $logger
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        CreatePayment $createPayment,
        LoggerInterface $logger
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->createPayment = $createPayment;
        $this->logger = $logger;
    }

    /**
     * @param OrderManagementInterface $orderManagementInterface
     * @param OrderInterface $order
     * @return OrderInterface $order
     */
    public function afterPlace(OrderManagementInterface $orderManagementInterface, $order)
    {
        $this->checkoutSession->setPrzelewyQuoteId($this->checkoutSession->getQuoteId());

        $p24Card = \Xcoding\Przelewy24\Model\Method\Card::PAYMENT_METHOD_CODE;
        if (in_array($order->getPayment()->getMethod(), [self::P24_PAYMENT_METHOD_CODE, $p24Card])) {
            $additionalInformation = $order->getPayment()->getAdditionalInformation();
            if (isset($additionalInformation['p24_forget'])) {
                Recurring::setP24Forget((int)$additionalInformation['p24_forget'] === 1);
            }

            try {
                $this->createPayment->execute($order);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }

            $this->checkoutSession->unsQuoteId();
        }
        return $order;
    }
}
<?php

namespace Xcoding\BusinessExtension\Model\Product\Feed;

use \Magento\Catalog\Model\Product;
use \Magento\Catalog\Model\Product\Gallery\ReadHandler;
use \Magento\Directory\Model\PriceCurrency;
use \Psr\Log\LoggerInterface;

class Builder extends \Facebook\BusinessExtension\Model\Product\Feed\Builder
{
    /** @var ReadHandler */
    protected $galleryReadHandler;

    /** @var PriceCurrency */
    protected $priceCurrency;

    /** @var \Xcoding\BusinessExtension\Helper\FBEHelper */
    protected $fbeHelper;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * @param \Xcoding\BusinessExtension\Helper\FBEHelper $fbeHelper
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Facebook\BusinessExtension\Model\Product\Feed\Builder\Tools $builderTools
     * @param \Facebook\BusinessExtension\Model\Product\Feed\Builder\Inventory $inventory
     * @param \Facebook\BusinessExtension\Model\Feed\EnhancedCatalogHelper $enhancedCatalogHelper
     * @param ReadHandler $galleryReadHandler
     * @param PriceCurrency $priceCurrency
     */
    public function __construct(
        \Xcoding\BusinessExtension\Helper\FBEHelper $fbeHelper,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Facebook\BusinessExtension\Model\Product\Feed\Builder\Tools $builderTools,
        \Facebook\BusinessExtension\Model\Product\Feed\Builder\Inventory $inventory,
        \Facebook\BusinessExtension\Model\Feed\EnhancedCatalogHelper $enhancedCatalogHelper,
        ReadHandler $galleryReadHandler,
        PriceCurrency $priceCurrency,
        LoggerInterface $logger
    ) {
        parent::__construct(
            $fbeHelper,
            $categoryCollectionFactory,
            $builderTools,
            $inventory,
            $enhancedCatalogHelper
        );
        $this->galleryReadHandler = $galleryReadHandler;
        $this->priceCurrency = $priceCurrency;
        $this->logger = $logger;
    }

    /**
     * @param Product $product
     * @return array
     */
    protected function getProductImages(Product $product)
    {
        $images = $product->getMediaGalleryImages();
        if (!$images || !count($images)) {
            $this->galleryReadHandler->execute($product);
            $images = $product->getMediaGalleryImages();
        }
        $main = '';
        $additional = [];
        $mainImageFile = $product->getThumbnail();
        foreach ($images as $img) {
            if ($img['file'] == $mainImageFile) {
                $main = $img['url'];
                continue;
            }
            $additional[] = $img['url'];
        }
        return [
            'main_image' => $main,
            'additional_images' => $additional
        ];
    }

    /**
     * @param Product $product
     * @return string
     */
    protected function getProductPrice(Product $product)
    {
        return $this->formatPrice($product->getFinalPrice());
    }

    /**
     * @param Product $product
     * @return string
     */
    protected function getProductSalePrice(Product $product)
    {
        return $this->formatPrice($product->getSpecialPrice());
    }

    /**
     * @param float $product
     * @return string
     */
    protected function formatPrice($price)
    {
        if (!$price) {
            return '';
        }
        $currencyCode = $this->fbeHelper->getStore()->getCurrentCurrency()->getCurrencyCode();
        return $this->priceCurrency->convert($price, null, $currencyCode) . ' ' . $currencyCode;
    }

    /**
     * @param Product $product
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function buildProductEntry(Product $product)
    {
        $this->inventory->initInventoryForProduct($product);

        $productType = $this->trimAttribute(self::ATTR_PRODUCT_TYPE, $this->getCategoryPath($product));

        $title = $product->getName();
        $productTitle = $this->trimAttribute(self::ATTR_NAME, $title);

        $images = $this->getProductImages($product);
        $imageUrl = $this->trimAttribute(self::ATTR_IMAGE_URL, $images['main_image']);

        $entry = [
            self::ATTR_RETAILER_ID          => $this->trimAttribute(self::ATTR_RETAILER_ID, $product->getId()),
            self::ATTR_ITEM_GROUP_ID        => $this->getItemGroupId($product),
            self::ATTR_NAME                 => $productTitle,
            self::ATTR_DESCRIPTION          => $this->getDescription($product),
            self::ATTR_AVAILABILITY         => $this->inventory->getAvailability(),
            self::ATTR_INVENTORY            => $this->inventory->getInventory(),
            self::ATTR_BRAND                => $this->getBrand($product),
            self::ATTR_PRODUCT_CATEGORY     => $product->getGoogleProductCategory() ?? '',
            self::ATTR_PRODUCT_TYPE         => $productType,
            self::ATTR_CONDITION            => $this->getCondition($product),
            self::ATTR_PRICE                => $this->getProductPrice($product),
            self::ATTR_SALE_PRICE           => $this->getProductSalePrice($product),
            self::ATTR_COLOR                => $this->getColor($product),
            self::ATTR_SIZE                 => $this->getSize($product),
            self::ATTR_URL                  => $product->getProductUrl(),
            self::ATTR_IMAGE_URL            => $imageUrl,
            self::ATTR_ADDITIONAL_IMAGE_URL => implode(',', $images['additional_images']),
        ];

        $debug = true;
        if ($debug) {
            $this->logger->debug('Generated feed for product ID ' . $product->getId() . ' from store: ' . $product->getStoreId());
            $this->logger->debug(print_r($entry, true));
        }

        $this->enhancedCatalogHelper->assignECAttribute($product, $entry);

        return $entry;
    }
}

<?php

namespace Xcoding\BusinessExtension\Helper;

use Facebook\BusinessExtension\Logger\Logger;
use Facebook\BusinessExtension\Model\ConfigFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

class FBEHelper extends \Facebook\BusinessExtension\Helper\FBEHelper
{
    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * FBEHelper constructor
     *
     * @param Context $context
     * @param ObjectManagerInterface $objectManager
     * @param ConfigFactory $configFactory
     * @param Logger $logger
     * @param DirectoryList $directorylist
     * @param StoreManagerInterface $storeManager
     * @param Curl $curl
     * @param ResourceConnection $resourceConnection
     * @param ModuleListInterface $moduleList
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        ObjectManagerInterface $objectManager,
        ConfigFactory $configFactory,
        Logger $logger,
        DirectoryList $directorylist,
        StoreManagerInterface $storeManager,
        Curl $curl,
        ResourceConnection $resourceConnection,
        ModuleListInterface $moduleList,
        ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct(
            $context,
            $objectManager,
            $configFactory,
            $logger,
            $directorylist,
            $storeManager,
            $curl,
            $resourceConnection,
            $moduleList
        );
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return \Magento\Store\Model\Store
     */
    public function getStore()
    {
        $id = $this->scopeConfig->getValue('facebook_business_extension/catalog_management/store_id');
        return $this->storeManager->getStore($id);
    }
}

<?php declare(strict_types=1);

namespace Xcoding\ShopByBrandGraphQl\Plugin\Resolver\Argument;

use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogWidget\Model\Rule;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Query\Resolver\Argument\AstConverter;
use Magento\Framework\GraphQl\Query\Resolver\Argument\FieldEntityAttributesPool;
use Magento\Framework\GraphQl\Query\Resolver\Argument\Filter\ClauseFactory;
use Magento\Rule\Model\Condition\Sql\Builder;
use Magento\Widget\Helper\Conditions;
use Xcoding\ShopByBrandGraphQl\Model\Resolver\Products\SearchCriteria\CollectionProcessor\FilterProcessor\BrandUrlAliasFilter;

class AstConverterPlugin
{
    /** @var Conditions */
    protected $conditionsHelper;

    /** @var Rule */
    protected $rule;

    /** @var FieldEntityAttributesPool */
    protected $fieldEntityAttributesPool;

    /** @var CollectionFactory */
    protected $productCollectionFactory;

    /** @var Builder */
    protected $sqlBuilder;

    /** @var ClauseFactory */
    protected $clauseFactory;
    /**
     * @var BrandUrlAliasFilter
     */
    private $brandUrlAliasFilter;

    /**
     * AstConverterPlugin constructor.
     * @param Conditions $conditionsHelper
     * @param Rule $rule
     * @param ClauseFactory $clauseFactory
     * @param FieldEntityAttributesPool $fieldEntityAttributesPool
     * @param Builder $sqlBuilder
     * @param CollectionFactory $productCollectionFactory
     * @param BrandUrlAliasFilter $brandUrlAliasFilter
     */
    public function __construct(
        Conditions $conditionsHelper,
        Rule $rule,
        ClauseFactory $clauseFactory,
        FieldEntityAttributesPool $fieldEntityAttributesPool,
        Builder $sqlBuilder,
        CollectionFactory $productCollectionFactory,
        BrandUrlAliasFilter $brandUrlAliasFilter
    ) {
        $this->fieldEntityAttributesPool = $fieldEntityAttributesPool;
        $this->conditionsHelper = $conditionsHelper;
        $this->clauseFactory = $clauseFactory;
        $this->rule = $rule;
        $this->sqlBuilder = $sqlBuilder;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->brandUrlAliasFilter = $brandUrlAliasFilter;
    }
    /**
     * @param AstConverter $subject
     * @param callable $next
     * @param string $fieldName
     * @param array $arguments
     * @return array
     * @throws LocalizedException
     */
    public function aroundGetClausesFromAst(
        AstConverter $subject,
        callable $next,
        string $fieldName,
        array $arguments
    ): array {
        if (!array_key_exists('brand_url_alias', $arguments)) {
            return $next($fieldName, $arguments);
        }

        $conditionArgument = $arguments['brand_url_alias'];
        $conditionArgumentType = array_key_first($conditionArgument);

        if (!in_array($conditionArgumentType, ['eq', 'in'])) {
            throw new LocalizedException(__("'brand_url_alias' field only supports 'eq' condition type."));
        }

        if (!is_array($conditionArgument[$conditionArgumentType])) {
            $conditionArgument[$conditionArgumentType] = [$conditionArgument[$conditionArgumentType]];
        }

        $brands = $this->brandUrlAliasFilter->getBrandsByUrlAliases($conditionArgument[$conditionArgumentType]);

        unset($arguments['brand_url_alias']);

        $conditions = $next($fieldName, $arguments);
        if (empty($brands)) {
            $brands = [null];
        }

        array_push($conditions, $this->clauseFactory->create(
            'brand',
            'in',
            $brands
        ));


        return $conditions;
    }
}
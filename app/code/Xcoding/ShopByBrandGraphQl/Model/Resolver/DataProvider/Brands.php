<?php
declare(strict_types=1);

namespace Xcoding\ShopByBrandGraphQl\Model\Resolver\DataProvider;

use Amasty\ShopbyBase\Api\Data\OptionSettingInterface;
use Amasty\ShopbyBrand\Block\Widget\BrandListAbstract;
use Magento\Eav\Model\Entity\Attribute\Option;

class Brands extends BrandListAbstract
{
    const CONFIG_VALUES_PATH = 'amshopby_brand/brands_landing';

    /**
     * @param Option $option
     * @param OptionSettingInterface $setting
     * @return array
     */
    protected function getItemData(Option $option, OptionSettingInterface $setting)
    {
        /** @var \Xcoding\ShopbyBrand\Model\OptionSetting $setting */
        $data = $setting->getData();
        $data['slider_image'] = $setting->getSliderImageUrl(true);
        $data['image_url'] = $setting->getImageUrl();
        $data['tablet_image_url'] = $setting->getTabletImageUrl();
        $data['mobile_image_url'] = $setting->getMobileImageUrl();
        $data['bottom_video_url'] = $setting->getBottomVideoUrl();
        $data['bottom_image_url'] = $setting->getBottomImageUrl();
        $data['measure_men_img_url'] = $setting->getCustomImageUrl('measure_men_img');
        $data['measure_women_img_url'] = $setting->getCustomImageUrl('measure_women_img');
        $data['measure_junior_img_url'] = $setting->getCustomImageUrl('measure_junior_img');
        $data['measure_unisex_img_url'] = $setting->getCustomImageUrl('measure_unisex_img');
        $data['box_1_image_url'] = $setting->getCustomImageUrl('box_1_image');
        $data['box_2_image_url'] = $setting->getCustomImageUrl('box_2_image');
        $data['box_3_image_url'] = $setting->getCustomImageUrl('box_3_image');
        foreach (['title', 'meta_title'] as $key) {
            if (!$data[$key]) {
                $data[$key] = $option->getLabel();
            }
        }
        if (!$data['meta_description']) {
            $data['meta_description'] = $data['meta_title'];
        }
        $data['model'] = $setting;
        return $data;
    }

    /**
     * @return string
     */
    protected function getConfigValuesPath()
    {
       return self::CONFIG_VALUES_PATH;
    }
}

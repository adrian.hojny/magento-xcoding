<?php
declare(strict_types=1);

namespace Xcoding\ShopByBrandGraphQl\Model\Resolver\Product;

use Amasty\ShopbyBase\Helper\OptionSetting as SettingHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Widget\Model\Template\FilterEmulate;
use Xcoding\ShopByBrandGraphQl\Model\Resolver\DataProvider\Brands as BrandsDataProvider;

class BrandDetails implements ResolverInterface
{
    /** @var SettingHelper */
    protected $settingHelper;

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /** @var BrandsDataProvider */
    protected $brandsDataProvider;

    /** @var FilterEmulate */
    private $widgetFilter;

    /**
     * @param SettingHelper $settingHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param BrandsDataProvider $brandsDataProvider
     * @param FilterEmulate $widgetFilter
     */
    public function __construct(
        SettingHelper $settingHelper,
        ScopeConfigInterface $scopeConfig,
        BrandsDataProvider $brandsDataProvider,
        FilterEmulate $widgetFilter
    ) {
        $this->settingHelper = $settingHelper;
        $this->scopeConfig = $scopeConfig;
        $this->brandsDataProvider = $brandsDataProvider;
        $this->widgetFilter = $widgetFilter;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }
        /** @var \Magento\Catalog\Model\Product $product*/
        $product = $value['model'];

        if ($optionId = (int)$product->getBrand()) {
            /** @var \Xcoding\ShopbyBrand\Model\OptionSetting $brand */
            $brand = $this->settingHelper->getSettingByValue($optionId, 'attr_brand', $product->getStoreId());
            $result = $brand->getData();
            if ($result['description'] ?? null) {
                $result['description'] = $this->widgetFilter->filterDirective($result['description']);
            }
            if ($result['s3_html'] ?? null) {
                $result['s3_html'] = $this->widgetFilter->filterDirective($result['s3_html']);
            }
            $result['model'] = $brand;
            $result['url_key'] = $this->scopeConfig->getValue('amshopby_brand/general/url_key', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            return $result;
        }
        return null;
    }
}


<?php

declare(strict_types=1);

namespace Xcoding\ShopByBrandGraphQl\Model\Resolver\Products\SearchCriteria\CollectionProcessor\FilterProcessor;

use Amasty\ShopbyBase\Helper\FilterSetting;
use Amasty\ShopbyBase\Model\ResourceModel\OptionSetting\Collection as OptionSettingsCollection;
use Amasty\ShopbyBase\Model\ResourceModel\OptionSetting\CollectionFactory as OptionSettingsCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor\FilterProcessor\CustomFilterInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Api\Filter;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Amasty\ShopbyBrand\Helper\Data as DataHelper;

class BrandUrlAliasFilter implements CustomFilterInterface
{
    /**
     * @var OptionSettingsCollectionFactory
     */
    protected $optionsSettingsCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var DataHelper
     */
    protected $helper;

    /**
     * BrandUrlAliasFilter constructor.
     * @param OptionSettingsCollectionFactory $optionsSettingsCollectionFactory
     * @param StoreManagerInterface $storeManager
     * @param DataHelper $helper
     */
    public function __construct(
        OptionSettingsCollectionFactory $optionsSettingsCollectionFactory,
        StoreManagerInterface $storeManager,
        DataHelper $helper
    ) {
        $this->optionsSettingsCollectionFactory = $optionsSettingsCollectionFactory;
        $this->storeManager = $storeManager;
        $this->helper = $helper;
    }

    /**
     * @param Filter $filter
     * @param AbstractDb $collection
     * @return bool
     * @throws NoSuchEntityException
     */
    public function apply(Filter $filter, AbstractDb $collection)
    {
        $attributeValue = is_array($filter->getValue()) ? $filter->getValue() : [$filter->getValue()];
        $brandValue = $this->getBrandsByUrlAliases($attributeValue);
        $collection->addFieldToFilter('brand', ['in' => $brandValue]);
        return true;
    }

    /**
     * @param array $attributeValue
     * @return array
     * @throws NoSuchEntityException
     */
    public function getBrandsByUrlAliases(array $attributeValue)
    {
        $filterCode = FilterSetting::ATTR_PREFIX .
            $this->helper->getBrandAttributeCode();

        $stores = [0,  $this->storeManager->getStore()->getId()];

        /** @var OptionSettingsCollection $collection */
        $collection = $this->optionsSettingsCollectionFactory->create()
            ->addFieldToFilter('store_id', $stores)
            ->addFieldToFilter('filter_code', $filterCode)
            ->addFieldToFilter('url_alias', ['in' => $attributeValue])
            ->addOrder('store_id', 'ASC'); //current store values will rewrite defaults

        return array_unique($collection->getColumnValues('value'));
    }
}

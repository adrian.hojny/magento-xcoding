<?php
declare(strict_types=1);

namespace Xcoding\ShopByBrandGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Widget\Model\Template\FilterEmulate;

class Brands implements ResolverInterface
{
    /** @var DataProvider\Brands $brandsDataProvider */
    private $brandsDataProvider;

    /** @var FilterEmulate */
    private $widgetFilter;

    /**
     * @param DataProvider\Brands $brandsDataProvider
     * @param FilterEmulate $widgetFilter
     */
    public function __construct(
        DataProvider\Brands $brandsDataProvider,
        FilterEmulate $widgetFilter
    ) {
        $this->brandsDataProvider = $brandsDataProvider;
        $this->widgetFilter = $widgetFilter;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $onlyFeaturedFilter = $args['only_featured'] ?? false;
        $urlAliasFilter = $args['url_alias'] ?? false;

        $brands = $this->brandsDataProvider->getItems();
        return $this->filterBrands($brands, $urlAliasFilter, $onlyFeaturedFilter);
    }

    /**
     * @param array $brands
     * @param $urlAliasFilter
     * @param $onlyFeaturedFilter
     * @return array
     */
    protected function filterBrands(array $brands, $urlAliasFilter, $onlyFeaturedFilter): array
    {
        $filteredBrands = [];

        foreach ($brands as $brand) {
            $meetsUrlFilter = !$urlAliasFilter || $brand['url_alias'] === $urlAliasFilter;
            $meetsFeaturedFilter = !$onlyFeaturedFilter || (bool)$brand['is_featured'] === $onlyFeaturedFilter;
            if ($meetsUrlFilter && $meetsFeaturedFilter) {
                if ($brand['description'] ?? null) {
                    $brand['description'] = $this->widgetFilter->filterDirective($brand['description']);
                }
                if ($brand['s3_html'] ?? null) {
                    $brand['s3_html'] = $this->widgetFilter->filterDirective($brand['s3_html']);
                }
                $filteredBrands[] = $brand;
            }
        }

        return $filteredBrands;
    }
}


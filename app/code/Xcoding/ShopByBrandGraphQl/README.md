# Xcoding_ShopByBrandGraphQl

Moduł rozszerzający `Amasty_ShopByBrand` o GraphQl

Zarządzanie markami odbywa się w panelu administracyjnym:

`store > configuration > amasty extensions > Improved Layered Navigation: Brands` (ustawiamy `Brand attribute: brand`)

Dodawanie marek:

`Amasty > Improved Layered Navigation: Brands > Brand Management`

#### Pobieranie listy marek

Przykładowe zapytanie pobierające wszystkie dostępne w module atrybuty:
```graphql
query {
  brands {
    description
    image_url
    is_featured
    meta_description
    meta_keywords
    meta_title
    short_description
    small_image_alt
    title
    url_alias
  }
}
```

przykładowa odpowierdź:

```json
{
  "data": {
    "brands": [
      {
        "description": "descritpion",
        "image_url": "https:\/\/dev-3.ftg.dev.x-coding.pl\/media\/amasty\/shopby\/option_images\/dummy3.jpeg",
        "is_featured": true,
        "meta_description": "Meta description",
        "meta_keywords": "meta,kewords,example",
        "meta_title": "Meta title",
        "short_description": "short desc example",
        "small_image_alt": "alt example",
        "title": "Title 2",
        "url_alias": "url2.html"
      },
      {
        "description": "descritpion",
        "image_url": "https:\/\/dev-3.ftg.dev.x-coding.pl\/media\/amasty\/shopby\/option_images\/dummy.png",
        "is_featured": false,
        "meta_description": "Meta description",
        "meta_keywords": "meta,kewords,example",
        "meta_title": "Meta title",
        "short_description": "short desc example",
        "small_image_alt": "alt example",
        "title": "Title 3",
        "url_alias": "url3.html"
      }
    ]
  }
}
```

Listę marek możemy ograniczyć za pomocą filtrów `only_featured` oraz `url_alias`, np:
```graphql
query {
  brands (url_alias: "url2.html") {
    title
    url_alias
  }
}
```

Odpowiedź:
```js
{
  "data": {
    "brands": [
      {
        "title": "Title 2",
        "url_alias": "url2.html"
      }
    ]
  }
}
```


#### Rozszerzenie query `products`

Informacje o marce produktu zawierają się w polu `brand_details` w `ProductInterface` (odpowiedź z query `products`).

Dodatkowo listę produktów możemy przefiltrować po `url_alias` marki. 

Przykładowe zapytanie z filtrowaniem:

```graphql
query {
  products(
    pageSize: 10,
    currentPage: 1,
    filter: {
      brand_url_alias: { 
        eq: "url2.html"
      }
    }
  ) {
    items {
      sku
      name
      
      brand_details {
        description
        bottow_cms_block_id
        image_url
        bottow_cms_block_id
        is_featured
        meta_description
        meta_keywords
        meta_title
        short_description
        url_alias
      }
    }
  }
}
```

Przykładowa odpowiedź:
```json
{
  "data": {
    "products": {
      "items": [
        {
          "sku": "n31091479",
          "name": "People of Shibuya Transitional Jacket",
          "brand_details": {
            "description": "descritpion",
            "bottow_cms_block_id": null,
            "image_url": null,
            "is_featured": true,
            "meta_description": "Meta description",
            "meta_keywords": "meta,kewords,example",
            "meta_title": "Meta title",
            "short_description": "short desc example",
            "url_alias": "url2.html"
          }
        }
      ]
    }
  }
}
```


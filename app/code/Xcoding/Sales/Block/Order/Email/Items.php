<?php
namespace Xcoding\Sales\Block\Order\Email;

/**
 * @method \Magento\Sales\Model\Order getOrder()
 */

class Items extends \Magento\Sales\Block\Items\AbstractItems
{
    /**
     * @param \Magento\Sales\Model\Order\Item $item
     * 
     * @return array
     */
    public function getItemOptions($item)
    {
        $result = [];
        if ($options = $item->getProductOptions()) {
            if (isset($options['options'])) {
                $result = array_merge($result, $options['options']);
            }
            if (isset($options['additional_options'])) {
                $result = array_merge($result, $options['additional_options']);
            }
            if (isset($options['attributes_info'])) {
                $result = array_merge($result, $options['attributes_info']);
            }
        }

        return $result;
    }
}

<?php

declare(strict_types=1);

namespace Xcoding\Sales\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Xcoding\Inpost\Helper\Data as InPostHelper;
use Xcoding\InStorePickup\Helper\Data as InStorePickupHelper;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;

class EmailOrderSetTemplateVars implements ObserverInterface
{
    /**
     * @var InPostHelper
     */
    protected $inPostHelper;

    /**
     * @var InStorePickupHelper
     */
    protected $inStorePickupHelper;

    /**
     * EmailOrderSetTemplateVars constructor.
     * @param InPostHelper $inPostHelper
     * @param InStorePickupHelper $inStorePickupHelper
     */
    public function __construct(
        InPostHelper $inPostHelper,
        InStorePickupHelper $inStorePickupHelper
    ) {
        $this->inPostHelper = $inPostHelper;
        $this->inStorePickupHelper = $inStorePickupHelper;
    }

    public function execute(Observer $observer)
    {
        $transportObject = $observer->getData('transportObject');
        $order = $transportObject->getOrder();

        $transportObject->setData(
            'inpost_machine_info',
            $this->getInpostMachineData($order)
        );

        $transportObject->setData(
            'instorepickup_info',
            $this->getInstorePickupPointData($order)
        );
    }

    /**
     * @param Order $order
     * @return string|null
     */
    protected function getInpostMachineData(Order $order){
        if ($order->getInpostMachineId()) {
            $inpostMachineId = __('InPost pickup point') . ': ' . $order->getInpostMachineId();
            if ($order->getInpostMachineInfo()) {
                $inpostMachineData = $this->inPostHelper->renderMachineAddress($order->getInpostMachineInfo());
            } else {
                $inpostMachineData = null;
            }

            return '<strong>' . $inpostMachineId . '</strong><br>' . $inpostMachineData;
        }

        return null;
    }

    /**
     * @param Order $order
     * @return string|null
     */
    protected function getInstorePickupPointData(Order $order) {
        if ($order->getInstorepickupStoreCode()) {
            $instorePickupPointCode =  __('Pickup point') . ': ' . $order->getInstorepickupStoreCode();
            if ($order->getInstorepickupStoreInfo()) {
                $instorePickupPointData = $this->inStorePickupHelper->renderPickupAddress($order->getInstorepickupStoreInfo());
            } else {
                $instorePickupPointData = null;
            }

            return '<strong>' . $instorePickupPointCode . '</strong><br>' . $instorePickupPointData;
        }

        return null;
    }
}
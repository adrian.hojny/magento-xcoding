# Xcoding_ReviewsGraphQlExtended

Moduł rozszerzający bazową funkcjonalność modułu `ScandiPWA_ReviewsGraphQl`

#### Dodatkowe atrybuty review

Nowe atrybuty dodajemy przez `extended_rating_data` w `productReviewItem` np.
```graphql
    extended_rating_data: {
      comfort_value: NOT_COMFORTABLE
      quality_value: QUALITY_OK
      size_value: SIZE_OK
      recommended_value: NOT_RECOMMENDED
    }
```

Możliwe wartości są zdefiniowane w poszczególnych enumach, np. ComfortValueEnum, SizeValueEnum itd.
W obecnej wersji nie istnieje wartość do wyświetlenia dla klienta dla poszczególnych wartosci (np. przez atrybut "label"). 

Wszystkie wartości do wyświetlenia zostały dodane do tyłumaczeń (np. `__('NOT_COMFORTABLE') => 'Not comfortable'`)

#### Agregowane dane dla review

Nowe atrybuty znajdują się w `extended_rating_summary` dla `review_summary`.

```graphql
review_summary {
    rating_summary
    review_count
    extended_rating_summary {
        code
        value
        value_count
        product_id
    }
}
```

Przykładowa odpowiedź:

```graphql
"extended_rating_summary": [
    {
        "code": "comfort_value",
        "value": "COMFORTABLE",
        "value_count": 3,
        "product_id": 1630
    },
    {
        "code": "comfort_value",
        "value": "NOT_COMFORTABLE",
        "value_count": 1,
        "product_id": 1630
    }
]
```
Jeśli wartość dla kombinacji atrybut-wartość zostanie zwrócona - oznacza 0 głosów.
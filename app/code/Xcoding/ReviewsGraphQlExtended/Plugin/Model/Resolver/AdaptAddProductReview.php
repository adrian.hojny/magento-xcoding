<?php declare(strict_types=1);

namespace Xcoding\ReviewsGraphQlExtended\Plugin\Model\Resolver;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\GroupedProduct\Model\Product\Type\Grouped;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\Review;
use Magento\Review\Model\ReviewFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use ScandiPWA\ReviewsGraphQl\Model\Resolver\AddProductReview;
use Xcoding\ReviewExtended\Api\Command\GetAvailableOptionsInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingInterfaceFactory;
use Xcoding\ReviewExtended\Api\ReviewRatingRepositoryInterface;

/**
 * Class AdaptAddProductReview
 * @package Xcoding\ReviewsGraphQlExtended\Plugin\Model\Resolver
 */
class AdaptAddProductReview
{
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var ReviewFactory
     */
    protected $reviewFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var RatingFactory
     */
    protected $ratingFactory;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var ReviewRatingRepositoryInterface
     */
    private $reviewRatingRepository;

    /**
     * @var GetAvailableOptionsInterface
     */
    private $getAvailableOptions;

    /**
     * @var ReviewRatingInterfaceFactory
     */
    private $reviewRatingFactory;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ProductInterface
     */
    private $product;

    /**
     * AddProductReview constructor.
     *
     * @param ProductRepositoryInterface $productRepository
     * @param ReviewFactory $reviewFactory
     * @param StoreManagerInterface $storeManager
     * @param RatingFactory $ratingFactory
     * @param CustomerSession $customerSession
     * @param ReviewRatingRepositoryInterface $reviewRatingRepository
     * @param GetAvailableOptionsInterface $getAvailableOptions
     * @param ReviewRatingInterfaceFactory $ratingReviewFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        ReviewFactory $reviewFactory,
        StoreManagerInterface $storeManager,
        RatingFactory $ratingFactory,
        CustomerSession $customerSession,
        ReviewRatingRepositoryInterface $reviewRatingRepository,
        GetAvailableOptionsInterface $getAvailableOptions,
        ReviewRatingInterfaceFactory $ratingReviewFactory,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->productRepository = $productRepository;
        $this->reviewFactory = $reviewFactory;
        $this->storeManager = $storeManager;
        $this->ratingFactory = $ratingFactory;
        $this->customerSession = $customerSession;
        $this->reviewRatingRepository = $reviewRatingRepository;
        $this->getAvailableOptions = $getAvailableOptions;
        $this->reviewRatingFactory = $ratingReviewFactory;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @inheritdoc
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     * @throws NoSuchEntityException
     */
    public function aroundResolve(
        AddProductReview $subject,
        callable $proceed,
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        return $this->resolve($field, $context, $info, $value, $args);
    }

    /**
     * @inheritdoc
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     * @throws NoSuchEntityException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($args['productReviewItem'])) {
            throw new GraphQlInputException(__('Review data is not valid'));
        }

        $productReviewItem = $args['productReviewItem'];
        $customerId = $context->getUserId();

        if ($customerId === 0) {
            throw new GraphQlInputException(__('Please login to add review'));
        } else {
            $this->customerSession->setCustomerId($customerId);
        }

        $this->product = $this->productRepository->get($productReviewItem['product_sku']);
        $productId = $this->product->getId();
        $productType = $this->product->getTypeId();
        $storeId = $this->storeManager->getStore()->getId();
        $reviewData = [
            'nickname' => $productReviewItem['nickname'],
            'title' => $productReviewItem['title'],
            'detail' => $productReviewItem['detail']
        ];

        try {
            /** @var Review $review */
            $review = $this->reviewFactory->create()->setData($reviewData);
            $review->unsetData('review_id');
            $validate = $review->validate();

            if ($validate === true) {
                $bought = $customerId ? $this->checkIfProductBought($customerId, (int)$productId, $productType) : false;
                $review->setEntityId($review->getEntityIdByCode(Review::ENTITY_PRODUCT_CODE))
                    ->setEntityPkValue($productId)
                    ->setStatusId(Review::STATUS_PENDING)
                    ->setProductBought($bought)
                    ->setCustomerId($customerId)
                    ->setStoreId($storeId)
                    ->setStores([$storeId])
                    ->save();

                if (isset($productReviewItem['rating_data'])) {
                    foreach ($productReviewItem['rating_data'] as $rating) {
                        $this->ratingFactory->create()
                            ->setRatingId($rating['rating_id'])
                            ->setReviewId($review->getId())
                            ->setCustomerId($customerId)
                            ->addOptionVote($rating['option_id'], $productId);
                    }
                }

                $this->addExtendedReviewRating($productReviewItem, $review->getId(), $productId, $customerId);

                $review->aggregate();
            }
        } catch (Exception $e) {
            throw new GraphQlNoSuchEntityException(__('We cannot post your review right now.'));
        }

        return $review->getData();
    }

    /**
     * @param $productReviewItem
     * @param $reviewId
     * @param $productId
     * @param $customerId
     * @throws LocalizedException
     */
    protected function addExtendedReviewRating($productReviewItem, $reviewId, $productId, $customerId)
    {
        $availableOptions = $this->getAvailableOptions->execute();
        if (isset($productReviewItem['extended_rating_data'])) {
            $data = $productReviewItem['extended_rating_data'];

            /** @var ReviewRatingInterface $reviewRating */
            $reviewRating = $this->reviewRatingFactory->create();
            $reviewRating->setCustomerId((int)$customerId)
                ->setProductId((int)$productId)
                ->setReviewId((int)$reviewId);

            foreach ($data as $code => $value) {
                if (isset($availableOptions[$code]) && in_array($value, $availableOptions[$code])) {
                    $reviewRating->setData($code, $value);
                }
            }

            $this->reviewRatingRepository->save($reviewRating);
        }
    }

    /**
     * @param $customerId
     * @param int|null $productId
     * @param $productType
     * @return bool
     */
    protected function checkIfProductBought($customerId, ?int $productId, $productType)
    {
        if (!$customerId || !$productId) {
            return false;
        }

        $productIds = [$productId];

        if ($productType === Configurable::TYPE_CODE) {
            $productConfigurationItems = $this->product->getTypeInstance()->getUsedProducts($this->product);
            foreach ($productConfigurationItems as $productConfigurationItem) {
                $productIds[] = $productConfigurationItem->getId();
            }
        } elseif ($productType === Grouped::TYPE_CODE) {
            $associatedProducts = $this->product->getTypeInstance()->getAssociatedProducts($this->product);
            foreach ($associatedProducts as $associatedProduct) {
                $productIds[] = $associatedProduct->getId();
            }
        }

        return $this->checkCustomerOrderedProducts($customerId, $productIds);
    }

    /**
     * @param int $customerId
     * @param array $productIds
     * @return bool
     */
    protected function checkCustomerOrderedProducts(int $customerId, array $productIds)
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter('customer_id', $customerId)->create();
        $customerOrders = $this->orderRepository->getList($searchCriteria);
        $ordered = false;

        foreach ($productIds as $productId) {
            foreach ($customerOrders as $customerOrder) {
                foreach ($customerOrder->getAllItems() as $orderedItem) {
                    if ($orderedItem->getProductId() == $productId) {
                        $ordered = true;
                        break;
                    }
                }
            }
        }

        return $ordered;
    }
}

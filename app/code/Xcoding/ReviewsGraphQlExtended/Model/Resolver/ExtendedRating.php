<?php declare(strict_types=1);

namespace Xcoding\ReviewsGraphQlExtended\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Xcoding\ReviewExtended\Api\ReviewRatingRepositoryInterface;

/**
 * Class ExtendedRating
 * @package Xcoding\ReviewsGraphQlExtended\Model\Resolver
 */
class ExtendedRating implements ResolverInterface
{
    /**
     * @var ReviewRatingRepositoryInterface
     */
    private $reviewRatingRepository;

    /**
     * ExtendedRating constructor.
     * @param ReviewRatingRepositoryInterface $reviewRatingRepository
     */
    public function __construct(
        ReviewRatingRepositoryInterface $reviewRatingRepository
    ) {
        $this->reviewRatingRepository = $reviewRatingRepository;
    }

    /**
     * @inheritdoc
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $reviewId = isset($value['review_id']) ? $value['review_id'] : null;
        if ($reviewId) {
            $reviewRating = $this->reviewRatingRepository->getByReviewId((int)$reviewId);

            return $reviewRating->getId() ? $reviewRating->getData() : null;
        }

        return null;
    }
}

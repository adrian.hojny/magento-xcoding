<?php declare(strict_types=1);

namespace Xcoding\ReviewsGraphQlExtended\Model\Resolver;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Xcoding\ReviewExtended\Api\ReviewRatingAggregatedRepositoryInterface;

/**
 * Class ExtendedRatingSummary
 * @package Xcoding\ReviewsGraphQlExtended\Model\Resolver
 */
class ExtendedRatingSummary implements ResolverInterface
{
    /**
     * @var ReviewRatingAggregatedRepositoryInterface
     */
    private $reviewRatingAggregatedRepository;

    /**
     * ExtendedRating constructor.
     * @param ReviewRatingAggregatedRepositoryInterface $reviewRatingAggregatedRepository
     */
    public function __construct(
        ReviewRatingAggregatedRepositoryInterface $reviewRatingAggregatedRepository
    ) {
        $this->reviewRatingAggregatedRepository = $reviewRatingAggregatedRepository;
    }

    /**
     * @inheritdoc
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        /** @var Product $product */
        $product = $value['model'];
        $ratingSummary = $product->getRatingSummary();

        $extendedSummary = [];
        $extendedRatingSummaryCollection = $this->reviewRatingAggregatedRepository->getListByProductId((int)$product->getId());
        foreach ($extendedRatingSummaryCollection as $extendedratingSummary) {
            $extendedSummary[] = $extendedratingSummary->getData();
        }

        if ($ratingSummary) {
            return [
                'rating_summary' => $ratingSummary->getRatingSummary(),
                'review_count' => $ratingSummary->getReviewsCount(),
                'extended_rating_summary' => $extendedSummary
            ];
        }

        return [
            'extended_rating_summary' => $extendedSummary
        ];
    }
}

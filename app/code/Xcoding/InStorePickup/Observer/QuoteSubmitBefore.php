<?php

namespace Xcoding\InStorePickup\Observer;

use Exception;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\HTTP\Client\Curl;
use Psr\Log\LoggerInterface;
use Xcoding\InStorePickup\Helper\Data as ModuleHelper;

class QuoteSubmitBefore  implements ObserverInterface
{
    const STORE_INFO_CONFIG_PATH = '';

    /**
     * @var Curl
     */
    protected $curl;

    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var ModuleHelper
     */
    private $moduleHelper;

    /**
     * QuoteSubmitBefore constructor.
     * @param Curl $curl
     * @param LoggerInterface $logger
     * @param ModuleHelper $moduleHelper
     */
    public function __construct(
        Curl $curl,
        LoggerInterface $logger,
        ModuleHelper $moduleHelper
    ) {

        $this->curl = $curl;
        $this->logger = $logger;
        $this->moduleHelper = $moduleHelper;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $quote = $observer->getQuote();
        $order = $observer->getOrder();

        $pickupStoreCode = $quote->getData('instorepickup_store_code');
        if ($pickupStoreCode) {
            try {
                $data = $this->moduleHelper->getAddressByCode($pickupStoreCode);
                $order->setData('instorepickup_store_code', $pickupStoreCode);
                $order->setData('instorepickup_store_info', $data);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
    }
}
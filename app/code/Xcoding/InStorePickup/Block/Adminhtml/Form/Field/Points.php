<?php

namespace Xcoding\InStorePickup\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class Points extends AbstractFieldArray
{
    /**
     * {@inheritdoc}
     */
    protected function _prepareToRender()
    {
        $this->addColumn('code', ['label' => __('Store code'), 'class' => 'required-entry', 'style' => 'width: 70px;']);
        $this->addColumn('address', ['label' => __('Address'), 'class' => 'required-entry', 'style' => 'width: 300px;']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add store');
    }
}
<?php declare(strict_types=1);

namespace Xcoding\InStorePickup\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    /**
     * @var JsonSerializer
     */
    protected $jsonSerializer;

    /**
     * Data constructor.
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param JsonSerializer $jsonSerializer
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        JsonSerializer $jsonSerializer
    )
    {
        parent::__construct($context);
        $this->jsonSerializer = $jsonSerializer;
        $this->scopeConfig = $scopeConfig;
    }

    public function renderPickupAddress($addressInfo)
    {
        return __('Odbiór osobisty pod adresem: <br>%1.', $addressInfo ?? '-');
    }

    public function getPickupPoints()
    {
        $points = $this->scopeConfig->getValue(
            'carriers/pickup/points',
            ScopeInterface::SCOPE_STORE
        );

        try {
            $mapping = $this->jsonSerializer->unserialize($points);
        } catch (Exception $e) {
            $mapping = [];
        }

        return $mapping;
    }

    public function getAddressByCode($storeCode)
    {
        $points = $this->getPickupPoints();
        foreach ($points as $point) {
            if ($point['code'] === $storeCode) {
                return $point['address'];
            }
        }

        return null;
    }
}

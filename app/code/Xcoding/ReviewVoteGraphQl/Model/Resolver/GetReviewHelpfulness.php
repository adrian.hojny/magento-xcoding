<?php

namespace Xcoding\ReviewVoteGraphQl\Model\Resolver;

use Exception;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Xcoding\ReviewVote\Model\Command\GetReviewHelpfulness as GetReviewHelpfulnessCommand;

/**
 * Class GetReviewVoteHelpfulness
 * @package Xcoding\ReviewVoteGraphQl\Model\Resolver
 */
class GetReviewHelpfulness implements ResolverInterface
{
    /**
     * @var GetReviewHelpfulnessCommand
     */
    private $getReviewHelpfulness;

    /**
     * GetReviewHelpfulness constructor.
     * @param GetReviewHelpfulnessCommand $getReviewHelpfulness
     */
    public function __construct(
        GetReviewHelpfulnessCommand $getReviewHelpfulness
    ) {
        $this->getReviewHelpfulness = $getReviewHelpfulness;
    }

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed|Value
     * @throws Exception
     */public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $reviewId = isset($value['review_id']) ? $value['review_id'] : null;
        if ($reviewId) {
            return $this->getReviewHelpfulness->execute((int)$reviewId);
        }

        return [];
    }
}

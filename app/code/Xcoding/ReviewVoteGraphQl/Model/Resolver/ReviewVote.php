<?php

namespace Xcoding\ReviewVoteGraphQl\Model\Resolver;

use Exception;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Xcoding\ReviewVote\Api\ReviewVoteRepositoryInterface;
use Xcoding\ReviewVote\Model\Command\GetReviewHelpfulness as GetReviewHelpfulnessCommand;

/**
 * Class ReviewVote
 * @package Xcoding\ReviewsGraphQlExtended\Model\Resolver
 */
class ReviewVote implements ResolverInterface
{
    /**
     * @var ReviewVoteRepositoryInterface
     */
    private $reviewVoteRepository;
    /**
     * @var GetReviewHelpfulnessCommand
     */
    private $getReviewHelpfulness;

    /**
     * ReviewVote constructor.
     * @param ReviewVoteRepositoryInterface $reviewVoteRepository
     * @param GetReviewHelpfulnessCommand $getReviewHelpfulness
     */
    public function __construct(
        ReviewVoteRepositoryInterface $reviewVoteRepository,
        GetReviewHelpfulnessCommand $getReviewHelpfulness
    ) {
        $this->reviewVoteRepository = $reviewVoteRepository;
        $this->getReviewHelpfulness = $getReviewHelpfulness;
    }

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed|Value
     * @throws Exception
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $customerId = $context->getUserId();
        if (!$customerId) {
            throw new GraphQlAuthorizationException(__('The current user cannot perform operations on review'));
        }

        $reviewId = $args['reviewId'];
        $voteType = $args['vote'];
        if ($reviewId !== null) {
            if ($this->reviewVoteRepository->vote($voteType, $customerId, $reviewId)) {
                return $this->getReviewHelpfulness->execute($reviewId);
            }
        }

        return [];
    }
}

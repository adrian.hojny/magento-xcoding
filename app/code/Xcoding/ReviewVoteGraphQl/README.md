# Xcoding_ReviewVoteGraphQl

Moduł rozszerzający `ReviewVote` GraphQl

#### Głosowanie na review

Głosowanie obdywa się przez nową mutację - `addReviewHelpfulnessVote`

Przykładowe zapytanie (z pobieraniem nowej liczby głosów na dane review):
```graphql
mutation{
  addReviewHelpfulnessVote(vote: VOTE_DOWN, reviewId: 21) {
   yes
   no
  }
}
```

przykładowa odpowierdź:

```graphql
{
  "data": {
    "addReviewHelpfulnessVote": {
      "yes": 0,
      "no": 1
    }
  }
}
```
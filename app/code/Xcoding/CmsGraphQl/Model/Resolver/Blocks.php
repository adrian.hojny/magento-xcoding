<?php
declare(strict_types=1);

namespace Xcoding\CmsGraphQl\Model\Resolver;

use Xcoding\CmsGraphQl\Model\Resolver\DataProvider\Block as BlockDataProvider;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * CMS blocks field resolver, used for GraphQL request processing
 * Rewrited to fix problem with getting block by store_id
 * @see \Magento\CmsGraphQl\Model\Resolver\Blocks
 */
class Blocks implements ResolverInterface
{
    /**
     * @var BlockDataProvider
     */
    private $blockDataProvider;

    /**
     * @param BlockDataProvider $blockDataProvider
     */
    public function __construct(
        BlockDataProvider $blockDataProvider
    ) {
        $this->blockDataProvider = $blockDataProvider;
    }

    /**
     * @inheritdoc
     * @param \Magento\GraphQl\Model\Query\Context $context
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $blockIdentifiers = $this->getBlockIdentifiers($args);
        $storeId = (int) $context->getExtensionAttributes()->getStore()->getId();
        $blocksData = $this->getBlocksData($blockIdentifiers, $storeId);

        return [
            'items' => $blocksData,
        ];
    }

    /**
     * Get block identifiers
     *
     * @param array $args
     * @return string[]
     * @throws GraphQlInputException
     */
    private function getBlockIdentifiers(array $args): array
    {
        if (!isset($args['identifiers']) || !is_array($args['identifiers']) || count($args['identifiers']) === 0) {
            throw new GraphQlInputException(__('"identifiers" of CMS blocks should be specified'));
        }

        return $args['identifiers'];
    }

    /**
     * Get blocks data
     *
     * @param array $identifiers
     * @param int   $storeId
     * @return array
     * @throws GraphQlNoSuchEntityException
     */
    private function getBlocksData(array $identifiers, $storeId): array
    {
        $blocksData = [];
        foreach ($identifiers as $identifier) {
            try {
                $blocksData[$identifier] = $this->blockDataProvider->getDataByIdentifier($identifier, $storeId);
            } catch (NoSuchEntityException $e) {
                continue;
            }
        }
        return $blocksData;
    }
}

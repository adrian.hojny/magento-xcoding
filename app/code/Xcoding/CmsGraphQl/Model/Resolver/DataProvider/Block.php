<?php
declare(strict_types=1);

namespace Xcoding\CmsGraphQl\Model\Resolver\DataProvider;

use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Api\Data\BlockInterface;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\ResourceModel\Block as BlockResource;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Widget\Model\Template\FilterEmulate;

/**
 * Cms block data provider
 * Rewrited to fix problem with getting block by store_id
 */
class Block extends \Magento\CmsGraphQl\Model\Resolver\DataProvider\Block
{
    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var BlockResource
     */
    private $blockResource;

    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;

    /**
     * @var FilterEmulate
     */
    private $widgetFilter;

    /**
     * @param BlockFactory $blockFactory
     * @param BlockResource $blockResource
     * @param BlockRepositoryInterface $blockRepository
     * @param FilterEmulate $widgetFilter
     */
    public function __construct(
        BlockFactory $blockFactory,
        BlockResource $blockResource,
        BlockRepositoryInterface $blockRepository,
        FilterEmulate $widgetFilter
    ) {
        $this->blockFactory = $blockFactory;
        $this->blockResource = $blockResource;
        $this->blockRepository = $blockRepository;
        $this->widgetFilter = $widgetFilter;
    }

    /**
     * Get block data
     *
     * @param string $blockIdentifier
     * @return array
     */
    public function getData(string $blockIdentifier): array
    {
        $block = $this->blockRepository->getById($blockIdentifier);
        return $this->convertBlockData($block);
    }

    /**
     * Get block data
     *
     * @param string $identifier
     * @param int    $storeId
     * @return array
     */
    public function getDataByIdentifier(string $identifier, $storeId = null): array
    {
        $block = $this->blockFactory->create();
        $block->setStoreId($storeId);
        $this->blockResource->load($block, $identifier, BlockInterface::IDENTIFIER);
        return $this->convertBlockData($block);
    }

    /**
     * Convert page data
     *
     * @param PageInterface $page
     * @return array
     * @throws NoSuchEntityException
     */
    private function convertBlockData($block)
    {
        if (false === $block->isActive()) {
            throw new NoSuchEntityException();
        }
        $renderedContent = $this->widgetFilter->filterDirective($block->getContent());

        return [
            BlockInterface::BLOCK_ID => $block->getId(),
            BlockInterface::IDENTIFIER => $block->getIdentifier(),
            BlockInterface::TITLE => $block->getTitle(),
            BlockInterface::CONTENT => $renderedContent,
        ];
    }
}

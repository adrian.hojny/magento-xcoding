# Xcoding_CmsGraphQl

Moduł rozszerza graphql stron i bloków statycznych

#### Modyfikatory stron

Dodano pole `scandipwa_modifiers`.

Przykładowe zapytanie:

```graphql
query {
  cmsPage (identifier: "privacy-policy-cookie-restriction-mode" ) {
    title
    scandipwa_modifiers
  }
}
```

Przykładowa odpowiedź:
```json
{
  "data": {
    "cmsPage": {
      "title": "Privacy and Cookie Policy",
      "scandipwa_modifiers": [
        "hide-breadcrumbs",
        "hide-page-title",
        "transparent-header"
      ]
    }
  }
}
```
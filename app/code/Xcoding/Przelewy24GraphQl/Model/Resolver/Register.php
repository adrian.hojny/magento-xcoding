<?php

namespace Xcoding\Przelewy24GraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\UrlInterface;
use Magento\Quote\Api\CartManagementInterface;
use Magento\QuoteGraphQl\Model\Cart\CheckCartCheckoutAllowance;
use Magento\QuoteGraphQl\Model\Cart\GetCartForUser;
use Magento\Store\Model\StoreManagerInterface;
use Xcoding\Przelewy24\Helper\Request as RequestHelper;

class Register implements ResolverInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var RequestHelper
     */
    protected $requestHelper;

    /**
     * @var CartManagementInterface
     */
    private $cartManagement;

    /**
     * @var GetCartForUser
     */
    private $getCartForUser;

    /**
     * @var CheckCartCheckoutAllowance
     */
    private $checkCartCheckoutAllowance;

    /**
     * @param RequestHelper $requestHelper
     * @param GetCartForUser $getCartForUser
     * @param CartManagementInterface $cartManagement
     * @param CheckCartCheckoutAllowance $checkCartCheckoutAllowance
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        RequestHelper $requestHelper,
        GetCartForUser $getCartForUser,
        CartManagementInterface $cartManagement,
        CheckCartCheckoutAllowance $checkCartCheckoutAllowance
    ) {
        $this->storeManager = $storeManager;
        $this->requestHelper = $requestHelper;
        $this->getCartForUser = $getCartForUser;
        $this->cartManagement = $cartManagement;
        $this->checkCartCheckoutAllowance = $checkCartCheckoutAllowance;
    }

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed|Value
     * @throws \Exception
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $guestCartId = $args['guestCartId'] ?? '';

        $customerId = $context->getUserId();
        $storeId = (int)$context->getExtensionAttributes()->getStore()->getId();

        if ($guestCartId !== '') {
            $quote = $this->getCartForUser->execute($guestCartId, $customerId, $storeId);
        } else {
            $quote = $this->cartManagement->getCartForCustomer($customerId);
        }
        /** @var \Magento\Quote\Model\Quote $quote */

        $this->checkCartCheckoutAllowance->execute($quote);

        $configHelper = $this->requestHelper->getConfigHelper();
        $merchantId = $configHelper->getValue('merchant_id');
        $baseUrl = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB);
        $amount = (int) ($quote->getGrandTotal() * 100);
        $currencyCode = $quote->getOrderCurrencyCode();
        $currencyCode = 'PLN'; // TODO remove
        $payment = $quote->getPayment();
        $params = [
            'merchantId' => $merchantId,
            'posId' => $merchantId,
            'sessionId' => $quote->getId(),
            'amount' => $amount,
            'currency' => $currencyCode,
            'description' => $quote->getId(),
            'email' => $quote->getCustomerEmail(),
            'client' => $quote->getCustomerFirstname(),
            'country' => $quote->getBillingAddress()->getCountryId(),
            'language' => 'pl',
            'regulationAccept' => true,
            'urlStatus' => $baseUrl . 'rest/V1/przelewy24/status',
            'urlReturn' => $baseUrl . 'checkout/success',
        ];
        $json = json_encode([
            'sessionId' => $quote->getId(),
            'merchantId' => (int) $merchantId,
            'amount' => (int) $amount,
            'currency' => $currencyCode,
            'crc' => $configHelper->getValue('salt'),
        ], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $hash = hash('sha384', $json);
        $params['sign'] = $hash;

        if ($method = $payment->getAdditionalInformation('method') ?? null) {
            $params['method'] = $method;
        }
        try {
            $response = $this->requestHelper->makeRequest('post', 'transaction/register', $params);
            $response = json_decode($response, true);
            $token = $response['data']['token'] ?? null;
            if (!$token) {
                return;
            }
            return [
                'token' => $token,
                'sign' => $hash,
                'url' => $this->requestHelper->getPrzelewy24Class()->getHost(),
            ];
        } catch (\Exception $e) {
            throw new GraphQlInputException(__('Unable to register transaction'), $e);
        }
    }
}
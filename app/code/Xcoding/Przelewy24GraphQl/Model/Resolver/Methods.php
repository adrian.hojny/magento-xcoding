<?php

namespace Xcoding\Przelewy24GraphQl\Model\Resolver;

use Magento\Framework\App\CacheInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Xcoding\Przelewy24\Helper\Request as RequestHelper;

class Methods implements ResolverInterface
{
    const CACHE_KEY = 'xcoding_przelewy24_methods_';
    const CACHE_LIFETIME = 10;

    /**
     * @var \Magento\Framework\App\Cache
     */
    protected $cache;

    /**
     * @var RequestHelper
     */
    protected $requestHelper;

    /**
     * @param RequestHelper $requestHelper
     */
    public function __construct(
        CacheInterface $cache,
        RequestHelper $requestHelper
    ) {
        $this->cache = $cache;
        $this->requestHelper = $requestHelper;
    }

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed|Value
     * @throws \Exception
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $lang = ($args['lang'] == 'pl') ? 'pl' : 'en';
        $cacheKey  = self::CACHE_KEY . $lang;
        $data = unserialize($this->cache->load($cacheKey));
        if ($data) {
            return $data;
        }
        $response = $this->requestHelper->makeRequest('get', "payment/methods/$lang");
        $response = json_decode($response, true);
        if (isset($response['error'])) {
            throw new LocalizedException(__($response['error']));
        }
        $methods = $response['data'];
        $result = [];
        foreach ($methods as $method) {
            $result[] = $method;
        }
        $this->cache->save(serialize($result), $cacheKey, [], self::CACHE_LIFETIME);
        return $result;
    }
}

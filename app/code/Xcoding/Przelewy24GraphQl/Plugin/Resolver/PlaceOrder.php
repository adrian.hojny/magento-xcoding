<?php

namespace Xcoding\Przelewy24GraphQl\Plugin\Resolver;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class PlaceOrder
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    public function afterResolve(
       $subject,
        $result
    ) {
        $order = $result['order']['model'];
        $paymentInfo = $order->getPayment()->getAdditionalInformation();
        if (isset($paymentInfo['payment_redirect_url'])) {
            $result['order']['payment_redirect_url'] = $paymentInfo['payment_redirect_url'];
        }

        return $result;
    }

    /**
     * @param $incrementId
     * @return OrderInterface[]
     */
    protected function getOrderByNumber($incrementId) {
        $this->searchCriteriaBuilder->addFilter('increment_id', $incrementId);

        $order = $this->orderRepository->getList(
            $this->searchCriteriaBuilder->create()
        );

        return $order->getItems();
    }
}
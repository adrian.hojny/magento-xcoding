<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Api;

use Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface;

/**
 * Interface ReviewRatingAggregatedRepositoryInterface
 * @package Xcoding\ReviewExtended\Api
 */
interface ReviewRatingAggregatedRepositoryInterface
{
    /**
     * @param int $productId
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface[]
     */
    public function getListByProductId(int $productId): array;

    /**
     * @param \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface $reviewRatingAggregated
     * @return bool
     */
    public function delete(ReviewRatingAggregatedInterface $reviewRatingAggregated): bool;

    /**
     * @param int $productId
     * @return bool
     */
    public function deleteByProductId(int $productId): bool;
}

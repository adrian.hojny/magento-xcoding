<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingSearchResultsInterface;

/**
 * Interface ReviewRatingRepositoryInterface
 * @package Xcoding\ReviewExtended\Api
 */
interface ReviewRatingRepositoryInterface
{
    /**
     * Save review_rating
     * @param \Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface $reviewRating
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(ReviewRatingInterface $reviewRating): ReviewRatingInterface;

    /**
     * Retrieve review_rating
     * @param int $id
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get(int $id): ReviewRatingInterface;

    /**
     * Retrieve review_rating matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface|null $searchCriteria
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): ReviewRatingSearchResultsInterface;

    /**
     * @param int $reviewId
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface
     */
    public function getByReviewId(int $reviewId): ReviewRatingInterface;

    /**
     * Delete review_rating by ID
     * @param int $reviewRatingId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById(int $reviewRatingId): bool;
}

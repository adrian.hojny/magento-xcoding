<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface ReviewRatingInterface
 * @package Xcoding\ReviewExtended\Api\Data
 */
interface ReviewRatingInterface extends ExtensibleDataInterface
{
    /**#@+
     * @var string
     */
    const ID = 'id';
    const CUSTOMER_ID = 'customer_id';
    const PRODUCT_ID = 'product_id';
    const REVIEW_ID = 'review_id';
    const SIZE_VALUE = 'size_value';
    const RECOMMENDED_VALUE = 'recommended_value';
    const QUALITY_VALUE = 'quality_value';
    const COMFORT_VALUE = 'comfort_value';
    /**#@-*/

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param int|null $id
     * @return ReviewRatingInterface
     */
    public function setId(?int $id): ReviewRatingInterface;

    /**
     * Get customer_id
     * @return int|null
     */
    public function getCustomerId(): ?int;

    /**
     * Set customer_id
     * @param int|null $customerId
     * @return ReviewRatingInterface
     */
    public function setCustomerId(?int $customerId): ReviewRatingInterface;

    /**
     * Get product_id
     * @return int|null
     */
    public function getProductId(): ?int;

    /**
     * Set product_id
     * @param int|null $productId
     * @return ReviewRatingInterface
     */
    public function setProductId(?int $productId): ReviewRatingInterface;

    /**
     * Get size_value
     * @return string|null
     */
    public function getSizeValue(): ?string;

    /**
     * Set size_value
     * @param string|null $sizeValue
     * @return ReviewRatingInterface
     */
    public function setSizeValue(?string $sizeValue): ReviewRatingInterface;

    /**
     * Get quality_value
     * @return string|null
     */
    public function getQualityValue(): ?string;

    /**
     * Set quality_value
     * @param string|null $qualityValue
     * @return ReviewRatingInterface
     */
    public function setQualityValue(?string $qualityValue): ReviewRatingInterface;

    /**
     * Get comfort_value
     * @return string|null
     */
    public function getComfortValue(): ?string;

    /**
     * Set comfort_value
     * @param string|null $comfortValue
     * @return ReviewRatingInterface
     */
    public function setComfortValue(?string $comfortValue): ReviewRatingInterface;

    /**
     * Get review_id
     * @return int|null
     */
    public function getReviewId(): ?int;

    /**
     * Set review_id
     * @param int|null $reviewId
     * @return ReviewRatingInterface
     */
    public function setReviewId(?int $reviewId): ReviewRatingInterface;

    /**
     * Get recommended_value
     * @return string|null
     */
    public function getRecommendedValue(): ?string;

    /**
     * Set recommended_value
     * @param string|null $recommendedValue
     * @return ReviewRatingInterface
     */
    public function setRecommendedValue(?string $recommendedValue): ReviewRatingInterface;

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingExtensionInterface|null
     */
    public function getExtensionAttributes(): ?ReviewRatingExtensionInterface;

    /**
     * Set an extension attributes object.
     * @param \Xcoding\ReviewExtended\Api\Data\ReviewRatingExtensionInterface $extensionAttributes
     * @return ReviewRatingInterface
     */
    public function setExtensionAttributes(
        ReviewRatingExtensionInterface $extensionAttributes
    ): ReviewRatingInterface;
}

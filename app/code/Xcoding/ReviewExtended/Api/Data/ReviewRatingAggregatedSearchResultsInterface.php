<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface ReviewRatingAggregatedSearchResultsInterface
 * @package Xcoding\ReviewExtended\Api\Data
 */
interface ReviewRatingAggregatedSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get review_rating_aggregated list.
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface[]
     */
    public function getItems(): array;

    /**
     * Set review_rating_aggregated list.
     * @param \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface[] $items
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingSearchResultsInterface
     */
    public function setItems(array $items): ReviewRatingSearchResultsInterface;
}

<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Interface ReviewRatingAggregatedInterface
 * @package Xcoding\ReviewExtended\Api\Data
 */
interface ReviewRatingAggregatedInterface extends ExtensibleDataInterface
{
    /**#@+
     * @var string
     */
    const ID = 'id';
    const PRODUCT_ID = 'product_id';
    const CODE = 'code';
    const VALUE_COUNT = 'value_count';
    const VALUE = 'value';
    /**#@-*/

    /**
     * Get id
     * @return int|null
     */
    public function getId();

    /**
     * Set id
     * @param int|null $id
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface
     */
    public function setId(?int $id): ReviewRatingAggregatedInterface;

    /**
     * Get product_id
     * @return int|null
     */
    public function getProductId(): ?int;

    /**
     * Set product_id
     * @param int|null $productId
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface
     */
    public function setProductId(?int $productId): ReviewRatingAggregatedInterface;

    /**
     * Get code
     * @return string|null
     */
    public function getCode(): ?string;

    /**
     * Set code
     * @param string $code
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface
     */
    public function setCode(string $code): ReviewRatingAggregatedInterface;

    /**
     * Get value_count
     * @return int|null
     */
    public function getValueCount(): ?int;

    /**
     * Set value_count
     * @param int $valueCount
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface
     */
    public function setValueCount(int $valueCount): ReviewRatingAggregatedInterface;

    /**
     * Get value
     * @return string|null
     */
    public function getValue(): ?string;

    /**
     * Set value
     * @param string $value
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface
     */
    public function setValue(string $value): ReviewRatingAggregatedInterface;

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedExtensionInterface|null
     */
    public function getExtensionAttributes(): ?ReviewRatingAggregatedExtensionInterface;

    /**
     * Set an extension attributes object.
     * @param \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedExtensionInterface $extensionAttributes
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface
     */
    public function setExtensionAttributes(
        ReviewRatingAggregatedExtensionInterface $extensionAttributes
    ): ReviewRatingAggregatedInterface;
}

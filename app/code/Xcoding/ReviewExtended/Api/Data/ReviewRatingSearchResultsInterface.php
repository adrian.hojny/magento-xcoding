<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface ReviewRatingSearchResultsInterface
 * @package Xcoding\ReviewExtended\Api\Data
 */
interface ReviewRatingSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get review_rating list.
     * @return \Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface[]
     */
    public function getItems(): array;

    /**
     * Set customer_id list.
     * @param \Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface[] $items
     * @return $this
     */
    public function setItems(array $items): ReviewRatingSearchResultsInterface;
}

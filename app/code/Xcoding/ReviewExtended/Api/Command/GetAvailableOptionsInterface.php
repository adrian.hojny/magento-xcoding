<?php

namespace Xcoding\ReviewExtended\Api\Command;

/**
 * Interface GetAvailableOptionsInterface
 * @package Xcoding\ReviewExtended\Api\Command
 */
interface GetAvailableOptionsInterface
{
    /**
     * @return array
     */
    public function execute(): array;
}

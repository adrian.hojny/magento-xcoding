# Xcoding_ReviewExtended

Moduł rozszerzający bazową funkcjonalność modułu `Magento_Review`

#### Dodatkowe atrybuty oceny produktu

Nowe atrybuty określające produkt są widoczne w `Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface`

Dostępne wartości atrybutów zwracane są przez `Xcoding\ReviewExtended\Api\Command\GetAvailableOptionsInterface::execute`

#### Agregowanie nowych atrybutów

Informacje o ilościach poszczególnych głosów są zwracane przez `Xcoding\ReviewExtended\Api\ReviewRatingAggregatedRepositoryInterface::getListByProductId`
<?php

namespace Xcoding\ReviewExtended\Block\Adminhtml\Edit;

use Magento\Backend\Block\Store\Switcher\Form\Renderer\Fieldset\Element;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Review\Block\Adminhtml\Edit\Form as BaseForm;
use Magento\Review\Block\Adminhtml\Rating\Detailed;
use Magento\Review\Block\Adminhtml\Rating\Summary;
use Magento\Store\Model\Store;
use Xcoding\ReviewExtended\Block\Adminhtml\ExtendedRating\Detailed as DetailedExtendedBlock;

/**
 * Class Form
 * @package Xcoding\ReviewExtended\Block\Adminhtml\Edit
 */
class Form extends BaseForm
{
    /**
     * Prepare edit review form
     *
     * @return BaseForm
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $review = $this->_coreRegistry->registry('review_data');
        $product = $this->_productFactory->create()->load($review->getEntityPkValue());

        $formActionParams = [
            'id' => $this->getRequest()->getParam('id'),
            'ret' => $this->_coreRegistry->registry('ret')
        ];
        if ($this->getRequest()->getParam('productId')) {
            $formActionParams['productId'] = $this->getRequest()->getParam('productId');
        }
        if ($this->getRequest()->getParam('customerId')) {
            $formActionParams['customerId'] = $this->getRequest()->getParam('customerId');
        }

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getUrl(
                        'review/*/save',
                        $formActionParams
                    ),
                    'method' => 'post',
                ],
            ]
        );

        $fieldset = $form->addFieldset(
            'review_details',
            ['legend' => __('Review Details'), 'class' => 'fieldset-wide']
        );

        $fieldset->addField(
            'product_name',
            'note',
            [
                'label' => __('Product'),
                'text' => '<a href="' . $this->getUrl(
                        'catalog/product/edit',
                        ['id' => $product->getId()]
                    ) . '" onclick="this.target=\'blank\'">' . $this->escapeHtml(
                        $product->getName()
                    ) . '</a>'
            ]
        );

        try {
            $customer = $this->customerRepository->getById($review->getCustomerId());
            $customerText = __(
                '<a href="%1" onclick="this.target=\'blank\'">%2 %3</a> <a href="mailto:%4">(%4)</a>',
                $this->getUrl('customer/index/edit', ['id' => $customer->getId(), 'active_tab' => 'review']),
                $this->escapeHtml($customer->getFirstname()),
                $this->escapeHtml($customer->getLastname()),
                $this->escapeHtml($customer->getEmail())
            );
        } catch (NoSuchEntityException $e) {
            $customerText = ($review->getStoreId() == Store::DEFAULT_STORE_ID)
                ? __('Administrator') : __('Guest');
        }

        $fieldset->addField('customer', 'note', ['label' => __('Author'), 'text' => $customerText]);

        $fieldset->addField(
            'summary-rating',
            'note',
            [
                'label' => __('Summary Rating'),
                'text' => $this->getLayout()->createBlock(
                    Summary::class
                )->toHtml()
            ]
        );

        $fieldset->addField(
            'detailed-rating',
            'note',
            [
                'label' => __('Detailed Rating'),
                'required' => true,
                'text' => '<div id="rating_detail">' . $this->getLayout()->createBlock(
                        Detailed::class
                    )->toHtml() . '</div>'
            ]
        );

        // xc
        $this->addReviewRatingField($fieldset);
        $this->addProductBoughtField($fieldset, $review);
        // end xc

        $fieldset->addField(
            'status_id',
            'select',
            [
                'label' => __('Status'),
                'required' => true,
                'name' => 'status_id',
                'values' => $this->_reviewData->getReviewStatusesOptionArray()
            ]
        );

        /**
         * Check is single store mode
         */
        if (!$this->_storeManager->hasSingleStore()) {
            $field = $fieldset->addField(
                'select_stores',
                'multiselect',
                [
                    'label' => __('Visibility'),
                    'required' => true,
                    'name' => 'stores[]',
                    'values' => $this->_systemStore->getStoreValuesForForm()
                ]
            );
            $renderer = $this->getLayout()->createBlock(
                Element::class
            );
            $field->setRenderer($renderer);
            $review->setSelectStores($review->getStores());
        } else {
            $fieldset->addField(
                'select_stores',
                'hidden',
                ['name' => 'stores[]', 'value' => $this->_storeManager->getStore(true)->getId()]
            );
            $review->setSelectStores($this->_storeManager->getStore(true)->getId());
        }

        $fieldset->addField(
            'nickname',
            'text',
            ['label' => __('Nickname'), 'required' => true, 'name' => 'nickname']
        );

        $fieldset->addField(
            'title',
            'text',
            ['label' => __('Summary of Review'), 'required' => true, 'name' => 'title']
        );

        $fieldset->addField(
            'detail',
            'textarea',
            ['label' => __('Review'), 'required' => true, 'name' => 'detail', 'style' => 'height:24em;']
        );

        $form->setUseContainer(true);
        $form->setValues($review->getData());
        $this->setForm($form);
    }

    protected function addReviewRatingField($fieldset)
    {
        $fieldset->addField(
            'extended-review-rating',
            'note',
            [
                'label' => __('Extended rating'),
                'text' => $this->getLayout()->createBlock(
                    DetailedExtendedBlock::class
                )->toHtml()
            ]
        );
    }

    protected function addProductBoughtField($fieldset, $review)
    {
        $fieldset->addField(
            'product-bought',
            'note',
            [
                'label' => __('Customer bought this product before review'),
                'text' => $review->getProductBought() ? __('Yes') : __('No')
            ]
        );
    }
}

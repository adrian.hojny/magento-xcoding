<?php

namespace Xcoding\ReviewExtended\Block\Adminhtml\ExtendedRating;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Review\Model\Rating;
use Xcoding\ReviewExtended\Api\Command\GetAvailableOptionsInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface;
use Xcoding\ReviewExtended\Api\ReviewRatingRepositoryInterface;

/**
 * Adminhtml extended detailed rating
 * @method setReviewId($reviewId)
 */
class Detailed extends Template
{
    /**
     * Rating detail template name
     *
     * @var string
     */
    protected $_template = 'Xcoding_ReviewExtended::extended_rating/detailed.phtml';

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var ReviewRatingRepositoryInterface
     */
    private $ratingRepository;
    /**
     * @var GetAvailableOptionsInterface
     */
    private $getAvailableOptions;

    /**
     * @param Context $context
     * @param ReviewRatingRepositoryInterface $ratingRepository
     * @param GetAvailableOptionsInterface $getAvailableOptions
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        ReviewRatingRepositoryInterface $ratingRepository,
        GetAvailableOptionsInterface $getAvailableOptions,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->ratingRepository = $ratingRepository;
        $this->getAvailableOptions = $getAvailableOptions;
        parent::__construct($context, $data);
    }

    /**
     * Initialize review data
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        if ($this->_coreRegistry->registry('review_data')) {
            $this->setReviewId($this->_coreRegistry->registry('review_data')->getReviewId());
        }
    }

    /**
     * Get collection of ratings
     *
     * @return ReviewRatingInterface|null
     */
    public function getExtendedRating()
    {
        $id = $this->getReviewId() ? (int)$this->getReviewId() : null;
        if (!$id) {
            $id = (int)$this->getRequest()->getParam('id');
        }

        if ($id) {
            return $this->ratingRepository->getByReviewId($id);
        }

        return null;
    }

    public function getDataToDisplay()
    {
        $rating = $this->getExtendedRating();
        $opts = $this->getAvailableOptions->execute();
        $data = [];

        foreach (array_keys($opts) as $key) {
            if ($rating->getData($key)) {
                $data[$key] = $rating->getData($key);
            }
        }

        return $data;
    }
}

<?php

namespace Xcoding\ReviewExtended\Plugin\Review\Model;

use Magento\Review\Model\Review as ReviewModel;
use Xcoding\ReviewExtended\Model\ReviewRating;

/**
 * Class Review
 * @package Xcoding\ReviewExtended\Plugin\Review\Model
 */
class Review
{
    /**
     * @var ReviewRating
     */
    private $reviewRating;

    public function __construct(ReviewRating $reviewRating)
    {
        $this->reviewRating = $reviewRating;
    }

    public function afterAggregate(
        ReviewModel $subject,
        $result
    ) {
        $allowedEntityTypeId = $subject->getEntityIdByCode(ReviewModel::ENTITY_PRODUCT_CODE);
        if ($subject->getEntityId() ===  $allowedEntityTypeId) {
            $this->reviewRating->aggregate($subject->getEntityPkValue());
        }

        return $result;
    }
}

<?php

namespace Xcoding\ReviewExtended\Model\Command;

use Xcoding\ReviewExtended\Api\Command\GetAvailableOptionsInterface;
use Xcoding\ReviewExtended\Model\Enum\Comfort;
use Xcoding\ReviewExtended\Model\Enum\Quality;
use Xcoding\ReviewExtended\Model\Enum\Recommended;
use Xcoding\ReviewExtended\Model\Enum\Size;

/**
 * Class GetAvailableOptions
 * @package Xcoding\ReviewExtended\Model\Command
 */
class GetAvailableOptions implements GetAvailableOptionsInterface
{
    /**
     * @return array
     */
    public function execute(): array
    {
        return [
            'comfort_value' => [
                Comfort::NOT_COMFORTABLE,
                Comfort::COMFORTABLE,
                Comfort::VERY_COMFORTABLE
            ],
            'quality_value' => [
                Quality::LOW,
                Quality::OK,
                Quality::HIGH
            ],
            'size_value' => [
                Size::TOO_SMALL,
                Size::OK,
                Size::TOO_LARGE
            ],
            'recommended_value' => [
                Recommended::NOT_RECOMMENDED,
                Recommended::RECOMMENDED
            ]
        ];
    }
}

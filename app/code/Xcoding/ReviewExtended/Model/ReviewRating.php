<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Model;

use Magento\Framework\Model\AbstractExtensibleModel;

use Magento\Review\Model\Review;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingExtensionInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRating as ReviewRatingResource;

/**
 * Class ReviewRating
 * @package Xcoding\ReviewExtended\Model
 */
class ReviewRating extends AbstractExtensibleModel implements ReviewRatingInterface
{
    protected $_eventPrefix = 'xc_review_rating';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ReviewRatingResource::class);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id): ReviewRatingInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId(): ?int
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId(?int $customerId): ReviewRatingInterface
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * @inheritDoc
     */
    public function getProductId(): ?int
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setProductId(?int $productId): ReviewRatingInterface
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * @inheritDoc
     */
    public function getSizeValue(): ?string
    {
        return $this->getData(self::SIZE_VALUE);
    }

    /**
     * @inheritDoc
     */
    public function setSizeValue(?string $sizeValue): ReviewRatingInterface
    {
        return $this->setData(self::SIZE_VALUE, $sizeValue);
    }

    /**
     * @inheritDoc
     */
    public function getQualityValue(): ?string
    {
        return $this->getData(self::QUALITY_VALUE);
    }

    /**
     * @inheritDoc
     */
    public function setQualityValue(?string $qualityValue): ReviewRatingInterface
    {
        return $this->setData(self::QUALITY_VALUE, $qualityValue);
    }

    /**
     * @inheritDoc
     */
    public function getComfortValue(): ?string
    {
        return $this->getData(self::COMFORT_VALUE);
    }

    /**
     * @inheritDoc
     */
    public function setComfortValue(?string $comfortValue): ReviewRatingInterface
    {
        return $this->setData(self::COMFORT_VALUE, $comfortValue);
    }

    /**
     * @inheritDoc
     */
    public function getReviewId(): ?int
    {
        return $this->getData(self::REVIEW_ID);
    }

    /**
     * @inheritDoc
     */
    public function setReviewId(?int $reviewId): ReviewRatingInterface
    {
        return $this->setData(self::REVIEW_ID, $reviewId);
    }

    /**
     * @inheritDoc
     */
    public function getRecommendedValue(): ?string
    {
        return $this->getData(self::RECOMMENDED_VALUE);
    }

    /**
     * @inheritDoc
     */
    public function setRecommendedValue(?string $recommendedValue): ReviewRatingInterface
    {
        return $this->setData(self::RECOMMENDED_VALUE, $recommendedValue);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes(): ?ReviewRatingExtensionInterface
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(ReviewRatingExtensionInterface $extensionAttributes): ReviewRatingInterface
    {
        $this->_setExtensionAttributes($extensionAttributes);

        return $this;
    }

    /**
     * Aggregate review rating
     *
     * @return $this
     */
    public function aggregate($productId)
    {
        if (!$productId) {
            return $this;
        }

        $this->getResource()->aggregate($productId);
        return $this;
    }
}

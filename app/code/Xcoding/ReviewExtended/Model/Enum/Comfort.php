<?php

namespace Xcoding\ReviewExtended\Model\Enum;

/**
 * Class Comfort
 * @package Xcoding\ReviewExtended\Model
 */
abstract class Comfort
{
    /**#@+
     * @var string
     */
    public const NOT_COMFORTABLE = 'NOT_COMFORTABLE';
    public const COMFORTABLE = 'COMFORTABLE';
    public const VERY_COMFORTABLE = 'VERY_COMFORTABLE';
    /**#@-*/
}

<?php

namespace Xcoding\ReviewExtended\Model\Enum;

/**
 * Class Recommended
 * @package Xcoding\ReviewExtended\Model
 */
abstract class Recommended
{
    /**#@+
     * @var string
     */
    public const RECOMMENDED = 'RECOMMENDED';
    public const NOT_RECOMMENDED = 'NOT_RECOMMENDED';
    /**#@-*/
}

<?php

namespace Xcoding\ReviewExtended\Model\Enum;

/**
 * Class Quality
 * @package Xcoding\ReviewExtended\Model
 */
abstract class Quality
{
    /**#@+
     * @var string
     */
    public const LOW = 'QUALITY_LOW';
    public const OK = 'QUALITY_OK';
    public const HIGH = 'QUALITY_HIGH';
    /**#@-*/
}

<?php

namespace Xcoding\ReviewExtended\Model\Enum;

/**
 * Class SizeEnum
 * @package Xcoding\ReviewExtended\Model
 */
abstract class Size
{
    /**#@+
     * @var string
     */
    public const TOO_SMALL = 'SIZE_TOO_SMALL';
    public const OK = 'SIZE_OK';
    public const TOO_LARGE = 'SIZE_TOO_LARGE';
    /**#@-*/
}

<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Model\ResourceModel\ReviewRating;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Review\Model\Review;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRating as ReviewRatingResource;
use Xcoding\ReviewExtended\Model\ReviewRating;

/**
 * Class Collection
 * @package Xcoding\ReviewExtended\Model\ResourceModel\ReviewRating
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    public function addApprovedReviewsFilter()
    {
        $this->getSelect()
            ->joinLeft([
                'r' => $this->_resource->getTable('review')
            ], 'r.review_id = main_table.review_id')
            ->where('status_id = ?', Review::STATUS_APPROVED);

        return $this;
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            ReviewRating::class,
            ReviewRatingResource::class
        );
    }
}

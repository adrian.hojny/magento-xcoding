<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Model\ResourceModel\ReviewRatingAggregated;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRatingAggregated as ReviewRatingAggregatedResource;
use Xcoding\ReviewExtended\Model\ReviewRatingAggregated;

/**
 * Class Collection
 * @package Xcoding\ReviewExtended\Model\ResourceModel\ReviewRatingAggregated
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            ReviewRatingAggregated::class,
            ReviewRatingAggregatedResource::class
        );
    }
}

<?php

namespace Xcoding\ReviewExtended\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface;

/**
 * Class ReviewRatingAggregated
 * @package Xcoding\ReviewExtended\Model\ResourceModel
 */
class ReviewRatingAggregated extends AbstractDb
{
    /**#@+
     * Constants related to specific db layer
     */
    const TABLE_NAME_SOURCE = 'xcoding_review_rating_aggregated';
    /**#@-*/

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME_SOURCE, ReviewRatingAggregatedInterface::ID);
    }
}

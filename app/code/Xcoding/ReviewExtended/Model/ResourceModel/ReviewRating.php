<?php

namespace Xcoding\ReviewExtended\Model\ResourceModel;

use Exception;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Xcoding\ReviewExtended\Api\Command\GetAvailableOptionsInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterfaceFactory;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface;
use Xcoding\ReviewExtended\Api\ReviewRatingAggregatedRepositoryInterface;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRating\Collection as ReviewRatingCollection;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRating\CollectionFactory as ReviewRatingCollectionFactory;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRatingAggregated as ReviewRatingAggregatedResource;

/**
 * Class ReviewRating
 * @package Xcoding\ReviewExtended\Model\ResourceModel
 */
class ReviewRating extends AbstractDb
{
    /**
     * Constants related to specific db layer
     */
    const TABLE_NAME_SOURCE = 'xcoding_review_rating';

    /**
     * @var ReviewRatingCollectionFactory
     */
    private $reviewRatingCollectionFactory;

    /**
     * @var GetAvailableOptionsInterface
     */
    private $getAvailableOptions;

    /**
     * @var ReviewRatingAggregated
     */
    private $reviewRatingAggregatedResource;

    /**
     * @var ReviewRatingAggregatedRepositoryInterface
     */
    private $reviewRatingAggregatedRepository;

    /**
     * @var ReviewRatingAggregatedInterfaceFactory
     */
    private $reviewRatingAggregatedFactory;

    /**
     * @param Context $context
     * @param ReviewRatingCollectionFactory $reviewRatingCollectionFactory
     * @param ReviewRatingAggregatedInterfaceFactory $reviewRatingAggregatedFactory
     * @param ReviewRatingAggregatedRepositoryInterface $reviewRatingAggregatedRepository
     * @param ReviewRatingAggregated $reviewRatingAggregatedResource
     * @param GetAvailableOptionsInterface $getAvailableOptions
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        ReviewRatingCollectionFactory $reviewRatingCollectionFactory,
        ReviewRatingAggregatedInterfaceFactory $reviewRatingAggregatedFactory,
        ReviewRatingAggregatedRepositoryInterface $reviewRatingAggregatedRepository,
        ReviewRatingAggregatedResource $reviewRatingAggregatedResource,
        GetAvailableOptionsInterface $getAvailableOptions,
        $connectionName = null
    ) {
        $this->reviewRatingCollectionFactory = $reviewRatingCollectionFactory;
        $this->getAvailableOptions = $getAvailableOptions;
        $this->reviewRatingAggregatedResource = $reviewRatingAggregatedResource;
        $this->reviewRatingAggregatedRepository = $reviewRatingAggregatedRepository;
        $this->reviewRatingAggregatedFactory = $reviewRatingAggregatedFactory;
        parent::__construct($context, $connectionName);
    }

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME_SOURCE, ReviewRatingInterface::ID);
    }

    /**
     * Aggregate
     *
     * @param $productId
     * @return ReviewRating
     * @throws Exception
     */
    public function aggregate($productId)
    {
        if (!$productId) {
            return $this;
        }

        /** @var ReviewRatingCollection $collection */
        $collection = $this->reviewRatingCollectionFactory->create();
        $collection->addApprovedReviewsFilter()
            ->addFieldToFilter('product_id', $productId);

        $aggregatedData = $this->getAggregatedVotes($collection);
        $this->saveAggregatedVotes($aggregatedData, $productId);

        return $this;
    }

    /**
     * @param ReviewRatingCollection $collection
     * @return array
     */
    protected function getAggregatedVotes(ReviewRatingCollection $collection): array
    {
        $availableOpts = array_keys($this->getAvailableOptions->execute());
        $aggregatedData = [];

        /** @var ReviewRatingInterface $item */
        foreach ($collection as $item) {
            foreach ($availableOpts as $opt) {
                if (!isset($aggregatedData[$opt])) {
                    $aggregatedData[$opt] = [];
                }

                $vote = $item->getData($opt);
                if (null !== $vote) {
                    $aggregatedData[$opt][$vote] = isset($aggregatedData[$opt][$vote]) ?
                        $aggregatedData[$opt][$vote] + 1 :
                        1;
                }
            }
        }
        return $aggregatedData;
    }

    /**
     * @param array $aggregatedData
     * @param int $productId
     * @throws Exception
     */
    protected function saveAggregatedVotes(array $aggregatedData, int $productId)
    {
        $connection = $this->getConnection();
        $connection->beginTransaction();

        try {
            $this->reviewRatingAggregatedRepository->deleteByProductId($productId);

            foreach ($aggregatedData as $code => $arrayValues) {
                foreach ($arrayValues as $value => $count) {
                    $aggregatedItem = $this->reviewRatingAggregatedFactory->create();
                    $aggregatedItem->setValueCount($count);
                    $aggregatedItem->setProductId($productId);
                    $aggregatedItem->setValue($value);
                    $aggregatedItem->setCode($code);

                    $this->reviewRatingAggregatedResource->save($aggregatedItem);
                }
            }
            $connection->commit();
        } catch (Exception $e) {
            $connection->rollBack();
            throw $e;
        }
    }
}

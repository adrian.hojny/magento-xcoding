<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Model;

use Magento\Framework\Model\AbstractExtensibleModel;

use Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedExtensionInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRatingAggregated as ReviewRatingAggregatedResource;

/**
 * Class ReviewRatingAggregated
 * @package Xcoding\ReviewExtended\Model
 */
class ReviewRatingAggregated extends AbstractExtensibleModel implements ReviewRatingAggregatedInterface
{
    protected $_eventPrefix = 'xc_review_rating_aggregated';

    /**
     * @inheritdoc
     */
    protected function _construct()
    {
        $this->_init(ReviewRatingAggregatedResource::class);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id): ReviewRatingAggregatedInterface
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getProductId(): ?int
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setProductId(?int $productId): ReviewRatingAggregatedInterface
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * @inheritDoc
     */
    public function getCode(): ?string
    {
        return $this->getData(self::CODE);
    }

    /**
     * @inheritDoc
     */
    public function setCode(string $code): ReviewRatingAggregatedInterface
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * @inheritDoc
     */
    public function getValueCount(): ?int
    {
        return $this->getData(self::VALUE_COUNT);
    }

    /**
     * @inheritDoc
     */
    public function setValueCount(int $valueCount): ReviewRatingAggregatedInterface
    {
        return $this->setData(self::VALUE_COUNT, $valueCount);
    }

    /**
     * @inheritDoc
     */
    public function getValue(): ?string
    {
        return $this->getData(self::VALUE);
    }

    /**
     * @inheritDoc
     */
    public function setValue(string $value): ReviewRatingAggregatedInterface
    {
        return $this->setData(self::VALUE, $value);
    }

    /**
     * @inheritDoc
     */
    public function getExtensionAttributes(): ?ReviewRatingAggregatedExtensionInterface
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * @inheritDoc
     */
    public function setExtensionAttributes(ReviewRatingAggregatedExtensionInterface $extensionAttributes): ReviewRatingAggregatedInterface
    {
        $this->_setExtensionAttributes($extensionAttributes);

        return $this;
    }
}

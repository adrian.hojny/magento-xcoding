<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Model;

use Xcoding\ReviewExtended\Api\Data\ReviewRatingAggregatedInterface;
use Xcoding\ReviewExtended\Api\ReviewRatingAggregatedRepositoryInterface;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRatingAggregated as ReviewRatingAggregatedResource;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRatingAggregated\CollectionFactory as ReviewRatingAggregatedCollectionFactory;

/**
 * Class ReviewRatingRepository
 * @package Xcoding\ReviewExtended\Model
 */
class ReviewRatingAggregatedRepository implements ReviewRatingAggregatedRepositoryInterface
{
    /**
     * @var ReviewRatingAggregatedCollectionFactory
     */
    private $reviewRatingAggregatedCollectionFactory;
    /**
     * @var ReviewRatingAggregatedResource
     */
    private $reviewRatingAggregatedResource;

    /**
     * ReviewRatingAggregatedRepository constructor.
     * @param ReviewRatingAggregatedResource $reviewRatingAggregatedResource
     * @param ReviewRatingAggregatedCollectionFactory $reviewRatingAggregatedCollectionFactory
     */
    public function __construct(
        ReviewRatingAggregatedResource $reviewRatingAggregatedResource,
        ReviewRatingAggregatedCollectionFactory $reviewRatingAggregatedCollectionFactory
    ) {
        $this->reviewRatingAggregatedCollectionFactory = $reviewRatingAggregatedCollectionFactory;
        $this->reviewRatingAggregatedResource = $reviewRatingAggregatedResource;
    }

    /**
     * @inheritDoc
     */
    public function getListByProductId(int $productId): array
    {
        return $this->reviewRatingAggregatedCollectionFactory->create()
            ->addFieldToFilter('product_id', $productId)
            ->getItems();
    }

    /**
     * @inheritDoc
     */
    public function delete(ReviewRatingAggregatedInterface $reviewRatingAggregated): bool
    {
        $this->reviewRatingAggregatedResource->delete($reviewRatingAggregated);

        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteByProductId(int $productId): bool
    {
        $reviewRatingAggregatedCollection = $this->reviewRatingAggregatedCollectionFactory->create()
            ->addFieldToFilter('product_id', $productId);
        $reviewRatingAggregatedCollection->walk('delete');

        return true;
    }
}

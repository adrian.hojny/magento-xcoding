<?php declare(strict_types=1);

namespace Xcoding\ReviewExtended\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingInterfaceFactory;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingSearchResultsInterface;
use Xcoding\ReviewExtended\Api\Data\ReviewRatingSearchResultsInterfaceFactory;
use Xcoding\ReviewExtended\Api\ReviewRatingRepositoryInterface;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRating as ReviewRatingResource;
use Xcoding\ReviewExtended\Model\ResourceModel\ReviewRating\CollectionFactory as ReviewRatingCollectionFactory;

/**
 * Class ReviewRatingRepository
 * @package Xcoding\ReviewExtended\Model
 */
class ReviewRatingRepository implements ReviewRatingRepositoryInterface
{
    /**
     * @var ReviewRatingResource
     */
    private $reviewRatingResource;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ReviewRatingInterfaceFactory
     */
    private $reviewRatingFactory;

    /**
     * @var ReviewRatingCollectionFactory
     */
    private $reviewRatingCollectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ReviewRatingSearchResultsInterfaceFactory
     */
    private $reviewRatingSearchResultsFactory;

    /**
     * ReviewRatingRepository constructor.
     * @param ReviewRatingResource $modelResource
     * @param ReviewRatingInterfaceFactory $reviewRatingFactory
     * @param ReviewRatingCollectionFactory $reviewRatingCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CollectionProcessorInterface $collectionProcessor
     * @param ReviewRatingSearchResultsInterfaceFactory $reviewRatingSearchResultsFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        ReviewRatingResource $modelResource,
        ReviewRatingInterfaceFactory $reviewRatingFactory,
        ReviewRatingCollectionFactory $reviewRatingCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CollectionProcessorInterface $collectionProcessor,
        ReviewRatingSearchResultsInterfaceFactory $reviewRatingSearchResultsFactory,
        LoggerInterface $logger
    ) {
        $this->reviewRatingResource = $modelResource;
        $this->logger = $logger;
        $this->reviewRatingFactory = $reviewRatingFactory;
        $this->reviewRatingCollectionFactory = $reviewRatingCollectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->collectionProcessor = $collectionProcessor;
        $this->reviewRatingSearchResultsFactory = $reviewRatingSearchResultsFactory;
    }

    /**
     * Save review_rating
     * @param ReviewRatingInterface $reviewRating
     * @return ReviewRatingInterface
     * @throws LocalizedException
     */
    public function save(ReviewRatingInterface $reviewRating): ReviewRatingInterface
    {
        try {
            $this->reviewRatingResource->save($reviewRating);
            return $reviewRating;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw new CouldNotSaveException(__('Could not save rating'), $e);
        }
    }

    /**
     * Retrieve review_rating
     * @param int $id
     * @return ReviewRatingInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): ReviewRatingInterface
    {
        /** @var ReviewRatingInterface $reviewRating */
        $reviewRating = $this->reviewRatingFactory->create();
        $this->reviewRatingResource->load($reviewRating, $id, ReviewRatingInterface::ID);

        if (null === $reviewRating->getId()) {
            throw new NoSuchEntityException(__('Review rating with id "%value" does not exist.', ['value' => $id]));
        }
        return $reviewRating;
    }

    /**
     * Retrieve review_rating matching the specified criteria.
     * @param SearchCriteriaInterface|null $searchCriteria
     * @return ReviewRatingSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): ReviewRatingSearchResultsInterface
    {
        /** @var Collection $collection */
        $collection = $this->reviewRatingCollectionFactory->create();

        if (null === $searchCriteria) {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        } else {
            $this->collectionProcessor->process($searchCriteria, $collection);
        }

        /** @var ReviewRatingSearchResultsInterface $searchResult */
        $searchResult = $this->reviewRatingSearchResultsFactory->create();
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;
    }

    /**
     * @param int $reviewId
     * @return ReviewRatingInterface
     */
    public function getByReviewId(int $reviewId): ReviewRatingInterface
    {
        return $this->reviewRatingCollectionFactory->create()
            ->addFieldTofilter('review_id', $reviewId)
            ->getFirstItem();
    }

    /**
     * Delete review_rating by ID
     * @param int $reviewRatingId
     * @return bool true on success
     * @throws CouldNotDeleteException
     * @throws LocalizedException
     */
    public function deleteById(int $reviewRatingId): bool
    {
        try {
            /** @var ReviewRatingInterface $reviewRating */
            $reviewRating = $this->get($reviewRatingId);

            $this->reviewRatingResource->delete($reviewRating);
        } catch (NoSuchEntityException $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new CouldNotDeleteException(__('Could not delete review rating'), $e);
        }
    }
}

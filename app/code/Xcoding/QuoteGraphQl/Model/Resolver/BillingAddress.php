<?php

namespace Xcoding\QuoteGraphQl\Model\Resolver;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\OrderRepository;

class BillingAddress implements ResolverInterface
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;
    
    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * BillingAddress constructor.
     * @param OrderRepository $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        OrderRepository $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $orderNumber = $value['order_number'] ?? $value['base_order_info']['increment_id'] ?? null;
        if (null !== $orderNumber) {
            $orders = $this->getOrderByNumber($orderNumber);
            $order = reset($orders) ?? null;
            if (null !== $order && $order->getIncrementId() === $orderNumber) {
                return $order->getBillingAddress()->getData();
            }
        }

        return [];
    }

    /**
     * @param $incrementId
     * @return OrderInterface[]
     */
    protected function getOrderByNumber($incrementId) {
        $this->searchCriteriaBuilder->addFilter('increment_id', $incrementId);

        $order = $this->orderRepository->getList(
            $this->searchCriteriaBuilder->create()
        );

        return $order->getItems();
    }
}

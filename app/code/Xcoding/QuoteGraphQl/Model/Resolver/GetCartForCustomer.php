<?php
declare(strict_types=1);

namespace Xcoding\QuoteGraphQl\Model\Resolver;

use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Query\Resolver\Value;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\Quote\Item as QuoteItem;

class GetCartForCustomer extends \ScandiPWA\QuoteGraphQl\Model\Resolver\GetCartForCustomer
{
    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field $field
     * @param ContextInterface $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return Value|CartInterface|mixed
     * @throws NotFoundException
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var \Magento\Quote\Model\Cart $cart */
        $cart = $this->getCart($args);
        $items = $cart->getItems();
        $itemsData = [];

        if ($items) {
            // Prepare product data in advance
            $products = array_map(function ($item) {
                return $item->getProduct();
            }, $items);

            $adjustedInfo = $info->fieldNodes[0];
            $this->productsData = $this->productPostProcessor->process(
                $products,
                'items/product',
                $adjustedInfo
            );

            foreach ($items as $item) {
                /** @var QuoteItem $item */
                $product = $item->getProduct();
                $itemsData[] = $this->mergeQuoteItemData($item, $product);
            }
        }

        /** @var \Magento\Quote\Model\Quote\Address $address */
        $address = $cart->isVirtual() ? $cart->getBillingAddress() : $cart->getShippingAddress();
        $taxAmount = $address->getTaxAmount();
        $subtotalTax = $taxAmount - $address->getShippingTaxAmount();
        $discountAmount = $address->getDiscountAmount();
        $discountedSubtotalInclTax = $cart->getSubtotalWithDiscount() + $subtotalTax + $address->getDiscountTaxCompensationAmount();

        return [
            'items' => $itemsData,
            'tax_amount' => $taxAmount,
            'subtotal_incl_tax' => $address->getSubtotalInclTax(),
            'discounted_subtotal_incl_tax' => $discountedSubtotalInclTax,
            'discount_amount' => $discountAmount,
            // In interface it is PHPDocumented that it returns bool,
            // while in implementation it returns int.
            'is_virtual' => (bool) $cart->getIsVirtual()
        ] + $cart->getData();
    }
}

<?php

namespace Xcoding\QuoteGraphQl\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\CartManagementInterface;
use Magento\QuoteGraphQl\Model\Cart\GetCartForUser;

class ChangeCurrency implements ResolverInterface
{

    /**
     * @var CartManagementInterface
     */
    private $cartManagement;

    /**
     * @var GetCartForUser
     */
    private $getCartForUser;

    /**
     * @param GetCartForUser $getCartForUser
     * @param CartManagementInterface $cartManagement
     */
    public function __construct(
        GetCartForUser $getCartForUser,
        CartManagementInterface $cartManagement
    ) {
        $this->getCartForUser = $getCartForUser;
        $this->cartManagement = $cartManagement;
    }

    /**
     * Fetches the data from persistence models and format it according to the GraphQL schema.
     *
     * @param Field $field
     * @param \Magento\GraphQl\Model\Query\Context $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return bool
     * @throws \Exception
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        $guestCartId = $args['guestCartId'] ?? '';
        $currency = $args['currency'] ?? '';

        $customerId = $context->getUserId();
        $storeId = (int)$context->getExtensionAttributes()->getStore()->getId();

        if ($guestCartId !== '') {
            $quote = $this->getCartForUser->execute($guestCartId, $customerId, $storeId);
        } else {
            $quote = $this->cartManagement->getCartForCustomer($customerId);
        }
        /** @var \Magento\Quote\Model\Quote $quote */

        if ($quote->getQuoteCurrencyCode() == $currency) {
            return true;
        }
        $quote->setQuoteCurrencyCode($currency);
        $quote->collectTotals()->save();

        return true;
    }
}
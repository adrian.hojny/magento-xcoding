<?php
declare(strict_types=1);

namespace Xcoding\QuoteGraphQl\Model\Resolver;

class ProductResolver extends \ScandiPWA\QuoteGraphQl\Model\Resolver\ProductResolver
{
    /**
     * Get All Product Items of Order.
     * @inheritdoc
     */
    public function resolve(
        \Magento\Framework\GraphQl\Config\Element\Field $field,
        $context,
        \Magento\Framework\GraphQl\Schema\Type\ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['products'])) {
            return [];
        }

        $productIds = array_map(function ($item) {
            return $item['product_id'];
        }, $value['products']);

        $attributeCodes = $this->getFieldsFromProductInfo($info, 'order_products');

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('entity_id', $productIds, 'in')
            ->create();

        $products = $this->productDataProvider
            ->getList(
                $searchCriteria,
                $attributeCodes,
                false,
                true,
                false,
                false,
                true
            )
            ->getItems();

        $productsData = $this->postProcessor->process(
            $products,
            'order_products',
            $info
        );

        $data = [];

        foreach ($value['products'] as $key => $item) {
            /** @var $item Item */
            $data[$key] = $productsData[$item->getProductId()];
            $data[$key]['qty'] = $item->getQtyOrdered();
            $data[$key]['row_total'] = $item->getRowTotalInclTax();
            $data[$key]['original_price'] = $item->getOriginalPrice();
            $data[$key]['special_price'] = $item->getPriceInclTax();
            $data[$key]['license_key'] = $item['license_key'];
        }

        return $data;
    }
}

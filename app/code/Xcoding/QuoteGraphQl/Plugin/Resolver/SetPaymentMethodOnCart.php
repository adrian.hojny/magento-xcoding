<?php

namespace Xcoding\QuoteGraphQl\Plugin\Resolver;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class SetPaymentMethodOnCart
{
    public function afterResolve(
        $subject,
        $result,
        \Magento\Framework\GraphQl\Config\Element\Field $field,
        $context,
        \Magento\Framework\GraphQl\Schema\Type\ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $result['cart']['model'];
        $additional = $args['input']['additional_data'] ?? null;
        if ($data = json_decode($additional, true)) {
            /** @var \Magento\Sales\Model\Order\Payment $payment */
            $payment = $quote->getPayment();
            foreach ($data as $key => $value) {
                $payment->setAdditionalInformation($key, $value);
            }
            $payment->save();
        }

        return $result;
    }
}
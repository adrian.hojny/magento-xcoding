<?php
namespace Xcoding\QuoteGraphQl\Plugin\Resolver;

class ProductResolver
{
    public function afterResolve(
        $subject,
        $result,
        \Magento\Framework\GraphQl\Config\Element\Field $field,
        $context,
        \Magento\Framework\GraphQl\Schema\Type\ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['products'])) {
            return [];
        }
        foreach ($value['products'] as $key => $item) {
            /** @var \Magento\Sales\Model\Order\Item $item */
            $attributes = $item->getProductOptions()['attributes_info'] ?? null;
            if (!$attributes) continue;
            $data = [];
            foreach ($attributes as $attribute) {
                $data[] = ['label' => __($attribute['label']), 'value' => $attribute['value']];
            }
            $result[$key]['attributes_info'] = $data;
        }
        return $result;
    }
}
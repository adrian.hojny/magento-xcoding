<?php

namespace Xcoding\QuoteGraphQl\Plugin\Resolver;

use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Quote\Api\CartManagementInterface;
use Magento\QuoteGraphQl\Model\Cart\GetCartForUser;

class SaveAddressInformation
{
    /**
     * @var CartManagementInterface
     */
    private $cartManagement;

    /**
     * @var GetCartForUser
     */
    private $getCartForUser;

    /**
     * @var SubscriberFactory
     */
    private $subscriberFactory;

    /**
     * @param GetCartForUser $getCartForUser
     * @param CartManagementInterface $cartManagement
     * @param SubscriberFactory $subscriberFactory
     */
    public function __construct(
        CartManagementInterface $cartManagement,
        GetCartForUser $getCartForUser,
        SubscriberFactory $subscriberFactory
    ) {
        $this->cartManagement = $cartManagement;
        $this->getCartForUser = $getCartForUser;
        $this->subscriberFactory = $subscriberFactory;
    }

    public function afterResolve(
        $subject,
        $result,
        \Magento\Framework\GraphQl\Config\Element\Field $field,
        $context,
        \Magento\Framework\GraphQl\Schema\Type\ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $subscribe = $args['addressInformation']['subscribe_newsletter'] ?? null;
        if (!$subscribe) {
            return $result;
        }
        $guestCartId = $args['guestCartId'] ?? '';

        $customerId = $context->getUserId();
        $storeId = (int) $context->getExtensionAttributes()->getStore()->getId();

        if ($guestCartId !== '') {
            $quote = $this->getCartForUser->execute($guestCartId, $customerId, $storeId);
        } else {
            $quote = $this->cartManagement->getCartForCustomer($customerId);
        }
        /** @var \Magento\Quote\Model\Quote $quote */

        $email = $quote->getCustomerEmail();
        if (!$email) {
            return $result;
        }
        $subscriber = $this->subscriberFactory->create()->loadByEmail($email);
        if (!$subscriber->getId()) {
            $subscriber->subscribe($email);
        }
        return $result;
    }
}
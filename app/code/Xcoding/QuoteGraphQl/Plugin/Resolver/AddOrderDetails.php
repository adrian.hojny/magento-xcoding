<?php
namespace Xcoding\QuoteGraphQl\Plugin\Resolver;
class AddOrderDetails
{

    public function afterResolve(
        $subject,
        $result
    ) {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $result['order']['model'] ?? null;
        if ($order) {
            $orderDetails = $this->resolveOrderDetails($order);
            foreach ($orderDetails as $key => $detail) {
                $result['order'][$key] = $detail;
            }
        }

        return $result;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return array
     */
    protected function resolveOrderDetails($order)
    {
        $itemsData = [];
        $trackNumbers = [];

        foreach ($order->getAllVisibleItems() as $item) {
            $itemsData[] = $item;
        }

        $tracksCollection = $order->getTracksCollection();

        foreach ($tracksCollection->getItems() as $track) {
            $trackNumbers[] = $track->getTrackNumber();
        }

        $shippingInfo = [
            'shipping_amount' => $order->getShippingAmount(),
            'shipping_incl_tax' => $order->getShippingInclTax(),
            'shipping_method' => $order->getShippingMethod(),
            'shipping_address' => $order->getShippingAddress(),
            'shipping_description' => $order->getShippingDescription(),
            'tracking_numbers' => $trackNumbers
        ];

        $base_info = [
            'id' => $order->getId(),
            'increment_id' => $order->getIncrementId(),
            'created_at' => $order->getCreatedAt(),
            'grand_total' => $order->getGrandTotal(),
            'sub_total' => $order->getBaseSubtotalInclTax(),
            'status' => $order->getStatus(),
            'status_label' => $order->getStatusLabel(),
            'total_qty_ordered' => $order->getTotalQtyOrdered(),
        ];

        return [
            'base_order_info' => $base_info,
            'shipping_info' => $shippingInfo,
            'payment_info' => $order->getPayment()->getData(),
            'products' => $itemsData
        ];
    }
}
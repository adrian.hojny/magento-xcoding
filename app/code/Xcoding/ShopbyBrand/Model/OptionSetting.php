<?php
declare(strict_types=1);

namespace Xcoding\ShopbyBrand\Model;

class OptionSetting extends \Amasty\ShopbyBase\Model\OptionSetting
{
    const VIDEOS_DIR = '/amasty/shopby/option_videos/';
    /**
     * Save image & slider_image
     *
     * @param OptionSetting $model
     * @param OptionSetting $defaultModel
     * @param array $data
     * @param bool $isSlider
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _processImages($model, $defaultModel, &$data, $isSlider = false)
    {
        if ($isSlider) {
            return parent::_processImages($model, $defaultModel, $data, $isSlider);
        }
        $fields = [
            'image',
            'measure_men_img',
            'measure_women_img',
            'measure_junior_img',
            'measure_unisex_img',
            'tablet_image',
            'mobile_image',
            'bottom_image',
            'box_1_image',
            'box_2_image',
            'box_3_image',
        ];
        foreach ($fields as $field) {
            $useDefaultImage = $this->isUseDefault($field, $data);
            if ($useDefaultImage
                && ($model->getData($field) != $defaultModel->getData($field))
                || isset($data[$field . '_delete'])
            ) {
                $model->removeImage($isSlider, $field);
                $data[$field] = '';
            }
            if (!$useDefaultImage) {
                try {
                    $imageName = $model->uploadImage($field, $isSlider);
                    $data[$field] = $imageName;
                } catch (\Exception $e) {
                    if ($e->getCode() != \Magento\Framework\File\Uploader::TMP_NAME_EMPTY
                        && $e->getMessage() != '$_FILES array is empty'
                    ) {
                        throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()), $e);
                    }
                }
            }
        }

        $this->processVideos($model, $defaultModel, $data);

        return parent::_processImages($model, $defaultModel, $data, true);
    }
    /**
     * Save image & slider_image
     *
     * @param OptionSetting $model
     * @param OptionSetting $defaultModel
     * @param array $data
     * 
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function processVideos($model, $defaultModel, &$data)
    {
        $fields = ['bottom_video'];
        foreach ($fields as $field) {
            $useDefaultImage = $this->isUseDefault($field, $data);
            if ($useDefaultImage
                && ($model->getData($field) != $defaultModel->getData($field))
                || isset($data[$field . '_delete'])
            ) {
                $model->removeVideo($field);
                $data[$field] = '';
            }
            if (!$useDefaultImage) {
                try {
                    $imageName = $model->uploadVideo($field);
                    $data[$field] = $imageName;
                } catch (\Exception $e) {
                    if ($e->getCode() != \Magento\Framework\File\Uploader::TMP_NAME_EMPTY
                        && $e->getMessage() != '$_FILES array is empty'
                    ) {
                        throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()), $e);
                    }
                }
            }
        }
    }

    /**
     * @param int $fileId
     * @param bool $isSlider
     * @return string
     */
    public function uploadVideo($fileId)
    {
        $mediaDir = $this->fileSystem->getDirectoryWrite('media');
        $uploader = $this->uploaderFactory->create(['fileId' => $fileId]);
        $uploader->setFilesDispersion(false);
        $uploader->setFilenamesCaseSensitivity(false);
        $uploader->setAllowRenameFiles(true);
        $uploader->setAllowedExtensions(['mp4']);
        $uploader->save($mediaDir->getAbsolutePath(self::VIDEOS_DIR));
        $result = $uploader->getUploadedFileName();

        return $result;
    }

    /**
     * @param string $field
     *
     * @return [void]
     */
    private function removeVideo($field = 'image')
    {
        $useDefault = $this->getData($field . '_use_default');
        if (!$useDefault || $this->getStoreId() == 0) {
            $img = $this->getData($field);
            if ($img) {
                $mediaDir = $this->fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $imgPath = self::VIDEOS_DIR . $img;
                $path = $mediaDir->getAbsolutePath($imgPath);
                if ($this->fileDriver->isExists($path)) {
                    $this->fileDriver->deleteFile($path);
                }
            }
        }
    }

    /**
     * @param bool $isSlider
     * @param string $field
     * @return void
     */
    public function removeImage($isSlider = false, $field = 'image')
    {
        if ($isSlider) {
            return parent::removeImage($isSlider);
        }
        $useDefault = $this->getData($field . '_use_default');
        if (!$useDefault || $this->getStoreId() == 0) {
            $img = $this->getData($field);
            if ($img) {
                $mediaDir = $this->fileSystem->getDirectoryRead(\Magento\Framework\App\Filesystem\DirectoryList::MEDIA);
                $imgPath = self::IMAGES_DIR . $img;
                $path = $mediaDir->getAbsolutePath($imgPath);
                if ($this->fileDriver->isExists($path)) {
                    $this->fileDriver->deleteFile($path);
                }
            }
        }
    }

    /**
     * @param string $field
     * @return null|string
     */
    public function getCustomImageUrl($field)
    {
        $value = $this->getData($field);
        if (!$value) {
            return null;
        }

        return $this->getMediaBaseUrl() . self::IMAGES_DIR . $value;
    }

    /**
     * @return null|string
     */
    public function getTabletImageUrl()
    {
        if (!$this->getTabletImage()) {
            return null;
        }

        return $this->getMediaBaseUrl() . self::IMAGES_DIR . $this->getTabletImage();
    }

    /**
     * @return null|string
     */
    public function getMobileImageUrl()
    {
        if (!$this->getMobileImage()) {
            return null;
        }

        return $this->getMediaBaseUrl() . self::IMAGES_DIR . $this->getMobileImage();
    }

    /**
     * @return null|string
     */
    public function getBottomImageUrl()
    {
        if (!$this->getBottomImage()) {
            return null;
        }

        return $this->getMediaBaseUrl() . self::IMAGES_DIR . $this->getBottomImage();
    }

    /**
     * @return null|string
     */
    public function getBottomVideoUrl()
    {
        if (!$this->getBottomVideo()) {
            return null;
        }

        return $this->getMediaBaseUrl() . self::VIDEOS_DIR . $this->getBottomVideo();
    }
}

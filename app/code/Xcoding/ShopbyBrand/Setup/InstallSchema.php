<?php
namespace Xcoding\ShopbyBrand\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $setup->getTable('amasty_amshopby_option_setting');
        
        $connection = $setup->getConnection();
        $connection->addColumn($tableName, 'tablet_image', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'after' => 'image',
                'comment' => 'Tablet image',
            ]);
        $connection->addColumn($tableName, 'mobile_image', [
                'type' => Table::TYPE_TEXT,
                'length' => 255,
                'after' => 'tablet_image',
                'comment' => 'Mobile image',
            ]);
        $installer->endSetup();
    }
}

<?php
namespace Xcoding\ShopbyBrand\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
	/**
	 * @param SchemaSetupInterface $setup
	 * @param ModuleContextInterface $context
	 * 
	 * @return void
	 */
	public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
		$setup->startSetup();
        $version = $context->getVersion();
		if (version_compare($version, '1.0.1', '<')) { $this->update1_0_1($setup); }
		if (version_compare($version, '1.0.2', '<')) { $this->update1_0_2($setup); }
		if (version_compare($version, '1.0.3', '<')) { $this->update1_0_3($setup); }

		$setup->endSetup();
	}

	/**
	 * @param SchemaSetupInterface $setup
	 * 
	 * @return void
	 */
    private function update1_0_1($setup)
    {
        $tableName = $setup->getTable('amasty_amshopby_option_setting');
        $connection = $setup->getConnection();
        $connection->addColumn($tableName, 'bottom_video',[
            'type' => 'text',
            'length' => 255,
            'after' => 'tablet_image',
            'comment' => 'Bottom Video',
        ]);
        $connection->addColumn($tableName, 'bottom_image',[
            'type' => 'text',
            'length' => 255,
            'after' => 'bottom_video',
            'comment' => 'Bottom Image',
        ]);
        $connection->addColumn($tableName, 'bottom_description',[
            'type' => 'text',
            'length' => 10000,
            'after' => 'bottom_image',
            'comment' => 'Bottom Description',
        ]);
    }

	/**
	 * @param SchemaSetupInterface $setup
	 * 
	 * @return void
	 */
    private function update1_0_2($setup)
    {
        $tableName = $setup->getTable('amasty_amshopby_option_setting');
        $connection = $setup->getConnection();

        /** Section 1 */
        $connection->addColumn($tableName, 's1_sort_order', [
            'type' => 'smallint',
            'after' => 'is_show_in_slider',
            'comment' => 'Section 1 sort order',
        ]);
        /** Section 1 Box 1 */
        $connection->addColumn($tableName, 'box_1_name', [
            'type' => 'text',
            'length' => 255,
            'after' => 's1_sort_order',
            'comment' => 'Section 1 Box 1 name',
        ]);
        $connection->addColumn($tableName, 'box_1_url', [
            'type' => 'text',
            'length' => 255,
            'after' => 'box_1_name',
            'comment' => 'Section 1 Box 1 url',
        ]);
        $connection->addColumn($tableName, 'box_1_image', [
            'type' => 'text',
            'length' => 255,
            'after' => 'box_1_url',
            'comment' => 'Section 1 Box 1 image',
        ]);
        /** Section 1 Box 2 */
        $connection->addColumn($tableName, 'box_2_name', [
            'type' => 'text',
            'length' => 255,
            'after' => 'box_1_image',
            'comment' => 'Section 1 Box 2 name',
        ]);
        $connection->addColumn($tableName, 'box_2_url', [
            'type' => 'text',
            'length' => 255,
            'after' => 'box_2_name',
            'comment' => 'Section 1 Box 2 url',
        ]);
        $connection->addColumn($tableName, 'box_2_image', [
            'type' => 'text',
            'length' => 255,
            'after' => 'box_2_url',
            'comment' => 'Section 1 Box 2 image',
        ]);
        /** Section 1 Box 3 */
        $connection->addColumn($tableName, 'box_3_name', [
            'type' => 'text',
            'length' => 255,
            'after' => 'box_2_image',
            'comment' => 'Section 1 Box 3 name',
        ]);
        $connection->addColumn($tableName, 'box_3_url', [
            'type' => 'text',
            'length' => 255,
            'after' => 'box_3_name',
            'comment' => 'Section 1 Box 3 url',
        ]);
        $connection->addColumn($tableName, 'box_3_image', [
            'type' => 'text',
            'length' => 255,
            'after' => 'box_3_url',
            'comment' => 'Section 1 Box 3 image',
        ]);

        /** Section 2 */
        $connection->addColumn($tableName, 's2_sort_order', [
            'type' => 'smallint',
            'after' => 'box_3_image',
            'comment' => 'Section 2 sort order',
        ]);
        $connection->addColumn($tableName, 's2_visible', [
            'type' => 'boolean',
            'after' => 's2_sort_order',
            'comment' => 'Section 2 visible',
        ]);
        $connection->addColumn($tableName, 's2_title', [
            'type' => 'text',
            'length' => 255,
            'after' => 's2_visible',
            'comment' => 'Section 2 title',
        ]);

        /** Section 3 */
        $connection->addColumn($tableName, 's3_sort_order', [
            'type' => 'smallint',
            'after' => 's2_title',
            'comment' => 'Section 3 sort order',
        ]);
        $connection->addColumn($tableName, 's3_html', [
            'type' => 'text',
            'length' => 10000,
            'after' => 's3_sort_order',
            'comment' => 'Section 3 html',
        ]);
    }

	/**
	 * @param SchemaSetupInterface $setup
	 * 
	 * @return void
	 */
    private function update1_0_3($setup)
    {
        $tableName = $setup->getTable('amasty_amshopby_option_setting');
        $connection = $setup->getConnection();

        $connection->addColumn($tableName, 'measure_description', [
            'type' => 'text',
            'length' => 10000,
            'after' => 'description',
            'comment' => 'Measure description',
        ]);
        $connection->addColumn($tableName, 'measure_men_img', [
            'type' => 'text',
            'length' => 255,
            'after' => 'measure_description',
            'comment' => 'Measure men image',
        ]);
        $connection->addColumn($tableName, 'measure_women_img', [
            'type' => 'text',
            'length' => 255,
            'after' => 'measure_men_img',
            'comment' => 'Measure women image',
        ]);
        $connection->addColumn($tableName, 'measure_junior_img', [
            'type' => 'text',
            'length' => 255,
            'after' => 'measure_women_img',
            'comment' => 'Measure junior image',
        ]);
        $connection->addColumn($tableName, 'measure_unisex_img', [
            'type' => 'text',
            'length' => 255,
            'after' => 'measure_junior_img',
            'comment' => 'Measure unisex image',
        ]);
    }
}

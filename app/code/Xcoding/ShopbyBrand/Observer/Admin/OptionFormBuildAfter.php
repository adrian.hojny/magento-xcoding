<?php
namespace Xcoding\ShopbyBrand\Observer\Admin;

use Xcoding\ShopbyBrand\Model\OptionSetting;
use Magento\Framework\Data\Form;
use Magento\Framework\Event\ObserverInterface;

class OptionFormBuildAfter implements ObserverInterface
{
    /**
     * @var \Magento\Cms\Model\Wysiwyg\Config
     */
    private $wysiwygConfig;

    public function __construct(
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig
    ) {
        $this->wysiwygConfig = $wysiwygConfig;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var Form $form */
        $form = $observer->getData('form');

        /** @var OptionSetting $setting */
        $setting = $observer->getData('setting');

        $this->addProductListFieldset($form, $setting);
    }

    /**
     * @param Form $form
     * @param OptionSetting $model
     */
    private function addProductListFieldset(\Magento\Framework\Data\Form $form, OptionSetting $model)
    {
        /** @var \Magento\Framework\Data\Form\Element\Fieldset $fieldset */
        $fieldset = $form->getElement('product_list_fieldset');

        $this->addMeasureSection($form, $model);

        $fieldset->addField(
            'tablet_image',
            'file',
            [
                'name' => 'tablet_image',
                'label' => __('Tablet Image'),
                'title' => __('Tablet Image'),
                'after_element_html' => $this->getImageFieldAfterHtml($model, 'tablet_image')
            ], 'image'
        );
        $fieldset->addField(
            'mobile_image',
            'file',
            [
                'name' => 'mobile_image',
                'label' => __('Mobile Image'),
                'title' => __('Mobile Image'),
                'after_element_html' => $this->getImageFieldAfterHtml($model, 'mobile_image')
            ], 'tablet_image'
        );
        $fieldset->addField(
            'bottom_video',
            'file',
            [
                'name' => 'bottom_video',
                'label' => __('Bottom Video'),
                'title' => __('Bottom Video'),
                'after_element_html' => $this->getVideoFieldAfterHtml($model, 'bottom_video', $model->getBottomVideoUrl())
            ], 'bottom_cms_block_id'
        );
        $fieldset->addField(
            'bottom_image',
            'file',
            [
                'name' => 'bottom_image',
                'label' => __('Bottom Image'),
                'title' => __('Bottom Image'),
                'after_element_html' => $this->getImageFieldAfterHtml($model, 'bottom_image')
            ], 'bottom_video'
        );
        $fieldset->addField(
            'bottom_description',
            'editor',
            [
                'name' => 'bottom_description',
                'label' => __('Bottom Description'),
                'title' => __('Bottom Description')
            ], 'bottom_image'
        );
        $this->addSection1($form, $model);
        $this->addSection2($form);
        $this->addSection3($form);
    }

    /**
     * @param Form $form
     * @param OptionSetting $model
     * 
     * @return void
     */
    private function addMeasureSection($form, $model)
    {
        $fieldset = $form->addFieldset(
            "measure_section", [
                'legend' => __('How to measure section'),
                'class'=>'form-inline'
            ]
        );
        $fieldset->addField(
            'measure_description',
            'editor',
            [
                'name' => 'measure_description',
                'label' => __('Description'),
                'title' => __('Description')
            ]
        );
        $fieldset->addField(
            'measure_men_img',
            'file',
            [
                'name' => 'measure_men_img',
                'label' => __('Men Image'),
                'title' => __('Men Image'),
                'after_element_html' => $this->getImageFieldAfterHtml($model, 'measure_men_img')
            ]
        );
        $fieldset->addField(
            'measure_women_img',
            'file',
            [
                'name' => 'measure_women_img',
                'label' => __('Women Image'),
                'title' => __('Women Image'),
                'after_element_html' => $this->getImageFieldAfterHtml($model, 'measure_women_img')
            ]
        );
        $fieldset->addField(
            'measure_junior_img',
            'file',
            [
                'name' => 'measure_junior_img',
                'label' => __('Junior Image'),
                'title' => __('Junior Image'),
                'after_element_html' => $this->getImageFieldAfterHtml($model, 'measure_junior_img')
            ]
        );
        $fieldset->addField(
            'measure_unisex_img',
            'file',
            [
                'name' => 'measure_unisex_img',
                'label' => __('Unisex Image'),
                'title' => __('Unisex Image'),
                'after_element_html' => $this->getImageFieldAfterHtml($model, 'measure_unisex_img')
            ]
        );
    }

    /**
     * @param Form $form
     * @param OptionSetting $model
     * 
     * @return void
     */
    private function addSection1($form, $model)
    {
        $fieldset = $form->addFieldset(
            "section_1", [
                'legend' => __('Section') . " 1",
                'class'=>'form-inline'
            ]
        );
        $this->addBox(1, $fieldset, $model);
        $this->addBox(2, $fieldset, $model);
        $this->addBox(3, $fieldset, $model);

        $fieldset->addField('s1_sort_order', 'text', [
            'name' => 's1_sort_order',
            'label' => __('Sort Order'),
            'title' => __('Sort Order'),
            'class' => 'validate-number',
        ]);
    }

    /**
     * @param int $number
     * @param Form $form
     * @param OptionSetting $model
     * 
     * @return void
     */
    private function addBox($number, $fieldset, $model)
    {
        $name = 'box_' . $number . '_name';
        $labelPrefix = 'Box #' . $number . ' ';
        $fieldset->addField($name, 'text',
            ['name' => $name, 'label' => $labelPrefix . __('Name'), 'title' => $labelPrefix . __('Name')]
        );
        $name = 'box_' . $number . '_url';
        $fieldset->addField($name, 'text',
            ['name' => $name, 'label' => $labelPrefix . __('Url'), 'title' => $labelPrefix . __('Url')]
        );
        $name = 'box_' . $number . '_image';
        $fieldset->addField($name, 'file', [
                'name' => $name,
                'label' => $labelPrefix . __('Image'),
                'title' => $labelPrefix . __('Image'),
                'after_element_html' => $this->getImageFieldAfterHtml($model, $name)
            ]
        );
    }

    /**
     * @param Form $form
     * 
     * @return void
     */
    private function addSection2($form)
    {
        $fieldset = $form->addFieldset(
            "section_2", [
                'legend' => __('Section') . " 2",
                'class'=>'form-inline'
            ]
        );
        $fieldset->addField('s2_visible', 'select', [
            'name' => 's2_visible',
            'label' => __('Display section'),
            'title' => __('Display section'),
            'values' => [['value' => 1, 'label' => __('Yes')], ['value' => 0, 'label' => __('No')]],
        ]);
        $fieldset->addField('s2_title', 'text', [
            'name' => 's2_title',
            'label' => __('Title'),
            'title' => __('Title')
        ]);
        $fieldset->addField('s2_sort_order', 'text', [
            'name' => 's2_sort_order',
            'label' => __('Sort Order'),
            'title' => __('Sort Order'),
            'class' => 'validate-number',
        ]);
    }

    /**
     * @param Form $form
     * 
     * @return void
     */
    private function addSection3($form)
    {
        $fieldset = $form->addFieldset(
            "section_3", [
                'legend' => __('Section') . " 3",
                'class'=>'form-inline'
            ]
        );
        $fieldset->addField('s3_html', 'editor', [
            'name' => 's3_html',
            'label' => __('HTML'),
            'title' => __('HTML'),
            'wysiwyg' => true,
            'config' => $this->wysiwygConfig->getConfig(['add_variables' => false]),
        ]);
        $fieldset->addField('s3_sort_order', 'text', [
            'name' => 's3_sort_order',
            'label' => __('Sort Order'),
            'title' => __('Sort Order'),
            'class' => 'validate-number',
        ]);
    }

    /**
     * @param OptionSetting $model
     * @param string $field
     * @param string $url
     * 
     * @return string
     */
    private function getImageFieldAfterHtml($model, $field, $url = null)
    {
        if (!$url) {
            $url = $model->getCustomImageUrl($field);
            if (!$url) {
                return '';
            }
        }
        $categoryImageUseDefault = $model->getData($field . '_use_default') && $model->getCurrentStoreId();
        return '<div><br><input type="checkbox" id="' . $field . '_delete" name="' . $field . '_delete" value="1" '
        . ($categoryImageUseDefault ? 'disabled="disabled"' : '' ) .
        ' /> <label for="' . $field . '_delete">' . __('Delete Image') . '</label><br><br><img src="'
        . $url . '" style="max-height:100px' .($categoryImageUseDefault ? ' display:none' : '').'"/></div>';
    }

    /**
     * @param OptionSetting $model
     * @param string $field
     * @param string $url
     * 
     * @return string
     */
    private function getVideoFieldAfterHtml($model, $field, $url)
    {
        if (!$url) {
            return '';
        }
        $useDefault = $model->getData($field . '_use_default') && $model->getCurrentStoreId();
        return '<div><br><input type="checkbox" id="' . $field . '_delete" name="' . $field . '_delete" value="1" '
        . ($useDefault ? 'disabled="disabled"' : '' )
        . ' /> <label for="' . $field . '_delete">' . __('Delete Video')
        . ' | <a href="' . $url . '">' . $model->getData($field) . '</a>'
        . '</label><br><br></div>';
    }
}

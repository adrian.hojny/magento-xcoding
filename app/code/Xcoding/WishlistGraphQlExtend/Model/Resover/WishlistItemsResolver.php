<?php

namespace Xcoding\WishlistGraphQlExtend\Model\Resover;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Exception\LocalizedException;
use Magento\Wishlist\Model\Item;
use ScandiPWA\WishlistGraphQl\Model\Resolver\WishlistItemsResolver as ScandiWishlistItemsResolver;

/**
 * Class WishlistItemsResolver
 * @package Xcoding\WishlistGraphQlExtend\Model\Resover
 */
class WishlistItemsResolver extends ScandiWishlistItemsResolver
{
    /**
     * Get wish-list item's sku
     *
     * @param Item $wishlistItem
     * @return string
     * @throws LocalizedException
     */
    protected function getWishListItemSku(
        Item $wishlistItem
    ): string {
        $product = $wishlistItem->getProduct();
        if ($product->getTypeId() === Configurable::TYPE_CODE) {
            $variantAttribute = $wishlistItem->getOptionByCode('simple_product');
            $variantId = $variantAttribute ? $variantAttribute->getValue() : null;
            if (null !== $variantId) {
                $childProduct = $this->productFactory->create()->load($variantId);

                return $childProduct->getSku();
            }
        }

        return $product->getSku();
    }
}

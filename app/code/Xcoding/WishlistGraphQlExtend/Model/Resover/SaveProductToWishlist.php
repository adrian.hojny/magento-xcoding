<?php

namespace Xcoding\WishlistGraphQlExtend\Model\Resover;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Wishlist\Model\Wishlist;
use ScandiPWA\WishlistGraphQl\Model\Resolver\SaveProductToWishlist as ScandiSaveProductToWishlist;

/**
 * Class SaveProductToWishlist
 * @package Xcoding\WishlistGraphQlExtend\Model\Resover
 */
class SaveProductToWishlist extends ScandiSaveProductToWishlist
{
    /**
     * @param Wishlist $wishlist
     * @param string $sku
     * @param array $parameters
     * @return array
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function addProductToWishlist(Wishlist $wishlist, string $sku, array $parameters)
    {
        $quantity = $parameters['quantity'] || 1;
        $description = $parameters['description'] ?? '';
        $productOption = $parameters['product_option'] ?? [];

        $product = $this->productRepository->get($sku);
        if (!$product->isVisibleInCatalog()) {
            throw new GraphQlInputException(__('Please specify valid product'));
        }

        try {
            $configurableData = [];
            if ($product->getTypeId() === Configurable::TYPE_CODE) {
                if (isset($productOption['extension_attributes']['configurable_item_options'])) {
                    $configurableOptions = $this->getOptionsArray($productOption['extension_attributes']['configurable_item_options']);
                    $configurableData['super_attribute'] = $configurableOptions;
                }
            }

            $wishlistItem = $wishlist->addNewItem($product, $configurableData);
            $wishlistItem->setDescription($description);
            $wishlistItem->setQty($quantity);

            $wishlist->save();
        } catch (Exception $e) {
            throw new GraphQlNoSuchEntityException(__('There was an error when trying to save wishlist'));
        }

        if ($wishlistItem->getProductId() === null) {
            return [];
        }

        return array_merge(
            $wishlistItem->getData(),
            ['model' => $wishlistItem],
            [
                'product' => array_merge(
                        $wishlistItem->getProduct()->getData(),
                        ['model' => $product]
                    ),
            ]
        );
    }
}

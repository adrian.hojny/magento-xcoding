<?php
declare(strict_types=1);

namespace Xcoding\CommingSoonProduct\Plugin\Magento\Quote\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote as BaseQuote;
use Magento\Quote\Model\Quote\Item;
use Xcoding\CommingSoonProduct\Model\Command\ProductCommingSoon;

class Quote
{
    /**
     * @var ProductCommingSoon
     */
    protected $productCommingSoon;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    public function __construct(
        ProductCommingSoon $productCommingSoon,
        ProductRepositoryInterface $productRepository
    ) {
        $this->productCommingSoon = $productCommingSoon;
        $this->productRepository = $productRepository;
    }

    /**
     * @param BaseQuote $subject
     * @param Item $item
     * @return null
     * @throws LocalizedException
     */
    public function beforeAddItem(BaseQuote $subject, Item $item) {
        try {
            $product = $this->productRepository->get($item->getSku());
        } catch (NoSuchEntityException $e) {
            // product is validated later
            return null;
        }

        if ($this->productCommingSoon->execute($product)) {
            throw new LocalizedException(
                __('Product that you are trying to add is not available.')
            );
        }

        return null;
    }
}

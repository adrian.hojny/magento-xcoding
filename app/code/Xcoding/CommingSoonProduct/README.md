# Xcoding_CommingSoonProduct

Ustawianie produktu jako "comming soon" odbywa się w panelu admina.
`catalog > product > [edit]` ustawiamy oba atrybuty: `Comming soon` oraz `Comming soon date`

#### Pobieranie informacji o dostępności produktu

Przykładowe zapytanie:
```graphql
query {
  products(
    pageSize:10,
    currentPage: 10,
    filter: {
        id: {
          from: "0"
        }
    }
  ) {
    items {
      sku
      name
      comming_soon
      comming_soon_date
    }
  }
}
```

Przykładowa odpowiedź:
```json
{
  "data": {
    "products": {
      "items": [
        {
          "sku": "n31091479",
          "name": "People of Shibuya Transitional Jacket",
          "comming_soon": null,
          "comming_soon_date": null
        },
        {
          "sku": "n31276018",
          "name": "J.B by Jean Biani Sneakers",
          "comming_soon": 1,
          "comming_soon_date": "2020-09-26T12:37:00+02:00"
        },
        {
          "sku": "n31350783",
          "name": "Cocktail Dress",
          "comming_soon": null,
          "comming_soon_date": null
        },
        {
          "sku": "grouped-01",
          "name": "testowy grupowy",
          "comming_soon": null,
          "comming_soon_date": null
        },
        {
          "sku": "aaaaaaan",
          "name": "aaaaaaan",
          "comming_soon": 1,
          "comming_soon_date": "2020-09-25T11:42:00+02:00"
        }
      ]
    }
  }
}
```

Walidacja dodawania do koszyka odbywa się w całości w backendzie (dla obu mutacji: `addSimpleProductsToCart` i `saveCartItem`).
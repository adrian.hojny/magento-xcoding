<?php

namespace Xcoding\CommingSoonProduct\Model\Resolver\Product;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class CommingSoonIsoDate implements ResolverInterface
{
    /**
     * @var TimezoneInterface
     */
    protected $date;

    public function __construct(
        TimezoneInterface $date
    ) {
        $this->date = $date;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        if (!isset($value['model'])) {
            throw new LocalizedException(__('"model" value should be specified'));
        }

        if (isset($value['comming_soon_date']) && $value['comming_soon_date']) {
            $commingSoonDate = \DateTime::createFromFormat('Y-m-d H:i:s', $value['comming_soon_date']);
            return $this->date->date($commingSoonDate)->format('c');
        }

        return $value['comming_soon_date'] ?? null;
    }
}
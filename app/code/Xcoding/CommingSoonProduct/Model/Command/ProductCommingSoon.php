<?php declare(strict_types=1);

namespace Xcoding\CommingSoonProduct\Model\Command;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

class ProductCommingSoon
{
    /**
     * @var TimezoneInterface
     */
    protected $date;

    public function __construct(
        TimezoneInterface $date
    ) {
        $this->date = $date;
    }

    public function execute(ProductInterface $product)
    {
        if ($product->getCommingSoon()) {
            $commingSoonDate = \DateTime::createFromFormat('Y-m-d H:i:s', $product->getCommingSoonDate());
            return $this->date->date($commingSoonDate) > $this->date->date();
        }

        return false;
    }
}
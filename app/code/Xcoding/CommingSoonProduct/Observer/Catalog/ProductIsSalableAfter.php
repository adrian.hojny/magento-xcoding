<?php
declare(strict_types=1);

namespace Xcoding\CommingSoonProduct\Observer\Catalog;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Xcoding\CommingSoonProduct\Model\Command\ProductCommingSoon;

class ProductIsSalableAfter implements ObserverInterface
{
    /**
     * @var ProductCommingSoon
     */
    protected $productCommingSoon;

    public function __construct(
        ProductCommingSoon $productCommingSoon
    ) {
        $this->productCommingSoon = $productCommingSoon;
    }

    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(
        Observer $observer
    ) {
        $product = $observer->getEvent()->getProduct();
        $salable = $observer->getEvent()->getSalable();

        if ($this->productCommingSoon->execute($product)) {
            $salable->setIsSalable(false);
        }
    }
}

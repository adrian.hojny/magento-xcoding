<?php
namespace Xcoding\Tracking\Cron;

use Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory as TrackCollectionFactory;

class Fedex {
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \SoapClient
     */
    protected $client;

    /**
     * @var TrackCollectionFactory
     */
    protected $trackCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        TrackCollectionFactory $trackCollectionFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->trackCollectionFactory = $trackCollectionFactory;
    }

   /**
    * @return void
    */
    public function execute() {
        $collection = $this->trackCollectionFactory->create();
        $collection->addFieldToFilter('carrier_code', 'fedex');
        foreach ($collection as $track) {
            /** @var \Magento\Sales\Model\Order\Shipment\Track $track */
            $data = $this->track($track->getTrackNumber());
            if (!$data) {
                continue;
            }
            var_dump($data->Events);
            var_dump($data->Events->EventType);
            var_dump($data->Events->EventDescription);
        }
    }

    /**
     * Retrieve information from fedex configuration
     *
     * @param   string $field
     * @return  false|string
     */
    public function getConfigData($field)
    {
        return $this->scopeConfig->getValue(
            'carriers/fedex/' . $field,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param string $tracking
     * 
     * @return \stdClass|false
     */
    private function track($tracking)
    {
        $request['WebAuthenticationDetail'] = array(
            'UserCredential' => array(
                'Key' => $this->getConfigData('key'),
                'Password' => $this->getConfigData('password'),
            )
        );

        $request['ClientDetail'] = array(
            'AccountNumber' => $this->getConfigData('account'),
            'MeterNumber' => $this->getConfigData('meter_number'),
        );
        $request['Version'] = array(
            'ServiceId' => 'trck', 
            'Major' => '19', 
            'Intermediate' => '0', 
            'Minor' => '0'
        );
        $request['SelectionDetails'] = array(
            'PackageIdentifier' => array(
                'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
                'Value' => '122816215025810'
            )
        );
        try {
            $response = $this->getSoapClient()->track($request);
        } catch (\Exception $e) {
            return false;
        }
        if (!$response || !$response->HighestSeverity || ($response->HighestSeverity != 'SUCCESS')) {
            return false;
        }
        return $response->CompletedTrackDetails->TrackDetails;
    }

    protected function getSoapClient()
    {
        if (!$this->client) {
            $this->client = new \SoapClient(dirname(__DIR__).'/lib/TrackService_v19.wsdl', array('trace' => 1));
            $url = $this->getConfigFlag('sandbox_mode') ?
                $this->getConfigData('sandbox_webservices_url') :
                $this->getConfigData('production_webservices_url');
            $this->client->__setLocation($url);
        }
        return $this->client;
    }

    private function getConfigFlag($flag)
    {
        return $this->scopeConfig->isSetFlag(
            'carriers/fedex/' . $flag,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
<?php
namespace Xcoding\Tracking\Controller\Adminhtml\Map;

use Magento\Backend\App\Action\Context;
use Xcoding\Tracking\Model\MappingFactory;
use Xcoding\Tracking\Model\ResourceModel\Mapping as MappingResource;

class Save extends \Magento\Backend\App\Action implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var MappingFactory
     */
    private $mappingFactory;

    /**
     * @var MappingResource
     */
    private $mappingResource;

    /**
     * @param MappingFactory $mappingFactory
     * @param MappingResource $mappingResource
     * @param Context $context
     */
    public function __construct(
        MappingFactory $mappingFactory,
        MappingResource $mappingResource,
        Context $context
    ) {
        $this->mappingFactory = $mappingFactory;
        $this->mappingResource = $mappingResource;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Framework\App\Request\Http $request */
        $request = $this->getRequest();
        $data = $request->getPostValue();
        $this->_redirect('*/*/index');
        if (!$data) {
            return $this->_redirect('*/*/index');
        }

        /** @var \Xcoding\Tracking\Model\Mapping $model */
        $model = $this->mappingFactory->create();
        if ($data['id']) {
            $this->mappingResource->load($model, $data['id']);
        }

        $fieldsToUpdate = ['courier_code', 'tracking_code', 'status'];
        foreach ($fieldsToUpdate as $field) {
            $model->setData($field, $data[$field]);
        }
        $this->mappingResource->save($model);
        $this->messageManager->addSuccessMessage(__('Saved successfully'));
        return $this->_redirect('*/*/index');
    }
}

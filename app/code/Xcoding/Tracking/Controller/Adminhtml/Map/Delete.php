<?php
namespace Xcoding\Tracking\Controller\Adminhtml\Map;

use Magento\Backend\App\Action\Context;
use Xcoding\Tracking\Model\MappingFactory;
use Xcoding\Tracking\Model\ResourceModel\Mapping as MappingResource;

class Delete extends \Magento\Backend\App\Action implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var MappingFactory
     */
    private $mappingFactory;

    /**
     * @var MappingResource
     */
    private $mappingResource;

    /**
     * @param MappingFactory $mappingFactory
     * @param MappingResource $mappingResource
     * @param Context $context
     */
    public function __construct(
        MappingFactory $mappingFactory,
        MappingResource $mappingResource,
        Context $context
    ) {
        $this->mappingFactory = $mappingFactory;
        $this->mappingResource = $mappingResource;
        parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        if (!$id) {
            return $this->_redirect('*/*/index');
        }
        try {
            $model = $this->mappingFactory->create();
            $this->mappingResource->load($model, $id);
            $this->mappingResource->delete($model);
            $this->messageManager->addSuccessMessage(__('Deleted successfully.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $this->_redirect('*/*/index');
    }
}

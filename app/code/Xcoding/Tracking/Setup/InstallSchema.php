<?php
namespace Xcoding\Tracking\Setup;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{

	public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
	{
		$installer = $setup;
		$installer->startSetup();
		if (!$installer->tableExists('xcoding_tracking_mapping')) {
			$table = $installer->getConnection()->newTable($installer->getTable('xcoding_tracking_mapping'))
				->addColumn(
					'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Mapping entity ID'
				)
				->addColumn(
					'courier_code',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					['nullable => false'],
					'Courier method code'
				)
				->addColumn(
					'tracking_code',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Tracking code'
				)
				->addColumn(
					'status',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[],
					'Status code'
				)
				->setComment('Tracking status mapping');
			$installer->getConnection()->createTable($table);
		}
		$installer->endSetup();
	}
}
<?php
namespace Xcoding\Tracking\Model\ResourceModel\Mapping;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'xcoding_tracking_mapping_collection';
	protected $_eventObject = 'mapping_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Xcoding\Tracking\Model\Mapping', 'Xcoding\Tracking\Model\ResourceModel\Mapping');
	}
}
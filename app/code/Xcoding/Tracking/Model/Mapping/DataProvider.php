<?php
declare(strict_types=1);

namespace Xcoding\Tracking\Model\Mapping;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var array
     */
    protected $loadedData;

    /**
     * RegistrationDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Xcoding\Tracking\Model\ResourceModel\Mapping\CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Xcoding\Tracking\Model\ResourceModel\Mapping\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->getCollection()->getItems();
        $model = array_shift($items);
        if ($model) {
            $data = $model->getData();
            $this->loadedData = [$model->getId() => $data];
        } else {
            $this->loadedData = [];
        }
        return $this->loadedData;
    }
}


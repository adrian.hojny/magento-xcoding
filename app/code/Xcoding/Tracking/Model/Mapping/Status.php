<?php
namespace Xcoding\Tracking\Model\Mapping;

class Status implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        $statuses = \Xcoding\Tracking\Model\Mapping::STATUSES;
        $result = [];
        foreach ($statuses as $code => $label) {
            $result[] = ['value' => $code, 'label' => $label];
        }
        return $result;
    }
}

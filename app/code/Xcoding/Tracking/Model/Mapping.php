<?php
namespace Xcoding\Tracking\Model;

class Mapping extends \Magento\Framework\Model\AbstractModel
{
    public const STATUSES = [
        'waiting' => 'Waiting',
        'in_transit' => 'In transit',
        'delivered' => 'Delivered',
	];

	protected function _construct()
	{
		$this->_init('Xcoding\Tracking\Model\ResourceModel\Mapping');
	}
}
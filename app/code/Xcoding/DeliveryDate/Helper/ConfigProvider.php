<?php

namespace Xcoding\DeliveryDate\Helper;

use DateTime;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ConfigProvider
 * @package Xcoding\DeliveryDate\Helper
 */
class ConfigProvider extends AbstractHelper
{
    public const XML_PATH_CUT_OFF_TIME = 'shipping/delivery_date/cut_of_time';
    public const XML_PATH_NON_WORKING_DAYS = 'shipping/delivery_date/non_working_days';
    public const DEFAULT_CUT_OFF_TIME_CONFIG_VALUE = '14,00,00';

    private $nonWorkingDays = null;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * ConfigProvider constructor.
     * @param Context $context
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        Context $context,
        TimezoneInterface $timezone
    ) {
        $this->timezone = $timezone;
        parent::__construct($context);
    }

    /**
     * @return DateTime|null ?DateTime today cut off time
     */
    public function getCutOffTime(): ?DateTime
    {
        $configValue = $this->scopeConfig->getValue(
            self::XML_PATH_CUT_OFF_TIME,
            ScopeInterface::SCOPE_STORE
        );

        if (!$configValue) {
            $configValue = static::DEFAULT_CUT_OFF_TIME_CONFIG_VALUE;
        }

        list($hour, $minutes, $seconds) = explode(',', $configValue);

        $dateTime = $this->timezone->date()->setTime($hour, $minutes, $seconds);
        return $dateTime ? $dateTime : null;
    }

    /**
     * @return DateTime[]
     */
    public function getNonWorkingDays(): array
    {
        if (null === $this->nonWorkingDays) {
            $nonWorkingDays = [];

            $configValue = $this->scopeConfig->getValue(
                self::XML_PATH_NON_WORKING_DAYS,
                ScopeInterface::SCOPE_STORE
            );
            $dates = explode(',', $configValue);

            foreach ($dates as $date) {
                $dateTime = DateTime::createFromFormat('m/d/Y', $date);
                if ($dateTime) {
                    $nonWorkingDays[] = $dateTime;
                }
            }

            $this->nonWorkingDays = $nonWorkingDays;
        }

        return $this->nonWorkingDays;
    }
}

#  Xcoding_DeliveryDate

Moduł obliczania daty dostawy produktu

#### API
- `GetDeliveryTimeInterface` - pobieranie dat dostawy
- `FormatDatesInterface` - formatowanie dat na jeden ze sposobów
    - `DATE` - dokładne daty w formacie d-m-Y np. `15.03.2020/01.04.2020`
    - `WEEKDAY` - pełna nazwa dnia tygodnia, jeśli aktualną datę i formatowaną dzieli więcej niż 6 dni - zwracana jest w formacie `DATE`
    - `DAYS_FROM_NOW` - dzisiaj, jutro, pojutrze lub w formacie `DATE`
    
#### Rozszerzanie funkcjonalności
- datę dostawy możemy modyfikować przez zaimplementowanie `DeliveryDateModifierInterface` i dodanie implementacji do `deliveryDateModifiers` w di.xml
- dodanie nowego formatowania dat odbywa się przez zaimplementowanie `DeliveryDateFormatterInterface` i dodanie do `formatters` w di.xml
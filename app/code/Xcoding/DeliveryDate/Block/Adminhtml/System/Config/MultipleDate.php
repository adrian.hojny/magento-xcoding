<?php

namespace Xcoding\DeliveryDate\Block\Adminhtml\System\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class MultipleDate
 * @package Xcoding\DeliveryDate\Block\Adminhtml\System\Config
 */
class MultipleDate extends Field
{
    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(
        AbstractElement $element
    ) {
        $html = $element->getElementHtml();
        $html .= sprintf('<script type="text/javascript">
            require(["jquery", "multi-date-picker","domReady!"], function ($) {
                var $el = $("#%s");
                $el.multiDatesPicker({
                    beforeShowDay: $.datepicker.noWeekends,
                    beforeShow: function( input, inst) {
                      $(inst.dpDiv).addClass("multi-date-picker");
                    }
                });
                $el.attr("readonly", true);

                $.datepicker._selectDateOverload = $.datepicker._selectDate;
                $.datepicker._selectDate = function(id, dateStr) {
                    var target = $(id);
                    var inst = this._getInst(target[0]);
                    inst.inline = true;
                    $.datepicker._selectDateOverload(id, dateStr);
                    inst.inline = false;
                    this._updateDatepicker(inst);
                };
            });
            </script>', $element->getHtmlId());

        return $html;
    }
}

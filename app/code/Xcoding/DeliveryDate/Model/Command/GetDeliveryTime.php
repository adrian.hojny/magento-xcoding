<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\Command;

use DateTime;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Xcoding\DeliveryDate\Api\GetDeliveryTimeInterface;
use Xcoding\DeliveryDate\Helper\ConfigProvider;
use Xcoding\DeliveryDate\Model\DeliveryDate\Modifier\DeliveryDateModifierInterface;

/**
 * Class GetDeliveryTime
 * @package Xcoding\DeliveryDate\Model\Command
 */
class GetDeliveryTime implements GetDeliveryTimeInterface
{
    /**
     * @var DeliveryDateModifierInterface[]
     */
    private $deliveryDateModifiers;

    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var GetNextWorkingDay
     */
    private $getNextWorkingDay;

    /**
     * GetDeliveryTime constructor.
     * @param DeliveryDateModifierInterface[] $deliveryDateModifiers
     * @param TimezoneInterface $timezone
     * @param ConfigProvider $configProvider
     * @param GetNextWorkingDay $nextWorkingDay
     */
    public function __construct(
        array $deliveryDateModifiers,
        TimezoneInterface $timezone,
        ConfigProvider $configProvider,
        GetNextWorkingDay $nextWorkingDay
    ) {
        $this->deliveryDateModifiers = $deliveryDateModifiers;
        $this->timezone = $timezone;
        $this->configProvider = $configProvider;
        $this->getNextWorkingDay = $nextWorkingDay;
    }

    /**
     * {@inheritDoc}
     */
    public function execute(ProductInterface $product): ?array
    {
        $firstDay = $this->getFirstDateCandidate($product);
        if (!$firstDay) {
            return null;
        }

        $firstDay = $this->getNextWorkingDay->execute($firstDay, false);
        $secondDay = $this->getNextWorkingDay->execute($firstDay);

        return [$firstDay, $secondDay];
    }

    /**
     * @param ProductInterface $product
     * @return DateTime|null
     */
    protected function getFirstDateCandidate(ProductInterface $product)
    {
        $dateTime = $this->timezone->date();

        $this->sortModifiers();
        foreach ($this->deliveryDateModifiers as $modifierData) {
            $modifier = $modifierData['class'] ?? null;
            if ($modifier instanceof DeliveryDateModifierInterface) {
                $dateTime = $modifier->modifyDate($dateTime, $product);
            }
        }

        return $dateTime ? $dateTime : null;
    }

    protected function sortModifiers(): void
    {
        usort($this->deliveryDateModifiers, function ($a, $b) {
            $orderA = $a['sortOrder'] ?? 0;
            $orderB = $b['sortOrder'] ?? 0;
            return $orderA - $orderB;
        });
    }
}

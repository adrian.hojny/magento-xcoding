<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\Command;

use Magento\Framework\Exception\LocalizedException;
use Xcoding\DeliveryDate\Api\FormatDatesInterface;
use Xcoding\DeliveryDate\Model\DeliveryDate\Formatter\DeliveryDateFormatterInterface;

/**
 * Class FormatDates
 * @package Xcoding\DeliveryDate\Model\Command
 */
class FormatDates implements FormatDatesInterface
{
    /**
     * @var DeliveryDateFormatterInterface[]
     */
    private $formatters;

    public function __construct(array $formatters)
    {
        $this->formatters = $formatters;
    }

    /**
     * @param array $dates
     * @param string $format
     * @return string[]
     * @throws LocalizedException
     */
    public function execute(array $dates = [], $format = self::DEFAULT_FORMAT): array
    {
        if (null === $format) {
            $format = self::DEFAULT_FORMAT;
        }

        if (!isset($this->formatters[$format])) {
            throw new LocalizedException(__('Format %1 does not exist', $format));
        }

        return $this->formatters[$format]->formatDates($dates);
    }
}

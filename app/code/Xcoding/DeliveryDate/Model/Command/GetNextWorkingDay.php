<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\Command;

use DateInterval;
use DateTime;
use Exception;
use Xcoding\DeliveryDate\Helper\ConfigProvider;

/**
 * Class GetNextWorkingDay
 * @package Xcoding\DeliveryDate\Model\Command
 */
class GetNextWorkingDay
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * GetNextWorkingDay constructor.
     * @param ConfigProvider $configProvider
     */
    public function __construct(
        ConfigProvider $configProvider
    ) {
        $this->configProvider = $configProvider;
    }

    /**
     * Get next working day after date without modifying param
     *
     * @param DateTime $dateTime
     * @param bool $forceNextDay false if param should be returned when is working day
     * @return DateTime
     * @throws Exception
     */
    public function execute(DateTime $dateTime, bool $forceNextDay = true)
    {
        $date = clone $dateTime;

        if ($forceNextDay) {
            $date->add(new DateInterval('P1D'));
        }

        while (!$this->isWorkingDay($date)) {
            $date->add(new DateInterval('P1D'));
        }

        return $date;
    }

    /**
     * @param DateTime $dateTime
     * @return bool
     */
    public function isWorkingDay(DateTime $dateTime)
    {
        $isWeekend = $dateTime->format('N') >= 6;
        $excludedDays = array_map(function (DateTime $excludedDateTime) {
            return $excludedDateTime->format('d-m-Y');
        }, $this->configProvider->getNonWorkingDays());

        return !$isWeekend && !in_array($dateTime->format('d-m-Y'), $excludedDays, true);
    }
}

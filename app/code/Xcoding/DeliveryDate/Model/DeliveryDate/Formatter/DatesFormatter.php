<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Formatter;

/**
 * Class DatesFormatter
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Formatter
 */
class DatesFormatter implements DeliveryDateFormatterInterface
{
    /**
     * {@inheritDoc}
     */
    public function formatDates(array $dates = []): array
    {
        $formattedDates = array_map(function ($date) {
            return $date->format('d.m.Y');
        }, $dates);

        return $formattedDates;
    }
}

<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Formatter;

/**
 * Class IsoDateFormatter
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Formatter
 */
class IsoDateFormatter implements DeliveryDateFormatterInterface
{
    /**
     * {@inheritDoc}
     */
    public function formatDates(array $dates = []): array
    {
        $formattedDates = array_map(function ($date) {
            return $date->setTime(0, 0, 0)->format('c');
        }, $dates);

        return $formattedDates;
    }
}

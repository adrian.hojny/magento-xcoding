<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Formatter;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class DaysFromNowFormatter
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Formatter
 */
class DaysFromNowFormatter implements DeliveryDateFormatterInterface
{
    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * DaysFromNowFormatter constructor.
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        TimezoneInterface $timezone
    ) {
        $this->timezone = $timezone;
    }

    /**
     * {@inheritDoc}
     */
    public function formatDates(array $dates = []): array
    {
        $today = $this->timezone->date();

        $formattedDates = array_map(function ($date) use ($today) {
            $diff = $today->diff($date);
            $diffInDays = $diff->format('%a');
            switch ($diffInDays) {
                case '0':
                    return __('Today');
                case '1':
                    return __('Tomorrow');
                case '2':
                    return __('Day after tomorrow');
                default:
                    return $date->format('d.m.Y');
            }
        }, $dates);

        return $formattedDates;
    }
}

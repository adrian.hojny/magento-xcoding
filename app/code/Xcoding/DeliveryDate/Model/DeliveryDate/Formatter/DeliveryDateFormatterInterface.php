<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Formatter;

use DateTime;

/**
 * Interface DeliveryDateModifierInterface
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Modifier
 */
interface DeliveryDateFormatterInterface
{
    /**
     * @param DateTime[] $dates
     * @return string[]
     */
    public function formatDates(array $dates = []): array;
}

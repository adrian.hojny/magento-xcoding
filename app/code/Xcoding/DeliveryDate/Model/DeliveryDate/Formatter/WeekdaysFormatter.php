<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Formatter;

use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class WeekdaysFormatter
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Formatter
 */
class WeekdaysFormatter implements DeliveryDateFormatterInterface
{
    /**
     * @var TimezoneInterface
     */
    private $timezone;

    /**
     * DaysFromNowFormatter constructor.
     * @param TimezoneInterface $timezone
     */
    public function __construct(
        TimezoneInterface $timezone
    ) {
        $this->timezone = $timezone;
    }

    /**
     * {@inheritDoc}
     */
    public function formatDates(array $dates = []): array
    {
        $today = $this->timezone->date();

        $formattedDates = array_map(function ($date) use ($today) {
            $diff = $today->diff($date);
            $diffInDays = $diff->format('%a');
            // todo check if full week
            if ((int) $diffInDays > 6) {
                return $date->format('d.m.Y');
            }

            return __($date->format('l'));
        }, $dates);

        return $formattedDates;
    }
}

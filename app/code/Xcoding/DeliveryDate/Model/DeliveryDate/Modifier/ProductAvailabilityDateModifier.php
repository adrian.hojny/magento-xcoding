<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Modifier;

use DateTime;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class ProductAvailabilityDateModifier
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Modifier
 */
class ProductAvailabilityDateModifier implements DeliveryDateModifierInterface
{
    /**
     * {@inheritDoc}
     */
    public function modifyDate(DateTime $dateTime, ProductInterface $product): DateTime
    {
        return $dateTime;
    }
}

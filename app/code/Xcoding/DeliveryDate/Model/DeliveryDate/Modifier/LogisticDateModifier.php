<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Modifier;

use DateTime;
use Magento\Catalog\Api\Data\ProductInterface;
use Xcoding\DeliveryDate\Model\Command\GetNextWorkingDay;

/**
 * Class LogisticDateModifier
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Modifier
 */
class LogisticDateModifier implements DeliveryDateModifierInterface
{
    /**
     * @var GetNextWorkingDay
     */
    private $getNextWorkingDay;

    /**
     * LogisticDateModifier constructor.
     * @param GetNextWorkingDay $getNextWorkingDay
     */
    public function __construct(
        GetNextWorkingDay $getNextWorkingDay
    ) {
        $this->getNextWorkingDay = $getNextWorkingDay;
    }

    /**
     * {@inheritDoc}
     */
    public function modifyDate(DateTime $dateTime, ProductInterface $product): DateTime
    {
        return $this->getNextWorkingDay->execute($dateTime);
    }
}

<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Modifier;

use DateTime;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Interface DeliveryDateModifierInterface
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Modifier
 */
interface DeliveryDateModifierInterface
{
    /**
     * @param DateTime $dateTime
     * @param ProductInterface $product
     * @return DateTime
     */
    public function modifyDate(DateTime $dateTime, ProductInterface $product): DateTime;
}

<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Model\DeliveryDate\Modifier;

use DateTime;
use Magento\Catalog\Api\Data\ProductInterface;
use Xcoding\DeliveryDate\Helper\ConfigProvider;
use Xcoding\DeliveryDate\Model\Command\GetNextWorkingDay;

/**
 * Class CutOffDateModifier
 * @package Xcoding\DeliveryDate\Model\DeliveryDate\Modifier
 */
class CutOffDateModifier implements DeliveryDateModifierInterface
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var GetNextWorkingDay
     */
    private $getNextWorkingDay;

    /**
     * CutOffDateModifier constructor.
     * @param ConfigProvider $configProvider
     * @param GetNextWorkingDay $getNextWorkingDay
     */
    public function __construct(
        ConfigProvider $configProvider,
        GetNextWorkingDay $getNextWorkingDay
    ) {
        $this->configProvider = $configProvider;
        $this->getNextWorkingDay = $getNextWorkingDay;
    }

    /**
     * {@inheritDoc}
     */
    public function modifyDate(DateTime $dateTime, ProductInterface $product): DateTime
    {
        $cutOffTime = $this->configProvider->getCutOffTime();

        if ($cutOffTime <= $dateTime || !$this->getNextWorkingDay->isWorkingDay($dateTime)) {
            $dateTime = $this->getNextWorkingDay->execute($dateTime);
        }

        return $dateTime;
    }
}

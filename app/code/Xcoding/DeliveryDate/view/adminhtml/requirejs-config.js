var config = {
    paths: {
        'multi-date-picker': 'Xcoding_DeliveryDate/js/jquery-ui.multidatespicker'
    },
    shim: {
        'multi-date-picker': {
            deps: ['jquery', 'jquery/ui']
        }
    }
};
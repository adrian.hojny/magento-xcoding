<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Api;

use DateTime;
use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Interface GetDeliveryTime
 * @package Xcoding\DeliveryDate\Api
 */
interface GetDeliveryTimeInterface
{
    /**
     * @param ProductInterface $product
     * @return DateTime[]|null
     */
    public function execute(ProductInterface $product): ?array;
}

<?php declare(strict_types=1);

namespace Xcoding\DeliveryDate\Api;

use Magento\Framework\Exception\LocalizedException;

/**
 * Interface FormatDatesInterface
 * @package Xcoding\DeliveryDate\Api
 */
interface FormatDatesInterface
{
    public const DEFAULT_FORMAT = 'ISO_DATE';

    /**
     * @param array $dates
     * @param string $format
     * @throws LocalizedException
     * @return string[]
     */
    public function execute(array $dates = [], $format = self::DEFAULT_FORMAT): array;
}

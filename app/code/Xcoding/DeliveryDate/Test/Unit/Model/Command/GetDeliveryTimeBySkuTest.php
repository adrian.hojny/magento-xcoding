<?php

namespace Xcoding\DeliveryDate\Test\Unit\Model\Command;

use DateTime;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Xcoding\DeliveryDate\Helper\ConfigProvider;
use Xcoding\DeliveryDate\Model\Command\GetDeliveryTime;
use Xcoding\DeliveryDate\Model\Command\GetNextWorkingDay;
use Xcoding\DeliveryDate\Model\DeliveryDate\Modifier\DeliveryDateModifierInterface;

class GetDeliveryTimeBySkuTest extends TestCase
{
    /**
     * @var MockObject|TimezoneInterface
     */
    private $timezoneMock;

    /**
     * @var MockObject|ProductRepositoryInterface
     */
    private $productRepositoryMock;

    /**
     * @var GetDeliveryTime
     */
    private $getDeliveryTime;

    /**
     * @var MockObject|ConfigProvider
     */
    private $configProviderMock;

    /**
     * @var MockObject|DeliveryDateModifierInterface
     */
    private $modifier1;

    /**
     * @var MockObject|ProductInterface
     */
    private $productMock;

    /**
     * @var MockObject|GetNextWorkingDay
     */
    private $getNextWorkingDay;

    protected function setUp()
    {
        $this->timezoneMock = $this->getMockBuilder(TimezoneInterface::class)
            ->getMockForAbstractClass();

        $this->productMock = $this->getMockBuilder(ProductInterface::class)
            ->getMockForAbstractClass();

        $this->getNextWorkingDay = $this->getMockBuilder(GetNextWorkingDay::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->configProviderMock = $this->getMockBuilder(ConfigProvider::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->modifier1 = $this->getMockBuilder(DeliveryDateModifierInterface::class)
            ->getMockForAbstractClass();

        $modifiers = [
            [
                'sortOrder' => 10,
                'class' => $this->modifier1
            ]
        ];

        $this->getDeliveryTime = new GetDeliveryTime(
            $modifiers,
            $this->timezoneMock,
            $this->configProviderMock,
            $this->getNextWorkingDay
        );
    }

    public function testExecute()
    {
        $startingDate = $this->getMockBuilder(DateTime::class)
            ->disableOriginalConstructor()
            ->getMock();

        $modifiedDate = $this->getMockBuilder(DateTime::class)
            ->disableOriginalConstructor()
            ->getMock();

        // calculated date is friday
        $modifiedDate->expects($this->any())->method('format')->willReturn(5);

        $this->timezoneMock->expects($this->once())->method('date')->willReturn($startingDate);
        $this->modifier1->expects($this->once())->method('modifyDate')->willReturn($modifiedDate);
        $this->getNextWorkingDay->expects($this->any())->method('execute')->willReturn($startingDate);

        $this->configProviderMock->expects($this->any())->method('getNonWorkingDays')->willReturn([]);

        $result = $this->getDeliveryTime->execute($this->productMock);
        $this->assertNotEmpty($result);
    }
}

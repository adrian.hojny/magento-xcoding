<?php

namespace Xcoding\DeliveryDate\Test\Unit\Helper;

use DateTime;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Xcoding\DeliveryDate\Helper\ConfigProvider;

/**
 * Class ConfigProviderTest
 * @package Xcoding\DeliveryDate\Test\Unit\Helper
 */
class ConfigProviderTest extends TestCase
{
    /**
     * @var ScopeConfigInterface|PHPUnit_Framework_MockObject_MockObject
     */
    protected $scopeConfig;

    /**
     * @var Context|PHPUnit_Framework_MockObject_MockObject
     */
    protected $context;

    /**
     * @var ConfigProvider
     */
    private $helper;

    /**
     * @var MockObject|TimezoneInterface
     */
    private $timezoneMock;

    protected function setUp()
    {
        $this->mockContext();
        $this->timezoneMock = $this->getMockBuilder(TimezoneInterface::class)
            ->getMockForAbstractClass();

        $this->helper = new ConfigProvider(
            $this->context,
            $this->timezoneMock
        );
    }

    protected function mockContext()
    {
        $this->context = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->scopeConfig = $this->getMockBuilder(ScopeConfigInterface::class)
            ->getMockForAbstractClass();
        $this->context->expects($this->any())
            ->method('getScopeConfig')
            ->willReturn($this->scopeConfig);
    }

    public function testGetCutOffTime()
    {
        $configValue = '13,00,00';
        $expectedResult = DateTime::createFromFormat('H,i,s', $configValue);
        $dateTimeMock = $this->getMockBuilder(DateTime::class)
            ->getMockForAbstractClass();
        $this->timezoneMock->expects($this->once())->method('date')->willReturn($dateTimeMock);
        $this->scopeConfig->expects($this->once())->method('getValue')->willReturn($configValue);
        $this->assertEquals($expectedResult->getTimestamp(), $this->helper->getCutOffTime()->getTimestamp());
    }

    public function testGetCutOffTimeShouldReturnDefaultValue()
    {
        $defaultValue = ConfigProvider::DEFAULT_CUT_OFF_TIME_CONFIG_VALUE;
        $expectedResult = DateTime::createFromFormat('H,i,s', $defaultValue);
        $dateTimeMock = $this->getMockBuilder(DateTime::class)
            ->getMockForAbstractClass();
        $this->timezoneMock->expects($this->once())->method('date')->willReturn($dateTimeMock);
        $this->scopeConfig->expects($this->once())->method('getValue')->willReturn(null);
        $this->assertEquals($expectedResult->getTimestamp(), $this->helper->getCutOffTime()->getTimestamp());
    }

    public function testGetNonWorkingDays()
    {
        $configValue = '05/20/2020,07/23/2020';
        $expectedResult1 = DateTime::createFromFormat('m/d/Y', '05/20/2020');
        $expectedResult2 = DateTime::createFromFormat('m/d/Y', '07/23/2020');
        $this->scopeConfig->expects($this->once())->method('getValue')->willReturn($configValue);
        $result = $this->helper->getNonWorkingDays();
        $this->assertEquals($expectedResult1->getTimestamp(), $result[0]->getTimestamp());
        $this->assertEquals($expectedResult2->getTimestamp(), $result[1]->getTimestamp());
        $this->assertCount(2, $result);
    }

    public function testGetNonWorkingDaysShouldReturnEmptyArray()
    {
        $this->scopeConfig->expects($this->once())->method('getValue')->willReturn('invalid_value');
        $this->assertEquals($this->helper->getNonWorkingDays(), []);
    }
}

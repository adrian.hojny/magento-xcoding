<?php declare(strict_types=1);

namespace Xcoding\ProductVideo\Rewrite\Model;

use Magento\CatalogGraphQl\Model\MediaGalleryTypeResolver as CoreMediaGalleryTypeResolve;

class MediaGalleryTypeResolver extends CoreMediaGalleryTypeResolve
{
    /**
     * @inheritdoc
     *
     * @param array $data
     * @return string
     */
    public function resolveType(array $data) : string
    {
        // resolve type based on the data
        if (isset($data['media_type']) && $data['media_type'] == 'image') {
            return 'ProductImage';
        }
        if (isset($data['media_type']) && $data['media_type'] == 'external-video') {
            return 'ProductVideo';
        }
        if (isset($data['media_type']) && $data['media_type'] == 'internal-video') {
            return 'ProductVideo';
        }
    }
}

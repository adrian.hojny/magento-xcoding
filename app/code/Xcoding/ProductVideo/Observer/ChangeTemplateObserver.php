<?php declare(strict_types=1);

namespace Xcoding\ProductVideo\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class ChangeTemplateObserver
 * @package Xcoding\ProductVideo\Observer
 */
class ChangeTemplateObserver implements ObserverInterface
{
    /**
     * @param mixed $observer
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @return void
     */
    public function execute(Observer $observer)
    {
        $observer->getBlock()->setTemplate('Xcoding_ProductVideo::helper/gallery.phtml');
    }
}

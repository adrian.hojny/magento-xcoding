<?php

namespace Xcoding\ProductVideo\Model\Resolver\Product;

use Magento\Catalog\Model\Product\Media\Config;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class VideoUrl implements ResolverInterface
{
    /**
     * @var Config
     */
    private $mediaConfig;

    public function __construct(
        Config $mediaConfig
    ) {
        $this->mediaConfig = $mediaConfig;
    }

    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        return $this->getImageUrl($value[$field->getName()] ?? '');
    }

    private function getImageUrl(?string $imagePath): string
    {
        return !strlen($imagePath) || strpos($imagePath, 'http') === 0 ?
            $imagePath :
            $this->mediaConfig->getMediaUrl($imagePath);
    }
}

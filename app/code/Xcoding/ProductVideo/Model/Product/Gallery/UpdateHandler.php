<?php

namespace Xcoding\ProductVideo\Model\Product\Gallery;

use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Media\Config;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Filesystem;
use Magento\Framework\Json\Helper\Data;
use Magento\MediaStorage\Helper\File\Storage\Database;
use Magento\Store\Model\Store;
use Xcoding\ProductVideo\Model\Product\Attribute\Media\InternalVideoEntryConverter;
use Xcoding\ProductVideo\Model\Product\Gallery\Command\CountVideoUses;

class UpdateHandler extends CreateHandler
{
    /**
     * @var CountVideoUses
     */
    private $countVideoUses;

    public function __construct(
        MetadataPool $metadataPool,
        ProductAttributeRepositoryInterface $attributeRepository,
        Gallery $resourceModel,
        Data $jsonHelper,
        Config $mediaConfig,
        Filesystem $filesystem,
        Database $fileStorageDb,
        CountVideoUses $countVideoUses
    ) {
        $this->countVideoUses = $countVideoUses;
        parent::__construct(
            $metadataPool,
            $attributeRepository,
            $resourceModel,
            $jsonHelper,
            $mediaConfig,
            $filesystem,
            $fileStorageDb
        );
    }

    /**
     * @inheritdoc
     *
     * @since 101.0.0
     */
    protected function processDeletedImages($product, array &$images)
    {
        $filesToDelete = [];
        $recordsToDelete = [];
        $picturesInOtherStores = [];

        foreach ($this->resourceModel->getProductImages($product, $this->extractStoreIds($product)) as $image) {
            $picturesInOtherStores[$image['filepath']] = true;
        }
        foreach ($images as &$image) {
            if (!empty($image['removed'])) {
                if (!empty($image['value_id'])) {
                    if (preg_match('/\.\.(\\\|\/)/', $image['file'])) {
                        continue;
                    }

                    if (preg_match('/\.\.(\\\|\/)/', $image['video_file'])) {
                        continue;
                    }
                    $recordsToDelete[] = $image['value_id'];
                    $catalogPath = $this->mediaConfig->getBaseMediaPath();
                    $isFile = $this->mediaDirectory->isFile($catalogPath . $image['file']);
                    // only delete physical files if they are not used by any other products and if this file exist
                    if ($isFile && !($this->resourceModel->countImageUses($image['file']) > 1)) {
                        $filesToDelete[] = ltrim($image['file'], '/');
                    }

                    if (strlen($image['video_file']) && $this->isInternalVideo($image)) {
                        $isVideoFile = $this->mediaDirectory->isFile($catalogPath . '/' . $image['video_file']);
                        // only delete physical files if they are not used by any other products and if this file exist
                        if ($isVideoFile && !($this->countVideoUses->execute($image['video_url']) > 1)) {
                            $filesToDelete[] = ltrim($image['video_file'], '/');
                        }
                    }
                }
            }
        }
        $this->resourceModel->deleteGallery($recordsToDelete);

        $this->removeDeletedImages($filesToDelete);
    }

    /**
     * @inheritdoc
     *
     * @since 101.0.0
     */
    protected function processNewImage($product, array &$image)
    {
        $data = [];

        if (empty($image['value_id'])) {
            $data['value'] = $image['file'];
            $data['attribute_id'] = $this->getAttribute()->getAttributeId();

            if (!empty($image['media_type'])) {
                $data['media_type'] = $image['media_type'];
            }

            $image['value_id'] = $this->resourceModel->insertGallery($data);

            $this->resourceModel->bindValueToEntity(
                $image['value_id'],
                $product->getData($this->metadata->getLinkField())
            );
        }

        return $data;
    }

    /**
     * Retrieve store ids from product.
     *
     * @param Product $product
     * @return array
     * @since 101.0.0
     */
    protected function extractStoreIds($product)
    {
        $storeIds = $product->getStoreIds();
        $storeIds[] = Store::DEFAULT_STORE_ID;

        // Removing current storeId.
        $storeIds = array_flip($storeIds);
        unset($storeIds[$product->getStoreId()]);
        $storeIds = array_keys($storeIds);

        return $storeIds;
    }

    /**
     * Remove deleted images.
     *
     * @param array $files
     * @return null
     * @since 101.0.0
     */
    protected function removeDeletedImages(array $files)
    {
        $catalogPath = $this->mediaConfig->getBaseMediaPath();

        foreach ($files as $filePath) {
            $this->mediaDirectory->delete($catalogPath . '/' . $filePath);
        }
    }

    /**
     * @param $image
     * @return bool
     */
    protected function isInternalVideo($image): bool
    {
        return $image['media_type'] === InternalVideoEntryConverter::MEDIA_TYPE_CODE;
    }
}

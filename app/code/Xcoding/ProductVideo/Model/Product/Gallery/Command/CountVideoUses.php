<?php declare(strict_types=1);

namespace Xcoding\ProductVideo\Model\Product\Gallery\Command;

use Magento\Catalog\Model\ResourceModel\Product\Gallery as GalleryResource;

class CountVideoUses
{
    /**
     * @var GalleryResource
     */
    private $galleryResource;

    public function __construct(
        GalleryResource $galleryResource
    ) {
        $this->galleryResource = $galleryResource;
    }

    public function execute(string $filename): int
    {
        $select = $this->galleryResource->getConnection()->select()
            ->from(['cpev' => 'catalog_product_entity_media_gallery_value_video'])
            ->where(
                'url = ?',
                $filename
            );

        return count($this->galleryResource->getConnection()->fetchAll($select));
    }
}

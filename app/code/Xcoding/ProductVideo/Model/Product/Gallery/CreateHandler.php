<?php

namespace Xcoding\ProductVideo\Model\Product\Gallery;

use Exception;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Gallery\CreateHandler as CoreCreateHandler;
use Xcoding\ProductVideo\Model\Product\Attribute\Media\InternalVideoEntryConverter;

class CreateHandler extends CoreCreateHandler
{
    /**
     * @var array
     */
    private $mediaAttributeCodes;

    public function execute($product, $arguments = [])
    {
        $attrCode = $this->getAttribute()->getAttributeCode();

        $value = $product->getData($attrCode);

        if (!is_array($value) || !isset($value['images'])) {
            return $product;
        }

        if (!is_array($value['images']) && strlen($value['images']) > 0) {
            $value['images'] = $this->jsonHelper->jsonDecode($value['images']);
        }

        if (!is_array($value['images'])) {
            $value['images'] = [];
        }
        $clearImages = [];
        $clearVideos = [];
        $newImages = [];
        $newVideos = [];
        $existImages = [];
        $existVideos = [];
        if ($product->getIsDuplicate() != true) {
            foreach ($value['images'] as &$image) {
                $videoFilename = $this->getVideoFilename($image);
                if (empty($image['video_file'])) {
                    $image['video_file'] = $videoFilename;
                }
                if (!empty($image['removed'])) {
                    $clearImages[] = $image['file'];
                    if ($this->isInternalVideo($image)) {
                        $clearImages[] = $videoFilename;
                    }
                } elseif (empty($image['value_id'])) {
                    $newFile = $this->moveImageFromTmp($image['file']);
                    if ($this->isInternalVideo($image)) {
                        $clearImages[] = $videoFilename;
                        try {
                            $newVideoFile = $this->moveImageFromTmp($videoFilename);
                        } catch (Exception $e) {
                            $newVideoFile = $videoFilename;
                        }
                        $image['new_video_file'] = $newVideoFile;
                        $image['video_file'] = $newVideoFile;
                        $image['video_url'] = $newVideoFile;
                    }
                    $image['new_file'] = $newFile;
                    $newImages[$image['file']] = $image;
                    $image['file'] = $newFile;
                } else {
                    if ($videoFilename && $this->isInternalVideo($image)) {
                        $videoUrl = $this->mediaConfig->getMediaShortUrl($videoFilename);
                        if ($image['video_url'] !== $videoUrl && strpos($image['video_url'], 'tmp/') !== false) {
                            try {
                                $newVideoFile = $this->moveImageFromTmp($videoFilename);
                            } catch (Exception $e) {
                                $newVideoFile = $videoFilename;
                            }
                            $image['new_video_file'] = $newVideoFile;
                            $image['video_file'] = $newVideoFile;
                            $image['video_url'] = $newVideoFile;
                        }
                    }

                    $existImages[$image['file']] = $image;
                }
            }
        } else {
            // For duplicating we need copy original images.
            $duplicate = [];
            foreach ($value['images'] as &$image) {
                if (empty($image['value_id']) || !empty($image['removed'])) {
                    continue;
                }
                $duplicate[$image['value_id']] = $this->copyImage($image['file']);

                $image['new_file'] = $duplicate[$image['value_id']];
                $newImages[$image['file']] = $image;
            }
            $value['duplicate'] = $duplicate;
        }
        /* @var $mediaAttribute ProductAttributeInterface */
        foreach ($this->getMediaAttributeCodes() as $mediaAttrCode) {
            $attrData = $product->getData($mediaAttrCode);
            if (empty($attrData) && empty($clearImages) && empty($newImages) && empty($existImages)) {
                continue;
            }
            $this->processMediaAttribute(
                $product,
                $mediaAttrCode,
                $clearImages,
                $newImages
            );
            if (in_array($mediaAttrCode, ['image', 'small_image', 'thumbnail'])) {
                $this->processMediaAttributeLabel(
                    $product,
                    $mediaAttrCode,
                    $clearImages,
                    $newImages,
                    $existImages
                );
            }
        }

        $product->setData($attrCode, $value);

        if ($product->getIsDuplicate() == true) {
            $this->duplicate($product);
            return $product;
        }

        if (!is_array($value) || !isset($value['images']) || $product->isLockedAttribute($attrCode)) {
            return $product;
        }

        $this->processDeletedImages($product, $value['images']);
        $this->processNewAndExistingImages($product, $value['images']);
        $product->setData($attrCode, $value);

        return $product;
    }

    protected function getVideoFilename($image)
    {
        if ($this->isInternalVideo($image) && isset($image['video_url']) && $image['video_url']) {
            $fileName = $image['video_url'];
            if (strpos($image['video_url'], 'tmp/') !== false) {
                $fileName = substr($image['video_url'], strpos($image['video_url'], "catalog/product") + 16, strlen($image['video_url']));
            }
            return $fileName;
        }

        return false;
    }
    /**
     * Get Media Attribute Codes cached value
     *
     * @return array
     */
    private function getMediaAttributeCodes()
    {
        if ($this->mediaAttributeCodes === null) {
            $this->mediaAttributeCodes = $this->mediaConfig->getMediaAttributeCodes();
        }
        return $this->mediaAttributeCodes;
    }

    /**
     * Process media attribute
     *
     * @param Product $product
     * @param string $mediaAttrCode
     * @param array $clearImages
     * @param array $newImages
     */
    private function processMediaAttribute(
        Product $product,
        $mediaAttrCode,
        array $clearImages,
        array $newImages
    ) {
        $attrData = $product->getData($mediaAttrCode);
        if (in_array($attrData, $clearImages)) {
            $product->setData($mediaAttrCode, 'no_selection');
        }

        if (in_array($attrData, array_keys($newImages))) {
            $product->setData($mediaAttrCode, $newImages[$attrData]['new_file']);
        }
        if (!empty($product->getData($mediaAttrCode))) {
            $product->addAttributeUpdate(
                $mediaAttrCode,
                $product->getData($mediaAttrCode),
                $product->getStoreId()
            );
        }
    }

    /**
     * Process media attribute label
     *
     * @param Product $product
     * @param string $mediaAttrCode
     * @param array $clearImages
     * @param array $newImages
     * @param array $existImages
     */
    private function processMediaAttributeLabel(
        Product $product,
        $mediaAttrCode,
        array $clearImages,
        array $newImages,
        array $existImages
    ) {
        $resetLabel = false;
        $attrData = $product->getData($mediaAttrCode);
        if (in_array($attrData, $clearImages)) {
            $product->setData($mediaAttrCode . '_label', null);
            $resetLabel = true;
        }

        if (in_array($attrData, array_keys($newImages))) {
            $product->setData($mediaAttrCode . '_label', $newImages[$attrData]['label']);
        }

        if (in_array($attrData, array_keys($existImages)) && isset($existImages[$attrData]['label'])) {
            $product->setData($mediaAttrCode . '_label', $existImages[$attrData]['label']);
        }

        if ($attrData === 'no_selection' && !empty($product->getData($mediaAttrCode . '_label'))) {
            $product->setData($mediaAttrCode . '_label', null);
            $resetLabel = true;
        }
        if (!empty($product->getData($mediaAttrCode . '_label'))
            || $resetLabel === true
        ) {
            $product->addAttributeUpdate(
                $mediaAttrCode . '_label',
                $product->getData($mediaAttrCode . '_label'),
                $product->getStoreId()
            );
        }
    }

    /**
     * @param $image
     * @return bool
     */
    protected function isInternalVideo($image): bool
    {
        return $image['media_type'] === InternalVideoEntryConverter::MEDIA_TYPE_CODE;
    }
}

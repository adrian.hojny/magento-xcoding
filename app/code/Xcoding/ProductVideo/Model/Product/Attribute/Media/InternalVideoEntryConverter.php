<?php

namespace Xcoding\ProductVideo\Model\Product\Attribute\Media;

use Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryExtensionFactory;
use Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterface;
use Magento\Catalog\Api\Data\ProductAttributeMediaGalleryEntryInterfaceFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Backend\Media\ImageEntryConverter;
use Magento\Framework\Api\Data\VideoContentInterface;
use Magento\Framework\Api\Data\VideoContentInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

/**
 * Converter for Internal Video media gallery type
 */
class InternalVideoEntryConverter extends ImageEntryConverter
{
    /**
     * Media Entry type code
     */
    const MEDIA_TYPE_CODE = 'internal-video';

    /**
     * @var VideoContentInterfaceFactory
     */
    protected $videoEntryFactory;

    /**
     * @var ProductAttributeMediaGalleryEntryExtensionFactory
     */
    protected $mediaGalleryEntryExtensionFactory;

    /**
     * @param ProductAttributeMediaGalleryEntryInterfaceFactory $mediaGalleryEntryFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param VideoContentInterfaceFactory $videoEntryFactory
     * @param ProductAttributeMediaGalleryEntryExtensionFactory $mediaGalleryEntryExtensionFactory
     */
    public function __construct(
        ProductAttributeMediaGalleryEntryInterfaceFactory $mediaGalleryEntryFactory,
        DataObjectHelper $dataObjectHelper,
        VideoContentInterfaceFactory $videoEntryFactory,
        ProductAttributeMediaGalleryEntryExtensionFactory $mediaGalleryEntryExtensionFactory
    ) {
        parent::__construct($mediaGalleryEntryFactory, $dataObjectHelper);
        $this->videoEntryFactory = $videoEntryFactory;
        $this->mediaGalleryEntryExtensionFactory = $mediaGalleryEntryExtensionFactory;
    }

    /**
     * @return string
     */
    public function getMediaEntryType()
    {
        return self::MEDIA_TYPE_CODE;
    }

    /**
     * @param Product $product
     * @param array $rowData
     * @return ProductAttributeMediaGalleryEntryInterface
     */
    public function convertTo(Product $product, array $rowData)
    {
        $entry = parent::convertTo($product, $rowData);
        $videoEntry = $this->videoEntryFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $videoEntry,
            $rowData,
            VideoContentInterface::class
        );
        $entryExtension = $this->mediaGalleryEntryExtensionFactory->create();
        $entryExtension->setVideoContent($videoEntry);
        $entry->setExtensionAttributes($entryExtension);
        return $entry;
    }

    /**
     * {@inheritdoc}
     */
    public function convertFrom(ProductAttributeMediaGalleryEntryInterface $entry)
    {
        $dataFromPreviewImageEntry = parent::convertFrom($entry);
        $videoContent = $entry->getExtensionAttributes()->getVideoContent();
        $entryArray = [
            'video_provider' => $videoContent->getVideoProvider(),
            'video_url' => $videoContent->getVideoUrl(),
            'video_title' => $videoContent->getVideoTitle(),
            'video_description' => $videoContent->getVideoDescription(),
            'video_metadata' => $videoContent->getVideoMetadata(),
        ];
        $entryArray = array_merge($dataFromPreviewImageEntry, $entryArray);
        return $entryArray;
    }
}

define([
    'jquery',
    'underscore',
    'jquery/ui',
    'Magento_Ui/js/modal/modal',
    'mage/translate',
    'mage/backend/tree-suggest',
    'mage/backend/validation'
], function ($, _) {
    'use strict';


    $.widget('mage.updateInternalInputFields', {
        options: {
            reset: false,
            DOM: {
                urlField: 'input[name="video_url"]',
                titleField: 'input[name="video_title"]',
                fileField: '#internal_file_name',
                descriptionField: 'textarea[name="video_description"]',
                thumbnailLocation: '.field-new_video_internal_screenshot_preview .admin__field-control'
            },
            data: {
                url: '',
                title: '',
                description: '',
                thumbnail: ''
            }
        },

        /**
         * @private
         */
        _init: function () {
            if (this.options.reset) {
                this.reset();
            } else {
                this.update();
            }
        },

        /**
         * Update
         */
        update: function () {
            $(this.options.DOM.titleField).val(this.options.data.title);
            $(this.options.DOM.descriptionField).val(this.options.data.description);
            $(this.options.DOM.urlField).val(this.options.data.url);
        },

        /**
         * Reset
         */
        reset: function () {
            $(this.options.DOM.fileField).val('');
            $(this.options.DOM.titleField).val('');
            $(this.options.DOM.descriptionField).val('');
        }
    });

    /**
     */
    $.widget('mage.newInternalVideoDialog', {

        _previewImage: null,

        clickedElement: '',

        _images: {},

        _imageTypes: [
            '.jpeg',
            '.pjpeg',
            '.jpeg',
            '.jpg',
            '.pjpeg',
            '.png',
            '.gif'
        ],

        _videoTypes: [
          '.mp4',
          '.ogg',
          '.webm',
        ],

        _imageProductGalleryWrapperSelector: '#internal-image-container',

        _videoPreviewInputSelector: '#new_internal_video_screenshot',

        _videoInputSelector: '#new_internal_video',

        _videoPreviewRemoteSelector: '',

        _videoDisableinputSelector: '#new_internal_video_disabled',

        _videoPreviewImagePointer: '#new_internal_video_screenshot_preview',

        _videoFormSelector: '#new_internal_video_form',

        _itemIdSelector: '#internal_item_id',

        _videoUrlSelector: '[name="internal_video_url"]',

        _videoImageFilenameselector: '#internal_file_name',

        _editVideoBtnSelector: '.image',

        _deleteGalleryVideoSelector: '[data-role=delete-button]',

        _deleteGalleryVideoSelectorBtn: null,

        _isEditPage: false,

        _onlyVideoPlayer: false,

        _tempPreviewImageData: null,

        _tempPreviewVideoData: null,

        _videoPlayerSelector: '.mage-new-internal-video-dialog',

        _videoRequestComplete: null,

        _gallery: null,

        /**
         * Bind events
         * @private
         */
        _bind: function () {
            var events = {
                'setImage': '_onSetImage'
            };

            this._on(events);
        },

        /**
         * Remove ".tmp"
         * @param {String} name
         * @returns {*}
         * @private
         */
        __prepareFilename: function (name) {
            var tmppost = '.tmp';

            if (!name) {
                return name;
            }

            if (name.endsWith(tmppost)) {
                name = name.slice(0, name.length - tmppost.length);
            }

            return name;
        },

        /**
         * Set image data
         * @param {String} file
         * @param {Object} imageData
         * @private
         */
        _setImage: function (file, imageData) {
            file = this.__prepareFilename(file);
            this._images[file] = imageData;
            this._gallery.trigger('addItem', imageData);
            this.element.trigger('setImage', imageData);
            this._addVideoClass(imageData.url);
        },

        /**
         * Get image data
         *
         * @param {String} file
         * @returns {*}
         * @private
         */
        _getImage: function (file) {
            file = this.__prepareFilename(file);

            return this._images[file];
        },

        /**
         * Replace image (update)
         * @param {String} oldFile
         * @param {String} newFile
         * @param {Object} imageData
         * @private
         */
        _replaceImage: function (oldFile, newFile, imageData) {
            var tmpNewFile = newFile,
                tmpOldImage,
                newImageId,
                oldNewFilePosition,
                fc,
                suff,
                searchsuff,
                key,
                oldValIdElem;

            oldFile = this.__prepareFilename(oldFile);
            newFile = this.__prepareFilename(newFile);
            tmpOldImage = this._images[oldFile];

            if (newFile === oldFile) {
                this._images[newFile] = imageData;
                this.saveImageRoles(imageData);
                this._updateVisibility(imageData);
                this._updateImageTitle(imageData);

                return null;
            }

            this._removeImage(oldFile);
            this._setImage(newFile, imageData);

            if (!oldFile || !imageData.oldFile) {
                return null;
            }

            newImageId = this.findElementId(tmpNewFile);
            fc = this.element.find(this._itemIdSelector).val();

            suff = 'product[media_gallery][images]' + fc;

            searchsuff = 'input[name="' + suff + '[value_id]"]';
            key = this._gallery.find(searchsuff).val();

            if (!key) {
                return null;
            }

            oldValIdElem = document.createElement('input');
            this._gallery.find('form[data-form="edit-product"]').append(oldValIdElem);
            $(oldValIdElem).attr({
                type: 'hidden',
                name: 'product[media_gallery][images][' + newImageId + '][save_data_from]'
            }).val(key);

            oldNewFilePosition = parseInt(tmpOldImage.position, 10);
            imageData.position = oldNewFilePosition;

            this._gallery.trigger('setPosition', {
                imageData: imageData,
                position: oldNewFilePosition
            });
        },

        /**
         * Remove image data
         * @param {String} file
         * @private
         */
        _removeImage: function (file) {
            var imageData = this._getImage(file);

            if (!imageData) {
                return null;
            }

            this._gallery.trigger('removeItem', imageData);
            this.element.trigger('removeImage', imageData);
            delete this._images[file];
        },

        /**
         * Fired when image setted
         * @param {Event} event
         * @param {Object} imageData
         * @private
         */
        _onSetImage: function (event, imageData) {
            this.saveImageRoles(imageData);
        },

        /**
         *
         * Wrap _uploadFile
         * @param {String} file
         * @param {String} oldFile
         * @param {Function} callback
         * @param itemId
         * @param videoFile
         * @private
         */
        _uploadImage: function (file, oldFile, callback, itemId = null, videoFile = null) {
            var url = this.options.saveImageUrl,
                data = {
                    files: file,
                    url: url
                };

            this._blockActionButtons(true, true);
            this._uploadFile(data, $.proxy(function (result) {
                this._onImageLoaded(result, file, oldFile, callback, itemId, videoFile);
                this._blockActionButtons(false);
            }, this));

        },

        /**
         *
         * Wrap _uploadFile
         * @param {String} file
         * @param {String} oldFile
         * @param {Function} callback
         * @param itemId
         * @private
         */
        _uploadVideo: function (file, oldFile, callback, itemId = null) {
            var url = this.options.saveVideoUrl,
                data = {
                    files: file,
                    url: url
                };

            this._blockActionButtons(true, true);
            this._uploadFile(data, $.proxy(function (result) {
                this._onVideoLoaded(result, callback, itemId);
                this._blockActionButtons(false);
            }, this));

        },

        /**
         * @param {String} result
         * @param {Function} callback
         * @param itemId
         * @private
         */
        _onVideoLoaded: function (result, callback, itemId) {
            var newdata = JSON.parse(result);
            if (this.element.find('#video-upload-error').length > 0) {
                this.element.find('#video-upload-error').remove();
            }

            if (newdata.errorcode || newdata.error) {
                this.element.find('.messages').append('<div id="video-upload-error"  class="image-upload-error">' +
                    '<div class="image-upload-error-cross"></div><span>Video upload error: ' + newdata.error + '</span></div>');

                return;
            }
            this.element.find('#internal_item_url').val(newdata['url']);
            $.each(this.element.find(this._videoFormSelector).serializeArray(), function (i, field) {
                newdata[field.name] = field.value;
            });

            if (itemId && newdata['video_url']) {
                this._gallery.find('input[name*="' + itemId + '][video_url]"]').val(newdata['video_url']);
                this._gallery.find('input[name*="' + itemId + '][video_file]"]').val(newdata['file']);
            }
            callback.call(newdata['video_file']);
        },

        /**
         * @param {String} result
         * @param {String} file
         * @param {String} oldFile
         * @param {Function} callback
         * @param itemId
         * @param vide
         * @private
         */
        _onImageLoaded: function (result, file, oldFile, callback, itemId = null, videoFile = null) {
            var data = JSON.parse(result);

            if (videoFile) {
                data['video_file'] = videoFile;
            }

            if (this.element.find('#video_url').parent().find('.image-upload-error').length > 0) {
                this.element.find('.image-upload-error').remove();
            }

            if (data.errorcode || data.error) {
                this.element.find('#video_url').parent().append('<div class="image-upload-error">' +
                '<div class="image-upload-error-cross"></div><span>' + data.error + '</span></div>');

                return;
            }
            $.each(this.element.find(this._videoFormSelector).serializeArray(), function (i, field) {
                if (field.name !== 'video_url' || field.value) {
                    data[field.name] = field.value;
                }
            });
            data.disabled = this.element.find(this._videoDisableinputSelector).attr('checked') ? 1 : 0;
            data['media_type'] = 'internal-video';
            if (!data['video_url']) {
                data['video_url'] = this._gallery.find('input[name*="' + itemId + '][video_url]"]').val();
            }

            data.oldFile = oldFile;

            oldFile ?
                this._replaceImage(oldFile, data.file, data) :
                this._setImage(data.file, data);
            callback.call(0, data);
        },

        /**
         * File uploader
         * @private
         */
        _uploadFile: function (data, callback) {
            $('body').trigger('processStart');

            if (data.url === this.options.saveImageUrl) {
                var fu = this.element.find(this._videoPreviewInputSelector);
            } else {
                var fu = this.element.find(this._videoInputSelector);
            }

            var tmpInput = document.createElement('input'),
                fileUploader = null;

            $(tmpInput).attr({
                'name': fu.attr('name'),
                'value': fu.val(),
                'type': 'file',
                'data-ui-ud': fu.attr('data-ui-ud')
            }).css('display', 'none');
            fu.parent().append(tmpInput);
            fileUploader = $(tmpInput).fileupload();
            fileUploader.fileupload('send', data).success(function (result, textStatus, jqXHR) {
                $('body').trigger('processStop');
                tmpInput.remove();
                callback.call(null, result, textStatus, jqXHR);
            }).error(function () {
                $('body').trigger('processStop');
            });
        },

        /**
         * Update style
         * @param {String} url
         * @private
         */
        _addVideoClass: function (url) {
            var classVideo = 'video-item';

            this._gallery.find('img[src="' + url + '"]').addClass(classVideo);
        },

        /**
         * Build widget
         * @private
         */
        _create: function () {
            var imgs = _.values(this.element.closest(this.options.videoSelector).data('images')) || [],
                widget,
                uploader,
                videoUploader,
                tmp,
                i;

            this._gallery =  this.element.closest(this.options.videoSelector);

            for (i = 0; i < imgs.length; i++) {
                tmp = imgs[i];
                this._images[tmp.file] = tmp;

                if (tmp['media_type'] === 'internal-video') {
                    tmp.subclass = 'video-item';
                    this._addVideoClass(tmp.url);
                }
            }

            this._gallery.on('openDialog', $.proxy(this._onOpenDialog, this));
            this._bind();
            this.createVideoItemIcons();
            widget = this;
            uploader = this.element.find(this._videoPreviewInputSelector);
            uploader.on('change', this._onImageInputChange.bind(this));
            uploader.attr('accept', this._imageTypes.join(','));

            videoUploader = this.element.find(this._videoInputSelector);
            videoUploader.on('change', this._onVideoInputChange.bind(this));
            videoUploader.attr('accept', this._videoTypes.join(','));

            this.element.modal({
                type: 'slide',
                modalClass: 'mage-new-internal-video-dialog form-inline',
                title: $.mage.__('New Video'),
                buttons: [
                    {
                        text: $.mage.__('Save'),
                        class: 'action-primary internal-video-create-button',
                        click: $.proxy(widget._onCreate, widget)
                    },
                    {
                        text: $.mage.__('Cancel'),
                        class: 'internal-video-cancel-button',
                        click: $.proxy(widget._onCancel, widget)
                    },
                    {
                        text: $.mage.__('Delete'),
                        class: 'internal-video-delete-button',
                        click: $.proxy(widget._onDelete, widget)
                    },
                    {
                        text: $.mage.__('Save'),
                        class: 'action-primary internal-video-edit',
                        click: $.proxy(widget._onUpdate, widget)
                    }
                ],

                /**
                 * @returns {null}
                 */
                opened: function () {
                    var roles,
                        file,
                        modalTitleElement,
                        imageData,
                        nvs = widget.element.find(widget._videoPreviewInputSelector),
                        vis = widget.element.find(this._videoInputSelector),
                        reqClass = 'required-entry _required',
                        modal = widget.element.closest('.mage-new-internal-video-dialog');

                    var getVideoUrl = function (imageData) {
                        var url = imageData.video_url;
                        if (imageData.file_id) {
                            url = $('.video-item input[name*="' + imageData.file_id + '][video_url]"]').val();
                        }
                        return url.includes('tmp/') ?
                            '/media/' + url :
                            '/media/catalog/product' + url;
                    };

                    nvs.removeClass(reqClass);
                    vis.removeClass(reqClass);

                    widget.element.find('#internal_video_url').focus();
                    roles = widget.element.find('.internal_video_image_role');
                    roles.prop('disabled', false);

                    file = widget.element.find('#internal_file_name').val();
                    modalTitleElement = modal.find('.modal-title');

                    if (!file) {
                        modal.find('.internal-video-delete-button').hide();
                        modal.find('.internal-video-edit').hide();
                        modal.find('.internal-video-create-button').show();
                        roles.prop('checked', widget._gallery.find('.image.item:not(.removed)').length < 1);
                        modalTitleElement.text($.mage.__('New Video'));
                        widget._isEditPage = false;

                        return null;
                    }
                    widget._blockActionButtons(false);
                    modalTitleElement.text($.mage.__('Edit Video'));
                    widget._isEditPage = true;
                    imageData = widget._getImage(file);

                    if (!imageData) {
                        imageData = {
                            url: _.find(widget._gallery.find('.product-image'), function (image) {
                                return image.src.indexOf(file) > -1;
                            }).src
                        };
                    }
                    widget._onPreview(null, imageData.url, false);
                    widget._onVideoPreview(null, getVideoUrl(imageData), false);
                },

                /**
                 * Closed
                 */
                closed: function () {
                    $('body').trigger('processStop');
                    widget._onClose();
                    widget.createVideoItemIcons();
                }
            });
            this.toggleButtons();
        },

        /**
         * @param {String} status
         * @private
         */
        _blockActionButtons: function (status) {
            this.element
                .closest('.mage-new-internal-video-dialog')
                .find('.page-actions-buttons button.internal-video-create-button, .page-actions-buttons button.internal-video-edit')
                .attr('disabled', status);
        },


        /**
         * Check form
         * @param {Function} callback
         */
        isValid: function (callback) {
            var videoForm = this.element.find(this._videoFormSelector),
                videoLoaded = true;

            this._blockActionButtons(true);
            videoForm.mage('validation', {

                /**
                 * @param {jQuery} error
                 * @param {jQuery} element
                 */
                errorPlacement: function (error, element) {
                    console.log(element);
                    error.insertAfter(element);
                }
            }).on('highlight.validate', function () {
                $(this).validation('option');
            });

            videoForm.validation();
            callback(videoForm.valid() && videoLoaded);

            this._blockActionButtons(false);
        },

        /**
         * Create video item icons
         */
        createVideoItemIcons: function () {
            var $imageWidget = this._gallery.find('.product-image.video-item'),
                $productGalleryWrapper = $(this._imageProductGalleryWrapperSelector).find('.product-image.video-item');

            $imageWidget.parent().addClass('video-item');
            $productGalleryWrapper.parent().addClass('video-item');
            $imageWidget.removeClass('video-item');
            $productGalleryWrapper.removeClass('video-item');
            $('.video-item .action-delete').attr('title', $.mage.__('Delete video'));
            $('.video-item .action-delete span').html($.mage.__('Delete video'));
        },

        /**
         * Fired when click on create video
         * @private
         */
        _onCreate: function () {
            var nvs = this.element.find(this._videoPreviewInputSelector),
                vis = this.element.find(this._videoInputSelector),
                file = nvs.get(0),
                videoFile = vis.get(0),
                reqClass = 'required-entry _required';

            if (file && file.files && file.files.length) {
                file = file.files[0];
            } else {
                file = null;
            }

            if (videoFile && videoFile.files && videoFile.files.length) {
                videoFile = videoFile.files[0];
            } else {
                videoFile = null;
            }

            if (!file && !this._tempPreviewImageData) {
                nvs.addClass(reqClass);
            }

            if (!videoFile) {
                vis.addClass(reqClass);
            }

            this.isValid($.proxy(
                function (videoValidStatus) {
                    if (!videoValidStatus) {
                        return;
                    }

                    let imageCallback = $.proxy(function (videoFile) {
                        if (file) {
                            this._uploadImage(file, null, $.proxy(function () {
                                this.close();
                            }, this), null, videoFile);
                        }
                    }, this);

                    let videoInputFile = this.element.find(this._videoInputSelector);
                    let videoFileName = videoInputFile.get(0).files;

                    if (!videoFileName || !videoFileName.length) {
                        videoFileName = null;
                    }
                    videoInputFile.replaceWith(videoInputFile);
                    if (videoFileName) {
                        this._uploadVideo(videoFileName, '', imageCallback);
                    } else {
                        imageCallback();
                    }
                    nvs.removeClass(reqClass);
                    vis.removeClass(reqClass);
                }, this
            ));
        },

        /**
         * Fired when click on update video
         * @private
         */
        _onUpdate: function () {
            var nvs = this.element.find(this._videoPreviewInputSelector),
                vis = this.element.find(this._videoInputSelector),
                file = nvs.get(0),
                reqClass = 'required-entry _required';

            var inputFile, itemId, _inputSelector, mediaFields, imageData, flagChecked, fileName, callback;

            if (file && file.files && file.files.length) {
                file = file.files[0];
            } else {
                file = null;
            }

            if (!file && !this._tempPreviewImageData && !this._previewImage) {
                nvs.addClass(reqClass);
            }
            vis.removeClass(reqClass);

            this.isValid($.proxy(
                function (videoValidStatus) {
                    if (!videoValidStatus) {
                        return;
                    }

                    if (!file && !this._tempPreviewImageData) {
                        nvs.addClass(reqClass);
                    }

                    imageData = this.imageData || {};
                    inputFile = this.element.find(this._videoPreviewInputSelector);
                    itemId = this.element.find(this._itemIdSelector).val();
                    itemId = itemId.slice(1, itemId.length - 1);
                    _inputSelector = '[name*="[' + itemId + ']"]';
                    mediaFields = this._gallery.find('input' + _inputSelector);
                    $.each(mediaFields, function (i, el) {
                        var elName = el.name,
                            start = elName.indexOf(itemId) + itemId.length + 2,
                            fieldName = elName.substring(start, el.name.length - 1),
                            _field = this.element.find('#internal_' + fieldName),
                            _tmp;

                        if (_field.length > 0) {
                            _tmp = _inputSelector.slice(0, _inputSelector.length - 2) + '[' + fieldName + ']"]';
                            this._gallery.find(_tmp).val(_field.val());
                            imageData[fieldName] = _field.val();
                        }
                    }.bind(this));

                    let callback = $.proxy(function () {
                        flagChecked = this.element.find(this._videoDisableinputSelector).attr('checked') ? 1 : 0;
                        this._gallery.find('input[name*="' + itemId + '][disabled]"]').val(flagChecked);
                        this._gallery.find(_inputSelector).siblings('.image-fade').css(
                            'visibility', flagChecked ? 'visible' : 'hidden'
                        );
                        imageData.disabled = flagChecked;

                        if (this._tempPreviewImageData) {
                            this._onImageLoaded(
                                this._tempPreviewImageData,
                                null,
                                imageData.file,
                                $.proxy(this.close, this)
                            );

                            return;
                        }
                        fileName = inputFile.get(0).files;

                        if (!fileName || !fileName.length) {
                            fileName = null;
                        }
                        inputFile.replaceWith(inputFile);

                        if (fileName) {
                            this._uploadImage(fileName, imageData.file, $.proxy(this.close, this), itemId);
                        } else {
                            this._replaceImage(imageData.file, imageData.file, imageData);
                            this.close();
                        }
                        nvs.removeClass(reqClass);
                    }, this);
                    let videoInputFile = this.element.find(this._videoInputSelector);
                    let videoFileName = videoInputFile.get(0).files;

                    if (!videoFileName || !videoFileName.length) {
                        videoFileName = null;
                    }
                    videoInputFile.replaceWith(videoInputFile);
                    if (videoFileName) {
                        this._uploadVideo(videoFileName, '', callback, itemId);
                    } else {
                        callback();
                    }

                }, this
            ));
        },

        /**
         * Delegates call to producwt gallery to update video visibility.
         *
         * @param {Object} imageData
         */
        _updateVisibility: function (imageData) {
            this._gallery.trigger('updateVisibility', {
                disabled: imageData.disabled,
                imageData: imageData
            });
        },

        /**
         * Delegates call to product gallery to update video title.
         *
         * @param {Object} imageData
         */
        _updateImageTitle: function (imageData) {
            imageData['label'] = imageData['video_title'];
            this._gallery.trigger('updateImageTitle', {
                imageData: imageData
            });
        },

        /**
         * Fired when clicked on cancel
         * @private
         */
        _onCancel: function () {
            this.close();
        },

        /**
         * Fired when clicked on delete
         * @private
         */
        _onDelete: function () {
            var filename = this.element.find(this._videoImageFilenameselector).val();

            this._removeImage(filename);
            this.close();
        },

        /**
         * @param {String} file
         * @param {Function} callback
         * @private
         */
        _readPreviewLocal: function (file, callback) {
            var fr = new FileReader;

            if (!window.FileReader) {
                return;
            }

            /**
             * On load end
             */
            fr.onloadend = function () {
                callback(fr.result);
            };
            fr.readAsDataURL(file);
        },

        /**
         *  Image file input handler
         * @private
         */
        _onImageInputChange: function () {
            var jFile = this.element.find(this._videoPreviewInputSelector),
                file = jFile[0],
                val = jFile.val(),
                prev = this._getPreviewImage(),
                ext = '.' + val.split('.').pop();

            if (!val) {
                return;
            }
            ext = ext ? ext.toLowerCase() : '';

            if (
                ext.length < 2 ||
                this._imageTypes.indexOf(ext.toLowerCase()) === -1 || !file.files || !file.files.length
            ) {
                prev.remove();
                this._previewImage = null;
                jFile.val('');

                return;
            } 
            file = file.files[0];
            this._tempPreviewImageData = null;
            this._onPreview(null, file, true);
        },

        /**
         *  Video file input handler
         * @private
         */
        _onVideoInputChange: function () {
            var jFile = this.element.find(this._videoInputSelector),
                file = jFile[0],
                val = jFile.val(),
                prev = this._getPreviewVideo(),
                ext = '.' + val.split('.').pop();

            if (!val) {
                return;
            }
            ext = ext ? ext.toLowerCase() : '';

            if (
                ext.length < 2 ||
                this._videoTypes.indexOf(ext.toLowerCase()) === -1 || !file.files || !file.files.length
            ) {
                prev.remove();
                this._previewVideo = null;
                jFile.val('');

                return;
            } 
            file = file.files[0];
            this._tempPreviewVideoData = null;
            this._onVideoPreview(null, file, true);
        },

        /**
         * Change Preview
         * @param {String} error
         * @param {String} src
         * @param {Boolean} local
         * @private
         */
        _onPreview: function (error, src, local) {
            var img, renderImage;

            img = this._getPreviewImage();

            /**
             * Callback
             * @param {String} source
             */
            renderImage = function (source) {
                img.attr({
                    'src': source
                }).show();
            };

            if (error) {
                return;
            }

            if (!local) {
                renderImage(src);
            } else {
                this._readPreviewLocal(src, renderImage);
            }
        },

        /**
         * Change Video Preview
         * @param {String} error
         * @param {String} src
         * @param {Boolean} local
         * @private
         */
        _onVideoPreview: function (error, src, local) {
            var vid, renderVideo;

            vid = this._getPreviewVideo();

            /**
             * Callback
             * @param {String} source
             */
            renderVideo = function (source) {
                vid.attr({
                    'src': source
                }).show();
            };

            if (error) {
                return;
            }

            if (!local) {
                renderVideo(src);
            } else {
                this._readPreviewLocal(src, renderVideo);
            }
        },

        /**
         *
         * Return preview image imstance
         * @returns {null}
         * @private
         */
        _getPreviewImage: function () {

            if (!this._previewImage) {
                this._previewImage = $(document.createElement('img')).css({
                    'width': '100%',
                    'display': 'none',
                    'src': ''
                });
                $(this._previewImage).insertAfter(this.element.find(this._videoPreviewImagePointer));
                $(this._previewImage).attr('data-role', 'internal_video_preview_image');
            }

            return this._previewImage;
        },

        _getPreviewVideo: function() {
            if (!this._previewVideo) {
                this._previewVideo = $(document.createElement('video')).css({
                    'width': '100%',
                    'display': 'none',
                    'src': ''
                });
                this._previewVideo.attr('controls', true);
                $(this._previewVideo).insertAfter(this.element.find(this._videoInputSelector));
                $(this._previewVideo).attr('data-role', 'internal_video_preview_video');
            }

            return this._previewVideo;
        },

        /**
         * Close slideout dialog
         */
        close: function () {
            this.element.modal('closeModal');
        },

        /**
         * Close dialog wrap
         * @private
         */
        _onClose: function () {
            var newVideoForm;

            this._isEditPage = true;
            this.imageData = null;

            if (this._previewImage) {
                this._previewImage.remove();
                this._previewImage = null;
            }
            if (this._previewVideo) {
                this._previewVideo.remove();
                this._previewVideo = null;
            }

            this._tempPreviewImageData = null;
            this._tempPreviewVideoData = null;
            this.element.trigger('reset');
            newVideoForm = this.element.find(this._videoFormSelector);

            $(newVideoForm).find('input[type="hidden"][name!="form_key"]').val('');
            this._gallery.find('input[name*="' + this.element.find(
                    this._itemIdSelector).val() + '"]'
            ).parent().removeClass('active');

            try {
                newVideoForm.validation('clearError');
            } catch (e) {

            }
            newVideoForm.trigger('reset');
        },

        /**
         * Find element by fileName
         * @param {String} file
         */
        findElementId: function (file) {
            var elem = this._gallery.find('.image.item').find('input[value="' + file + '"]');

            if (!elem.length) {
                return null;
            }

            return $(elem).attr('name').replace('product[media_gallery][images][', '').replace('][file]', '');
        },

        /**
         * Save image roles
         * @param {Object} imageData
         */
        saveImageRoles: function (imageData) {
            var data = imageData.file,
                self = this,
                containers;

            if (data && data.length > 0) {
                containers = this._gallery.find('.image-placeholder').siblings('input');
                $.each(containers, function (i, el) {
                    var start = el.name.indexOf('[') + 1,
                        end = el.name.indexOf(']'),
                        imageType = el.name.substring(start, end),
                        imageCheckbox = self.element.find(
                            self._videoFormSelector + ' input[value="' + imageType + '"]'
                        );

                    self._changeRole(imageType, imageCheckbox.attr('checked'), imageData);
                });
            }
        },

        /**
         * Change image role
         * @param {String} imageType - role name
         * @param {bool} isEnabled - role active status
         * @param {Object} imageData - image data object
         * @private
         */
        _changeRole: function (imageType, isEnabled, imageData) {
            var needCheked = true;

            if (!isEnabled) {
                needCheked = this._gallery.find('input[name="product[' + imageType + ']"]').val() === imageData.file;
            }

            if (!needCheked) {
                return null;
            }

            this._gallery.trigger('setImageType', {
                type: imageType,
                imageData: isEnabled ? imageData : null
            });
        },

        /**
         * On open dialog
         * @param {Object} e
         * @param {Object} imageData
         * @private
         */
        _onOpenDialog: function (e, imageData) {
            var formFields, flagChecked, file,
                modal = this.element.closest('.mage-new-internal-video-dialog');
            if (imageData['media_type'] === 'internal-video') {
                this.imageData = imageData;
                modal.find('.internal-video-create-button').hide();
                modal.find('.internal-video-delete-button').show();
                modal.find('.internal-video-edit').show();
                formFields = modal.find(this._videoFormSelector).find('.edited-data');

                $.each(formFields, function (i, field) {
                    $(field).val(imageData[field.name]);
                });

                var fieldsMapping = {
                    video_title: 'internal_video_title',
                    video_description: 'internal_video_description',
                    file: 'internal_file_name'
                };

                flagChecked = imageData.disabled > 0;
                modal.find(this._videoDisableinputSelector).prop('checked', flagChecked);
                file = modal.find('#internal_file_name').val(imageData.file);
                var filename = file.val();
                for(var key in fieldsMapping) {
                    $('#' + fieldsMapping[key]).val(imageData[key]);
                }
                $.each(modal.find('.internal_video_image_role'), function () {
                    $(this).prop('checked', false).prop('disabled', false);
                });
                $.each(this._gallery.find('.image-placeholder').siblings('input:hidden'), function () {
                    var start, end, imageRole;
                    if ($(this).val() === filename) {
                        start = this.name.indexOf('[') + 1;
                        end = this.name.length - 1;
                        imageRole = this.name.substring(start, end);
                        modal.find('#new_internal_video_form input[value="' + imageRole + '"]').prop('checked', true);
                    }
                });
            }

        },

        /**
         * Toggle buttons
         */
        toggleButtons: function () {
            var self = this,
                modal = this.element.closest('.mage-new-internal-video-dialog');
            modal.find('.video-placeholder, .add-video-button-container > button').click(function () {
                modal.find('.internal-video-create-button').show();
                modal.find('.internal-video-delete-button').hide();
                modal.find('.internal-video-edit').hide();
                modal.createVideoPlayer({
                    reset: true
                }).createVideoPlayer('reset').updateInternalInputFields({
                    reset: true
                }).updateInternalInputFields('reset');
            });
            this._gallery.on('click', '.item.video-item', function () {
                modal.find('.internal-video-create-button').hide();
                modal.find('.internal-video-delete-button').show();
                modal.find('.internal-video-edit').show();
                modal.find('.mage-new-internal-video-dialog').createVideoPlayer({
                    reset: true
                }).createVideoPlayer('reset');
            });
            this._gallery.on('click', '.item.video-item:not(.removed)', function () {
                var flagChecked,
                    file,
                    formFields = modal.find('.edited-data'),
                    container = $(this);

                $.each(formFields, function (i, field) {
                    $(field).val(container.find('input[name*="' + field.name + '"]').val());
                });
                flagChecked = container.find('input[name*="disabled"]').val() > 0;
                self._gallery.find(self._videoDisableinputSelector).attr('checked', flagChecked);

                if (container.find('input[name*="file"]').val()) {
                    file = $('#internal_file_name').val(container.find('input[name*="file"]').val());
                } else {
                    file = $('#internal_file_name');
                }
                $.each(self._gallery.find('.internal_video_image_role'), function () {
                    $(this).prop('checked', false).prop('disabled', false);
                });

                $.each(self._gallery.find('.image-placeholder').siblings('input:hidden'), function () {
                    var start, end, imageRole;

                    if ($(this).val() !== file.val()) {
                        return null;
                    }

                    start = this.name.indexOf('[') + 1;
                    end = this.name.length - 1;
                    imageRole = this.name.substring(start, end);
                    self._gallery.find('input[value="' + imageRole + '"]').prop('checked', true);
                });
            });
        }
    });

    $('#group-fields-image-management > legend > span').text($.mage.__('Images and Videos'));

    return $.mage.newInternalVideoDialog;
});

define([
    'jquery',
    'openVideoModal',
    'productGallery',
    'jquery/ui',
    'Magento_Ui/js/modal/modal',
    'mage/translate',
    'mage/backend/tree-suggest',
    'mage/backend/validation',
    'newInternalVideoDialog'
], function ($, openVideoModal) {
    'use strict';

    $.widget('mage.productGallery', openVideoModal, {

        /**
         * Bind events
         * @private
         */
        _bind: function () {
            var events = {},
                itemId;

            this._super();

            /**
             * Add item_id value to opened modal
             * @param {Object} event
             */
            events['click ' + this.options.imageSelector] = function (event) {
                if (!$(event.currentTarget).is('.ui-sortable-helper')) {
                    itemId = $(event.currentTarget).find('input')[0].name.match(/\[([^\]]*)\]/g)[2];
                    this.videoInternalDialog.find('#internal_item_id').val(itemId);
                }
            };
            this._on(events);
            this.element.prev().find('[data-role="add-internal-video-button"]').on('click', this.showInternalModal.bind(this));
            this.element.on('openDialog', '.gallery.ui-sortable', $.proxy(this._onOpenDialog, this));
        },

        /**
         * @private
         */
        _create: function () {
            this._super();
            this.videoInternalDialog = this.element.find('#new-internal-video');
            this.videoInternalDialog.mage('newInternalVideoDialog', this.videoInternalDialog.data('modalInfo'));
        },

        /**
         * Open dialog for external video
         * @private
         */
        _onOpenDialog: function (e, imageData) {

            if (imageData['media_type'] !== 'external-video' && imageData['media_type'] !== 'internal-video') {
                this._superApply(arguments);
            } else if (imageData['media_type'] === 'internal-video') {
                this.showInternalModal();
            } else {
                this.showModal();
            }
        },

        /**
         * Fired on trigger "openModal"
         */
        showInternalModal: function () {
            this.videoInternalDialog.modal('openModal');
        }
    });

    return $.mage.productGallery;
});

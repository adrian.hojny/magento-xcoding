<?php

namespace Xcoding\ProductVideo\Controller\Adminhtml\Product\Gallery;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\Product\Media\Config;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\Read;
use Magento\MediaStorage\Model\File\Uploader;

class VideoUpload extends Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Catalog::products';

    /**
     * @var RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var array
     */
    private $allowedMimeTypes = [
        'mp4' => 'video/mp4',
        'ogg' => 'video/ogg',
        'webm' => 'video/webm'
    ];

    /**
     * @param Context $context
     * @param RawFactory $resultRawFactory
     */
    public function __construct(
        Context $context,
        RawFactory $resultRawFactory
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;
    }

    /**
     * Upload image(s) to the product gallery.
     *
     * @return Raw
     */
    public function execute()
    {
        try {
            $uploader = $this->_objectManager->create(
                Uploader::class,
                ['fileId' => 'video']
            );
            $uploader->setAllowedExtensions($this->getAllowedExtensions());

            if (!$uploader->checkMimeType($this->getAllowedMimeTypes())) {
                throw new LocalizedException(__('Disallowed File Type.'));
            }

            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            /** @var Read $mediaDirectory */
            $mediaDirectory = $this->_objectManager->get(Filesystem::class)
                ->getDirectoryRead(DirectoryList::MEDIA);
            $config = $this->_objectManager->get(Config::class);
            $result = $uploader->save($mediaDirectory->getAbsolutePath($config->getBaseTmpMediaPath()));

            unset($result['tmp_name']);
            unset($result['path']);
            $result['url'] = $this->_objectManager->get(Config::class)
                ->getTmpMediaShortUrl($result['file']);
            $result['video_file'] = $result['file'] . '.tmp';
            unset($result['file']);
        } catch (Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }

        /** @var Raw $response */
        $response = $this->resultRawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($result));
        return $response;
    }

    /**
     * Get the set of allowed file extensions.
     *
     * @return array
     */
    private function getAllowedExtensions()
    {
        return array_keys($this->allowedMimeTypes);
    }

    /**
     * Get the set of allowed mime types.
     *
     * @return array
     */
    private function getAllowedMimeTypes()
    {
        return array_values($this->allowedMimeTypes);
    }
}

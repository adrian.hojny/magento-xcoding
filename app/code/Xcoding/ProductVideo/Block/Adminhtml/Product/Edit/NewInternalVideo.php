<?php
namespace Xcoding\ProductVideo\Block\Adminhtml\Product\Edit;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Catalog\Model\Product;
use Magento\Framework\Data\Form;
use Magento\Framework\Data\Form\Element\Fieldset;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\ProductVideo\Helper\Media;

/**
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class NewInternalVideo extends Generic
{
    /**
     * Anchor is product video
     */
    const PATH_ANCHOR_PRODUCT_VIDEO = 'catalog_product_internal_video-link';

    /**
     * @var Media
     */
    protected $mediaHelper;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var EncoderInterface
     */
    protected $jsonEncoder;

    /**
     * @var string
     */
    protected $videoSelector = '#media_gallery_content';

    /**
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param Media $mediaHelper
     * @param EncoderInterface $jsonEncoder
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Media $mediaHelper,
        EncoderInterface $jsonEncoder,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->mediaHelper = $mediaHelper;
        $this->urlBuilder = $context->getUrlBuilder();
        $this->jsonEncoder = $jsonEncoder;
        $this->setUseContainer(true);
    }

    /**
     * Form preparation
     *
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        /** @var Form $form */
        $form = $this->_formFactory->create([
            'data' => [
                'id' => 'new_internal_video_form',
                'class' => 'admin__scope-old',
                'enctype' => 'multipart/form-data',
            ]
        ]);
        $form->setUseContainer($this->getUseContainer());
        $form->addField('new_internal_video_messages', 'note', []);
        $fieldset = $form->addFieldset('new_internal_video_form_fieldset', []);
        $fieldset->addField(
            '',
            'hidden',
            [
                'name' => 'form_key',
                'value' => $this->getFormKey(),
            ]
        );

        $fieldset->addField(
            'internal_item_url',
            'hidden',
            [
                'name' => 'video_url'
            ]
        );

        $fieldset->addField(
            'internal_item_id',
            'hidden',
            []
        );
        $fieldset->addField(
            'internal_file_name',
            'hidden',
            []
        );
        $fieldset->addField(
            'internal_video_title',
            'text',
            [
                'class' => 'edited-data',
                'label' => __('Title'),
                'title' => __('Title'),
                'required' => true,
                'name' => 'video_title',
            ]
        );
        $fieldset->addField(
            'internal_video_description',
            'textarea',
            [
                'class' => 'edited-data',
                'label' => __('Description'),
                'title' => __('Description'),
                'name' => 'video_description',
            ]
        );
        $fieldset->addField(
            'new_internal_video_screenshot',
            'file',
            [
                'label' => __('Preview Image'),
                'title' => __('Preview Image'),
                'name' => 'image',
            ]
        );

        $fieldset->addField(
            'new_internal_video_screenshot_preview',
            'button',
            [
                'class' => 'preview-image-hidden-input',
                'label' => '',
                'name' => '_preview',
            ]
        );
        $fieldset->addField(
            'new_internal_video',
            'file',
            [
                'label' => __('Video'),
                'title' => __('Video'),
                'name' => 'video',
            ]
        );
        $this->addMediaRoleAttributes($fieldset);
        $fieldset->addField(
            'new_internal_video_disabled',
            'checkbox',
            [
                'class' => 'edited-data',
                'label' => __('Hide from Product Page'),
                'title' => __('Hide from Product Page'),
                'name' => 'disabled',
            ]
        );
        $this->setForm($form);
    }

    /**
     * Get html id
     *
     * @return mixed
     * @throws LocalizedException
     */
    public function getHtmlId()
    {
        if (null === $this->getData('id')) {
            $this->setData('id', $this->mathRandom->getUniqueHash('id_'));
        }
        return $this->getData('id');
    }

    /**
     * Get widget options
     *
     * @return string
     * @throws LocalizedException
     */
    public function getWidgetOptions()
    {
        return $this->jsonEncoder->encode(
            [
                'saveImageUrl' => $this->getUrl('catalog/product_gallery/upload'),
                'saveVideoUrl' => $this->getUrl('xc_catalog/product_gallery/videoUpload'),
                'saveRemoteVideoUrl' => $this->getUrl('product_video/product_gallery/retrieveImage'),
                'htmlId' => $this->getHtmlId(),
                'youTubeApiKey' => $this->mediaHelper->getYouTubeApiKey(),
                'videoSelector' => $this->videoSelector
            ]
        );
    }

    /**
     * Retrieve currently viewed product object
     *
     * @return Product
     */
    protected function getProduct()
    {
        if (!$this->hasData('product')) {
            $this->setData('product', $this->_coreRegistry->registry('product'));
        }
        return $this->getData('product');
    }

    /**
     * Add media role attributes to fieldset
     *
     * @param Fieldset $fieldset
     * @return $this
     */
    protected function addMediaRoleAttributes(Fieldset $fieldset)
    {
        $fieldset->addField('internal-role-label', 'note', ['text' => __('Role')]);
        $mediaRoles = $this->getProduct()->getMediaAttributes();
        ksort($mediaRoles);
        foreach ($mediaRoles as $mediaRole) {
            $fieldset->addField(
                'internal_video_' . $mediaRole->getAttributeCode(),
                'checkbox',
                [
                    'class' => 'internal_video_image_role',
                    'label' => __($mediaRole->getFrontendLabel()),
                    'title' => __($mediaRole->getFrontendLabel()),
                    'data-role' => 'role-type-selector',
                    'value' => $mediaRole->getAttributeCode(),
                ]
            );
        }
        return $this;
    }
}

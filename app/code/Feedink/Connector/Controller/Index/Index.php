<?php

namespace Feedink\Connector\Controller\Index;

use Feedink\Connector\Api\ConnectorRepositoryInterface;
use Feedink\Connector\Helper\Data;
use Feedink\Connector\Model\AuthenticationInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\HTTP\Authentication;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Index
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Index extends Action
{
    /**
     * @var ConnectorRepositoryInterface
     */
    protected $connectorRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var Authentication
     */
    protected $httpAuthentication;

    /**
     * @var AuthenticationInterface
     */
    protected $authentication;

    /**
     * Index constructor.
     * @param ConnectorRepositoryInterface $connectorRepository
     * @param StoreManagerInterface $storeManager
     * @param Data $helperData
     * @param FileFactory $fileFactory
     * @param Authentication $httpAuthentication
     * @param AuthenticationInterface $authentication
     * @param Context $context
     */
    public function __construct(
        ConnectorRepositoryInterface $connectorRepository,
        StoreManagerInterface $storeManager,
        Data $helperData,
        FileFactory $fileFactory,
        Authentication $httpAuthentication,
        AuthenticationInterface $authentication,
        Context $context
    ) {
        $this->connectorRepository = $connectorRepository;
        $this->storeManager = $storeManager;
        $this->helperData = $helperData;
        $this->fileFactory = $fileFactory;
        $this->httpAuthentication = $httpAuthentication;
        $this->authentication = $authentication;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        try {
            $this->authenticate();
            $storeId = $this->storeManager->getStore()->getId();
            $connector = $this->connectorRepository->getByStoreId($storeId);
            $fileName = $connector->getFilename();

            return $this->fileFactory->create(
                $fileName,
                [
                    'type'  => 'filename',
                    'value' => 'feedink' . DIRECTORY_SEPARATOR . $fileName
                ],
                DirectoryList::VAR_DIR
            );
        } catch (AuthenticationException $e) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $result->setHttpResponseCode(401);
            $result->setData(['message' => $e->getMessage()]);
        } catch (\Exception $e) {
            $result = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $result->setHttpResponseCode(404);
            $result->setData(['message' => __('File not found')]);
        }

        return $result;
    }

    /**
     * @return bool
     * @throws AuthenticationException
     */
    protected function authenticate(): bool
    {
        if ($this->helperData->isBasicAuthEnabled()) {
            list($username, $password) = $this->httpAuthentication->getCredentials();
            $this->authentication->authenticate($username, $password);
        }

        return true;
    }
}

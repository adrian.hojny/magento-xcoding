<?php

namespace Feedink\Connector\Controller\Adminhtml\Connector;

use Feedink\Connector\Api\ConnectorRepositoryInterface;
use Feedink\Connector\Model\ConnectorFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Save extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Feedink_Connector::connector';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var ConnectorFactory
     */
    protected $connectorFactory;

    /**
     * @var ConnectorRepositoryInterface
     */
    protected $connectorRepository;

    /**
     * Save constructor.
     *
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param ConnectorFactory $connectorFactory
     * @param ConnectorRepositoryInterface $connectorRepository
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor,
        ConnectorFactory $connectorFactory,
        ConnectorRepositoryInterface $connectorRepository
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->connectorFactory = $connectorFactory;
        $this->connectorRepository = $connectorRepository;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            if (empty($data['connector_id'])) {
                $data['connector_id'] = null;
            }

            $id = (int)$this->getRequest()->getParam('connector_id');
            $model = $this->connectorFactory->create();

            if ($id) {
                try {
                    $model = $this->connectorRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This connector no longer exists.'));
                    return $resultRedirect->setPath('*/*/index');
                }
            }

            foreach ($data as $field => $value) {
                $model->setDataUsingMethod($field, $value);
            }

            try {
                $this->connectorRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the connector.'));
                $this->dataPersistor->clear('feedink_connector_connector');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['connector_id' => $model->getId()]);
                }

                return $resultRedirect->setPath('*/*/index');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the connector.'));
            }

            $this->dataPersistor->set('feedink_connector_connector', $data);

            return $resultRedirect->setPath('*/*/edit', ['connector_id' => $id]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}

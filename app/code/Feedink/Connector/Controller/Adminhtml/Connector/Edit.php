<?php

namespace Feedink\Connector\Controller\Adminhtml\Connector;

use Feedink\Connector\Api\ConnectorRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Edit
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Edit extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Feedink_Connector::connector';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ConnectorRepositoryInterface
     */
    protected $connectorRepository;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param ConnectorRepositoryInterface $connectorRepository
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Action\Context $context,
        ConnectorRepositoryInterface $connectorRepository,
        PageFactory $resultPageFactory
    ) {
        $this->connectorRepository = $connectorRepository;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Edit connector
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $id = (int)$this->getRequest()->getParam('connector_id');

        if ($id) {
            $model = $this->connectorRepository->getById($id);

            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This connector no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $resultPage = $this->resultPageFactory->create();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Connector') : __('New Connector'),
            $id ? __('Edit Connector') : __('New Connector')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Connectors'));
        $resultPage->getConfig()->getTitle()
            ->prepend($id ? __('Edit Connector') : __('New Connector'));

        return $resultPage;
    }
}

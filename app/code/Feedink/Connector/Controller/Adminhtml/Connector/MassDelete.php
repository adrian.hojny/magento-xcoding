<?php

namespace Feedink\Connector\Controller\Adminhtml\Connector;

use Feedink\Connector\Api\ConnectorRepositoryInterface;
use Feedink\Connector\Model\ResourceModel\Connector\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Exception\NotFoundException;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class MassDelete extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Feedink_Connector::connector';

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var ConnectorRepositoryInterface
     */
    private $connectorRepository;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ConnectorRepositoryInterface $connectorRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ConnectorRepositoryInterface $connectorRepository
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->connectorRepository = $connectorRepository;
        parent::__construct($context);
    }

    /**
     * Mass Delete action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws NotFoundException
     */
    public function execute()
    {
        if (!$this->getRequest()->isPost()) {
            throw new NotFoundException(__('Page not found.'));
        }

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        $deletedItems = 0;
        foreach ($collection as $connector) {
            try {
                $this->connectorRepository->delete($connector);
                $deletedItems++;
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }

        if ($deletedItems != 0) {
            if ($collectionSize != $deletedItems) {
                $this->messageManager->addErrorMessage(
                    __('Failed to delete %1 connector(s).', $collectionSize - $deletedItems)
                );
            }

            $this->messageManager->addSuccessMessage(
                __('A total of %1 connector(s) have been deleted.', $deletedItems)
            );
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}

<?php

namespace Feedink\Connector\Controller\Adminhtml\Connector;

use Magento\Backend\App\Action;
use Feedink\Connector\Api\ConnectorRepositoryInterface;

/**
 * Class Delete
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Delete extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Feedink_Connector::connector';

    /**
     * @var ConnectorRepositoryInterface
     */
    private $connectorRepository;

    /**
     * Delete constructor.
     * @param ConnectorRepositoryInterface $connectorRepository
     * @param Action\Context $context
     */
    public function __construct(
        ConnectorRepositoryInterface $connectorRepository,
        Action\Context $context
    ) {
        $this->connectorRepository = $connectorRepository;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('connector_id');

        if ($id) {
            try {
                $connector = $this->connectorRepository->getById($id);
                $this->connectorRepository->delete($connector);
                $this->messageManager->addSuccess(__('You deleted the connector.'));

                return $resultRedirect->setPath('*/*/index');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());

                return $resultRedirect->setPath('*/*/edit', ['connector_id' => $id]);
            }
        }

        $this->messageManager->addError(__('We can\'t find a connector to delete.'));

        return $resultRedirect->setPath('*/*/index');
    }
}

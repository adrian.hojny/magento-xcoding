<?php

namespace Feedink\Connector\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\Filesystem\Io\File;

/**
 * Class Router
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * @var File
     */
    private $file;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param File $file
     */
    public function __construct(
        ActionFactory $actionFactory,
        File $file
    ) {
        $this->actionFactory = $actionFactory;
        $this->file = $file;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        if (!$this->validate($request)) {
            return null;
        }

        $identifier = trim($request->getPathInfo(), '/');
        if (strpos($identifier, 'feedink') === false) {
            return null;
        }

        $params = explode('/', $identifier);
        if (!is_array($params) || !count($params) === 2) {
            return null;
        }

        $request->setModuleName('feedink');
        $request->setControllerName('index');
        $request->setActionName('index');

        return $this->actionFactory->create(Forward::class, ['request' => $request]);
    }

    /**
     * @param RequestInterface $request
     * @return bool
     */
    protected function validate(RequestInterface $request): bool
    {
        $result = true;

        if ($request->getModuleName() === 'feedink') {
            $result = false;
        }

        if ($request->getActionName() === 'noroute') {
            $result = false;
        }

        if ($request->getControllerName() === 'noroute') {
            $result = false;
        }

        return $result;
    }
}

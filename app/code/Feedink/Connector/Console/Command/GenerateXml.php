<?php

namespace Feedink\Connector\Console\Command;

use Feedink\Connector\Api\ConnectorRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Store\Model\StoreManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateXml
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class GenerateXml extends Command
{
    const STORE = 'store';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ConnectorRepositoryInterface
     */
    private $connectorRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * GenerateXml constructor.
     * @param StoreManagerInterface $storeManager
     * @param ConnectorRepositoryInterface $connectorRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ConnectorRepositoryInterface $connectorRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->storeManager = $storeManager;
        $this->connectorRepository = $connectorRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;

        parent::__construct();
    }

    /**
     * Configure command options
     */
    protected function configure()
    {
        $definition = [
            new InputOption(self::STORE, null, InputOption::VALUE_OPTIONAL, 'Single store code or ID.')
        ];
        $this->setName('feedink:connector:generate');
        $this->setDescription('Generate Feedink files');
        $this->setDefinition($definition);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('is_active', 1);

        if ($storeCode = $input->getOption(self::STORE)) {
            $store = $this->storeManager->getStore($storeCode);
            $searchCriteria->addFilter('store_id', $store->getId());
        }

        $items = $this->connectorRepository
            ->getList($searchCriteria->create())
            ->getItems();

        foreach ($items as $connector) {
            try {
                $store = $connector->getStore();
                $output->writeln(__('<comment>Start generating for "%1" store.</comment>', $store->getName()));
                $start = microtime(true);
                $connector->generate();
                $this->connectorRepository->save($connector);
                $output->writeln(__(
                    '<comment>Generated for "%1" store in %2s.</comment>',
                    $store->getName(),
                    round(microtime(true) - $start, 2)
                ));
                $output->writeln(sprintf('Memory Usage: %d MB' . PHP_EOL, (memory_get_usage()/1048576)));
            } catch (\Exception $e) {
                $connector->unlock();
                $output->writeln("<error>{$e->getMessage()}</error>");
            }
        }
    }
}

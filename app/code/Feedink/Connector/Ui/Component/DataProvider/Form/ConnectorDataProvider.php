<?php

namespace Feedink\Connector\Ui\Component\DataProvider\Form;

use Feedink\Connector\Model\ResourceModel\Connector\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

/**
 * Class ConnectorDataProvider
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class ConnectorDataProvider extends AbstractDataProvider
{
    /**
     * @var CollectionFactory
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * RegistrationDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->getCollection()->getItems();
        /** @var $connector \Feedink\Connector\Model\Connector */
        foreach ($items as $connector) {
            $this->loadedData[$connector->getId()] = $connector->getData();
        }

        $data = $this->dataPersistor->get('feedink_connector_connector');

        if (!empty($data)) {
            $connector = $this->getCollection()->getNewEmptyItem();
            $connector->setData($data);
            $this->loadedData[$connector->getId()] = $connector->getData();
            $this->dataPersistor->clear('feedink_connector_connector');
        }

        return $this->loadedData;
    }
}

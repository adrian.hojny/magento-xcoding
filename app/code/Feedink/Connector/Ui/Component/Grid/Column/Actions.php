<?php

namespace Feedink\Connector\Ui\Component\Grid\Column;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Actions
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Actions extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $name = $this->getData('name');
                $item[$name]['edit'] = [
                    'href' => $this->urlBuilder->getUrl(
                        'feedink/connector/edit',
                        ['connector_id' => $item['connector_id']]
                    ),
                    'label' => __('Edit'),
                    'hidden' => false
                ];
                $item[$name]['delete'] = [
                    'href' => $this->urlBuilder->getUrl(
                        'feedink/connector/delete',
                        ['connector_id' => $item['connector_id']]
                    ),
                    'label' => __('Remove'),
                    'hidden' => false,
                    'confirm' => [
                        'title' => __('Delete "%1"', ['${ $.$data.connector_id }']),
                        'message' => __(
                            'Are you sure you want to delete a "%1"?',
                            ['${ $.$data.connector_id }']
                        )
                    ],
                ];
            }
        }

        return $dataSource;
    }
}

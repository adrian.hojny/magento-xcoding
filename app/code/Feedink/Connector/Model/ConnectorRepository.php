<?php

namespace Feedink\Connector\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Feedink\Connector\Api\ConnectorRepositoryInterface;
use Feedink\Connector\Api\Data\ConnectorInterface;
use Feedink\Connector\Api\Data\ConnectorInterfaceFactory;
use Feedink\Connector\Api\Data\ConnectorSearchResultsInterfaceFactory;
use Feedink\Connector\Model\ResourceModel\Connector as ResourceConnector;
use Feedink\Connector\Model\ResourceModel\Connector\CollectionFactory as ConnectorCollectionFactory;

/**
 * Class ConnectorRepository
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class ConnectorRepository implements ConnectorRepositoryInterface
{
    /**
     * @var ResourceConnector
     */
    private $resource;

    /**
     * @var ConnectorFactory
     */
    private $connectorFactory;

    /**
     * @var ConnectorCollectionFactory
     */
    private $connectorCollectionFactory;

    /**
     * @var ConnectorSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    private $dataObjectProcessor;

    /**
     * @var ConnectorInterfaceFactory
     */
    private $dataConnectorFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * ConnectorRepository constructor.
     * @param ResourceConnector $resource
     * @param \Feedink\Connector\Model\ConnectorFactory $connectorFactory
     * @param ConnectorInterfaceFactory $dataConnectorFactory
     * @param ConnectorCollectionFactory $connectorCollectionFactory
     * @param ConnectorSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceConnector $resource,
        ConnectorFactory $connectorFactory,
        ConnectorInterfaceFactory $dataConnectorFactory,
        ConnectorCollectionFactory $connectorCollectionFactory,
        ConnectorSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->connectorFactory = $connectorFactory;
        $this->dataConnectorFactory = $dataConnectorFactory;
        $this->connectorCollectionFactory = $connectorCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * Save Connector data
     *
     * @param ConnectorInterface $connector
     * @return Connector
     * @throws CouldNotSaveException
     */
    public function save(ConnectorInterface $connector)
    {
        try {
            $this->resource->save($connector);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $connector;
    }

    /**
     * Load Connector data by given Identity
     *
     * @param int $connectorId
     * @return Connector
     * @throws NoSuchEntityException
     */
    public function getById($connectorId)
    {
        $connector = $this->connectorFactory->create();
        $this->resource->load($connector, $connectorId);

        if (!$connector->getId()) {
            throw new NoSuchEntityException(__('No such Connector'));
        }

        return $connector;
    }

    /**
     * Load Connector data by given Store ID
     *
     * @param int $storeId
     * @return Connector
     * @throws NoSuchEntityException
     */
    public function getByStoreId($storeId)
    {
        $connector = $this->connectorFactory->create();
        $this->resource->load($connector, $storeId, 'store_id');

        if (!$connector->getId()) {
            throw new NoSuchEntityException(__('No such Connector'));
        }

        return $connector;
    }

    /**
     * Load connector data collection by given search criteria
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Feedink\Connector\Api\Data\ConnectorSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Feedink\Connector\Model\ResourceModel\Connector\Collection $collection */
        $collection = $this->connectorCollectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\ConnectorSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());

        return $searchResults;
    }

    /**
     * Delete Connector
     *
     * @param ConnectorInterface $connector
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(ConnectorInterface $connector)
    {
        try {
            $this->resource->delete($connector);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * Delete connector by given Identity
     *
     * @param int $connectorId
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function deleteById($connectorId)
    {
        return $this->delete($this->getById($connectorId));
    }
}

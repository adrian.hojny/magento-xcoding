<?php

namespace Feedink\Connector\Model;

use Magento\Framework\Encryption\EncryptorInterface as Encryptor;
use Magento\Framework\Encryption\Helper\Security;
use Magento\Framework\Exception\AuthenticationException;
use Feedink\Connector\Helper\Data;

/**
 * Class Authentication
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Authentication implements AuthenticationInterface
{
    /**
     * @var Encryptor
     */
    private $encryptor;

    /**
     * @var Data
     */
    private $helperData;

    /**
     * @param Encryptor $encryptor
     * @param Data $helperData
     */
    public function __construct(
        Encryptor $encryptor,
        Data $helperData
    ) {
        $this->encryptor = $encryptor;
        $this->helperData = $helperData;
    }

    /**
     * @inheritdoc
     */
    public function authenticate($username, $password)
    {
        if (!Security::compareStrings($this->helperData->getAuthUsername(), $username)) {
            throw new AuthenticationException(__('Invalid username or password.'));
        }

        if (!Security::compareStrings($this->encryptor->decrypt($this->helperData->getAuthPasswordHash()), $password)) {
            throw new AuthenticationException(__('Invalid username or password.'));
        }

        return true;
    }
}

<?php

namespace Feedink\Connector\Model;

use Feedink\Connector\Api\ProductListInterface;
use Feedink\Connector\Model\ResourceModel\AddMediaGalleryToCollection;
use Feedink\Connector\Model\ResourceModel\AddParentUrlToCollection;
use Feedink\Connector\Model\ResourceModel\AddRelationToCollection;
use Feedink\Connector\Model\ResourceModel\AddSuperAttributeToCollection;
use Magento\Catalog\Api\Data\ProductSearchResultsInterfaceFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogInventory\Model\ResourceModel\Stock\Status as StockStatus;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Feedink\Connector\Api\Data\ConnectorInterface;

/**
 * Class ProductList
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class ProductList implements ProductListInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var ProductSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var AddRelationToCollection
     */
    private $addRelationToCollection;

    /**
     * @var AddParentUrlToCollection
     */
    private $addParentUrlToCollection;

    /**
     * @var AddSuperAttributeToCollection
     */
    private $addSuperAttributeToCollection;

    /**
     * @var AddMediaGalleryToCollection
     */
    private $addMediaGalleryToCollection;

    /**
     * @var StockStatus
     */
    private $stockStatus;

    /**
     * @var ConnectorInterface
     */
    private $connector;

    /**
     * Product List constructor.
     *
     * @param CollectionFactory $collectionFactory
     * @param ProductSearchResultsInterfaceFactory $searchResultsFactory
     * @param AddRelationToCollection $addRelationToCollection
     * @param AddParentUrlToCollection $addParentUrlToCollection
     * @param AddSuperAttributeToCollection $addSuperAttributeToCollection
     * @param AddMediaGalleryToCollection $addMediaGalleryToCollection
     * @param StockStatus $stockStatus
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        ProductSearchResultsInterfaceFactory $searchResultsFactory,
        AddRelationToCollection $addRelationToCollection,
        AddParentUrlToCollection $addParentUrlToCollection,
        AddSuperAttributeToCollection $addSuperAttributeToCollection,
        AddMediaGalleryToCollection $addMediaGalleryToCollection,
        StockStatus $stockStatus,
        ConnectorInterface $connector = null,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->addRelationToCollection = $addRelationToCollection;
        $this->addParentUrlToCollection = $addParentUrlToCollection;
        $this->addSuperAttributeToCollection = $addSuperAttributeToCollection;
        $this->addMediaGalleryToCollection = $addMediaGalleryToCollection;
        $this->stockStatus = $stockStatus;
        $this->connector = $connector;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return \Magento\Catalog\Api\Data\ProductSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $attributes = $this->connector->getProductAttributes();
        $collection = $this->collectionFactory->create();
        $collection->setStoreId($this->connector->getStoreId());
        $collection->addAttributeToSelect($attributes);
        $collection->addMinimalPrice();
        $collection->joinAttribute('status', 'catalog_product/status', 'entity_id', null, 'inner');
        $collection->joinAttribute('visibility', 'catalog_product/visibility', 'entity_id', null, 'inner');

        $this->collectionProcessor->process($searchCriteria, $collection);

        $this->stockStatus->addStockDataToCollection($collection, false);
        $collection->load();
        $collection->addCategoryIds();
        $collection->addUrlRewrite();

        $this->addMediaGalleryToCollection->execute($collection);
        $this->addRelationToCollection->execute($collection);
        $this->addSuperAttributeToCollection->execute($collection);
        $this->addParentUrlToCollection->execute($collection);

        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->count());

        return $searchResult;
    }
}

<?php

namespace Feedink\Connector\Model\Config\Backend;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Config\Value as ConfigValue;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Feedink\Connector\Model\Config\Source\Frequency;

/**
 * Class Cron
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Cron extends ConfigValue
{
    /**
     * XML config path
     */
    const CRON_STRING_PATH = 'crontab/feedink/jobs/feedink_connector/schedule/cron_expr';

    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $config
     * @param TypeListInterface $cacheTypeList
     * @param WriterInterface $configWriter
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        WriterInterface $configWriter,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->configWriter = $configWriter;

        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Cron settings after save
     *
     * @return ConfigValue
     * @throws LocalizedException
     */
    public function afterSave()
    {
        try {
            $this->configWriter->save(self::CRON_STRING_PATH, $this->getExpression());
        } catch (\Exception $e) {
            throw new LocalizedException(__('We can\'t save the Cron expression.'));
        }

        return parent::afterSave();
    }

    /**
     * Retrieve cron expression string.
     *
     * @return string
     * @throws LocalizedException
     */
    private function getExpression()
    {
        $frequency = $this->getData('groups/cron/fields/frequency/value');
        $hours = (int)$this->getData('groups/cron/fields/hours/value');
        $time = $this->getData('groups/cron/fields/time/value');

        switch ($frequency) {
            case Frequency::CRON_HOURLY:
                if (!in_array($hours, range(1, 23))) {
                    throw new LocalizedException(__('Hours not valid.'));
                }
                $cronExprArray = [0, '*/' . $hours, '*', '*', '*'];
                break;
            default:
                $cronExprArray = [
                    (int)$time[1],
                    (int)$time[0],
                    $frequency == Frequency::CRON_MONTHLY ? '1' : '*',
                    '*',
                    $frequency == Frequency::CRON_WEEKLY ? '1' : '*',
                ];
        }

        return join(' ', $cronExprArray);
    }
}

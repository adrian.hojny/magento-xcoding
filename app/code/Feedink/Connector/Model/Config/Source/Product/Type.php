<?php

namespace Feedink\Connector\Model\Config\Source\Product;

use Magento\Catalog\Api\ProductTypeListInterface;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Type
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Type implements OptionSourceInterface
{
    protected $options;

    /**
     * @var ProductTypeListInterface
     */
    protected $productTypeList;

    /**
     * Type constructor.
     * @param ProductTypeListInterface $productTypeList
     */
    public function __construct(ProductTypeListInterface $productTypeList)
    {
        $this->productTypeList = $productTypeList;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            foreach ($this->productTypeList->getProductTypes() as $productType) {
                $this->options[] = [
                    'value' => $productType->getName(),
                    'label' => $productType->getLabel()
                ];
            }
        }

        return $this->options;
    }
}

<?php

namespace Feedink\Connector\Model\Config\Source\Product;

use Magento\Catalog\Model\Product\AttributeSet\Options;

/**
 * Class AttributeSet
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class AttributeSet extends Options
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = parent::toOptionArray();
        $defaultOption[] = ['value' => '', 'label' => __('-- Disabled --')];

        return array_merge($defaultOption, $options);
    }
}

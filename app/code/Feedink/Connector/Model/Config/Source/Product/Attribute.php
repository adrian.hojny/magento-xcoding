<?php

namespace Feedink\Connector\Model\Config\Source\Product;

use Feedink\Connector\Helper\Data;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Attribute
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Attribute implements OptionSourceInterface
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var Data
     */
    private $helperData;

    /**
     * @var array
     */
    private $options;

    /**
     * Attribute constructor.
     * @param AttributeRepositoryInterface $attributeRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     * @param Data $helperData
     */
    public function __construct(
        AttributeRepositoryInterface $attributeRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        Data $helperData
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->helperData = $helperData;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $sortOrder = $this->sortOrderBuilder
                ->setField(AttributeInterface::ATTRIBUTE_CODE)
                ->setAscendingDirection()
                ->create();
            $searchCriteria = $this->searchCriteriaBuilder
                ->addFilter(
                    AttributeInterface::ATTRIBUTE_CODE,
                    $this->helperData->getRequiredAttributes(),
                    'nin'
                )->addFilter(
                    AttributeInterface::BACKEND_TYPE,
                    'static',
                    'neq'
                )->addFilter(
                    AttributeInterface::FRONTEND_INPUT,
                    'media_image',
                    'neq'
                )->addSortOrder(
                    $sortOrder
                )->create();
            $attributes = $this->attributeRepository->getList(
                ProductAttributeInterface::ENTITY_TYPE_CODE,
                $searchCriteria
            );
            $this->options = [];

            foreach ($attributes->getItems() as $attribute) {
                $this->options[] = [
                    'value' => $attribute->getAttributeCode(),
                    'label' => $attribute->getDefaultFrontendLabel()
                ];
            }
        }

        return $this->options;
    }
}

<?php

namespace Feedink\Connector\Model\Config\Source\Product;

use Magento\CatalogInventory\Model\Source\Stock as SourceStock;

/**
 * Class Stock
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Stock extends SourceStock
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = parent::toOptionArray();
        $defaultOption[] = ['value' => '', 'label' => __('-- Disabled --')];

        return array_merge($defaultOption, $options);
    }
}

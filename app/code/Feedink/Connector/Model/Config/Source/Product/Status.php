<?php

namespace Feedink\Connector\Model\Config\Source\Product;

use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;

/**
 * Class Status
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Status extends ProductStatus
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = parent::toOptionArray();
        $defaultOption[] = ['value' => '', 'label' => __('-- Disabled --')];

        return array_merge($defaultOption, $options);
    }
}

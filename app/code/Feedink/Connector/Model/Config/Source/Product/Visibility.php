<?php

namespace Feedink\Connector\Model\Config\Source\Product;

use Magento\Catalog\Model\Product\Visibility as ProductVisibility;

/**
 * Class Visibility
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Visibility extends ProductVisibility
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = parent::toOptionArray();
        $defaultOption[] = ['value' => '', 'label' => __('-- Disabled --')];

        return array_merge($defaultOption, $options);
    }
}

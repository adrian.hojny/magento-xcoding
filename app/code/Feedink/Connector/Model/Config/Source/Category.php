<?php

namespace Feedink\Connector\Model\Config\Source;

use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Category
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Category implements OptionSourceInterface
{
    const DELIMITER = ' > ';

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var array
     */
    private $categoryMap;

    /**
     * @var array
     */
    private $options;

    /**
     * Category constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return array|null
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $collection = $this->collectionFactory->create()
                ->addNameToResult()
                ->addOrderField('level')
                ->setPageSize(1000);
            $lastPage = $collection->getLastPageNumber();
            $page = 1;
            $this->options = [
                ['value' => '', 'label' => __('-- Disabled --')]
            ];

            while ($page <= $lastPage) {
                $collection->setCurPage($page);

                foreach ($collection as $category) {
                    $categoryName = $this->getFullCategoryName($category);
                    $this->categoryMap[$category->getId()] = $categoryName;

                    if ($categoryName) {
                        $this->options[$category->getPath()] = [
                            'value' => $category->getId(),
                            'label' => $categoryName
                        ];
                    }
                }

                $collection->clear();
                $page++;
            }

            ksort($this->options);
        }

        return $this->options;
    }

    /**
     * Gets category name joined with its parents names
     *
     * @param CategoryInterface $category
     * @return string
     */
    private function getFullCategoryName(CategoryInterface $category)
    {
        $parentId = $category->getParentId();
        if (!$parentId || $parentId === 1) {
            return '';
        }

        $prefix = isset($this->categoryMap[$parentId]) && !empty($this->categoryMap[$parentId])
            ? $this->categoryMap[$parentId] . self::DELIMITER : '';

        return $prefix . $category->getName();
    }
}

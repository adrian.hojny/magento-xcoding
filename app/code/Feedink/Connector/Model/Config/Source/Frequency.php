<?php

namespace Feedink\Connector\Model\Config\Source;

/**
 * Class Frequency
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Frequency implements \Magento\Framework\Data\OptionSourceInterface
{
    const CRON_HOURLY = 'H';
    const CRON_DAILY = 'D';
    const CRON_WEEKLY = 'W';
    const CRON_MONTHLY = 'M';

    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::CRON_HOURLY,
                'label' => __('Every X hour'),
            ],
            [
                'value' => self::CRON_DAILY,
                'label' => __('Daily'),
            ],
            [
                'value' => self::CRON_WEEKLY,
                'label' => __('Weekly'),
            ],
            [
                'value' => self::CRON_MONTHLY,
                'label' => __('Monthly'),
            ]
        ];
    }
}

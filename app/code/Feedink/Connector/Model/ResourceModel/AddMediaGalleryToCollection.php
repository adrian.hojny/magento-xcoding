<?php

namespace Feedink\Connector\Model\ResourceModel;

use Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\Gallery;
use Magento\Framework\DB\Select;

/**
 * Class AddMediaGalleryToCollection
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class AddMediaGalleryToCollection
{
    /**
     * @var Gallery
     */
    private $mediaGalleryResource;

    /**
     * @var GalleryReadHandler
     */
    private $productGalleryReadHandler;

    /**
     * AddMediaGalleryToCollection constructor.
     * @param Gallery $gallery
     * @param GalleryReadHandler $productGalleryReadHandler
     */
    public function __construct(
        Gallery $gallery,
        GalleryReadHandler $productGalleryReadHandler
    ) {
        $this->mediaGalleryResource = $gallery;
        $this->productGalleryReadHandler = $productGalleryReadHandler;
    }

    /**
     * @param Collection $collection
     * @return void
     */
    public function execute(Collection $collection)
    {
        if ($collection->getFlag('media_gallery_added')) {
            return;
        }

        if (!$collection->count()) {
            return;
        }

        $items = $collection->getItems();
        $linkField = $collection->getProductEntityMetadata()->getLinkField();

        $select = $this->getMediaGalleryResource()
            ->createBatchBaseSelect(
                $collection->getStoreId(),
                $collection->getAttribute('media_gallery')->getAttributeId()
            )->reset(
                Select::ORDER // we don't care what order is in current scenario
            )->where(
                'entity.' . $linkField . ' IN (?)',
                array_map(
                    function ($item) use ($linkField) {
                        return (int) $item->getOrigData($linkField);
                    },
                    $items
                )
            );

        $mediaGalleries = [];
        foreach ($collection->getConnection()->fetchAll($select) as $row) {
            $mediaGalleries[$row[$linkField]][] = $row;
        }

        foreach ($items as $item) {
            $this->getGalleryReadHandler()
                ->addMediaDataToProduct(
                    $item,
                    $mediaGalleries[$item->getOrigData($linkField)] ?? []
                );
        }

        $collection->setFlag('media_gallery_added', true);

        return $this;
    }

    /**
     * @return Gallery
     */
    private function getMediaGalleryResource()
    {
        return $this->mediaGalleryResource;
    }

    /**
     * @return GalleryReadHandler
     */
    private function getGalleryReadHandler()
    {
        return $this->productGalleryReadHandler;
    }
}

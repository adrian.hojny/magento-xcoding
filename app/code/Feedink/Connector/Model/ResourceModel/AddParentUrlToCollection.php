<?php

namespace Feedink\Connector\Model\ResourceModel;

use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\Product;

/**
 * Class AddParentUrlToCollection
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class AddParentUrlToCollection
{
    /**
     * @param Collection $collection
     * @return void
     */
    public function execute(Collection $collection)
    {
        if ($collection->getFlag('parent_url_added')) {
            return;
        }

        $productIds = [];

        foreach ($collection->getItems() as $item) {
            if (!$item->hasData('request_path')) {
                $productIds[] = $item->getId();
            }
        }

        $adapter = $collection->getConnection();
        $select = $adapter->select();
        $resource = $collection->getResource();

        $select->from(
            ['cpr' => $resource->getTable('catalog_product_relation')],
            ['child_id']
        );
        $select->join(
            ['ur' => $resource->getTable('url_rewrite')],
            implode(' AND ', [
                'cpr.parent_id = ur.entity_id',
                'ur.entity_type = "product"',
                'ur.store_id =' . $collection->getStoreId()
            ]),
            ['request_path']
        );
        $select->where('child_id IN (?)', $productIds);
        $select->group('cpr.child_id');
        $data = $adapter->fetchAll($select);

        foreach ($data as $row) {
            $product = $collection->getItemById($row['child_id']);

            if ($product instanceof Product) {
                $product->setData('request_path', $row['request_path']);
            }
        }

        $collection->setFlag('parent_url_added', true);
    }
}

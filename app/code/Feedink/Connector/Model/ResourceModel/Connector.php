<?php

namespace Feedink\Connector\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Connector
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Connector extends AbstractDb
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('feedink_connector', 'connector_id');
    }

    /**
     * Initialize unique fields
     *
     * @return Connector
     */
    protected function _initUniqueFields()
    {
        $this->_uniqueFields = [
            ['field' => ['filename'], 'title' => __('Filename')],
            ['field' => ['store_id'], 'title' => __('Feed for this store')]
        ];
        return $this;
    }
}

<?php

namespace Feedink\Connector\Model\ResourceModel;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\Collection;

/**
 * Class AddRelationToCollection
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class AddRelationToCollection
{
    /**
     * @param Collection $collection
     * @return void
     */
    public function execute(Collection $collection)
    {
        if ($collection->getFlag('product_relation_added')) {
            return;
        }

        $parentIds = [];
        $childIds = [];

        foreach ($collection->getItems() as $item) {
            if (in_array($item->getTypeId(), ['bundle', 'grouped', 'configurable'])) {
                $parentIds[] = $item->getEntityId();
            } else {
                $childIds[] = $item->getEntityId();
            }
        }

        $adapter = $collection->getConnection();
        $select = $adapter->select();
        $resource = $collection->getResource();

        $select->from(
            ['cpr' => $resource->getTable('catalog_product_relation')],
            ['parent_id', 'child_id']
        );
        $select->join(
            ['e' => $resource->getTable('catalog_product_entity')],
            'cpr.parent_id = e.entity_id',
            ['sku']
        );
        $select->where('cpr.parent_id IN (?)', $parentIds);
        $select->orWhere('cpr.child_id IN (?)', $childIds);
        $data = $adapter->fetchAll($select);

        foreach ($data as $row) {
            $parent = $collection->getItemById($row['parent_id']);
            $child = $collection->getItemById($row['child_id']);

            if ($parent instanceof Product) {
                $childIds = $parent->getData('child_ids');

                if (is_array($childIds)) {
                    if (!in_array($row['child_id'], $childIds)) {
                        $childIds[] = $row['child_id'];
                        $parent->setData('child_ids', $childIds);
                    }
                } else {
                    $parent->setData('child_ids', [$row['child_id']]);
                }
            }

            if ($child instanceof Product) {
                $parentIds = $child->getData('parent_ids');

                if (is_array($parentIds)) {
                    if (!in_array($row['parent_id'], $parentIds)) {
                        $parentIds[] = $row['parent_id'];
                        $child->setData('parent_ids', $childIds);
                    }
                } else {
                    $child->setData('parent_ids', [$row['parent_id']]);
                }

                $parentSkus = $child->getData('parent_skus');

                if (is_array($parentSkus)) {
                    if (!in_array($row['sku'], $parentSkus)) {
                        $parentSkus[] = $row['sku'];
                        $child->setData('parent_skus', $parentSkus);
                    }
                } else {
                    $child->setData('parent_skus', [$row['sku']]);
                }
            }
        }

        $collection->setFlag('product_relation_added', true);
    }
}

<?php

namespace Feedink\Connector\Model\ResourceModel;

use Magento\Catalog\Model\ResourceModel\Product\Collection;

/**
 * Class AddSuperAttributeToCollection
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class AddSuperAttributeToCollection
{
    private $attributes = [
        'color', 'size'
    ];

    /**
     * @param Collection $collection
     * @return void
     */
    public function execute(Collection $collection)
    {
        if ($collection->getFlag('super_attribute_added')) {
            return;
        }

        $productIds = [];

        foreach ($collection->getItems() as $item) {
            $parentIds = $item->getData('parent_ids');

            if (is_array($parentIds)) {
                foreach ($parentIds as $parentId) {
                    if (!in_array($parentId, $productIds)) {
                        $productIds[] = $parentId;
                    }
                }
            }
        }

        $adapter = $collection->getConnection();
        $select = $adapter->select();
        $resource = $collection->getResource();

        $select->from(
            ['cpsa' => $resource->getTable('catalog_product_super_attribute')],
            ['product_id']
        );
        $select->join(
            ['ea' => $resource->getTable('eav_attribute')],
            'cpsa.attribute_id = ea.attribute_id',
            ['attribute_code' => 'GROUP_CONCAT(attribute_code)']
        );
        $select->where('cpsa.product_id IN (?)', $productIds);
        $select->where('ea.attribute_code IN (?)', $this->attributes);
        $select->group('cpsa.product_id');
        $data = $adapter->fetchPairs($select);

        foreach ($collection->getItems() as $item) {
            $parentIds = (array)$item->getData('parent_ids');

            if (!empty($parentIds) && !$item->hasData('super_attribute')) {
                foreach ($parentIds as $parentId) {
                    if (isset($data[$parentId])) {
                        $superAttribute = explode(',', $data[$parentId]);
                        $item->setData('super_attribute', $superAttribute);
                    }
                }
            }
        }

        $collection->setFlag('super_attribute_added', true);
    }
}

<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class Type
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Type implements ParserInterface
{
    /**
     * Available variants
     */
    const NO_VARIANT = 0;
    const VARIANT_COLOR = 1;
    const VARIANT_SIZE = 2;
    const VARIANT_COLOR_SIZE = 3;

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return mixed
     */
    public function getData(ProductInterface $product, $field)
    {
        $attributes = $product->getData('super_attribute');

        if ($attributes) {
            if (count($attributes) > 1) {
                return self::VARIANT_COLOR_SIZE;
            }

            if (in_array('color', $attributes)) {
                return self::VARIANT_COLOR;
            }

            if (in_array('size', $attributes)) {
                return self::VARIANT_SIZE;
            }
        }

        return self::NO_VARIANT;
    }
}

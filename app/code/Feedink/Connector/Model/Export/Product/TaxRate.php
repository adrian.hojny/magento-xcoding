<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Tax\Api\TaxCalculationInterface;

/**
 * Class TaxRate
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class TaxRate implements ParserInterface
{
    /**
     * @var TaxCalculationInterface
     */
    private $taxCalculation;

    /**
     * @var array
     */
    private $taxRates = [];

    /**
     * Rate constructor.
     *
     * @param TaxCalculationInterface $taxCalculation
     */
    public function __construct(
        TaxCalculationInterface $taxCalculation
    ) {
        $this->taxCalculation = $taxCalculation;
    }

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return mixed
     */
    public function getData(ProductInterface $product, $field)
    {
        return $this->getTaxRate($product->getStoreId(), $product->getTaxClassId());
    }

    /**
     * Retrieve tax rate
     *
     * @param $storeId
     * @param $taxClassId
     * @return float
     */
    private function getTaxRate($storeId, $taxClassId)
    {
        if (!isset($this->taxRates[$storeId][$taxClassId])) {
            $rate = $this->taxCalculation->getCalculatedRate($taxClassId, null, $storeId);
            $this->taxRates[$storeId][$taxClassId] = $rate;
        }

        return $this->taxRates[$storeId][$taxClassId];
    }
}

<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class Parser
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Parser
{
    const DEFAULT_PARSER = 'attribute';

    /**
     * @var array
     */
    protected $parsers;

    /**
     * @var array
     */
    protected $fields = [
        'sku',
        'type',
        'group_id',
        'name',
        'meta_title',
        'url',
        'price',
        'tax_rate',
        'old_price',
        'special_from_date',
        'special_to_date',
        'availability',
        'description',
        'meta_description',
        'news',
        'weight',
        'weight_type',
        'size',
        'category_ids',
        'category_paths',
        'images',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $fieldsMap = [
        'sku' => 'id',
        'name' => 'title',
        'special_from_date' => 'sale_price_from_date',
        'special_to_date' => 'sale_price_to_date'
    ];

    /**
     * @var array
     */
    protected $skippedAttributes = [
        'request_path',
        'required_options',
        'has_options',
        'special_price',
        'final_price',
        'minimal_price',
        'min_price',
        'max_price',
        'tier_price'
    ];

    /**
     * Parser constructor.
     *
     * @param array $parsers
     */
    public function __construct(
        $parsers = []
    ) {
        $this->parsers = $parsers;
    }

    /**
     * @param ProductInterface $product
     * @return array
     */
    public function parse(ProductInterface $product)
    {
        $productData = $product->getData();
        $mappedData = [];

        foreach ($this->fields as $field) {
            $key = $field;

            if (isset($this->fieldsMap[$field])) {
                $key = $this->fieldsMap[$field];
            }

            $mappedData[$key] = $this->getFieldData($product, $field);
            unset($productData[$field]);
            unset($productData[$key]);
        }

        $mappedData = $this->fillConfigurableData($product, $mappedData);
        $mappedData['attributes'] = [];

        foreach ($productData as $attributeName => $value) {
            if (in_array($attributeName, $this->skippedAttributes)) {
                continue;
            }

            $mappedData['attributes'][$attributeName] = $this->getFieldData($product, $attributeName);
        }

        $mappedData['attributes']['currency'] = $this->getFieldData($product, 'currency');

        return $mappedData;
    }

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return mixed
     */
    private function getFieldData(ProductInterface $product, $field)
    {
        $parserKey = $field;
        if (isset($this->parsers[$parserKey])) {
            if (!($this->parsers[$parserKey] instanceof ParserInterface)) {
                throw new \InvalidArgumentException(
                    __('Parser %s must implement %s interface.', $field, ParserInterface::class)
                );
            }

            return $this->parsers[$parserKey]->getData($product, $field);
        }

        return $this->parsers[self::DEFAULT_PARSER]->getData($product, $field);
    }

    /**
     * @param ProductInterface $product
     * @param array $data
     * @return array
     */
    protected function fillConfigurableData(ProductInterface $product, $data)
    {
        if ($product->getTypeId() != 'configurable') {
            return $data;
        }

        $attributes = $product->getTypeInstance()->getConfigurableAttributes($product);
        /** @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute $attribute */
        foreach ($attributes as $attribute) {
            $attrValues = [];
            foreach ($attribute->getOptions() as $option) {
                $attrValues[] = $option['label'];
            }

            $data[$attribute->getProductAttribute()->getAttributeCode()] = implode(', ', $attrValues);
        }

        return $data;
    }
}

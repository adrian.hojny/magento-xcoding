<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Media\ConfigInterface;

/**
 * Class Images
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Images implements ParserInterface
{
    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * Images constructor.
     *
     * @param ConfigInterface $config
     */
    public function __construct(
        ConfigInterface $config
    ) {
        $this->config = $config;
    }

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return mixed
     */
    public function getData(ProductInterface $product, $field)
    {
        $mediaGallery = $product->getMediaGalleryEntries();
        $images = [];

        if (empty($mediaGallery)) {
            return $images;
        }

        foreach ($mediaGallery as $entry) {
            $imgUrl = $this->config->getMediaUrl($entry->getFile());
            /**
             * @see https://github.com/magento/magento2/issues/5321
             */
            $imgUrl = str_replace('/pub/media/catalog/product/', '/media/catalog/product/', $imgUrl);
            $images[] = $imgUrl;
        }

        return $images;
    }
}

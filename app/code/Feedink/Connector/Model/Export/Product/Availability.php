<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class Availability
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Availability implements ParserInterface
{
    /**
     * @param ProductInterface $product
     * @param string $field
     * @return int
     */
    public function getData(ProductInterface $product, $field)
    {
        return (int)$product->getData('is_salable');
    }
}

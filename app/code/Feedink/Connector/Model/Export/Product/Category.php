<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use function array_key_exists;
use function array_reverse;
use function explode;
use function implode;
use function join;

/**
 * Class Category
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Category implements ParserInterface
{
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CollectionFactory
     */
    private $collection;

    /**
     * @var array
     */
    private $categoryData;

    private $fetchedCollection = [];


    /**
     * Category constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return array|mixed
     */
    public function getData(ProductInterface $product, $field)
    {
        $result = [];
        $categoryIds = $product->getCategoryIds();

        if (empty($categoryIds)) {
            return $result;
        }

        foreach ($categoryIds as $categoryId) {
            if (isset($this->categoryData[$categoryId][$field])) {
                if (!empty($this->categoryData[$categoryId][$field])) {
                    $result[] = $this->categoryData[$categoryId][$field];
                }

                continue;
            }

            $data = [];
            $category = $this->getCategory($categoryId, $product->getStoreId());
            if (!$category) {
                $this->categoryData[$categoryId][$field] = null;
                continue;
            }

            $category->setStoreId($product->getStoreId());
            if (!$this->getPathInStore($category)) {
                $this->categoryData[$categoryId][$field] = null;
                continue;
            }

            $pathIds = explode(',', (string)$this->getPathInStore($category));
            $pathIds = array_reverse($pathIds);

            if ($field == 'category_ids') {
                $data = $pathIds;
            } elseif ($field == 'category_paths') {
                foreach ($pathIds as $id) {
                    $parent = $this->getCategory($id, $product->getStoreId());

                    if ($parent) {
                        $data[] = $parent->getName();
                    }
                }
            }

            $value = join($this->getPathSeparator(), $data);
            $this->categoryData[$categoryId][$field] = $value;
            $result[] = $value;
        }

        return $result;
    }

    /**
     * Retrieve category model
     *
     * @param $categoryId
     * @param $storeId
     * @return \Magento\Framework\DataObject|null
     */
    private function getCategory($categoryId, $storeId)
    {
        if (!array_key_exists($storeId, $this->fetchedCollection) || $this->fetchedCollection[$storeId] !== true) {
            $this->collection = $this->collectionFactory->create()
                ->addAttributeToSelect('name')
                ->addFieldToFilter('is_active', 1)
                ->setStoreId($storeId)
                ->load();

            $this->fetchedCollection[$storeId] = true;
        }

        return $this->collection->getItemById($categoryId);
    }

    /**
     * @return string
     */
    private function getPathSeparator()
    {
        return ' > ';
    }

    /**
     * @param $category
     * @return string
     */
    public function getPathInStore($category)
    {
        $result = [];
        $path = array_reverse($category->getPathIds());
        foreach ($path as $itemId) {
            if ($itemId == $category->getStore()->getRootCategoryId()) {
                break;
            }
            $result[] = $itemId;
        }

        return implode(',', $result);
    }
}

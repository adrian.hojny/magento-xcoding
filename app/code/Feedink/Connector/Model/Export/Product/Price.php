<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Store\Model\StoreManagerInterface;
use function array_key_exists;

/**
 * Class Price
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Price implements ParserInterface
{
    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var CurrencyFactory
     */
    private $currencyFactory;

    /**
     * @var array
     */
    private $fetchedCurrencies = [];

    /**
     * Currency constructor.
     * @param StoreManagerInterface $storeManager
     * @param CurrencyFactory $currencyFactory
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        CurrencyFactory $currencyFactory
    )
    {
        $this->storeManager = $storeManager;
        $this->currencyFactory = $currencyFactory;
    }

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return mixed
     */
    public function getData(ProductInterface $product, $field)
    {
        $value = 0;

        if ($field == 'price') {
            $value = $product->getData('min_price');
        } elseif ($field == 'old_price') {
            if (!$product->getSpecialPrice()) {
                return null;
            }

            $value = $product->getData('price');
        }

        $storeId = $product->getStoreId();
        $store = $this->storeManager->getStore($storeId);
        $baseCurrency = $store->getBaseCurrency()->getCode();
        $currencyCode = $store->getCurrentCurrency()->getCode();

        if (!array_key_exists($storeId, $this->fetchedCurrencies)) {
            $this->fetchedCurrencies[$storeId] = $this->currencyFactory->create()->load($baseCurrency);
        }

        if ($currencyCode !== $baseCurrency) {
            $value = $this->fetchedCurrencies[$storeId]->convert($value, $currencyCode);
        }

        return number_format($value, 2);
    }
}

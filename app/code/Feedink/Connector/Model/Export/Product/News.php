<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;

/**
 * Class News
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class News implements ParserInterface
{
    /**
     * @var TimezoneInterface
     */
    protected $localeDate;

    /**
     * Rate constructor.
     *
     * @param TimezoneInterface $localeDate
     */
    public function __construct(
        TimezoneInterface $localeDate
    ) {
        $this->localeDate = $localeDate;
    }

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return mixed
     */
    public function getData(ProductInterface $product, $field)
    {
        if (!$product->getNewsFromDate() && !$product->getNewsToDate()) {
            return 0;
        }

        return (int) $this->localeDate->isScopeDateInInterval(
            null,
            $product->getNewsFromDate(),
            $product->getNewsToDate()
        );
    }
}

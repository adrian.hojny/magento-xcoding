<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Currency
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Currency implements ParserInterface
{
    /**
     * Store manager
     *
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Currency constructor.
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return string
     */
    public function getData(ProductInterface $product, $field)
    {
        $store = $product->getStoreId();

        return $this->getCurrentCurrencyCode($store);
    }

    /**
     * Retrieve currency code
     *
     * @param null $store
     * @return mixed
     */
    private function getCurrentCurrencyCode($store = null)
    {
        return $this->storeManager->getStore($store)->getCurrentCurrency()->getCode();
    }
}

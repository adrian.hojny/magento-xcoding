<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Magento\Framework\Registry;

/**
 * Class Attribute
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Attribute implements ParserInterface
{
    /**
     * @var Registry
     */
    private $coreRegistry;

    /**
     * @var CollectionFactory
     */
    private $attributeColFactory;

    /**
     * @var array
     */
    private $attributeValues = [];

    /**
     * @var array
     */
    private $attributeTypes = [];

    /**
     * Attribute constructor.
     * @param Registry $coreRegistry
     * @param CollectionFactory $attributeColFactory
     */
    public function __construct(
        Registry $coreRegistry,
        CollectionFactory $attributeColFactory
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->attributeColFactory = $attributeColFactory;
    }

    public function getData(ProductInterface $product, $field)
    {
        $this->initAttributes($product->getStoreId());
        $attrValue = $product->getData($field);

        if (!$this->isValidAttributeValue($field, $attrValue)) {
            return $attrValue;
        }

        if (isset($this->attributeValues[$field][$attrValue]) && !empty($this->attributeValues[$field])) {
            $attrValue = $this->attributeValues[$field][$attrValue];
        }

        if ($this->attributeTypes[$field] !== 'multiselect') {
            if (is_scalar($attrValue)) {
                $attrValue = htmlspecialchars_decode($attrValue);
            }
        } else {
            $attrValue = implode(', ', $this->getMultiselectValues($product, $field));
        }

        return $attrValue;
    }

    /**
     * Initialize attribute option values and types.
     *
     * @param int|null $store
     * @return $this
     */
    protected function initAttributes($store = null)
    {
        if (empty($this->attributeValues)) {
            foreach ($this->getAttributeCollection($store) as $attribute) {
                $this->attributeValues[$attribute->getAttributeCode()] = $this->getAttributeOptions($attribute);
                $this->attributeTypes[$attribute->getAttributeCode()] = $attribute->getFrontendInput();
            }
        }

        return $this;
    }

    /**
     * Entity attributes collection getter.
     *
     * @param int|null $store
     * @return Collection
     */
    public function getAttributeCollection($store = null)
    {
        $attrCollection = $this->attributeColFactory->create()->addStoreLabel($store);
        $connector = $this->coreRegistry->registry('current_connector');

        if ($connector instanceof \Feedink\Connector\Model\Connector) {
            $attributes = $connector->getProductAttributes();
            $attrCollection->setCodeFilter($attributes);
        }

        return $attrCollection;
    }

    /**
     * Check attribute is valid.
     *
     * @param string $code
     * @param mixed $value
     * @return bool
     */
    protected function isValidAttributeValue($code, $value)
    {
        $isValid = true;

        if (!is_numeric($value) && empty($value)) {
            $isValid = false;
        }

        if (!isset($this->attributeValues[$code])) {
            $isValid = false;
        }

        if (is_array($value)) {
            $isValid = false;
        }

        return $isValid;
    }

    /**
     * Collect multiselect values based on value
     *
     * @param \Magento\Catalog\Model\Product $item
     * @param string $attrCode
     * @return array
     */
    protected function getMultiselectValues($item, $attrCode)
    {
        $attrValue = $item->getData($attrCode);
        $optionIds = explode(',', $attrValue);
        $options = array_intersect_key(
            $this->attributeValues[$attrCode],
            array_flip($optionIds)
        );

        return $options;
    }

    /**
     * Returns attributes all values in label-value or value-value pairs form. Labels are lower-cased.
     *
     * @param \Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute
     * @return array
     */
    public function getAttributeOptions(\Magento\Eav\Model\Entity\Attribute\AbstractAttribute $attribute)
    {
        $options = [];

        if ($attribute->usesSource()) {
            try {
                foreach ($attribute->getSource()->getAllOptions(false) as $option) {
                    foreach (is_array($option['value']) ? $option['value'] : [$option] as $innerOption) {
                        if (strlen($innerOption['value'])) {
                            $options[$innerOption['value']] = (string)$innerOption['label'];
                        }
                    }
                }
                // phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
            } catch (\Exception $e) {
                // ignore exceptions connected with source models
            }
        }

        return $options;
    }
}

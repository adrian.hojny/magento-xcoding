<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Interface ParserInterface
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
interface ParserInterface
{
    /**
     * Parse field data.
     *
     * @param ProductInterface $product
     * @param string $field
     * @return mixed
     */
    public function getData(ProductInterface $product, $field);
}

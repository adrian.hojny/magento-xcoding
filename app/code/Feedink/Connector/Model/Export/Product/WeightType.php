<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Directory\Helper\Data;

/**
 * Class WeightType
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class WeightType implements ParserInterface
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var array
     */
    private $weightUnit;

    /**
     * WeightType constructor.
     *
     * @param Data $helper
     */
    public function __construct(
        Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @param ProductInterface $product
     * @param string $field
     * @return mixed
     */
    public function getData(ProductInterface $product, $field)
    {
        if (!isset($this->weightUnit[$product->getStoreId()])) {
            $this->weightUnit[$product->getStoreId()] = $this->helper->getWeightUnit();
        }

        return $this->weightUnit[$product->getStoreId()];
    }
}

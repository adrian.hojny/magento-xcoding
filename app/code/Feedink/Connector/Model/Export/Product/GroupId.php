<?php

namespace Feedink\Connector\Model\Export\Product;

use Magento\Catalog\Api\Data\ProductInterface;

/**
 * Class GroupId
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class GroupId implements ParserInterface
{
    /**
     * @param ProductInterface $product
     * @param string $field
     * @return string
     */
    public function getData(ProductInterface $product, $field)
    {
        $parentSkus = $product->getData('parent_skus');

        if ($parentSkus) {
            return implode(',', $parentSkus);
        }

        return $product->getHasOptions() ? $product->getSku() : null;
    }
}

<?php

namespace Feedink\Connector\Model\Export;

use Feedink\Connector\Api\Data\ConnectorInterface;
use Feedink\Connector\Api\ProductListInterfaceFactory;
use Feedink\Connector\Model\Export\Product\Parser;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class Handler
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Handler
{
    const PAGE_SIZE = 1000;

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var FilterGroupBuilder
     */
    private $filterGroupBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SearchCriteria
     */
    private $searchCriteria;

    /**
     * @var ProductListInterfaceFactory
     */
    private $productListFactory;

    /**
     * @var ProductSearchResultsInterface
     */
    private $searchResults;

    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var ConnectorInterface
     */
    private $connector;

    /**
     * Handler constructor.
     *
     * @param FilterBuilder $filterBuilder
     * @param FilterGroupBuilder $filterGroupBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductListInterfaceFactory $productListFactory
     * @param Parser $parser
     * @param ConnectorInterface $connector
     */
    public function __construct(
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductListInterfaceFactory $productListFactory,
        Parser $parser,
        ConnectorInterface $connector = null
    ) {
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productListFactory = $productListFactory;
        $this->parser = $parser;
        $this->connector = $connector;
    }

    /**
     * @throws \Exception
     */
    public function init()
    {
        $this->initFilters();
        $this->searchCriteria = $this->searchCriteriaBuilder->create();
        $this->searchCriteria->setPageSize(self::PAGE_SIZE);

        return $this;
    }

    /**
     * @param bool $reset
     * @return ProductSearchResultsInterface
     */
    public function getList($reset = false)
    {
        if ($this->searchResults === null || $reset) {
            $productList = $this->productListFactory->create(['connector' => $this->connector]);
            $this->searchResults = $productList->getList($this->searchCriteria);
        }

        return $this->searchResults;
    }

    /**
     * @param $lastId
     * @return ProductInterface[]
     */
    public function getItems($lastId)
    {
        if ($lastId > 0) {
            $this->addProductIdFilter($lastId);
        }

        return $this->getList(true)->getItems();
    }

    /**
     * Add product id filter
     *
     * @param int $lastId
     * @return $this
     */
    private function addProductIdFilter($lastId)
    {
        $filterGroups = $this->searchCriteria->getFilterGroups();

        foreach ($filterGroups as $filterGroup) {
            $filters = [];
            foreach ($filterGroup->getFilters() as $filter) {
                if ($filter->getField() != 'entity_id') {
                    $filters[] = $filter;
                }
            }

            $filterGroup->setFilters($filters);
        }

        $idFilter[] = $this->filterBuilder
            ->setField('entity_id')
            ->setConditionType('gt')
            ->setValue($lastId)
            ->create();

        $filterGroups[] = $this->filterGroupBuilder->setFilters($idFilter)->create();
        $this->searchCriteria->setFilterGroups($filterGroups);

        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    protected function initFilters()
    {
        $filters = $this->getFilters();
        $this->searchCriteriaBuilder->addFilter(
            'store_id',
            $this->connector->getStore()->getId(),
            'eq'
        );

        foreach ($filters as $attributeId => $values) {
            $arrValues = explode(',', $values);

            if (!is_array($arrValues)) {
                continue;
            }

            $arrValues = array_filter($arrValues, 'strlen');

            if (empty($arrValues)) {
                continue;
            }

            $this->searchCriteriaBuilder->addFilter(
                $attributeId,
                explode(',', $values),
                'in'
            );
        }

        return $this;
    }

    protected function getFilters()
    {
        return [
            'type_id' => $this->connector->getFilterType(),
            'category_id' => $this->connector->getFilterCategory(),
            'attribute_set_id' => $this->connector->getFilterAttributeSet(),
            'stock' => $this->connector->getFilterStock(),
            'visibility' => $this->connector->getFilterVisibility(),
            'status' => $this->connector->getFilterStatus()
        ];
    }

    /**
     * @param ProductInterface $product
     * @return array
     */
    public function parse(ProductInterface $product)
    {
        return $this->parser->parse($product);
    }
}

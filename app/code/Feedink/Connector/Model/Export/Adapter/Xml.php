<?php

namespace Feedink\Connector\Model\Export\Adapter;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\DomDocument\DomDocumentFactory;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\File\WriteInterface;

/**
 * Class Xml
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Xml
{
    /**
     * @var WriteInterface
     */
    private $write;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var DomDocumentFactory
     */
    private $domDocumentFactory;

    /**
     * Xml constructor.
     *
     * @param Filesystem $filesystem
     * @param DomDocumentFactory $domDocumentFactory
     */
    public function __construct(
        Filesystem $filesystem,
        DomDocumentFactory $domDocumentFactory
    ) {
        $this->filesystem = $filesystem;
        $this->domDocumentFactory = $domDocumentFactory;
    }

    /**
     * @return Filesystem\Directory\WriteInterface
     * @throws FileSystemException
     */
    protected function getDirectoryWrite()
    {
        return $this->filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
    }

    /**
     * @param string $destination
     * @return $this
     * @throws FileSystemException
     */
    public function init($destination)
    {
        $this->write = $this->getDirectoryWrite()->openFile($destination, 'w');
        $this->initHeaders();

        return $this;
    }

    /**
     * @return $this
     * @throws FileSystemException
     */
    public function initHeaders()
    {
        $this->write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<offers>\n");

        return $this;
    }

    /**
     * @throws FileSystemException
     */
    public function close()
    {
        $this->write('</offers>');
    }

    /**
     * @param array $data
     * @throws FileSystemException
     */
    public function writeRow($data)
    {
        $this->write($this->toXml($data));
    }

    /**
     * Convert object data into XML string
     *
     * @param array $data
     * @param bool $recursive
     * @param string $parent
     * @return string
     */
    public function toXml($data, $recursive = false, $parent = '')
    {
        $xml = '';

        foreach ($data as $fieldName => $fieldValue) {
            if ($parent == 'attributes') {
                if (is_array($fieldValue) || is_object($fieldValue)) {
                    continue;
                }

                $this->formatValue($fieldValue);
                $xml .= "\n\t\t\t";
                $xml .= "<attribute name=\"{$fieldName}\">$fieldValue</attribute>";
                continue;
            }

            if (is_numeric($fieldName)) {
                $fieldName = $fieldName == 0 ? 'main' : 'additional';
            }

            if (is_object($fieldValue)) {
                continue;
            }

            if (is_array($fieldValue)) {
                $xml .= "\n\t\t";
                $xml .= "<{$fieldName}>";
                $xml .= "\t\t\t";
                $xml .= $this->toXml($fieldValue, true, $fieldName);
                $xml .= "\n\t\t";
                $xml .= "</{$fieldName}>";
            } else {
                $this->formatValue($fieldValue);
                $xml .= "\n\t\t";

                if ($recursive) {
                    $xml .= "\t";
                }

                if ($fieldValue == '') {
                    $xml .= "<{$fieldName}/>";
                } else {
                    $xml .= "<{$fieldName}>{$fieldValue}</{$fieldName}>";
                }
            }
        }

        if (!$recursive) {
            $xml = "\t<offer>" . $xml . "\n\t</offer>\n";
        }

        return $xml;
    }

    /**
     * @param string $data
     * @throws FileSystemException
     */
    public function write(string $data)
    {
        $this->write->write($data);
    }

    /**
     * @param $value
     * @return mixed|string
     */
    protected function formatValue(&$value)
    {
        if ($value != strip_tags($value)) {
            $value = "<![CDATA[{$value}]]>";
        } else {
            $value = str_replace(
                ['&', '"', "'", '<', '>'],
                ['&amp;', '&quot;', '&apos;', '&lt;', '&gt;'],
                $value
            );
        }

        return $value;
    }
}

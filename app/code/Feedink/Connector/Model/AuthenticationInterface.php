<?php

namespace Feedink\Connector\Model;

use Magento\Framework\Exception\AuthenticationException;

/**
 * Interface AuthenticationInterface
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
interface AuthenticationInterface
{
    /**
     * Authenticate user
     *
     * @param string $username
     * @param string $password
     * @return boolean
     * @throws AuthenticationException
     * @throws \Exception
     */
    public function authenticate($username, $password);
}

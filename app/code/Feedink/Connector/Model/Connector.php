<?php

namespace Feedink\Connector\Model;

use Feedink\Connector\Api\Data\ConnectorInterface;
use Feedink\Connector\Helper\Data;
use Feedink\Connector\Model\Export\Adapter\Xml;
use Feedink\Connector\Model\Export\Handler;
use Feedink\Connector\Model\Export\HandlerFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\WriteInterface;
use Magento\Framework\Filesystem\Io\File;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Connector
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Connector extends AbstractModel implements ConnectorInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var File
     */
    protected $file;

    /**
     * @var WriteInterface
     */
    protected $varDirectory;

    /**
     * @var Xml
     */
    protected $writer;

    /**
     * @var DateTimeFactory
     */
    protected $dateFactory;

    /**
     * @var HandlerFactory
     */
    protected $handlerFactory;

    /**
     * @var Handler
     */
    protected $handler;

    /**
     * @var Data
     */
    protected $helperData;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        StoreManagerInterface $storeManager,
        Filesystem $filesystem,
        File $file,
        Xml $writer,
        DateTimeFactory $dateFactory,
        HandlerFactory $handlerFactory,
        Data $helperData,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->storeManager = $storeManager;
        $this->filesystem = $filesystem;
        $this->varDirectory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        $this->file = $file;
        $this->writer = $writer;
        $this->dateFactory = $dateFactory;
        $this->handlerFactory = $handlerFactory;
        $this->helperData = $helperData;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\Connector::class);
    }

    /**
     * @throws \LogicException
     */
    private function beforeGenerate()
    {
        $lock = $this->getLockFile();

        if ($this->varDirectory->isExist($lock)) {
            $stat = $this->varDirectory->stat($lock);

            if ((time() - $stat['mtime']) < 4 * 3600) {
                throw new \LogicException(__(
                    'File for store "%1" already locked by another process',
                    $this->getStore()->getName()
                ));
            }
        }

        $this->varDirectory->touch($lock);
        if ($this->_registry->registry('current_connector')) {
            $this->_registry->unregister('current_connector');
        }

        $this->_registry->register('current_connector', $this);
    }

    /**
     * @return void
     * @throws \LogicException
     */
    public function generate()
    {
        $this->beforeGenerate();
        $this->writer->init($this->getTmpFilePath());
        $lastId = 0;

        while (true) {
            $items = $this->getHandler()->getItems($lastId);
            $xmlContent = '';

            if (empty($items)) {
                if ($lastId === 0) {
                    throw new \LogicException(__(
                        'Skipping store "%1". Products don\'t match the criteria.',
                        $this->getStore()->getName()
                    ));
                }

                break;
            }

            foreach ($items as $product) {
                $xmlContent .= $this->writer->toXml($this->getHandler()->parse($product));
                $lastId = $product->getId();
            }

            $this->writer->write($xmlContent);
        }

        $this->writer->close();
        $this->afterGenerate();
    }

    /**
     * @throws FileSystemException
     */
    protected function afterGenerate()
    {
        $date = $this->dateFactory->create()->gmtDate();
        $this->setGenerationTime($date);

        $this->varDirectory->renameFile($this->getTmpFilePath(), $this->getFinalFilePath());

        $this->unlock();
        $this->_registry->unregister('current_connector');
    }

    /**
     * @return mixed
     */
    protected function getHandler()
    {
        if (!$this->handler) {
            $this->handler = $this->handlerFactory
                ->create(['connector' => $this])
                ->init();
        }

        return $this->handler;
    }

    /**
     * @return array
     */
    public function getProductAttributes()
    {
        $requiredAttr = $this->helperData->getRequiredAttributes();
        $additionalAttr = explode(',', $this->getAdditionalAttribute());

        return array_unique(array_merge($requiredAttr, $additionalAttr));
    }

    /**
     * Retrieve connector id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::CONNECTOR_ID);
    }

    /**
     * Retrieve connector filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->getData(self::FILENAME);
    }

    /**
     * Check whatever connector is active or not
     *
     * @return bool
     */
    public function getIsActive()
    {
        return (bool) $this->getData(self::IS_ACTIVE);
    }

    /**
     * Retrieve connector store ID
     *
     * @return int
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * Retrieve product status filter
     *
     * @return string|null
     */
    public function getFilterStatus()
    {
        return $this->getData(self::FILTER_STATUS);
    }

    /**
     * Retrieve product visibility filter
     *
     * @return string|null
     */
    public function getFilterVisibility()
    {
        return $this->getData(self::FILTER_VISIBILITY);
    }

    /**
     * Retrieve product stock filter
     *
     * @return string|null
     */
    public function getFilterStock()
    {
        return $this->getData(self::FILTER_STOCK);
    }

    /**
     * Retrieve product attribute set filter
     *
     * @return string|null
     */
    public function getFilterAttributeSet()
    {
        return $this->getData(self::FILTER_ATTRIBUTE_SET);
    }

    /**
     * Retrieve category filter
     *
     * @return string|null
     */
    public function getFilterCategory()
    {
        return $this->getData(self::FILTER_CATEGORY);
    }

    /**
     * Retrieve product type filter
     *
     * @return string|null
     */
    public function getFilterType()
    {
        return $this->getData(self::FILTER_TYPE);
    }

    /**
     * Retrieve product additional attribute
     *
     * @return string|null
     */
    public function getAdditionalAttribute()
    {
        return $this->getData(self::ADDITIONAL_ATTRIBUTE);
    }

    /**
     * Retrieve connector creation time
     *
     * @return string
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Retrieve connector generation time
     *
     * @return string
     */
    public function getGenerationTime()
    {
        return $this->getData(self::GENERATION_TIME);
    }

    /**
     * Set connector ID
     *
     * @param int $id
     * @return ConnectorInterface
     */
    public function setId($id)
    {
        return $this->setData(self::CONNECTOR_ID, $id);
    }

    /**
     * Set connector filename
     *
     * @param string $filename
     * @return ConnectorInterface
     */
    public function setFilename($filename)
    {
        return $this->setData(self::FILENAME, $filename);
    }

    /**
     * Set connector is active flag
     *
     * @param int $isActive
     * @return ConnectorInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }

    /**
     * Set connector store ID
     *
     * @param int $storeId
     * @return ConnectorInterface
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * Set product status filter
     *
     * @param string $filterStatus
     * @return ConnectorInterface
     */
    public function setFilterStatus($filterStatus)
    {
        $filterStatus = $this->convertValues($filterStatus);

        return $this->setData(self::FILTER_STATUS, $filterStatus);
    }

    /**
     * Set product visibility filter
     *
     * @param string $filterVisibility
     * @return ConnectorInterface
     */
    public function setFilterVisibility($filterVisibility)
    {
        $filterVisibility = $this->convertValues($filterVisibility);

        return $this->setData(self::FILTER_VISIBILITY, $filterVisibility);
    }

    /**
     * Set product stock filter
     *
     * @param string $filterStock
     * @return ConnectorInterface
     */
    public function setFilterStock($filterStock)
    {
        $filterStock = $this->convertValues($filterStock);

        return $this->setData(self::FILTER_STOCK, $filterStock);
    }

    /**
     * Set product attribute set filter
     *
     * @param string $filterAttributeSet
     * @return ConnectorInterface
     */
    public function setFilterAttributeSet($filterAttributeSet)
    {
        $filterAttributeSet = $this->convertValues($filterAttributeSet);

        return $this->setData(self::FILTER_ATTRIBUTE_SET, $filterAttributeSet);
    }

    /**
     * Set category filter
     *
     * @param string $filterCategory
     * @return ConnectorInterface
     */
    public function setFilterCategory($filterCategory)
    {
        $filterCategory = $this->convertValues($filterCategory);

        return $this->setData(self::FILTER_CATEGORY, $filterCategory);
    }

    /**
     * Set product type filter
     *
     * @param string $filterType
     * @return ConnectorInterface
     */
    public function setFilterType($filterType)
    {
        $filterType = $this->convertValues($filterType);

        return $this->setData(self::FILTER_TYPE, $filterType);
    }

    /**
     * Set product additional attribute
     *
     * @param string $additionalAttribute
     * @return ConnectorInterface
     */
    public function setAdditionalAttribute($additionalAttribute)
    {
        $additionalAttribute = $this->convertValues($additionalAttribute);

        return $this->setData(self::ADDITIONAL_ATTRIBUTE, $additionalAttribute);
    }

    /**
     * Set connector creation time
     *
     * @param string $creationTime
     * @return ConnectorInterface
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * Set connector generation time
     *
     * @param string $generationTime
     * @return ConnectorInterface
     */
    public function setGenerationTime($generationTime)
    {
        return $this->setData(self::GENERATION_TIME, $generationTime);
    }

    /**
     * @param $values
     * @return string|null
     */
    protected function convertValues($values)
    {
        if (is_array($values)) {
            $values = array_filter(array_map('trim', $values), 'strlen');

            if (!empty($values)) {
                return implode(',', $values);
            }
        }

        return null;
    }

    public function beforeSave()
    {
        parent::beforeSave();

        /**
         * Check allow filename
         */
        if (!preg_match('#^[a-zA-Z0-9_\.]+$#', $this->getFilename())) {
            throw new LocalizedException(
                __(
                    'Please use only letters (a-z or A-Z), numbers (0-9) or underscores (_) in the filename.'
                    . ' No spaces or other characters are allowed.'
                )
            );
        }

        if (!preg_match('#\.xml$#', $this->getFilename())) {
            $this->setFilename($this->getFilename() . '.xml');
        }

        if (!$this->getId()) {
            $date = $this->dateFactory->create()->gmtDate();
            $this->setCreationTime($date);
        }

        return $this;
    }

    public function afterDelete()
    {
        parent::afterDelete();
        $this->varDirectory->delete($this->getFinalFilePath());

        return $this;
    }

    /**
     * Retrieve store model instance
     *
     * @return \Magento\Store\Model\Store
     */
    public function getStore()
    {
        $storeId = $this->getStoreId();

        if ($storeId) {
            return $this->storeManager->getStore($storeId);
        }

        return $this->storeManager->getStore();
    }

    /**
     * Retrieve lock file path.
     *
     * @return string
     */
    protected function getLockFile()
    {
        return $this->getTmpPath() . DIRECTORY_SEPARATOR . $this->getId() . '.lock';
    }

    protected function getTmpFilePath()
    {
        return $this->getTmpPath() . DIRECTORY_SEPARATOR . $this->getFilename();
    }

    /**
     * Base path to feed directory (var/feedink)
     *
     * @return string
     */
    public function getBasePath()
    {
        return $this->filesystem
                ->getDirectoryRead(DirectoryList::VAR_DIR)
                ->getAbsolutePath() . 'feedink';
    }

    /**
     * Path for store temporary files
     *
     * @return string
     */
    public function getTmpPath()
    {
        return $this->getBasePath() . DIRECTORY_SEPARATOR . 'tmp';
    }

    /**
     * @return string
     */
    protected function getFinalFilePath()
    {
        return $this->getBasePath() . DIRECTORY_SEPARATOR . $this->getFilename();
    }

    /**
     * Remove lock file from filesystem
     *
     * @return $this
     * @throws FileSystemException
     */
    public function unlock()
    {
        if ($this->getId()) {
            $this->varDirectory->delete($this->getLockFile());
        }

        return $this;
    }
}

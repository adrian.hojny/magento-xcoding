<?php

namespace Feedink\Connector\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Data extends AbstractHelper
{
    /**
     * XML Config paths
     */
    const XML_CONFIG_PATH_ACTIVE = 'feedink_connector/general/active';
    const XML_CONFIG_PATH_CRONJOB = 'feedink_connector/general/cronjob';
    const XML_CONFIG_PATH_BASIC_AUTH_ACTIVE = 'feedink_connector/auth/enabled';
    const XML_CONFIG_PATH_BASIC_AUTH_USERNAME = 'feedink_connector/auth/username';
    const XML_CONFIG_PATH_BASIC_AUTH_PASSWORD_HASH = 'feedink_connector/auth/password';
    const XML_CONFIG_PATH_ATTRIBUTES = 'feedink_connector/attributes';

    /**
     * Determine whether connector is enabled or not.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_CONFIG_PATH_ACTIVE
        );
    }

    /**
     * Check use default magento cronjob
     *
     * @return bool
     */
    public function isCronjobEnabled()
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_CONFIG_PATH_CRONJOB
        );
    }

    /**
     * Determine whether or not basic auth is enabled.
     *
     * @return bool
     */
    public function isBasicAuthEnabled()
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_CONFIG_PATH_BASIC_AUTH_ACTIVE
        );
    }

    /**
     * Retrieve authentication username
     *
     * @return string
     */
    public function getAuthUsername()
    {
        return $this->scopeConfig->getValue(
            self::XML_CONFIG_PATH_BASIC_AUTH_USERNAME
        );
    }

    /**
     * Retrieve authentication password
     *
     * @return string
     */
    public function getAuthPasswordHash()
    {
        return $this->scopeConfig->getValue(
            self::XML_CONFIG_PATH_BASIC_AUTH_PASSWORD_HASH
        );
    }

    /**
     * Retrieve required product attributes
     *
     * @return array
     */
    public function getRequiredAttributes()
    {
        $attributes = (array) $this->scopeConfig->getValue(
            self::XML_CONFIG_PATH_ATTRIBUTES
        );

        return array_keys($attributes);
    }
}

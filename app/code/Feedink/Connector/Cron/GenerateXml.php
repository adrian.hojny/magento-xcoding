<?php

namespace Feedink\Connector\Cron;

use Feedink\Connector\Api\ConnectorRepositoryInterface;
use Feedink\Connector\Helper\Data;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Psr\Log\LoggerInterface;

/**
 * Class GenerateXml
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class GenerateXml
{
    /**
     * @var Data
     */
    private $helperData;

    /**
     * @var ConnectorRepositoryInterface
     */
    private $connectorRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * GenerateXml constructor.
     * @param Data $helperData
     * @param ConnectorRepositoryInterface $connectorRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param LoggerInterface $logger
     */
    public function __construct(
        Data $helperData,
        ConnectorRepositoryInterface $connectorRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        LoggerInterface $logger
    ) {
        $this->helperData = $helperData;
        $this->connectorRepository = $connectorRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->logger = $logger;
    }

    /**
     * Generate xml feed.
     *
     * @return void
     */
    public function execute()
    {
        if (!$this->helperData->isCronjobEnabled()) {
            return;
        }

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('is_active', 1)
            ->create();

        $items = $this->connectorRepository
            ->getList($searchCriteria)
            ->getItems();

        foreach ($items as $connector) {
            try {
                $connector->generate();
                $this->connectorRepository->save($connector);
            } catch (\Throwable $e) {
                $connector->unlock();
                $this->logger->critical($e->getMessage());
            }
        }
    }
}

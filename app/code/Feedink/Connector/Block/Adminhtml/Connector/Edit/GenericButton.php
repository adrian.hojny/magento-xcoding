<?php

namespace Feedink\Connector\Block\Adminhtml\Connector\Edit;

use Feedink\Connector\Api\ConnectorRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class GenericButton
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var ConnectorRepositoryInterface
     */
    protected $connectorRepository;

    /**
     * @param Context $context
     * @param ConnectorRepositoryInterface $connectorRepository
     */
    public function __construct(
        Context $context,
        ConnectorRepositoryInterface $connectorRepository
    ) {
        $this->context = $context;
        $this->connectorRepository = $connectorRepository;
    }

    /**
     * Return Connector by ID
     *
     * @return Feedink\Connector\Model\Connector|null
     */
    public function getConnector()
    {
        $connectorId = $this->context->getRequest()->getParam('connector_id');

        try {
            $connector = $this->connectorRepository->getById($connectorId);
        } catch (LocalizedException $e) {
            return null;
        }

        return $connector;
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}

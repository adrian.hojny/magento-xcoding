<?php
/**
 * Info block
 *
 * @copyright Copyright © 2020 Feedink. All rights reserved.
 * @author    contact@feedink.com
 */

namespace Feedink\Connector\Block\Adminhtml\Connector;

use Magento\Backend\Block\Template;

/**
 * Class Info
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
class Info extends Template
{

}

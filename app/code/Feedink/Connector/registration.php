<?php
/**
 * Registration file
 *
 * @copyright Copyright © 2020 Feedink. All rights reserved.
 * @author    contact@feedink.com
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Feedink_Connector',
    __DIR__
);

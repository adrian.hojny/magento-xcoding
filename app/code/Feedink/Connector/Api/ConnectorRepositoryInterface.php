<?php

namespace Feedink\Connector\Api;

/**
 * Feedink connector CRUD interface.
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
interface ConnectorRepositoryInterface
{
    /**
     * Save connector.
     *
     * @param \Feedink\Connector\Api\Data\ConnectorInterface $connector
     * @return \Feedink\Connector\Api\Data\ConnectorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\ConnectorInterface $connector);

    /**
     * Retrieve connector.
     *
     * @param int $connectorId
     * @return \Feedink\Connector\Api\Data\ConnectorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($connectorId);

    /**
     * Retrieve connector by given Store ID.
     *
     * @param int $storeId
     * @return \Feedink\Connector\Api\Data\ConnectorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getByStoreId($storeId);

    /**
     * Retrieve connectors matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \MFeedink\Connector\Api\Data\ConnectorSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete connector.
     *
     * @param \Feedink\Connector\Api\Data\ConnectorInterface $connector
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\ConnectorInterface $connector);

    /**
     * Delete connector by ID.
     *
     * @param int $connectorId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($connectorId);
}

<?php

namespace Feedink\Connector\Api\Data;

/**
 * Feedink connector interface.
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
interface ConnectorInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const CONNECTOR_ID = 'connector_id';
    const FILENAME = 'filename';
    const IS_ACTIVE = 'is_active';
    const STORE_ID = 'store_id';
    const FILTER_STATUS = 'filter_status';
    const FILTER_VISIBILITY = 'filter_visibility';
    const FILTER_STOCK = 'filter_stock';
    const FILTER_ATTRIBUTE_SET = 'filter_attribute_set';
    const FILTER_CATEGORY = 'filter_category';
    const FILTER_TYPE = 'filter_type';
    const ADDITIONAL_ATTRIBUTE = 'additional_attribute';
    const CREATION_TIME = 'creation_time';
    const GENERATION_TIME = 'generation_time';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename();

    /**
     * Get is active flag
     *
     * @return int|null
     */
    public function getIsActive();

    /**
     * Get store ID
     *
     * @return int|null
     */
    public function getStoreId();

    /**
     * Get filter status
     *
     * @return string|null
     */
    public function getFilterStatus();

    /**
     * Get filter visibility
     *
     * @return string|null
     */
    public function getFilterVisibility();

    /**
     * Get filter stock
     *
     * @return string|null
     */
    public function getFilterStock();

    /**
     * Get filter attribute set
     *
     * @return string|null
     */
    public function getFilterAttributeSet();

    /**
     * Get filter category
     *
     * @return string|null
     */
    public function getFilterCategory();

    /**
     * Get filter type
     *
     * @return string|null
     */
    public function getFilterType();

    /**
     * Get product additional attribute
     *
     * @return string|null
     */
    public function getAdditionalAttribute();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get generation time
     *
     * @return string|null
     */
    public function getGenerationTime();

    /**
     * Set ID
     *
     * @param int $id
     * @return ConnectorInterface
     */
    public function setId($id);

    /**
     * Set filename
     *
     * @param string $filename
     * @return ConnectorInterface
     */
    public function setFilename($filename);

    /**
     * Set is active flag
     *
     * @param int $isActive
     * @return ConnectorInterface
     */
    public function setIsActive($isActive);

    /**
     * Set store ID
     *
     * @param int $storeId
     * @return ConnectorInterface
     */
    public function setStoreId($storeId);

    /**
     * Set filter status
     *
     * @param string $filterStatus
     * @return ConnectorInterface
     */
    public function setFilterStatus($filterStatus);

    /**
     * Set filter visibility
     *
     * @param string $filterVisibility
     * @return ConnectorInterface
     */
    public function setFilterVisibility($filterVisibility);

    /**
     * Set filter stock
     *
     * @param string $filterStock
     * @return ConnectorInterface
     */
    public function setFilterStock($filterStock);

    /**
     * Set filter attribute set
     *
     * @param string $filterAttributeSet
     * @return ConnectorInterface
     */
    public function setFilterAttributeSet($filterAttributeSet);

    /**
     * Set filter category
     *
     * @param string $filterCategory
     * @return ConnectorInterface
     */
    public function setFilterCategory($filterCategory);

    /**
     * Set filter type
     *
     * @param string $filterType
     * @return ConnectorInterface
     */
    public function setFilterType($filterType);

    /**
     * Set product additional attribute
     *
     * @param string $additionalAttribute
     * @return ConnectorInterface
     */
    public function setAdditionalAttribute($additionalAttribute);

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return ConnectorInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set generation time
     *
     * @param string $generationTime
     * @return ConnectorInterface
     */
    public function setGenerationTime($generationTime);
}

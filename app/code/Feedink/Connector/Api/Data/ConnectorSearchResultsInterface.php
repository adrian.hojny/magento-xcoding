<?php

namespace Feedink\Connector\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for connector search results.
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
interface ConnectorSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get connectors list.
     *
     * @return \Feedink\Connector\Api\Data\ConnectorInterface[]
     */
    public function getItems();

    /**
     * Set connectors list.
     *
     * @param \Feedink\Connector\Api\Data\ConnectorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

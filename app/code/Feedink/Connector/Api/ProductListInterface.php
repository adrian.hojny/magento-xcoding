<?php
/**
 * Product List Interface
 *
 * @copyright Copyright © 2020 Feedink. All rights reserved.
 * @author    contact@feedink.com
 */

namespace Feedink\Connector\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Catalog\Api\Data\ProductSearchResultsInterface;

/**
 * Interface ProductListInterface
 *
 * @category   Feedink Sp. z o.o.
 * @author     (contact@feedink.com)
 * @copyright  Copyright (c) 2020 Feedink Sp. z o.o. (https://www.feedink.com/)
 * @license    (https://www.gnu.org/licenses/gpl-3.0.html)
 */
interface ProductListInterface
{
    /**
     * Get product list
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return ProductSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}

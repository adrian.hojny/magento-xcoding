<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Model\AlternateHreflang;

use Magefan\AlternateHreflang\Model\AbstractAlternateHreflang;
use Magefan\AlternateHreflang\Model\Config;
use Magefan\AlternateHreflang\Model\WoomCmsTreeFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\StoreManagerInterface;
use Magefan\AlternateHreflang\Model\ResourceModel\AlternateHreflang\CollectionFactory as AlternateHreflangCollectionFactory;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\Registry;

class CmsPage extends AbstractAlternateHreflang
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var WoomCmsTreeFactory
     */
    private $woomCmsTreeFactory;

    /**
     * Cms constructor.
     * @param RequestInterface $request
     * @param StoreManagerInterface $storeManager
     * @param Config $config
     * @param AlternateHreflangCollectionFactory $alternateHreflangCollectionFactory
     * @param Registry $coreRegistry
     * @param Emulation $emulation
     * @param PageFactory $pageFactory
     * @param WoomCmsTreeFactory|null $woomCmsTreeFactory
     */
    public function __construct(
        RequestInterface $request,
        StoreManagerInterface $storeManager,
        Config $config,
        AlternateHreflangCollectionFactory $alternateHreflangCollectionFactory,
        Registry $coreRegistry,
        Emulation $emulation,
        PageFactory $pageFactory,
        WoomCmsTreeFactory $woomCmsTreeFactory = null
    ) {
        parent::__construct($request, $storeManager, $config, $alternateHreflangCollectionFactory, $coreRegistry, $emulation);
        $this->pageFactory = $pageFactory;
        $this->woomCmsTreeFactory = $woomCmsTreeFactory ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(WoomCmsTreeFactory::class);
    }

    /**
     * @param $id
     * @param $storeId
     * @return string
     */
    public function getObjectUrl($id, $storeId)
    {
        $object = $this->pageFactory->create();

        $object->load($id);

        if ($object->getId() && $object->getData('is_active')) {
            $woomCmsTree = $this->woomCmsTreeFactory->getTreeByPageId($id);
            if ($woomCmsTree) {
                return $woomCmsTree->getUrl($storeId);
            } else {
                return $this->storeManager->getStore($storeId)->getBaseUrl() . $object->getIdentifier();
            }
        }

        return false;
    }

    /**
     * @return string
     */
    protected function getObjectType()
    {
        return Config::PAGE_TITLE_CMS;
    }

    /**
     * @param $currentObject
     * @return array
     */
    public function getAlternateUrls($currentObject)
    {
        $urls = parent::getAlternateUrls($currentObject);
        foreach ($this->storeManager->getStores() as $store) {
            $storeId = $store->getId();
            if (!$this->isAvailableStoreGroup($this->storeManager->getStore($storeId))) {
                continue;
            }

            $languageCode = $this->config->getLocaleCode($storeId);
            if (empty($urls[$languageCode])) {
                if (in_array(0, $currentObject->getStores())
                    || in_array($storeId, $currentObject->getStores())) {
                    $url = $this->getObjectUrl($currentObject->getId(), $storeId);
                    if ($url) {
                        $urls[$languageCode] = $url;
                    }
                }
            }
        }
        return $urls;
    }
}

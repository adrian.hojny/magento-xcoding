<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Model;

use Magefan\AlternateHreflang\Api\GetAlternateHreflangInterface;
use Magefan\AlternateHreflang\Model\AlternateHreflang\Category as CategoryModel;
use Magefan\AlternateHreflang\Model\AlternateHreflang\CmsPage as CmsPageModel;
use Magefan\AlternateHreflang\Model\AlternateHreflang\CmsIndex as CmsIndexModel;
use Magefan\AlternateHreflang\Model\AlternateHreflang\Product as ProductModel;
use Magento\Framework\Exception\NoSuchEntityException;

class GetAlternateHreflang implements GetAlternateHreflangInterface
{
    /**
     * @var CategoryModel
     */
    private $categoryModel;

    /**
     * @var CmsPageModel
     */
    private $cmsPageModel;

    /**
     * @var ProductModel
     */
    private $productModel;

    /**
     * @var CmsIndexModel
     */
    private $cmsIndexModel;

    /**
     * GetAlternateHreflang constructor.
     * @param CategoryModel $categoryModel
     * @param CmsPageModel $cmsPageModel
     * @param ProductModel $productModel
     * @param CmsIndexModel $cmsIndexModel
     */
    public function __construct(
        CategoryModel $categoryModel,
        CmsPageModel $cmsPageModel,
        ProductModel $productModel,
        CmsIndexModel $cmsIndexModel
    ) {
        $this->categoryModel = $categoryModel;
        $this->cmsPageModel = $cmsPageModel;
        $this->productModel = $productModel;
        $this->cmsIndexModel = $cmsIndexModel;
    }

    /**
     * @param $object
     * @param $type
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function execute($object, $type)
    {
        switch ($type) {
            case Config::PAGE_TITLE_CATEGORY:
                return $this->categoryModel->getAlternateUrls($object);
            case Config::PAGE_TITLE_PRODUCT:
                return $this->productModel->getAlternateUrls($object);
            case Config::PAGE_TITLE_CMS:
                return $this->cmsPageModel->getAlternateUrls($object);
            case Config::PAGE_TITLE_HOMEPAGE:
                return $this->cmsIndexModel->getAlternateUrls($object);
            default:
                throw new NoSuchEntityException(
                    __("The page type that was requested doesn't exist.")
                );
        }
    }
}

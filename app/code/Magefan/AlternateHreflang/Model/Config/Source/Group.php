<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Config group options
 */
class Group implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $group = [];

    /**
     * @return array
     */
    public function toOptionArray()
    {
        if (!$this->group) {
            for ($i = 1; $i <= 100; $i++) {
                $this->group[] = ['value' => $i, 'label' => 'Group ' . $i];
            }
        }

        return $this->group;
    }
}

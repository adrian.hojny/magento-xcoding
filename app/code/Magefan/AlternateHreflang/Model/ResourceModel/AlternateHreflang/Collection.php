<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */
namespace Magefan\AlternateHreflang\Model\ResourceModel\AlternateHreflang;

/**
 * Class Alternate Hreflang Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Magefan\AlternateHreflang\Model\AlternateHreflang::class,
            \Magefan\AlternateHreflang\Model\ResourceModel\AlternateHreflang::class
        );
    }
}

<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */
namespace Magefan\AlternateHreflang\Model\ResourceModel;

/**
 * Class Alternate Hreflang ResourceModel
 */
class AlternateHreflang extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Serializable field: localization
     *
     * @var array
     */
    protected $_serializableFields = ['localization' => [null, []]];

    /**
     * AlternateHreflang constructor
     */
    protected function _construct()
    {
        $this->_init('magefan_alternate_hreflang', 'id');
    }
}

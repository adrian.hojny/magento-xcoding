<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Model;

use Magento\Cms\Helper\Page;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Locale\ResolverInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Request\Http;

/**
 * Class provices methods to get module configuration properties
 */
class Config
{
    /**
     * Path to extension status
     */
    const EXTENSION_ENABLED = 'alternatehreflang/general/enabled';

    /**
     * Path to hreflang tags status
     */
    const DISPLAY_HREFLANG_TAGS_FOR = 'alternatehreflang/general/use_hreflang_tag_for';

    /**
     * Path to locale code
     */
    const LOCALE_CODE = 'general/locale/code';

    /**
     * Path to product use categories
     */
    const PRODUCT_USE_CATEGORIES = 'catalog/seo/product_use_categories';

    /**
     * Path to locale depends on region status
     */
    const LOCALE_DEPENDS_ON_REGION = 'alternatehreflang/locale_options/depends_on_region';

    /**
     * Path to store group options
     */
    const STORE_GROUP = 'alternatehreflang/locale_options/group';

    /**
     * Path to x-default value store id
     */
    const XDEFAULT_STORE = 'alternatehreflang/locale_options/xdefault_store';

    /**
     * Path to add store code to urls status
     */
    const STORE_CODE_ENABLED = 'web/url/use_store';

    /**
     * Path to add custom locale code
     */
    const CUSTOM_LOCALE_CODE = 'alternatehreflang/locale_options/locale';

    /**
     * Get hreflang tags
     */
    const PAGE_TITLE_NONE = 'none';
    const PAGE_TITLE_HOMEPAGE = 'homepage';
    const PAGE_TITLE_PRODUCT = 'product';
    const PAGE_TITLE_CATEGORY = 'category';
    const PAGE_TITLE_CMS = 'cms';


    /**
     * Page type
     */
    const STATIC_PAGE_TYPE = 3;
    const CATALOG_PRODUCT_TYPE = 4;
    const CATALOG_CATEGORY_TYPE = 5;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ResolverInterface
     */
    protected $localeResolver;

    /**
     * @var Http
     */
    protected $request;

    /**
     * Config constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ResolverInterface $localeResolver
     * @param Http $request
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ResolverInterface $localeResolver,
        Http $request
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->localeResolver = $localeResolver;
        $this->request = $request;
    }

    /**
     * @param $type
     * @return int
     */
    public function getPageTypeId($type)
    {
        switch ($type) {
            case self::PAGE_TITLE_CMS:
                return self::STATIC_PAGE_TYPE;
            case self::PAGE_TITLE_PRODUCT:
                return self::CATALOG_PRODUCT_TYPE;
            case self::PAGE_TITLE_CATEGORY:
                return self::CATALOG_CATEGORY_TYPE;
        }
    }

    /**
     * Retrieve true if module is enabled
     *
     * @param null $storeId
     * @return bool
     */
    public function isEnabled($storeId = null)
    {
        return (bool)$this->getConfigValue(self::EXTENSION_ENABLED, $storeId);
    }


    /**
     * @param $pageType
     * @param null $storeId
     * @return bool
     */
    public function getDisplayHreflangTagsFor($pageType, $storeId = null)
    {
        $displayFor = explode(',', $this->getConfigValue(self::DISPLAY_HREFLANG_TAGS_FOR, $storeId));

        return in_array(self::PAGE_TITLE_NONE, $displayFor) ? false : in_array($pageType, $displayFor);
    }

    /**
     * Retrieve locale code
     *
     * @param null $storeId
     * @return mixed
     */
    public function getLocaleCode($storeId = null)
    {
        $languageCode = $this->getConfigValue(self::LOCALE_CODE, $storeId);

        if (!$this->isLocaleDependsOnRegion($storeId)) {
            $languageInfo = explode('_', $languageCode);
            $languageCode = $languageInfo[0];
        } elseif ($customLocaleCode = $this->getCustomLocaleCode($storeId)) {
            $languageCode = $customLocaleCode;
        }

        $languageCode = str_replace('_', '-', $languageCode);

        return $languageCode;
    }

    /**
     * Retrieve true if product include a category in URL
     *
     * @param null $storeId
     * @return bool
     */
    public function isProductUseCategoriesPath($storeId = null)
    {
        return (bool)$this->getConfigValue(self::PRODUCT_USE_CATEGORIES, $storeId);
    }

    /**
     * @param $storeId
     * @return bool
     */
    public function isLocaleDependsOnRegion($storeId)
    {
        return $this->getConfigValue(self::LOCALE_DEPENDS_ON_REGION, $storeId);
    }

    /**
     * @return string
     * @deprecated Add $pageType property to class
     */
    public function getPageType()
    {
        switch ($this->request->getFullActionName()) {
            case 'cms_index_index':
                return self::PAGE_TITLE_HOMEPAGE;
            case 'catalog_product_view':
                return self::PAGE_TITLE_PRODUCT;
            case 'catalog_category_view':
                return self::PAGE_TITLE_CATEGORY;
            case 'cms_page_view':
                return self::PAGE_TITLE_CMS;
        }
    }

    /**
     * Retrieve store group
     *
     * @param $storeId
     * @return int
     */
    public function getGroup($storeId)
    {
        return $this->getConfigValue(self::STORE_GROUP, $storeId);
    }

    /**
     * Retrieve x-default store id
     *
     * @param $storeId
     * @return int
     */
    public function getXDefaultStoreId($storeId = null)
    {
        return (int)$this->getConfigValue(self::XDEFAULT_STORE, $storeId);
    }

    /**
     * @return bool
     */
    public function isStoreCodeInUrlEnabled()
    {
        return (bool)$this->getConfigValue(self::STORE_CODE_ENABLED);
    }

    /**
     * @return string
     */
    public function getCmsIndex()
    {
        return (string)$this->getConfigValue(Page::XML_PATH_HOME_PAGE);
    }

    /**
     * @param $storeId
     *
     * @return string
     */
    public function getCustomLocaleCode($storeId)
    {
        return (string)$this->getConfigValue(self::CUSTOM_LOCALE_CODE, $storeId);
    }

    /**
     * @param $path
     * @param null $storeId
     * @return mixed
     */
    public function getConfigValue($path, $storeId = null)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $storeId);
    }
}

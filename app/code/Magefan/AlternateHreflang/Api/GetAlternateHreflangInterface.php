<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Api;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Retrieve Alternate Hreflang URLs for object
 * by it's type, e.g. homepage, product, category, cms
 *
 * @api
 * @since 2.1.0
 */
interface GetAlternateHreflangInterface
{
    /**
     * @param mixed $object
     * @param string $type
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function execute($object, $type);
}

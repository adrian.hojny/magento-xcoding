<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Plugin\Magento\UrlRewrite\Model\StoreSwitcher;

use Magefan\AlternateHreflang\Model\Switchers;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\HTTP\PhpEnvironment\RequestFactory;
use Magento\UrlRewrite\Model\StoreSwitcher\RewriteUrl;
use Magefan\AlternateHreflang\Model\Config;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Detect correct store switch redirect for CMS page
 */
class RewriteUrlPlugin
{
    /**
     * @var RequestFactory
     */
    protected $requestFactory;

    /**
     * @var Switchers
     */
    protected $switchers;

    /**
     * @var PageRepositoryInterface
     */
    protected $pageRepository;

    /**
     * @var Config
     */
    protected $config;

    /**
     * RewriteUrlPlugin constructor.
     * @param RequestFactory $requestFactory
     * @param Switchers $switchers
     * @param PageRepositoryInterface $pageRepository
     * @param Config $config
     */
    public function __construct(
        RequestFactory $requestFactory,
        Switchers $switchers,
        PageRepositoryInterface $pageRepository,
        Config $config
    ) {
        $this->requestFactory = $requestFactory;
        $this->switchers = $switchers;
        $this->pageRepository = $pageRepository;
        $this->config = $config;
    }

    /**
     * @param RewriteUrl $subject
     * @param $result
     * @param $fromStore
     * @param $targetStore
     * @param $redirectUrl
     * @return string
     */
    public function afterSwitch(RewriteUrl $subject, $result, $fromStore, $targetStore, $redirectUrl)
    {
        if (!$this->config->isEnabled()) {
            return $result;
        }

        if ($result != $redirectUrl && $result == $targetStore->getBaseUrl()) {

            $targetUrl = $redirectUrl;
            $request = $this->requestFactory->create(['uri' => $targetUrl]);
            $identifier = ltrim($request->getPathInfo(), '/');

            $storePath = $targetStore->getCode() . '/';
            if (0 === strpos($identifier,  $storePath)) {
                $identifier = substr($identifier, strlen($storePath));
            }

            $storeId = $targetStore->getId();

            $switchers = $this->switchers->getSwitchers($identifier, Switchers::CMS);
            $redirectId = null;

            foreach ($switchers as $switcher) {
                if (!empty($switcher['localization'])) {
                    $localization = json_decode($switcher['localization'], true);
                } else {
                    $localization = [];
                }

                if (!empty($localization[$storeId])) {
                    $redirectId = $localization[$storeId];
                }
            }

            if ($redirectId) {
                try {
                    $page = $this->pageRepository->getById($redirectId);
                    $result =  $targetStore->getBaseUrl() . $page->getIdentifier();
                } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                    $error = $e;
                }
            }
        }

        return $result;
    }
}

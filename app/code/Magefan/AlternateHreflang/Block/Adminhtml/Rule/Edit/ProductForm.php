<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Block\Adminhtml\Rule\Edit;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Ui\Component\Form\FieldFactory;
use Magento\Store\Model\StoreManagerFactory;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magefan\AlternateHreflang\Api\AlternateHreflangRepositoryInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Model\ProductFactory;

class ProductForm extends AbstractModifier
{
    /**
     * @var StoreManagerFactory
     */
    private $_storeManager;

    /**
     * @var array
     */
    private $storeGroup = [];

    /**
     * @var FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var AlternateHreflangRepositoryInterface
     */
    private $alternateHreflangRepository;

    /**
     * @var Http
     */
    private $request;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var FieldFactory
     */
    private $fieldFactory;

    /**
     * ProductForm constructor.
     * @param ContextInterface $context
     * @param FieldFactory $fieldFactory
     * @param StoreManagerFactory $storeManager
     * @param FilterBuilder $filterBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param AlternateHreflangRepositoryInterface $alternateHreflangRepository
     * @param Http $request
     * @param ScopeConfigInterface $scopeConfig
     * @param ProductFactory $productFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        FieldFactory $fieldFactory,
        StoreManagerFactory $storeManager,
        FilterBuilder $filterBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        AlternateHreflangRepositoryInterface $alternateHreflangRepository,
        Http $request,
        ScopeConfigInterface $scopeConfig,
        ProductFactory $productFactory,
        array $components = [],
        array $data = []
    ) {
        $this->fieldFactory = $fieldFactory;
        $this->filterBuilder = $filterBuilder;
        $this->_storeManager = $storeManager;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->alternateHreflangRepository = $alternateHreflangRepository;
        $this->request = $request;
        $this->scopeConfig = $scopeConfig;
        $this->productFactory = $productFactory;
    }

    /**
     * @param array $meta
     * @return array|null
     */
    public function modifyMeta(array $meta)
    {
        $id = $this->request->getParam('id');
        $storeManager = $this->_storeManager->create();
        $model = $this->productFactory->create()->load($id);
        $filter = $this->filterBuilder
            ->setField('parent_id')
            ->setValue($id)
            ->create();
        $filter1 = $this->filterBuilder
            ->setField('url_key')
            ->setValue($model->getIdentifier() ?: $model->getUrlKey())
            ->create();
        $filter2 = $this->filterBuilder
            ->setField('type')
            ->setValue(\Magefan\AlternateHreflang\Model\Config::CATALOG_PRODUCT_TYPE)
            ->create();
        $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter, $filter1, $filter2])->create();
        $localization = [];
        try {
            $switcherId = $this->alternateHreflangRepository->getList($searchCriteria)->getItems()[0]['id'] ?? null;
            if ($switcherId) {
                $localization = $this->alternateHreflangRepository->getById($switcherId)->getLocalization();
            }
        } catch (\Exception $e) {
            $e->getMessage();
        }

        $notice = 'Please define alternatives for product ONLY when you have multi-website Magento instance and use '
            . 'different product IDs (Entity IDs) for each website (duplicate product for each website). '
            . 'Otherwise alternate hreflang tags will be defined automatically.';

        if (!$this->storeGroup) {
            foreach ($storeManager->getWebsites() as $website) {
                foreach ($website->getGroups() as $group) {
                    $stores = $group->getStores();
                    if (count($stores) == 0) {
                        continue;
                    }
                    foreach ($stores as $store) {
                      $this->storeGroup[$store->getId()]['label'] =
                            $store->getName() . ' (' . $website->getName() . '/' . $group->getName() .')';
                       foreach ($localization as $k => $field) {
                           $object = $this->productFactory->create()->load($field);
                           $title = $object->getTitle() ?: $object->getName();
                           $this->storeGroup[$k]['value'] = $field . ". " . $title;
                        }
                    }
                }
            }
        }

        $child = [];
        foreach ($this->storeGroup as $k => $fieldConfig) {
            $name = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => __($fieldConfig['label']),
                            'componentType' => Field::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' =>  $k,
                            'dataType' => Text::NAME,
                            'sortOrder' => $k,
                            'notice' => $notice,
                            'value' => array_key_exists('value', $fieldConfig) ? $fieldConfig['value']: '',
                            'disabled'=> false
                        ],
                    ],
                ],
            ];
            array_push($child, $name);
        }

        $meta = array_replace_recursive(
            $meta,
            [
                'localization' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Alternative Store View Localizations '),
                                'collapsible' => true,
                                'componentType' => Fieldset::NAME,
                                'dataScope' => 'data.localization',
                                'sortOrder' => 51
                            ],
                        ],
                    ],
                    'children' => $child
                ],
            ]
        );

        return $meta;
    }

    public function modifyData(array $data)
    {
        return $data;
    }


}
<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;
use Magefan\AlternateHreflang\Model\Config\Source\Group as SourceGroup;

/**
 * Class for alternate group
 */
class Group extends Select
{
    /**
     * @var array
     */
    protected $group = [];

    /**
     * @var SourceGroup
     */
    protected $sourceGroup;

    /**
     * Group constructor.
     * @param Context $context
     * @param SourceGroup $sourceGroup
     * @param array $data
     */
    public function __construct(
        Context $context,
        SourceGroup $sourceGroup,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->sourceGroup = $sourceGroup;
    }

    /**
     * @return array
     */
    public function getGroupData()
    {
        if (!$this->group) {
            $this->group = $this->sourceGroup->toOptionArray();
        }

        return $this->group;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * @return string
     */
    public function toHtml()
    {
        if (!$this->getOptions()) {
            foreach ($this->getGroupData() as $group) {
                $this->addOption($group['value'], addslashes($group['label']));
            }
        }

        return parent::toHtml();
    }
}

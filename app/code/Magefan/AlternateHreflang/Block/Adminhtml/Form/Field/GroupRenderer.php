<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflang\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\DataObject;

/**
 * Class Group Renderer block
 */
class GroupRenderer extends AbstractFieldArray
{
    /**
     * @var \Magento\Framework\View\Element\BlockInterface
     */
    protected $groupRenderer;

    /**
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getGroupRenderer()
    {
        if (!$this->groupRenderer) {
            $this->groupRenderer = $this->createBlock(
                \Magefan\AlternateHreflang\Block\Adminhtml\Form\Field\Group::class
            );
            $this->groupRenderer->setClass('group_field_select');
        }
        return $this->groupRenderer;
    }

    /**
     * @param $object
     * @return \Magento\Framework\View\Element\BlockInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function createBlock($object)
    {
        return $this->getLayout()->createBlock($object, '', ['data' => ['is_render_to_js_template' => true]]);
    }

    /**
     * Prepare to render
     *
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareToRender()
    {
        $this->addColumn('group', ['label' => __('Group'), 'renderer' => $this->getGroupRenderer()]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add');
    }

    /**
     * @param AbstractElement $element
     * @return bool
     */
    protected function _isInheritCheckboxRequired(AbstractElement $element)
    {
        return false;
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row)
    {
        $optionExtraAttr = [];
        $optionExtraAttr['option_' . $this->getGroupRenderer()
            ->calcOptionHash($row->getData('group'))] = 'selected="selected"';

        $row->setData('option_extra_attrs', $optionExtraAttr);
    }
}

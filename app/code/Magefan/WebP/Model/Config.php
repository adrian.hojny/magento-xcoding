<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

declare(strict_types = 1);

namespace Magefan\WebP\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{

    const XML_PATH_EXTENSION_ENABLED                    = 'mfwebp/general/enabled';
    const XML_PATH_EXTENSION_IMAGE_QUALITY              = 'mfwebp/general/quality';
    const XML_PATH_EXTENSION_MAGEFAN_CONVERSION_SERVICE = 'mfwebp/general/magefan_conversion_service';
    const XML_PATH_EXTENSION_SKIP_MEDIA_FOLDERS         = 'mfwebp/general/skip_media_folders';
    const XML_PATH_EXTENSION_SKIP_STATIC_FOLDERS        = 'mfwebp/general/skip_static_folders';
    const XML_PATH_EXTENSION_WHEN_GENERATE_WEBP         = 'mfwebp/general/creation_options';
    const XML_PATH_EXTENSION_CONVERT_EXISTS_PICTURE     = 'mfwebp/advanced_settings/convert_exists_picture';
    const XML_PATH_EXTENSION_CONVERT_BACKGROUND_IMAGES  = 'mfwebp/advanced_settings/convert_background_images';
    const XML_PATH_EXTENSION_CONVERT_IMAGE_IN_GRAPHQL   = 'mfwebp/advanced_settings/convert_images_in_graphql_requests';
    const XML_PATH_EXTENSION_CONVERT_IMAGE_IN_REST_API  = 'mfwebp/advanced_settings/convert_images_in_rest_api_requests';
    const XML_PATH_EXTENSION_EXTERNAL_WEBSITES          = 'mfwebp/advanced_settings/external_websites';

    const PICTURE_WITH_MFWEBP_CLASS                 = '<picture class="mfwebp">';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var bool
     */
    private $browserWebPCompatible;

    /**
     * @var array
     */
    private $skipFolders;

    /**
     * @var array
     */
    private $externalWebsites;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param RequestInterface $request
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        RequestInterface $request = null
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->request = $request ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\App\RequestInterface::class);
    }

    /**
     * Retrieve true if webp module is enabled
     * @param null $storeId
     * @return bool
     */
    public function isEnabled(?int $storeId = null): bool
    {
        return (bool) $this->getConfig(
            self::XML_PATH_EXTENSION_ENABLED,
            $storeId
        );
    }

    /**
     * Retrieve store config value
     * @param string $path
     * @param null $storeId
     * @return mixed
     */
    private function getConfig(string $path, ?string $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $path,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Return quality for convert
     * @param null $storeId
     * @return int
     */
    public function getQuality(?int $storeId = null): int
    {
        return (int) $this->getConfig(
            self::XML_PATH_EXTENSION_IMAGE_QUALITY,
            $storeId
        );
    }

    /**
     * Return quality for convert
     * @param null $storeId
     * @return int
     */
    public function useMagefanConversionService(?int $storeId = null): int
    {
        return (int) $this->getConfig(
            self::XML_PATH_EXTENSION_MAGEFAN_CONVERSION_SERVICE,
            $storeId
        );
    }

    /**
     * Return list of folders that shouldn't be converted
     * @param null $storeId
     * @return array
     */
    public function getSkipFolders(string $folderType, ?int $storeId = null): array
    {
        if (null === $this->skipFolders) {
            $skipFolders = (string) $this->getConfig($folderType, $storeId);
            $skipFolders = str_replace("\r", "\n", trim($skipFolders));
            $this->skipFolders = explode("\n", $skipFolders);
            foreach ($this->skipFolders as $k => $v) {
                $v = trim($v);
                if ($v) {
                    $this->skipFolders[$k] = $v;
                } else {
                    unset($this->skipFolders[$k]);
                }
            }

        }


        return $this->skipFolders;
    }

    /**
     * Return list of External Websites, images from that shouldn't be converted
     * @param null $storeId
     * @return array
     */
    public function getExternalWebsites(?int $storeId = null): array
    {
        if (null === $this->externalWebsites) {
            $skipFolders = (string) $this->getConfig(self::XML_PATH_EXTENSION_EXTERNAL_WEBSITES);
            $skipFolders = str_replace("\r", "\n", trim($skipFolders));
            $this->externalWebsites = explode("\n", $skipFolders);
            foreach ($this->externalWebsites as $k => $v) {
                $v = trim($v);
                if ($v) {
                    $this->externalWebsites[$k] = $v;
                } else {
                    unset($this->externalWebsites[$k]);
                }
            }
        }

        return $this->externalWebsites;
    }

    /**
     * @return mixed
     */
    public function getGenerationOption()
    {
        return $this->getConfig(self::XML_PATH_EXTENSION_WHEN_GENERATE_WEBP);
    }

    /**
     * Retrieve true if option is enabled
     * @param null $storeId
     * @return bool
     */
    public function convertExistingPictureTag(?int $storeId = null): bool
    {
        return (bool) $this->getConfig(
            self::XML_PATH_EXTENSION_CONVERT_EXISTS_PICTURE,
            $storeId
        );
    }

    /**
     * Retrieve true if option is enabled
     * @param null $storeId
     * @return bool
     */
    public function convertBackgroundImages(?int $storeId = null): bool
    {
        return (bool) $this->getConfig(
            self::XML_PATH_EXTENSION_CONVERT_BACKGROUND_IMAGES,
            $storeId
        );
    }

    /**
     * Retrieve true if browser is compatible with WebP
     * @return bool
     */
    public function isBrowserWebPCompatible():bool
    {
        if (null === $this->browserWebPCompatible) {
            $this->browserWebPCompatible = (bool)preg_match(
                '/(Edge|Firefox|Chrome|Opera)/i',
                $this->request->getServer('HTTP_USER_AGENT')
            );
        }

        return $this->browserWebPCompatible;
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isConvertImagesInGraphQLRequests(?int $storeId = null): bool
    {
        return (bool) $this->getConfig(
            self::XML_PATH_EXTENSION_CONVERT_IMAGE_IN_GRAPHQL,
            $storeId
        );
    }

    /**
     * @param int|null $storeId
     * @return bool
     */
    public function isConvertImagesInRestApiRequests(?int $storeId = null): bool
    {
        return (bool) $this->getConfig(
            self::XML_PATH_EXTENSION_CONVERT_IMAGE_IN_REST_API,
            $storeId
        );
    }
}

<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

declare(strict_types=1);

namespace Magefan\WebP\Plugin\Magento\Framework\GraphQl\Query;

/*
Comment for Magento 2.2.x as it does not has GraphQl
use Magento\Framework\GraphQl\Query\QueryProcessor;
use Magento\Framework\GraphQl\Query\Resolver\ContextInterface;
use Magento\Framework\GraphQl\Schema;
*/
use Magefan\WebP\Api\HtmlParserInterface;
use Magefan\WebP\Api\GetWebPUrlInterface;
use Magefan\WebP\Api\CreateWebPImageInterface;
use Magefan\WebP\Model\Config;

/**
 * Class QueryProcessorPlugin (only for graphql)
 */
class QueryProcessorPlugin
{
    /**
     * @var HtmlParserInterface
     */
    private $htmlParser;

    /**
     * @var GetWebPUrlInterface
     */
    private $webPUrl;

    /**
     * @var CreateWebPImageInterface
     */
    private $createWebPImage;

    /**
     * @var Config
     */
    private $config;

    /**
     * QueryProcessorPlugin constructor.
     * @param HtmlParserInterface $htmlParser
     * @param GetWebPUrlInterface $webPUrl
     * @param CreateWebPImageInterface $createWebPImage
     * @param Config $config
     */
    public function __construct(
        HtmlParserInterface $htmlParser,
        GetWebPUrlInterface $webPUrl,
        CreateWebPImageInterface $createWebPImage,
        Config $config
    ) {
        $this->htmlParser = $htmlParser;
        $this->webPUrl = $webPUrl;
        $this->createWebPImage = $createWebPImage;
        $this->config = $config;
    }

    /**
     * @param $subject
     * @param $result
     * @param $schema
     * @param string $source
     * @param null $contextValue
     * @param array|null $variableValues
     * @param string|null $operationName
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterProcess(
        /*QueryProcessor*/ $subject,
        $result,
        /*Schema*/ $schema,
        string $source,
        /*ContextInterface*/ $contextValue = null,
        array $variableValues = null,
        string $operationName = null
    ) : array {
        if (!$this->config->isEnabled() || !$this->config->isConvertImagesInGraphQLRequests() || !$this->config->isBrowserWebPCompatible()) {
            return $result;
        }

        return $this->replaceLinks($result);
    }

    /**
     * @param $result
     * @return array
     * @throws \Magento\Framework\Exception\FileSystemException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    private function replaceLinks($result) : array
    {
        if (is_array($result)) {
            foreach ($result as $key => $value) {
                if (is_array($value)) {
                    $value = $this->replaceLinks($value);
                } elseif (is_string($value)) {
                    if (strpos((string)$value, '<img')) {
                        $value = $this->htmlParser->execute($value);
                    } else {
                        $allowedImageFormats = ['jpg', 'png', 'gif', 'jpeg'];
                        $ext = substr((string)strrchr($value, '.'), 1);
                        if (in_array($ext, $allowedImageFormats)) {
                            if ($this->createWebPImage->execute($value)) {
                                $value = $this->webPUrl->execute($value);
                            }
                        }
                    }
                }
                $result[$key] = $value;
            }
        }

        return $result;
    }
}

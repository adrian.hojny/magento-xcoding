<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

declare(strict_types=1);

namespace Magefan\AlternateHreflangGraphQl\Model\Resolver\DataProvider;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Cms\Api\PageRepositoryInterface;
use Magefan\AlternateHreflang\Model\Config;
use Magento\Framework\Exception\NoSuchEntityException;
use Magefan\AlternateHreflang\Api\GetAlternateHreflangInterface;

class AlternateHreflang
{


    /**
     * @var ProductRepositoryInterface
     */
    private $productRepositoryInterface;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepositoryInterface;

    /**
     * @var PageRepositoryInterface
     */
    private $pageRepositoryInterface;

    /**
     * @var GetAlternateHreflangInterface
     */
    private $getAlternateHreflang;

    /**
     * AlternateHreflang constructor.
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param CategoryRepositoryInterface $categoryRepositoryInterface
     * @param PageRepositoryInterface $pageRepositoryInterface
     * @param GetAlternateHreflangInterface $getAlternateHreflang
     */
    public function __construct(
        ProductRepositoryInterface $productRepositoryInterface,
        CategoryRepositoryInterface $categoryRepositoryInterface,
        PageRepositoryInterface $pageRepositoryInterface,
        GetAlternateHreflangInterface $getAlternateHreflang
    ) {
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->categoryRepositoryInterface = $categoryRepositoryInterface;
        $this->pageRepositoryInterface = $pageRepositoryInterface;
        $this->getAlternateHreflang = $getAlternateHreflang;
    }

    /**
     * @param string $id
     * @param string $type
     * @return array
     * @throws NoSuchEntityException
     */
    public function getData(string $id, string $type): array
    {
        switch ($type) {
            case Config::PAGE_TITLE_HOMEPAGE:
                return $this->getUrls($id, $type, null);
            case Config::PAGE_TITLE_CATEGORY:
                return $this->getUrls($id, $type, 'categoryRepositoryInterface');
            case Config::PAGE_TITLE_PRODUCT:
                return $this->getUrls($id, $type, 'productRepositoryInterface');
            case Config::PAGE_TITLE_CMS:
                return $this->getUrls($id, $type, 'pageRepositoryInterface');
            default:
                throw new NoSuchEntityException(
                    __("The page type that was requested doesn't exist. Verify the page type and try again.")
                );
        }
    }

    /**
     * @param string $id
     * @param string $type
     * @param string|null $object
     * @return array
     * @throws NoSuchEntityException
     */
    private function getUrls(string $id, string $type, string $object = null): array
    {
        if ($type == Config::PAGE_TITLE_CATEGORY) {
            $object = $this->$object->get($id);
        } elseif ($type ==  Config::PAGE_TITLE_HOMEPAGE) {
            $object = new \Magento\Framework\DataObject();
        } else {
            $object = $this->$object->getById($id);
        }

        return $this->getAlternateHreflang->execute($object, $type);
    }
}

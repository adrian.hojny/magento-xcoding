<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflangGraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magefan\AlternateHreflang\Model\Config;

class CmsPagesAlternateHreflang implements ResolverInterface
{
    /**
     * @var DataProvider\AlternateHreflang
     */
    private $alternateHreflangDataProvider;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param DataProvider\AlternateHreflang $alternateHreflangDataProvider
     * @param Config $config
     */
    public function __construct(
        DataProvider\AlternateHreflang $alternateHreflangDataProvider,
        Config $config
    )
    {
        $this->alternateHreflangDataProvider = $alternateHreflangDataProvider;
        $this->config = $config;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        try {
            $items = [];

            if ($value['url_key'] == $this->config->getCmsIndex()) {
                $urls = $this->alternateHreflangDataProvider->getData($value['page_id'], 'homepage');
            } else {
                $urls = $this->alternateHreflangDataProvider->getData($value['page_id'], 'cms');
            }
            foreach ($urls as $languageCode => $url) {
                $items[] = ['hreflang' => $languageCode, 'href' => $url];
            }
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }

        return $items;
    }
}

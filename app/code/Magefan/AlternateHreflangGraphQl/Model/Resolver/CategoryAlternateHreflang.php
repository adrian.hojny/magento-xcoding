<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

namespace Magefan\AlternateHreflangGraphQl\Model\Resolver;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

class CategoryAlternateHreflang implements ResolverInterface
{
    /**
     * @var DataProvider\AlternateHreflang
     */
    private $alternateHreflangDataProvider;

    /**
     * @param DataProvider\AlternateHreflang $alternateHreflangDataProvider
     */
    public function __construct(
        DataProvider\AlternateHreflang $alternateHreflangDataProvider
    )
    {
        $this->alternateHreflangDataProvider = $alternateHreflangDataProvider;
    }

    /**
     * @inheritdoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    {
        try {
            $items = [];
            $urls = $this->alternateHreflangDataProvider->getData($value['id'], 'category');
            foreach ($urls as $languageCode => $url) {
                $items[] = ['hreflang' => $languageCode, 'href' => $url];
            }
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        }

        return $items;
    }
}

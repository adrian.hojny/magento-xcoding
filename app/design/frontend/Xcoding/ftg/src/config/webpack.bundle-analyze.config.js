const webpack = require('webpack');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const [config] = require('./webpack.production.config');

config.plugins.push(new BundleAnalyzerPlugin());

module.exports = config;

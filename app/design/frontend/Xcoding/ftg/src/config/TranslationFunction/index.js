/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

const translations = require('../../../i18n/pl_PL.json');

const mockTranslations = (format, ...args) => {
    // eslint-disable-next-line fp/no-let
    let i = 0;

    const hardcodedPlTranslatedStr = translations[format] || format;
    // const txt = format;
    const txt = process.env.NODE_ENV === 'development' ? hardcodedPlTranslatedStr : format;
    return txt.replace(/%s/g, () => args[i++]);
};

module.exports = mockTranslations;

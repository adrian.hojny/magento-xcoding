/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */
export const UPDATE_CONFIG = 'UPDATE_CONFIG';
export const SET_CURRENCY = 'SET_CURRENCY';
export const SET_STORE_LIST = 'SET_STORE_LIST';

export const updateConfig = (config) => ({
    type: UPDATE_CONFIG,
    config,
});

export const setCurrency = (currency) => ({
    type: SET_CURRENCY,
    currency,
});

export const setStoreList = (storeList) => ({
    type: SET_STORE_LIST,
    storeList,
});

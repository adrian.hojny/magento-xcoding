export const UPDATE_REVIEW_HELPFULNESS_VOTES = 'UPDATE_REVIEW_HELPFULNESS_VOTES';

export const updateReviewHelpfulnessVotes = (reviewId, votes) => ({
    type: UPDATE_REVIEW_HELPFULNESS_VOTES,
    reviewId,
    votes,
});

export { UPDATE_PRODUCT_DETAILS, updateProductDetails } from 'SourceStore/Product/Product.action';

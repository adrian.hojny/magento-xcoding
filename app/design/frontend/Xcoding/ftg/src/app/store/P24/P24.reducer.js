/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import {
    BLIK_CODE_NAME,
    BLIK_CODE_NAME_TOC,
    P24_INNER_METHODS_NAME,
    P24_INNER_METHODS_NAME_TOC,
    P24CARD_CODE_NAME,
    P24CARD_CODE_NAME_TOC,
} from 'Component/Przelewy24/Przelewy24.config';
import { checkIfGroupIsValid, getCorrespondingTOCName } from 'Component/Przelewy24/Przelewy24.utils';
import {
    PURGE_ALL_FORM_FIELD,
    PURGE_FORM_FIELD,
    PURGE_OTHER_FORM_GROUPS,
    SET_FORM_FIELD_VALUE,
    VALIDATE_FORM_GROUP,
} from './P24.actions';

export const initialDataFieldsValues = {
    [P24_INNER_METHODS_NAME]: null,
    [BLIK_CODE_NAME]: null,
    [P24CARD_CODE_NAME]: null,
};

export const initialTOCValues = {
    [P24_INNER_METHODS_NAME_TOC]: false,
    [P24CARD_CODE_NAME_TOC]: false,
    [BLIK_CODE_NAME_TOC]: false,
};

export const initialFormValues = {
    ...initialDataFieldsValues,
    ...initialTOCValues,
};

export const initialState = {
    formValues: initialFormValues,
    formGroupsValidity: initialDataFieldsValues,
};

const getFieldGroupValues = (name, formValues) => {
    const TOCName = getCorrespondingTOCName(name);
    return {
        [name]: formValues[name],
        [TOCName]: formValues[TOCName],
    };
};

export const P24Reducer = (state = initialState, action) => {
    const { formValues, formGroupsValidity } = state;
    const { name, value } = action;

    switch (action.type) {
        case SET_FORM_FIELD_VALUE:
            return {
                ...state,
                formValues: {
                    ...formValues,
                    [name]: value,
                },
            };

        case PURGE_FORM_FIELD:
            return {
                ...state,
                formValues: {
                    ...formValues,
                    [name]: null,
                },
            };

        case PURGE_ALL_FORM_FIELD:
            return {
                ...initialState,
            };

        case VALIDATE_FORM_GROUP:
            return {
                ...state,
                formGroupsValidity: {
                    ...formGroupsValidity,
                    [name]: checkIfGroupIsValid(name, state.formValues),
                },
            };

        case PURGE_OTHER_FORM_GROUPS:
            return {
                ...state,
                formValues: {
                    ...getFieldGroupValues(name, formValues),
                    // skipping rest so it's cleared
                },
            };

        default:
            return state;
    }
};

export default P24Reducer;

import { ReviewDispatcher as SourceReviewDispatcher } from 'SourceStore/Review/Review.dispatcher';

import ReviewQuery from 'Query/Review.query';
import { showNotification } from 'Store/Notification/Notification.action';
import { updateReviewHelpfulnessVotes } from 'Store/Product/Product.action';
import { fetchMutation } from 'Util/Request';

export class ReviewDispatcher extends SourceReviewDispatcher {
    submitProductReview(dispatch, options) {
        const reviewItem = options;

        reviewItem.rating_data = this.prepareRatingData(reviewItem);

        return fetchMutation(ReviewQuery.getAddProductReviewMutation(reviewItem)).then(
            () => dispatch(showNotification('success', __('You submitted your review for moderation.'))),
            // eslint-disable-next-line no-console
            (error) =>
                dispatch(
                    showNotification('error', error?.[0]?.message ? error[0].message : __('Error submitting review!')),
                ) && console.log(error),
        );
    }

    addHelpfulnessVote(dispatch, options) {
        const { vote, reviewId } = options;

        return fetchMutation(ReviewQuery.getAddReviewHelpfulnessVoteMutation(vote, reviewId)).then(
            ({ addReviewHelpfulnessVote: votes }) => {
                dispatch(showNotification('success', __('A vote has been cast.')));
                dispatch(updateReviewHelpfulnessVotes(reviewId, votes));
            },
            // eslint-disable-next-line no-console
            (error) =>
                dispatch(
                    showNotification(
                        'error',
                        error?.[0]?.message ? error[0].message : __('Error! Something went wrong'),
                    ),
                ) && console.log(error),
        );
    }
}

export default new ReviewDispatcher();

import ProductListQuery from 'Query/ProductList.query';
import { showNotification } from 'Store/Notification/Notification.action';
import { getLocalRecentlyViewed } from 'Store/RecentlyViewed';
import { getIndexedProduct } from 'Util/Product';
import { QueryDispatcher } from 'Util/Request';

export class RecentlyViewedDispatcher extends QueryDispatcher {
    currentProductLinks = [];

    constructor() {
        super('RecentlyViewed');
    }

    onSuccess(data, dispatch) {
        const {
            products: { items },
        } = data;
        const indexedProducts = items.map((product) => getIndexedProduct(product));
        dispatch(getLocalRecentlyViewed(indexedProducts));
    }

    onError(error, dispatch) {
        dispatch(showNotification('error', 'Error fetching RecentlyViewed!', error));
    }

    prepareRequest(product_links) {
        if (JSON.stringify(this.currentProductLinks) === JSON.stringify(product_links)) {
            return [];
        }
        this.currentProductLinks = product_links;
        return [
            ProductListQuery.getQuery({
                args: {
                    filter: {
                        productsSkuArray: product_links,
                    },
                },
            }),
        ];
    }
}

export default new RecentlyViewedDispatcher();

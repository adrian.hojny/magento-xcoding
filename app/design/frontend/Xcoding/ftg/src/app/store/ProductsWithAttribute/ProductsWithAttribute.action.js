export const UPDATE_PRODUCTS_WITH_ATTRIBUTE = 'UPDATE_PRODUCTS_WITH_ATTRIBUTE';
export const SET_PRODUCTS_WITH_ATTRIBUTE_LOADING = 'SET_PRODUCTS_WITH_ATTRIBUTE_LOADING';

export const updateProductsWithAttribute = (attribute, products) => ({
    type: UPDATE_PRODUCTS_WITH_ATTRIBUTE,
    attribute,
    products,
});

export const setProductsWithAttributeLoading = (attribute, isLoading) => ({
    type: SET_PRODUCTS_WITH_ATTRIBUTE_LOADING,
    attribute,
    isLoading,
});

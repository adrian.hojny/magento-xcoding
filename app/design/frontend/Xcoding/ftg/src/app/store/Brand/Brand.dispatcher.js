/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import ProductListQuery from 'Query/ProductList.query';
import { updateCurrentBrand } from 'Store/Brand';
import { updateNoMatch } from 'Store/NoMatch/NoMatch.action';
import { showNotification } from 'Store/Notification/Notification.action';
import { QueryDispatcher } from 'Util/Request';

/**
 * Brand Dispatcher
 * @class BrandDispatcher
 * @extends QueryDispatcher
 */
export class BrandDispatcher extends QueryDispatcher {
    constructor() {
        super('Brand');
    }

    onSuccess(data, dispatch, { isSearchPage }) {
        const {
            category = {},
            category: { id },
        } = data;

        if (!id && !isSearchPage) {
            dispatch(updateNoMatch(true));
        }
        dispatch(updateCurrentBrand(category));
    }

    onError(error, dispatch, { isSearchPage }) {
        if (!isSearchPage) {
            dispatch(updateNoMatch(true));
            dispatch(showNotification('error', 'Error fetching Category!', error));
        } else {
            dispatch(updateCurrentBrand({ id: 'all-products' }));
        }
    }

    prepareRequest(options) {
        return ProductListQuery.getQuery(options);
    }
}

export default new BrandDispatcher();

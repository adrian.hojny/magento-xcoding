import { CHANGE_NAVIGATION_STATE, GOTO_PREVIOUS_NAVIGATION_STATE } from 'SourceStore/Navigation/Navigation.action';

export const RELOAD_HEADER = 'RELOAD_HEADER';

export const reloadHeader = () => ({
    type: RELOAD_HEADER,
});

export const goToPreviousNavigationState = (navigationType, ignoredStates) => ({
    type: GOTO_PREVIOUS_NAVIGATION_STATE,
    navigationType,
    ignoredStates,
});

export const changeNavigationState = (navigationType, navigationState) => ({
    type: CHANGE_NAVIGATION_STATE,
    navigationType,
    navigationState,
});

export { CHANGE_NAVIGATION_STATE, GOTO_PREVIOUS_NAVIGATION_STATE };

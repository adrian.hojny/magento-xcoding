import { formatConfigurableOptions, initialState } from 'SourceStore/Product/Product.reducer';

import { UPDATE_PRODUCT_DETAILS, UPDATE_REVIEW_HELPFULNESS_VOTES } from 'Store/Product/Product.action';
import { getIndexedProduct } from 'Util/Product';
import RecentlyViewed from 'Util/RecentlyViewed/RecentlyViewed';

const ProductReducer = (state = initialState, action) => {
    if (state.product.sku && !RecentlyViewed.existProductInRecentlyViewed(state.product.sku)) {
        RecentlyViewed.updateLocalRecentlyViewed(state.product.sku);
    }

    switch (action.type) {
        case UPDATE_PRODUCT_DETAILS:
            const { product } = action;

            return {
                ...state,
                product: getIndexedProduct(product),
            };

        case UPDATE_REVIEW_HELPFULNESS_VOTES:
            const { reviewId, votes } = action;

            return {
                ...state,
                product: {
                    ...state.product,
                    reviews: state.product.reviews.map((review) => {
                        if (review.review_id === reviewId) return { ...review, helpfulness: votes };
                        return review;
                    }),
                },
            };

        default:
            return state;
    }
};

export { formatConfigurableOptions, initialState };
export default ProductReducer;

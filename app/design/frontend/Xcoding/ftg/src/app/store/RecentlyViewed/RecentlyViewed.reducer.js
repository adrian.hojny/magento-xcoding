import { GET_LOCAL_RECENTLY_VIEWED } from './RecentlyViewed.actions';

export const initialState = {
    recentlyViewed: [],
};

const RecentlyViewedReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_LOCAL_RECENTLY_VIEWED:
            return {
                ...state,
                recentlyViewed: action.recentlyViewed,
            };

        default:
            return state;
    }
};

export default RecentlyViewedReducer;

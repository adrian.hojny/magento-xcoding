import { CART_TOTALS } from 'SourceStore/Cart/Cart.reducer';

import {
    DISABLE_CART_LOADER,
    REMOVE_CART_ITEM,
    SET_CART_ITEM_REMOVE_STATUS,
    SET_SHIPPING_METHOD,
    UPDATE_TOTALS,
} from 'Store/Cart/Cart.action';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { getIndexedProduct } from 'Util/Product';

export { CART_TOTALS } from 'SourceStore/Cart/Cart.reducer';

export const initialState = {
    cartTotals: {},
    shippingMethod: null,
    isLoading: true,
};

const setItemRemoveStatus = (state, action) => {
    const {
        cartTotals: { items },
    } = state;
    const { itemId, status } = action;

    const index = items.findIndex((el) => el.item_id === itemId);

    if (index < 0) return state;

    const newItem = { ...items[index], removeStatus: status };
    const newItems = Array.from(items);

    newItems[index] = newItem;

    return { ...state, cartTotals: { ...state.cartTotals, items: newItems } };
};

const updateCartTotals = (state, action) => {
    const { cartData: cartTotals } = action;
    const {
        cartTotals: { items: oldItems },
    } = state;

    if (Object.hasOwnProperty.call(cartTotals, 'items')) {
        cartTotals.items = cartTotals.items.map((item) => {
            const normalizedItem = item;
            normalizedItem.product = getIndexedProduct(item.product);

            return normalizedItem;
        });
    }

    const removedItems = (oldItems || []).reduce((acc, item, index) => {
        if (item.removeStatus) return [...acc, { index, item }];
        return acc;
    }, []);

    BrowserDatabase.setItem(cartTotals, CART_TOTALS);

    const itemsWithRemoved = Array.from(cartTotals.items || []);

    removedItems.forEach(({ index, item }) => itemsWithRemoved.splice(index, 0, item));

    return { ...state, cartTotals: { ...cartTotals, items: itemsWithRemoved } };
};

const removeItem = (state, action) => {
    const { itemId } = action;
    const { cartTotals } = state;

    return {
        ...state,
        cartTotals: { ...cartTotals, items: cartTotals.items.filter(({ item_id }) => item_id !== itemId) },
    };
};

const CartReducer = (state = initialState, action) => {
    const { type } = action;

    switch (type) {
        case SET_CART_ITEM_REMOVE_STATUS:
            return setItemRemoveStatus(state, action);

        case UPDATE_TOTALS:
            return updateCartTotals(state, action);

        case REMOVE_CART_ITEM:
            return removeItem(state, action);

        case SET_SHIPPING_METHOD:
            return {
                ...state,
                shippingMethod: action.method,
            };
        case DISABLE_CART_LOADER:
            return {
                ...state,
                isLoading: false,
            };

        default:
            return state;
    }
};

export default CartReducer;

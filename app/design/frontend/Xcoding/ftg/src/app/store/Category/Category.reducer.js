import { UPDATE_CURRENT_CATEGORY, UPDATE_CURRENT_PARENT_CATEGORY } from 'Store/Category/Category.actions';

export const initialState = {
    category: {
        isLoading: true,
    },
};

const CategoryReducer = (state = initialState, { type, category }) => {
    switch (type) {
        case UPDATE_CURRENT_CATEGORY:
            const newCategory = { ...category };
            const { breadcrumbs } = newCategory;
            const lastBreadcrumb = breadcrumbs && breadcrumbs.length > 0 ? breadcrumbs[breadcrumbs.length - 1] : {};

            const breadcrumbsUrlKey = lastBreadcrumb.category_url && lastBreadcrumb.category_url.substring(1);
            // change category 1 step forward
            if (breadcrumbsUrlKey === state.category.url_key) {
                newCategory.parent = state.category;
            }

            // change category parallel
            if (breadcrumbsUrlKey === (state.category.parent || {}).url_key) {
                newCategory.parent = state.category.parent;
            }

            return {
                ...state,
                category: {
                    isLoading: false,
                    ...newCategory,
                },
            };

        case UPDATE_CURRENT_PARENT_CATEGORY:
            return {
                ...state,
                category: {
                    ...state.category,
                    parent: category,
                },
            };

        default:
            return state;
    }
};

export default CategoryReducer;

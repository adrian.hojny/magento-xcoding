import { HIDE_ACTIVE_OVERLAY } from 'SourceStore/Overlay/Overlay.action';
import sourceOverlayReducer from 'SourceStore/Overlay/Overlay.reducer';

import { SHOW_DIMMER } from './Overlay.action';

export const initialState = {
    activeOverlay: '',
    areOtherOverlaysOpen: true,
    isDimmerActive: false,
};

export const OverlayReducer = (state = initialState, action) => {
    switch (action.type) {
        case HIDE_ACTIVE_OVERLAY:
            return {
                ...state,
                activeOverlay: '',
                areOtherOverlaysOpen: false,
                isDimmerActive: false,
            };

        case SHOW_DIMMER:
            return {
                ...state,
                isDimmerActive: true,
            };

        default:
            return sourceOverlayReducer(state, action);
    }
};

export default OverlayReducer;

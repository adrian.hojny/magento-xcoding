import sourceProductListReducer, { initialState } from 'SourceStore/ProductListInfo/ProductListInfo.reducer';

import { UPDATE_PRODUCT_LIST_INFO } from 'Store/ProductListInfo/ProductListInfo.action';

const reduceFilters = (filters) =>
    filters.reduce((co, item) => {
        const { request_var: attribute_code, name: attribute_label, input_type: attribute_type, filter_items } = item;

        // TODO: Remove this hardcoded check, after solving the problem on BE: https://github.com/magento/magento2/blob/89cf888f6f3c7b163702969a8e256f9f0486f6b8/app/code/Magento/Catalog/Model/Layer/FilterList.php#L70
        if (attribute_code === 'cat') {
            return co;
        }

        const { attribute_values, attribute_options } = filter_items.reduce(
            (attribute, option) => {
                const { value_string } = option;
                const { attribute_values, attribute_options } = attribute;

                attribute_values.push(value_string);
                return {
                    ...attribute,
                    attribute_options: {
                        ...attribute_options,
                        [+value_string]: option,
                    },
                };
            },
            { attribute_values: [], attribute_options: {} },
        );

        return {
            ...co,
            [attribute_code]: {
                attribute_code,
                attribute_label,
                attribute_values,
                attribute_type,
                attribute_options,
            },
        };
    }, {});

const ProductListReducer = (state = initialState, action) => {
    const {
        type,
        products: { filters: availableFilters = [], min_price, max_price, sort_fields: sortFields } = {},
        selectedFilter,
    } = action;

    switch (type) {
        case UPDATE_PRODUCT_LIST_INFO:
            const minPrice = selectedFilter?.priceRange?.min || min_price;
            const maxPrice = selectedFilter?.priceRange?.max || max_price;

            return {
                ...state,
                filters: reduceFilters(availableFilters),
                sortFields,
                minPrice: Math.floor(minPrice),
                maxPrice: Math.ceil(maxPrice),
                isLoading: false,
            };

        default:
            return sourceProductListReducer(state, action);
    }
};

export default ProductListReducer;

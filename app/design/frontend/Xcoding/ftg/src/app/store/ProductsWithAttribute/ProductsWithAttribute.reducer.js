import { LOOK_ATTRIBUTE_NAME } from 'Component/CompleteLook/CompleteLook.container';
import { COLLECTION_ATTRIBUTE_NAME } from 'Component/ProductCollections/ProductCollections.container';
import { SET_PRODUCTS_WITH_ATTRIBUTE_LOADING, UPDATE_PRODUCTS_WITH_ATTRIBUTE } from 'Store/ProductsWithAttribute';

// Array of names of all attributes used with this reducer
const USED_ATTRIBUTES = [LOOK_ATTRIBUTE_NAME, COLLECTION_ATTRIBUTE_NAME];

const getInitialState = (attributeNames) =>
    attributeNames.reduce(
        (acc, attribute) => ({
            ...acc,
            [attribute]: {
                isLoading: false,
                items: [],
                total_count: 0,
                page_info: {},
            },
        }),
        {},
    );

export const initialState = {
    ...getInitialState(USED_ATTRIBUTES),
};

export const ProductsWithAttributeReducer = (state = initialState, action) => {
    const { attribute, products, isLoading } = action;

    switch (action.type) {
        case UPDATE_PRODUCTS_WITH_ATTRIBUTE:
            return {
                ...state,
                [attribute]: {
                    isLoading: false,
                    ...products,
                },
            };

        case SET_PRODUCTS_WITH_ATTRIBUTE_LOADING:
            return {
                ...state,
                [attribute]: {
                    ...state[attribute],
                    isLoading,
                },
            };

        default:
            return state;
    }
};

export default ProductsWithAttributeReducer;

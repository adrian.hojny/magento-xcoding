export const UPDATE_CURRENT_BRAND = 'UPDATE_CURRENT_BRAND';

/**
 * Update Current Category
 * @param {String} categoryUrlPath url path Main Category object
 * @return {void}
 */
export const updateCurrentBrand = (brand) => ({
    type: UPDATE_CURRENT_BRAND,
    brand,
});

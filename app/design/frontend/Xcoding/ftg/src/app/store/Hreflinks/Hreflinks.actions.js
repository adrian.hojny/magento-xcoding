export const UPDATE_LINKS = 'UPDATE_LINKS';
export const REMOVE_LINKS = 'REMOVE_LINKS';

export const updateLinks = (links) => ({
    type: UPDATE_LINKS,
    payload: {
        links,
    },
});

export const removeLinks = () => ({
    type: REMOVE_LINKS,
});

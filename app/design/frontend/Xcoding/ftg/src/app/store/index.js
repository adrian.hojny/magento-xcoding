import { combineReducers, createStore } from 'redux';

import { staticReducers as sourceReducers } from 'SourceStore/index';

import { BrandReducer } from 'Store/Brand';
import { HreflinksReducer } from 'Store/Hreflinks';
import { P24Reducer } from 'Store/P24';
import { ProductsWithAttributeReducer } from 'Store/ProductsWithAttribute';
import { RecentlyViewedReducer } from 'Store/RecentlyViewed';

export const staticReducers = {
    ...sourceReducers,
    RecentlyViewedReducer,
    ProductsWithAttributeReducer,
    BrandReducer,
    P24Reducer,
    HreflinksReducer,
};

export function createReducer(asyncReducers) {
    return combineReducers({
        ...staticReducers,
        ...asyncReducers,
    });
}

export const store = createStore(
    createReducer(),
    // enable Redux dev-tools only in development
    process.env.NODE_ENV === 'development' &&
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__({
            trace: true,
        }),
);

// Configure the store
export default function configureStore() {
    // Add a dictionary to keep track of the registered async reducers
    store.asyncReducers = {};

    // Create an inject reducer function
    // This function adds the async reducer, and creates a new combined reducer
    store.injectReducer = (key, asyncReducer) => {
        store.asyncReducers[key] = asyncReducer;
        store.replaceReducer(createReducer(store.asyncReducers));
    };

    // Return the modified store
    return store;
}

export function getStore() {
    return store;
}

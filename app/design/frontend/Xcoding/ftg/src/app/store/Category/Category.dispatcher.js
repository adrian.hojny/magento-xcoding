import { CategoryDispatcher as SourceCategoryDispatcher } from 'SourceStore/Category/Category.dispatcher';

import { updateCurrentCategory } from 'Store/Category/Category.action';
import { updateNoMatch } from 'Store/NoMatch/NoMatch.action';

class CategoryDispatcher extends SourceCategoryDispatcher {
    onSuccess(data, dispatch, { isSearchPage }) {
        const {
            category = {},
            category: { id },
        } = data;

        if (!id && !isSearchPage) dispatch(updateNoMatch(true));
        dispatch(updateCurrentCategory(category));
        // CmsBlocksAndSliderDispatcher
        //     .handleData(dispatch, { identifiers: [`${ADVERTISING_TILE_PREFIX}/${category.id}`] });
    }
}

export default new CategoryDispatcher();

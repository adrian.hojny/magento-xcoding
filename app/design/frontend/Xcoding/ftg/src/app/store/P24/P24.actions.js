export const SET_FORM_FIELD_VALUE = 'SET_FORM_FIELD_VALUE';
export const PURGE_FORM_FIELD = 'PURGE_FORM_FIELD';
export const PURGE_ALL_FORM_FIELD = 'PURGE_ALL_FORM_FIELD';
export const VALIDATE_FORM_GROUP = 'VALIDATE_FORM_GROUP';
export const PURGE_OTHER_FORM_GROUPS = 'PURGE_OTHER_FORM_GROUPS';

export const setFormFieldValue = (name, value) => ({
    type: SET_FORM_FIELD_VALUE,
    name,
    value,
});

export const purgeFormField = (name) => ({
    type: PURGE_FORM_FIELD,
    name,
});

export const purgeAllFormField = () => ({
    type: PURGE_ALL_FORM_FIELD,
});

export const validateFieldGroup = (name) => ({
    type: VALIDATE_FORM_GROUP,
    name,
});

export const purgeOtherFormGroups = (name) => ({
    type: PURGE_OTHER_FORM_GROUPS,
    name,
});

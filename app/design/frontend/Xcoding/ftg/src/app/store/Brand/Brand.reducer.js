import { UPDATE_CURRENT_BRAND } from 'Store/Brand';

export const initialState = {
    category: {
        isLoading: true,
    },
};

const BrandReducer = (state = initialState, { type, category }) => {
    switch (type) {
        case UPDATE_CURRENT_BRAND:
            const newCategory = { ...category };
            const { breadcrumbs } = newCategory;
            const lastBreadcrumb = breadcrumbs && breadcrumbs.length > 0 ? breadcrumbs[breadcrumbs.length - 1] : {};

            // change category 1 step forward
            if (lastBreadcrumb.category_url_key === state.category.url_key) {
                newCategory.parent = state.category;
            }

            // change category parallel
            if (lastBreadcrumb.category_url_key === (state.category.parent || {}).url_key) {
                newCategory.parent = state.category.parent;
            }

            return {
                ...state,
                category: {
                    isLoading: false,
                    ...newCategory,
                },
            };

        default:
            return state;
    }
};

export default BrandReducer;

import * as actionTypes from './Hreflinks.actions';

const initialState = {
    links: [],
};

const HreflinksReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_LINKS: {
            const { links = [] } = action.payload;

            return {
                ...state,
                links: Array.from(links),
            };
        }
        case actionTypes.REMOVE_LINKS: {
            return {
                ...state,
                links: [],
            };
        }
        default:
            return state;
    }
};

export default HreflinksReducer;

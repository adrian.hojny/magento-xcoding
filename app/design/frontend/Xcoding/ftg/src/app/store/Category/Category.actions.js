export const UPDATE_CURRENT_PARENT_CATEGORY = 'UPDATE_CURRENT_PARENT_CATEGORY';

export const updateCurrentParentCategory = (category) => ({
    type: UPDATE_CURRENT_PARENT_CATEGORY,
    category,
});

export { UPDATE_CURRENT_CATEGORY, updateCurrentCategory } from 'SourceStore/Category/Category.action';

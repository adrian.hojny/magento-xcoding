import ProductListQuery from 'Query/ProductList.query';
import {
    setProductsWithAttributeLoading,
    updateProductsWithAttribute,
} from 'Store/ProductsWithAttribute/ProductsWithAttribute.action';
import { getIndexedProduct } from 'Util/Product';
import { QueryDispatcher } from 'Util/Request';
import { ONE_MONTH_IN_SECONDS } from 'Util/Request/QueryDispatcher';

export const getAttributeNameFromOptions = (options) => options.args.filter.attribute.name;

export class ProductsWithAttributeDispatcher extends QueryDispatcher {
    constructor() {
        super('ProductsWithAttribute', ONE_MONTH_IN_SECONDS);
    }

    onSuccess({ products }, dispatch, options) {
        const indexedProducts = products;
        indexedProducts.items = products.items.map((product) => getIndexedProduct(product));
        dispatch(updateProductsWithAttribute(getAttributeNameFromOptions(options), indexedProducts));
    }

    onError(error, dispatch, options) {
        dispatch(setProductsWithAttributeLoading(getAttributeNameFromOptions(options), false));
    }

    prepareRequest(options, dispatch) {
        dispatch(setProductsWithAttributeLoading(getAttributeNameFromOptions(options), true));

        return [ProductListQuery.getQuery(options)];
    }
}

export default new ProductsWithAttributeDispatcher();

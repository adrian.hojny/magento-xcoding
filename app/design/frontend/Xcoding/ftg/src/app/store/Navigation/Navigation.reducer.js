import sourceNavigationReducer, {
    BOTTOM_NAVIGATION_TYPE,
    initialState as sourceInitialState,
    TOP_NAVIGATION_TYPE,
} from 'SourceStore/Navigation/Navigation.reducer';

import { CHANGE_NAVIGATION_STATE, GOTO_PREVIOUS_NAVIGATION_STATE } from 'Store/Navigation/Navigation.action';
import { RELOAD_HEADER } from './Navigation.action';

export const initialState = {
    ...sourceInitialState,
    reloadFlag: false,
};

const getNewNavigationState = (navigationStateHistory, ignoredStates) => {
    navigationStateHistory.pop();

    const newNavigationState = navigationStateHistory.slice(-1)[0];

    if (ignoredStates && newNavigationState && ignoredStates.includes(newNavigationState.name)) {
        return getNewNavigationState(navigationStateHistory, ignoredStates);
    }

    return navigationStateHistory && navigationStateHistory.slice(-1)[0];
};

const NavigationReducer = (state = initialState, action) => {
    const { navigationType, navigationState, ignoredStates } = action;

    const initialTypeState = initialState[TOP_NAVIGATION_TYPE];

    const {
        [navigationType]: { navigationStateHistory, navigationState: prevNavigationState } = initialTypeState,
    } = state;

    switch (action.type) {
        case RELOAD_HEADER:
            return {
                ...state,
                reloadFlag: !state.reloadFlag,
            };
        case GOTO_PREVIOUS_NAVIGATION_STATE:
            const newNavigationState = getNewNavigationState(navigationStateHistory, ignoredStates);

            if (!newNavigationState) return state;

            return {
                ...state,
                [navigationType]: {
                    navigationStateHistory,
                    navigationState: newNavigationState,
                },
            };
        case CHANGE_NAVIGATION_STATE:
            const { name: nextName, force = false } = navigationState;
            const { name: prevName } = prevNavigationState || {};

            if (nextName === prevName && !force) {
                navigationStateHistory[navigationStateHistory.length - 1] = navigationState;
            } else {
                navigationStateHistory.push(navigationState);
            }

            return {
                ...state,
                [navigationType]: {
                    navigationStateHistory,
                    navigationState,
                },
            };

        default:
            return sourceNavigationReducer(state, action);
    }
};

export { BOTTOM_NAVIGATION_TYPE, TOP_NAVIGATION_TYPE };
export default NavigationReducer;

export const GET_LOCAL_RECENTLY_VIEWED = 'GET_LOCAL_RECENTLY_VIEWED';
export const SET_ = 'GET_LOCAL_RECENTLY_VIEWED';
export const GET_LOCAL_RECENTLY_VIEWED = 'GET_LOCAL_RECENTLY_VIEWED';

export const getLocalRecentlyViewed = (data) => ({
    type: GET_LOCAL_RECENTLY_VIEWED,
    recentlyViewed: data,
});

import { HIDE_ACTIVE_OVERLAY, hideActiveOverlay, TOGGLE_OVERLAY } from 'SourceStore/Overlay/Overlay.action';

export { HIDE_ACTIVE_OVERLAY, hideActiveOverlay, TOGGLE_OVERLAY };

export const SHOW_DIMMER = 'SHOW_DIMMER';

export const toggleOverlayByKey = (overlayKey) => ({
    type: TOGGLE_OVERLAY,
    overlayKey,
});

export const showDimmer = () => ({
    type: SHOW_DIMMER,
});

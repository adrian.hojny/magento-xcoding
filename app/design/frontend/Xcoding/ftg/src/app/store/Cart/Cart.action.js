export * from 'SourceStore/Cart/Cart.action';

export const SET_CART_ITEM_REMOVE_STATUS = 'SET_CART_ITEM_REMOVE_STATUS';

export const REMOVE_CART_ITEM = 'REMOVE_CART_ITEM';

export const CART_ITEM_REMOVED = 'removed';
export const CART_ITEM_IN_STASH = 'stash';
export const SET_SHIPPING_METHOD = 'SET_SHIPPING_METHOD';
export const DISABLE_CART_LOADER = 'DISABLE_CART_LOADER';

export const setCartItemRemoveStatus = (itemId, status) => ({
    type: SET_CART_ITEM_REMOVE_STATUS,
    status,
    itemId,
});

export const removeCartItem = (itemId) => ({
    type: REMOVE_CART_ITEM,
    itemId,
});

export const setShippingMethod = (method) => ({
    type: SET_SHIPPING_METHOD,
    method,
});

export const disableCartLoader = () => ({
    type: DISABLE_CART_LOADER,
});

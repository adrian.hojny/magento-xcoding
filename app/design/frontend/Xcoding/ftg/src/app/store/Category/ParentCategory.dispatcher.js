import CategoryQuery from 'Query/Category.query';
import { updateCurrentParentCategory } from 'Store/Category/Category.actions';
import { QueryDispatcher } from 'Util/Request';

/**
 * ParentCategory Dispatcher
 * @class ParentCategoryDispatcher
 * @extends QueryDispatcher
 */
export class ParentCategoryDispatcher extends QueryDispatcher {
    constructor() {
        super('ParentCategory');
    }

    onSuccess(data, dispatch) {
        const {
            category = {},
            category: { id },
        } = data;

        if (id) dispatch(updateCurrentParentCategory(category));
    }

    prepareRequest(options) {
        return CategoryQuery.getQuery(options);
    }
}

export default new ParentCategoryDispatcher();

export { RatingItemsType, RatingOptionItemType } from 'SourceType/Rating';

export const NOT_COMFORTABLE = 'NOT_COMFORTABLE';
export const COMFORTABLE = 'COMFORTABLE';
export const VERY_COMFORTABLE = 'VERY_COMFORTABLE';

export const SIZE_TOO_SMALL = 'SIZE_TOO_SMALL';
export const SIZE_OK = 'SIZE_OK';
export const SIZE_TOO_LARGE = 'SIZE_TOO_LARGE';

export const QUALITY_LOW = 'QUALITY_LOW';
export const QUALITY_OK = 'QUALITY_OK';
export const QUALITY_HIGH = 'QUALITY_HIGH';

export const RECOMMENDED = 'RECOMMENDED';
export const NOT_RECOMMENDED = 'NOT_RECOMMENDED';

export const SIZE_FIELD_NAME = 'size_value';
export const QUALITY_FIELD_NAME = 'quality_value';
export const COMFORT_FIELD_NAME = 'comfort_value';

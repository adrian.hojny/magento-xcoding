import { OrderQuery as SourceOrderQuery } from 'SourceQuery/Order.query';

export class OrderQuery extends SourceOrderQuery {
    _getBaseOrderInfoFields(isList) {
        return [
            'id',
            'increment_id',
            'created_at',
            'status_label',
            'grand_total',
            'order_currency_code',
            ...(isList ? [] : ['sub_total']),
        ];
    }
}

export default new OrderQuery();

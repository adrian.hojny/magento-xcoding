import { isSignedIn } from 'Util/Auth';
import { Field } from 'Util/Query';

export class ConfigQuery {
    getChangeCurrencyMutation(currency, guestCartId) {
        const mutation = new Field('changeCurrency').addArgument('currency', 'String', currency);

        if (!isSignedIn()) {
            mutation.addArgument('guestCartId', 'String', guestCartId);
        }

        return mutation;
    }

    getCurrencies() {
        return new Field('currency').addFieldList(this._getCurrenciesFields());
    }

    _getCurrenciesFields() {
        return ['available_currency_codes', 'default_display_currency_code', 'default_display_currency_symbol'];
    }

    getStoreListField() {
        return new Field('storeList').addFieldList(this._getStoreListFields());
    }

    getContactTopicsListField() {
        return new Field('storeConfig').addFieldList([this._getContactTopicsListFields()]);
    }

    getCheckoutAgreements() {
        return new Field('checkoutAgreements').addFieldList(this._getCheckoutAgreementFields());
    }

    _getCheckoutAgreementFields() {
        return ['agreement_id', 'checkbox_text', 'content', 'content_height', 'is_html', 'mode', 'name'];
    }

    _getStoreListFields() {
        return [
            'name',
            'is_active',
            'base_url',
            'base_link_url',
            'code',
            'default_display_currency_code',
            'default_country',
            'website_id',
            'locale',
        ];
    }

    _getContactTopicsListFields() {
        return new Field('contact_form_topics').addFieldList(['code', 'label']);
    }

    getQuery() {
        // debugger;
        return new Field('storeConfig').addFieldList(this._getStoreConfigFields());
    }

    _getStoreConfigFields() {
        return [
            'code',
            'is_active',
            'cms_home_page',
            'cms_no_route',
            'copyright',
            'timezone',
            'header_logo_src',
            'timezone',
            'title_prefix',
            'title_suffix',
            'default_display_currency_code',
            'default_keywords',
            'default_title',
            'default_country',
            'default_display_currency_code',
            'secure_base_media_url',
            'paypal_sandbox_flag',
            'paypal_client_id',
            'logo_alt',
            'cookie_text',
            'cookie_link',
            'terms_are_enabled',
            'base_url',
            'pagination_frame',
            'pagination_frame_skip',
            'anchor_text_for_previous',
            'anchor_text_for_next',
            'reviews_are_enabled',
            'reviews_allow_guest',
            'demo_notice',
            'guest_checkout',
            'is_email_confirmation_required',
            'base_link_url',
            'price_slider_vat_rate',
            'website_id',
            'locale',
            'brand_desctiption_suffix',
            'category_desctiption_suffix',
            'product_desctiption_suffix',
        ];
    }
}

export default new ConfigQuery();

import { CheckoutQuery as SourceCheckoutQuery } from 'SourceQuery/Checkout.query';

import ProductListQuery from 'Query/ProductList.query';
import { isSignedIn } from 'Util/Auth';
import { Field } from 'Util/Query';

class CheckoutQuery extends SourceCheckoutQuery {
    _getOrderField() {
        return new Field('order').addFieldList([
            'order_id',
            this._getShippingInfo(),
            this._getBaseOrderInfo(),
            this._getPaymentInfo(),
            this._getAddress('billing_address'),
            this.getOrderProducts(),
            'payment_redirect_url',
        ]);
    }

    _getTotalsFields() {
        return [
            'subtotal',
            'tax_amount',
            'base_grand_total',
            'grand_total',
            'discount_amount',
            'subtotal_incl_tax',
            'shipping_incl_tax',
            'quote_currency_code',
            'shipping_tax_amount',
            'subtotal_with_discount',
            'shipping_discount_amount',
            this._getTotalItemField(),
        ];
    }

    _getShippingInfo() {
        return new Field('shipping_info').addFieldList([
            'shipping_incl_tax',
            'shipping_method',
            'shipping_description',
            'tracking_numbers',
            this._getAddress('shipping_address'),
        ]);
    }

    _getPaymentInfo() {
        return new Field('payment_info').addFieldList(['method', this._getPaymentAdditionalInfo()]);
    }

    _getPaymentAdditionalInfo() {
        return new Field('additional_information').addFieldList(['bank', 'method_title', 'credit_type', 'month']);
    }

    _getPaymentMethodFields() {
        return ['code', 'title', 'instructions'];
    }

    _getAddress(fieldName) {
        return new Field(fieldName).addFieldList([
            'city',
            'company',
            'firstname',
            'lastname',
            'middlename',
            'telephone',
            'district',
            'house_number',
            'apartment_number',
            'postomat_code',
            'store_pickup_code',
            'post_office_code',
            'postcode',
            'street',
            'is_b2b',
            'region',
            'organizationname',
            'organizationbin',
            'organizationaddress',
            'organizationiic',
            'organizationbik',
        ]);
    }

    _getBaseOrderInfo() {
        return new Field('base_order_info').addFieldList([
            'id',
            'increment_id',
            'created_at',
            'grand_total',
            'sub_total',
            'status',
            'status_label',
            'total_qty_ordered',
        ]);
    }

    _getSaveAddressInformationFields() {
        return [this._getPaymentMethodsField(), this._getTotalsField()];
    }

    getPlaceP24OrderMutation(guestCartId) {
        const mutation = new Field('s_placeOrder')
            .setAlias('placeOrder')
            .addField(
                new Field('order').addFieldList([
                    'order_id',
                    this._getShippingInfo(),
                    this._getBaseOrderInfo(),
                    this._getPaymentInfo(),
                    this._getAddress('billing_address'),
                    this.getOrderProducts(),
                    'order_number',
                    'payment_redirect_url',
                ]),
            );

        if (!isSignedIn()) {
            mutation.addArgument('guestCartId', 'String', guestCartId);
        }

        return mutation;
    }

    getEstimateShippingCosts(address, guestCartId) {
        const mutation = new Field('estimateShippingCosts')
            .addArgument('address', 'EstimateShippingCostsAddress!', address)
            .addField(new Field('pickup_points').addFieldList(['address', 'code']))
            .addFieldList(this._getEstimatedShippingFields());

        this._addGuestCartId(guestCartId, mutation);

        return mutation;
    }

    getOrderProducts() {
        return new Field('order_products')
            .addFieldList([
                'name',
                'qty',
                'sku',
                'original_price',
                'special_price',
                ProductListQuery._getMediaGalleryField(),
            ])
            .addField(new Field('attributes_info').addFieldList(['label', 'value']))
            .addField(new Field('thumbnail').addFieldList(['label', 'url']));
    }
}

export default new CheckoutQuery();

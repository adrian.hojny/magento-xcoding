import { CmsPageQuery as SourceCmsPageQuery } from 'SourceQuery/CmsPage.query';

import { Field } from 'Util/Query';

/**
 * CMS Page Query
 * @class CmsPageQuery
 */
export class CmsPageQuery extends SourceCmsPageQuery {
    /**
     * get CMS Page query
     * @param  {{url_key: String, title: Int, content: String, content_heading: String, page_layout: String, meta_title: String, meta_description: String, meta_keywords, string}} options A object containing different aspects of query, each item can be omitted
     * @return {Query} CMS Page query
     * @memberof CmsPageQuery
     */
    // getQuery({ id, url_key, identifier }) {
    //     if (!id && !url_key && !identifier) {
    //         throw new Error('Missing argument `id` or `url_key`!');
    //     }
    //
    //     const cmsPage = new Field('cmsPage')
    //         .addFieldList(this._getPageFields());
    //
    //     if (identifier) {
    //         cmsPage.addArgument('identifier', 'String!', identifier);
    //     } else if (id) {
    //         cmsPage.addArgument('id', 'Int!', id);
    //     }
    //
    //     return cmsPage;
    // }
    _getHreflangFields() {
        return new Field('items').addFieldList(['href', 'hreflang']);
    }

    getPageHreflangsQuery(type) {
        return new Field('mfAlternateHreflang')
            .addArgument('id', 'String', '')
            .addArgument('type', 'String', type)
            .addField(this._getHreflangFields());
    }


    _getPageFields() {
        return [
            'title',
            'content',
            'page_width',
            'content_heading',
            'meta_title',
            'meta_description',
            'meta_keywords',
            'scandipwa_modifiers',
        ];
    }
}

export default new CmsPageQuery();

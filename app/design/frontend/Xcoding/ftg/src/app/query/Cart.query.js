import { CartQuery as SourceCartQuery } from 'SourceQuery/Cart.query';

class CartQuery extends SourceCartQuery {
    _getCartTotalsFields() {
        return [
            'subtotal',
            'subtotal_incl_tax',
            'discounted_subtotal_incl_tax',
            'items_qty',
            'tax_amount',
            'grand_total',
            'discount_amount',
            'quote_currency_code',
            'subtotal_with_discount',
            'coupon_code',
            'is_virtual',
            this._getCartItemsField(),
        ];
    }
}

export default new CartQuery();

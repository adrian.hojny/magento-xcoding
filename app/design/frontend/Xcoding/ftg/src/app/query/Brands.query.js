import { Field } from 'Util/Query';

export class BrandsQuery {
    getBrandsQuery() {
        return new Field('brands').addFieldList(this._getDefaultFields());
    }

    getBrandSizingPageQuery(brandName) {
        return new Field('brands')
            .addArgument('url_alias', 'String!', brandName)
            .addFieldList([
                'meta_title',
                'title',
                'small_image_alt',
                'slider_image',
                'url_alias',
                'measure_description',
                'measure_men_img_url',
                'measure_women_img_url',
                'measure_junior_img_url',
                'measure_unisex_img_url',
                this.getSizingFields(),
            ]);
    }

    getBrandPageQuery(brandName) {
        return new Field('brands')
            .addArgument('url_alias', 'String!', brandName)
            .addFieldList(this._getBrandPageFields())
            .addField(this._getAlternateHreflangFields());
    }

    getSizingFields() {
        return new Field('sizing').addFieldList([
            'id',
            'title',
            'content',
            'content_json',
            'type',
            'block_title',
            'block_mobile_image',
            'block_image',
            'block_url',
        ]);
    }

    _getDefaultFields() {
        return ['is_featured', 'title', 'url_alias', 'url_key'];
    }

    _getBrandPageFields() {
        return [
            'description',
            'image_url',
            'mobile_image_url',
            'tablet_image_url',
            'is_featured',
            'meta_description',
            'meta_keywords',
            'meta_title',
            'short_description',
            'small_image_alt',
            'title',
            'url_alias',
            'slider_image',
            'bottom_video_url',
            'bottom_image_url',
            'bottom_description',
            'box_1_image_url',
            'box_1_name',
            'box_1_url',
            'box_2_image_url',
            'box_2_name',
            'box_2_url',
            'box_3_image_url',
            'box_3_name',
            'box_3_url',
            's1_sort_order',
            's2_sort_order',
            's2_title',
            's2_visible',
            's3_html',
            's3_sort_order',
        ];
    }

    _getAlternateHreflangFields() {
        return new Field('mf_alternate_urls').addFieldList(['href', 'hreflang']);
    }
}

export default new BrandsQuery();

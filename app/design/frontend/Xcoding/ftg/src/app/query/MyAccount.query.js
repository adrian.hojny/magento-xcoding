import { MyAccountQuery as SourceMyAccountQuery } from 'SourceQuery/MyAccount.query';

import { getGuestCartId } from 'Util/Cart';
import { Field } from 'Util/Query';

class MyAccountQuery extends SourceMyAccountQuery {
    getCreateAccountMutation(options) {
        const { customer, password, gRecaptchaResponse } = options;

        return new Field('createCustomer')
            .addArgument('input', 'CustomerInput!', { ...customer, password, gRecaptchaResponse })
            .addField(this._getCustomerField());
    }

    /**
     * Get SignIn mutation
     * @param {{email: String, password: String}} options A object containing different aspects of query, each item can be omitted
     * @return {Field}
     * @memberof MyAccount
     */
    getSignInMutation(options) {
        const { email, password } = options;
        const guestQuoteId = getGuestCartId();

        return new Field('generateCustomerToken')
            .addArgument('email', 'String!', email)
            .addArgument('password', 'String!', password)
            .addArgument('guest_quote_id', 'String!', guestQuoteId)
            .addField('token');
    }
}

export default new MyAccountQuery();

import { SliderQuery as SourceSliderQuery } from 'SourceQuery/Slider.query';

export class SliderQuery extends SourceSliderQuery {
    _getSlideFields() {
        return [
            'slide_text',
            'slide_link',
            'slide_id',
            'mobile_image',
            'tablet_image',
            'desktop_image',
            'title',
            'is_active',
        ];
    }
}

export default new SliderQuery();

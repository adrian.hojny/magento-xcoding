import ProductListQuery from 'Query/ProductList.query';
import { isSignedIn } from 'Util/Auth';
import { getGuestCartId } from 'Util/Cart';
import { Field } from 'Util/Query';

export class WishlistQuery {
    getWishlistQuery(sharingCode) {
        const field = new Field('wishlist').addFieldList(this._getWishlistFields());

        if (sharingCode) {
            field.addArgument('sharing_code', 'ID', sharingCode);
        }

        return field;
    }

    getSaveWishlistItemMutation(wishlistItem) {
        return new Field('saveWishlistItem')
            .addArgument('wishlistItem', 'WishlistItemInput!', wishlistItem)
            .addFieldList(this._getItemsFields());
    }

    getShareWishlistMutation(input) {
        return new Field('shareWishlist').addArgument('input', 'ShareWishlistInput!', input);
    }

    getClearWishlist() {
        return new Field('clearWishlist');
    }

    getMoveWishlistToCart(sharingCode) {
        const field = new Field('moveWishlistToCart');

        if (sharingCode) {
            field.addArgument('sharingCode', 'ID', sharingCode);

            if (!isSignedIn()) {
                const guestQuoteId = getGuestCartId();
                field.addArgument('guestCartId', 'ID', guestQuoteId);
            }
        }

        return field;
    }

    getRemoveProductFromWishlistMutation(item_id) {
        return new Field('removeProductFromWishlist').addArgument('itemId', 'ID!', item_id);
    }

    _getWishlistFields() {
        return ['updated_at', 'items_count', 'creators_name', this._getItemsField()];
    }

    _getItemsFields() {
        return ['id', 'sku', 'qty', 'description', this._getProductField()];
    }

    _getProductField() {
        return new Field('product').addFieldList(ProductListQuery._getProductInterfaceFields());
    }

    _getItemsField() {
        return new Field('items').addFieldList(this._getItemsFields());
    }
}

export default new WishlistQuery();

import { Field } from 'Util/Query';

export class CategoryQuery {
    constructor() {
        this.options = {};
    }

    getQuery(options = {}) {
        this.options = options;

        return new Field('category')
            .addArgument(...this._getConditionalArguments())
            .addFieldList(this._getDefaultFields())
            .addField(this._getChildrenFields())
            .addField(this._getAlternateHreflangFields());
    }

    _getConditionalArguments() {
        const { categoryUrlPath, categoryIds } = this.options;

        if (categoryUrlPath) {
            return ['url_path', 'String!', categoryUrlPath];
        }
        if (categoryIds) {
            return ['id', 'Int!', categoryIds];
        }
        throw new Error(__('There was an error requesting the category'));
    }

    _getChildrenFields() {
        return new Field('children').addFieldList(this._getDefaultFields());
    }

    _getBreadcrumbsField() {
        return new Field('breadcrumbs').addFieldList(this._getBreadcrumbFields());
    }

    _getBreadcrumbFields() {
        return ['category_name', 'category_level', 'category_url', 'category_is_active'];
    }

    _getCmsBlockFields() {
        return ['content', 'disabled', 'title', 'identifier'];
    }

    _getCmsBlockField() {
        return new Field('cms_block').addFieldList(this._getCmsBlockFields());
    }

    _getAlternateHreflangFields() {
        return new Field('mf_alternate_urls').addFieldList(['href', 'hreflang']);
    }

    _getDefaultFields() {
        return [
            'id',
            'url',
            'name',
            'image',
            'url_key',
            'url_path',
            'is_active',
            'meta_title',
            'description',
            'canonical_url',
            'product_count',
            'meta_keywords',
            'default_sort_by',
            'meta_description',
            'landing_page',
            this._getCmsBlockField(),
            this._getBreadcrumbsField(),
        ];
    }
}

export default new CategoryQuery();

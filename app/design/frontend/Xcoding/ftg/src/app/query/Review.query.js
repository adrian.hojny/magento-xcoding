import { ReviewQuery as SourceReviewQuery } from 'SourceQuery/Review.query';

import { Field } from 'Util/Query';

class ReviewQuery extends SourceReviewQuery {
    getAddReviewHelpfulnessVoteMutation(vote, reviewId) {
        return new Field('addReviewHelpfulnessVote')
            .addArgument('vote', 'VoteEnum!', vote)
            .addArgument('reviewId', 'Int!', reviewId)
            .addFieldList(['yes', 'no']);
    }
}

export default new ReviewQuery();

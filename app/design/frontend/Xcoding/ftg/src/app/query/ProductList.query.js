import { ProductListQuery as SourceProductListQuery } from 'SourceQuery/ProductList.query';

import { CUSTOMER } from 'Store/MyAccount/MyAccount.dispatcher';
import BrowserDatabase from 'Util/BrowserDatabase';
import { Field, Fragment } from 'Util/Query';

export class ProductListQuery extends SourceProductListQuery {
    getQuery(options) {
        if (!options) {
            throw new Error('Missing argument `options`');
        }

        this.options = options;

        if (options.useProductPageRequest) {
            return this.getProductPageFields();
        }

        if (options.useProductListRequest) {
            return this.getProductListFields();
        }

        return this._getProductsField();
    }

    _getStockItemDetails() {
        return new Field('stock_item').addFieldList(['max_sale_qty', 'min_sale_qty', 'qty']);
    }

    _getProductInterfaceFields(isVariant, isForLinkedProducts = false) {
        const { isSingleProduct } = this.options;

        return [
            ...super._getProductInterfaceFields(isVariant, isForLinkedProducts),
            ...(isSingleProduct
                ? [
                      this._getProductDeliveryDate(),
                      this._getBrandDetailsField(),
                      this._getSizingDetails(),
                      this._getCommingSoon(),
                      this._getCommingSoonDate(),
                  ]
                : [
                      this._getBrandDetailsField(),
                      this._getSizingDetails(),
                      this._getCommingSoon(),
                      this._getCommingSoonDate(),
                  ]),
            'is_low_in_stock',
            'salable_qty',
            this._getStockItemDetails(),
            'model_story',
        ];
    }

    _getProductDeliveryDate() {
        return new Field('delivery_date')
            .addArgument('formats', '[DeliveryDateFormat]', ['ISO_DATE'])
            .addFieldList(['format', 'value']);
    }

    _getItemsField() {
        const { isSingleProduct } = this.options;
        const items = new Field('items').addFieldList(this._getProductInterfaceFields());

        if (isSingleProduct) {
            items.addField(this._getAlternateHreflangsFields());
            items.addField(this._getGroupedProductItems());
        }

        return items;
    }

    _getProductFields() {
        const { requireInfo, isSingleProduct, notRequireInfo } = this.options;

        // do not request total count for PDP
        if (isSingleProduct || notRequireInfo) {
            return [this._getItemsField()];
        }

        // for filters only request
        if (requireInfo) {
            return [this._getSortField(), this._getAggregationsField(), this._getMinPrice(), this._getMaxPrice()];
        }

        return ['total_count', this._getItemsField(), this._getPageInfoField()];
    }

    _getMinPrice() {
        return new Field('min_price');
    }

    _getMaxPrice() {
        return new Field('max_price');
    }

    _getCommingSoon() {
        return new Field('comming_soon');
    }

    _getCommingSoonDate() {
        return new Field('comming_soon_date');
    }

    _getSizingDetails() {
        return new Field('sizing_details').addFieldList(['content', 'title']);
    }

    encodeSpecialChar = (sku) => {
        const regExpComma = /\,/g;
        const regExpColon = /\:/g;
        let fixedSku = sku;

        if (typeof sku === 'string') {
            fixedSku = sku.replace(regExpComma, '\\,').replace(regExpColon, '\\:');
        } else {
            fixedSku = sku.map((item) => item.replace(regExpComma, '\\,').replace(regExpColon, '\\:'));
        }

        return fixedSku;
    };

    _getFilterArgumentMap() {
        return {
            categoryIds: (id) => [`category_id: { eq: ${id} }`],
            categoryUrlPath: (url) => [`category_url_path: { eq: ${url} }`],
            priceRange: ({ min, max }) => {
                const filters = [];

                if (min && !max) {
                    filters.push(`price: { from: ${min} }`);
                } else if (!min && max) {
                    filters.push(`price: { to: ${max} }`);
                } else if (min && max) {
                    filters.push(`price: { from: ${min}, to: ${max} }`);
                }

                return filters;
            },
            productsSkuArray: (sku) => [`sku: { in: [${encodeURIComponent(this.encodeSpecialChar(sku))}] }`],
            productSKU: (sku) => [`sku: { eq: ${encodeURIComponent(this.encodeSpecialChar(sku))} }`],
            productUrlPath: (url) => [`url_key: { eq: ${url}}`],
            customFilters: this._getCustomFilters,
            newToDate: (date) => [`news_to_date: { gteq: ${date} }`],
            conditions: (conditions) => [`conditions: { eq: ${conditions} }`],
            customerGroupId: (id) => [`customer_group_id: { eq: ${id} }`],
            attribute: ({ name, value }) => [`${name}: { finset: ${value} }`],
            brandName: (name) => [`brand_url_alias: { eq: "${name}" }`],
        };
    }

    _getArgumentsMap() {
        const { requireInfo } = this.options;
        const filterArgumentMap = this._getFilterArgumentMap();

        return {
            currentPage: { type: 'Int!' },
            pageSize: {
                type: 'Int!',
                handler: (option) => (requireInfo ? 1 : option),
            },
            search: {
                type: 'String!',
                handler: (option) => encodeURIComponent(option),
            },
            sort: {
                type: 'ProductAttributeSortInput!',
                handler: ({ sortKey, sortDirection }) => `{${sortKey}: ${sortDirection || 'ASC'}}`,
            },
            filter: {
                type: 'ProductAttributeFilterInput!',
                handler: (initialOptions = {}) => {
                    // add customer group by default to all requests
                    const { group_id } = BrowserDatabase.getItem(CUSTOMER) || {};
                    const options = {
                        ...initialOptions,
                        customerGroupId: group_id || '0',
                    };

                    const { customFilters: { category_id } = {} } = options;

                    /**
                     * Remove category ID from select, if there is a custom filter
                     * of category already selected in filtering options.
                     */
                    if (category_id) {
                        // eslint-disable-next-line fp/no-delete
                        options.categoryIds = undefined;
                    }

                    const parsedOptions = Object.entries(options).reduce((acc, [key, option]) => {
                        // if there is no value, or if the key is just not present in options object
                        if (!option || !filterArgumentMap[key]) {
                            return acc;
                        }

                        return [...acc, ...filterArgumentMap[key](option)];
                    }, []);

                    return `{${parsedOptions.join(',')}}`;
                },
            },
        };
    }

    _getReviewSummaryFields() {
        return [...super._getReviewSummaryFields(), this._getExtendedRatingSummary()];
    }

    _getExtendedRatingSummary() {
        return new Field('extended_rating_summary').addFieldList(['code', 'value', 'value_count', 'product_id']);
    }

    _getReviewFields() {
        return [...super._getReviewFields(), this._getHelpfulness(), this._getExtendedRating()];
    }

    _getBrandDetailsField() {
        return new Field('brand_details').addFieldList(['title', 'url_alias', 'url_key']);
    }

    _getExtendedRating() {
        return new Field('extended_rating').addFieldList(['recommended_value']);
    }

    _getHelpfulness() {
        return new Field('helpfulness').addFieldList(['yes', 'no']);
    }

    _getMediaSmallField() {
        return new Field('small').addField('url');
    }

    _getMediaGalleryFields() {
        return [
            'id',
            'file',
            'label',
            'position',
            'disabled',
            'media_type',
            'types',
            this._getVideoContentField(),
            this._getMediaThumbnailField(),
            this._getMediaBaseField(),
            this._getMediaSmallField(),
        ];
    }

    _getAlternateHreflangsFields() {
        return new Field('mf_alternate_urls').addFieldList(['href', 'hreflang']);
    }

    getProductPageFields() {
        const items = new Field('items');

        items.addFieldList([
            'id',
            'sku',
            'name',
            'type_id',
            'stock_status',
            this._getBrandDetailsField(),
            this._getPriceRangeField(),
            this._getProductThumbnailField(),
            this._getProductSmallField(),
            this._getShortDescriptionField(),
            this._getSizingDetails(),
            'url',
            this._getReviewSummaryField(),
            this._getBundleProductFragment(),
            'meta_title',
            'meta_keyword',
            'canonical_url',
            'meta_description',
            'salable_qty',
            this._getDescriptionField(),
            this._getMediaGalleryField(),
            this._getSimpleProductFragment(),
            this._getProductLinksField(),
            this._getCategoriesField(),
            this._getReviewsField(),
            this._getVirtualProductFragment(),
            this._getCustomizableProductFragment(),
            this._getAlternateHreflangsFields(),
            this._getAttributesField(false),
            new Fragment('ConfigurableProduct').addFieldList([
                this._getConfigurableOptionsField(),
                new Field('variants').addFieldList([
                    new Field('product').addFieldList([
                        'id',
                        'type_id',
                        'salable_qty',
                        'stock_status',
                        this._getPriceRangeField(),
                        this._getAttributesField(true),
                    ]),
                ]),
            ]),
        ]);

        const products = new Field('products').addFieldList([items]);

        this._getProductArguments().forEach((arg) => products.addArgument(...arg));

        return products;
    }

    getProductListFields() {
        const items = new Field('items');

        items.addFieldList([
            'id',
            'sku',
            'name',
            'type_id',
            'stock_status',
            this._getBrandDetailsField(),
            this._getPriceRangeField(),
            this._getProductThumbnailField(),
            this._getProductSmallField(),
            'special_from_date',
            'special_to_date',
            this._getTierPricesField(),
            'url',
            this._getBundleProductFragment(),
            'salable_qty',
            this._getSimpleProductFragment(),
            this._getVirtualProductFragment(),
            this._getCustomizableProductFragment(),
            this._getAttributesField(false),
            new Fragment('ConfigurableProduct').addFieldList([
                this._getConfigurableOptionsField(),
                new Field('variants').addFieldList([
                    new Field('product').addFieldList(['id', this._getPriceRangeField()]),
                ]),
            ]),
        ]);

        const products = new Field('products').addFieldList(['total_count', this._getPageInfoField(), items]);

        this._getProductArguments().forEach((arg) => products.addArgument(...arg));

        return products;
    }
}

export default new ProductListQuery();

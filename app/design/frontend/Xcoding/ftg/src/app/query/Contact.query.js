import { Field } from 'Util/Query';

export class ContactQuery {
    submitContactForm({ name, comment, topic, email }) {
        const mutation = new Field('contactFormSend')
            .addArgument('name', 'String!', name)
            .addArgument('comment', 'String!', comment)
            .addArgument('topic', 'String!', topic)
            .addArgument('email', 'String!', email);

        return mutation;
    }
}

export default new ContactQuery();

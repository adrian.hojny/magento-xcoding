import {
    checkEveryOption,
    getExtensionAttributes,
    getIndexedAttributeOption,
    getIndexedAttributes,
    getIndexedConfigurableOptions,
    getIndexedParameteredProducts,
    getIndexedProduct as sourceGetIndexedProduct,
    getIndexedProducts,
    getIndexedVariants,
    getVariantIndex,
    getVariantsIndexes,
} from 'SourceUtil/Product/Product';

import { COMFORT_FIELD_NAME, QUALITY_FIELD_NAME, SIZE_FIELD_NAME } from 'Type/Rating';

export const getIndexedProduct = (product) => {
    const { review_summary } = product;

    const INIT_RATING = {
        [COMFORT_FIELD_NAME]: {},
        [QUALITY_FIELD_NAME]: {},
        [SIZE_FIELD_NAME]: {},
    };

    const rating = (review_summary.extended_rating_summary || []).reduce(
        (acc, { code, value, value_count }) => ({ ...acc, [code]: { ...acc[code], [value]: value_count } }),
        INIT_RATING,
    );

    return {
        ...sourceGetIndexedProduct(product),
        review_summary: {
            ...review_summary,
            extended_rating_summary: rating,
        },
    };
};

const _getVariantIndex = (item) => {
    const { sku: itemSku, product: { variants = [] } = {} } = item;

    return variants.findIndex(({ sku }) => sku === itemSku || itemSku.includes(sku));
};

export const getCurrentProduct = (item) => {
    const variantIndex = _getVariantIndex(item);

    return variantIndex < 0 ? item.product : item.product.variants[variantIndex];
};

export const getProductVariant = (item, sizeAttributeValue) => {
    return item.product.attributes?.shoes_size?.attribute_options[sizeAttributeValue]?.label || '';
};

export const getProductsForGTM = (items, data) => {
    return items.map((item, index) => {
        const product = getCurrentProduct(item);
        const sizeAttributeValue = product?.attributes?.shoes_size?.attribute_value || null;
        const size = getProductVariant(item, sizeAttributeValue);

        return {
            id: item?.product?.sku,
            name: item?.product?.name || '',
            // "list_name": "Search Results",
            brand: item?.product?.brand_details?.url_alias || '',
            category: data[item.product.id] || '',
            variant: size || '',
            list_position: index + 1,
            quantity: item.qty,
            price: product?.price_range?.minimum_price?.final_price?.value,
        };
    });
};

export const getConfigurableVariantIndex = (sku, variants) =>
    Object.keys(variants).find((i) => variants[i].sku === sku);

export {
    checkEveryOption,
    getExtensionAttributes,
    getIndexedAttributeOption,
    getIndexedAttributes,
    getIndexedConfigurableOptions,
    getIndexedParameteredProducts,
    getIndexedProducts,
    getIndexedVariants,
    getVariantIndex,
    getVariantsIndexes,
};

export const ONE_FIFTH_OF_A_HUNDRED = 20;

export const getRating = (ratingSummary, fractionDigits = 1) =>
    (ratingSummary / ONE_FIFTH_OF_A_HUNDRED).toFixed(fractionDigits);

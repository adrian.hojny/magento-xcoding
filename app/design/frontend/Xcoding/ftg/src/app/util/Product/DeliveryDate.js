export const ONE_DAY_IN_MS = 86400000;

export const getDaysDifferenceFromNow = (date) => {
    const now = new Date();
    const diffTime = Math.abs(now - date);

    return Math.ceil(diffTime / ONE_DAY_IN_MS);
};

export const getFormattedDateString = (date) => {
    const day = date.getDate();
    const month = date.getMonth() + 1;

    return `${day}.${month}`;
};

/* eslint-disable no-magic-numbers */
export const getDayOfTheWeekString = (date) => {
    const dayOfTheWeek = date.getDay();

    switch (dayOfTheWeek) {
        case 0:
            return __('on Sunday');
        case 1:
            return __('on Monday');
        case 2:
            return __('on Tuesday');
        case 3:
            return __('on Wednesday');
        case 4:
            return __('on Thursday');
        case 5:
            return __('on Friday');
        case 6:
            return __('on Saturday');
        default:
            return '';
    }
};
/* eslint-enable no-magic-numbers */

export const getDeliveryDateString = (date) => {
    const differenceInDays = getDaysDifferenceFromNow(date);

    switch (differenceInDays) {
        case 0:
            return __('today');
        case 1:
            return __('tomorrow');
        case 2:
            return __('day after tomorrow');
        default:
            return `${getDayOfTheWeekString(date)} (${getFormattedDateString(date)})`;
    }
};

export const getFormattedDeliveryDate = (valuesArr = []) => {
    const date1 = new Date(Date.parse(valuesArr[0]));
    const date2 = new Date(Date.parse(valuesArr[1]));

    if (Number.isNaN(date1) || Number.isNaN(date2)) return null;

    return `${getDeliveryDateString(date1)} / ${getDeliveryDateString(date2)}`;
};

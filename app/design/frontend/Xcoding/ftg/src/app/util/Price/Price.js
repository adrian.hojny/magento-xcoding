export * from 'SourceUtil/Price/Price';

export const roundPrice = (price) => parseFloat(price).toFixed(2).replace('.', ',');

/* eslint-disable no-magic-numbers */

export const getPluralForm = (number, singularTranslation, pluralTranslation1, pluralTranslation2) => {
    if (number === 1) return __(singularTranslation, number);

    const lastDigit = number % 10;

    if (lastDigit > 1 && lastDigit < 5) return __(pluralTranslation1, number);

    return __(pluralTranslation2, number);
};

/* eslint-enable no-magic-numbers */

export const getStaticFilePath = (path) => {
    if (path.match(/^data:/)) {
        return path;
    }
    let newPath = path;

    if (newPath.charAt(0) === '/') {
        newPath = newPath.substring(1);
    }

    if (process.env.NODE_ENV === 'production') {
        return `/static/frontend/Xcoding/ftg/pl_PL/Magento_Theme/${newPath}`;
    }

    return `/${newPath}`;
};

export const getWebpImagePath = (imageUrl) => {
    let url = imageUrl.trim();
    const { protocol, origin: baseUrl } = window.location;

    // sometimes imageUrl is relative path and does not include the domain
    if (imageUrl.includes('/media') && !imageUrl.includes(`${protocol}//`)) {
        url = `${baseUrl}${url}`;
    }

    const sourceImageFormat = url.split('.').pop();
    const mediaBaseUrl = `${baseUrl}/media/`;
    const staticBaseUrl = `${baseUrl}/static/`;

    if (url.indexOf(mediaBaseUrl) === -1 && url.indexOf(staticBaseUrl) === -1) {
        return false;
    }

    let webpImagePath = url;
    webpImagePath = webpImagePath.replace(mediaBaseUrl, 'media/');
    webpImagePath = webpImagePath.replace(staticBaseUrl, 'static/');
    webpImagePath = webpImagePath.replace(`${baseUrl}/media/`, 'media/');
    webpImagePath = webpImagePath.replace(`${baseUrl}/static/`, 'static/');
    webpImagePath = webpImagePath.replace(/\.(jpg|jpeg|png|JPG|JPEG|PNG|gif|GIF)/i, '.webp');
    webpImagePath = `${mediaBaseUrl}mf_webp/${sourceImageFormat}/${webpImagePath}`;
    webpImagePath = webpImagePath.replace('%20', ' ');

    return { webpImagePath, sourceImageFormat };
};

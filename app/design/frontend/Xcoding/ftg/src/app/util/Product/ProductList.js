import ProductListQuery from 'Query/ProductList.query';
import { Field, prepareQuery } from 'Util/Query';
import { ONE_MONTH_IN_SECONDS } from 'Util/Request/QueryDispatcher';
import { executeGet } from 'Util/Request/Request';

export const getProductList = (options, name = 'product-list') => {
    const rawQueries = ProductListQuery.getQuery(options);
    const queries = rawQueries instanceof Field ? [rawQueries] : rawQueries;

    return executeGet(prepareQuery(queries), name, ONE_MONTH_IN_SECONDS);
};

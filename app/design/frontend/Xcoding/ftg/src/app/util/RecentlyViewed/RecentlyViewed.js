import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';

const ONE_YEAR_IN_SECONDS = 2630000;
const RECENTLY_STORAGE = 'recentlyViewed';
const MAX_ITEMS = 50;

/**
 * Set of helpers to check local Recently Viewed Products
 */
class RecentlyViewed {
    getLocalRecentlyViewed() {
        return BrowserDatabase.getItem(RECENTLY_STORAGE);
    }

    updateLocalRecentlyViewed(productSku) {
        const recentlyViewed = this.getLocalRecentlyViewed();

        if (!recentlyViewed) {
            const products = [{ sku: productSku }];
            BrowserDatabase.setItem(products, RECENTLY_STORAGE, ONE_YEAR_IN_SECONDS);
            return;
        }

        if (recentlyViewed.length >= MAX_ITEMS) {
            recentlyViewed.pop();
        }
        recentlyViewed.unshift({ sku: productSku });
        BrowserDatabase.setItem(recentlyViewed, RECENTLY_STORAGE, ONE_YEAR_IN_SECONDS);
    }

    existProductInRecentlyViewed(productSku) {
        if (this.getLocalRecentlyViewed()) {
            return this.getLocalRecentlyViewed().some((elem) => elem.sku === productSku);
        }

        return false;
    }
}

export default new RecentlyViewed();

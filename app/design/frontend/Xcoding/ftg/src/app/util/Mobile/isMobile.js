import sourceIsMobile from 'SourceUtil/Mobile/isMobile';

const MOBILE_WIDTH = 767;
const TABLET_WIDTH = 1024;

const isMobile = {
    ...sourceIsMobile,
    width: () => window.innerWidth <= MOBILE_WIDTH,
    tabletWidth: () => window.innerWidth <= TABLET_WIDTH,
};

export default isMobile;

import { getStore } from 'Store';
import { GUEST_QUOTE_ID } from 'Store/Cart/Cart.dispatcher';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';

export const getGuestCartId = () => {
    const { ConfigReducer: { code = '' } = {} } = getStore().getState() || {};
    const cartID =
        code !== '' ? BrowserDatabase.getItem(`${GUEST_QUOTE_ID}_${code}`) : BrowserDatabase.getItem(GUEST_QUOTE_ID);

    return cartID;
};

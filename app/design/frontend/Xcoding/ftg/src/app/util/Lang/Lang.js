export const getLangPrefix = (url = window.location.href) => {
    const regexpText = `\/(at|bg|cz|de|en|es|fr|hr|hu|it|nl|pl|ru|si|sk|ua)`;
    const customRegexp = `${regexpText}\/`;
    const regexp = new RegExp(customRegexp);
    const match = regexp.exec(url);
    let lang = match?.[1] || 'en';

    if (!match) {
        const lastCharacters = url.slice(-3);
        const regexp2 = new RegExp(regexpText);
        const match2 = regexp2.exec(lastCharacters);

        lang = match2?.[1] || 'en';
    }

    return lang;
};

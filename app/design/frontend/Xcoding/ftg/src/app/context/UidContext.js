import React, { createContext, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { v4 as uuidv4 } from 'uuid';

export const UidContext = createContext();

export const UidProvider = ({ children }) => {
    const [uid] = useState(uuidv4());
    return <UidProviderInner uid={uid}>{children}</UidProviderInner>;
};

const UidProviderInner = memo(({ children, uid }) => (
    <UidContext.Provider value={{ uid }}>{children}</UidContext.Provider>
));

UidProvider.propTypes = {
    children: PropTypes.node.isRequired,
};

UidProviderInner.propTypes = {
    ...UidProvider.propTypes,
    uid: PropTypes.string.isRequired,
};

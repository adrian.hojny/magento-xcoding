import React, { createContext, memo, useState } from 'react';
import PropTypes from 'prop-types';

export const ThemeContext = createContext();

export const ThemeProvider = ({ children }) => {
    const [theme, setTheme] = useState({
        header: 'default',
    });

    const handleSetTheme = (themeDiff) => {
        const newState = {
            ...theme,
            ...themeDiff,
        };

        setTheme(newState);
    };

    // eslint-disable-next-line react/jsx-no-bind
    return (
        <ThemeProviderInner theme={theme} setTheme={handleSetTheme}>
            {children}
        </ThemeProviderInner>
    );
};

const ThemeProviderInner = memo(({ children, theme, setTheme }) => (
    <ThemeContext.Provider value={{ theme, setTheme }}>{children}</ThemeContext.Provider>
));

ThemeProvider.propTypes = {
    children: PropTypes.node.isRequired,
};

ThemeProviderInner.propTypes = {
    ...ThemeProvider.propTypes,
    theme: PropTypes.shape({
        header: PropTypes.string,
    }).isRequired,
    setTheme: PropTypes.func.isRequired,
};

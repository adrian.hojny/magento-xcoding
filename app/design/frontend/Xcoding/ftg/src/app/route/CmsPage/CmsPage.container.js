import { connect } from 'react-redux';

import {
    CmsPageContainer as SourceCmsPageContainer,
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
} from 'SourceRoute/CmsPage/CmsPage.container';

import { CMS_PAGE } from 'Component/Header/Header.config';
import CmsPageQuery from 'Query/CmsPage.query';
import { removeLinks, updateLinks } from 'Store/Hreflinks/Hreflinks.actions';
import history from 'Util/History';
import { debounce, fetchQuery } from 'Util/Request';
import { appendWithStoreCode } from 'Util/Url';
import { LOADING_TIME } from './CmsPage.config';

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    cms_home_page: state.ConfigReducer.cms_home_page,
    header_logo_src: state.ConfigReducer.header_logo_src,
    default_country: state.ConfigReducer.default_country,
});

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    updateLinks: (links) => dispatch(updateLinks(links)),
    removeLinks: () => dispatch(removeLinks()),
});
export class CmsPageContainer extends SourceCmsPageContainer {
    updateHreflangs = (title) => {
        fetchQuery(CmsPageQuery.getPageHreflangsQuery(title)).then((data) => {
            const items = data?.mfAlternateHreflang?.items || [];
            this.props.updateLinks(items);
        });
    };

    onPageLoad = ({ cmsPage: page }) => {
        const {
            location: { pathname },
            updateMeta,
            setHeaderState,
            updateBreadcrumbs,
            pageIdentifiers,
            cms_home_page,
        } = this.props;

        const { content_heading, meta_title, title, meta_description } = page;

        debounce(this.setOfflineNoticeSize, LOADING_TIME)();

        updateBreadcrumbs(page);

        const params = this.getRequestQueryParams();
        const { identifier } = params;

        if (identifier) {
            this.updateHreflangs(identifier);
        }

        const canonical_url = pageIdentifiers === cms_home_page ? location.href : null;
        updateMeta({ title: meta_title || title, description: meta_description, canonical_url });

        if (pathname !== appendWithStoreCode('/') && pathname !== '/') {
            setHeaderState({
                name: CMS_PAGE,
                title: content_heading,
                onBackClick: () => history.goBack(),
            });
        }

        this.setState({ page, isLoading: false });
    };

    componentDidUpdate(prevProps) {
        const {
            location: { pathname },
            pageIdentifiers,
            pageIds,
            removeLinks,
        } = this.props;

        const {
            location: { pathname: prevPathname },
            pageIdentifiers: prevPageIdentifiers,
            pageIds: prevPageIds,
        } = prevProps;

        if (pathname !== prevPathname || pageIds !== prevPageIds || pageIdentifiers !== prevPageIdentifiers) {
            removeLinks();
            this.requestPage();
        }
    }

    componentWillUnmount() {
        const { removeLinks } = this.props;

        removeLinks();
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CmsPageContainer);

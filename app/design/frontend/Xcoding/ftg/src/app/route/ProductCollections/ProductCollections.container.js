import { connect } from 'react-redux';

import BreadcrumbsDispatcher from 'Store/Breadcrumbs/Breadcrumbs.dispatcher';
import { updateMeta } from 'Store/Meta/Meta.action';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import ProductCollections from './ProductCollections.component';

export const mapStateToProps = (state) => ({
    productCollections: state.ProductsWithAttributeReducer.product_collection,
});

export const mapDispatchToProps = (dispatch) => ({
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    updateBreadcrumbs: (breadcrumbs) => BreadcrumbsDispatcher.update(breadcrumbs, dispatch),
    updateMeta: (meta) => dispatch(updateMeta(meta)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductCollections);

import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Image from 'Component/Image';
import Link from 'Component/Link/Link.component';
import isMobile from 'Util/Mobile/isMobile';

export default class SizingPageTable extends PureComponent {
    static propTypes = {
        data: PropTypes.object.isRequired,
        id: PropTypes.number.isRequired,
        index: PropTypes.number.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            isOpen: true,
        };
    }

    renderDesktopTable = ({ headers, rows }, id) => {
        return (
            <table block="SizingPage" elem="Table">
                <thead>
                    <tr>
                        {headers?.map((header, i) => (
                            <th key={header + i + id} block="SizingPage" elem="TableHead">
                                {header}
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody block="SizingPage" elem="TableBody">
                    {rows.map((row, i) => (
                        <tr key={`row${i + id}`}>
                            {row.map((cell, i) => (
                                <td key={cell + i + id} block="SizingPage" elem="Cell">
                                    {cell}
                                </td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        );
    };

    renderMobileTable = ({ headers, rows }, id) => {
        return (
            <div block="SizingPage" elem="TableMobileWrapper">
                <table block="SizingPage" elem="TableMobile">
                    <tbody block="SizingPage" elem="TableBody">
                        {headers?.map((header, headerIndex) => (
                            <tr key={header + headerIndex + id}>
                                <th block="SizingPage" elem="TableHead">
                                    {header}
                                </th>
                                {rows.map((row, i) => (
                                    <td key={row + i} block="SizingPage" elem="Cell">
                                        {row[headerIndex]}
                                    </td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    };

    renderSizing() {
        const { data, id, index } = this.props;
        const { title, content_json = {}, block_title, block_image, block_mobile_image, block_url } = data;
        const { isOpen } = this.state;
        const isMobileWidth = isMobile.width();

        return (
            <div key={title + index + id} block="SizingPage" elem="SizingContainer">
                <div block="SizingPage" elem="TableContainer">
                    <button
                        block="SizingPage"
                        elem="SizingHeader"
                        mods={{ isOpen }}
                        type="button"
                        onClick={() =>
                            isMobileWidth ? this.setState((prevState) => ({ isOpen: !prevState.isOpen })) : {}
                        }
                    >
                        {title}
                        <span block="SizingPage" elem="SizingHeaderMobileArrow" mods={{ isOpen }} />
                    </button>

                    {isOpen && (
                        <div block="SizingPage" elem="TableWrapper">
                            {isMobileWidth && content_json?.columns > 4
                                ? this.renderMobileTable(content_json, id)
                                : this.renderDesktopTable(content_json, id)}
                            {isMobileWidth && <p>{__('Scroll to see more')}</p>}
                        </div>
                    )}
                </div>
                <div block="SizingPage" elem="SizingImage">
                    {!isMobileWidth ? (
                        <Image src={block_image} alt={block_title} useWebp />
                    ) : (
                        <Image src={block_mobile_image} alt={block_title} useWebp />
                    )}
                    <div block="SizingPage" elem="CategoryLinkContainer">
                        <p block="SizingPage" elem="CategoryLinkTitle">
                            {block_title}
                        </p>
                        <Link to={block_url} className="SizingPage-BrandLink">
                            {__('Check it')}
                        </Link>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return this.renderSizing();
    }
}

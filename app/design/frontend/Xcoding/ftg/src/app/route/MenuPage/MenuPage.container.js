/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import { PureComponent } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';

import { MENU } from 'Component/Header/Header.config';
import MenuOverlay from 'Component/MenuOverlay/MenuOverlay.container';
import { updateMeta } from 'Store/Meta/Meta.action';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { hideActiveOverlay, toggleOverlayByKey } from 'Store/Overlay/Overlay.action';
import { HistoryType } from 'Type/Common';
import { isSignedIn } from 'Util/Auth';
import isMobile from 'Util/Mobile';
import { appendWithStoreCode } from 'Util/Url';

import './MenuPage.style';

export const mapDispatchToProps = (dispatch) => ({
    showOverlay: (overlayKey) => dispatch(toggleOverlayByKey(overlayKey)),
    hideActiveOverlay: () => dispatch(hideActiveOverlay()),
    updateMeta: (meta) => dispatch(updateMeta(meta)),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
});

export class MenuPageContainer extends PureComponent {
    static propTypes = {
        updateMeta: PropTypes.func.isRequired,
        history: HistoryType.isRequired,
        changeHeaderState: PropTypes.func.isRequired,
        showOverlay: PropTypes.func.isRequired,
        hideActiveOverlay: PropTypes.func.isRequired,
    };

    componentDidMount() {
        const { updateMeta, changeHeaderState, showOverlay, history } = this.props;

        if (!isMobile.tabletWidth() && !isMobile.any() && !isMobile.tablet()) {
            history.push('/');
        } else {
            updateMeta({ title: __('Menu') });
            showOverlay(MENU);
            changeHeaderState({
                name: MENU,
                title: 'Menu',
                onCloseClick: () => history.push({ pathname: appendWithStoreCode('/') }),
            });
        }
    }

    componentWillUnmount() {
        const { hideActiveOverlay } = this.props;
        hideActiveOverlay();
    }

    render() {
        return <MenuOverlay isSignedIn={isSignedIn()} />;
    }
}

export default withRouter(connect(null, mapDispatchToProps)(MenuPageContainer));

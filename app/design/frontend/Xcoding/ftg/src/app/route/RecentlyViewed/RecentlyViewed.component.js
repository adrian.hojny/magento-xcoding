import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ContentWrapper from 'Component/ContentWrapper';
import RecentlyViewed from 'Component/RecentlyViewed';

import './RecentlyViewedPage.style';

const RENDER_TYPE = 'listing';

class RecentlyViewedPage extends PureComponent {
    static propTypes = {
        updateBreadcrumbs: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        const { updateBreadcrumbs } = props;
        updateBreadcrumbs([{ url: '/recently-viewed', name: __('Recently Viewed') }]);
    }

    render() {
        return (
            <main block="RecentlyViewed">
                <ContentWrapper wrapperMix={{ block: 'RecentlyViewed', elem: 'Wrapper' }} label="Recently Viewed page">
                    <RecentlyViewed renderType={RENDER_TYPE} />
                </ContentWrapper>
            </main>
        );
    }
}

export default RecentlyViewedPage;

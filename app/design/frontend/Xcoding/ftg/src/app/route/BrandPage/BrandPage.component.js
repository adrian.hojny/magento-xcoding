import PropTypes from 'prop-types';

import BrandHeader from 'Component/BrandHeader/BrandHeader';
import CategoryDetails from 'Component/CategoryDetails';
import CategoryFilterOverlay from 'Component/CategoryFilterOverlay';
import { CATEGORY_FILTER_OVERLAY_ID } from 'Component/CategoryFilterOverlay/CategoryFilterOverlay.component';
import CategoryProductList from 'Component/CategoryProductList';
import CollectionSelector from 'Component/CollectionSelector';
import ContentWrapper from 'Component/ContentWrapper';
import { CategoryTreeType } from 'Type/Category';
import { FilterInputType, FilterType } from 'Type/ProductList';
import CategoryPage from '../CategoryPage/CategoryPage.component';

import './BrandPage.extend.style';

export default class BrandPage extends CategoryPage {
    static propTypes = {
        category: CategoryTreeType.isRequired,
        getIsNewCategory: PropTypes.func.isRequired,
        filters: PropTypes.objectOf(PropTypes.shape).isRequired,
        sortFields: PropTypes.shape({
            options: PropTypes.array,
        }).isRequired,
        selectedSort: PropTypes.shape({
            sortDirection: PropTypes.oneOf(['ASC', 'DESC']),
            sortKey: PropTypes.string,
        }).isRequired,
        getFilterUrl: PropTypes.func.isRequired,
        onSortChange: PropTypes.func.isRequired,
        updateFilter: PropTypes.func.isRequired,
        toggleOverlayByKey: PropTypes.func.isRequired,
        selectedFilters: FilterType.isRequired,
        filter: FilterInputType.isRequired,
        search: PropTypes.string.isRequired,
        isContentFiltered: PropTypes.bool,
        totalPages: PropTypes.number,
        isMatchingListFilter: PropTypes.bool,
        toggleBreadcrumbs: PropTypes.func.isRequired,
        brandDescriptionSuffix: PropTypes.string,
        defaultTitle: PropTypes.string,
    };

    static defaultProps = {
        isContentFiltered: true,
        totalPages: 1,
        isMatchingListFilter: false,
    };

    onFilterButtonClick = this.onFilterButtonClick.bind(this);

    onFilterButtonClick() {
        const { toggleOverlayByKey } = this.props;
        toggleOverlayByKey(CATEGORY_FILTER_OVERLAY_ID);
    }

    renderCategoryDetails() {
        const { category } = this.props;

        return <CategoryDetails category={category} />;
    }

    renderFilterButton() {
        const { isContentFiltered, totalPages } = this.props;

        if (!isContentFiltered && totalPages === 0) {
            return null;
        }

        return (
            <button block="BrandPage" elem="Filter" onClick={this.onFilterButtonClick}>
                {__('Filter')}
            </button>
        );
    }

    renderSubcategorySelector() {
        const { filters, match, selectedFilters, storeViewCode } = this.props;
        const { product_collection } = filters;

        if (!product_collection) {
            return null;
        }

        return (
            <CollectionSelector
                collection={product_collection}
                match={match}
                selectedFilters={selectedFilters}
                storeViewCode={storeViewCode}
            />
        );
    }

    renderFilterOverlay() {
        const {
            filters,
            selectedFilters,
            updateFilter,
            getFilterUrl,
            filter: { priceRange },
            minPriceRange,
            maxPriceRange,
            sortFields,
            clearFilters,
            selectedSort,
            onSortChange,
        } = this.props;

        return (
            <CategoryFilterOverlay
                getFilterUrl={getFilterUrl}
                availableFilters={filters}
                customFiltersValues={selectedFilters}
                updateFilter={updateFilter}
                priceValue={priceRange}
                minPriceValue={minPriceRange}
                maxPriceValue={maxPriceRange}
                clearFilters={clearFilters}
                sortFields={sortFields}
                selectedSort={selectedSort}
                onSortChange={onSortChange}
            />
        );
    }

    renderBrandProductList() {
        const { filter, search, selectedSort, selectedFilters, getIsNewCategory } = this.props;

        return (
            <div block="BrandPage" elem="ProductListWrapper">
                <CategoryProductList
                    filter={filter}
                    search={search}
                    sort={selectedSort}
                    selectedFilters={selectedFilters}
                    getIsNewCategory={getIsNewCategory}
                />
            </div>
        );
    }

    render() {
        const {
            location,
            match,
            isMatchingListFilter,
            currentArgs: { currentPage },
            brandDescriptionSuffix,
            updateMetaFromCategory,
            defaultTitle,
            updateLinks,
            removeLinks,
        } = this.props;

        return (
            <main block="BrandPage">
                <ContentWrapper wrapperMix={{ block: 'BrandPage', elem: 'Wrapper' }} label="Brand page">
                    <BrandHeader
                        isMatchingListFilter={isMatchingListFilter}
                        location={location}
                        match={match}
                        currentPage={currentPage}
                        brandDescriptionSuffix={brandDescriptionSuffix}
                        updateMetaFromCategory={updateMetaFromCategory}
                        defaultTitle={defaultTitle}
                        updateLinks={updateLinks}
                        removeLinks={removeLinks}
                    >
                        {this.renderSubcategorySelector()}
                        {this.renderFilterOverlay()}
                        <aside block="BrandPage" elem="Miscellaneous">
                            {this.renderFilterButton()}
                        </aside>
                        {this.renderBrandProductList()}
                    </BrandHeader>
                </ContentWrapper>
            </main>
        );
    }
}

import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import {
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps,
    MyAccountMyWishlistContainer,
} from 'Component/MyAccountMyWishlist/MyAccountMyWishlist.container';
import { ThemeContext } from 'Context/ThemeContext';
import StashPage from 'Route/StashPage/StashPage.component';
import BreadcrumbsDispatcher from 'Store/Breadcrumbs/Breadcrumbs.dispatcher';
import { updateMeta } from 'Store/Meta/Meta.action';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    updateBreadcrumbs: (breadcrumbs) => {
        // debugger;
        return BreadcrumbsDispatcher.update(breadcrumbs, dispatch);
    },
    updateMeta: (meta) => dispatch(updateMeta(meta)),
});

class StashPageContainer extends MyAccountMyWishlistContainer {
    componentDidMount() {
        const { updateMeta } = this.props;

        updateMeta({ title: __('Stash') });

        this._updateBreadcrumbs();
        // this._changeHeaderState();
    }

    _updateBreadcrumbs() {
        const { updateBreadcrumbs } = this.props;
        const breadcrumbs = [{ url: '/stash', name: __('Stash') }];

        updateBreadcrumbs(breadcrumbs);
    }

    render() {
        return <StashPage {...this.props} {...this.containerProps()} {...this.containerFunctions()} />;
    }
}

const StashPageContainerThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <StashPageContainer {...props} />;
};

export default connect(mapStateToProps, mapDispatchToProps)(StashPageContainerThemed);

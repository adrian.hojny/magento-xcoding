import React from 'react';

import CartPageSource from 'SourceRoute/CartPage/CartPage.component';

import CartItem from 'Component/CartItem/CartItem.container';
import CmsBlock from 'Component/CmsBlock/CmsBlock.container';
import ContentWrapper from 'Component/ContentWrapper';
import Link from 'Component/Link';
import Loader from 'Component/Loader';
import RecentlyViewed from 'Component/RecentlyViewed/RecentlyViewed.container';
import { getStaticFilePath } from 'Util/Image/Image';
import isMobile from 'Util/Mobile';
import EmptyImage from '../../style/images/cart_empty.jpg';
import MobileEmptyImage from '../../style/images/cart_empty_mobile.jpg';

const DOUBLEBOX_CMS = 'doublebox';
const SHIPPING_PAYMENTS_ICONS = 'footer-icons';

export default class CartPage extends CartPageSource {
    renderCartItems() {
        const {
            isEditing,
            totals: { items, quote_currency_code },
        } = this.props;

        if (!items || items.length < 1) {
            return null;
        }

        return (
            <ul block="CartPage" elem="Items" aria-label="List of items in cart">
                {items.map((item) => (
                    <CartItem
                        key={item.item_id}
                        item={item}
                        currency_code={quote_currency_code}
                        isEditing={!isMobile.any() || isEditing}
                        isLikeTable
                        showActions
                    />
                ))}
            </ul>
        );
    }

    renderButtons() {
        const { onCheckoutButtonClick } = this.props;
        return (
            <div block="CartPage" elem="CheckoutButtons">
                <button
                    block="CartPage"
                    elem="CheckoutButton"
                    mix={{ block: 'Button' }}
                    onClick={onCheckoutButtonClick}
                >
                    <span>{__('Go To Checkout')}</span>
                </button>
            </div>
        );
    }

    renderEmptyCart() {
        return (
            <>
                <ContentWrapper wrapperMix={{ block: 'CartPage', elem: 'EmptyWrapper' }} label="Cart page details">
                    <div block="CartPage" elem="Empty">
                        <img
                            src={getStaticFilePath(!isMobile.any() ? EmptyImage : MobileEmptyImage)}
                            alt={__('empty cart')}
                        />
                        <p>{__('Your cart is still empty.')}</p>
                        <Link to="/" block="CartPage" elem="Back">
                            {__('Continue shopping')}
                        </Link>
                    </div>
                </ContentWrapper>
                <RecentlyViewed title={__('Recently viewed')} />
            </>
        );
    }

    renderTotalDetails(isMobile = false) {
        return (
            <dl block="CartPage" elem="TotalDetails" aria-label={__('Order total details')} mods={{ isMobile }}>
                <dt>{__('Whole:')}</dt>
                <dd>{this.renderPriceLine(this.getFinalPricesTotal())}</dd>
                <dt block="CartPage" elem="DiscountLabel">
                    {__('Saving:')}
                </dt>
                <dd block="CartPage" elem="Discsubtotal_incl_taxountLabel">
                    {this.renderPriceLine(this.getSaving())}
                </dd>
                {this.renderDiscount()}
            </dl>
        );
    }

    getRegularPricesTotal() {
        const {
            totals: { items },
        } = this.props;
        let tempTotal = 0;

        items.forEach((item) => {
            tempTotal += item.product.price_range.minimum_price.regular_price.value * item.qty;
        });

        return tempTotal;
    }

    getFinalPricesTotal() {
        const {
            totals: { items },
        } = this.props;
        let tempTotal = 0;

        items.forEach((item) => {
            tempTotal += item.product.price_range.minimum_price.final_price.value * item.qty;
        });

        return tempTotal;
    }

    renderTotal() {
        const {
            totals: { discounted_subtotal_incl_tax = 0 },
        } = this.props;

        return (
            <dl block="CartPage" elem="Total" aria-label="Complete order total">
                <dt>{__('Order total:')}</dt>
                <dd>{this.renderPriceLine(discounted_subtotal_incl_tax)}</dd>
            </dl>
        );
    }

    renderTotals() {
        return (
            <article block="CartPage" elem="Summary">
                <h4 block="CartPage" elem="SummaryHeading">
                    {__('Summary')}
                </h4>
                {Number(this.getSaving()) > 0 && this.renderTotalDetails(true)}
                {this.renderTotal()}
                {this.renderButtons()}
            </article>
        );
    }

    getSaving() {
        const regularPricesTotal = this.getRegularPricesTotal();
        const finalPricesTotal = this.getFinalPricesTotal();

        return (Number(regularPricesTotal) - Number(finalPricesTotal)).toFixed(2);
    }

    renderPromoContent() {
        const { cart_content: { cart_cms } = {} } = window.contentConfiguration;

        if (cart_cms) {
            return <CmsBlock identifier={cart_cms} />;
        }

        return null;
    }

    renderCartLoading() {
        const { isCartLoading } = this.props;

        return (
            <ContentWrapper wrapperMix={{ block: 'CartPage', elem: 'Loader' }} label="Cart page details">
                <Loader isLoading={isCartLoading} />
            </ContentWrapper>
        );
    }

    render() {
        const {
            totals: { items_qty },
            isCartLoading,
        } = this.props;
        let text = '';
        const lastDigit = items_qty % 10;
        const qty = items_qty && items_qty > 0 ? items_qty : '0';

        if (items_qty === 1) {
            text = __('%s product', qty);
        } else if (lastDigit > 1 && lastDigit < 5) {
            text = __('%s PRODUCT_PLURAL_1', qty);
        } else {
            text = __('%s PRODUCT_PLURAL_2', qty);
        }

        if (isCartLoading) {
            return this.renderCartLoading();
        }

        if (!items_qty || items_qty < 1) {
            return <>{this.renderEmptyCart()}</>;
        }

        return (
            <main block="CartPage" aria-label="Cart Page">
                <ContentWrapper
                    wrapperMix={{ block: 'CartPage', elem: 'HeadingWrapper' }}
                    label="Cart page heading wrapper"
                >
                    {this.renderHeading()}
                    <span block="CartPage" elem="HeadingQty">
                        <span>{text}</span>
                    </span>
                </ContentWrapper>
                <ContentWrapper wrapperMix={{ block: 'CartPage', elem: 'Wrapper' }} label="Cart page details">
                    <div block="CartPage" elem="Static">
                        {this.renderCartItems()}
                        <CmsBlock identifier={DOUBLEBOX_CMS} />
                        {Number(this.getSaving()) > 0 && this.renderTotalDetails(true)}

                        {this.renderCrossSellProducts()}
                        <div block="CartPage" elem="AdditionalBlocks">
                            <CmsBlock identifier={SHIPPING_PAYMENTS_ICONS} />
                        </div>
                    </div>
                    <div block="CartPage" elem="Floating">
                        {this.renderPromo()}
                        <div block="CartPage" elem="SummaryWrapper">
                            {this.renderTotals()}
                            {this.renderDiscountCode()}
                        </div>
                    </div>
                </ContentWrapper>
                {this.renderCrossSellProducts()}
                <RecentlyViewed title={__('Recently viewed')} />
            </main>
        );
    }
}

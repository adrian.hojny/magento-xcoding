import React from 'react';
import PropTypes from 'prop-types';

import SourceProductPage from 'SourceRoute/ProductPage/ProductPage.component';

import CmsBlock from 'Component/CmsBlock';
import CompleteLook from 'Component/CompleteLook/CompleteLook.container';
import ContentWrapper from 'Component/ContentWrapper';
import { DEFAULT_CURERNCY } from 'Component/LangPanel/LangPanel.component';
import Overlay from 'Component/Overlay/Overlay.container';
import ProductActions from 'Component/ProductActions/ProductActions.container';
import ProductCardAdditionalDetails from 'Component/ProductCardAdditionalDetails';
import ProductCardDetails from 'Component/ProductCardDetails';
import ProductCardGalleryComponent from 'Component/ProductCardGallery';
import ProductCardGalleryMobile from 'Component/ProductCardGalleryMobile';
import ProductCardTabs from 'Component/ProductCardTabs';
import ProductCollections from 'Component/ProductCollections';
import ProductGalleryOverlayMobile from 'Component/ProductGalleryOverlayMobile';
import ProductGalleryOverlaySlider from 'Component/ProductGalleryOverlaySlider';
import { CLICKED_CATEGORIES } from 'Component/ProductList/ProductList.component';
import ProductReviewForm from 'Component/ProductReviewForm';
import ProductReviews from 'Component/ProductReviews';
import ProductStory from 'Component/ProductStory';
import ProductVariants from 'Component/ProductVariants';
import ProductWishlistButton from 'Component/ProductWishlistButton';
import RecentlyViewed from 'Component/RecentlyViewed';
import { RELATED } from 'Store/LinkedProducts/LinkedProducts.reducer';
import { ProductType } from 'Type/ProductList';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import isMobileUtil from 'Util/Mobile';
import { getRating } from 'Util/Product/Review';

export * from 'SourceRoute/ProductPage/ProductPage.component';

export const PRODUCT_GALLERY_OVERLAY_KEY = 'product-gallery-overlay';
export const PRODUCT_GALLERY_MOBILE_OVERLAY_KEY = 'product-gallery-mobile-overlay';
export const PRODUCT_REVIEW_OVERLAY_KEY = 'product-review-overlay';

export const DETAILS_TAB_ID = 'details';
export const ADDITIONAL_INFO_TAB_ID = 'additional-info';
export const TECHNOLOGY_TAB_ID = 'technology';
export const DELIVERY_TAB_ID = 'delivery';
export const REVIEW_TAB_ID = 'review';

export const DELIVERY_BLOCK_ID = 'delivery';

class ProductPage extends SourceProductPage {
    static propTypes = {
        configurableVariantIndex: PropTypes.number.isRequired,
        activeImageOverlayGallery: PropTypes.number.isRequired,
        productOrVariant: ProductType.isRequired,
        getLink: PropTypes.func.isRequired,
        parameters: PropTypes.objectOf(PropTypes.string).isRequired,
        updateConfigurableVariant: PropTypes.func.isRequired,
        updateActiveImageOverlayGallery: PropTypes.func.isRequired,
        dataSource: ProductType.isRequired,
        areDetailsLoaded: PropTypes.bool.isRequired,
    };

    state = {
        isMobile: isMobileUtil.width(),
        galleryRendered: false,
    };

    handleMobile = this.handleMobile.bind(this);

    componentDidMount() {
        const { dataSource, currencyCode } = this.props;
        const categoriesData = BrowserDatabase.getItem(CLICKED_CATEGORIES) || {};
        const list = categoriesData[dataSource.id] || '';
        const userPreferredCurrency = BrowserDatabase.getItem(DEFAULT_CURERNCY);
        const currency = userPreferredCurrency || currencyCode;

        /* On product click event */
        gtag('event', 'select_content', {
            content_type: 'product',
            items: [
                {
                    id: dataSource?.sku,
                    name: dataSource?.name || '',
                    list_name: list,
                    brand: dataSource?.brand_details?.url_alias || '',
                    category: list,
                    variant: '',
                    list_position: 1,
                    price: dataSource?.price_range?.minimum_price?.final_price?.value,
                },
            ],
        });

        /* Product details event */
        gtag('event', 'view_item', {
            items: [
                {
                    id: dataSource?.sku,
                    name: dataSource?.name || '',
                    list_name: list,
                    brand: dataSource?.brand_details?.url_alias || '',
                    category: list,
                    variant: '',
                    list_position: 1,
                    price: dataSource?.price_range?.minimum_price?.final_price?.value,
                },
            ],
        });

        /* Facebook event */

        if (window?.fbq) {
            fbq('track', 'ViewContent', {
                content_ids: dataSource?.sku,
                content_type: 'product',
                value: dataSource?.price_range?.minimum_price?.final_price?.value,
                currency,
            });
        }

        window.addEventListener('resize', this.handleMobile);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleMobile);
    }

    handleMobile() {
        this.setState({
            isMobile: isMobileUtil.width(),
        });
    }

    showGallery = () => {
        const { showOverlay } = this.props;

        this.setState(
            {
                galleryRendered: true,
            },
            () => {
                showOverlay(PRODUCT_GALLERY_OVERLAY_KEY);
            },
        );
    };

    showMobileGallery = () => {
        const { showOverlay } = this.props;

        this.setState(
            {
                galleryRendered: true,
            },
            () => {
                showOverlay(PRODUCT_GALLERY_MOBILE_OVERLAY_KEY);
            },
        );
    };

    closeGallery = () => {
        const { hideActiveOverlay } = this.props;
        hideActiveOverlay();
    };

    toggleReviewSidebar = () => {
        const { showOverlay } = this.props;

        showOverlay(PRODUCT_REVIEW_OVERLAY_KEY);
    };

    renderMobileGallery() {
        const {
            productOrVariant,
            areDetailsLoaded,
            hideActiveOverlay,
            product: { attributes },
        } = this.props;
        const { galleryRendered } = this.state;

        return (
            <>
                <ProductCardGalleryMobile
                    attributes={attributes}
                    product={productOrVariant}
                    areDetailsLoaded={areDetailsLoaded}
                    showGallery={this.showMobileGallery}
                />
                {galleryRendered && (
                    <Overlay
                        mix={{ block: 'GalleryMobileOverlay' }}
                        id={PRODUCT_GALLERY_MOBILE_OVERLAY_KEY}
                        onOpenFreeze={false}
                    >
                        <ProductGalleryOverlayMobile product={productOrVariant} areDetailsLoaded={areDetailsLoaded} />
                        <button block="GalleryMobileOverlay" elem="Close" onClick={() => hideActiveOverlay()} />
                    </Overlay>
                )}
            </>
        );
    }

    renderDesktopGallery() {
        const {
            product,
            product: { attributes },
            productOrVariant,
            areDetailsLoaded,
            updateActiveImageOverlayGallery,
            activeImageOverlayGallery,
        } = this.props;
        const { galleryRendered } = this.state;

        return (
            <>
                <ProductWishlistButton product={product} quantity={1} configurableVariantIndex={0} />
                {galleryRendered && (
                    <Overlay mix={{ block: 'GalleryDesktopOverlay' }} id={PRODUCT_GALLERY_OVERLAY_KEY}>
                        <ProductGalleryOverlaySlider
                            product={productOrVariant}
                            areDetailsLoaded={areDetailsLoaded}
                            updateActiveImageOverlayGallery={updateActiveImageOverlayGallery}
                            showGallery={this.showGallery}
                            activeImageOverlayGallery={activeImageOverlayGallery}
                        />
                        <button block="GalleryDesktopOverlay" elem="Close" onClick={this.closeGallery} />
                    </Overlay>
                )}
                <ProductCardGalleryComponent
                    attributes={attributes}
                    product={productOrVariant}
                    areDetailsLoaded={areDetailsLoaded}
                    updateActiveImageOverlayGallery={updateActiveImageOverlayGallery}
                    showGallery={this.showGallery}
                />
            </>
        );
    }

    renderProductPageContent() {
        const {
            configurableVariantIndex,
            parameters,
            getLink,
            dataSource,
            updateConfigurableVariant,
            productOrVariant,
            areDetailsLoaded,
            selectedBundlePrice,
            setBundlePrice,
            getSelectedCustomizableOptions,
            productOptionsData,
        } = this.props;
        const { isMobile } = this.state;

        return (
            <>
                <div block="ProductPage" elem="SideWrapper">
                    {isMobile ? this.renderMobileGallery() : this.renderDesktopGallery()}
                </div>

                <ProductActions
                    getLink={getLink}
                    updateConfigurableVariant={updateConfigurableVariant}
                    product={dataSource}
                    productOrVariant={productOrVariant}
                    parameters={parameters}
                    areDetailsLoaded={areDetailsLoaded}
                    configurableVariantIndex={configurableVariantIndex}
                    toggleReviewSidebar={this.toggleReviewSidebar}
                    selectedBundlePrice={selectedBundlePrice}
                    setBundlePrice={setBundlePrice}
                    productOptionsData={productOptionsData}
                    getSelectedCustomizableOptions={getSelectedCustomizableOptions}
                />

                <div block="ProductVariants">
                    <ProductVariants
                        linkType={RELATED}
                        title={__('Available Variants')}
                        areDetailsLoaded={areDetailsLoaded}
                        product={dataSource}
                    />
                </div>
            </>
        );
    }

    renderRatingInfo() {
        const {
            dataSource: { review_summary: { review_count, rating_summary } = {} },
        } = this.props;

        const reviewInfo = review_count ? ` ${getRating(rating_summary)} (${review_count})` : '';

        return (
            <>
                {__('Customer reviews')}
                <strong>{reviewInfo}</strong>
            </>
        );
    }

    _getAttributesWithValues() {
        const {
            dataSource: { attributes = {}, parameters = {} },
        } = this.props;

        const allAttribsWithValues = Object.entries(attributes).reduce((acc, [key, val]) => {
            const { attribute_label, attribute_value } = val;
            if (attribute_value) {
                return { ...acc, [attribute_label]: val };
            }

            const valueIndexFromParameter = parameters[key];
            if (valueIndexFromParameter) {
                return { ...acc, [attribute_label]: { ...val, attribute_value: valueIndexFromParameter } };
            }

            return acc;
        }, {});

        return allAttribsWithValues;
    }

    _getDefaultTab(enableCardDetails, enableAdditionalDetails, enableTechnologyDetails) {
        return enableCardDetails
            ? DETAILS_TAB_ID
            : enableAdditionalDetails
            ? ADDITIONAL_INFO_TAB_ID
            : enableTechnologyDetails
            ? TECHNOLOGY_TAB_ID
            : DELIVERY_TAB_ID;
    }

    renderAdditionalSections() {
        const { productOrVariant, dataSource, areDetailsLoaded, parameters } = this.props;
        const attributes = this._getAttributesWithValues();
        const areParameters = Object.keys(parameters || {}).length > 0;
        const areAttributes = Object.keys(attributes || {}).length > 0;
        const enableCardDetails = areParameters || areAttributes;
        const enableAdditionalDetails = dataSource.description?.html?.length > 0;
        const enableTechnologyDetails = dataSource.short_description?.html?.length > 0;

        const defaultTab = this._getDefaultTab(enableCardDetails, enableAdditionalDetails, enableTechnologyDetails);

        return (
            <>
                <ProductCardTabs
                    product={productOrVariant}
                    toggleReviewSidebar={this.toggleReviewSidebar}
                    defaultTab={defaultTab}
                    tabs={[
                        {
                            id: DETAILS_TAB_ID,
                            title: __('Details'),
                            content: enableCardDetails ? (
                                <ProductCardDetails product={{ ...dataSource, parameters }} />
                            ) : null,
                        },
                        {
                            id: ADDITIONAL_INFO_TAB_ID,
                            title: __('Additional information'),
                            content: enableAdditionalDetails ? (
                                <ProductCardAdditionalDetails
                                    content={dataSource.description}
                                    elem="AdditionalInfo"
                                    itemprop="description"
                                />
                            ) : null,
                        },
                        {
                            id: TECHNOLOGY_TAB_ID,
                            title: __('Technology'),
                            content: enableTechnologyDetails ? (
                                <ProductCardAdditionalDetails content={dataSource.short_description} />
                            ) : null,
                        },
                        {
                            id: DELIVERY_TAB_ID,
                            title: __('Delivery and payment'),
                            content: <CmsBlock identifier={DELIVERY_BLOCK_ID} ignoredHtmlRules={['img']} />,
                        },
                        {
                            id: REVIEW_TAB_ID,
                            title: this.renderRatingInfo(),
                            content: (
                                <ProductReviews
                                    product={dataSource}
                                    areDetailsLoaded={areDetailsLoaded}
                                    toggleReviewSidebar={this.toggleReviewSidebar}
                                />
                            ),
                        },
                    ]}
                />
                <ProductStory product={dataSource} />
                <ProductCollections product={dataSource} />
                <CompleteLook product={dataSource} />
            </>
        );
    }

    render() {
        const { product } = this.props;

        return (
            <>
                <main block="ProductPage" aria-label="Product page" itemScope itemType="http://schema.org/Product">
                    <ContentWrapper
                        wrapperMix={{ block: 'ProductPage', elem: 'Wrapper' }}
                        label={__('Main product details')}
                    >
                        {this.renderProductPageContent()}
                    </ContentWrapper>
                    {this.renderAdditionalSections()}
                    <RecentlyViewed title={__('Recently viewed')} />
                    <ProductReviewForm product={product} />
                </main>
            </>
        );
    }
}

export default ProductPage;

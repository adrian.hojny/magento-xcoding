import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ContentWrapper from 'Component/ContentWrapper';
import ProductCollections from 'Component/ProductCollections';
import { RENDER_TYPE } from 'Component/ProductCollections/ProductCollections.component';

class ProductCollectionsPage extends PureComponent {
    static propTypes = {
        updateBreadcrumbs: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        const { updateBreadcrumbs } = props;
        updateBreadcrumbs([
            { url: '/product-collections', name: __('Product from Collections') },
            { url: '/', name: __('Home') },
        ]);
    }

    render() {
        return (
            <main block="RecentlyViewed">
                <ContentWrapper
                    wrapperMix={{ block: 'ProductCollections', elem: 'Wrapper' }}
                    label="Product Collections page"
                >
                    <ProductCollections renderType={RENDER_TYPE} />
                </ContentWrapper>
            </main>
        );
    }
}

export default ProductCollectionsPage;

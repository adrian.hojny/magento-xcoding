import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { AccountAbstractContainer } from 'Component/AccountAbstract/AccountAbstract.container';
import { STATE_CONFIRM_EMAIL } from 'Component/MyAccountOverlay/MyAccountOverlay.config';
import ConfigQuery from 'Query/Config.query';
import RegisterPage from 'Route/RegisterPage/RegisterPage.component';
import { toggleBreadcrumbs } from 'Store/Breadcrumbs/Breadcrumbs.action';
import { updateMeta } from 'Store/Meta/Meta.action';
import MyAccountDispatcher from 'Store/MyAccount/MyAccount.dispatcher';
import { showNotification } from 'Store/Notification/Notification.action';
import { fetchQuery } from 'Util/Request';

const REGISTER_TERMS_NAME = 'REGISTER_TERMS';
export const DATA_COMPLIANCE_INFO_ID = 'register-data-compliance';
export const REGISTER_INFO_ID = 'register-info-block';

export const mapDispatchToProps = (dispatch) => ({
    updateMeta: (meta) => dispatch(updateMeta(meta)),
    toggleBreadcrumbs: (state) => dispatch(toggleBreadcrumbs(state)),
    showNotification: (type, message) => dispatch(showNotification(type, message)),
    createAccount: (options) => MyAccountDispatcher.createAccount(options, dispatch),
});

class RegisterPageContainer extends AccountAbstractContainer {
    static propTypes = {
        updateMeta: PropTypes.func.isRequired,
        toggleBreadcrumbs: PropTypes.func.isRequired,
        showNotification: PropTypes.func.isRequired,
        createAccount: PropTypes.func.isRequired,
    };

    containerFunctions = {
        onCreateAccountAttempt: this.onCreateAccountAttempt.bind(this),
        onCreateAccountSuccess: this.onCreateAccountSuccess.bind(this),
    };

    state = {
        termsData: {},
    };

    componentDidMount() {
        super.componentDidMount();

        const { updateMeta } = this.props;

        updateMeta({ title: __('Sign-up') });

        this.getCheckboxData();
    }

    getCheckboxData() {
        fetchQuery(ConfigQuery.getCheckoutAgreements()).then(({ checkoutAgreements }) => {
            const data = checkoutAgreements.find((term) => term.name === REGISTER_TERMS_NAME);

            if (data) {
                this.setState({ termsData: data });
            }
        });
    }

    onCreateAccountAttempt(_, invalidFields) {
        const { showNotification } = this.props;

        if (invalidFields) {
            showNotification('info', __('Incorrect data! Please resolve all field validation errors.'));
        }

        this.setState({ isLoading: !invalidFields });
    }

    async onCreateAccountSuccess(fields) {
        // eslint-disable-next-line fp/no-let
        let gRecaptchaResponse = null;
        const { createAccount } = this.props;
        const { password, email, firstname, lastname, is_subscribed } = fields;

        if (window.grecaptcha) {
            gRecaptchaResponse = window.grecaptcha.getResponse();
        }

        const customerData = {
            customer: {
                firstname,
                lastname,
                email,
                is_subscribed,
            },
            password,
            gRecaptchaResponse,
        };

        createAccount({
            ...customerData,
        }).then((code) => {
            // if user needs confirmation
            if (code === 2) {
                this.setState({ state: STATE_CONFIRM_EMAIL });
            } else if (code) {
                this.onSignIn();
            }
            this.stopLoading();
        }, this.stopLoading);
    }

    render() {
        return <RegisterPage {...this.containerFunctions} {...this.state} />;
    }
}

export default connect(null, mapDispatchToProps)(RegisterPageContainer);

import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import {
    CartPageContainer as SourceCartPageContainer,
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
} from 'SourceRoute/CartPage/CartPage.container';

import { CART, CART_EDITING } from 'Component/Header/Header.config';
import { DEFAULT_STATE_NAME } from 'Component/NavigationAbstract/NavigationAbstract.config';
import { ThemeContext } from 'Context/ThemeContext';
import history from 'Util/History';
import isMobile from 'Util/Mobile';

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    isCartLoading: state.CartReducer.isLoading,
});

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
});

class CartPageContainer extends SourceCartPageContainer {
    _updateBreadcrumbs() {
        const { updateBreadcrumbs } = this.props;
        const breadcrumbs = [{ url: '/cart', name: __('Shopping cart') }];

        updateBreadcrumbs(breadcrumbs);
    }

    _changeHeaderState() {
        const {
            changeHeaderState,
            totals: { items_qty },
        } = this.props;
        const title = __('%s Items', items_qty || 0);

        if (isMobile.tabletWidth() || isMobile.any()) {
            changeHeaderState({
                name: DEFAULT_STATE_NAME,
            });
        } else {
            changeHeaderState({
                name: CART,
                title,
                onEditClick: () => {
                    this.setState({ isEditing: true });
                    changeHeaderState({
                        name: CART_EDITING,
                        title,
                        onOkClick: () => this.setState({ isEditing: false }),
                        onCancelClick: () => this.setState({ isEditing: false }),
                    });
                },
                onCloseClick: () => {
                    this.setState({ isEditing: false });
                    history.push('/');
                },
            });
        }
    }
}

const CartPageContainerThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <CartPageContainer {...props} />;
};

export default connect(mapStateToProps, mapDispatchToProps)(CartPageContainerThemed);

import PropTypes from 'prop-types';

import { NoMatch as SourceNoMatch } from 'SourceRoute/NoMatch/NoMatch.component';

export class NoMatch extends SourceNoMatch {
    static propTypes = {
        updateBreadcrumbs: PropTypes.func.isRequired,
        cleanUpTransition: PropTypes.func.isRequired,
        changeHeaderState: PropTypes.func.isRequired,
    };

    updateBreadcrumbs() {
        const { updateBreadcrumbs } = this.props;
        const breadcrumbs = [
            {
                url: '',
                name: __('Not Found'),
            },
        ];

        updateBreadcrumbs(breadcrumbs);
    }
}

export default NoMatch;

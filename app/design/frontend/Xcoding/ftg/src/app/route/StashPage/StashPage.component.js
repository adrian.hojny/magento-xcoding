import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ContentWrapper from 'Component/ContentWrapper';
import Link from 'Component/Link';
import Loader from 'Component/Loader';
import ProductCard from 'Component/ProductCard';
import WishlistItem from 'Component/WishlistItem';
import { ProductType } from 'Type/ProductList';
import { getStaticFilePath } from 'Util/Image/Image';

import './StashPage.style.scss';

class StashPage extends PureComponent {
    static propTypes = {
        isLoading: PropTypes.bool.isRequired,
        isWishlistLoading: PropTypes.bool.isRequired,
        isWishlistEmpty: PropTypes.bool.isRequired,
        wishlistItems: PropTypes.objectOf(ProductType).isRequired,
    };

    renderHeader() {
        const { wishlistItems } = this.props;

        const productsCounter = Object.keys(wishlistItems).length;
        let text = '';
        const lastDigit = productsCounter % 10;
        if (productsCounter === 1) {
            text = __('%s product', productsCounter);
        } else if (lastDigit > 1 && lastDigit < 5) {
            text = __('%s PRODUCT_PLURAL_1', productsCounter);
        } else {
            text = __('%s PRODUCT_PLURAL_2', productsCounter);
        }

        return (
            <>
                <h1 block="StashPage" elem="Heading">
                    {__('Stash')}
                </h1>
                <p block="StashPage" elem="ProductCount">
                    {text}
                </p>
            </>
        );
    }

    renderEmptyStash() {
        return (
            <div block="StashPage" elem="Empty">
                <img src={getStaticFilePath('/assets/images/global/empty-stash.png')} alt={__('empty stash')} />
                <p>{__('Your stash is still empty.')}</p>
                <p>{__('Add the products you want to keep an eye on here.')}</p>
                <Link to="/" block="Button" mods={{ green: true }}>
                    {__('Continue shopping')}
                </Link>
            </div>
        );
    }

    renderContent() {
        const { isWishlistLoading, isWishlistEmpty, isLoading } = this.props;

        if (isWishlistEmpty && !isWishlistLoading) {
            return this.renderEmptyStash();
        }

        return (
            <div block="StashPage" elem="Products">
                <Loader isLoading={isLoading} />
                {isWishlistLoading && isWishlistEmpty ? this.renderPlaceholders() : this.renderProducts()}
            </div>
        );
    }

    renderPlaceholders() {
        return Array.from({ length: 2 }, (_, i) => <ProductCard key={i} withoutItemProps />);
    }

    renderProducts() {
        const { wishlistItems } = this.props;
        return Object.entries(wishlistItems).map(([id, product]) => <WishlistItem key={id} product={product} />);
    }

    render() {
        return (
            <main block="StashPage" aria-label="Stash Page">
                <ContentWrapper wrapperMix={{ block: 'StashPage', elem: 'Wrapper' }} label="Stash page details">
                    {this.renderHeader()}
                    {this.renderContent()}
                </ContentWrapper>
            </main>
        );
    }
}

export default StashPage;

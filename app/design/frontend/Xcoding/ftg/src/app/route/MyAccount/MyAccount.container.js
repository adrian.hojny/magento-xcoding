import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import { MY_ACCOUNT_URL } from 'SourceRoute/MyAccount/MyAccount.config';
import {
    mapDispatchToProps,
    mapStateToProps,
    MyAccountContainer as SourceMyAccountContainer,
} from 'SourceRoute/MyAccount/MyAccount.container';

import { CUSTOMER_ACCOUNT_PAGE } from 'Component/Header/Header.config';
import { ThemeContext } from 'Context/ThemeContext';
import isMobile from 'Util/Mobile';
import { appendWithStoreCode } from 'Util/Url';

export class MyAccountContainer extends SourceMyAccountContainer {
    onSignIn() {
        const { requestCustomerData, changeHeaderState, isSignedIn, history } = this.props;

        if (isSignedIn) {
            requestCustomerData();
        }

        changeHeaderState({
            title: 'My account',
            name: CUSTOMER_ACCOUNT_PAGE,
            onBackClick: () => history.push(appendWithStoreCode('/')),
        });
    }

    changeActiveTab(activeTab) {
        const { history } = this.props;
        const {
            [activeTab]: { url },
        } = this.tabMap;

        history.push(appendWithStoreCode(`${MY_ACCOUNT_URL}${url}`));
    }

    redirectIfNotSignedIn() {
        const {
            isSignedIn,
            history,
            location: { pathname },
        } = this.props;

        if (isSignedIn) {
            // do nothing for signed-in users
            return;
        }

        if (isMobile.any()) {
            // do not redirect on mobile
            return;
        }

        if (pathname === appendWithStoreCode('/forgot-password')) {
            // forward the forgot password state
            history.push({ pathname: appendWithStoreCode('/'), state: { isForgotPassword: true } });
            return;
        }

        history.push({ pathname: appendWithStoreCode('/') });
    }
}

const MyAccountContainerThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <MyAccountContainer {...props} />;
};

export default connect(mapStateToProps, mapDispatchToProps)(MyAccountContainerThemed);

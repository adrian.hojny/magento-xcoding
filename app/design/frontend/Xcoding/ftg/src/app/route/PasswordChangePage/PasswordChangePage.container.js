import { connect } from 'react-redux';
import { Redirect } from 'react-router';

import {
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
    PasswordChangePageContainer as SourcePasswordChangePageContainer,
} from 'SourceRoute/PasswordChangePage/PasswordChangePage.container';

import PasswordChangePage from './PasswordChangePage.component';
import { STATUS_PASSWORD_UPDATED } from './PasswordChangePage.config';

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
});

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
});

export class PasswordChangePageContainer extends SourcePasswordChangePageContainer {
    render() {
        const { passwordResetStatus } = this.state;

        if (passwordResetStatus === STATUS_PASSWORD_UPDATED) {
            return <Redirect to="/" />;
        }

        return <PasswordChangePage {...this.containerProps()} {...this.containerFunctions} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PasswordChangePageContainer);

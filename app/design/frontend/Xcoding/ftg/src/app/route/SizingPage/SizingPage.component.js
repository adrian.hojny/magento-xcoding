import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import CmsBlock from 'Component/CmsBlock/CmsBlock.container';
import ContentWrapper from 'Component/ContentWrapper';
import Html from 'Component/Html/Html.component';
import Image from 'Component/Image';
import Link from 'Component/Link/Link.component';
import { getRouteUrl } from 'Component/Router/Router.config';
import isMobile from 'Util/Mobile/isMobile';
import SizingPageTable from './components/SizingPageTable';

import './SizingPage.style';

export const SIZING_LINKS = 'sizing_links';

export default class SizingPage extends PureComponent {
    static propTypes = {
        activeTab: PropTypes.number.isRequired,
        handleTabChange: PropTypes.func.isRequired,
        toggleAllBrandsList: PropTypes.func.isRequired,
        sizing: PropTypes.array.isRequired,
        brands: PropTypes.array.isRequired,
        storeViewCode: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        slider_image: PropTypes.string,
        small_image_alt: PropTypes.string,
        allBrandsOpened: PropTypes.bool.isRequired,
        measure: PropTypes.object.isRequired,
    };

    static defaultProps = {
        slider_image: '',
        small_image_alt: '',
    };

    renderTabButton = this.renderTabButton.bind(this);

    renderTabContent = this.renderTabContent.bind(this);

    renderSizing = this.renderSizing.bind(this);

    renderHeader = this.renderHeader.bind(this);

    renderHeader() {
        const { title, slider_image, small_image_alt } = this.props;

        return (
            <div block="SizingPage" elem="Header">
                <h1>
                    {__('Size guide')}
                    <br />
                    {title}
                </h1>
                <div block="SizingPage" elem="HeaderLink">
                    <Image src={slider_image} alt={small_image_alt} useWebp />
                    <Link to="#otherBrands" block="SizingPage" elem="BrandLink">
                        {__('Other brands')}
                    </Link>
                </div>
            </div>
        );
    }

    renderTabButton({ label, id }) {
        const { activeTab, handleTabChange } = this.props;
        const active = activeTab === id;

        return (
            <button key={id} block="SizingPage" elem="TabLabel" mods={{ active }} onClick={() => handleTabChange(id)}>
                <span>{label}</span>
            </button>
        );
    }

    renderTabContent({ label, id, data }) {
        const { activeTab, measure } = this.props;
        const { measure_description } = measure;
        const active = activeTab === id;
        const measure_img = measure?.[`img_url_${activeTab}`] || '';

        return (
            <div key={id} block="SizingPage" elem="TabContent" mods={{ active }}>
                <div block="SizingPage" elem="Desc">
                    <div block="SizingPage" elem="DescContent">
                        <Html content={measure_description} />
                    </div>
                    <div block="SizingPage" elem="DescImage">
                        <Image src={measure_img} alt={label} useWebp />
                    </div>
                </div>
                <div key={id} block="SizingPage" elem="TabContent" mods={{ active: activeTab === id }}>
                    {data?.map((item, i) => this.renderSizing(item, id, i))}
                </div>
            </div>
        );
    }

    renderTabs() {
        const { sizing } = this.props;

        return (
            <>
                <div block="SizingPage" elem="TabsWrapper">
                    <p>{__('What size are you looking for?')}</p>
                    <div block="SizingPage" elem="Tabs">
                        {sizing.map(this.renderTabButton)}
                    </div>
                    {this.renderMobileAllBrands()}
                </div>
                <div block="SizingPage" elem="TabContentWrapper">
                    {sizing.map(this.renderTabContent)}
                </div>
            </>
        );
    }

    renderSizing(data, id, i) {
        return <SizingPageTable key={id + i} data={data} id={id} index={i} />;
    }

    renderAllBrands() {
        const { brands, storeViewCode } = this.props;

        return (
            <ul>
                {brands.map(({ title, url_alias }) => (
                    <li key={url_alias}>
                        <Link to={`${getRouteUrl('sizing', storeViewCode)}/${url_alias}`}>{title}</Link>
                    </li>
                ))}
            </ul>
        );
    }

    renderDesktopAllBrands() {
        if (isMobile.width()) return null;

        return (
            <div id="otherBrands" block="SizingPage" elem="AllBrands">
                <h3>{__('Other brands sizes')}</h3>
                {this.renderAllBrands()}
            </div>
        );
    }

    renderMobileAllBrands() {
        const { allBrandsOpened, toggleAllBrandsList } = this.props;

        if (!isMobile.width()) return null;

        return (
            <div id="otherBrands" block="SizingPage" elem="AllBrandsMobile">
                <button block="SizingPage" elem="AllBrandsMobileHeader" onClick={toggleAllBrandsList}>
                    {__('Brands list')}
                    <span block="SizingPage" elem="AllBrandsMobileArrow" mods={{ allBrandsOpened }} />
                </button>
                {allBrandsOpened && this.renderAllBrands()}
            </div>
        );
    }

    render() {
        return (
            <main block="SizingPage">
                <ContentWrapper wrapperMix={{ block: 'SizingPage', elem: 'Wrapper' }} label="Sizing page">
                    <div>
                        {this.renderHeader()}
                        <div>
                            <CmsBlock identifier={SIZING_LINKS} />
                        </div>
                        {this.renderTabs()}
                    </div>
                    {this.renderDesktopAllBrands()}
                </ContentWrapper>
            </main>
        );
    }
}

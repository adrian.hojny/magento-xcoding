import React from 'react';

import SourceCheckout from 'SourceRoute/Checkout/Checkout.component';
import { BILLING_STEP, CHECKOUT_URL, DETAILS_STEP, SHIPPING_STEP } from 'SourceRoute/Checkout/Checkout.config';

import CheckoutBilling from 'Component/CheckoutBilling';
import CheckoutOrderSummary from 'Component/CheckoutOrderSummary';
import CheckoutShipping from 'Component/CheckoutShipping/CheckoutShipping.container';
import CheckoutSuccess from 'Component/CheckoutSuccess';
import ContentWrapper from 'Component/ContentWrapper';
import Loader from 'Component/Loader';
import { appendWithStoreCode } from 'Util/Url';

export const REDIRECT_STEP = 'REDIRECT_STEP';

class Checkout extends SourceCheckout {
    stepMap = {
        [SHIPPING_STEP]: {
            title: __('Shipping step'),
            url: '/shipping',
            render: this.renderShippingStep.bind(this),
            areTotalsVisible: true,
            isCustom: false,
        },
        [BILLING_STEP]: {
            title: __('Billing step'),
            url: '/billing',
            render: this.renderBillingStep.bind(this),
            areTotalsVisible: true,
            isCustom: false,
        },
        [REDIRECT_STEP]: {
            title: __('Redirect'),
            url: '/redirect',
            render: this.renderRedirectStep.bind(this),
            areTotalsVisible: false,
            isCustom: true,
        },
        [DETAILS_STEP]: {
            title: __('Thank you for your purchase!'),
            url: '/success',
            render: this.renderDetailsStep.bind(this),
            areTotalsVisible: false,
            isCustom: true,
        },
    };

    componentDidMount() {
        const {
            checkoutStep,
            history,
            totals: { items = [] },
            orderData,
        } = this.props;
        const { url } = this.stepMap[checkoutStep];

        if (!items.length && !orderData) {
            history.push({ pathname: appendWithStoreCode('/cart') });
        } else {
            this.updateHeader();
            history.replace(appendWithStoreCode(`${CHECKOUT_URL}${url}`));
        }
    }

    updateStep() {
        const { checkoutStep, history } = this.props;
        const { url } = this.stepMap[checkoutStep];

        history.push(appendWithStoreCode(`${CHECKOUT_URL}${url}`));
    }

    renderDetailsStep() {
        const { orderData } = this.props;

        // const orderData = orderData.orderID ? BrowserDatabase.getItem(`order${ orderID }`) : undefined;
        return <CheckoutSuccess orderID={orderData} orderData={orderData} />;
    }

    renderRedirectStep() {
        return (
            <div block="Checkout" elem="RedirectMessage">
                <div>
                    <Loader isLoading />
                </div>
                <h4>{__('CHECKOUT_REDIRECT_MESSAGE')}</h4>
            </div>
        );
    }

    renderShippingStep() {
        const {
            shippingMethods,
            onShippingEstimationFieldsChange,
            saveAddressInformation,
            isDeliveryOptionsLoading,
            onAdditionalInformationChange,
            shippingAdditionalInformation,
            totals,
        } = this.props;

        return (
            <CheckoutShipping
                isLoading={isDeliveryOptionsLoading}
                shippingMethods={shippingMethods}
                saveAddressInformation={saveAddressInformation}
                onShippingEstimationFieldsChange={onShippingEstimationFieldsChange}
                onAdditionalInformationChange={onAdditionalInformationChange}
                shippingAdditionalInformation={shippingAdditionalInformation}
                totals={totals}
            />
        );
    }

    renderBillingStep() {
        const {
            setLoading,
            setDetailsStep,
            shippingAddress,
            paymentMethods = [],
            savePaymentInformation,
            totals,
        } = this.props;

        return (
            <CheckoutBilling
                setLoading={setLoading}
                paymentMethods={paymentMethods}
                setDetailsStep={setDetailsStep}
                shippingAddress={shippingAddress}
                savePaymentInformation={savePaymentInformation}
                totals={totals}
            />
        );
    }

    renderSummary() {
        const { checkoutTotals, checkoutStep, paymentTotals, selectedShippingMethod } = this.props;
        const { areTotalsVisible } = this.stepMap[checkoutStep];

        if (!areTotalsVisible) {
            return null;
        }

        return (
            <CheckoutOrderSummary
                checkoutStep={checkoutStep}
                totals={checkoutTotals}
                paymentTotals={paymentTotals}
                selectedShippingMethod={selectedShippingMethod}
            />
        );
    }

    render() {
        const { checkoutStep } = this.props;
        const { isCustom: isStepCustom = false } = this.stepMap[checkoutStep];
        return (
            <main block="Checkout">
                <ContentWrapper
                    wrapperMix={isStepCustom ? undefined : { block: 'Checkout', elem: 'Wrapper' }}
                    label={__('Checkout page')}
                >
                    {isStepCustom ? (
                        this.renderStep()
                    ) : (
                        <>
                            <div block="Checkout" elem="Step">
                                {this.renderTitle()}
                                {this.renderGuestForm()}
                                {this.renderStep()}
                                {this.renderLoader()}
                            </div>
                            <div>
                                {this.renderSummary()}
                                {this.renderPromo()}
                            </div>
                        </>
                    )}
                </ContentWrapper>
            </main>
        );
    }
}

export { BILLING_STEP, CHECKOUT_URL, DETAILS_STEP, SHIPPING_STEP };

export default Checkout;

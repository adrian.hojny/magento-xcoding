import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import {
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
    SearchPageContainer as SourceSearchPageContainer,
} from 'SourceRoute/SearchPage/SearchPage.container';

import { ThemeContext } from 'Context/ThemeContext';
import { removeLinks, updateLinks } from 'Store/Hreflinks/Hreflinks.actions';

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    links: state.HreflinksReducer.links,
});

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    removeLinks: () => dispatch(removeLinks()),
    updateLinks: (links) => dispatch(updateLinks(links)),
});

export class SearchPageContainer extends SourceSearchPageContainer {
    updateBreadcrumbs() {
        const { updateBreadcrumbs } = this.props;
        const search = this.getSearchParam();

        updateBreadcrumbs([
            {
                url: '',
                name: search.toUpperCase(),
            },
        ]);
    }
}

const SearchPageContainerThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <SearchPageContainer {...props} />;
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPageContainerThemed);

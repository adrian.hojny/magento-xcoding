import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import { ThemeContext } from 'Context/ThemeContext';
import { BreadcrumbsDispatcher, toggleBreadcrumbs } from 'Store/Breadcrumbs';
import { changeNavigationState } from 'Store/Navigation';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import PremiumBrand from './PremiumBrandPage.component';

export const mapStateToProps = (state) => ({
    navigationMode: state.NavigationReducer[TOP_NAVIGATION_TYPE].navigationState.name,
});
export const mapDispatchToProps = (dispatch) => ({
    updateBreadcrumbs: (breadcrumbs) => BreadcrumbsDispatcher.update(breadcrumbs, dispatch),
    // eslint-disable-next-line max-len
    toggleBreadcrumbs: (state) => dispatch(toggleBreadcrumbs(state)),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
});

const PremiumBrandPageContainer = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ header: 'transparent' });
        // props.changeHeaderState({
        //     name: PREMIUM_BRANDS
        // });

        // return
    }, []);

    return <PremiumBrand {...props} />;
};

PremiumBrandPageContainer.propTypes = PremiumBrand.propTypes;

// export default PremiumBrandPageContainer;
// export default connect(null, mapDispatchToProps)(PremiumBrand);
export default connect(null, mapDispatchToProps)(PremiumBrandPageContainer);

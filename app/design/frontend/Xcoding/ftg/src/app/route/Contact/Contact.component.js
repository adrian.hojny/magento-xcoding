import React, { PureComponent } from 'react';

import CmsBlock from 'Component/CmsBlock';
import ContentWrapper from 'Component/ContentWrapper';
import Field from 'Component/Field';
import Form from 'Component/Form';
import Loader from 'Component/Loader';
import { getStaticFilePath } from 'Util/Image/Image';

import './Contact.style.scss';

export const CONTACT_VCARD_ID = 'contact-vcard';
export const PRM_CONTACT_PAGE = 'prm_contact_page';

const VCard = () => <CmsBlock identifier={CONTACT_VCARD_ID} withoutWrapper />;

class Contact extends PureComponent {
    renderForm() {
        const { isLoading, setLoader, onSuccess, onError, topics, setFormRef, termsData } = this.props;

        return (
            <div block="Contact" elem="form-wrap">
                <Loader isLoading={isLoading} />

                <h1>{__('Contact')}</h1>

                <Form
                    key="contact-form"
                    ref={setFormRef}
                    onSubmit={setLoader}
                    onSubmitSuccess={onSuccess}
                    onSubmitError={onError}
                >
                    <Field
                        type="text"
                        block="Contact"
                        elem="Field"
                        label={__('First Name')}
                        id="name"
                        name="name"
                        validation={['notEmpty']}
                    />

                    <Field
                        type="text"
                        block="Contact"
                        elem="Field"
                        label={__('E-mail')}
                        id="email"
                        name="email"
                        validation={['notEmpty', 'email']}
                    />

                    <span block="Contact" elem="subject-label">
                        {__('How can we help?')}
                    </span>

                    <Field
                        type="select"
                        block="Contact"
                        elem="Field"
                        placeholder={__('Choose your topic')}
                        id="topic"
                        name="topic"
                        selectOptions={topics}
                        validation={['notEmpty']}
                    />

                    <Field
                        type="textarea"
                        block="Contact"
                        elem="Field"
                        placeholder={`${__('Describe your issue')}.`}
                        id="comment"
                        name="comment"
                        validation={['notEmpty']}
                        maxLength={500}
                        rows={7}
                    />

                    {termsData?.content && (
                        <Field
                            type="checkbox"
                            label={<div dangerouslySetInnerHTML={{ __html: termsData.content }} />}
                            id="terms"
                            name="terms"
                            validation={['isChecked']}
                        />
                    )}

                    <div block="Contact" elem="Buttons">
                        <button block="Contact" elem="Button" mix={{ block: 'Button' }} disabled={topics.length === 0}>
                            {__('Send')}
                        </button>
                    </div>
                </Form>
            </div>
        );
    }

    renderContent() {
        return (
            <ContentWrapper wrapperMix={{ block: 'Contact', elem: 'Wrapper' }} label="Contact page">
                {this.renderForm()}
                <div block="Contact" elem="img-wrap">
                    <img
                        src={getStaticFilePath('/assets/images/contact/contact_hero_182673.jpeg')}
                        alt="contact_hero"
                    />
                </div>
                <div block="Contact" elem="address-wrap">
                    <h2>{__('Sneakerstudio - e-commerce')}</h2>
                    <div block="Contact" elem="address-block">
                        <div block="Contact" elem="address-block-icon-wrap">
                            <svg
                                block="Contact"
                                elem="address-block-icon"
                                width="24"
                                height="24"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    d="M19.8 19.75v-1.33a.44.44 0 00-.89 0l.03 2.39a.43.43 0 00.13.31.42.42 0
                                    00.3.13l2.4-.03a.45.45 0 00.44-.45.44.44 0 00-.45-.44h-1.32l.06-.06A11.87 11.87
                                    0 007.98.82a.44.44 0 00.3.82 10.99 10.99 0 0111.56 18.03l-.05.08zM16.02
                                    23.18a.44.44 0 10-.3-.82A11 11 0 014.16 4.32l.06-.07v1.33a.44.44 0
                                    00.44.44.45.45 0 00.44-.45l-.03-2.39a.44.44 0 00-.42-.44l-2.4.03a.45.45 0
                                    00-.43.45.44.44 0 00.44.43h1.33l-.06.07a11.87 11.87 0 0012.5 19.46z"
                                    fill="#ADF51D"
                                />
                                <path
                                    d="M6.12 17.07a.67.67 0 00.2 1.27 4.16 4.16 0 002.74-.69 6.4 6.4 0
                                    10-2-1.64c-.2.45-.52.82-.94 1.06zm2.02-9a5.52 5.52 0 111.1 8.68.46.46 0
                                    00-.22-.06c-.1 0-.2.03-.27.09-.48.38-1.07.62-1.68.7.4-.4.7-.86.9-1.37a.44.44 0
                                    00-.06-.46 5.53 5.53 0 01.23-7.57z"
                                    fill="#ADF51D"
                                />
                                <path
                                    d="M12 12.44a.45.45 0 00.45-.44.44.44 0 00-.13-.31.45.45 0 00-.32-.13.45.45 0
                                    00-.44.44.44.44 0 00.44.44zM13.64 12.44a.45.45 0 00.44-.44.43.43 0
                                    00-.13-.31.43.43 0 00-.62 0 .43.43 0 00-.1.48l.1.14c.08.08.2.13.3.13zM10.36
                                    12.44a.45.45 0 00.45-.44.44.44 0 00-.88 0c0 .12.05.23.13.31.08.08.19.13.3.13z"
                                    fill="#ADF51D"
                                />
                            </svg>
                        </div>
                        <div block="Contact" elem="address-block-data">
                            <VCard />
                        </div>
                    </div>
                </div>
            </ContentWrapper>
        );
    }

    renderPrmContent() {
        return (
            <>
                <div block="Contact" elem="Container">
                    <ContentWrapper wrapperMix={{ block: 'Contact', elem: 'Wrapper' }}>
                        {this.renderForm()}
                        <div block="Contact" elem="img-wrap">
                            <img src={getStaticFilePath('/assets/images/contact/prm_contact.png')} alt="prm_contact" />
                        </div>
                    </ContentWrapper>
                </div>
                <ContentWrapper wrapperMix={{ block: 'Contact', elem: 'Content' }}>
                    <CmsBlock identifier={PRM_CONTACT_PAGE} withoutWrapper />
                </ContentWrapper>
            </>
        );
    }

    render() {
        const { isPrm } = this.props;

        return (
            <main block="Contact" aria-label="Contact Page">
                {!isPrm ? this.renderContent() : this.renderPrmContent()}
            </main>
        );
    }
}

export default Contact;

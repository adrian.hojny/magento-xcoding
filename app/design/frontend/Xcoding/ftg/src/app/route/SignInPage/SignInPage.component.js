import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ContentWrapper from 'Component/ContentWrapper';
import Field from 'Component/Field';
import Form from 'Component/Form';
import Link from 'Component/Link';
import Loader from 'Component/Loader';
import { getStaticFilePath } from 'Util/Image/Image';

import './SignInPage.style.scss';

class SignInPage extends PureComponent {
    static propTypes = {
        onSignInAttempt: PropTypes.func.isRequired,
        onSignInSuccess: PropTypes.func.isRequired,
        onFormError: PropTypes.func.isRequired,
        isLoading: PropTypes.bool.isRequired,
        errorMessage: PropTypes.string,
    };

    static defaultProps = {
        errorMessage: undefined,
    };

    renderLoginForm() {
        const { onSignInAttempt, onSignInSuccess, onFormError, errorMessage } = this.props;

        return (
            <Form
                key="sign-in"
                onSubmit={onSignInAttempt}
                onSubmitSuccess={onSignInSuccess}
                onSubmitError={onFormError}
            >
                <Field
                    type="text"
                    label={__('Login or Email')}
                    id="email"
                    name="email"
                    validation={['notEmpty', 'email']}
                />
                <Field
                    type="password"
                    label={__('Password')}
                    id="password"
                    name="password"
                    validation={['notEmpty', 'password']}
                />
                <div block="SignInPage" elem="ErrorMessage">
                    {errorMessage}
                </div>
                <div block="SignInPage" elem="Buttons">
                    <button block="Button" mods={{ green: true, fullWidth: true }}>
                        {__('Sign in')}
                    </button>
                </div>
                <Link block="SignInPage" elem="ForgotPasswordLink" to="/remind-password">
                    {__('Forgot password?')}
                </Link>
            </Form>
        );
    }

    render() {
        const { isLoading } = this.props;

        return (
            <main aria-label="Login Page">
                <ContentWrapper wrapperMix={{ block: 'SignInPage' }} label="Login page">
                    <div block="SignInPage" elem="ImageWrapper">
                        <img
                            src={getStaticFilePath('/assets/images/global/remind_password.jpg')}
                            alt={__('login page image')}
                        />
                    </div>
                    <div block="SignInPage" elem="FormWrapper">
                        <Loader isLoading={isLoading} />
                        <h1>{__('Sign-in')}</h1>
                        <p block="SignInPage" elem="SignUpMessage">
                            {`${__("Don't have an account yet?")} `}
                            <Link to="/register">{__('Sign up')}</Link>
                        </p>
                        {this.renderLoginForm()}
                    </div>
                </ContentWrapper>
            </main>
        );
    }
}

export default SignInPage;

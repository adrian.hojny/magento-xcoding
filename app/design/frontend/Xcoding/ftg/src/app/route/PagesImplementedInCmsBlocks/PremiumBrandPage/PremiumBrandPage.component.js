import React, { PureComponent, useState } from 'react';
import PropTypes from 'prop-types';

import CmsBlock from 'Component/CmsBlock';
import ContentFullWidth from 'Component/ContentFullWidth';
import TabbedRemoteNavi from 'Component/TabbedRemoteNavi';
import { UidProvider } from 'Context/UidContext';

import './PremiumBrandPage.style.scss';

const brand = {
    imgSrc: '',
    label: '',
    url: '',
    decoration: [
        // possible values
        'left-offset',
        'right-offset',
    ],
};

const convere = {
    imgSrc: '/assets/images/premium-brand/dimme.jpeg',
    label: 'Converse',
    url: '',
};

const eastpak = {
    imgSrc: '/assets/images/premium-brand/moshino.jpeg',
    label: 'Eastpak',
    url: '',
};

const moshino = {
    imgSrc: '/assets/images/premium-brand/moshino.jpeg',
    label: 'Moshino',
    url: '',
};

const diemme = {
    imgSrc: '/assets/images/premium-brand/dimme.jpeg',
    label: 'Diemme',
    url: '',
};

const woman = {
    imgSrc: '/assets/images/premium-brand/dimme.jpeg',
    label: 'Buty Damskie',
    url: '',
};

const man = {
    imgSrc: '/assets/images/premium-brand/maharashi.jpeg',
    label: 'Buty Męskie',
    url: '',
};

const fillingPieces = {
    imgSrc: '/assets/images/premium-brand/filling_pieces.png',
    label: 'Filling Pieces',
    url: '',
    attachedProducts: [
        {
            label: 'Low Fade Cosmo Mix Multi',
            price: '339,00 PLN',
            url: '#',
            imgSrc: '/assets/images/premium-brand/rectangle 219.jpg',
        },
        {
            label: 'Low Fade Cosmo Mix Multi',
            price: '339,00 PLN',
            url: '#',
            imgSrc: '/assets/images/premium-brand/rectangle 219.jpg',
        },
        {
            label: 'Low Fade Cosmo Mix Multi',
            price: '339,00 PLN',
            url: '#',
            imgSrc: '/assets/images/premium-brand/rectangle 219.jpg',
        },
    ],
};
const woodWood = {
    imgSrc: '/assets/images/premium-brand/wood_wood.jpeg',
    label: 'Wood Wood',
    url: '',
    decoration: 'left-offset',
};

const jasonMark = {
    imgSrc: '/assets/images/premium-brand/jason_markk.png',
    label: 'Jason Markk',
    url: '',
};

const maharishi = {
    imgSrc: '/assets/images/premium-brand/maharashi.jpeg',
    label: 'Maharishi',
    url: '',
};

const featureBrandWithDescription = {
    imgSrc: '',
    label: '',
    url: '',
    description: '',
};

const threeInRowBrand = {
    imgsSrc: [
        '/assets/images/premium-brand/diadora1.jpeg',
        '/assets/images/premium-brand/diadora2.jpeg',
        '/assets/images/premium-brand/diadora3.jpeg',
    ],
    label: 'Diadora',
    url: '',
    decoration: 'three-in-row',
};

const topFeatured = [convere, eastpak];

const middleFeatured = [woman, man, moshino, diemme, fillingPieces, woodWood, jasonMark, maharishi];

const product = {
    imgSrc: '',
    title: '',
    shortDesc: '',
    price: '12.99',
    url: '',
    offset: ['left-offset', 'right-offset'],
};

const predefPropTypes = {
    product: {
        imgSrc: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        shortDesc: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        offset: PropTypes.arrayOf(PropTypes.string),
    },
};

const tabbedBrandsSample = [
    {
        label: 'Jason Mark',
        products: [product, product],
    },
];

const BrandLabel = ({ label, mix, url }) => (
    <div block="BrandLabel" mix={mix}>
        <div block="BrandLabel" elem="header">
            {label}
        </div>
        <a block="BrandLabel" elem="action-text" href="#">
            {__('See other products')}
        </a>
    </div>
);

const sampelMarksList = ['Alife', 'Clarks Original', 'Converse', 'Diadora', 'Eastpak X Rraf Simons'];

const PremiumBrandsList = () => (
    <div block="PremiumBrandsList">
        <h3 block="PremiumBrandsList" elem="header">
            {/* eslint-disable-next-line react/forbid-dom-props */}
            <svg
                width="44"
                height="33"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                className="PremiumBrandsList-header-icon"
            >
                <path
                    d="M42.5 12.46L23.24 31.7a1.75 1.75 0 01-2.48 0L1.51 12.46a3.46 3.46 0 010-4.9l6.06-6.05A3.43 3.43 0 0110 .5h7.4a3.46 3.46 0 012.41 5.94l-4 3.88 2.33 4.06a1.75 1.75 0 01.05 1.65l1.06 1.86a.97.97 0 11-1.69.97L16.51 17a1.75 1.75 0 01-1.4-.87l-2.56-4.46a2.92 2.92 0 01.5-3.55l4.24-4.11h-7.26l-6 6L22 28 39.98 10l-6-6h-7.26l4.24 4.11a2.91 2.91 0 01.5 3.55l-2.56 4.46a1.75 1.75 0 01-1.4.87l-1.06 1.86a.97.97 0 01-1.7-.97l1.07-1.86a1.75 1.75 0 01.05-1.65l2.33-4.06-4-3.88A3.46 3.46 0 0126.59.5H34a3.43 3.43 0 012.45 1.01l6.06 6.06a3.46 3.46 0 010 4.89z"
                    fill="#141414"
                />
            </svg>

            <span block="PremiumBrandsList" elem="header-text">
                Marki Premium
            </span>
        </h3>
        <ul block="PremiumBrandsList" elem="list">
            {[
                ...sampelMarksList,
                ...sampelMarksList,
                ...sampelMarksList,
                ...sampelMarksList,
                'Alife',
                'Clarks Original',
                'Converse',
            ].map((brand) => (
                <li block="PremiumBrandsList" elem="list-item">
                    {brand}
                </li>
            ))}
        </ul>
    </div>
);

const PREMIUM_BRANDS_SLIDER_WRAP_ID = 'premium-brands-slider-wrap';

const ProductsSlider = () => <CmsBlock identifier={PREMIUM_BRANDS_SLIDER_WRAP_ID} withoutWrapper />;

const Hero = ({ title, imgSrc }) => (
    <div block="PremiumBrandHero">
        <h3 block="PremiumBrandHero" elem="title">
            {title}
        </h3>
        <div block="PremiumBrandHero" elem="img-wrap">
            <img block="PremiumBrandHero" elem="img" src={imgSrc} alt="" />
        </div>
    </div>
);

Hero.propTypes = {
    title: PropTypes.string.isRequired,
    imgSrc: PropTypes.string.isRequired,
};

const SeeProducts = () => (
    <div block="PremiumBrandSeeProducts">
        <img
            src="/assets/images/premium-brand/prada_adidas1.jpeg"
            alt="prada_adidas"
            block="PremiumBrandSeeProducts"
            elem="first-img"
        />
        <BrandLabel label="Prada Adidas Originals" url="#" />
        <img
            src="/assets/images/premium-brand/prada_adidas2.jpeg"
            alt="prada_adidas"
            block="PremiumBrandSeeProducts"
            elem="second-img"
        />
    </div>
);

const FeaturedBrand = ({ imgsSrc, label, offset = 'default', url, mods: propMods, attachedProducts }) => {
    imgsSrc = typeof imgsSrc === 'string' ? [imgsSrc] : imgsSrc;
    const hasAttachedProducts = attachedProducts && attachedProducts.length > 0;
    const mods = {
        ...propMods,
        offset,
    };

    if (hasAttachedProducts) mods.hasAttachedProducts = hasAttachedProducts;

    return (
        <div block="FeaturedBrand" mods={mods}>
            <div block="FeaturedBrand" elem="inner-wrap">
                <div block="FeaturedBrand" elem="img-wrap">
                    {imgsSrc.map((imgSrc) => (
                        <img src={imgSrc} block="FeaturedBrand" elem="img" />
                    ))}
                </div>
                <BrandLabel label={label} mix={{ block: 'FeaturedBrand', elem: 'label' }} url={url} />
                {attachedProducts && (
                    <div block="FeaturedBrandAttachedProductsWrap">
                        {attachedProducts.map(({ label, price, url, imgSrc }) => (
                            <a block="FeaturedBrandAttachedProduct" href={url}>
                                <div block="FeaturedBrandAttachedProduct" elem="img-wrap">
                                    <img src={imgSrc} alt="" />
                                </div>
                                <div block="FeaturedBrandAttachedProduct" elem="label">
                                    {label}
                                </div>
                                <div block="FeaturedBrandAttachedProduct" elem="price">
                                    {price}
                                </div>
                            </a>
                        ))}
                    </div>
                )}
            </div>
        </div>
    );
};

// const FeaturedBrand = ({
//     imgSrc, label, offset, url, mods
// }) => (
//     <div block="FeaturedBrand" mods={ { ...mods, offset, } }>
//         <div block="FeaturedBrand" elem="inner-wrap">
//             <div block="FeaturedBrand" elem="img-wrap">
//                 <img src={ imgSrc } block="FeaturedBrand" elem="img" />
//             </div>
//             <BrandLabel label={ label } mix={ { block: 'FeaturedBrand', elem: 'label' } } />
//         </div>
//     </div>
// );

const FeaturedBrands = ({ brands, mods }) => (
    <div block="FeaturedBrands" mods={mods}>
        {brands.map(({ imgSrc, label, url, offset, attachedProducts }) => (
            <FeaturedBrand
                imgsSrc={imgSrc}
                label={label}
                url={url}
                offset={offset}
                attachedProducts={attachedProducts}
            />
        ))}
    </div>
);

const TabbedBrandsCarousel = ({ tabbedBrands = tabbedBrandsSample }) => {
    const firstLabel = tabbedBrands && tabbedBrands[0] && tabbedBrands[0].label;

    const [selectedBrand, setSelectedBrand] = useState(firstLabel || null);

    const products = tabbedBrands['Jason Mark'];
    return (
        <UidProvider>
            <TabbedRemoteNavi />
            {/* <BrandTabs
                currentlySelected={ selectedBrand }
                brands={ tabbedBrands.map(({ label }) => label) }
                handleBrandSelection={ setSelectedBrand }
            /> */}
            <div block="ProductsSliderWrap">
                <ProductsSlider products={products} />
            </div>
        </UidProvider>
    );
};

const BrandDescription = () => (
    <div block="BrandDescription">
        <h3 block="BrandDescription" elem="header">
            Clarks ORGINALS dla niej i dla niego
        </h3>
        <div block="BrandDescription" elem="desc">
            <p>
                Clarks Originals to specjalna kolekcja obuwia, która nawiązuje do najbardziej klasycznych modeli firmy
                Clarks. Buty tej serii zadebiutowały po raz pierwszy w 1950 roku i dopiero po kilkunastu latach zostały
                docenione przez wydawcę Esquire, zyskując tym samym ogromną popularność. Clarks Originals to specjalna
                kolekcja obuwia, która nawiązuje do najbardziej klasycznych modeli firmy Clarks. Buty tej serii
                zadebiutowały po raz pierwszy w 1950 roku i dopiero po kilkunastu latach zostały docenione przez wydawcę
                Esquire, zyskując tym samym ogromną popularność.{' '}
            </p>
        </div>
    </div>
);

const FeaturedWithDescription = () => (
    <div block="FeaturedWithDescription">
        <div block="FeaturedWithDescription" elem="inner-wrap">
            <div block="FeaturedWithDescription" elem="img-wrap">
                <img src="/assets/images/premium-brand/inukki.jpeg" alt="inukki" />
            </div>
            <div block="FeaturedWithDescription" elem="desc-wrap">
                <h4 block="FeaturedWithDescription" elem="heaeer">
                    Inuikki
                </h4>
                <p block="FeaturedWithDescription" elem="desc">
                    Inuikii shoes! They are a go to for any situation and you always feel comfortable in them wherever
                    you go and with whomever you meet. Inuikii has a way of playing with materials creating shoes that
                    can really transform a look (and mood!). The shoes are practical, yet fun with shapes and
                    eye-catching color combinations making any look interesting.
                </p>
            </div>
            <div block="FeaturedWithDescription" elem="example-product">
                <img
                    src="/assets/images/premium-brand/inukki_buty.jpeg"
                    block="FeaturedWithDescription"
                    elem="example-product-img"
                    alt={__('Inuikki shoes')}
                />
                <span block="FeaturedWithDescription" elem="example-product-title">
                    Buty Inuikii Sneaker Curly
                </span>
            </div>
        </div>
    </div>
);

class PremiumBrandPage extends PureComponent {
    static propTypes = {
        // requestBlocks: PropTypes.func.isRequired,
        updateBreadcrumbs: PropTypes.func.isRequired,
        toggleBreadcrumbs: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        };

        const { updateBreadcrumbs } = props;
        updateBreadcrumbs([
            { url: '/premium_brand_page', name: __('PremiumBrandPage') },
            { url: '/', name: __('Home') },
        ]);
    }

    onSuccess = (formData) => {
        console.log(formData);

        // trigger loading send data and after receiving response turn off loading
    };

    componentDidMount() {
        const { toggleBreadcrumbs } = this.props;

        toggleBreadcrumbs(false);
    }

    render() {
        return (
            <main block="PremiumBrand" aria-label="Premium Brand Page">
                <ContentFullWidth wrapperMix={{ block: 'PremiumBrand', elem: 'Wrapper' }} label="Premium brand page">
                    <Hero title="Prada for adidas originals" imgSrc="/assets/images/premium-brand/prada.jpg" />
                    <SeeProducts />
                    <PremiumBrandsList />
                    <FeaturedBrands brands={topFeatured} />
                    <BrandDescription />
                    <FeaturedBrands brands={middleFeatured} mods={{ style: 'middle' }} />
                    <FeaturedWithDescription brand={featureBrandWithDescription} />
                    <FeaturedBrand {...threeInRowBrand} mods={{ style: 'three-in-row' }} />
                    <TabbedBrandsCarousel />
                </ContentFullWidth>
            </main>
        );
    }
}

export default PremiumBrandPage;

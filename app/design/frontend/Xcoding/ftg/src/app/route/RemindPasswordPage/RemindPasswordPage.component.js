import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ContentWrapper from 'Component/ContentWrapper';
import Field from 'Component/Field';
import Form from 'Component/Form';
import Link from 'Component/Link';
import Loader from 'Component/Loader';
import { getStaticFilePath } from 'Util/Image/Image';

import './RemindPasswordPage.style.scss';

class RemindPasswordPage extends PureComponent {
    static propTypes = {
        forgotPassword: PropTypes.func.isRequired,
        toggleBreadcrumbs: PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            email: null,
        };

        const { toggleBreadcrumbs } = props;
        toggleBreadcrumbs(false);
    }

    onForgotPasswordSuccess = (fields) => {
        const { forgotPassword } = this.props;

        forgotPassword(fields).then(() => {
            this.setState({ email: fields.email });
            this.stopLoading();
        }, this.stopLoading);
    };

    onForgotPasswordAttempt = () => {
        this.setState({ isLoading: true });
    };

    onFormError = () => {
        this.setState({ isLoading: false });
    };

    stopLoading = () => this.setState({ isLoading: false });

    render() {
        const { isLoading, email } = this.state;

        if (email) {
            return (
                <ContentWrapper
                    wrapperMix={{ block: 'RemindPasswordPage', elem: 'WrapperSuccess' }}
                    label="Remind password"
                >
                    <div block="RemindPasswordPage" elem="Image">
                        <img src={getStaticFilePath('/assets/images/global/empty-stash.png')} alt={__('empty stash')} />
                    </div>
                    <div block="RemindPasswordPage" elem="Info" mods={{ success: true }}>
                        {__('Your password remind instructions was sent to your e-mail.')}
                    </div>
                    <div>
                        <Link to="/sign-in" block="Button" mods={{ green: true }}>
                            {__('Return to login')}
                        </Link>
                    </div>

                    <div block="RemindPasswordPage" elem="ResendEmail">
                        <button
                            block="Button"
                            mods={{ underline: true }}
                            onClick={() => this.onForgotPasswordSuccess({ email })}
                        >
                            {__('Resend the e-mail')}
                        </button>
                    </div>
                </ContentWrapper>
            );
        }

        return (
            <main aria-label="Remind Password Page">
                <ContentWrapper wrapperMix={{ block: 'RemindPasswordPage' }} label="Remind password">
                    <div block="RemindPasswordPage" elem="ImageWrapper">
                        <img
                            src={getStaticFilePath('/assets/images/global/remind_password.jpg')}
                            alt={__('remind password page image')}
                        />
                    </div>
                    <div block="RemindPasswordPage" elem="Content">
                        <Loader isLoading={isLoading} />
                        <h1 block="RemindPasswordPage" elem="Heading">
                            {__("Don't you remember the password?")}
                        </h1>
                        <div block="RemindPasswordPage" elem="Info">
                            {__('We will send you an email that allows you to reset your password.')}
                        </div>
                        <Form
                            key="forgot-password"
                            onSubmit={this.onForgotPasswordAttempt}
                            onSubmitSuccess={this.onForgotPasswordSuccess}
                            onSubmitError={this.onFormError}
                        >
                            <Field
                                type="text"
                                id="email"
                                name="email"
                                label="Email"
                                validation={['notEmpty', 'email']}
                            />
                            <div block="RemindPasswordPage" elem="Buttons">
                                <button block="Button" mods={{ green: true, fullWidth: true }} type="submit">
                                    {__('Send')}
                                </button>
                            </div>
                        </Form>
                        <div block="RemindPasswordPage" elem="ReturnToLogin">
                            <Link to="/sign-in" block="Button" mods={{ underline: true }}>
                                {__('Return to login')}
                            </Link>
                        </div>
                    </div>
                </ContentWrapper>
            </main>
        );
    }
}

export default RemindPasswordPage;

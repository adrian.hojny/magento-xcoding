import { connect } from 'react-redux';

import RemindPasswordPage from 'Route/RemindPasswordPage/RemindPasswordPage.component';
import { toggleBreadcrumbs } from 'Store/Breadcrumbs/Breadcrumbs.action';
import MyAccountDispatcher from 'Store/MyAccount/MyAccount.dispatcher';

export const mapDispatchToProps = (dispatch) => ({
    forgotPassword: (options) => MyAccountDispatcher.forgotPassword(options, dispatch),
    toggleBreadcrumbs: (state) => dispatch(toggleBreadcrumbs(state)),
});

export default connect(null, mapDispatchToProps)(RemindPasswordPage);

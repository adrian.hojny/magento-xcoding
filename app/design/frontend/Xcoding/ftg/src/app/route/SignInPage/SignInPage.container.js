import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { AccountAbstractContainer } from 'Component/AccountAbstract/AccountAbstract.container';
import SignInPage from 'Route/SignInPage/SignInPage.component';
import { toggleBreadcrumbs } from 'Store/Breadcrumbs/Breadcrumbs.action';
import { updateMeta } from 'Store/Meta/Meta.action';
import MyAccountDispatcher from 'Store/MyAccount/MyAccount.dispatcher';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { showNotification } from 'Store/Notification/Notification.action';

export const mapDispatchToProps = (dispatch) => ({
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    showNotification: (type, message) => dispatch(showNotification(type, message)),
    updateMeta: (meta) => dispatch(updateMeta(meta)),
    signIn: (options) => MyAccountDispatcher.signIn(options, dispatch),
    toggleBreadcrumbs: (state) => dispatch(toggleBreadcrumbs(state)),
    requestCustomerData: () => MyAccountDispatcher.requestCustomerData(dispatch),
});

class SignInPageContainer extends AccountAbstractContainer {
    static propTypes = {
        updateMeta: PropTypes.func.isRequired,
        toggleBreadcrumbs: PropTypes.func.isRequired,
        signIn: PropTypes.func.isRequired,
    };

    state = {
        isLoading: false,
        errorMessage: null,
    };

    containerFunctions = {
        onSignInSuccess: this.onSignInSuccess.bind(this),
        onSignInAttempt: this.onSignInAttempt.bind(this),
        onFormError: this.onFormError.bind(this),
    };

    componentDidMount() {
        super.componentDidMount();
        const { updateMeta } = this.props;

        updateMeta({ title: __('Sign in') });
    }

    onSignInAttempt() {
        this.setState({ isLoading: true });
    }

    onFormError() {
        this.setState({ isLoading: false });
    }

    async onSignInSuccess(fields) {
        const { signIn } = this.props;

        try {
            await signIn(fields);
            this.onSignIn();
        } catch (e) {
            this.setState({ isLoading: false, errorMessage: e.message });
        }
    }

    render() {
        return <SignInPage {...this.state} {...this.containerFunctions} />;
    }
}

export default connect(null, mapDispatchToProps)(SignInPageContainer);

import { connect } from 'react-redux';

import {
    HomePageContainer as SourceHomePageContainer,
    mapDispatchToProps,
    mapStateToProps,
} from 'SourceRoute/HomePage/HomePage.container';

import InstallPrompt from 'Component/InstallPrompt';
import CmsPage from 'Route/CmsPage';

class HomePageContainer extends SourceHomePageContainer {
    render() {
        return (
            <div block="HomePage">
                <InstallPrompt />
                <CmsPage {...this.props} />
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePageContainer);

import { createRef, PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { PREMIUM } from 'Component/Router/Router.component';
import ConfigQuery from 'Query/Config.query';
import ContactQuery from 'Query/Contact.query';
import { toggleBreadcrumbs } from 'Store/Breadcrumbs/Breadcrumbs.action';
import BreadcrumbsDispatcher from 'Store/Breadcrumbs/Breadcrumbs.dispatcher';
import { updateMeta } from 'Store/Meta/Meta.action';
import { showNotification } from 'Store/Notification/Notification.action';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { fetchMutation, fetchQuery } from 'Util/Request';
import Contact from './Contact.component';

const CONTACT_TERMS_NAME = 'CONTACT_TERMS';

export const mapDispatchToProps = (dispatch) => ({
    updateBreadcrumbs: (breadcrumbs) => BreadcrumbsDispatcher.update(breadcrumbs, dispatch),
    toggleBreadcrumbs: (state) => dispatch(toggleBreadcrumbs(state)),
    showNotification: (type, message) => dispatch(showNotification(type, message)),
    updateMeta: (meta) => dispatch(updateMeta(meta)),
});

export class ContactContainer extends PureComponent {
    static propTypes = {
        updateBreadcrumbs: PropTypes.func.isRequired,
        toggleBreadcrumbs: PropTypes.func.isRequired,
        showNotification: PropTypes.func.isRequired,
        updateMeta: PropTypes.func.isRequired,
    };

    containerFunctions = {
        setLoader: this.setLoader.bind(this),
        getTopicsList: this.getTopicsList.bind(this),
        setFormRef: this.setFormRef.bind(this),
        onSuccess: this.onSuccess.bind(this),
        onError: this.onError.bind(this),
    };

    constructor(props) {
        super(props);

        this.isPrm = BrowserDatabase.getItem(PREMIUM);

        this.state = {
            isLoading: true,
            topics: [],
            isPrm: this.isPrm,
            termsData: {},
        };

        this.formRef = createRef();

        const { updateBreadcrumbs, toggleBreadcrumbs, updateMeta } = props;

        if (this.isPrm) {
            toggleBreadcrumbs(false);
        } else {
            updateBreadcrumbs([
                { url: '/contact', name: __('Contact') },
                { url: '/', name: __('Home') },
            ]);
        }

        updateMeta({ title: __('Contact') });
    }

    componentDidMount() {
        this.getTopicsList();
        this.getCheckboxData();
    }

    handleError() {
        this.setState({
            isLoading: false,
        });
        this.showFormNotification('error');
    }

    onError() {
        this.handleError();
    }

    async onSuccess(contactFormFields) {
        try {
            const { contactFormSend } = await fetchMutation(ContactQuery.submitContactForm(contactFormFields));

            if (contactFormSend) {
                this.setState({ isLoading: false });
                this.showFormNotification('success');
            } else {
                this.handleError();
            }
        } catch (err) {
            this.handleError();
        }
    }

    getTopicsList() {
        fetchQuery(ConfigQuery.getContactTopicsListField())
            .then(({ storeConfig }) => {
                if (storeConfig?.contact_form_topics) {
                    this.setState({
                        topics: this.prepareTopics(storeConfig.contact_form_topics),
                        isLoading: false,
                    });
                } else {
                    this.handleError();
                }
            })
            .catch(() => {
                this.handleError();
            });
    }

    getCheckboxData() {
        fetchQuery(ConfigQuery.getCheckoutAgreements()).then(({ checkoutAgreements }) => {
            const data = checkoutAgreements.find((term) => term.name === CONTACT_TERMS_NAME);

            if (data) {
                this.setState({ termsData: data });
            }
        });
    }

    setLoader() {
        this.setState({
            isLoading: true,
        });
    }

    setFormRef(el) {
        this.formRef = el;
    }

    prepareTopics(topics) {
        return topics.map(({ code, label }) => ({
            id: code,
            value: code,
            label,
        }));
    }

    showFormNotification(contactFormSubmitStatus) {
        const { showNotification } = this.props;
        switch (contactFormSubmitStatus) {
            case 'success':
                showNotification('success', __('Your message was sent!'));
                break;
            case 'error':
            default:
                showNotification('error', __('Error! Something went wrong'));
                break;
        }
    }

    render() {
        return <Contact {...this.props} {...this.state} {...this.containerFunctions} />;
    }
}

export default connect(null, mapDispatchToProps)(ContactContainer);

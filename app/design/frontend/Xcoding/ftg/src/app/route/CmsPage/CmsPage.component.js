/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import { memo, useContext, useEffect } from 'react';
import isEqual from 'lodash.isequal';

import SourceCmsPage from 'SourceRoute/CmsPage/CmsPage.component';

import { ThemeContext } from 'Context/ThemeContext';
import media from 'Util/Media';
import { LOGO_MEDIA } from 'Util/Media/Media';

import './CmsPage.style';

const ThemedMain = memo(({ block, mods, themeModifiers, isBreadcrumbsActive, children }) => {
    const { setTheme, theme } = useContext(ThemeContext);

    useEffect(() => {
        if (!isEqual(themeModifiers, theme?.mods)) {
            setTheme({
                mods: themeModifiers,
            });
        }
    }, [themeModifiers]);

    const styledMods = themeModifiers.reduce(
        (acc, current) => ({
            ...acc,
            [current]: true,
        }),
        {},
    );

    return (
        <main
            block="CmsPage"
            mods={{
                isBreadcrumbsHidden: !isBreadcrumbsActive,
            }}
            mix={{
                block: 'StyledMain',
                mods: styledMods,
            }}
        >
            {children}
        </main>
    );
});

export default class CmsPage extends SourceCmsPage {
    componentDidMount() {
        const { pageIdentifiers, cms_home_page } = this.props;

        if (pageIdentifiers === cms_home_page) {
            this.handleItemPropScript();
        }
    }

    componentWillUnmount() {
        if (this.script) {
            this.removeItemPropScript();
        }
    }

    handleItemPropScript() {
        const { header_logo_src, default_country } = this.props;
        const { href } = window.location;

        this.script = document.createElement('script');

        this.script.type = 'application/ld+json';
        this.script.text = `
        [
            {
                "@context": "https://schema.org",
                "@type": "WebSite",
                "url": "${href}",
                "potentialAction": {
                    "@type": "SearchAction",
                    "target": "${href}search/{search_term_string}",
                    "query-input": "required name=search_term_string"
                }
            },
            {
                "@context": "https://schema.org",
                "@type": "Organization",
                "name": "SneakerStudioPRM",
                "alternateName": "PRM",
                "url": "${href}",
                "logo": "${media(header_logo_src, LOGO_MEDIA)}",
                "contactPoint": {
                  "@type": "ContactPoint",
                  "email": "info@sneakerstudioprm.com",
                  "contactType": "customer service",
                  "areaServed": "${default_country}",
                  "availableLanguage": "en"
                },
                "sameAs": [
                  "https://www.instagram.com/sneakerstudio_prm/",
                  "https://www.facebook.com/SneakerStudioPRM/"
                ]
            }
        ]
        `;

        document.head.appendChild(this.script);
    }

    removeItemPropScript() {
        document.head.removeChild(this.script);
    }

    render() {
        const { page, isBreadcrumbsActive } = this.props;
        const { page_width, scandipwa_modifiers = [] } = page;

        return (
            <ThemedMain
                themeModifiers={scandipwa_modifiers}
                block="CmsPage"
                mods={{ isBreadcrumbsHidden: !isBreadcrumbsActive }}
            >
                <div block="CmsPage" elem="Wrapper" mods={{ page_width }}>
                    {this.renderHeading()}
                    <div block="CmsPage" elem="Content">
                        {this.renderContent()}
                    </div>
                </div>
            </ThemedMain>
        );
    }
}

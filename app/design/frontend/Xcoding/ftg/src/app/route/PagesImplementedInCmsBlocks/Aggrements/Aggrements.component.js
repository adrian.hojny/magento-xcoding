import React, { PureComponent } from 'react';

import ContentWrapper from 'Component/ContentWrapper';

const Menu = () => (
    <nav className="HelpMenu">
        <ul>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#definicje">
                    1. Definicje
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#postanowienia-ogolne">
                    2. Postanowienia ogólne i korzystanie ze Sklepu Internetowego
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#rejestracja">
                    3. Rejestracja
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#zamowienia">
                    4. Zamówienia
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#platnosci">
                    5. Płatności
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#dostawa">
                    6. Dostawa
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#rekojmia">
                    7. Rękojmia
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#reklamacje">
                    8. Reklamacje
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#gwarancja">
                    9. Gwarancje
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#odstapienie-od-umowy">
                    10. Odstąpienie od umowy sprzedaży
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#uslugi-nieodplatne">
                    11. Usługi nieodpłatne
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#odpowiedzialnosc-za-tresci">
                    12. Odpowiedzialność klienta w zakresie umieszczanych przez niego treści.
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#naruszenia">
                    13. Zgłaszanie zagrożenia lub naruszenia praw
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#ochrona-danych">
                    14. Ochrona danych osobowych
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#rozwiazanie-umowy">
                    15. Rozwiązanie umowy (nie dotyczy Umów sprzedaży)
                </a>
            </li>
            <li className="HelpMenu-itemwrap">
                <a className="HelpMenu-item" href="#postanowienia-koncowe">
                    16. Postanowienia końcowe
                </a>
            </li>
        </ul>
    </nav>
);

const Content = () => (
    <div block="Content">
        <p>
            Niniejszy Regulamin określa ogólne warunki, zasady oraz sposób sprzedaży prowadzonej przez FASHION TRENDS
            GROUP SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą w Żohatyniu za pośrednictwem sklepu internetowego
            SneakerStudio.pl (zwanego dalej: „Sklepem Internetowym”) oraz określa zasady i warunki świadczenia przez
            FASHION TRENDS GROUP SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą w Żohatyniu usług nieodpłatnych
            drogą elektroniczną.
        </p>
        <p>
            RODO - Rozporządzenie Parlamentu Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie
            ochrony osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego przepływu takich
            danych oraz uchylenia dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych)
        </p>
        <p>
            Na podstawie art. 13 ust. 1 i 2 Rozporządzenia Parlamentu Europejskiego i Rady (UE) 2016/679 z 27.04 2016r.
            w sprawie ochrony danych osób fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego
            przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (ogólne rozporządzenie o ochronie danych) (Dz.Urz.
            UE L 119,s.1) (dalej: RODO), informuję, że:
        </p>
        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="definicje" />
            <label htmlFor="definicje" block="CssAccordion-label">
                <h3>1. Definicje</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    <dfn>Dni robocze</dfn>
                    oznacza dni tygodnia od poniedziałku do piątku z wyłączeniem dni ustawowo wolnych od pracy. Dostawa
                    oznacza czynność faktyczną polegającą na dostarczeniu Klientowi przez Sprzedawcę, za pośrednictwem
                    Dostawcy, Towaru określonego w zamówieniu. Dostawca oznacza podmiot, z którym współpracuje
                    Sprzedawca w zakresie dokonywania Dostawy Towarów: firmę kurierską; InPost Paczkomaty Sp. z o.o. z
                    siedzibą w Krakowie, świadcząca usługi Dostawy i obsługi systemu skrytek pocztowych (Paczkomat); DPD
                    Polska z siedzibą w Warszawie świadcząca usługi Dostawy.
                </p>
                <p>
                    <dfn>Hasło</dfn>
                    oznacza ciąg znaków literowych, cyfrowych lub innych wybranych przez Klienta podczas Rejestracji w
                    Sklepie Internetowym, wykorzystywanych w celu zabezpieczenia dostępu do Konta Klienta w Sklepie
                    Internetowym.
                </p>
                <p>
                    <dfn>Klient</dfn>
                    oznacza podmiot, na rzecz którego zgodnie z Regulaminem i przepisami prawa mogą być świadczone
                    usługi drogą elektroniczną lub z którym zawarta może być Umowa sprzedaży.
                </p>
                <p>
                    <dfn>Konsument</dfn>
                    oznacza osobę fizyczną dokonującą z przedsiębiorcą czynności prawnej niezwiązanej bezpośrednio z jej
                    działalnością gospodarczą lub zawodową.
                </p>
                <p>
                    <dfn>Konto Klienta</dfn>
                    oznacza indywidualny dla każdego Klienta panel, uruchomiony na jego rzecz przez Sprzedawcę, po
                    dokonaniu przez Klienta Rejestracji i zawarciu umowy świadczenia usługi prowadzenia Konta Klienta.
                    Login oznacza indywidualne oznaczenie Klienta, przez niego ustalone, składające się z ciągu znaków
                    literowych, cyfrowych lub innych, wymagane wraz z Hasłem do założenia Konta Klienta w Sklepie
                    Internetowym. Login jest właściwym adresem poczty elektronicznej Klienta.
                </p>
                <p>
                    <dfn>Przedsiębiorca</dfn>
                    oznacza osobę fizyczną, osobę prawną lub jednostkę organizacyjną niebędącą osobą prawną, której
                    ustawa przyznaje zdolność prawną, prowadzącą we własnym imieniu działalność gospodarczą lub zawodową
                    i dokonującą czynności prawnej związaną bezpośrednio z jej działalnością gospodarczą lub zawodową.
                </p>
                <p>
                    <dfn>Regulamin</dfn>
                    oznacza niniejszy regulamin.
                </p>
                <p>
                    <dfn>Rejestracja</dfn>
                    oznacza czynność faktyczną dokonaną w sposób określony w Regulaminie, wymaganą dla korzystania przez
                    Klienta ze wszystkich funkcjonalności Sklepu Internetowego.
                </p>
                <p>
                    <dfn>Sklep Stacjonarny</dfn>
                    oznacza miejsce przeznaczone do obsługi Klientów, pod adresem: ul. Starowiślna 55/3, 31-035 Kraków.
                </p>
                <p>
                    <dfn>Sklep Internetowy</dfn>
                    oznacza miejsce przeznaczone do obsługi Klientów, których lista dostępna jest na Stronie
                    Internetowej Sklepu.
                </p>
                <p>
                    <dfn>Sprzedawca</dfn>
                    oznacza FASHION TRENDS GROUP SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą w Żohatyniu 37-751,
                    Lipa 8, NIP: 7952542640, REGON: 367335288, KRS: 0000690278 wpisaną do Krajowego Rejestru Sądowego
                    przez Sąd Rejonowy w Rzeszowie, XII wydział gospodarczy Krajowego Rejestru Sądowego oraz wpisaną do
                    Centralnej Ewidencji i Informacji o Działalności Gospodarczej prowadzonej przez Ministra Gospodarki;
                    Kapitał zakładowy: 48 700,00 ZŁ e-mail: sklep@sneakerstudio.pl, będącego jednocześnie właścicielem
                    Sklepu Internetowego.
                </p>
                <p>
                    <dfn>Towar</dfn>
                    oznacza produkt przedstawiony przez Sprzedawcę za pośrednictwem Strony Internetowej Sklepu, mogący
                    być przedmiotem Umowy sprzedaży.
                </p>
                <p>
                    <dfn>Strona Internetowa Sklepu</dfn>
                    oznacza strony internetowe, pod którymi Sprzedawca prowadzi Sklep Internetowy, działający w domenie
                    www.sneakerstudio.pl
                </p>
                <p>
                    <dfn>Trwały nośnik</dfn>
                    oznacza materiał lub narzędzie umożliwiające Klientowi lub Sprzedawcy przechowywanie informacji
                    kierowanych osobiście do niego, w sposób umożliwiający dostęp do informacji w przyszłości przez czas
                    odpowiedni do celów, jakim te informacje służą, i które pozwalają na odtworzenie przechowywanych
                    informacji w niezmienionej postaci.
                </p>
                <p>
                    <dfn>Umowa sprzedaży</dfn>
                    oznacza umowę sprzedaży zawartą na odległość, na zasadach określonych w Regulaminie, między Klientem
                    a Sprzedawcą.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="postanowienia-ogolne" />
            <label htmlFor="postanowienia-ogolne" block="CssAccordion-label">
                <h3>2. Postanowienia ogólne i korzystanie ze Sklepu Internetowego</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Wszelkie prawa do Sklepu Internetowego, w tym majątkowe prawa autorskie, prawa własności
                    intelektualnej do jego nazwy, jego domeny internetowej, Strony Internetowej Sklepu, a także do
                    wzorców, formularzy, logotypów zamieszczanych na Stronie Internetowej Sklepu (za wyjątkiem logotypów
                    i zdjęć prezentowanych na Stronie Internetowej Sklepu w celach prezentacji towarów, do których to
                    prawa autorskie należą do podmiotów trzecich) należą do Sprzedawcy, a korzystanie z nich może
                    następować wyłącznie w sposób określony i zgodny z Regulaminem oraz zgodą Sprzedawcy wyrażoną na
                    piśmie.
                </p>
                <p>
                    Sprzedawca dołoży starań, aby korzystanie ze Sklepu Internetowego było możliwe dla użytkowników
                    Internetu z użyciem wszystkich popularnych przeglądarek internetowych, systemów operacyjnych, typów
                    urządzeń oraz typów połączeń internetowych. Minimalne wymagania techniczne umożliwiające korzystanie
                    ze Strony Internetowej Sklepu to przeglądarka internetowa w wersji co najmniej Internet Explorer 11
                    lub Chrome 39 lub FireFox 34 lub Opera 26 lub Safari 5 lub nowszych, z włączoną obsługą języka
                    Javascript, akceptująca pliki typu „cookies” oraz łącze internetowe o przepustowości co najmniej 256
                    kbit/s. Strona Internetowa Sklepu jest zoptymalizowana dla minimalnej rozdzielczości ekranu 1024x768
                    pikseli.
                </p>
                <p>
                    Sprzedawca stosuje mechanizm plików &quot;cookies&quot;, które podczas korzystania przez Klientów ze
                    Strony Internetowej Sklepu, zapisywane są przez serwer Sprzedawcy na dysku twardym urządzenia
                    końcowego Klienta. Stosowanie plików &quot;cookies&quot; ma na celu poprawne działanie Strony
                    Internetowej Sklepu na urządzeniach końcowych Klientów. Mechanizm ten nie niszczy urządzenia
                    końcowego Klienta oraz nie powoduje zmian konfiguracyjnych w urządzeniach końcowych Klientów ani w
                    oprogramowaniu zainstalowanym na tych urządzeniach. Każdy Klient może wyłączyć mechanizm „cookies” w
                    przeglądarce internetowej swojego urządzenia końcowego. Sprzedawca wskazuje, że wyłączenie „cookies”
                    może jednak spowodować utrudnienia lub uniemożliwić korzystanie ze Strony Internetowej Sklepu.
                </p>
                <p>
                    W celu złożenia zamówienia w Sklepie Internetowym za pośrednictwem Strony Internetowej Sklepu lub za
                    pośrednictwem poczty elektronicznej oraz w celu korzystania z usług dostępnych na Stronach
                    Internetowych Sklepu, konieczne jest posiadanie przez Klienta aktywnego konta poczty elektronicznej.
                    W celu złożenia zamówienia w Sklepie Internetowym przez telefon, konieczne jest posiadanie przez
                    Klienta aktywnego numeru telefonu oraz aktywnego konta poczty elektronicznej.
                </p>
                <p>
                    Zakazane jest dostarczanie przez Klienta treści o charakterze bezprawnym oraz wykorzystywanie przez
                    Klienta Sklepu Internetowego, Strony Internetowej Sklepu lub usług nieodpłatnych świadczonych przez
                    Sprzedawcę, w sposób sprzeczny z prawem, dobrymi obyczajami lub naruszający dobra osobiste osób
                    trzecich.
                </p>
                <p>
                    Sprzedawca oświadcza, iż publiczny charakter sieci Internet i korzystanie z usług świadczonych drogą
                    elektroniczną wiązać może się z zagrożeniem pozyskania i modyfikowania danych Klientów przez osoby
                    nieuprawnione, dlatego Klienci powinni stosować właściwe środki techniczne, które zminimalizują
                    wskazane wyżej zagrożenia. W szczególności powinni stosować programy antywirusowe i chroniące
                    tożsamość korzystających z sieci Internet. Sprzedawca nigdy nie zwraca się do Klienta z prośbą o
                    udostępnienie mu w jakiejkolwiek formie Hasła.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="rejestracja" />
            <label htmlFor="rejestracja" block="CssAccordion-label">
                <h3>3. Rejestracja</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    W celu utworzenia Konta Klienta, Klient obowiązany jest dokonać nieodpłatnej Rejestracji.
                    Rejestracja nie jest konieczna do złożenia zamówienia w Sklepie Internetowym. W celu Rejestracji,
                    Klient powinien wypełnić formularz rejestracyjny udostępniony przez Sprzedawcę na Stronie
                    Internetowej Sklepu i przesłać wypełniony formularz rejestracyjny drogą elektroniczną do Sprzedawcy
                    poprzez wybór odpowiedniej funkcji znajdującej się w formularzu rejestracyjnym. Podczas Rejestracji
                    Klient ustala indywidualne Hasło.
                </p>
                <p>
                    W trakcie wypełniania formularza rejestracyjnego, Klient ma możliwość i zapoznania się z
                    Regulaminem, akceptując jego treść poprzez oznaczenie odpowiedniego pola w formularzu. W trakcie
                    Rejestracji Klient może dobrowolnie wyrazić zgodę na przetwarzanie swoich danych osobowych w celach
                    marketingowych poprzez zaznaczenie odpowiedniego pola formularza rejestracyjnego. W takim przypadku
                    Sprzedawca wyraźnie informuje o celu zbierania danych osobowych Klienta, a także o znanych
                    Sprzedawcy lub przewidywanych odbiorcach tych danych.
                </p>
                <p>
                    Wyrażenie zgody oznacza w szczególności zgodę na otrzymywanie informacji handlowych od Sprzedawcy na
                    adres poczty elektronicznej Klienta, podany w formularzu rejestracyjnym. Wyrażenie przez Klienta
                    zgody na przetwarzanie jego danych osobowych w celach marketingowych nie warunkuje możliwości
                    zawarcia ze Sprzedawcą umowy świadczenia drogą elektroniczną usługi prowadzenia Konta Klienta. Zgoda
                    może być w każdej chwili cofnięta, poprzez złożenie Sprzedawcy stosownego oświadczenia Klienta na
                    Trwałym nośniku. Oświadczenie może zostać przykładowo przesłana adres Sprzedawcy za pośrednictwem
                    poczty elektronicznej.
                </p>
                <p>
                    Po przesłaniu wypełnionego formularza rejestracyjnego Klient otrzymuje niezwłocznie, drogą
                    elektroniczną na adres poczty elektronicznej podany w formularzu rejestracyjnym potwierdzenie
                    Rejestracji przez Sprzedawcę. Z tą chwilą zawarta zostaje umowa o świadczenie drogą elektroniczną
                    usługi prowadzenia Konta Klienta, zaś Klient uzyskuje możliwość dostępu do Konta Klienta i
                    dokonywania zmian podanych podczas Rejestracji danych, za wyjątkiem Loginu.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="zamowienia" />
            <label htmlFor="zamowienia" block="CssAccordion-label">
                <h3>4. Zamówienia</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Informacje zawarte na Stronie Internetowej Sklepu nie stanowią oferty Sprzedawcy w rozumieniu
                    Kodeksu Cywilnego, a jedynie zaproszenie Klientów do składania ofert zawarcia Umowy sprzedaży.
                    Klient może składać zamówienia w Sklepie Internetowym za pośrednictwem Strony Internetowej Sklepu
                    lub poczty elektronicznej przez 7 dni w tygodniu, 24 godziny na dobę. Klient może składać zamówienia
                    w Sklepie Internetowym za pośrednictwem telefonu w godzinach i dniach wskazanych na Stronie
                    Internetowej Sklepu. Klient składający zamówienie za pośrednictwem Strony Internetowej Sklepu,
                    kompletuje zamówienie wybierając Towar, którym jest zainteresowany. Dodanie Towaru do zamówienia
                    następuje przez wybór polecenia "DODAJ DO KOSZYKA" pod danym Towarem prezentowanym na Stronie
                    Internetowej Sklepu. Klient po skompletowaniu całości zamówienia i wskazaniu w „KOSZYKU” sposobu
                    Dostawy oraz formy płatności, składa zamówienie przez wysłanie formularza zamówienia do Sprzedawcy,
                    wybierając na Stronie Internetowej Sklepu przycisk „Zamawiam i płacę”.
                </p>
                <p>
                    Każdorazowo przed wysyłką zamówienia do Sprzedawcy, Klient jest informowany o łącznej cenie za
                    wybrany Towar i Dostawę, jak też o wszystkich dodatkowych kosztach jakie jest zobowiązany ponieść w
                    związku z Umową sprzedaży. Klient składający zamówienie za pośrednictwem telefonu, wykorzystuje do
                    tego numer telefoniczny podany przez Sprzedawcę na Stronie Internetowej Sklepu. Klient składa
                    zamówienie przez telefon podając Sprzedawcy nazwę Towaru spośród Towarów znajdujących się na Stronie
                    Internetowej Sklepu oraz ilość Towaru jaką chce zamówić. Następnie, po skompletowaniu całości
                    zamówienia Klient określa sposób Dostawy oraz formę płatności, a także wskazuje, wedle wyboru, swój
                    adres poczty elektronicznej albo adres korespondencyjny. Każdorazowo podczas składania przez Klienta
                    zamówienia przez telefon Sprzedawca informuje Klienta o cenie łącznej wybranych Towarów oraz łącznym
                    koszcie wybranego sposobu Dostawy, a także o wszystkich dodatkowych kosztach jakie jest zobowiązany
                    ponieść w związku z Umową sprzedaży. Po złożeniu przez Klienta zamówienia za pośrednictwem telefonu,
                    Sprzedawca prześle na Trwałym nośniku, na podany przez Klienta adres poczty elektronicznej lub
                    korespondencyjny informację zawierającą potwierdzenie warunków Umowy sprzedaży. Potwierdzenie
                    zawiera w szczególności: określenie Towaru będącego przedmiotem Umowy sprzedaży, jego cenę, koszt
                    Dostawy i informację o wszelkich innych kosztach jakie Klient zobowiązany jest ponieść w związku z
                    Umową sprzedaży.
                </p>
                <p>
                    Klient składający zamówienie za pośrednictwem poczty elektronicznej, przesyła je na adres email
                    podany przez Sprzedawcę na Stronie Internetowej Sklepu. Klient w wiadomości przesyłanej do
                    Sprzedawcy, podaje w szczególności: nazwę Towaru, kolor oraz jego ilość, spośród Towarów
                    prezentowanych na Stronie Internetowej Sklepu. Po otrzymaniu od Klienta drogą elektroniczną
                    wiadomości o której mowa, Sprzedawca przesyła Klientowi zwrotną wiadomość za pośrednictwem poczty
                    elektronicznej, podając swoje dane rejestrowe, cenę wybranych Towarów oraz możliwe formy płatności
                    oraz sposób Dostawy wraz z jego kosztem, jak też informacje o wszystkich dodatkowych płatnościach
                    jakie Klient miałby ponieść z tytułu Umowy sprzedaży. Wiadomość zawiera także informację dla
                    Klienta, że zawarcie przez niego Umowy sprzedaży za pośrednictwem poczty elektronicznej pociąga za
                    sobą obowiązek zapłaty za zamówiony Towar.
                </p>
                <p>
                    Na podstawie podanych przez Sprzedawcę informacji, Klient może złożyć zamówienie, przesyłając
                    wiadomość elektroniczną do Sprzedawcy wskazując wybraną formę płatności oraz sposób Dostawy.
                    Złożenie zamówienia stanowi złożenie Sprzedawcy przez Klienta oferty zawarcia Umowy sprzedaży
                    Towarów będących przedmiotem zamówienia. Po złożeniu zamówienia, Sprzedawca przesyła na podany przez
                    Klienta adres poczty elektronicznej potwierdzenie jego złożenia.
                </p>
                <p>
                    Następnie, po potwierdzeniu złożenia zamówienia, Sprzedawca przesyła na podany przez Klienta adres
                    poczty elektronicznej informację o przyjęciu zamówienia do realizacji. Informacja o przyjęciu
                    zamówienia do realizacji jest oświadczeniem Sprzedawcy o przyjęciu oferty, o której mowa w powyżej i
                    z chwilą jego otrzymania przez Klienta zostaje zawarta Umowa sprzedaży. Po zawarciu Umowy sprzedaży,
                    Sprzedawca potwierdza Klientowi jej warunki, przesyłając je na Trwałym nośniku na adres poczty
                    elektronicznej Klienta lub pisemnie na wskazany przez Klienta podczas Rejestracji lub składania
                    zamówienia adres.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="platnosci" />
            <label htmlFor="platnosci" block="CssAccordion-label">
                <h3>5. Płatności</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Ceny na Stronie Internetowej Sklepu zamieszczone przy danym Towarze stanowią ceny brutto i nie
                    zawierają informacji odnośnie kosztów Dostawy i wszelkich innych kosztów, które Klient będzie
                    zobowiązany ponieść w związku z Umową sprzedaży, o których Klient będzie poinformowany przy wyborze
                    sposób u Dostawy i składaniu zamówienia.
                </p>
                <p>Klient może wybrać następujące formy płatności za zamówione Towary:</p>
                <p>
                    <strong>przelew bankowy na rachunek bankowy Sprzedawcy</strong> (w tym przypadku realizacja
                    zamówienia rozpoczęta zostanie po przesłaniu Klientowi przez Sprzedawcę potwierdzenia przyjęcia
                    zamówienia, zaś wysyłka dokonana zostanie niezwłocznie po wpłynięciu środków na rachunek bankowy
                    Sprzedawcy);
                </p>
                <p>
                    <strong>przelew bankowy poprzez zewnętrzny system płatności Dotpay</strong>, obsługiwany przez firmę
                    Dotpay z siedzibą w Krakowie (w tym przypadku realizacja zamówienia rozpoczęta zostanie po
                    przesłaniu Klientowi przez Sprzedawcę potwierdzenia przyjęcia zamówienia oraz po otrzymaniu przez
                    Sprzedawcę informacji z systemu Dotpay o dokonaniu płatności przez Klienta);
                </p>
                <p>
                    <strong>przelew bankowy poprzez zewnętrzny system płatności PayPal</strong>, obsługiwany przez firmę
                    PayPal (Europe) S.à r.l. & Cie, S.C.A. z siedzibą w Luksemburgu (w tym przypadku realizacja
                    zamówienia rozpoczęta zostanie po przesłaniu Klientowi przez Sprzedawcę potwierdzenia przyjęcia
                    zamówienia oraz po otrzymaniu przez Sprzedawcę informacji z systemu PayPal o dokonaniu płatności
                    przez Klienta);
                </p>
                <p>
                    <strong>gotówką za pobraniem</strong>, płatność Dostawcy przy dokonywaniu Dostawy (w tym przypadku
                    realizacja zamówienia i jego wysyłka zostanie rozpoczęta po przesłaniu Klientowi przez Sprzedawcę
                    potwierdzenia przyjęcia zamówienia);
                </p>
                <p>
                    <strong>gotówką przy odbiorze osobistym</strong> – płatność w biurze Sprzedawcy (w tym przypadku
                    realizacja zamówienia zostanie dokonana niezwłocznie po przesłaniu Klientowi przez Sprzedawcę
                    potwierdzenia przyjęcia zamówienia, zaś Towar wydany zostanie w biurze Sprzedawcy).
                </p>
                <p>
                    Klient jest każdorazowo informowany przez Sprzedawcę na Stronie Internetowej Sklepu o terminie w
                    jakim jest zobowiązany dokonać płatności za zamówienie w wysokości wynikającej z zawartej Umowy
                    sprzedaży. W wypadku niewywiązania się przez Klienta z płatności w terminie o którym mowa w §5 ust.
                    3, Sprzedawca wyznacza Klientowi dodatkowy termin na dokonanie płatności i informuje o nim Klienta
                    na Trwałym nośniku.
                </p>
                <p>
                    Informacja o dodatkowym terminie na dokonanie płatności zawiera również informację, że po
                    bezskutecznym upływie tego terminu, Sprzedawca odstąpi od Umowy sprzedaży. W wypadku bezskutecznego
                    upływu drugiego terminu na dokonanie płatności, Sprzedawca prześle Klientowi na Trwałym nośniku
                    oświadczenie o odstąpieniu od umowy na podstawie art. 491 Kodeksu Cywilnego.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="dostawa" />
            <label htmlFor="dostawa" block="CssAccordion-label">
                <h3>6. Dostawa</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Sprzedawca realizuje Dostawę wyłącznie na terytorium Rzeczpospolitej Polskiej. Sprzedawca jest
                    zobowiązany dostarczyć Towar będący przedmiotem Umowy sprzedaży bez wad. Sprzedawca zamieszcza na
                    Stronie Internetowej Sklepu informację o liczbie Dni Roboczych potrzebnych do Dostawy i realizacji
                    zamówienia. Termin Dostawy i realizacji zamówienia wskazany na Stronie Internetowej Sklepu liczony
                    jest w Dniach roboczych zgodnie z §5 ust. 2.Termin Dostawy i realizacji zamówienia wskazany na
                    Stronie Internetowej Sklepu liczony jest w Dniach roboczych od dnia zawarcia Umowy sprzedaży w
                    wypadku wyboru przez Klienta opcji płatności „za pobraniem”. Zamówione Towary są dostarczane do
                    Klienta za pośrednictwem Dostawcy na adres wskazany w formularzu zamówienia.
                </p>
                <p>
                    W przypadku wyboru InPost Paczkomaty Sp. z o.o. z siedzibą w Krakowie jako Dostawcy, adresem Dostawy
                    będzie adres paczkomatu wybranego przez Klienta w momencie składania zamówienia. W dniu wysłania
                    Towaru do Klienta (jeśli nie wybrano możliwości osobistego odbioru Towaru) przekazywana jest na
                    adres poczty elektronicznej Klienta informacja potwierdzająca nadanie przesyłki przez Sprzedawcę.
                </p>
                <p>
                    Klient powinien zbadać doręczoną przesyłkę w czasie i w sposób przyjęty przy przesyłkach danego
                    rodzaju, w obecności pracownika Dostawcy. W razie stwierdzenia ubytku lub uszkodzenia przesyłki.
                    Klient ma również prawo żądać od pracownika Dostawcy spisania właściwego protokołu. Klient ma
                    możliwość odbioru osobistego zamówionego Towaru.
                </p>
                <p>
                    Odbioru można dokonać w sklepie stacjonarnym w Dni Robocze, w godzinach otwarcia wskazanych na
                    Stronie Internetowej Sklepu, po wcześniejszym ustaleniu ze Sprzedawcą terminu odbioru pocztą
                    elektroniczną lub telefonicznie. Zamówiony towar będzie czekał na Klienta 4 dni robocze.
                </p>
                <p>
                    Sprzedawca, zgodnie z wolą Klienta, dołącza do przesyłki będącej przedmiotem Dostawy paragon albo
                    fakturę VAT obejmującą dostarczane Towary. W przypadku nieobecności Klienta pod wskazanym przez
                    niego adresem, podanym przy składaniu zamówienia jako adres Dostawy, pracownik Dostawcy pozostawi
                    awizo lub podejmie próbę kontaktu telefonicznego celem ustalenia terminu, w którym Klient będzie
                    obecny. W przypadku zwrotnego odesłania zamówionego Towaru do Sklepu Internetowego przez Dostawcę,
                    Sprzedawca skontaktuje się z Klientem drogą elektroniczną lub telefonicznie, ustalając ponownie z
                    Klientem termin i koszt Dostawy.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="rekojmia" />
            <label htmlFor="rekojmia" block="CssAccordion-label">
                <h3>7. Rękojmia</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Sprzedawca zapewnia Dostawę Towaru pozbawionego wad fizycznych i prawnych. Sprzedawca jest
                    odpowiedzialny względem Klienta, jeżeli Towar ma wadę fizyczną lub prawną (rękojmia). Jeżeli Towar
                    ma wadę Klient może: złożyć oświadczenie o obniżeniu ceny albo odstąpieniu od Umowy sprzedaży, chyba
                    że Sprzedawca niezwłocznie i bez nadmiernych niedogodności dla Klienta wymieni Towar wadliwy na
                    wolny od wad albo wadę usunie.
                </p>
                <p>
                    Ograniczenie to nie ma zastosowania, jeżeli Towar był już wymieniony lub naprawiany przez sprzedawcę
                    albo sprzedawca nie uczynił zadość obowiązkowi wymiany Towaru na wolny od wad lub usunięcia wad.
                    Klient może zamiast zaproponowanego przez Sprzedawcę usunięcia wady żądać wymiany Towaru na wolny od
                    wad albo zamiast wymiany Towaru żądać usunięcia wady, chyba że doprowadzenie rzeczy do zgodności z
                    umową w sposób wybrany przez Klienta jest niemożliwe albo wymagałoby nadmiernych kosztów w
                    porównaniu ze sposobem proponowanym przez Sprzedawcę.
                </p>
                <p>
                    Przy ocenie nadmierności kosztów uwzględnia się wartość Towaru wolnego od wad, rodzaj i znaczenie
                    stwierdzonej wady, a także bierze się pod uwagę niedogodności, na jakie narażałby Klienta inny
                    sposób zaspokojenia. żądać wymiany Towaru wadliwego na wolny od wad albo usunięcia wady. Sprzedawca
                    jest obowiązany wymienić Towar wadliwy na wolny od wad lub usunąć wadę w rozsądnym czasie bez
                    nadmiernych niedogodności dla Klienta.
                </p>
                <p>
                    Sprzedawca może odmówić zadośćuczynienia żądaniu Klienta, jeżeli doprowadzenie do zgodności z Umową
                    sprzedaży Towaru wadliwego w sposób wybrany przez Klienta jest niemożliwe lub w porównaniu z drugim
                    możliwym sposobem doprowadzenia do zgodności z Umową sprzedaży wymagałoby nadmiernych kosztów.
                    Koszty naprawy lub wymiany ponosi Sprzedawca. Klient, który wykonuje uprawnienia z tytułu rękojmi,
                    jest obowiązany dostarczyć rzecz wadliwą na adres Sprzedawcy.
                </p>
                <p>
                    W wypadku Klienta będącego Konsumentem koszt dostarczenia pokrywa Sprzedawca. Sprzedawca odpowiada z
                    tytułu rękojmi, jeżeli wada fizyczna zostanie stwierdzona przed upływem dwóch lat od wydania Towaru
                    Klientowi. Roszczenie o usunięcie wady lub wymianę Towaru na wolny od wad przedawnia się z upływem
                    roku, lecz termin ten nie może zakończyć się przed upływem terminu określonego w zdaniu pierwszym. W
                    tym terminie Klient może odstąpić od Umowy sprzedaży lub złożyć oświadczenie o obniżeniu ceny z
                    powodu wady Towaru. Jeżeli Klient żądał wymiany Towaru na wolny od wad lub usunięcia wady, termin do
                    odstąpienia od Umowy sprzedaży lub złożenia oświadczenia o obniżeniu ceny rozpoczyna się z chwilą
                    bezskutecznego upływu terminu do wymiany Towaru lub usunięcia wady.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="reklamacje" />
            <label htmlFor="reklamacje" block="CssAccordion-label">
                <h3>8. Reklamacje</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Wszelkie reklamacje związane z Towarem lub realizacją Umowy sprzedaży, Klient może kierować w formie
                    pisemnej na adres Sprzedawcy. Sprzedawca, w ciągu 14 dni od dnia żądania zawierającego reklamację
                    ustosunkuje się do reklamacji Towaru lub reklamacji związanej z realizacją Umowy sprzedaży
                    zgłoszonej przez Klienta. Klient może zgłosić Sprzedawcy reklamację w związku z korzystaniem z usług
                    nieodpłatnych świadczonych drogą elektroniczną przez Sprzedawcę. Reklamacja może być złożona w
                    formie elektronicznej i przesłana na adres elektroniczny Sprzedawcy. W zgłoszeniu reklamacyjnym
                    Klient winien zawrzeć opis zaistniałego problemu. Sprzedawca niezwłocznie, lecz nie później niż w
                    terminie 14 dni rozpatruje reklamacje i udziela Klientowi odpowiedzi. Reklamowany towar odsyłany
                    winien być pod poniższy adres .Dokładna procedura reklamacyjną zostanie przedstawiona klientowi po
                    kontakcie z obsługą sklepu internetowego pod adresem mailowym : „sklep@sneakerstudio.pl” Adres do
                    odsyłki /przekazania reklamacji: SneakerStudio Magazyn B ul Krakowska 87, rampa numer 17, 32-050
                    Skawina. Telefon stacjonarny : +48 122100251
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="gwarancja" />
            <label htmlFor="gwarancja" block="CssAccordion-label">
                <h3>9. Gwarancja</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Towary sprzedawane przez Sprzedawcę mogą być objęte gwarancją udzieloną przez producenta Towaru bądź
                    dystrybutora. W wypadku Towarów objętych gwarancją, informacja dotycząca istnienia i treści
                    gwarancji jest każdorazowo prezentowana na Stronie Internetowej Sklepu.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="odstapienie-od-umowy" />
            <label htmlFor="odstapienie-od-umowy" block="CssAccordion-label">
                <h3>10. Odstąpienie od umowy sprzedaży</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Klient będący Konsumentem, który zawarł Umowę sprzedaży, może w terminie 30 dni odstąpić od niej bez
                    podawania przyczyny. Termin 30 dniowego okresu zwrotu nie dotyczy obuwia marki adidas Yeezy i w
                    przypadku zakupu tego modelu obuwia termin zwrotu wynosi 14 dni. Bieg terminu na odstąpienie od
                    Umowy sprzedaży rozpoczyna się od chwili objęcia Towaru w posiadanie przez Klienta.
                </p>
                <p>
                    Klient może odstąpić od Umowy sprzedaży składając Sprzedawcy oświadczenie o odstąpieniu.
                    Oświadczenie można złożyć na formularzu, którego wzór został zamieszczony przez Sprzedawcę na
                    Stronie Internetowej Sklepu pod adresem: www.sneakerstudio.pl Formularz odstąpienia. Do zachowania
                    terminu wystarczy wysłanie oświadczenia przed jego upływem.
                </p>
                <p>
                    Klient może odstąpić od Umowy sprzedaży, składając Sprzedawcy oświadczenie o odstąpieniu za
                    pośrednictwem formularza udostępnionego na stronie internetowej pod adresem: Elektroniczny Formularz
                    odstąpienia. Do zachowania terminu wystarczy wysłanie oświadczenia przed jego upływem. Sprzedawca
                    niezwłocznie potwierdza Klientowi otrzymanie formularza złożonego za pośrednictwem strony
                    internetowej.
                </p>
                <p>
                    W przypadku odstąpienia od Umowy sprzedaży, jest ona uważana za niezawartą. Jeśli Klient złożył
                    oświadczenie o odstąpieniu od Umowy sprzedaży zanim Sprzedawca przyjął jego ofertę, oferta przestaje
                    wiązać. Sprzedawca ma obowiązek niezwłocznie, nie później niż w terminie 14 dni od dnia otrzymania
                    oświadczenia Klienta o odstąpieniu od Umowy sprzedaży, zwrócić mu wszystkie dokonane przez niego
                    płatności, w tym koszt Dostawy. Sprzedawca może wstrzymać się ze zwrotem płatności otrzymanych od
                    Klienta do chwili otrzymania z powrotem Towaru lub dostarczenia przez Klienta dowodu odesłania
                    Towaru, w zależności od tego, które zdarzenie nastąpi wcześniej.
                </p>
                <p>
                    Jeżeli Klient korzystający z prawa do odstąpienia wybrał sposób dostarczenia Towaru inny niż
                    najtańszy zwykły sposób Dostawy oferowany przez Sprzedawcę, Sprzedawca nie jest zobowiązany do
                    zwrotu Klientowi poniesionych przez niego dodatkowych kosztów. Klient ma obowiązek zwrócić Towar
                    Sprzedawcy niezwłocznie, jednak nie później niż w terminie 14 dni od dnia, w którym odstąpił od
                    Umowy sprzedaży. Do zachowania terminu wystarczy odesłanie Towaru na adres Sprzedawcy przed upływem
                    tego terminu. W wypadku odstąpienia Klient ponosi tylko bezpośrednie koszty zwrotu Towaru.
                </p>
                <p>
                    Jeśli ze względu na swój charakter Towar nie może zostać w zwykłym trybie odesłany pocztą,
                    Sprzedawca informuje Klienta o kosztach zwrotu rzeczy na Stronie Internetowej Sklepu. Klient ponosi
                    odpowiedzialność za zmniejszenie wartości Towaru będące wynikiem korzystania z niego w sposób
                    wykraczający poza sposób konieczny do stwierdzenia charakteru, cech i funkcjonowania Towaru.
                    Sprzedawca dokonuje zwrotu płatności przy użyciu takiego samego sposobu zapłaty, jakiego użył
                    Klient, chyba, że Klient wyraźnie zgodził się na inny sposób zwrotu, który nie wiąże się dla niego z
                    żadnymi kosztami.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="uslugi-nieodplatne" />
            <label htmlFor="uslugi-nieodplatne" block="CssAccordion-label">
                <h3>11. Usługi nieodpłatne</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Sprzedawca świadczy na rzecz Klientów, drogą elektroniczną usługi nieodpłatne: Formularz kontaktowy;
                    Newsletter; Poleć znajomemu; Prowadzenie Konta Klienta; Zamieszczanie opinii. Usługi wskazane
                    powyżej świadczone są 7 dni w tygodniu, 24 godziny na dobę. Sprzedawca zastrzega sobie możliwość
                    wyboru i zmiany rodzaju, form, czasu oraz sposobu udzielania dostępu do wybranych wymienionych
                    usług, o czym poinformuje Klientów w sposób właściwy dla zmiany Regulaminu.
                </p>
                <p>
                    Usługa Formularz kontaktowy polega na wysłaniu za pomocą formularza umieszczonego na Stronie
                    Internetowej Sklepu wiadomości do Sprzedawcy. Rezygnacja z usługi nieodpłatnej Formularz kontaktowy,
                    możliwa jest w każdej chwili i polega na zaprzestaniu wysyłania zapytań do Sprzedawcy. Z usługi
                    Newsletter może skorzystać każdy Klient, który wprowadzi swój adres poczty elektronicznej,
                    wykorzystując w tym celu formularz rejestracyjny udostępniony przez Sprzedawcę na Stronie
                    Internetowej Sklepu. Po przesłaniu wypełnionego formularza rejestracyjnego, Klient otrzymuje
                    niezwłocznie, drogą elektroniczną na adres poczty elektronicznej podany w formularzu rejestracyjnym
                    potwierdzenie przez Sprzedawcę.
                </p>
                <p>
                    Z tą chwilą zawarta zostaje umowa o świadczenie drogą elektroniczną usługi Newsletter. Klient może
                    dodatkowo podczas Rejestracji zaznaczyć odpowiednie pole w formularzu rejestracyjnym w celu
                    subskrypcji Newsletter. Usługa Newsletter polega na przesyłaniu przez Sprzedawcę, na adres poczty
                    elektronicznej, wiadomości w formie elektronicznej zawierającej informacje o nowych produktach lub
                    usługach w ofercie Sprzedawcy. Newsletter przesyłany jest przez Sprzedawcę do wszystkich Klientów,
                    którzy dokonali subskrypcji.
                </p>
                <p>
                    Każdy Newsletter kierowany do danych Klientów zawiera, w szczególności: informację o nadawcy,
                    wypełnione pole „temat”, określające treść przesyłki oraz informację o możliwości i sposobie
                    rezygnacji z usługi nieodpłatnej Newsletter. Klient może w każdej chwili zrezygnować z otrzymywania
                    Newsletter’a przez wypisanie się z subskrypcji za pośrednictwem odnośnika zamieszczonego w każdej
                    wiadomości elektronicznej wysłanej w ramach usługi Newsletter lub za pośrednictwem aktywacji
                    odpowiedniego pola w Koncie Klienta.
                </p>
                <p>
                    Usługa nieodpłatna Poleć znajomemu polega na umożliwieniu przez Sprzedawcę Klientom wysłanie przez
                    tych Klientów do znajomego, wiadomości elektronicznej dotyczącej wybranego przez nich Towaru. Klient
                    przed wysłaniem wiadomości określa Towar mający być przedmiotem polecenia, a następnie poprzez
                    funkcję "Poleć znajomemu" wypełnia formularz podając swój adres email oraz adres email osoby
                    znajomej, której chce polecić wybrany Towar. Klient nie może wykorzystywać przedmiotowej usługi w
                    innym celu niż polecenie wybranego Towaru.
                </p>
                <p>
                    Klient nie otrzymuje wynagrodzenia lub innego typu korzyści za korzystnie z przedmiotowej usługi.
                    Rezygnacja z usługi nieodpłatnej Poleć znajomemu, możliwa jest w każdej chwili i polega na
                    zaprzestaniu wysyłania poleceń wybranych produktów znajomym Klienta. Usługa Prowadzenie Konta
                    Klienta dostępna jest po dokonaniu Rejestracji na zasadach opisanych w Regulaminie i polega na
                    udostępnieniu Klientowi dedykowanego panelu w ramach Strony Internetowej Sklepu, umożliwiającego
                    Klientowi modyfikacje danych, które podał podczas Rejestracji, jak też śledzenia stanu realizacji
                    zamówień, jak też historii zamówień już zrealizowanych. Klient, który dokonał Rejestracji może
                    zgłosić żądanie usunięcia Konta Klienta Sprzedawcy, przy czym w przypadku zgłoszenia żądania
                    usunięcia Konta Klienta przez Sprzedawcę, może ono zostać usunięte do 14 dni od zgłoszenia żądania.
                    Usługa Zamieszczanie opinii polega na umożliwieniu przez Sprzedawcę, Klientom posiadającym Konto
                    Klienta publikacji na Stronie Internetowej Sklepu indywidualnych i subiektywnych wypowiedzi Klienta
                    dotyczących w szczególności Towarów.
                </p>
                <p>
                    Rezygnacja z usługi Zamieszczanie opinii możliwa jest w każdej chwili i polega na zaprzestaniu
                    zamieszczania treści przez Klienta na Stronie Internetowej Sklepu. Sprzedawca jest uprawniony do
                    zablokowania dostępu do Konta Klienta i usług nieodpłatnych, w przypadku działania przez Klienta na
                    szkodę Sprzedawcy lub innych Klientów, naruszenia przez Klienta przepisów prawa lub postanowień
                    Regulaminu, a także, gdy zablokowanie dostępu do Konta Klienta i usług nieodpłatnych jest
                    uzasadnione względami bezpieczeństwa w szczególności: przełamywaniem przez Klienta zabezpieczeń
                    Strony Internetowej Sklepu lub inne działania hakerskie.
                </p>
                <p>
                    Zablokowanie dostępu do Konta Klienta i usług nieodpłatnych z wymienionych przyczyn trwa przez okres
                    niezbędny do rozwiązania kwestii stanowiącej podstawę zablokowania dostępu do Konta Klienta i usług
                    nieodpłatnych. Sprzedawca zawiadamia Klienta o zablokowaniu dostępu do Konta Klienta i usług
                    nieodpłatnych drogą elektroniczną na adres podany przez Klienta w formularzu rejestracyjnym.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="odpowiedzialnosc-za-tresci" />
            <label htmlFor="odpowiedzialnosc-za-tresci" block="CssAccordion-label">
                <h3>12. Odpowiedzialność klienta w zakresie umieszczanych przez niego treści.</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Zamieszczając treści oraz udostępniając je Klient dokonuje dobrowolnego rozpowszechniania treści.
                    Zamieszczane treści nie wyrażają poglądów Sprzedawcy i nie powinny być utożsamiane z jego
                    działalnością. Sprzedawca nie jest dostawcą treści, a jedynie podmiotem, który zapewnia w tym celu
                    odpowiednie zasoby teleinformatyczne.
                </p>
                <p>
                    Klient oświadcza, że: jest uprawniony do korzystania z autorskich praw majątkowych, praw własności
                    przemysłowej i/lub praw pokrewnych do – odpowiednio utworów, przedmiotów praw własności przemysłowej
                    (np. znaki towarowe) i/lub przedmiotów praw pokrewnych, które składają się na treści; umieszczenie
                    oraz udostępnienie w ramach usług, o których danych osobowych, wizerunku oraz informacji dotyczących
                    osób trzecich nastąpiło w sposób legalny, dobrowolny oraz za zgodą osób, których one dotyczą; wyraża
                    zgodę na wgląd do opublikowanych treści przez innych Klientów oraz Sprzedawcę, jak również upoważnia
                    Sprzedawcę do ich wykorzystania nieodpłatnie zgodnie z postanowieniami niniejszego Regulaminu;
                    wyraża zgodę na dokonywanie opracowań utworów w rozumieniu Ustawy o prawie autorskim i prawach
                    pokrewnych.
                </p>
                <p>
                    Klient nie jest uprawniony do: zamieszczania w ramach korzystania z usług, danych osobowych osób
                    trzecich oraz rozpowszechniania wizerunku osób trzecich bez wymaganego prawem zezwolenia lub zgody
                    osoby trzeciej; zamieszczania w ramach korzystania z usług, treści o charakterze reklamowym i/lub
                    promocyjnym. Sprzedawca ponosi odpowiedzialność za zamieszczane przez Klientów treści pod warunkiem
                    otrzymania powiadomienia zgodnie z Regulaminem.
                </p>
                <p>
                    Zabronione jest zamieszczanie przez Klientów w ramach korzystania z usług, treści które mogłyby w
                    szczególności: zostać zamieszczane w złej wierze, np. z zamiarem naruszenia dóbr osobistych osób
                    trzecich; naruszać jakiekolwiek prawa osób trzecich, w tym prawa związane z ochroną praw autorskich
                    i praw pokrewnych, ochroną praw własności przemysłowej, tajemnicą przedsiębiorstwa lub mające
                    związek ze zobowiązaniami o zachowaniu poufności; posiadać charakter obraźliwy bądź stanowić groźbę
                    skierowaną do innych osób, zawierałyby słownictwo naruszające dobre obyczaje (np. poprzez użycie
                    wulgaryzmów lub określeń powszechnie uznawanych za obraźliwe); pozostawać w sprzeczności z interesem
                    Sprzedawcy; naruszać w inny sposób postanowienia Regulaminu, dobre obyczaje, przepisy obowiązującego
                    prawa, normy społeczne lub obyczajowe.
                </p>
                <p>
                    W przypadku otrzymania powiadomienia zgodnie z Regulaminem, Sprzedawca zastrzega sobie prawo do
                    modyfikowania lub usuwania treści zamieszczanych przez Klientów w ramach korzystania przez nich z
                    usług, w szczególności w odniesieniu do treści co, do których, opierając się na doniesieniach osób
                    trzecich lub odpowiednich organów stwierdzono, że mogą one stanowić naruszenie niniejszego
                    Regulaminu lub obowiązujących przepisów prawa. Sprzedawca nie prowadzi bieżącej kontroli
                    zamieszczanych treści. Klient wyraża zgodę na nieodpłatne wykorzystywanie przez Sprzedawcę
                    umieszczonych przez niego treści w ramach Strony Internetowej Sklepu.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="naruszenia" />
            <label htmlFor="naruszenia" block="CssAccordion-label">
                <h3>13. Zgłaszanie zagrożenia lub naruszenie praw.</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    W przypadku gdy Klient lub inna osoba lub podmiot uzna, iż treść publikowana na Stronie Internetowej
                    Sklepu narusza ich prawa, dobra osobiste, dobre obyczaje, uczucia, moralność, przekonania, zasady
                    uczciwej konkurencji, knowhow, tajemnicę chronioną prawem lub na podstawie zobowiązania, może
                    powiadomić Sprzedawcę o potencjalnym naruszeniu. Sprzedawca powiadomiony o potencjalnym naruszeniu,
                    podejmuje niezwłoczne działania mające na celu usunięcie ze Strony Internetowej Sklepu, treści
                    będących przyczyną naruszenia.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="ochrona-danych" />
            <label htmlFor="ochrona-danych" block="CssAccordion-label">
                <h3>14. Ochrona danych osobowych.</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Administratorem danych osobowych Klientów przekazanych Sprzedawcy dobrowolnie w ramach Rejestracji,
                    składania zamówienia jednorazowego oraz w ramach świadczenia przez Sprzedawcę usług drogą
                    elektroniczną lub w ramach innych okoliczności określonych w Regulaminie, jest Sprzedawca.
                    Sprzedawca przetwarza dane osobowe Klientów w celu realizacji zamówień, świadczenia przez Sprzedawcę
                    usług drogą elektroniczną oraz innych celów określonych w Regulaminie. Dane są przetwarzane
                    wyłącznie na podstawie przepisów prawa lub zgody wyrażonej przez Klienta.
                </p>
                <p>
                    Zbiór danych osobowych przekazanych Sprzedawcy zgłaszany jest przez Sprzedawcę Generalnemu
                    Inspektorowi Ochrony Danych Osobowych. Dane osobowe przekazane Sprzedawcy podawane są mu
                    dobrowolnie, z tym jednak zastrzeżeniem, że niepodanie określonych w Regulaminie danych w procesie
                    Rejestracji uniemożliwia Rejestrację i założenie Konta Klienta oraz uniemożliwia złożenie i
                    realizację zamówienia Klienta, w przypadku składania zamówienia bez Rejestracji Konta Klienta.
                </p>
                <p>
                    Każdy kto przekaże Sprzedawcy swoje dane osobowe, ma prawo dostępu do ich treści oraz do ich
                    poprawiania. Sprzedawca zapewnia możliwość usunięcia danych osobowych z prowadzonego zbioru, w
                    szczególności w przypadku usunięcia Konta Klienta. Sprzedawca może odmówić usunięcia danych
                    osobowych, jeżeli Klient nie uregulował wszystkich należności wobec Sprzedawcy lub naruszył
                    obowiązujące przepisy prawa, a zachowanie danych osobowych jest niezbędne do wyjaśnienia tych
                    okoliczności i ustalenia odpowiedzialności Klienta.
                </p>
                <p>
                    Sprzedawca chroni przekazane mu dane osobowe oraz dokłada wszelkich starań w celu zabezpieczenia ich
                    przed nieuprawnionym dostępem lub wykorzystaniem. Sprzedawca przekazuje dane osobowe Klienta
                    Dostawcy w zakresie niezbędnym do realizacji Dostawy. Administratorem danych osobowych jest FASHION
                    TRENDS GROUP SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ z siedzibą w Żohatyniu 37-751, Lipa 8. Kontakt
                    z Administratorem Danych Osobowych: sklep@sneakerstudio.pl. Dane osobowe będą przetwarzane przez
                    okres 5 lat.
                </p>
                <p>
                    Klient ma prawo żądania dostępu do swoich danych osobowych, a także ma prawo do ich sprostowania,
                    usunięcia, lub ograniczenia przetwarzani, jak również ma prawo wnieść sprzeciw wobec przetwarzania,
                    a także przenoszenia danych. Klient ma prawo do wniesienia skargi do organu nadzorczego, którym jest
                    Prezes Urzędu Ochrony Danych Osobowych. Administrator zobowiązuje się do przestrzegana tajemnicy
                    związanej z danymi Klienta i nieudostępnianiem tych danych osobom nieuprawnionym.
                </p>
                <p>
                    Administrator zobowiązuje się do nie wykorzystywania danych osobowych w celach innych niż te
                    wskazane w Regulaminie. W przypadku, gdy Klient wybierze płatność poprzez system Dotpay, jego dane
                    osobowe są przekazywane w zakresie niezbędnym dla realizacji płatności spółce Dotpay S.A. z siedzibą
                    w Krakowie (30-552 Kraków, ul. Wielicka 72), wpisanej do rejestru przedsiębiorców prowadzonego przez
                    Sąd Rejonowy Kraków-Śródmieście Wydział XI Gospodarczy KRS nr 296790.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="rozwiazanie-umowy" />
            <label htmlFor="rozwiazanie-umowy" block="CssAccordion-label">
                <h3>15. Rozwiązanie umowy (nie dotyczy umowy sprzedaży).</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Zarówno Klient, jak i Sprzedawca mogą rozwiązać umowę o świadczenie usług drogą elektroniczną w
                    każdym czasie bez podania przyczyn, z zastrzeżeniem zachowania praw nabytych przez drugą stronę
                    przed rozwiązaniem ww. umowy oraz postanowień poniżej. Klient, który dokonał Rejestracji rozwiązuje
                    umowę o świadczenie usług drogą elektroniczną, poprzez wysłanie do Sprzedawcy stosownego
                    oświadczenia woli, przy użyciu dowolnego środka komunikacji na odległość, umożliwiającego zapoznanie
                    się Sprzedawcy z oświadczeniem woli Klienta. Sprzedawca wypowiada umowę o świadczenie usług drogą
                    elektroniczną poprzez wysłanie do Klienta stosownego oświadczenia woli na adres poczty
                    elektronicznej podany przez Klienta podczas Rejestracji.
                </p>
            </div>
        </div>

        <div block="CssAccordion CssAccordion--default-open">
            <input type="checkbox" id="postanowienia-koncowe" />
            <label htmlFor="postanowienia-koncowe" block="CssAccordion-label">
                <h3>16. Postawnowienia końcowe.</h3>
            </label>
            <div block="CssAccordion-content">
                <p>
                    Sprzedawca ponosi odpowiedzialność z tytułu niewykonania lub nienależytego wykonania umowy, lecz w
                    przypadku umów zawieranych z Klientami będącymi Przedsiębiorcami Sprzedawca ponosi odpowiedzialność
                    tylko w przypadku umyślnego wyrządzenia szkody i w granicach rzeczywiście poniesionych strat przez
                    Klienta będącego Przedsiębiorcą.
                </p>
                <p>
                    Treść niniejszego Regulaminu może zostać utrwalona poprzez wydrukowanie, zapisanie na nośniku lub
                    pobranie w każdej chwili ze Strony Internetowej Sklepu. W przypadku powstania sporu na gruncie
                    zawartej Umowy sprzedaży, strony będą dążyły do rozwiązania sprawy polubownie. Prawem właściwym dla
                    rozstrzygania wszelkich sporów powstałych na gruncie niniejszego Regulaminu jest prawo polskie.
                    Każdy Klient może skorzystać z pozasądowych sposobów rozpatrywania reklamacji i dochodzenia
                    roszczeń. W tym zakresie możliwe jest skorzystanie przez Klienta z mediacji. Listy stałych
                    mediatorów oraz istniejących ośrodkach mediacyjnych przekazywane są i udostępniane przez Prezesów
                    właściwych Sądów Okręgowych. Sprzedawca zastrzega sobie prawo zmiany niniejszego Regulaminu.
                </p>
                <p>
                    Wszystkie zamówienia przyjęte przez Sprzedawcę do realizacji przed dniem wejścia w życie nowego
                    Regulaminu są realizowane na podstawie Regulaminu, który obowiązywał w dniu składania zamówienia
                    przez Klienta. Zmiana Regulaminu wchodzi w życie w terminie 7 dni od opublikowania na Stronie
                    Internetowej Sklepu. Sprzedawca poinformuje Klienta na 7 dni przed wejściem w życie nowego
                    Regulaminu o zmianie Regulaminu za pomocą wiadomości przesłanej drogą elektroniczną zawierającej
                    odnośnik do tekstu zmienionego Regulaminu.
                </p>
                <p>
                    W razie, gdy Klient nie akceptuje nowej treści Regulaminu obowiązany jest zawiadomić o tym fakcie
                    Sprzedawcę, co skutkuje rozwiązaniem umowy zgodnie z postanowieniami §15. W sprawach
                    nieuregulowanych niniejszym regulaminem zastosowanie mają postanowienia Kodeksu cywilnego i
                    odpowiednich ustaw prawa polskiego, a także prawa Unii Europejskiej, w szczególności RODO".
                </p>
            </div>
        </div>
        <h3>Regulamin wchodzi w życie z dniem 28 luty 2019 r.</h3>
    </div>
);

class Aggrements extends PureComponent {
    render() {
        return (
            <main block="Agreements" aria-label="Agreements">
                <ContentWrapper wrapperMix={{ block: 'Agreements', elem: 'Wrapper' }} label="Agreements page">
                    <div block="HelpPages HelpPages-Agreements">
                        <Menu />
                        <Content />
                    </div>
                </ContentWrapper>
            </main>
        );
    }
}

export default Aggrements;

import { PureComponent, useContext, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getRouteUrl } from 'Component/Router/Router.config';
import { ThemeContext } from 'Context/ThemeContext';
import BrandsQuery from 'Query/Brands.query';
import NoMatch from 'Route/NoMatch';
import SizingPage from 'Route/SizingPage/SizingPage.component';
import BreadcrumbsDispatcher from 'Store/Breadcrumbs/Breadcrumbs.dispatcher';
import { updateMeta } from 'Store/Meta/Meta.action';
import { MatchType } from 'Type/Common';
import { fetchQuery } from 'Util/Request';
import { TABS_LABEL } from './SizingPage.config';

export const mapStateToProps = (state) => ({
    storeViewCode: state.ConfigReducer.code,
});

export const mapDispatchToProps = (dispatch) => ({
    updateBreadcrumbs: (breadcrumbs) => BreadcrumbsDispatcher.update(breadcrumbs, dispatch),
    updateMeta: (meta) => dispatch(updateMeta(meta)),
});

export class SizingPageContainer extends PureComponent {
    static propTypes = {
        match: MatchType.isRequired,
        storeViewCode: PropTypes.string.isRequired,
        updateMeta: PropTypes.func.isRequired,
        updateBreadcrumbs: PropTypes.func.isRequired,
    };

    static defaultProps = {};

    containerFunctions = {
        toggleAllBrandsList: this.toggleAllBrandsList.bind(this),
        handleTabChange: this.handleTabChange.bind(this),
    };

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            noMatch: false,
            allBrandsOpened: false,
            activeTab: 1,
            sizing: [],
            brands: [],
            title: '',
            measure: {},
        };
    }

    componentDidMount() {
        this.updateData();
    }

    componentDidUpdate(prevProps) {
        const {
            match: {
                params: { brandName },
            },
        } = this.props;

        const {
            match: {
                params: { brandName: prevBandName },
            },
        } = prevProps;

        if (brandName !== prevBandName) {
            this.updateData();
        }
    }

    handleTabChange(activeTab) {
        this.setState({ activeTab });
    }

    updateData = () => {
        const {
            updateMeta,
            updateBreadcrumbs,
            storeViewCode,
            match: {
                params: { brandName = '' },
            },
        } = this.props;

        const getBrandSizingPageQuery = fetchQuery(BrandsQuery.getBrandSizingPageQuery(brandName));
        const getBrandsQuery = fetchQuery(BrandsQuery.getBrandsQuery());

        Promise.all([getBrandSizingPageQuery, getBrandsQuery]).then((data) => {
            const [{ brands: brand }, { brands }] = data;

            if (brand && brands) {
                const {
                    meta_title,
                    title,
                    small_image_alt,
                    slider_image,
                    sizing,
                    measure_description,
                    measure_junior_img_url,
                    measure_men_img_url,
                    measure_unisex_img_url,
                    measure_women_img_url,
                } = brand[0];

                this.setState(
                    {
                        title,
                        small_image_alt,
                        slider_image,
                        brands,
                        sizing: this.prepareTabContentData(sizing),
                        isLoading: false,
                        noMatch: false,
                        measure: {
                            measure_description,
                            img_url_3: measure_junior_img_url,
                            img_url_1: measure_men_img_url,
                            img_url_4: measure_unisex_img_url,
                            img_url_2: measure_women_img_url,
                        },
                    },
                    () => {
                        updateMeta({ title: `${__('Sizing')} - ${meta_title}` });

                        updateBreadcrumbs([
                            { url: `${getRouteUrl('sizing', storeViewCode)}/${brandName}`, name: meta_title },
                            { url: `${getRouteUrl('sizing', storeViewCode)}/${brandName}`, name: __('Sizing') },
                        ]);
                    },
                );
            } else {
                this.setState({ isLoading: false, noMatch: true });
            }
        });
    };

    prepareTabContentData(data) {
        const newData = [];
        const parsedData = data.map(({ content_json, ...rest }) => ({
            content_json: JSON.parse(content_json),
            ...rest,
        }));

        TABS_LABEL.forEach(({ label, id }) => {
            const filteredData = parsedData.filter(({ type }) => type === id);

            if (filteredData.length) {
                newData.push({ data: filteredData, id, label });
            }
        });

        return newData;
    }

    toggleAllBrandsList() {
        this.setState((prevState) => ({
            allBrandsOpened: !prevState.allBrandsOpened,
        }));
    }

    render() {
        const { noMatch } = this.state;

        if (noMatch) {
            return <NoMatch {...this.props} />;
        }

        return <SizingPage {...this.state} {...this.props} {...this.containerFunctions} />;
    }
}

const SizingPageContainerThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <SizingPageContainer {...props} />;
};

export default connect(mapStateToProps, mapDispatchToProps)(SizingPageContainerThemed);

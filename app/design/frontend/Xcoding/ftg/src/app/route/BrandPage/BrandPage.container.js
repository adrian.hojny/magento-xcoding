import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { CATEGORY } from 'Component/Header/Header.config';
import { MENU_TAB } from 'Component/NavigationTabs/NavigationTabs.config';
import { ThemeContext } from 'Context/ThemeContext';
import BrandPage from 'Route/BrandPage/BrandPage.component';
import { BrandDispatcher } from 'Store/Brand/';
import { toggleBreadcrumbs } from 'Store/Breadcrumbs/Breadcrumbs.action';
import { removeLinks, updateLinks } from 'Store/Hreflinks/Hreflinks.actions';
import MetaDispatcher from 'Store/Meta/Meta.dispatcher';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import { BOTTOM_NAVIGATION_TYPE, TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { setBigOfflineNotice } from 'Store/Offline/Offline.action';
import { toggleOverlayByKey } from 'Store/Overlay/Overlay.action';
import { updateInfoLoadStatus } from 'Store/ProductListInfo/ProductListInfo.action';
import ProductListInfoDispatcher from 'Store/ProductListInfo/ProductListInfo.dispatcher';
import { CategoryTreeType } from 'Type/Category';
import { HistoryType, LocationType, MatchType } from 'Type/Common';
import { debounce } from 'Util/Request';
import {
    clearQueriesFromUrl,
    convertQueryStringToKeyValuePairs,
    getQueryParam,
    getUrlParam,
    setQueryParams,
} from 'Util/Url';
import { CategoryPageContainer } from '../CategoryPage/CategoryPage.container';

export const NoMatchDispatcher = import(
    /* webpackMode: "lazy", webpackChunkName: "dispatchers" */
    'Store/NoMatch/NoMatch.dispatcher'
);

export const mapStateToProps = (state) => ({
    storeViewCode: state.ConfigReducer.code,
    category: state.CategoryReducer.category,
    isOffline: state.OfflineReducer.isOffline,
    filters: state.ProductListInfoReducer.filters,
    sortFields: state.ProductListInfoReducer.sortFields,
    isInfoLoading: state.ProductListInfoReducer.isLoading,
    totalPages: state.ProductListReducer.totalPages,
    minPriceRange: state.ProductListInfoReducer.minPrice,
    maxPriceRange: state.ProductListInfoReducer.maxPrice,
    currentArgs: state.ProductListReducer.currentArgs,
    brandDescriptionSuffix: state.ConfigReducer.brand_desctiption_suffix,
    defaultTitle: state.ConfigReducer.default_title,
});

export const mapDispatchToProps = (dispatch) => ({
    toggleOverlayByKey: (key) => dispatch(toggleOverlayByKey(key)),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    changeNavigationState: (state) => dispatch(changeNavigationState(BOTTOM_NAVIGATION_TYPE, state)),
    requestCategory: (options) => BrandDispatcher.handleData(dispatch, options),
    requestProductListInfo: (options) => ProductListInfoDispatcher.handleData(dispatch, options),
    updateLoadStatus: (isLoading) => dispatch(updateInfoLoadStatus(isLoading)),
    updateNoMatch: (options) => NoMatchDispatcher.updateNoMatch(dispatch, options),
    setBigOfflineNotice: (isBig) => dispatch(setBigOfflineNotice(isBig)),
    updateMetaFromCategory: (category) => MetaDispatcher.updateWithCategory(category, dispatch),
    toggleBreadcrumbs: (visible) => dispatch(toggleBreadcrumbs(visible)),
    updateLinks: (links) => dispatch(updateLinks(links)),
    removeLinks: () => dispatch(removeLinks()),
});

export const UPDATE_FILTERS_FREQUENCY = 0;
export const LOADING_TIME = 500;

export class BrandPageContainer extends CategoryPageContainer {
    static propTypes = {
        history: HistoryType.isRequired,
        category: CategoryTreeType.isRequired,
        location: LocationType.isRequired,
        match: MatchType.isRequired,
        requestCategory: PropTypes.func.isRequired,
        changeHeaderState: PropTypes.func.isRequired,
        changeNavigationState: PropTypes.func.isRequired,
        requestProductListInfo: PropTypes.func.isRequired,
        setBigOfflineNotice: PropTypes.func.isRequired,
        updateMetaFromCategory: PropTypes.func.isRequired,
        updateLoadStatus: PropTypes.func.isRequired,
        updateNoMatch: PropTypes.func.isRequired,
        toggleBreadcrumbs: PropTypes.func.isRequired,
        filters: PropTypes.objectOf(PropTypes.shape).isRequired,
        sortFields: PropTypes.shape({
            options: PropTypes.array,
        }).isRequired,
        isInfoLoading: PropTypes.bool.isRequired,
        isOffline: PropTypes.bool.isRequired,
        categoryIds: PropTypes.number,
        isOnlyPlaceholder: PropTypes.bool,
        isSearchPage: PropTypes.bool,
        brandDescriptionSuffix: PropTypes.string,
        defaultTitle: PropTypes.string,
    };

    static defaultProps = {
        categoryIds: 0,
        isOnlyPlaceholder: false,
        isSearchPage: false,
    };

    config = {
        sortKey: 'name',
        sortDirection: 'ASC',
    };

    containerFunctions = {
        onSortChange: this.onSortChange.bind(this),
        getIsNewCategory: this.getIsNewCategory.bind(this),
        updateFilter: this.updateFilter.bind(this),
        getFilterUrl: this.getFilterUrl.bind(this),
        clearFilters: this._clearFilters.bind(this),
    };

    _debounceRequestCategoryProductsInfo = debounce(
        () => this._requestCategoryProductsInfo(),
        UPDATE_FILTERS_FREQUENCY,
    );

    componentDidMount() {
        const { isOnlyPlaceholder, updateLoadStatus, toggleBreadcrumbs } = this.props;

        if (isOnlyPlaceholder) {
            updateLoadStatus(true);
        }

        // request data only if URL does not match loaded category
        if (this.getIsNewCategory()) {
            this._requestCategoryWithPageList();
            debounce(this.setOfflineNoticeSize, LOADING_TIME)();
        } else {
            this._onCategoryUpdate();
        }

        toggleBreadcrumbs(false);
    }

    componentDidUpdate(prevProps) {
        const {
            category: { id },
            isOffline,
            match: { params },
        } = this.props;

        const {
            category: { id: prevId },
            match: { params: prevParams },
        } = prevProps;

        if (isOffline) {
            debounce(this.setOfflineNoticeSize, LOADING_TIME)();
        }

        // update breadcrumbs only if category has changed or brand name
        if (id !== prevId || params?.brandName !== prevParams?.brandName) {
            this._onCategoryUpdate();
        }

        this._updateData(prevProps);
    }

    componentWillUnmount() {
        this.props.toggleBreadcrumbs(true);
    }

    onSortChange(sortDirection, sortKey) {
        const { location, history } = this.props;

        setQueryParams({ sortKey }, location, history);
        setQueryParams({ sortDirection }, location, history);
    }

    setOfflineNoticeSize = () => {
        const { setBigOfflineNotice, isInfoLoading } = this.props;

        if (isInfoLoading) {
            setBigOfflineNotice(true);
        } else {
            setBigOfflineNotice(false);
        }
    };

    getFilterUrl(filterName, filterArray, isFull = true) {
        const {
            location: { pathname },
        } = this.props;
        const selectedFilters = this._getNewSelectedFiltersString(filterName, filterArray);
        return `${isFull ? `${pathname}?customFilters=` : ''}${this._formatSelectedFiltersString(selectedFilters)}`;
    }

    getIsNewCategory() {
        const { category: { url_path } = {} } = this.props;
        return url_path !== this._getCategoryUrlPath();
    }

    containerProps = () => ({
        filter: this.getFilter(),
        search: this._getSearchParam(),
        selectedSort: this.getSelectedSortFromUrl(),
        selectedFilters: this._getSelectedFiltersFromUrl(),
        isContentFiltered: this.isContentFiltered(),
        isMatchingListFilter: this.getIsMatchingListFilter(),
    });

    isContentFiltered() {
        const { customFilters, priceMin, priceMax } = this.urlStringToObject();
        return !!(customFilters || priceMin || priceMax);
    }

    urlStringToObject() {
        const {
            location: { search },
        } = this.props;

        return search
            .substr(1)
            .split('&')
            .reduce((acc, part) => {
                const [key, value] = part.split('=');
                return { ...acc, [key]: value };
            }, {});
    }

    updateSearch(value) {
        const { location, history } = this.props;

        setQueryParams(
            {
                search: value,
                page: '',
            },
            location,
            history,
        );
    }

    updateFilter(filterName, filterArray) {
        const { location, history } = this.props;

        setQueryParams(
            {
                customFilters: this.getFilterUrl(filterName, filterArray, false),
                page: '',
            },
            location,
            history,
        );
    }

    _updateData(prevProps) {
        const {
            categoryIds,
            location: { search },
        } = this.props;
        const {
            categoryIds: prevCategoryIds,
            location: { search: prevSearch },
        } = prevProps;

        // ComponentDidUpdate fires multiple times, to prevent getting same data we check that url has changed
        // getIsNewCategory prevents getting Category data, when sort or filter options have changed
        if (this._urlHasChanged(location, prevProps) && this.getIsNewCategory()) {
            this._requestCategoryWithPageList();
            return;
        }

        if (categoryIds !== prevCategoryIds) {
            this._requestCategoryWithPageList();
            return;
        }

        if (!this._compareQueriesByFilters(search, prevSearch)) {
            this._debounceRequestCategoryProductsInfo();
        }
    }

    _getNewSelectedFiltersString(filterName, filterArray) {
        const prevCustomFilters = this._getSelectedFiltersFromUrl();
        const customFilers = {
            ...prevCustomFilters,
            [filterName]: filterArray,
        };

        return Object.entries(customFilers)
            .reduce((accumulator, [filterKey, filterValue]) => {
                if (filterValue.length) {
                    const filterValues = filterValue.sort().join(',');

                    accumulator.push(`${filterKey}:${filterValues}`);
                }

                return accumulator;
            }, [])
            .sort()
            .join(';');
    }

    _formatSelectedFiltersString(string) {
        const hasTrailingSemicolon = string[string.length - 1] === ';';
        const hasLeadingSemicolon = string[0] === ';';

        if (hasLeadingSemicolon) {
            return this._formatSelectedFiltersString(string.slice(0, -1));
        }

        if (hasTrailingSemicolon) {
            return string.slice(1);
        }

        return string;
    }

    _getSearchParam() {
        const search = getQueryParam('search', location);
        return search ? decodeURIComponent(search) : '';
    }

    _getSelectedFiltersFromUrl() {
        const { location } = this.props;
        const selectedFiltersString = (getQueryParam('customFilters', location) || '').split(';');
        return selectedFiltersString.reduce((acc, filter) => {
            if (!filter) {
                return acc;
            }
            const [key, value] = filter.split(':');
            return { ...acc, [key]: value.split(',') };
        }, {});
    }

    getSelectedSortFromUrl() {
        const {
            location,
            category: { default_sort_by },
        } = this.props;
        const { sortKey: globalDefaultSortKey, sortDirection: defaultSortDirection } = this.config;
        const sortDirection = getQueryParam('sortDirection', location) || defaultSortDirection;
        const defaultSortKey = default_sort_by || globalDefaultSortKey;
        const sortKey = getQueryParam('sortKey', location) || defaultSortKey;
        return { sortDirection, sortKey };
    }

    _getCategoryUrlPath() {
        const { location, match } = this.props;
        const path = getUrlParam(match, location);
        return path.indexOf('search') === 0 ? null : path;
    }

    _getSelectedPriceRangeFromUrl() {
        const { location } = this.props;
        const min = +getQueryParam('priceMin', location);
        const max = +getQueryParam('priceMax', location);
        return { min, max };
    }

    getFilter() {
        const customFilters = this._getSelectedFiltersFromUrl();
        const priceRange = this._getSelectedPriceRangeFromUrl();
        const {
            match: {
                params: { brandName },
            },
        } = this.props;

        return {
            priceRange,
            // categoryIds,
            customFilters,
            brandName,
            // categoryUrlPath
        };
    }

    _getProductListOptions(currentPage) {
        // const { categoryIds } = this.props;
        // const categoryUrlPath = !categoryIds ? this._getCategoryUrlPath() : null;
        // const customFilters = this._getSelectedFiltersFromUrl();
        const {
            match: {
                params: { brandName },
            },
        } = this.props;

        return {
            args: {
                filter: {
                    brandName,
                },
            },
            requireInfo: true,
            currentPage,
        };
    }

    _onCategoryUpdate() {
        const { category, updateNoMatch } = this.props;
        const { is_active, isLoading } = category;

        if (!isLoading && !is_active) {
            updateNoMatch({ noMatch: true });
        } else {
            const { updateMetaFromCategory, category } = this.props;

            updateMetaFromCategory(category);
            this._updateHeaderState();
            this._updateNavigationState();
        }
    }

    _updateNavigationState() {
        const { changeNavigationState } = this.props;

        changeNavigationState({
            name: MENU_TAB,
            isVisibleOnScroll: true,
        });
    }

    _updateHeaderState() {
        const {
            changeHeaderState,
            category: { name },
            history,
        } = this.props;

        const {
            location: { state: { isFromCategory } = {} },
        } = history;

        const onBackClick = isFromCategory ? () => history.goBack() : () => history.push('/menu');

        changeHeaderState({
            name: CATEGORY,
            title: name,
            onBackClick,
        });
    }

    _requestCategoryProductsInfo() {
        const { requestProductListInfo } = this.props;
        requestProductListInfo(this._getProductListOptions(1));
    }

    getIsMatchingListFilter() {
        const { location, currentArgs: { currentPage, sort, filter } = {} } = this.props;
        /**
         * ? implementation bellow blinks, implementation with categoryIds check only does not show loading when selecting filters.
         * TODO: resolve it to be a combination of these two behaviour
         */

        // Data used to request category matches current data
        return (
            JSON.stringify(filter) === JSON.stringify(this.getFilter()) &&
            JSON.stringify(sort) === JSON.stringify(this.getSelectedSortFromUrl()) &&
            currentPage === +(getQueryParam('page', location) || 1)
        );
    }

    _requestCategoryWithPageList() {
        this._requestCategoryProductsInfo();
    }

    _compareQueriesWithFilter(search, prevSearch, filter) {
        const currentParams = filter(convertQueryStringToKeyValuePairs(search));
        const previousParams = filter(convertQueryStringToKeyValuePairs(prevSearch));
        return JSON.stringify(currentParams) === JSON.stringify(previousParams);
    }

    _compareQueriesWithoutPage(search, prevSearch) {
        return this._compareQueriesWithFilter(search, prevSearch, ({ page, ...filteredParams }) => filteredParams);
    }

    _compareQueriesByFilters(search, prevSearch) {
        return this._compareQueriesWithFilter(search, prevSearch, ({ customFilters }) => customFilters);
    }

    _urlHasChanged(location, prevProps) {
        const { pathname, search } = location;
        const {
            location: { pathname: prevPathname, search: prevSearch },
        } = prevProps;
        const pathnameHasChanged = pathname !== prevPathname;
        const searchQueryHasChanged = !this._compareQueriesWithoutPage(search, prevSearch);

        return pathnameHasChanged || searchQueryHasChanged;
    }

    _clearFilters() {
        const { location, history } = this.props;
        const { sortKey: defaultSortKey, sortDirection: defaultSortDirection } = this.config;

        const sortDirection = getQueryParam('sortDirection', location) || defaultSortDirection;
        const sortKey = getQueryParam('sortKey', location) || defaultSortKey;
        const page = getQueryParam('page', location) || 1;

        clearQueriesFromUrl(history);
        setQueryParams({ sortKey, sortDirection, page }, location, history);
    }

    render() {
        const { pageSize } = this.config;

        return (
            <BrandPage {...this.props} pageSize={pageSize} {...this.containerFunctions} {...this.containerProps()} />
        );
    }
}

const BrandPageContainerThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <BrandPageContainer {...props} />;
};

export default connect(mapStateToProps, mapDispatchToProps)(BrandPageContainerThemed);

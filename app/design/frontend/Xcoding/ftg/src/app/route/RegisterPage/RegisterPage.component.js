import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import CmsBlock from 'Component/CmsBlock';
import ContentWrapper from 'Component/ContentWrapper';
import Field from 'Component/Field/Field.container';
import Form from 'Component/Form';
import Link from 'Component/Link';
import Loader from 'Component/Loader';
import ReCaptchaField from 'Component/ReCaptchaField';
import { DATA_COMPLIANCE_INFO_ID, REGISTER_INFO_ID } from 'Route/RegisterPage/RegisterPage.container';

import './RegisterPage.style.scss';

class RegisterPage extends PureComponent {
    static propTypes = {
        isLoading: PropTypes.bool.isRequired,
        onCreateAccountAttempt: PropTypes.func.isRequired,
        onCreateAccountSuccess: PropTypes.func.isRequired,
        termsData: PropTypes.object.isRequired,
    };

    renderDataCompliance() {
        return (
            <div block="RegisterPage" elem="DataCompliance">
                <CmsBlock identifier={DATA_COMPLIANCE_INFO_ID} withoutWrapper />
            </div>
        );
    }

    renderForm() {
        const { onCreateAccountAttempt, onCreateAccountSuccess, termsData } = this.props;

        return (
            <Form
                key="create-account"
                onSubmit={onCreateAccountAttempt}
                onSubmitSuccess={onCreateAccountSuccess}
                onSubmitError={onCreateAccountAttempt}
            >
                <Field
                    type="text"
                    label={__('First Name')}
                    id="firstname"
                    name="firstname"
                    validation={['notEmpty']}
                    autoComplete="off"
                />
                <Field type="text" label={__('Last Name')} id="lastname" name="lastname" validation={['notEmpty']} />
                <Field
                    type="text"
                    label="Email"
                    id="email"
                    name="email"
                    autoComplete="off"
                    validation={['notEmpty', 'email']}
                />
                <Field
                    type="password"
                    label={__('Password')}
                    id="password"
                    name="password"
                    validation={['notEmpty', 'password']}
                    autoComplete="off"
                />
                <Field
                    type="password"
                    label={__('Confirm password')}
                    id="confirm_password"
                    name="confirm_password"
                    validation={['notEmpty', 'password', 'password_match']}
                />
                <div block="RegisterPage" elem="Checkboxes">
                    <Field
                        type="checkbox"
                        value="is_subscribed"
                        label={__('Subscribe to newsletter')}
                        id="is_subscribed"
                        mix={{ block: 'RegisterPage', elem: 'Checkbox' }}
                        name="is_subscribed"
                    />

                    {termsData?.content && (
                        <Field
                            type="checkbox"
                            value="accept_agreements"
                            label={<div dangerouslySetInnerHTML={{ __html: termsData.content }} />}
                            id="accept_agreements"
                            mix={{ block: 'RegisterPage', elem: 'Checkbox' }}
                            name="accept_agreements"
                            validation={['isChecked']}
                        />
                    )}

                    <ReCaptchaField />
                </div>
                <div block="RegisterPage" elem="Buttons">
                    <button block="Button" type="submit" mods={{ green: true, fullWidth: true }}>
                        {__('Sign up')}
                    </button>
                </div>
            </Form>
        );
    }

    render() {
        const { isLoading } = this.props;

        return (
            <ContentWrapper wrapperMix={{ block: 'RegisterPage' }} label="Register page">
                <div block="RegisterPage" elem="FormWrapper">
                    <Loader isLoading={isLoading} />
                    <h1>{__('Sign-up')}</h1>
                    <p block="RegisterPage" elem="SignInMessage">
                        {`${__('Already have an account?')} `}
                        <Link to="/sign-in">{__('Sign in')}</Link>
                    </p>
                    {this.renderForm()}
                    {this.renderDataCompliance()}
                </div>
                <div block="RegisterPage" elem="ImageWrapper">
                    <CmsBlock identifier={REGISTER_INFO_ID} withoutWrapper />
                </div>
            </ContentWrapper>
        );
    }
}

export default RegisterPage;

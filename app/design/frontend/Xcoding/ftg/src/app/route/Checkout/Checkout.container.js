import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import { PAYMENT_TOTALS } from 'SourceRoute/Checkout/Checkout.config';
import {
    CheckoutContainer as SourceCheckoutContainer,
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
} from 'SourceRoute/Checkout/Checkout.container';

import { PRZELEWY24, PRZELEWY24_CARD } from 'Component/CheckoutPayments/CheckoutPayments.config';
import { CART_TAB } from 'Component/NavigationTabs/NavigationTabs.config';
import { BLIK_CODE_NAME } from 'Component/Przelewy24/Przelewy24.config';
import { ThemeContext } from 'Context/ThemeContext';
import CheckoutQuery from 'Query/Checkout.query';
import { BILLING_STEP, DETAILS_STEP, REDIRECT_STEP, SHIPPING_STEP } from 'Route/Checkout/Checkout.component';
import { GUEST_QUOTE_ID } from 'Store/Cart/Cart.dispatcher';
import { reloadHeader } from 'Store/Navigation/Navigation.action';
import { setFormFieldValue } from 'Store/P24/P24.actions';
import { isSignedIn } from 'Util/Auth';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { getGuestCartId } from 'Util/Cart';
import { fetchMutation } from 'Util/Request';
import { getQueryParam } from 'Util/Url';

const CartDispatcher = import(
    /* webpackMode: "lazy", webpackChunkName: "dispatchers" */
    'Store/Cart/Cart.dispatcher'
);

export const ORDER_DATA_EXPIRATION_TIME = 3600;
export const REDIRECT_TIME = 5000; // 5 seconds
const ADDITIONAL_ORDER_INFO = 'ADDITIONAL_ORDER_INFO';

const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    storeCode: state.ConfigReducer.code,
    selectedShippingMethod: state.CartReducer.shippingMethod,
});

const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    resetBlikFormField: () => dispatch(setFormFieldValue(BLIK_CODE_NAME, null)),
    reloadHeader: () => dispatch(reloadHeader()),
    reloadCart: () => CartDispatcher.then(({ default: dispatcher }) => dispatcher.updateInitialCartData(dispatch)),
});

export class CheckoutContainer extends SourceCheckoutContainer {
    containerFunctions = {
        setLoading: this.setLoading.bind(this),
        setDetailsStep: this.setDetailsStep.bind(this),
        savePaymentInformation: this.savePaymentInformation.bind(this),
        saveAddressInformation: this.saveAddressInformation.bind(this),
        onShippingEstimationFieldsChange: this.onShippingEstimationFieldsChange.bind(this),
        onEmailChange: this.onEmailChange.bind(this),
        onCreateUserChange: this.onCreateUserChange.bind(this),
        onPasswordChange: this.onPasswordChange.bind(this),
        goBack: this.goBack.bind(this),
        onAdditionalInformationChange: this.onAdditionalInformationChange.bind(this),
    };

    constructor(props) {
        super(props);

        const {
            toggleBreadcrumbs,
            totals: { is_virtual },
            location,
        } = props;

        toggleBreadcrumbs(false);

        const orderId = getQueryParam('order_id', location);

        const orderData = orderId ? BrowserDatabase.getItem(`order${orderId}`) : undefined;

        this.state = {
            isLoading: is_virtual,
            isDeliveryOptionsLoading: false,
            requestsSent: 0,
            paymentMethods: [],
            shippingMethods: [],
            shippingAddress: {},
            checkoutStep: this.getCheckoutStep(is_virtual, orderData),
            orderID: '',
            paymentTotals: BrowserDatabase.getItem(PAYMENT_TOTALS) || {},
            email: '',
            isCreateUser: false,
            isGuestEmailSaved: false,
            shippingAdditionalInformation: {},
            orderData,
        };

        if (is_virtual) {
            this._getPaymentMethods();
        }
    }

    componentDidMount() {
        const { history, guest_checkout, updateMeta, reloadHeader, reloadCart } = this.props;

        // if guest checkout is disabled and user is not logged in => throw him to homepage
        if (!guest_checkout && !isSignedIn()) {
            history.push('/');
        }

        reloadCart();
        updateMeta({ title: __('Checkout') });
        reloadHeader();
    }

    componentWillUnmount() {
        const { toggleBreadcrumbs, reloadCart } = this.props;
        toggleBreadcrumbs(true);
        reloadCart();
    }

    componentDidUpdate(prevProps) {
        const {
            match,
            customer: { email },
            totals: { quote_currency_code, shipping_amount },
        } = this.props;
        const step = match?.params?.step || '';
        const prevStep = prevProps.match?.params?.step || '';
        const { email: stateEmail, shippingAdditionalInformation } = this.state;

        if (step !== prevStep && step === 'shipping') {
            this.setState({
                isLoading: false,
                checkoutStep: SHIPPING_STEP,
            });
        }

        if (step !== prevStep && step === 'billing') {
            const dataToSave = {
                email,
                stateEmail,
                shippingAdditionalInformation,
                quote_currency_code,
                shipping_amount,
            };

            BrowserDatabase.setItem(dataToSave, ADDITIONAL_ORDER_INFO);
        }
    }

    _getGuestCartId = () => getGuestCartId();

    saveGuestEmail() {
        const { email } = this.state;
        const guestCartId = this._getGuestCartId();
        const mutation = CheckoutQuery.getSaveGuestEmailMutation(email, guestCartId);

        return fetchMutation(mutation).then(
            ({ setGuestEmailOnCart: data }) => {
                if (data) {
                    this.setState({ isGuestEmailSaved: true });
                }

                return true;
            },
            this._handleError
        );
    }

    onAdditionalInformationChange(data) {
        this.setState({ shippingAdditionalInformation: data });
    }

    getCheckoutStep(is_virtual, orderData) {
        if (is_virtual) {
            return BILLING_STEP;
        }
        if (orderData) {
            return DETAILS_STEP;
        }

        return SHIPPING_STEP;
    }

    async savePaymentMethodAndPlaceOrder(paymentInformation) {
        const {
            paymentMethod: { code, additional_data: { przelewy24 } = {}, additional_data },
        } = paymentInformation;
        const {
            customer: { email },
            totals: { quote_currency_code, shipping_amount },
        } = this.props;
        const { email: stateEmail, shippingAdditionalInformation, paymentMethods } = this.state;

        const guest_cart_id = !isSignedIn() ? this._getGuestCartId() : '';

        const paymentInstruction = (paymentMethods.find(({ code: methodCode }) => methodCode === code) || {})
            .instructions;

        const isP24 = code === PRZELEWY24 || code === PRZELEWY24_CARD;
        const isBlik = przelewy24?.method === 154;

        let paymentMethodInput = {
            guest_cart_id,
            payment_method: {
                code,
                [code]: additional_data,
            },
        };

        if (isP24)
            paymentMethodInput = {
                ...paymentMethodInput,
                payment_method: {
                    code,
                },
                additional_data: JSON.stringify(przelewy24),
            };

        try {
            await fetchMutation(CheckoutQuery.getSetPaymentMethodOnCartMutation(paymentMethodInput));
            const getPlaceOrderMutationName = isP24 ? 'getPlaceP24OrderMutation' : 'getPlaceOrderMutation';
            const orderData = await fetchMutation(CheckoutQuery[getPlaceOrderMutationName](guest_cart_id));

            const {
                placeOrder: { order: { order_id, order_number, payment_redirect_url } = {}, order },
            } = orderData;

            const dataToSave = {
                ...order,
                email: email || stateEmail,
                shippingAdditionalInformation,
                currencyCode: quote_currency_code,
                shippingAmount: shipping_amount,
                paymentInstruction,
                isBlik,
                isP24,
            };

            BrowserDatabase.setItem(dataToSave, `order${isP24 ? order_number : order_id}`, ORDER_DATA_EXPIRATION_TIME);

            this.cleanCart();

            if (payment_redirect_url) {
                this.setRedirectStep(payment_redirect_url);
            } else {
                this.setDetailsStep(dataToSave);
            }
        } catch (e) {
            console.log('error', e);
            this._handleError(e);
        }
    }

    setRedirectStep(redirectUrl) {
        this.setState({
            isLoading: false,
            paymentTotals: {},
            checkoutStep: REDIRECT_STEP,
        });

        window.setTimeout(() => {
            window.location.href = redirectUrl;
        }, REDIRECT_TIME);
    }

    cleanCart() {
        const { resetCart, resetBlikFormField, storeCode } = this.props;

        // For some reason not logged in user cart preserves qty in it
        if (!isSignedIn()) {
            BrowserDatabase.deleteItem(`${GUEST_QUOTE_ID}_${storeCode}`);
        }

        BrowserDatabase.deleteItem(PAYMENT_TOTALS);
        resetBlikFormField();
        resetCart();
    }

    setDetailsStep(order) {
        const { setNavigationState } = this.props;
        this.setState({
            isLoading: false,
            paymentTotals: {},
            checkoutStep: DETAILS_STEP,
            orderData: order,
        });

        setNavigationState({
            name: CART_TAB,
        });
    }

    async createUserOrSaveGuest() {
        const {
            createAccount,
            totals: { is_virtual },
        } = this.props;

        const {
            email,
            password,
            isCreateUser,
            shippingAddress: { firstname, lastname },
        } = this.state;

        if (!isCreateUser) {
            return this.saveGuestEmail();
        }

        let gRecaptchaResponse = null;

        if (window.grecaptcha) {
            gRecaptchaResponse = window.grecaptcha.getResponse();
        }

        const options = {
            customer: {
                email,
                firstname,
                lastname,
            },
            password,
            gRecaptchaResponse,
        };

        const creation = await createAccount(options);

        if (!creation) {
            return creation;
        }

        if (!is_virtual) {
            return this.setShippingAddress();
        }

        return true;
    }
}

const CheckoutContainerThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <CheckoutContainer {...props} />;
};

export { mapDispatchToProps, mapStateToProps };

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutContainerThemed);

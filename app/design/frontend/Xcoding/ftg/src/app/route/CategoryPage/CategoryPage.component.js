import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import CategoryDetails from 'Component/CategoryDetails';
import CategoryFilterOverlay from 'Component/CategoryFilterOverlay';
import { CATEGORY_FILTER_OVERLAY_ID } from 'Component/CategoryFilterOverlay/CategoryFilterOverlay.config';
import CategoryItemsCount from 'Component/CategoryItemsCount';
import CategoryProductList from 'Component/CategoryProductList';
import ContentWrapper from 'Component/ContentWrapper';
import Html from 'Component/Html';
import SubcategorySelector from 'Component/SubcategorySelector';
import { CategoryTreeType } from 'Type/Category';
import { FilterInputType, FilterType } from 'Type/ProductList';

import './CategoryPage.extend.style';

export class CategoryPage extends PureComponent {
    static propTypes = {
        category: CategoryTreeType.isRequired,
        filters: PropTypes.objectOf(PropTypes.shape).isRequired,
        sortFields: PropTypes.shape({
            options: PropTypes.array,
        }).isRequired,
        selectedSort: PropTypes.shape({
            sortDirection: PropTypes.oneOf(['ASC', 'DESC']),
            sortKey: PropTypes.string,
        }).isRequired,
        currentArgs: PropTypes.shape({
            currentPage: PropTypes.number,
        }).isRequired,
        onSortChange: PropTypes.func.isRequired,
        toggleOverlayByKey: PropTypes.func.isRequired,
        selectedFilters: FilterType.isRequired,
        filter: FilterInputType.isRequired,
        search: PropTypes.string,
        isContentFiltered: PropTypes.bool,
        isMatchingListFilter: PropTypes.bool,
        isMatchingInfoFilter: PropTypes.bool,
        totalPages: PropTypes.number,
    };

    static defaultProps = {
        isContentFiltered: true,
        isMatchingListFilter: false,
        isMatchingInfoFilter: false,
        totalPages: 1,
        search: '',
    };

    onFilterButtonClick = this.onFilterButtonClick.bind(this);

    onFilterButtonClick() {
        const { toggleOverlayByKey } = this.props;
        toggleOverlayByKey(CATEGORY_FILTER_OVERLAY_ID);
    }

    renderCategoryDetails() {
        const { category } = this.props;

        return <CategoryDetails category={category} />;
    }

    renderFilterButton() {
        const { isContentFiltered, totalPages } = this.props;

        if (!isContentFiltered && totalPages === 0) {
            return null;
        }

        return (
            <button block="CategoryPage" elem="Filter" onClick={this.onFilterButtonClick}>
                {__('Filter')}
            </button>
        );
    }

    renderFilterOverlay() {
        const { filters, selectedFilters, isMatchingInfoFilter, sortFields, selectedSort, onSortChange } = this.props;

        return (
            <CategoryFilterOverlay
                sortFields={sortFields}
                selectedSort={selectedSort}
                onSortChange={onSortChange}
                availableFilters={filters}
                customFiltersValues={selectedFilters}
                isMatchingInfoFilter={isMatchingInfoFilter}
            />
        );
    }

    renderItemsCount() {
        const { isMatchingListFilter } = this.props;

        return <CategoryItemsCount isMatchingListFilter={isMatchingListFilter} />;
    }

    renderCategoryProductList() {
        const { filter, search, selectedSort, selectedFilters, isMatchingListFilter } = this.props;

        return (
            <div block="CategoryPage" elem="ProductListWrapper">
                {this.renderItemsCount()}
                <CategoryProductList
                    filter={filter}
                    search={search}
                    sort={selectedSort}
                    selectedFilters={selectedFilters}
                    isMatchingListFilter={isMatchingListFilter}
                />
            </div>
        );
    }

    renderCmsBlock() {
        const {
            category: { cms_block },
            currentArgs: { currentPage },
        } = this.props;

        if (!cms_block) {
            return null;
        }

        const { content, disabled } = cms_block;

        if (disabled || currentPage !== 1) {
            return null;
        }

        return (
            <div block="CategoryPage" elem="CMS">
                <Html content={content} />
            </div>
        );
    }

    renderCategoryDescription() {
        const {
            category: { description },
            currentArgs: { currentPage },
        } = this.props;

        if (!description || currentPage !== 1) {
            return null;
        }

        return <Html content={description} />;
    }

    renderContent() {
        const { filter, search, selectedSort, selectedFilters, isMatchingListFilter, category } = this.props;

        return (
            <>
                <div block="CategoryPage" elem="DetailsWrapper">
                    {this.renderCategoryDetails()}
                    {this.renderItemsCount()}
                </div>
                {this.renderSubcategorySelector()}
                {this.renderFilterOverlay()}
                <aside block="CategoryPage" elem="Miscellaneous">
                    {this.renderFilterButton()}
                </aside>
                <div block="CategoryPage" elem="ProductListWrapper">
                    <CategoryProductList
                        filter={filter}
                        search={search}
                        sort={selectedSort}
                        selectedFilters={selectedFilters}
                        isMatchingListFilter={isMatchingListFilter}
                    />
                </div>
                {this.renderCategoryDescription()}
                {this.renderCmsBlock()}
            </>
        );
    }

    renderSubcategorySelector() {
        const { category } = this.props;
        return <SubcategorySelector category={category} />;
    }

    render() {
        return (
            <main block="CategoryPage">
                <ContentWrapper
                    wrapperMix={{
                        block: 'CategoryPage',
                        elem: 'Wrapper',
                    }}
                    label="Category page"
                >
                    {this.renderContent()}
                </ContentWrapper>
            </main>
        );
    }
}

export default CategoryPage;

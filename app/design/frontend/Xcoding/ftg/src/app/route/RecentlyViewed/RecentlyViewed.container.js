import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import { ThemeContext } from 'Context/ThemeContext';
import BreadcrumbsDispatcher from 'Store/Breadcrumbs/Breadcrumbs.dispatcher';
import { updateMeta } from 'Store/Meta/Meta.action';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import RecentlyViewed from './RecentlyViewed.component';

export const mapDispatchToProps = (dispatch) => ({
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    updateBreadcrumbs: (breadcrumbs) => BreadcrumbsDispatcher.update(breadcrumbs, dispatch),
    updateMeta: (meta) => dispatch(updateMeta(meta)),
});

const RecentlyViewedThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <RecentlyViewed {...props} />;
};

export default connect(null, mapDispatchToProps)(RecentlyViewedThemed);

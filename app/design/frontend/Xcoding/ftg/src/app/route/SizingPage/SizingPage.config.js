export const TABS_LABEL = [
    {
        id: 1,
        label: __("Men's"),
    },
    {
        id: 2,
        label: __("Women's"),
    },
    {
        id: 3,
        label: __("Junior's"),
    },
    {
        id: 4,
        label: __('Unisex'),
    },
];

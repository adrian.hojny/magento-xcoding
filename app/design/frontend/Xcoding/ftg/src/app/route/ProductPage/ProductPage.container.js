import { useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import {
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
    ProductPageContainer as SourceProductPageContainer,
} from 'SourceRoute/ProductPage/ProductPage.container';

import { ThemeContext } from 'Context/ThemeContext';
import { LOADING_TIME } from 'Route/CategoryPage/CategoryPage.config';
import { removeLinks, updateLinks } from 'Store/Hreflinks/Hreflinks.actions';
import { hideActiveOverlay, toggleOverlayByKey } from 'Store/Overlay/Overlay.action';
import { debounce } from 'Util/Request';

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    productDescriptionSuffix: state.ConfigReducer.product_desctiption_suffix,
    defaultTitle: state.ConfigReducer.default_title,
    links: state.HreflinksReducer.links,
    currencyCode: state.ConfigReducer.default_display_currency_code,
});

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    showOverlay: (overlayKey) => dispatch(toggleOverlayByKey(overlayKey)),
    hideActiveOverlay: () => dispatch(hideActiveOverlay()),
    updateLinks: (links) => dispatch(updateLinks(links)),
    removeLinks: () => dispatch(removeLinks()),
});

export class ProductPageContainer extends SourceProductPageContainer {
    state = {
        configurableVariantIndex: -1,
        parameters: {},
        activeImageOverlayGallery: 0,
        selectedBundlePrice: 0,
        productOptionsData: {},
    };

    containerFunctions = {
        updateConfigurableVariant: this.updateConfigurableVariant.bind(this),
        updateActiveImageOverlayGallery: this.updateActiveImageOverlayGallery.bind(this),
        getLink: this.getLink.bind(this),
        setBundlePrice: this.setBundlePrice.bind(this),
        getSelectedCustomizableOptions: this.getSelectedCustomizableOptions.bind(this),
    };

    componentDidUpdate(prevProps) {
        const {
            isOffline,
            productSKU,
            product: { sku, options, items, mf_alternate_urls },
            links,
            updateLinks,
        } = this.props;

        const {
            productSKU: prevProductSKU,
            product: { sku: prevSku, options: prevOptions, items: prevItems },
        } = prevProps;

        const { sku: stateSKU } = history?.state?.state?.product || {};

        if (isOffline) {
            debounce(this.setOfflineNoticeSize, LOADING_TIME)();
        }

        /**
         * We should also update product based data if, the URL
         * rewrite SKU has changed to matching the product history SKU
         * one. At this point there could be sufficient data for
         * some updates (i.e. header state).
         */
        if (productSKU !== prevProductSKU && stateSKU === productSKU) {
            this.updateHeaderState();
        }

        /**
         * If the currently loaded category ID does not match the ID of
         * category ID from URL rewrite, request category.
         */
        if (productSKU !== sku) {
            this.requestProduct();
        }

        /**
         * If product ID was changed => it is loaded => we need to
         * update product specific information, i.e. breadcrumbs.
         */
        if (sku !== prevSku) {
            this.updateBreadcrumbs();
            this.updateHeaderState();
            this.updateMeta();
            updateLinks(mf_alternate_urls);
        }

        /**
         * Since product from redux is not cleared and hreflinks are,
         * when reentering the product which was in redux previously,
         * we need to update the links
         */
        if (!links.length && mf_alternate_urls?.length) {
            updateLinks(mf_alternate_urls);
        }

        /**
         * LEGACY: needed to make sure required items are
         * selected in the bundle product.
         */
        if (JSON.stringify(options) !== JSON.stringify(prevOptions)) {
            this.getRequiredProductOptions(options);
        }

        /**
         * LEGACY needed to make sure required options are
         * selected in the customizable options product.
         */
        if (JSON.stringify(items) !== JSON.stringify(prevItems)) {
            this.getRequiredProductOptions(items);
        }
    }

    componentWillUnmount() {
        const { removeLinks } = this.props;

        removeLinks();
    }

    getProductOrVariant() {
        const dataSource = this.getDataSource();

        return dataSource;
    }

    setBundlePrice(price) {
        this.setState({ selectedBundlePrice: price });
    }

    updateActiveImageOverlayGallery(newIndex) {
        const { activeImageOverlayGallery } = this.state;

        if (activeImageOverlayGallery !== newIndex) {
            this.setState({ activeImageOverlayGallery: newIndex });
        }
    }

    getSelectedCustomizableOptions(values, updateArray = false) {
        const { productOptionsData } = this.state;

        if (updateArray) {
            this.setState({
                productOptionsData: { ...productOptionsData, productOptionsMulti: values },
            });
        } else {
            this.setState({
                productOptionsData: { ...productOptionsData, productOptions: values },
            });
        }
    }

    requestProduct() {
        const { requestProduct, productSKU } = this.props;
        const { currentProductSKU } = this.state;

        /**
         * If URL rewrite was not passed - do not request the product.
         */
        if (!productSKU) {
            return;
        }

        if (currentProductSKU === productSKU) {
            return;
        }

        this.setState({ currentProductSKU: productSKU });

        const options = {
            isSingleProduct: true,
            args: { filter: this.getProductRequestFilter() },
            useProductPageRequest: true,
        };

        requestProduct(options);
    }

    updateMeta() {
        const { updateMetaFromProduct, productDescriptionSuffix, defaultTitle } = this.props;
        const productMeta = this.getDataSource();

        const { meta_title, meta_keyword, meta_description, canonical_url, name } = productMeta;

        const customMetaDesc = `${meta_title || name} ${__('at')} ${defaultTitle}`;
        const metaDesc = meta_description === name ? customMetaDesc : meta_description;

        const metaData = {
            meta_title,
            meta_description: `${metaDesc} ${productDescriptionSuffix}`,
            canonical_url,
            meta_keyword,
        };

        updateMetaFromProduct(metaData);
    }
}

const ProductPageContainerThemed = (props) => {
    const { setTheme } = useContext(ThemeContext);

    useEffect(() => {
        setTheme({ mods: [] });
    }, []);

    return <ProductPageContainer {...props} />;
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductPageContainerThemed);

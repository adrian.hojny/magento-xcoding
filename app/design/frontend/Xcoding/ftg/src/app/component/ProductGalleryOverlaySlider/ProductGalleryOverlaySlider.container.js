import React from 'react';
import { connect } from 'react-redux';
import { Subscribe } from 'unstated';

import ProductGalleryContainer from 'Component/ProductGallery/ProductGallery.container';
import SharedTransitionContainer from 'Component/SharedTransition/SharedTransition.unstated';
import { hideActiveOverlay } from 'Store/Overlay/Overlay.action';
import ProductGalleryOverlaySlider from './ProductGalleryOverlaySlider.component';

export * from 'Component/ProductGallery/ProductGallery.container';

export const mapStateToProps = (state) => ({
    activeOverlay: state.OverlayReducer.activeOverlay,
});

export const mapDispatchToProps = (dispatch) => ({
    hideActiveOverlay: () => dispatch(hideActiveOverlay()),
});

class ProductGalleryOverlaySliderContainer extends ProductGalleryContainer {
    constructor(props) {
        super(props);

        const {
            product: { id },
            activeImageOverlayGallery,
        } = props;

        this.state = {
            activeImage: activeImageOverlayGallery,
            isZoomEnabled: false,
            prevProdId: id,
        };
    }

    containerProps = () => {
        const { isZoomEnabled } = this.state;
        const {
            product: { id },
            activeImageOverlayGallery,
        } = this.props;

        return {
            gallery: this.getGalleryPictures(),
            productName: this._getProductName(),
            activeImage: activeImageOverlayGallery,
            activeImageOverlayGallery,
            isZoomEnabled,
            productId: id,
        };
    };

    onActiveImageChange(activeImage) {
        const { updateActiveImageOverlayGallery } = this.props;

        updateActiveImageOverlayGallery(activeImage);
        this.setState({
            activeImage,
            isZoomEnabled: false,
        });
    }

    render() {
        return (
            <Subscribe to={[SharedTransitionContainer]}>
                {({ registerSharedElementDestination }) => (
                    <ProductGalleryOverlaySlider
                        registerSharedElementDestination={registerSharedElementDestination}
                        {...this.containerProps()}
                        {...this.containerFunctions}
                        {...this.props}
                    />
                )}
            </Subscribe>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductGalleryOverlaySliderContainer);

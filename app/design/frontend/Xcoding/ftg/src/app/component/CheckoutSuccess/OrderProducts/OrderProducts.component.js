import React from 'react';
import PropTypes from 'prop-types';

import Product from './OrderProduct.component';

import './OrderProducts.style';

const OrderProducts = ({ data: { order_products, currencyCode, total_qty_ordered } }) => (
    <div block="OrderProducts" elem="Container">
        <div block="OrderProducts" elem="Header">
            {__('Ordered products (%s)', total_qty_ordered)}
        </div>
        <ul>
            {order_products?.map((product, index) => (
                <Product key={product.sku + index} {...product} currencyCode={currencyCode} />
            ))}
        </ul>
    </div>
);

OrderProducts.propTypes = {
    data: PropTypes.shape({
        order_products: PropTypes.arrayOf(
            PropTypes.shape({
                qty: PropTypes.number.isRequired,
                sku: PropTypes.string.isRequired,
                name: PropTypes.string.isRequired,
                original_price: PropTypes.number.isRequired,
                thumbnail: PropTypes.shape({
                    label: PropTypes.string.isRequired,
                    url: PropTypes.string.isRequired,
                }).isRequired,
                attributes_info: PropTypes.arrayOf(
                    PropTypes.shape({
                        label: PropTypes.string.isRequired,
                        value: PropTypes.string.isRequired,
                    }),
                ).isRequired,
                special_price: PropTypes.number,
            }),
        ).isRequired,
        currencyCode: PropTypes.string.isRequired,
        total_qty_ordered: PropTypes.number.isRequired,
    }).isRequired,
};

export default OrderProducts;

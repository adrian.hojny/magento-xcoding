import { Fragment } from 'react';
import PropTypes from 'prop-types';

import SourceProductConfigurableAttributes from 'SourceComponent/ProductConfigurableAttributes/ProductConfigurableAttributes.component';

import BrandSizing from 'Component/BrandSizing/BrandSizing.component';
import Html from 'Component/Html/Html.component';
import {
    IS_AVALABLE_CHECK,
    IS_ENABLED_CHECK,
    IS_LAST_ITEM_CHECK,
} from 'Component/ProductActions/ProductActions.config';
import Sidebar from 'Component/Sidebar/Sidebar.container';
import { MixType } from 'Type/Common';
import { AttributeType } from 'Type/ProductList';
import { AVAILABLE, LAST_ITEM, UNAVAILABLE } from 'Util/Product';

import './ProductConfigurableAttributes.style.scss';

const PRODUCT_CONFIG_SIDEBAR_PREFIX = 'product-configuration';

export default class ProductConfigurableAttributes extends SourceProductConfigurableAttributes {
    static propTypes = {
        isContentExpanded: PropTypes.bool,
        numberOfPlaceholders: PropTypes.arrayOf(PropTypes.number),
        configurable_options: PropTypes.objectOf(AttributeType).isRequired,
        parameters: PropTypes.shape({}).isRequired,
        updateConfigurableVariant: PropTypes.func.isRequired,
        isReady: PropTypes.bool,
        mix: MixType,
        getIsConfigurableAttributeAvailable: PropTypes.func,
        getIsConfigurableAttributeLowInStock: PropTypes.func,
        getIsConfigurableAttributeEnable: PropTypes.func,
        getLink: PropTypes.func.isRequired,
        sizingDetails: PropTypes.object,
    };

    static defaultProps = {
        isReady: true,
        mix: {},
        // eslint-disable-next-line no-magic-numbers
        numberOfPlaceholders: [6, 10, 7],
        isContentExpanded: false,
        getIsConfigurableAttributeAvailable: () => true,
    };

    onOptionClick = (code, value, isAvailable, isSelected) => (event) => {
        const { handleOptionClick } = this.props;

        event.preventDefault();
        if (isAvailable && !isSelected) {
            handleOptionClick({ attribute_code: code, attribute_value: value });
        }
    };

    getAvailabilityInformation = (availabilityStatus) => {
        switch (availabilityStatus) {
            case AVAILABLE:
                return __('product available');
            case UNAVAILABLE:
                return __('Unavailable');
            case LAST_ITEM:
                return __('Last item');
            default:
                return '';
        }
    };

    renderOptions(attribute_code, options = []) {
        const { getProductAvailability, parameters } = this.props;

        return Object.values(options).map(({ label, value }) => {
            const isEnabled = getProductAvailability({ attribute_code, attribute_value: value }, IS_ENABLED_CHECK);

            if (!isEnabled) {
                return null;
            }

            const isAvailable = getProductAvailability({ attribute_code, attribute_value: value }, IS_AVALABLE_CHECK);
            const isLastItem = getProductAvailability({ attribute_code, attribute_value: value }, IS_LAST_ITEM_CHECK);
            const isSelected = parameters[attribute_code] === value;

            const availabilityStatus = () => {
                if (isAvailable) {
                    if (isLastItem) {
                        return LAST_ITEM;
                    }

                    return AVAILABLE;
                }

                return UNAVAILABLE;
            };

            return (
                <div
                    block="ProductConfigurableAttributes"
                    elem="Option"
                    mods={{ isAvailable, isLastItem: isAvailable && isLastItem }}
                    role="button"
                    onClick={this.onOptionClick(attribute_code, value, isAvailable, isSelected)}
                    key={`${attribute_code}/${value}`}
                >
                    <div block="ProductConfigurableAttributes" elem="OptionLabel">
                        {label}
                    </div>
                    <div block="ProductConfigurableAttributes" elem="OptionAvailability">
                        {this.getAvailabilityInformation(availabilityStatus())}
                    </div>
                </div>
            );
        });
    }

    renderConfigurableAttributes() {
        const { configurable_options, toggleOverlayByKey, parameters, sizingDetails } = this.props;

        return Object.values(configurable_options).map((option) => {
            const { attribute_label, attribute_code, attribute_options } = option;

            const sidebarId = `${PRODUCT_CONFIG_SIDEBAR_PREFIX}/${attribute_code}`;
            const selectedOption = parameters[attribute_code];
            const sidebarTitle = __('Choose %s', attribute_label);
            const title = selectedOption
                ? `${attribute_label}: ${attribute_options[selectedOption].label}`
                : sidebarTitle;

            return (
                <Fragment key={sidebarId}>
                    <button
                        onClick={() => {
                            toggleOverlayByKey(sidebarId, true);
                        }}
                        block="ProductActions"
                        elem="ConfigButton"
                    >
                        {title}
                    </button>
                    <Sidebar id={sidebarId} title={sidebarTitle} mix={{ block: 'SizingSidebar' }}>
                        {this.renderOptions(attribute_code, attribute_options)}
                        {sizingDetails && (
                            <>
                                <button
                                    onClick={() => {
                                        toggleOverlayByKey(sizingDetails.title, true);
                                    }}
                                    block="ProductActions"
                                    elem="SizingButton"
                                >
                                    {__('Sizing')}
                                </button>

                                <Sidebar
                                    // eslint-disable-next-line react/jsx-no-bind
                                    onCloseButtonClick={() => {
                                        toggleOverlayByKey(sidebarId, true);
                                    }}
                                    id={sizingDetails.title}
                                    title={sizingDetails.title}
                                    mix={{ block: 'BrandSizing', elem: 'Content' }}
                                >
                                    <BrandSizing>
                                        <Html content={sizingDetails.content} />
                                    </BrandSizing>
                                </Sidebar>
                            </>
                        )}
                    </Sidebar>
                </Fragment>
            );
        });
    }

    render() {
        const { isReady } = this.props;

        return isReady ? this.renderConfigurableAttributes() : this.renderPlaceholders();
    }
}

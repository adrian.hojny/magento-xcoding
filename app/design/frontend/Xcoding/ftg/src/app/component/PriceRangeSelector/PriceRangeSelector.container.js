import { connect } from 'react-redux';

import {
    mapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
} from 'Component/FilterSelector/FilterSelector.container';
import PriceRangeSelector from 'Component/PriceRangeSelector/PriceRangeSelector.component';

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    priceSliderVatRate: state.ConfigReducer.price_slider_vat_rate,
    storeMinPriceValue: state.ProductListInfoReducer.minPrice,
    storeMaxPriceValue: state.ProductListInfoReducer.maxPrice,
});

export default connect(mapStateToProps, mapDispatchToProps)(PriceRangeSelector);

import React from 'react';
import PropTypes from 'prop-types';

import { AddToCart as SourceAddToCart } from 'SourceComponent/AddToCart/AddToCart.component';

import { DEFAULT_CURERNCY } from 'Component/LangPanel/LangPanel.component';
import { CLICKED_CATEGORIES } from 'Component/ProductList/ProductList.component';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';

import './AddToCart.style';

export class AddToCart extends SourceAddToCart {
    state = {
        hover: false,
    };

    static propTypes = {
        ...super.propTypes,
        variant: PropTypes.string,
    };

    handleButtonClick = () => {
        const { buttonClick, quantity, product, variant, configurableVariantIndex } = this.props;
        const { variants } = product;
        const categoriesData = BrowserDatabase.getItem(CLICKED_CATEGORIES) || {};
        const sizeLabel = product.configurable_options?.shoes_size?.attribute_options[variant]?.label || '';
        const currency = BrowserDatabase.getItem(DEFAULT_CURERNCY);
        const productOrVariant =
            variants && variants[configurableVariantIndex] !== undefined ? variants[configurableVariantIndex] : product;

        buttonClick();

        if (window?.fbq) {
            fbq('track', 'AddToCart', {
                content_ids: productOrVariant?.sku,
                content_type: 'product',
                value: productOrVariant?.price_range?.minimum_price?.final_price?.value,
                currency,
            });
        }

        gtag('event', 'add_to_cart', {
            items: [
                {
                    id: product.sku,
                    name: product.name || '',
                    list_name: categoriesData[product.id] || '',
                    brand: product.brand_details?.url_alias || '',
                    category: categoriesData[product.id] || '',
                    variant: sizeLabel,
                    list_position: 1,
                    quantity,
                    price: product.price_range?.minimum_price?.final_price?.value,
                },
            ],
        });
    };

    componentDidUpdate(prevProps) {
        const { isLoading } = this.props;

        if (prevProps.isLoading !== isLoading && !isLoading) {
            this.setState({ hover: false });
        }
    }

    handleOnMouseEnter = () => {
        this.setState({ hover: true });
    };

    handleOnMouseLeave = () => {
        this.setState({ hover: false });
    };

    render() {
        const {
            mix,
            product: { type_id },
            isLoading,
        } = this.props;
        const { hover } = this.state;

        if (!type_id) {
            this.renderPlaceholder();
        }

        return (
            <button
                onClick={this.handleButtonClick}
                block="Button AddToCart"
                mix={mix}
                mods={{ isLoading, hover }}
                disabled={isLoading}
                onMouseEnter={this.handleOnMouseEnter}
                onMouseLeave={this.handleOnMouseLeave}
            >
                <span data-btn={__('Add to cart')}>{__('Add to cart')}</span>
                <span data-btn={__('Adding...')}>{__('Adding...')}</span>
            </button>
        );
    }
}

export default AddToCart;

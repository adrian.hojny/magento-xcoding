import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { MediaType } from 'SourceType/ProductList';

import ContentWrapper from 'Component/ContentWrapper';
import Html from 'Component/Html';
import Image from 'Component/Image/Image.container';
import isMobile from 'Util/Mobile/isMobile';

import './ProductStory.style.scss';

export default class ProductStory extends PureComponent {
    static propTypes = {
        product: PropTypes.shape({
            media_gallery_entries: MediaType,
            model_story: PropTypes.string,
        }).isRequired,
    };

    constructor(props) {
        super(props);

        this.state = {
            readMore: false,
        };

        this.handleShowMore = this.handleShowMore.bind(this);
    }

    handleShowMore() {
        this.setState((prevState) => ({
            readMore: !prevState.readMore,
        }));
    }

    getStoryImage() {
        const {
            product: { media_gallery_entries },
        } = this.props;

        const storyImage = media_gallery_entries?.find((item) => item.types.includes('story_image'));

        if (storyImage) {
            return storyImage;
        }

        // find first image in the galery
        const zeroPositionIndex = media_gallery_entries?.findIndex(({ position }) => position === 0);

        // details tab always shows 0 element, ensure story image won't be the same
        if (zeroPositionIndex === 0) {
            return media_gallery_entries?.[1] || {};
        }

        return media_gallery_entries?.[zeroPositionIndex] || {};
    }

    render() {
        const {
            product: { model_story },
        } = this.props;
        const { label, small: { url } = {} } = this.getStoryImage();
        const { readMore } = this.state;

        if (!model_story) {
            return null;
        }

        return (
            <div block="ProductStory">
                <ContentWrapper wrapperMix={{ block: 'ProductStory', elem: 'Wrapper' }} label={__('Product Story')}>
                    <div block="ProductStory" elem="Container">
                        {!isMobile.any() && (
                            <Image
                                src={url}
                                mix={{
                                    block: 'ProductStory',
                                    elem: 'Image',
                                }}
                                ratio="custom"
                                alt={label}
                                useWebp
                            />
                        )}
                        <div block="ProductStory" elem="Content" mods={{ readMore }}>
                            <h1>{__('Story')}</h1>
                            <Html content={model_story} />
                        </div>
                        <button block="ProductStory" elem="ReadMore" onClick={this.handleShowMore}>
                            {__('read more')}
                        </button>
                    </div>
                </ContentWrapper>
            </div>
        );
    }
}

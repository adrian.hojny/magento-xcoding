import PropTypes from 'prop-types';

import { MyAccountOrderTableRow as SourceMyAccountOrderTableRow } from 'SourceComponent/MyAccountOrderTableRow/MyAccountOrderTableRow.component';

import { baseOrderInfoType } from 'Type/Account';

import './MyAccountOrderTableRow.style';

export class MyAccountOrderTableRow extends SourceMyAccountOrderTableRow {
    static propTypes = {
        currency_code: PropTypes.string.isRequired,
        base_order_info: baseOrderInfoType.isRequired,
        onViewClick: PropTypes.func.isRequired,
    };

    render() {
        const {
            base_order_info: { created_at, status_label, increment_id, grand_total, order_currency_code },
            onViewClick,
        } = this.props;

        return (
            <tr onClick={onViewClick} block="MyAccountOrderTableRow">
                <td>{increment_id ? `#${increment_id}` : ''}</td>
                <td>{created_at}</td>
                <td>{status_label}</td>
                <td block="hidden-mobile">{grand_total ? `${grand_total} ${order_currency_code}` : ''}</td>
            </tr>
        );
    }
}

export default MyAccountOrderTableRow;

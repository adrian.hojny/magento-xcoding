import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ActiveFilterButton from 'Component/ActiveFilterButton';
import { CATEGORY_FILTER_OVERLAY_ID } from 'Component/CategoryFilterOverlay/CategoryFilterOverlay.component';
import ClickOutside from 'Component/ClickOutside';
import FilterSelectorOption from 'Component/FilterSelectorOption';
import FilterSelectorSearch from 'Component/FilterSelectorSearch';
import { FILTER_DETAIL } from 'Component/Header/Header.component';
import Overlay from 'Component/Overlay';
import isMobile from 'Util/Mobile';

import './FilterSelector.style.scss';

export const MAX_OPTIONS_WITHOUT_SEARCH = 9;
export const FILTER_SELECTOR_OVERLAY_ID = 'category-filter-details';

class FilterSelector extends PureComponent {
    static propTypes = {
        label: PropTypes.string.isRequired,
        options: PropTypes.object,
        selectedOptions: PropTypes.array,
        updateFilter: PropTypes.func.isRequired,
        name: PropTypes.string.isRequired,
        isWide: PropTypes.bool,
        toggleOverlayByKey: PropTypes.func.isRequired,
        changeHeaderState: PropTypes.func.isRequired,
        goToPreviousHeaderState: PropTypes.func.isRequired,
        activeOverlay: PropTypes.string.isRequired,
        withButtons: PropTypes.bool,
        isDropdown: PropTypes.bool,
        mods: PropTypes.object,
        onFilterOverlayVisible: PropTypes.func,
    };

    static defaultProps = {
        options: {},
        selectedOptions: [],
        isWide: false,
        withButtons: true,
        isDropdown: true,
        mods: {},
        onFilterOverlayVisible: undefined,
    };

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            displayedOptions: [],
            selectedOptions: {},
            searchStr: '',
        };

        this.dropdownClose = this.dropdownClose.bind(this);
        this.onSeeResultsClick = this.onSeeResultsClick.bind(this);
    }

    componentDidMount() {
        this.setDisplayedOptions();
        this.setSelectedOptionsFromProps();
    }

    componentDidUpdate(prevProps) {
        const { selectedOptions, options } = this.props;

        if (prevProps.selectedOptions !== selectedOptions) {
            this.setSelectedOptionsFromProps();
        }
        if (prevProps.options !== options) {
            this.setDisplayedOptions();
        }

        this.submitFiltersOnOverlayClose(prevProps);
    }

    submitFiltersOnOverlayClose = (prevProps) => {
        const { activeOverlay, goToPreviousHeaderState } = this.props;
        const { isOpen } = this.state;

        if (isOpen && !activeOverlay && prevProps.activeOverlay === FILTER_SELECTOR_OVERLAY_ID) {
            goToPreviousHeaderState();
            this.dropdownClose();
        }
    };

    setSelectedOptionsFromProps = () => {
        const { selectedOptions = [] } = this.props;

        this.setState({ selectedOptions: selectedOptions.reduce((prev, cur) => ({ ...prev, [cur]: true }), {}) });
    };

    setDisplayedOptions = (searchStr) => {
        const { options } = this.props;

        if (options) {
            const optionsArr = Object.values(options);

            this.setState({
                displayedOptions: searchStr
                    ? optionsArr.filter((option) => option.label.toLowerCase().includes(searchStr.toLowerCase()))
                    : optionsArr,
            });
        }
    };

    selectOption = (optionValue, value) => {
        const {
            selectedOptions: { [optionValue]: _, ...rest },
        } = this.state;

        this.setState({ selectedOptions: value ? { ...rest, [optionValue]: true } : rest });
    };

    clear = () => {
        const { isDropdown } = this.props;

        this.setState({ isOpen: false, selectedOptions: {} }, () => {
            this.setDisplayedOptions();
            if (isDropdown) this.submit();
        });
    };

    submit = () => {
        const { updateFilter, name } = this.props;
        const { selectedOptions } = this.state;

        if (typeof updateFilter === 'function') {
            updateFilter(name, Object.keys(selectedOptions));
        }
    };

    dropdownToggle = () => {
        const { toggleOverlayByKey, changeHeaderState, label } = this.props;
        this.setState(({ isOpen }) => ({ isOpen: !isOpen }));

        if (isMobile.width()) {
            changeHeaderState({
                name: FILTER_DETAIL,
                title: label,
                onCloseClick: this.optionsOverlayClose,
                onBackClick: this.optionsOverlayClose,
            });
            toggleOverlayByKey(FILTER_SELECTOR_OVERLAY_ID);
        }
    };

    updateSearchString = (value) => {
        this.setState({ searchStr: value });
        this.setDisplayedOptions(value);
    };

    checkIfSelected = () => {
        const { selectedOptions = [] } = this.props;

        const selectedOptionsCount = selectedOptions.length;
        return selectedOptionsCount > 0;
    };

    optionsOverlayClose = () => {
        const { toggleOverlayByKey, goToPreviousHeaderState } = this.props;
        goToPreviousHeaderState();
        toggleOverlayByKey(CATEGORY_FILTER_OVERLAY_ID);
        this.dropdownClose();
    };

    toggleBooleanOption = () => {
        const { updateFilter, name, onFilterOverlayVisible } = this.props;
        const isSelected = this.checkIfSelected();
        updateFilter(name, isSelected ? '' : [1]);

        onFilterOverlayVisible();
    };

    dropdownClose() {
        const { isDropdown } = this.props;

        this.setState({ isOpen: false, searchStr: '' });
        this.setDisplayedOptions();
        if (isDropdown) this.submit();
    }

    renderActiveFilters() {
        const { options } = this.props;
        const { selectedOptions = {} } = this.state;

        const activeFilters = Object.keys(selectedOptions).map(
            (optionKey) =>
                options[optionKey] && (
                    <ActiveFilterButton
                        label={options[optionKey].label}
                        key={optionKey}
                        /* eslint-disable-next-line react/jsx-no-bind */
                        onClick={() => this.selectOption(optionKey, false)}
                    />
                ),
        );

        return (
            <>
                {activeFilters}
                {activeFilters.length > 1 && (
                    <ActiveFilterButton label={`${__('Clear')} (${activeFilters.length})`} onClick={this.clear} />
                )}
            </>
        );
    }

    renderDropdown() {
        return (
            <ClickOutside onClick={this.dropdownClose}>
                <div block="FilterSelector" elem="DropdownMenu">
                    {this.renderOptions()}
                    {this.renderDropdownButtons()}
                </div>
            </ClickOutside>
        );
    }

    onSeeResultsClick() {
        const { hideActiveOverlay, goToPreviousHeaderState, goToPreviousNavigationState } = this.props;

        hideActiveOverlay();
        goToPreviousHeaderState();
        goToPreviousNavigationState();
    }

    renderSeeResults() {
        return (
            <div block="CategoryFilterOverlay" elem="SeeResults">
                <button
                    block="CategoryFilterOverlay"
                    elem="Button"
                    mix={{ block: 'Button' }}
                    onClick={this.onSeeResultsClick}
                >
                    {__('SEE RESULTS')}
                </button>
            </div>
        );
    }

    renderOverlay() {
        return (
            <Overlay id={FILTER_SELECTOR_OVERLAY_ID} mix={{ block: 'FilterSelector', elem: 'Overlay' }}>
                {this.renderActiveFilters()}
                {this.renderOptions()}
                {this.renderDropdownButtons()}
                {this.renderSeeResults()}
            </Overlay>
        );
    }

    renderOptions() {
        const { selectedOptions, searchStr, displayedOptions } = this.state;

        return (
            <>
                {(displayedOptions.length > MAX_OPTIONS_WITHOUT_SEARCH || searchStr) && (
                    <FilterSelectorSearch value={searchStr} onChange={this.updateSearchString} />
                )}
                <div block="FilterSelector" elem="Options">
                    {searchStr && displayedOptions.length === 0 && (
                        <div block="FilterSelector" elem="NoResults">
                            {__('No results found!')}
                        </div>
                    )}
                    {displayedOptions.map(({ value_string, label }) => (
                        <FilterSelectorOption
                            key={value_string}
                            label={label}
                            value={value_string}
                            isSelected={!!selectedOptions[value_string]}
                            selectOption={this.selectOption}
                        />
                    ))}
                </div>
            </>
        );
    }

    renderDropdownButtons() {
        const { withButtons } = this.props;

        if (!withButtons) return null;

        return (
            <div block="FilterSelector" elem="DropdownMenuButtons">
                <button block="Button" mix={{ block: 'FilterSelector', elem: 'ClearBtn' }} onClick={this.clear}>
                    {__('Clear')}
                </button>
                <button block="Button" mix={{ block: 'FilterSelector', elem: 'SaveBtn' }} onClick={this.dropdownClose}>
                    {__('Save')}
                </button>
            </div>
        );
    }

    render() {
        const { label, isWide, isDropdown, mods } = this.props;
        const { isOpen, selectedOptions = {}, displayedOptions = [] } = this.state;

        const selectedOptionsCount = displayedOptions.filter(({ value_string }) => !!selectedOptions[value_string])
            .length;
        const isSelected = this.checkIfSelected();

        return (
            <div
                block="FilterSelector"
                mods={{
                    isDropdown,
                    isWide,
                    isSelected,
                    ...mods,
                }}
                aria-expanded={isOpen}
            >
                <button
                    block="FilterSelector"
                    elem="Button"
                    onClick={isDropdown ? this.dropdownToggle : this.toggleBooleanOption}
                >
                    {label}
                    {selectedOptionsCount > 0 && isDropdown && ` (${selectedOptionsCount})`}
                </button>
                {isOpen && (isMobile.width() ? this.renderOverlay() : this.renderDropdown())}
            </div>
        );
    }
}

export default FilterSelector;

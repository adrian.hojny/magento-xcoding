import { Breadcrumbs as SourceBreadcrumbs } from 'SourceComponent/Breadcrumbs/Breadcrumbs.component';

import Breadcrumb from 'Component/Breadcrumb';
import ContentWrapper from 'Component/ContentWrapper';
import Link from 'Component/Link';
import { appendWithStoreCode } from 'Util/Url';

import './Breadcrumbs.style';

export class Breadcrumbs extends SourceBreadcrumbs {
    renderBreadcrumb({ url, name }, i) {
        const { breadcrumbs } = this.props;
        const isDisabled = !url || breadcrumbs.length - 1 === i;

        return <Breadcrumb name={name} url={url?.pathname || url} index={i + 1} key={i + 1} isDisabled={isDisabled} />;
    }

    render() {
        const { breadcrumbs, areBreadcrumbsVisible } = this.props;

        if (!areBreadcrumbsVisible || location.pathname === appendWithStoreCode('/') || location.pathname === '/') {
            return null;
        }

        return (
            <ContentWrapper mix={{ block: 'Breadcrumbs' }} label={__('Breadcrumbs (current location)...')}>
                <nav aria-label="Breadcrumbs navigation">
                    <ul block="Breadcrumbs" elem="List" itemScope itemType="http://schema.org/BreadcrumbList">
                        <li
                            block="Breadcrumb"
                            key="Home"
                            itemProp="itemListElement"
                            itemScope
                            itemType="http://schema.org/ListItem"
                        >
                            <Link block="Breadcrumb" elem="Link" to="/" tabIndex="0">
                                <meta itemProp="item" content={window.location.origin + appendWithStoreCode('/')} />
                                <span itemProp="name">{__('Home')}</span>
                                <meta itemProp="position" content={0} />
                            </Link>
                        </li>
                        {breadcrumbs.length ? this.renderBreadcrumbList(breadcrumbs) : this.renderBreadcrumb({}, 0)}
                    </ul>
                </nav>
            </ContentWrapper>
        );
    }
}

export default Breadcrumbs;

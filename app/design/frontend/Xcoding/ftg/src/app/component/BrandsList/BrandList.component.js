/* eslint-disable react/forbid-component-props */
import React from 'react';
import { useMediaPredicate } from 'react-media-hook';
import SlickSlider from 'react-slick';

import CmsBlock from 'Component/CmsBlock/CmsBlock.container';

import './BrandList.style.scss';

const CMS_BLOCKS = ['brands-list1', 'brands-list2', 'brands-list3', 'brands-list4', 'brands-list5'];

const BrandListDesktop = ({ children }) => (
    <div block="PremiumBrands" elem="list">
        {children}
    </div>
);

const BrandListMobile = ({ children }) => (
    <SlickSlider
        prevArrow={<div block="PremiumBrands" elem="slider-arrow" mods={{ left: true }} />}
        nextArrow={<div block="PremiumBrands" elem="slider-Arrow" mods={{ right: true }} />}
        centerMode
        alignCenter
        infinite={false}
        centerPadding="20px"
        className="PremiumBrands-slider"
    >
        {children}
    </SlickSlider>
);

const BrandList = ({ isMobile, children }) =>
    isMobile ? <BrandListMobile>{children}</BrandListMobile> : <BrandListDesktop>{children}</BrandListDesktop>;

const BrandListComponent = () => {
    const isMobile = useMediaPredicate('(max-width: 767px)');

    return (
        <div block="PremiumBrands">
            <div block="PremiumBrandsList" elem="header">
                <CmsBlock identifier="premium-brands-brands-list-header" withoutWrapper />
            </div>
            <BrandList isMobile={isMobile}>
                {CMS_BLOCKS.map((key) => (
                    <CmsBlock key={key} identifier={key} withoutWrapper />
                ))}
            </BrandList>
        </div>
    );
};

BrandListComponent.propTypes = {
    // requestBlocks: PropTypes.func.isRequired
};

export default BrandListComponent;

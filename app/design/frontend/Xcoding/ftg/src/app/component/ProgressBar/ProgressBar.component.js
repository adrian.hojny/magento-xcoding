import { PureComponent } from 'react';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types';

import './ProgressBar.style.scss';

class ProgressBar extends PureComponent {
    static propTypes = {
        isVisible: PropTypes.bool.isRequired,
        timeout: PropTypes.number.isRequired,
    };

    render() {
        const { isVisible, timeout } = this.props;

        return (
            <CSSTransition in={isVisible} timeout={0} classNames={{ enterDone: 'ProgressBar_isVisible' }} unmountOnExit>
                <div block="ProgressBar" style={{ transitionDuration: `${timeout}ms` }} />
            </CSSTransition>
        );
    }
}

export default ProgressBar;

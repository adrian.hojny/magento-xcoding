import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './ActiveFilterButton.style.scss';

class ActiveFilterButton extends PureComponent {
    static propTypes = {
        label: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
    };

    render() {
        const { label, onClick } = this.props;

        return (
            <button block="ActiveFilters" elem="Button" onClick={onClick}>
                {label}
            </button>
        );
    }
}

export default ActiveFilterButton;

import React from 'react';
import LazyLoad from 'react-lazyload';
import SlickSlider from 'react-slick';
import PropTypes from 'prop-types';

import SourceProductCard from 'SourceComponent/ProductCard/ProductCard.component';

import Image from 'Component/Image';
import Link from 'Component/Link';
import Loader from 'Component/Loader';
import LoaderPlaceholder from 'Component/LoaderPlaceholder';
import ProductCardComingSoon from 'Component/ProductCardComingSoon/ProductCardComingSoon.component';
import ProductLabels from 'Component/ProductLabels';
import ProductPrice from 'Component/ProductPrice/ProductPrice.container';
import ProductWishlistButton from 'Component/ProductWishlistButton';
import TextPlaceholder from 'Component/TextPlaceholder';

import './ProductCard.extend.style.scss';

export * from 'SourceComponent/ProductCard/ProductCard.component';

class ProductCard extends SourceProductCard {
    static propTypes = {
        ...super.propTypes,
        onProductClick: PropTypes.func,
    };

    renderVisualConfigurableOptions() {
        const { product } = this.props;

        if (product?.attributes) {
            const { available_sizes = {} } = product.attributes;
            const { attribute_value = null } = available_sizes;

            if (!attribute_value) {
                return null;
            }

            const parsedAvailableSizes = JSON.parse(attribute_value);

            if (parsedAvailableSizes) {
                // size - product size
                // value - product quantity
                const availableSizes = Object.keys(parsedAvailableSizes).reduce((filtered, size) => {
                    // show sizes in which quantity > 0
                    if (parsedAvailableSizes[size] > 0) {
                        filtered.push({ label: size, value: size });
                    }

                    return filtered;
                }, []);

                const settings = {
                    dots: false,
                    infinite: false,
                    speed: 500,
                    slidesToShow: 7,
                    slidesToScroll: 1,
                    className: 'ConfigurableOptionsSlider',
                    variableWidth: true,
                };

                return (
                    <SlickSlider {...settings}>
                        {availableSizes.map(({ value, label }) => (
                            <span block="ProductCard" elem="Size" key={value} aria-label={label}>
                                {value}
                            </span>
                        ))}
                    </SlickSlider>
                );
            }
        }

        return null;
    }

    renderProductPrice() {
        const {
            productOrVariant: { price_range },
        } = this.props;

        if (!price_range) return <TextPlaceholder />;

        return <ProductPrice price={price_range} mix={{ block: 'ProductCard', elem: 'Price' }} showPriceDiscount />;
    }

    renderBrandAttr() {
        const {
            product: { brand_details },
        } = this.props;

        if (!brand_details) {
            return <div block="ProductCard" elem="Brand" mods={{ placeholder: true }} />;
        }

        const { title } = brand_details;

        return (
            <div block="ProductCard" elem="Brand" itemProp={this.renderItemProps('brand')}>
                {title}
            </div>
        );
    }

    renderCardWrapper(children) {
        const { linkTo } = this.props;

        return (
            <Link block="ProductCard" elem="Link" to={linkTo} onClick={this.registerSharedElement}>
                {children}
            </Link>
        );
    }

    renderMainDetails() {
        const {
            product: { name },
        } = this.props;

        return (
            <p block="ProductCard" elem="Name" mods={{ isLoaded: !!name }} itemProp={this.renderItemProps('name')}>
                <TextPlaceholder content={name} length="medium" />
            </p>
        );
    }

    renderProductLabels() {
        const {
            product: { attributes },
        } = this.props;

        return <ProductLabels attributes={attributes} />;
    }

    renderPicture() {
        const {
            product: { id, name, small_image, thumbnail },
            thumbnail: sourceThumbnail,
        } = this.props;
        const src = small_image?.url || thumbnail?.url || sourceThumbnail;

        return (
            <LazyLoad once offset={200} placeholder={<LoaderPlaceholder />}>
                <Image
                    imageRef={this.imageRef}
                    src={src}
                    alt={name}
                    ratio="custom"
                    mix={{ block: 'ProductCard', elem: 'Picture' }}
                    isPlaceholder={!id}
                    useWebp
                />
            </LazyLoad>
        );
    }

    renderItemProps(content) {
        const {
            product: { sku },
            withoutItemProps,
        } = this.props;

        if (!withoutItemProps && sku) {
            return content;
        }

        return undefined;
    }

    handleProductClick = () => {
        const { onProductClick, product } = this.props;

        if (onProductClick) {
            onProductClick(product);
        }
    };

    render() {
        const {
            product: { sku, name },
            children,
            mix,
            isLoading,
            product,
            simpleCard,
        } = this.props;

        if (simpleCard) {
            return (
                <div
                    block="ProductCard"
                    itemScope={this.renderItemProps(true)}
                    itemType={this.renderItemProps('https://schema.org/Product')}
                    mix={mix}
                    onClick={this.handleProductClick}
                >
                    <Loader isLoading={isLoading} />
                    {this.renderItemProps(<meta itemProp="sku" content={sku} />)}
                    {this.renderItemProps(<meta itemProp="mpn" content={sku} />)}
                    {this.renderCardWrapper(
                        <>
                            <figure block="ProductCard" elem="Figure">
                                {this.renderPicture()}
                            </figure>
                            <div block="ProductCard" elem="Content">
                                <div block="ProductCard" elem="Brand" itemProp={this.renderItemProps('name')}>
                                    {name}
                                </div>
                            </div>
                        </>,
                    )}
                </div>
            );
        }

        return (
            <div
                block="ProductCard"
                itemScope={this.renderItemProps(true)}
                itemType={this.renderItemProps('https://schema.org/Product')}
                mix={mix}
                onClick={this.handleProductClick}
            >
                <Loader isLoading={isLoading} />
                {this.renderItemProps(<meta itemProp="sku" content={sku} />)}
                {this.renderItemProps(<meta itemProp="mpn" content={sku} />)}
                {this.renderProductLabels()}
                <ProductWishlistButton product={product} quantity={1} configurableVariantIndex={0} />
                {this.renderCardWrapper(
                    <>
                        <figure block="ProductCard" elem="Figure">
                            {this.renderPicture()}
                            <ProductCardComingSoon product={product} />
                        </figure>
                        <div block="ProductCard" elem="Content">
                            {this.renderBrandAttr()}
                            {this.renderMainDetails()}
                            {this.renderProductPrice()}
                            <div block="ProductCard" elem="ConfigurableOptions">
                                {this.renderVisualConfigurableOptions()}
                            </div>
                        </div>
                    </>,
                )}
                <div block="ProductCard" elem="AdditionalContent">
                    {children}
                </div>
            </div>
        );
    }
}
export default ProductCard;

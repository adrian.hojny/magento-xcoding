import { connect } from 'react-redux';

import { toggleOverlayByKey } from 'Store/Overlay/Overlay.action';
import Footer from './Footer.component';

export const mapStateToProps = (state) => ({
    copyright: state.ConfigReducer.copyright,
});

export const mapDispatchToProps = (dispatch) => ({
    showOverlay: (overlayKey) => dispatch(toggleOverlayByKey(overlayKey)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Footer);

import ISO6391 from 'iso-639-1';

export const BEFORE_ITEMS_TYPE = 'BEFORE_ITEMS_TYPE';
export const SWITCH_ITEMS_TYPE = 'SWITCH_ITEMS_TYPE';
export const AFTER_ITEMS_TYPE = 'AFTER_ITEMS_TYPE';

const allPrefixList = window.storeList.concat(ISO6391.getAllCodes());
const regexText = `/(${allPrefixList.join('|')})?`;
export const withStoreRegex = (path) => `${regexText}${path}`;

export const ROUTES = {
    brand: {
        uk: '/brand',
        pl: '/marka',
    },
    sizing: {
        uk: '/sizing',
        pl: '/rozmiary',
    },
};

export const getRouteUrl = (name, code) => {
    switch (code) {
        case 'uk':
        case 'pl':
            return ROUTES[name][code];
        default:
            return ROUTES[name].uk;
    }
};

import SourceLink from 'SourceComponent/Link/Link.component';

import { ChildrenType } from 'Type/Common';

class Link extends SourceLink {
    static propTypes = {
        ...super.propTypes,
        children: ChildrenType,
    };
}

export default Link;

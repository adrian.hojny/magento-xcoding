export const IS_AVALABLE_CHECK = 'IS_AVALABLE_CHECK';
export const IS_ENABLED_CHECK = 'IS_ENABLED_CHECK';
export const IS_LAST_ITEM_CHECK = 'IS_LAST_ITEM_CHECK';

export * from 'SourceComponent/ProductActions/ProductActions.config';

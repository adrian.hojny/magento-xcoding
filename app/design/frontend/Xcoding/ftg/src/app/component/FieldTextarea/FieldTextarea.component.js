import PropTypes from 'prop-types';

import { FieldTextarea as SourceFieldTextarea } from 'SourceComponent/FieldTextarea/FieldTextarea.component';

export class FieldTextarea extends SourceFieldTextarea {
    static propTypes = {
        ...super.propTypes,
        placeholder: PropTypes.string,
    };

    static defaultProps = {
        ...super.defaultProps,
        placeholder: '',
    };

    render() {
        const {
            id,
            value,
            name,
            rows,
            formRef,
            isDisabled,
            maxLength,
            onChange,
            onFocus,
            onClick,
            placeholder,
        } = this.props;

        return (
            <textarea
                ref={formRef}
                id={id}
                name={name}
                rows={rows}
                value={value}
                disabled={isDisabled}
                onChange={onChange}
                onFocus={onFocus}
                onClick={onClick}
                maxLength={maxLength}
                placeholder={placeholder}
            />
        );
    }
}

export default FieldTextarea;

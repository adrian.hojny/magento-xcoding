import { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { CART_TOTALS } from 'SourceStore/Cart/Cart.reducer';

import ConfigQuery from 'Query/Config.query';
import { GUEST_QUOTE_ID } from 'Store/Cart/Cart.dispatcher';
import { setCurrency, setStoreList } from 'Store/Config/Config.action';
import { updateMeta } from 'Store/Meta/Meta.action';
import BrowserDatabase from 'Util/BrowserDatabase';
import { fetchQuery } from 'Util/Request';
import Router from './Router.component';

const DEFAULT_CURERNCY = 'DEFAULT_CURERNCY';
const USD = 'USD';
const GBP = 'GBP';
const PLN = 'PLN';
const EUR = 'EUR';
const PL_STORE_VIEW_CODE = 'pl';

export const CartDispatcher = import(
    /* webpackMode: "lazy", webpackChunkName: "dispatchers" */
    'Store/Cart/Cart.dispatcher'
);
export const ConfigDispatcher = import(
    /* webpackMode: "lazy", webpackChunkName: "dispatchers" */
    'Store/Config/Config.dispatcher'
);
export const WishlistDispatcher = import(
    /* webpackMode: "lazy", webpackChunkName: "dispatchers" */
    'Store/Wishlist/Wishlist.dispatcher'
);

export const mapStateToProps = (state) => ({
    isLoading: state.ConfigReducer.isLoading,
    default_description: state.ConfigReducer.default_description,
    default_keywords: state.ConfigReducer.default_keywords,
    default_title: state.ConfigReducer.default_title,
    title_prefix: state.ConfigReducer.title_prefix,
    title_suffix: state.ConfigReducer.title_suffix,
    storeViewCode: state.ConfigReducer.code,
    storeList: state.ConfigReducer.storeList,
    webSiteId: state.ConfigReducer.website_id,
    locale: state.ConfigReducer.locale,
    isOffline: state.OfflineReducer.isOffline,
    isBigOffline: state.OfflineReducer.isBig,
});

export const mapDispatchToProps = (dispatch) => ({
    updateMeta: (meta) => dispatch(updateMeta(meta)),
    setCurrency: (currency) => dispatch(setCurrency(currency)),
    setStoreList: (storeList) => dispatch(setStoreList(storeList)),
    init: () => {
        WishlistDispatcher.then(({ default: dispatcher }) => dispatcher.updateInitialWishlistData(dispatch));
        ConfigDispatcher.then(({ default: dispatcher }) => {
            dispatcher.handleData(dispatch);
        });
    },
    cartInit: () => {
        CartDispatcher.then(({ default: dispatcher }) => dispatcher.updateInitialCartData(dispatch));
    },
});

export class RouterContainer extends PureComponent {
    static propTypes = {
        init: PropTypes.func.isRequired,
        cartInit: PropTypes.func.isRequired,
        updateMeta: PropTypes.func.isRequired,
        setCurrency: PropTypes.func.isRequired,
        setStoreList: PropTypes.func.isRequired,
        base_link_url: PropTypes.string,
        default_description: PropTypes.string,
        default_keywords: PropTypes.string,
        default_title: PropTypes.string,
        title_prefix: PropTypes.string,
        title_suffix: PropTypes.string,
        isLoading: PropTypes.bool,
        isBigOffline: PropTypes.bool,
        storeViewCode: PropTypes.string,
        locale: PropTypes.string.isRequired,
        storeList: PropTypes.array,
        webSiteId: PropTypes.number.isRequired,
    };

    static defaultProps = {
        base_link_url: '',
        default_description: '',
        default_keywords: '',
        default_title: '',
        title_prefix: '',
        title_suffix: '',
        isLoading: true,
        isBigOffline: false,
        storeViewCode: '',
        storeList: [],
    };

    // flag to prevent double request for cart
    shouldCartInit = true;

    constructor(props) {
        super(props);

        this.initializeApplication();
        this.redirectFromPartialUrl();
    }

    componentDidMount() {
        const { setCurrency, setStoreList, storeViewCode, cartInit, isLoading } = this.props;
        const isStorePL = storeViewCode === PL_STORE_VIEW_CODE || location.pathname.includes('/pl/');
        let userCurrency = BrowserDatabase.getItem(DEFAULT_CURERNCY);
        BrowserDatabase.deleteItem(GUEST_QUOTE_ID);

        this._setHtmlLang();

        if (this.shouldCartInit && !isLoading) {
            this.shouldCartInit = false;
            BrowserDatabase.setItem({}, CART_TOTALS);
            cartInit();
        }

        if (isStorePL) {
            BrowserDatabase.setItem(PLN, DEFAULT_CURERNCY);
        }

        if ((storeViewCode !== PL_STORE_VIEW_CODE || !location.pathname.includes('/pl/')) && userCurrency === PLN) {
            BrowserDatabase.setItem(EUR, DEFAULT_CURERNCY);
            userCurrency = EUR;
        }

        fetchQuery(ConfigQuery.getCurrencies())
            .then((data) => {
                const lang = window.navigator.language;
                const { available_currency_codes } = data.currency;

                if (!userCurrency && (storeViewCode !== PL_STORE_VIEW_CODE || !location.pathname.includes('/pl/'))) {
                    let defaultCurrency = data?.currency?.default_display_currency_code;

                    if (available_currency_codes.indexOf(USD) !== -1 && lang === 'en-US') {
                        defaultCurrency = USD;
                    }

                    if (available_currency_codes.indexOf(GBP) !== -1 && lang === 'en-GB') {
                        defaultCurrency = GBP;
                    }

                    BrowserDatabase.setItem(defaultCurrency, DEFAULT_CURERNCY);
                }

                setCurrency(data.currency);
            })
            .catch((err) => {
                if (err?.[0]?.message === 'Please correct the target currency') {
                    const defCurrency = isStorePL ? PLN : EUR;
                    BrowserDatabase.setItem(defCurrency, DEFAULT_CURERNCY);
                }
            });

        fetchQuery(ConfigQuery.getStoreListField()).then((data) => {
            setStoreList(data.storeList);
        });
    }

    componentDidUpdate(prevProps) {
        const { isLoading, updateMeta, cartInit } = this.props;
        const { isLoading: prevIsLoading } = prevProps;

        this._setHtmlLang();

        if (!isLoading && isLoading !== prevIsLoading) {
            const { default_description, default_keywords, default_title, title_prefix, title_suffix } = this.props;

            updateMeta({
                default_title,
                title: default_title,
                default_description,
                description: default_description,
                default_keywords,
                keywords: default_keywords,
                title_prefix,
                title_suffix,
            });
        }

        if (this.shouldCartInit && !isLoading) {
            this.shouldCartInit = false;
            BrowserDatabase.setItem({}, CART_TOTALS);
            cartInit();
        }
    }

    containerProps = () => {
        const { isBigOffline, storeViewCode, storeList, webSiteId } = this.props;

        return { isBigOffline, storeViewCode, storeList, webSiteId };
    };

    initializeApplication() {
        const { init } = this.props;
        init();
    }

    redirectFromPartialUrl() {
        const { base_link_url } = this.props;
        const { pathname: storePrefix } = new URL(base_link_url || window.location.origin);
        const { pathname } = location;

        if (storePrefix === '/') {
            return;
        }

        if (storePrefix.slice(0, -1) === pathname) {
            history.replace(storePrefix);
        }
    }

    _setHtmlLang() {
        const { locale } = this.props;

        // set html lang attribute to current locale
        const htmlEl = document.querySelector('html');
        const langPrefix = locale?.split('_') || null;

        if (locale && langPrefix?.length) {
            htmlEl.setAttribute('lang', langPrefix[0]);
        }
    }

    render() {
        const { storeViewCode } = this.props;

        return storeViewCode ? <Router {...this.containerProps()} /> : null;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RouterContainer);

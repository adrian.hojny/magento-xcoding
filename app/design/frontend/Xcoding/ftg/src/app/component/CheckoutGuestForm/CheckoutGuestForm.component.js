import PropTypes from 'prop-types';

import { CheckoutGuestForm as SourceCheckoutGuestForm } from 'SourceComponent/CheckoutGuestForm/CheckoutGuestForm.component';

import Field from 'Component/Field';
import FormPortal from 'Component/FormPortal';
import ReCaptchaField from 'Component/ReCaptchaField';

import 'SourceComponent/CheckoutGuestForm/CheckoutGuestForm.style';

export class CheckoutGuestForm extends SourceCheckoutGuestForm {
    static propTypes = {
        formId: PropTypes.string.isRequired,
        handleEmailInput: PropTypes.func.isRequired,
        handleCreateUser: PropTypes.func.isRequired,
    };

    get fieldMap() {
        const { handleEmailInput, handlePasswordInput, formId, isCreateUser } = this.props;

        const fields = {
            guest_email: {
                form: formId,
                label: __('Email'),
                validation: ['notEmpty', 'email'],
                onChange: handleEmailInput,
                skipValue: true,
            },
        };

        if (isCreateUser) {
            fields.guest_password = {
                form: formId,
                label: __('Create Password'),
                onChange: handlePasswordInput,
                validation: ['notEmpty', 'password'],
                type: 'password',
                skipValue: true,
            };
        }

        return fields;
    }

    renderHeading() {
        return (
            <h2 block="Checkout" elem="Heading">
                {__('Enter personal information')}
            </h2>
        );
    }

    renderCreateUserCheckbox() {
        const { isCreateUser, handleCreateUser, isEmailConfirmationRequired } = this.props;

        // if email confirmation required and user is not logged in
        // the user is 100% not logged in (we are in the guest form)
        // do not show the checkbox to create the user account
        if (isEmailConfirmationRequired) {
            return null;
        }

        return (
            <Field
                type="checkbox"
                label={__('Create free account and keep track of your orders')}
                id="guest_create_user"
                name="guest_create_user"
                value={isCreateUser}
                skipValue
                onChange={handleCreateUser}
            />
        );
    }

    render() {
        const { formId, isCreateUser } = this.props;

        return (
            <div block="CheckoutGuestForm" mix={{ block: 'FieldForm' }}>
                {this.renderHeading()}
                <FormPortal id={formId} name="CheckoutGuestForm">
                    {this.renderFields()}
                    {isCreateUser && <ReCaptchaField />}
                    {this.renderCreateUserCheckbox()}
                </FormPortal>
            </div>
        );
    }
}

export default CheckoutGuestForm;

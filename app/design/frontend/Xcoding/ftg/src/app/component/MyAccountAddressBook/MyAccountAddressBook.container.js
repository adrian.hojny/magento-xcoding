import {
    MyAccountAddressBookContainer as SourceMyAccountAddressBookContainer,
    mapStateToProps,
    mapDispatchToProps,
} from 'SourceComponent/MyAccountAddressBook/MyAccountAddressBook.container';
import { connect } from 'react-redux';

export class MyAccountAddressBookContainer extends SourceMyAccountAddressBookContainer {
    getDefaultPostfix(address) {
        const { default_billing, default_shipping } = address;
        if (!default_billing && !default_shipping) {
            return '';
        }
        if (default_billing && default_shipping) {
            return __(' - default shipping, billing address');
        }
        if (default_billing) {
            return __(' - default billing address');
        }

        return __(' - default shipping address');
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyAccountAddressBookContainer);

import SourceWidgetFactory from 'SourceComponent/WidgetFactory/WidgetFactory.component';

import BrandList from 'Component/BrandsList';
import DeliveryMap from 'Component/DeliveryMapAndLegend';
import NewProducts from 'Component/NewProducts';
import PrmSaleBlock from 'Component/PrmSaleBlock';
import ProductListWidget from 'Component/ProductListWidget';
import RecentlyViewed from 'Component/RecentlyViewed';
import SaleBlock from 'Component/SaleBlock';
import HomeSlider from 'Component/SliderWidget';
import TabbedRemoteNavi from 'Component/TabbedRemoteNavi';

export * from 'SourceComponent/WidgetFactory/WidgetFactory.component';

export const SLIDER = 'Slider';
export const NEW_PRODUCTS = 'NewProducts';
export const CATALOG_PRODUCT_LIST = 'CatalogProductList';
export const REACT_COMPONENT = 'ReactComponent';

class WidgetFactory extends SourceWidgetFactory {
    reactComponents = {
        RecentlyViewed,
        SaleBlock,
        PrmSaleBlock,
        BrandList,
        TabbedRemoteNavi,
        DeliveryMap,
    };

    renderMap = {
        [SLIDER]: {
            component: HomeSlider,
        },
        [NEW_PRODUCTS]: {
            component: NewProducts,
        },
        [CATALOG_PRODUCT_LIST]: {
            component: ProductListWidget,
        },
        [REACT_COMPONENT]: {
            component: this.reactComponents[this.props.componentName],
        },
    };

    render() {
        const { type } = this.props;
        const { component: Widget } = this.renderMap[type] || {};

        return Widget !== undefined ? <Widget {...this.props} widget /> : null;
    }
}

export default WidgetFactory;

import PropTypes from 'prop-types';

import SourceTextPlaceholder from 'SourceComponent/TextPlaceholder/TextPlaceholder.component';

class TextPlaceholder extends SourceTextPlaceholder {
    static propTypes = {
        ...super.propTypes,
        content: PropTypes.oneOfType([PropTypes.string, PropTypes.bool, PropTypes.number, PropTypes.node]),
    };
}

export default TextPlaceholder;

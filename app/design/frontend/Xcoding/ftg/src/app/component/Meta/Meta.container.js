import { connect } from 'react-redux';

import { mapStateToProps, MetaContainer as SourceMetaContainer } from 'SourceComponent/Meta/Meta.container';

export class MetaContainer extends SourceMetaContainer {
    _getTitle() {
        const { title, default_title, title_suffix } = this.props;
        const titleSuffix = title_suffix ? `- ${title_suffix}` : '';

        return `${title || default_title} ${titleSuffix}`;
    }
}

export default connect(mapStateToProps)(MetaContainer);

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class PriceRangeSelectorInput extends PureComponent {
    static propTypes = {
        currencyCode: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
        changeValue: PropTypes.func.isRequired,
        submit: PropTypes.func.isRequired,
        dropdownClose: PropTypes.func.isRequired,
    };

    onFocus = ({ target }) => {
        target.select();
    };

    onBlur = () => {
        const { submit } = this.props;
        submit();
    };

    handleEnterKeydown = ({ key }) => {
        const { dropdownClose, submit } = this.props;
        if (key === 'Enter') {
            submit();
            setTimeout(() => dropdownClose(), 100);
        }
    };

    render() {
        const { currencyCode, name, value, changeValue } = this.props;

        return (
            <div block="PriceRangeSelector" elem="Input">
                <input
                    type="number"
                    name={name}
                    value={value}
                    onChange={changeValue}
                    onFocus={this.onFocus}
                    onKeyDown={this.handleEnterKeydown}
                    onBlur={this.onBlur}
                />
                <label htmlFor={name}>{currencyCode}</label>
            </div>
        );
    }
}

export default PriceRangeSelectorInput;

import SourceProductPriceContainer from 'SourceComponent/ProductPrice/ProductPrice.container';

import { calculateFinalPrice, roundPrice } from 'Util/Price';

export default class ProductPriceContainer extends SourceProductPriceContainer {
    containerProps = () => {
        const {
            price: {
                minimum_price: {
                    discount: { percent_off: discountPercentage } = {},
                    final_price: { value: minimalPriceValue, currency: priceCurrency } = {},
                    regular_price: { value: regularPriceValue } = {},
                } = {},
            } = {},
        } = this.props;

        if (!minimalPriceValue || !regularPriceValue) {
            return {};
        }

        const roundedRegularPrice = roundPrice(regularPriceValue);
        const finalPrice = calculateFinalPrice(discountPercentage, minimalPriceValue, regularPriceValue);
        const formatedCurrency = roundPrice(finalPrice);
        const currency = priceCurrency;

        const priceDifference = roundPrice(regularPriceValue - minimalPriceValue);

        return {
            roundedRegularPrice,
            priceCurrency,
            discountPercentage,
            formatedCurrency,
            currency,
            priceDifference,
        };
    };
}

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { ProductType } from 'Type/ProductList';
import ProductVariants from './ProductVariants.component';

export const mapStateToProps = (state) => ({
    linkedProducts: state.LinkedProductsReducer.linkedProducts,
});

export * from 'SourceComponent/ProductLinks/ProductLinks.container';

export class ProductVariantsContainer extends PureComponent {
    static propTypes = {
        linkedProducts: PropTypes.objectOf(ProductType).isRequired,
        linkType: PropTypes.string.isRequired,
    };

    render() {
        const {
            linkType,
            linkedProducts: { [linkType]: { items = [] } = {} },
        } = this.props;

        if (items.length === 0) {
            return null;
        }

        return <ProductVariants {...this.props} />;
    }
}

export default connect(mapStateToProps)(ProductVariantsContainer);

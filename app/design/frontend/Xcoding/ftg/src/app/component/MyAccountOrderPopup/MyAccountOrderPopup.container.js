import { connect } from 'react-redux';

import {
    mapDispatchToProps,
    mapStateToProps,
    MyAccountOrderPopupContainer as SourceMyAccountOrderPopupContainer,
} from 'SourceComponent/MyAccountOrderPopup/MyAccountOrderPopup.container';

export class MyAccountOrderPopupContainer extends SourceMyAccountOrderPopupContainer {
    componentDidUpdate() {
        const {
            payload: { increment_id: id },
        } = this.props;
        const { order } = this.state;
        if (!!id && !order?.base_order_info?.id) {
            this.requestOrderDetails();
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyAccountOrderPopupContainer);

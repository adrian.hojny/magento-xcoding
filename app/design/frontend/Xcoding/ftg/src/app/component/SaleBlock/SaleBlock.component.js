import React, { PureComponent } from 'react';

import CmsBlock from 'Component/CmsBlock/CmsBlock.container';
import StyledSlickSlider from 'Component/StyledSlickSlider';

import './SaleBlock.style.scss';

const CMS_BLOCKS = ['homepage-sale-block-1', 'homepage-sale-block-2', 'homepage-sale-block-3', 'homepage-sale-block-4'];

export default class SaleBlock extends PureComponent {
    render() {
        return (
            <div block="SaleBlock" elem="wrapper">
                <CmsBlock key="homepage-sale-block-5" identifier="homepage-sale-block-5" />

                <StyledSlickSlider block="SaleBlock-Carousel">
                    {CMS_BLOCKS.map((key) => (
                        <CmsBlock key={key} identifier={key} />
                    ))}
                </StyledSlickSlider>
            </div>
        );
    }
}

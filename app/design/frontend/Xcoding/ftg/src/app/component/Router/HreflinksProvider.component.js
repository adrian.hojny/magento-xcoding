import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import PropTypes from 'prop-types';

import usePrevious from 'Util/Hooks/usePrevious';

const HreflinksProvider = ({ storeList, storeViewCode }) => {
    const defaultLinks = useSelector((state) => state.HreflinksReducer?.links);
    const [links, setLinks] = useState(defaultLinks);
    const prevLinks = usePrevious(links);
    const location = useLocation();

    const getDefaultUrl = (hreflinks) => {
        const currentStore = storeList.find((store) => store.code === storeViewCode);
        const currentLang = currentStore?.locale?.split('_')[0];

        if (currentLang) {
            return hreflinks.find((hreflink) => hreflink.hreflang === currentLang)?.href;
        }

        return '';
    };

    const generateLinks = () => {
        const hreflinks = Array.from(defaultLinks);

        if (!hreflinks.length) {
            return;
        }

        const defaultUrl = getDefaultUrl(hreflinks);

        if (!defaultUrl) {
            return;
        }

        hreflinks.push({
            href: defaultUrl,
            hreflang: 'x-default',
        });

        setLinks(hreflinks);
    };

    const removeLinks = () => {
        if (!prevLinks?.length) {
            return;
        }

        prevLinks.forEach((link) => {
            const node = document.getElementById(link.hreflang);

            if (node) {
                node.remove();
            }
        });
    };

    const addLinks = () => {
        if (!links.length) {
            return;
        }

        links.forEach((link) => {
            const linkTag = document.createElement('link');
            const id = document.createAttribute('id');
            const rel = document.createAttribute('rel');
            const href = document.createAttribute('href');
            const hreflang = document.createAttribute('hreflang');
            let correctedHref = link.href;

            //Fix for hreflings in categories, where url in link.href has different lang code than link.hreflang
            if (correctedHref && hreflang !== 'x-default' && hreflang !== storeViewCode) {
                const hrefArr = link.href.split(`/${storeViewCode}/`);

                if (hrefArr.length > 1) {
                    correctedHref = `${hrefArr[0]}/${link.hreflang}/${hrefArr[1]}`;
                }
            }

            id.value = link.hreflang;
            rel.value = 'alternate';
            href.value = correctedHref;
            hreflang.value = link.hreflang;

            linkTag.setAttributeNode(id);
            linkTag.setAttributeNode(rel);
            linkTag.setAttributeNode(href);
            linkTag.setAttributeNode(hreflang);

            document.head.appendChild(linkTag);
        });
    };

    useEffect(() => {
        setLinks(defaultLinks);
    }, [defaultLinks]);

    useEffect(() => {
        generateLinks();
    }, [defaultLinks, location, storeList]);

    useEffect(() => {
        removeLinks();
    }, [location]);

    useEffect(() => {
        removeLinks();
        addLinks();
    }, [links]);

    return null;
};

HreflinksProvider.propTypes = {
    storeViewCode: PropTypes.string,
    storeList: PropTypes.array,
};

HreflinksProvider.defaultProps = {
    storeViewCode: '',
    storeList: [],
};

export default HreflinksProvider;

import ProductCardDetails from 'Component/ProductCardDetails/ProductCardDetails.component';
import ProductInformationContainer from 'Component/ProductInformation';

class ProductCardDetailsContainer extends ProductInformationContainer {
    render() {
        return <ProductCardDetails {...this.props} {...this.containerProps()} />;
    }
}

export default ProductCardDetailsContainer;

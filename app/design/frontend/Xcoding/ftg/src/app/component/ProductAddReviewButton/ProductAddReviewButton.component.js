import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './ProductAddReviewButton.style.scss';

class ProductAddReviewButton extends PureComponent {
    static propTypes = {
        toggleReviewSidebar: PropTypes.func.isRequired,
        mods: PropTypes.object,
    };

    static defaultProps = {
        mods: {},
    };

    render() {
        const { toggleReviewSidebar, mods } = this.props;

        return (
            <div block="ProductAddReviewButton" mods={mods}>
                <div>{__('Your opinion matters to us')}</div>
                <button onClick={toggleReviewSidebar}>{__('Write a review')}</button>
            </div>
        );
    }
}

export default ProductAddReviewButton;

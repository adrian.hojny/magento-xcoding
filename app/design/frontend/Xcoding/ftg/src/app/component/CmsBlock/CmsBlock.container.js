import PropTypes from 'prop-types';

import { CmsBlockContainer as SourceCmsBlockContainer } from 'SourceComponent/CmsBlock/CmsBlock.container';

import CmsBlock from './CmsBlock.component';

export class CmsBlockContainer extends SourceCmsBlockContainer {
    static propTypes = {
        ...super.propTypes,
        withoutWrapper: PropTypes.bool,
        ignoredHtmlRules: PropTypes.array,
    };

    static defaultProps = {
        withoutWrapper: false,
    };

    state = {
        cmsBlock: {},
    };

    containerProps = () => {
        const { cmsBlock } = this.state;
        const { withoutWrapper, ignoredHtmlRules } = this.props;
        return { cmsBlock, withoutWrapper, ignoredHtmlRules };
    };

    render() {
        return <CmsBlock {...this.containerProps()} />;
    }
}

export default CmsBlockContainer;

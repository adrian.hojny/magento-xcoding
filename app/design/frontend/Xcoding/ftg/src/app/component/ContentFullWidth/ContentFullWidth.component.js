import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { ChildrenType, MixType } from 'Type/Common';

import './ContentFullWidth.style';

/**
 * Content Full Width
 * @class ContentFullWidth
 */
export default class ContentFullWidth extends PureComponent {
    static propTypes = {
        children: ChildrenType,
        mix: MixType,
        wrapperMix: PropTypes.shape({
            block: PropTypes.string,
            elem: PropTypes.string,
        }),
        label: PropTypes.string.isRequired,
    };

    static defaultProps = {
        mix: {},
        wrapperMix: {},
        children: null,
    };

    render() {
        const { children, mix, wrapperMix, label } = this.props;

        return (
            <section mix={mix} aria-label={label}>
                <div block="ContentFullWidth" mix={wrapperMix}>
                    {children}
                </div>
            </section>
        );
    }
}

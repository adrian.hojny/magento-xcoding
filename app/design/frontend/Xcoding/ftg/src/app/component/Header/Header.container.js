import { connect } from 'react-redux';

import {
    CART_OVERLAY,
    CATEGORY,
    CHECKOUT,
    CMS_PAGE,
    CUSTOMER_ACCOUNT,
    CUSTOMER_ACCOUNT_PAGE,
    CUSTOMER_SUB_ACCOUNT,
    MENU,
    MENU_SUBCATEGORY,
    PDP,
    SEARCH,
} from 'SourceComponent/Header/Header.config';
import {
    HeaderContainer as SourceHeaderContainer,
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
} from 'SourceComponent/Header/Header.container';

import ClickOutside from 'Component/ClickOutside';
import { CUSTOMER_ACCOUNT_OVERLAY_KEY } from 'Component/MyAccountOverlay/MyAccountOverlay.config';
import { DEFAULT_STATE_NAME } from 'Component/NavigationAbstract/NavigationAbstract.config';
import { CHECKOUT_URL } from 'Route/Checkout/Checkout.config';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { toggleOverlayByKey } from 'Store/Overlay/Overlay.action';
import { isSignedIn } from 'Util/Auth';
import history from 'Util/History';
import isMobile from 'Util/Mobile';
import { appendWithStoreCode } from 'Util/Url';

export const DEFAULT_HEADER_STATE = {
    name: DEFAULT_STATE_NAME,
    isHiddenOnMobile: false,
};

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    wishlistCount: Object.keys(state.WishlistReducer.productsInWishlist).length,
    reloadFlag: state.NavigationReducer.reloadFlag,
});

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    toggleOverlayByKey: (key) => dispatch(toggleOverlayByKey(key)),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
});

class HeaderContainer extends SourceHeaderContainer {
    default_state = DEFAULT_HEADER_STATE;

    routeMap = {
        '/account/confirm': { name: CMS_PAGE, title: __('Confirm account'), onBackClick: () => history.push('/') },
        '/category': { name: CATEGORY, onBackClick: this.onMenuButtonClick.bind(this) },
        '/checkout': {
            name: CHECKOUT,
            onBackClick: () => {
                history.push('/');
                history.goBack();
            },
        },
        '/my-account': { name: CUSTOMER_ACCOUNT_PAGE, onBackClick: () => history.push('/') },
        '/product': { name: PDP, onBackClick: () => history.goBack() },
        '/cart': { name: DEFAULT_STATE_NAME },
        '/menu': { name: MENU },
        '/page': { name: CMS_PAGE, onBackClick: () => history.goBack() },
        '/': { name: DEFAULT_STATE_NAME, isHiddenOnMobile: false },
    };

    containerProps = () => {
        const { navigationState, cartTotals, header_logo_src, logo_alt, isLoading, menu, wishlistCount } = this.props;

        const { isClearEnabled, searchCriteria, showMyAccountLogin, activeMegaMenu } = this.state;

        const {
            location: { pathname },
        } = history;

        const isCheckout = pathname.includes(CHECKOUT_URL);

        return {
            navigationState,
            cartTotals,
            header_logo_src,
            logo_alt,
            isLoading,
            isClearEnabled,
            searchCriteria,
            isCheckout,
            showMyAccountLogin,
            menu,
            wishlistCount,
            activeMegaMenu,
        };
    };

    containerFunctions = {
        onBackButtonClick: this.onBackButtonClick.bind(this),
        onCheckoutBackButtonClick: this.onCheckoutBackButtonClick.bind(this),
        onCloseButtonClick: this.onCloseButtonClick.bind(this),
        onSearchBarFocus: this.onSearchBarFocus.bind(this),
        onMenuButtonClick: this.onMenuButtonClick.bind(this),
        onClearSearchButtonClick: this.onClearSearchButtonClick.bind(this),
        onMyAccountButtonClick: this.onMyAccountButtonClick.bind(this),
        onSearchBarChange: this.onSearchBarChange.bind(this),
        onClearButtonClick: this.onClearButtonClick.bind(this),
        onEditButtonClick: this.onEditButtonClick.bind(this),
        onMinicartButtonClick: this.onMinicartButtonClick.bind(this),
        onOkButtonClick: this.onOkButtonClick.bind(this),
        onCancelButtonClick: this.onCancelButtonClick.bind(this),
        onSearchOutsideClick: this.onSearchOutsideClick.bind(this),
        onMenuOutsideClick: this.onMenuOutsideClick.bind(this),
        onMyAccountOutsideClick: this.onMyAccountOutsideClick.bind(this),
        onMinicartOutsideClick: this.onMinicartOutsideClick.bind(this),
        closeOverlay: this.closeOverlay.bind(this),
        onSignIn: this.onSignIn.bind(this),
        setActiveMegaMenu: this.setActiveMegaMenu.bind(this),
        hideActiveOverlay: this.props.hideActiveOverlay,
        setNavigationState: this.props.setNavigationState,
        showOverlay: this.props.showOverlay,
        goToPreviousNavigationState: this.props.goToPreviousNavigationState,
    };

    constructor(props) {
        super(props);

        this.state = {
            ...this.state,
            activeMegaMenu: null,
        };
    }

    setActiveMegaMenu(id) {
        this.setState({ activeMegaMenu: id });
    }

    handleHeaderVisibility() {
        const {
            navigationState: { isHiddenOnMobile },
        } = this.props;

        if (isHiddenOnMobile) {
            document.documentElement.classList.add('hiddenHeaderOnMobile');
            return;
        }

        document.documentElement.classList.remove('hiddenHeaderOnMobile');
    }

    onMinicartButtonClick() {
        const {
            showOverlay,
            navigationState: { name },
        } = this.props;

        if (name === CART_OVERLAY) {
            return;
        }

        if (isMobile.any() || isMobile.tabletWidth()) {
            history.push(appendWithStoreCode('/cart'));

            return null;
        }

        this.setState({ shouldRenderCartOverlay: true });

        showOverlay(CART_OVERLAY);
    }

    onMinicartOutsideClick() {
        const {
            goToPreviousNavigationState,
            hideActiveOverlay,
            navigationState: { name },
        } = this.props;

        if (isMobile.any() || name !== CART_OVERLAY) {
            return;
        }

        goToPreviousNavigationState();
        hideActiveOverlay();
    }

    renderAccount(isVisible = false) {
        const { onMyAccountOutsideClick, isCheckout } = this.props;

        // on mobile and tablet hide button if not in checkout
        if (!isCheckout) {
            return null;
        }

        if (isCheckout && isSignedIn()) {
            return null;
        }

        return (
            <ClickOutside onClick={onMyAccountOutsideClick} key="account">
                <div aria-label="My account" block="Header" elem="MyAccount">
                    {this.renderAccountButton(isVisible)}
                    {this.renderAccountOverlay()}
                </div>
            </ClickOutside>
        );
    }

    onMenuButtonClick() {
        history.push(appendWithStoreCode('/menu'));
    }

    onCloseButtonClick(e) {
        const { hideActiveOverlay, changeHeaderState } = this.props;
        const {
            navigationState: { onCloseClick },
        } = this.props;

        this.setState({ searchCriteria: '' });

        if (onCloseClick) {
            onCloseClick(e);
        }

        hideActiveOverlay();
        changeHeaderState({ name: DEFAULT_STATE_NAME, force: true });
    }

    onClearSearchButtonClick() {
        const { goToPreviousNavigationState } = this.props;

        this.setState({ searchCriteria: '' });
        goToPreviousNavigationState();
    }

    onMenuOutsideClick() {
        const {
            goToPreviousNavigationState,
            hideActiveOverlay,
            navigationState: { name },
        } = this.props;

        if (isMobile.any()) {
            return;
        }

        if (name === MENU || name === MENU_SUBCATEGORY) {
            if (name === MENU_SUBCATEGORY) {
                goToPreviousNavigationState();
            }
            goToPreviousNavigationState();
            hideActiveOverlay();
        }
    }

    onCheckoutBackButtonClick(e) {
        const {
            navigationState: { onBackClick },
        } = this.props;

        this.setState({ searchCriteria: '' });

        if (onBackClick) {
            onBackClick(e);
        } else {
            history.push({ pathname: appendWithStoreCode('/cart') });
        }
    }

    onMyAccountOutsideClick() {
        const {
            goToPreviousNavigationState,
            hideActiveOverlay,
            navigationState: { name },
            activeOverlay,
        } = this.props;

        if (isMobile.any() || name === CART_OVERLAY || (!isMobile.any() && name === SEARCH)) {
            return;
        }

        if (name === CUSTOMER_SUB_ACCOUNT) {
            goToPreviousNavigationState();
        }

        if (activeOverlay === CUSTOMER_ACCOUNT) {
            this.goToDefaultHeaderState();
            hideActiveOverlay();
        }
    }

    onCancelButtonClick() {
        const {
            navigationState: { onCancelClick },
            goToPreviousNavigationState,
        } = this.props;

        if (onCancelClick) {
            onCancelClick();
        }

        goToPreviousNavigationState();
    }

    onMyAccountButtonClick() {
        const {
            showOverlay,
            setNavigationState,
            navigationState: { name },
        } = this.props;

        if (isSignedIn()) {
            history.push({ pathname: appendWithStoreCode('/my-account/dashboard') });
            return;
        }

        if (!isMobile.any() && name !== CUSTOMER_ACCOUNT) {
            showOverlay(CUSTOMER_ACCOUNT_OVERLAY_KEY);
            setNavigationState({ name: CUSTOMER_ACCOUNT, title: 'Sign in' });
        }

        history.push({ pathname: appendWithStoreCode('/sign-in') });
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);

import CategorySort from 'Component/CategorySort/CategorySort.container';
import FilterSelector from 'Component/FilterSelector/FilterSelector.container';
import { DEFAULT_CURERNCY } from 'Component/LangPanel/LangPanel.component';
import PriceRangeSelector from 'Component/PriceRangeSelector';
// eslint-disable-next-line max-len
import ProductConfigurableAttributes from 'Component/ProductConfigurableAttributes/ProductConfigurableAttributes.component';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';

export class CategoryConfigurableAttributes extends ProductConfigurableAttributes {
    getPriceLabel(option) {
        const { currency_code } = this.props;
        const { value_string } = option;
        const [from, to] = value_string.split('_');
        const userPreferredCurrency = BrowserDatabase.getItem(DEFAULT_CURERNCY);
        const currency = userPreferredCurrency || currency_code;

        if (from === '*') {
            return __('Up to %s%s', to, currency);
        }

        if (to === '*') {
            return __('From %s%s', from, currency);
        }

        return __('From %s%s, to %s%s', from, currency, to, currency);
    }

    renderPriceSwatch(option) {
        const { attribute_options, ...priceOption } = option;

        if (attribute_options) {
            // do not render price filter if it includes "*_*" aggregation
            if (attribute_options['*_*']) {
                return null;
            }

            priceOption.attribute_options = Object.entries(attribute_options).reduce((acc, [key, option]) => {
                acc[key] = {
                    ...option,
                    label: this.getPriceLabel(option),
                };

                return acc;
            }, {});
        }

        return this.renderDropdown(priceOption);
    }

    renderCategorySort() {
        const { sortFields, selectedSort, onSortChange } = this.props;
        const { options = {} } = sortFields;

        const updatedSortFields = Object.values(options).map(({ value: id, label }) => ({ id, label }));
        const { sortDirection, sortKey } = selectedSort;

        return (
            <CategorySort
                onSortChange={onSortChange}
                sortFields={updatedSortFields}
                sortKey={sortKey}
                sortDirection={sortDirection}
            />
        );
    }

    renderPriceRange() {
        const { updatePriceRange, currency_code, currentArgs } = this.props;
        const userPreferredCurrency = BrowserDatabase.getItem(DEFAULT_CURERNCY);
        const currency = userPreferredCurrency || currency_code;

        return (
            <PriceRangeSelector
                key="price_range_selector"
                label={__('Price')}
                name="price"
                isWide
                updateFilter={updatePriceRange}
                currentFilterPriceRange={currentArgs?.filter?.priceRange || { min: 0, max: 0 }}
                currencyCode={currency}
            />
        );
    }

    renderConfigurableOption = (option) => {
        const { attribute_code } = option;

        switch (attribute_code) {
            case 'price':
                return this.renderPriceSwatch(option);
            default:
                return this.renderDropdown(option);
        }
    };

    renderConfigurableAttributes() {
        const { configurable_options } = this.props;

        return Object.values(configurable_options).map(this.renderConfigurableOption);
    }

    renderDropdown(attribute) {
        const { attribute_code, attribute_label, attribute_options, attribute_type } = attribute;
        const { updateFilter, isSelected, parameters, onFilterOverlayVisible } = this.props;

        if (attribute_code === 'category_id') {
            return null;
        }

        if (attribute_code === 'price') {
            return this.renderPriceRange();
        }

        // eslint-disable-next-line consistent-return
        return (
            <FilterSelector
                updateFilter={updateFilter}
                isSelected={isSelected}
                isDropdown={attribute_type !== 'boolean'}
                options={attribute_options}
                key={attribute_label}
                label={attribute_label}
                name={attribute_code}
                selectedOptions={parameters[attribute_code] || []}
                onFilterOverlayVisible={onFilterOverlayVisible}
            />
        );
    }

    renderContent() {
        return (
            <>
                {this.renderCategorySort()}
                {this.renderConfigurableAttributes()}
            </>
        );
    }

    render() {
        const { isReady, mix } = this.props;

        return (
            <div block="ProductConfigurableAttributes" mods={{ isLoading: !isReady }} mix={mix}>
                {isReady ? this.renderContent() : this.renderPlaceholders()}
            </div>
        );
    }
}

export default CategoryConfigurableAttributes;

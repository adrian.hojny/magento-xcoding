import React, { PureComponent } from 'react';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import PropTypes from 'prop-types';

import Link from 'Component/Link';
import { ROUTES } from 'Component/Router/Router.config';
import { MatchType } from 'Type/Common';

import './CollectionSelector.style.scss';

export default class CollectionSelector extends PureComponent {
    static propTypes = {
        collection: PropTypes.object.isRequired,
        match: MatchType.isRequired,
        storeViewCode: PropTypes.string.isRequired,
        selectedFilters: PropTypes.object.isRequired,
    };

    getCollectionData = () => {
        const {
            collection: { attribute_options = {} },
        } = this.props;

        const label = __('All');

        const collectionOptions = Object.entries(attribute_options).map(([key, { label, value_string }]) => ({
            label,
            value_string: `?customFilters=product_collection:${value_string}`,
            id: key,
        }));

        return [{ label, value_string: '', id: -1 }, ...collectionOptions];
    };

    getSubcategoryLinks = (data) =>
        data.map(({ label, value_string, id }) => {
            const { match, storeViewCode } = this.props;
            const { brandName } = match.params;

            return (
                <Link to={`${ROUTES.brand[storeViewCode]}/${brandName}${value_string}`} key={id} draggable={false}>
                    {label}
                </Link>
            );
        });

    render() {
        const {
            selectedFilters,
            collection: { attribute_options = {} },
        } = this.props;

        const selectedId =
            Object.keys(attribute_options).find(
                (id) => selectedFilters.product_collection && selectedFilters.product_collection.includes(id),
            ) || -1;

        const subcategoryData = this.getCollectionData();

        return (
            <ScrollMenu
                menuClass="CollectionSelector"
                itemClass="CollectionSelector-Item"
                itemClassActive="CollectionSelector-Item_active"
                arrowClass="CollectionSelector-ArrowWrapper"
                arrowDisabledClass="CollectionSelector-ArrowWrapper_hidden"
                arrowLeft={<div block="CollectionSelector" elem="Arrow" mods={{ left: true }} />}
                arrowRight={<div block="CollectionSelector" elem="Arrow" mods={{ right: true }} />}
                scrollToSelected
                scrollBy={5}
                alignCenter={false}
                selected={selectedId.toString()}
                data={this.getSubcategoryLinks(subcategoryData)}
            />
        );
    }
}

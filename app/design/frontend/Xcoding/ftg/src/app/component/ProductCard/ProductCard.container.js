import { connect } from 'react-redux';

import {
    mapDispatchToProps,
    ProductCardContainer as SourceProductCardContainer,
} from 'SourceComponent/ProductCard/ProductCard.container';

class ProductCardContainer extends SourceProductCardContainer {
    _getAvailableVisualOptions() {
        const {
            product: { configurable_options = [] },
        } = this.props;

        return Object.entries(configurable_options).reduce(
            (acc, [attribute_code, { attribute_options = {}, attribute_values }]) => {
                const visualOptions = Object.values(attribute_options).reduce((acc, option) => {
                    const { swatch_data, label, value: attrValue } = option;

                    const isEnabled = this.getIsConfigurableAttributeEnable({
                        attribute_code,
                        attribute_value: attrValue,
                    });

                    const { type, value } = swatch_data || {};

                    if (type === '0' && attribute_values.includes(attrValue) && isEnabled) {
                        acc.push({ value, label });
                    }

                    return acc;
                }, []);

                if (visualOptions.length > 0) return [...acc, ...visualOptions];
                return acc;
            },
            [],
        );
    }

    getIsConfigurableAttributeEnable = ({ attribute_code, attribute_value }) => {
        const {
            product: { variants },
        } = this.props;

        return variants.some(({ attributes }) => {
            const { attribute_value: foundValue } = attributes[attribute_code] || {};

            return foundValue === attribute_value;
        });
    };
}

export default connect(null, mapDispatchToProps)(ProductCardContainer);

import { connect } from 'react-redux';

import {
    CheckoutAddressBookContainer as SourceCheckoutAddressBookContainer,
    mapDispatchToProps,
    mapStateToProps,
} from 'SourceComponent/CheckoutAddressBook/CheckoutAddressBook.container';

export class CheckoutAddressBookContainer extends SourceCheckoutAddressBookContainer {
    containerFunctions = {
        onAddressSelect: this.onAddressSelect.bind(this),
        onIsCompanyChange: this.onIsCompanyChange.bind(this),
    };

    constructor(props) {
        super(props);

        const { requestCustomerData, customer, onAddressSelect, isSignedIn } = props;

        if (isSignedIn && !Object.keys(customer).length) {
            requestCustomerData();
        }

        const defaultAddressId = CheckoutAddressBookContainer._getDefaultAddressId(props);

        if (defaultAddressId) {
            onAddressSelect(defaultAddressId);
            this.estimateShipping(defaultAddressId);
        }

        this.state = {
            prevDefaultAddressId: defaultAddressId,
            selectedAddressId: defaultAddressId,
            isCompany: false,
        };
    }

    onIsCompanyChange(name, value) {
        this.setState({ isCompany: value });
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutAddressBookContainer);

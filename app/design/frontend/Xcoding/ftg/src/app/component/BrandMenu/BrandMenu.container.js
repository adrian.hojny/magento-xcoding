import { PureComponent } from 'react';
import { connect } from 'react-redux';

import CmsBlockQuery from 'Query/CmsBlock.query';
import { makeCancelable } from 'Util/Promise';
import { prepareQuery } from 'Util/Query';
import { executeGet } from 'Util/Request';
import { hash } from 'Util/Request/Hash';
import { ONE_MONTH_IN_SECONDS } from 'Util/Request/QueryDispatcher';
import BrandMenu from './BrandMenu.component';

export class BrandMenuContainer extends PureComponent {
    state = {
        blocks: null,
    };

    componentDidMount() {
        this.getCmsBlock(['brand-menu-right']);
    }

    containerFunctions = {
        fetchData: this.fetchData.bind(this),
        getCmsBlock: this.getCmsBlock.bind(this),
    };

    getCmsBlock(identifier) {
        this.fetchData(
            [CmsBlockQuery.getQuery({ identifiers: Array.from(identifier) })],
            ({ cmsBlocks: { items } }) => {
                if (!items.length) {
                    return;
                }

                this.setState({ blocks: items[0] });
            },
        );
    }

    fetchData(rawQueries, onSucces = () => {}, onError = () => {}) {
        const preparedQuery = prepareQuery(rawQueries);
        const { query, variables } = preparedQuery;
        const queryHash = hash(query + JSON.stringify(variables));
        if (!window.dataCache) {
            window.dataCache = {};
        }

        if (window.dataCache[queryHash]) {
            onSucces(window.dataCache[queryHash]);
            return;
        }

        this.promise = makeCancelable(executeGet(preparedQuery, this.dataModelName, ONE_MONTH_IN_SECONDS));

        this.promise.promise.then(
            (response) => {
                window.dataCache[queryHash] = response;
                onSucces(response);
            },
            (err) => {
                console.log({ err });
                return onError(err);
            },
        );
    }

    render() {
        return <BrandMenu {...this.state} {...this.props} {...this.containerFunctions} />;
    }
}

export default connect(null)(BrandMenuContainer);

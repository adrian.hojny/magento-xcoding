import React, { useEffect, useState } from 'react';
import SlickSlider from 'react-slick';
import PropTypes from 'prop-types';

import './StyledSlickSlider.style.scss';

const MoveDragThreshold = 10;

function useDragDetection() {
    const [mouseDown, setMouseDown] = useState(false);
    const [dragging, setDragging] = useState(false);

    useEffect(() => {
        let mouseMove = 0;

        function handleMouseUp() {
            setMouseDown(false);
        }

        function handleMouseMove(e) {
            mouseMove += Math.abs(e.movementX) + Math.abs(e.movementY);
            setDragging(mouseMove > MoveDragThreshold);
        }

        if (mouseDown) {
            document.addEventListener('mouseup', handleMouseUp);
            document.addEventListener('mousemove', handleMouseMove);
        }

        return () => {
            document.removeEventListener('mouseup', handleMouseUp);
            document.removeEventListener('mousemove', handleMouseMove);
        };
    }, [mouseDown]);

    function handleMouseDown() {
        setMouseDown(true);
        setDragging(false);
    }

    return {
        handleMouseDown,
        dragging,
    };
}

const Arrow = ({
    block,
    className,
    mods = {},
    onClickHandler,
    // currentSlide,
    // slideCount,
}) => <div elem="Arrow" block={block} mods={mods} onClick={onClickHandler} className={className} />;

const PrevArrow = ({ block, onClick, className, currentSlide, slideCount }) => (
    <Arrow
        block={block}
        mods={{ left: true }}
        onClickHandler={onClick}
        className={className}
        currentSlide={currentSlide}
        slideCount={slideCount}
    />
);

const NextArrow = ({ block, onClick, className, currentSlide, slideCount }) => (
    <Arrow
        block={block}
        mods={{ right: true }}
        onClickHandler={onClick}
        className={className}
        currentSlide={currentSlide}
        slideCount={slideCount}
    />
);

const StyledSlickSliderComponent = ({ children, block, ...rest }) => {
    const { handleMouseDown, dragging } = useDragDetection();

    function handleChildClick(e) {
        if (dragging) {
            e.preventDefault();
        }
    }

    return (
        <SlickSlider
            prevArrow={<PrevArrow block={block} />}
            nextArrow={<NextArrow block={block} />}
            className={`Slick-slider-wrap ${block}`}
            infinite={false}
            lazyLoad={false}
            variableWidth
            {...rest}
        >
            {React.Children.map(children, (child) => (
                <div onMouseDownCapture={handleMouseDown} onClickCapture={handleChildClick}>
                    {child}
                </div>
            ))}
        </SlickSlider>
    );
};

StyledSlickSliderComponent.propTypes = {
    children: PropTypes.node,
    block: PropTypes.string,
};

StyledSlickSliderComponent.defaultProps = {
    children: null,
    block: 'Slick-Slider',
};

export default StyledSlickSliderComponent;

import React from 'react';
import PropTypes from 'prop-types';

import Image from 'Component/Image/Image.container';
import { roundPrice } from 'Util/Price';

import './OrderProducts.style';

export const Product = ({
    qty,
    sku,
    name,
    special_price,
    original_price,
    thumbnail,
    attributes_info,
    currencyCode,
}) => {
    const renderAttributes = () =>
        attributes_info?.map(({ label, value }) => (
            <span key={label + value} block="OrderProduct" elem="Option">
                {`${label}: `}
                <strong>{value}</strong>
            </span>
        ));

    const renderPrice = () => {
        const formatPrice = (price) => `${roundPrice(price)} ${currencyCode}`;

        if (special_price) {
            return (
                <>
                    <span block="OrderProduct" elem="Price">
                        {formatPrice(special_price)}
                    </span>
                    <span block="OrderProduct" elem="Price" mods={{ Discount: true }}>
                        {formatPrice(original_price)}
                    </span>
                </>
            );
        }

        return (
            <span block="OrderProduct" elem="Price">
                {formatPrice(original_price)}
            </span>
        );
    };

    return (
        <li block="OrderProduct">
            <figure block="OrderProduct" elem="Wrapper">
                <div block="OrderProduct" elem="Link">
                    <Image
                        src={thumbnail.url}
                        alt={__('Product %s thumbnail.', thumbnail.label)}
                        mix={{
                            block: 'OrderProduct',
                            elem: 'Picture',
                        }}
                        ratio="custom"
                    />
                    <img style={{ display: 'none' }} alt={thumbnail.label} src={thumbnail.url} itemProp="image" />
                </div>
                <figcaption block="OrderProduct" elem="Content">
                    <p block="OrderProduct" elem="HeadingTop">
                        {sku}
                    </p>
                    <div block="OrderProduct" elem="PriceWrapper">
                        {renderPrice()}
                    </div>
                    <p block="OrderProduct" elem="Heading">
                        {name}
                    </p>
                    <div block="OrderProduct" elem="OptionsWrapper">
                        {renderAttributes()}
                        <span block="OrderProduct" elem="Option">
                            {`${__('Quantity')}: `}
                            <strong>{qty}</strong>
                        </span>
                    </div>
                </figcaption>
            </figure>
        </li>
    );
};

Product.propTypes = {
    qty: PropTypes.number.isRequired,
    sku: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    original_price: PropTypes.number.isRequired,
    thumbnail: PropTypes.shape({
        label: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
    }).isRequired,
    attributes_info: PropTypes.arrayOf(
        PropTypes.shape({
            label: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired,
        }),
    ).isRequired,
    currencyCode: PropTypes.string.isRequired,
    special_price: PropTypes.number,
};

Product.defaultProps = {
    special_price: null,
};

export default Product;

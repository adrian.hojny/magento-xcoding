import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ContentWrapper from 'Component/ContentWrapper';
import ExpandableContent from 'Component/ExpandableContent';
import ProductAddReviewButton from 'Component/ProductAddReviewButton';
import { REVIEW_TAB_ID } from 'Route/ProductPage/ProductPage.component';
import { getWebpImagePath } from 'Util/Image/Image';
import isMobile from 'Util/Mobile/isMobile';

import './ProductCardTabs.style.scss';

class ProductCardTabs extends PureComponent {
    static propTypes = {
        product: PropTypes.object.isRequired,
        tabs: PropTypes.array.isRequired,
        activeTab: PropTypes.string.isRequired,
        setActiveTab: PropTypes.func.isRequired,
        toggleReviewSidebar: PropTypes.func.isRequired,
    };

    onTabButtonClick = (id) => () => {
        const { setActiveTab } = this.props;
        setActiveTab(id);
    };

    renderProductImage() {
        const {
            product: { media_gallery_entries, title },
            tabs,
            activeTab,
        } = this.props;

        if (!media_gallery_entries) return null;

        const indexOfActiveTab = tabs.findIndex(({ id }) => id === activeTab) || 0;
        const imageIndex = indexOfActiveTab % media_gallery_entries.length;
        const imageUrl = media_gallery_entries && media_gallery_entries[imageIndex]?.small.url;

        if (imageUrl) {
            const { webpImagePath, sourceImageFormat } = getWebpImagePath(imageUrl);

            return (
                <picture>
                    {webpImagePath && <source srcSet={webpImagePath} type="image/webp" />}
                    <source srcSet={imageUrl} type={`image/${sourceImageFormat}`} />
                    <img src={imageUrl} alt={title} block="ProductCardTabs" elem="SideImage" loading="lazy" />
                </picture>
            );
        }
    }

    renderButton(id, title, activeTab) {
        return (
            <button
                block="ProductCardTabs"
                elem="Button"
                mods={{ isActive: id === activeTab }}
                onClick={this.onTabButtonClick(id)}
                key={id}
            >
                {title}
            </button>
        );
    }

    renderTabs() {
        const { tabs, activeTab } = this.props;

        return tabs.map(
            ({ id, title, content }) =>
                content !== null && (
                    <div block="ProductCardTabs" elem="Tab" mods={{ isActive: id === activeTab }} key={id}>
                        <ExpandableContent heading={title} mix={{ block: 'ProductCardTabs', elem: 'Content' }}>
                            {content}
                        </ExpandableContent>
                    </div>
                ),
        );
    }

    renderDesktopNav() {
        const { tabs, activeTab } = this.props;

        return (
            <div block="ProductCardTabs" elem="Nav">
                {tabs.map(({ id, title, content }) => content !== null && this.renderButton(id, title, activeTab))}
            </div>
        );
    }

    renderReviewButton() {
        const { toggleReviewSidebar, activeTab } = this.props;

        if (activeTab === REVIEW_TAB_ID) {
            return <ProductAddReviewButton toggleReviewSidebar={toggleReviewSidebar} mods={{ isAbsolute: true }} />;
        }

        return null;
    }

    render() {
        return (
            <ContentWrapper wrapperMix={{ block: 'ProductCardTabs', elem: 'Wrapper' }} label="Additional information">
                <div>
                    {this.renderDesktopNav()}
                    {this.renderTabs()}
                </div>
                <div>
                    {!isMobile.any() && this.renderProductImage()}
                    {this.renderReviewButton()}
                </div>
            </ContentWrapper>
        );
    }
}

export default ProductCardTabs;

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { PAYMENT_TOTALS } from 'SourceRoute/Checkout/Checkout.config';

import { PAYPAL_EXPRESS } from 'Component/CheckoutPayments/CheckoutPayments.config';
import { CLICKED_CATEGORIES } from 'Component/ProductList/ProductList.component';
import CheckoutQuery from 'Query/Checkout.query';
import PayPalQuery from 'Query/PayPal.query';
import { ORDER_DATA_EXPIRATION_TIME } from 'Route/Checkout/Checkout.container';
import { GUEST_QUOTE_ID } from 'Store/Cart/Cart.dispatcher';
import { showNotification } from 'Store/Notification/Notification.action';
import { isSignedIn } from 'Util/Auth';
import BrowserDatabase from 'Util/BrowserDatabase';
import { getGuestCartId } from 'Util/Cart';
import { getProductsForGTM } from 'Util/Product/Product';
import { fetchMutation } from 'Util/Request';
import PayPal from './PayPal.component';
import { PAYPAL_SCRIPT } from './PayPal.config';

const ADDITIONAL_ORDER_INFO = 'ADDITIONAL_ORDER_INFO';

export const CartDispatcher = import(
    /* webpackMode: "lazy", webpackChunkName: "dispatchers" */
    'Store/Cart/Cart.dispatcher'
);

export const mapStateToProps = (state) => ({
    cartTotals: state.CartReducer.cartTotals,
    storeCode: state.ConfigReducer.code,
    clientId: state.ConfigReducer.paypal_client_id,
    isSandboxEnabled: state.ConfigReducer.paypal_sandbox_flag,
});

export const mapDispatchToProps = (dispatch) => ({
    showNotification: (type, message, e) => dispatch(showNotification(type, message, e)),
});

export class PayPalContainer extends PureComponent {
    static propTypes = {
        clientId: PropTypes.string,
        storeCode: PropTypes.string,
        isSandboxEnabled: PropTypes.bool,
        setLoading: PropTypes.func.isRequired,
        setDetailsStep: PropTypes.func.isRequired,
        showNotification: PropTypes.func.isRequired,
        selectedPaymentCode: PropTypes.string.isRequired,
        cartTotals: PropTypes.object.isRequired,
    };

    static defaultProps = {
        clientId: 'sb',
        isSandboxEnabled: false,
    };

    componentDidMount() {
        const script = document.getElementById(PAYPAL_SCRIPT);

        if (script) {
            script.onload = () => this.forceUpdate();
        }
    }

    componentWillUnmount() {
        // resetting all pay-pal related properties
        Object.keys(window).forEach((key) => {
            if (/paypal|zoid|post_robot/.test(key)) {
                // eslint-disable-next-line fp/no-delete
                delete window[key];
            }
        });
    }

    containerProps = () => ({
        paypal: this.getPayPal(),
        environment: this.getEnvironment(),
        isDisabled: this.getIsDisabled(),
    });

    getIsDisabled = () => {
        const { selectedPaymentCode } = this.props;
        return selectedPaymentCode !== PAYPAL_EXPRESS;
    };

    containerFunctions = () => ({
        onError: this.onError,
        onCancel: this.onCancel,
        onApprove: this.onApprove,
        createOrder: this.createOrder,
    });

    saveCheckoutGAData = () => {
        const { cartTotals } = this.props;
        const categoriesData = BrowserDatabase.getItem(CLICKED_CATEGORIES) || {};

        const productsGTM = getProductsForGTM(cartTotals.items, categoriesData);
        gtag('event', 'checkout_progress', {
            items: Array.from(productsGTM),
            checkout_step: 3,
            checkout_option: 'Paypal',
        });
    };

    onApprove = async (data) => {
        const { showNotification, setDetailsStep, storeCode } = this.props;
        const { orderID, payerID } = data;
        const guest_cart_id = isSignedIn() ? '' : getGuestCartId();
        const additionalData = BrowserDatabase.getItem(ADDITIONAL_ORDER_INFO);

        this.saveCheckoutGAData();

        try {
            await fetchMutation(
                CheckoutQuery.getSetPaymentMethodOnCartMutation({
                    guest_cart_id,
                    payment_method: {
                        code: 'paypal_express',
                        paypal_express: {
                            token: orderID,
                            payer_id: payerID,
                        },
                    },
                }),
            );

            const orderData = await fetchMutation(CheckoutQuery.getPlaceOrderMutation(guest_cart_id));
            const {
                placeOrder: { order: { order_id } = {}, order },
            } = orderData;

            const dataToSave = {
                ...order,
                email: additionalData?.email || additionalData?.stateEmail,
                shippingAdditionalInformation: additionalData?.shippingAdditionalInformation,
                currencyCode: additionalData?.quote_currency_code,
                shippingAmount: additionalData?.shipping_amount,
                paymentInstruction: false,
                isBlik: false,
                isP24: false,
            };

            BrowserDatabase.setItem(dataToSave, `order${order_id}`, ORDER_DATA_EXPIRATION_TIME);
            if (!isSignedIn()) {
                BrowserDatabase.deleteItem(`${GUEST_QUOTE_ID}_${storeCode}`);
            }
            BrowserDatabase.deleteItem(ADDITIONAL_ORDER_INFO);
            BrowserDatabase.deleteItem(PAYMENT_TOTALS);
            setDetailsStep(dataToSave);
        } catch (e) {
            showNotification('error', 'Something went wrong');
        }
    };

    onCancel = (data) => {
        const { showNotification, setLoading } = this.props;
        setLoading(false);
        showNotification('info', 'Your payment has been canceled', data);
    };

    onError = (err) => {
        const { showNotification, setLoading } = this.props;
        setLoading(false);
        showNotification('error', 'Some error appeared with PayPal', err);
    };

    getPayPal = () => {
        const { paypal } = window;
        return paypal || false;
    };

    getEnvironment = () => {
        const { isSandboxEnabled } = this.props;
        return isSandboxEnabled ? 'sandbox' : 'production';
    };

    createOrder = async () => {
        const { setLoading, selectedPaymentCode } = this.props;
        const guest_cart_id = isSignedIn() ? '' : getGuestCartId();
        setLoading(true);

        const {
            paypalExpress: { token },
        } = await fetchMutation(
            PayPalQuery.getCreatePaypalExpressTokenMutation({
                guest_cart_id,
                express_button: false,
                code: selectedPaymentCode,
                // use_paypal_credit: this.getIsCredit(),
                urls: {
                    cancel_url: 'www.paypal.com/checkoutnow/error',
                    return_url: 'www.paypal.com/checkoutnow/error',
                },
            }),
        );

        return token;
    };

    render() {
        return <PayPal {...this.props} {...this.containerProps()} {...this.containerFunctions()} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PayPalContainer);

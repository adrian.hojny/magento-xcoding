import React from 'react';

import SourceProductLinks from 'SourceComponent/ProductLinks/ProductLinks.component';

import './ProductVariants.style';

export * from 'SourceComponent/ProductLinks/ProductLinks.component';

class ProductVariants extends SourceProductLinks {
    renderHeading() {
        const { title } = this.props;

        return (
            <h4 block="ProductVariants" elem="Title">
                {title}
            </h4>
        );
    }

    renderItems() {
        const {
            linkType,
            linkedProducts: {
                [linkType]: { items },
            },
            numberOfProductsToDisplay,
        } = this.props;

        if (!items) {
            return Array.from({ length: numberOfProductsToDisplay }, (_, i) => this.renderProductCard({}, i));
        }

        return items.slice(0, numberOfProductsToDisplay).map(this.renderProductCard);
    }

    renderCurrentVariant() {
        const {
            product,
            linkType,
            linkedProducts: {
                [linkType]: { items },
            },
        } = this.props;

        if (!items) {
            return '';
        }

        return this.renderProductCard(product);
    }

    render() {
        const { areDetailsLoaded } = this.props;

        if (!areDetailsLoaded) {
            return null;
        }

        return (
            <>
                {this.renderHeading()}
                <ul block="ProductVariants" elem="List">
                    {this.renderCurrentVariant()}
                    {this.renderItems()}
                </ul>
            </>
        );
    }
}

export default ProductVariants;

import { connect } from 'react-redux';

import {
    CheckoutGuestFormContainer as SourceCheckoutGuestFormContainer,
    mapDispatchToProps,
    mapStateToProps,
} from 'SourceComponent/CheckoutGuestForm/CheckoutGuestForm.container';

import CheckoutGuestForm from './CheckoutGuestForm.component';

export class CheckoutGuestFormContainer extends SourceCheckoutGuestFormContainer {
    render() {
        const { isSignedIn, isBilling } = this.props;

        if (isSignedIn || isBilling) {
            return null;
        }

        return <CheckoutGuestForm {...this.props} {...this.containerFunctions} {...this.containerProps()} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutGuestFormContainer);

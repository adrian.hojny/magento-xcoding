import { connect } from 'react-redux';

import {
    mapStateToProps as sourceMapStateToProps,
    ProductActionsContainer as SourceProductActionsContainer,
} from 'SourceComponent/ProductActions/ProductActions.container';

import ProductActions from 'Component/ProductActions/ProductActions.component';
import { toggleOverlayByKey } from 'Store/Overlay/Overlay.action';
import { DEFAULT_MAX_PRODUCTS, IS_AVALABLE_CHECK, IS_ENABLED_CHECK, IS_LAST_ITEM_CHECK } from './ProductActions.config';

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    storeViewCode: state.ConfigReducer.code,
});

export const mapDispatchToProps = (dispatch) => ({
    toggleOverlayByKey: (key) => dispatch(toggleOverlayByKey(key)),
});

export { DEFAULT_MAX_PRODUCTS };

export class ProductActionsContainer extends SourceProductActionsContainer {
    containerFunctions = {
        showOnlyIfLoaded: this.showOnlyIfLoaded.bind(this),
        onProductValidationError: this.onProductValidationError.bind(this),
        getIsOptionInCurrentVariant: this.getIsOptionInCurrentVariant.bind(this),
        setQuantity: this.setQuantity.bind(this),
        setGroupedProductQuantity: this._setGroupedProductQuantity.bind(this),
        clearGroupedProductQuantity: this._clearGroupedProductQuantity.bind(this),
        getProductAvailability: this.getProductAvailability.bind(this),
    };

    getOfferType() {
        return 'https://schema.org/Offer';
    }

    getProductAvailability({ attribute_code, attribute_value }, checkType) {
        if (!checkType) {
            throw new Error('checkType is required!');
        }

        const {
            parameters,
            product: { variants },
        } = this.props;

        const isAttributeSelected = Object.hasOwnProperty.call(parameters, attribute_code);

        if (checkType === IS_ENABLED_CHECK || checkType === IS_AVALABLE_CHECK) {
            // If value matches current attribute_value, option should be enabled
            if (isAttributeSelected && parameters[attribute_code] === attribute_value) {
                return true;
            }
        }

        const parameterPairs = Object.entries(parameters);

        const selectedAttributes = isAttributeSelected
            ? // Need to exclude itself, otherwise different attribute_values of the same attribute_code will always be disabled
              parameterPairs.filter(([key]) => key !== attribute_code)
            : parameterPairs;

        if (checkType === IS_ENABLED_CHECK) {
            return variants.some(({ attributes }) => {
                const { attribute_value: foundValue } = attributes[attribute_code] || {};

                return (
                    foundValue === attribute_value &&
                    // Variant must have all currently selected attributes
                    selectedAttributes.every(([key, value]) => attributes[key].attribute_value === value)
                );
            });
        }

        if (checkType === IS_AVALABLE_CHECK || checkType === IS_LAST_ITEM_CHECK) {
            return variants.some(({ attributes, salable_qty }) => {
                const { attribute_value: foundValue } = attributes[attribute_code] || {};
                const isAvailableOrLastItem = checkType === IS_LAST_ITEM_CHECK ? salable_qty === 1 : salable_qty >= 1;

                return (
                    isAvailableOrLastItem &&
                    // Variant must have currently checked attribute_code and attribute_value
                    foundValue === attribute_value &&
                    // Variant must have all currently selected attributes
                    selectedAttributes.every(([key, value]) => attributes[key].attribute_value === value)
                );
            });
        }

        return null;
    }

    render() {
        return (
            <ProductActions {...this.props} {...this.state} {...this.containerProps()} {...this.containerFunctions} />
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductActionsContainer);

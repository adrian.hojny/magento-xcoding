import React from 'react';
import { Subscribe } from 'unstated';

import ProductGalleryContainer from 'Component/ProductGallery/ProductGallery.container';
import SharedTransitionContainer from 'Component/SharedTransition/SharedTransition.unstated';
import ProductGalleryOverlayMobile from './ProductGalleryOverlayMobile.component';

export * from 'Component/ProductGallery/ProductGallery.container';

class ProductGalleryOverlaySliderContainer extends ProductGalleryContainer {
    render() {
        return (
            <Subscribe to={[SharedTransitionContainer]}>
                {({ registerSharedElementDestination }) => (
                    <ProductGalleryOverlayMobile
                        registerSharedElementDestination={registerSharedElementDestination}
                        {...this.containerProps()}
                        {...this.containerFunctions}
                    />
                )}
            </Subscribe>
        );
    }
}

export default ProductGalleryOverlaySliderContainer;

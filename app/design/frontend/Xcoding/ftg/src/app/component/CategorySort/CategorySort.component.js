import PropTypes from 'prop-types';

import FilterSelector, { FILTER_SELECTOR_OVERLAY_ID } from 'Component/FilterSelector/FilterSelector.component';
import FilterSelectorOption from 'Component/FilterSelectorOption';
import { FILTER_DETAIL } from 'Component/Header/Header.component';
import isMobile from 'Util/Mobile';

export default class CategorySort extends FilterSelector {
    static propTypes = {
        ...FilterSelector.propTypes,
        updateFilter: PropTypes.func,
    };

    selectOption = (direction, key) => {
        const { onSortChange } = this.props;

        return () => {
            onSortChange(direction, key);
            this.optionsOverlayClose();
        };
    };

    dropdownToggle = () => {
        const { toggleOverlayByKey, changeHeaderState } = this.props;
        this.setState(({ isOpen }) => ({ isOpen: !isOpen }));

        if (isMobile.width()) {
            changeHeaderState({
                name: FILTER_DETAIL,
                title: __('Sorting'),
                onCloseClick: this.optionsOverlayClose,
                onBackClick: this.optionsOverlayClose,
            });
            toggleOverlayByKey(FILTER_SELECTOR_OVERLAY_ID);
        }
    };

    renderOptions() {
        const { selectOptions, sortDirection, sortKey } = this.props;

        return (
            <div block="FilterSelector" elem="Options">
                {selectOptions.map(({ value, label, direction, name }) => (
                    <FilterSelectorOption
                        key={value}
                        label={label}
                        value={value}
                        isSelected={value === `${sortDirection} ${sortKey}`}
                        selectOption={this.selectOption(direction, name)}
                        isCheckbox={false}
                    />
                ))}
            </div>
        );
    }
}

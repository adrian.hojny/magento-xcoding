import React, { PureComponent } from 'react';

import CmsBlock from 'Component/CmsBlock';

import './StaticBlocksFull.style.scss';

const BOTTOM_LEFT_BANNER = 'category-bottom-fullwidth-left';
const BOTTOM_RIGHT_BANNER = 'category-bottom-fullwidth-right';

export default class StaticBlocksFullComponent extends PureComponent {
    render() {
        return (
            <div block="CategoryPage" elem="BottomFullWidth">
                <div block="CategoryPage" elem="BottomLeftBanner">
                    <CmsBlock identifier={BOTTOM_LEFT_BANNER} />
                </div>
                <div block="CategoryPage" elem="BottomRightBanner">
                    <CmsBlock identifier={BOTTOM_RIGHT_BANNER} />
                </div>
            </div>
        );
    }
}

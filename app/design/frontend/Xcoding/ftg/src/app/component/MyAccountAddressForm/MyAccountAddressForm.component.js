import { MyAccountAddressForm as SourceMyAccountAddressForm } from 'SourceComponent/MyAccountAddressForm/MyAccountAddressForm.component';

export class MyAccountAddressForm extends SourceMyAccountAddressForm {
    get fieldMap() {
        const { countryId } = this.state;
        const { countries, address } = this.props;
        const { default_billing, default_shipping, street = [] } = address;

        return {
            default_billing: {
                type: 'checkbox',
                label: __('This is default Billing Address'),
                value: 'default_billing',
                checked: default_billing,
            },
            default_shipping: {
                type: 'checkbox',
                label: __('This is default Shipping Address'),
                value: 'default_shipping',
                checked: default_shipping,
            },
            firstname: {
                label: __('First name'),
                validation: ['notEmpty', 'maxLength'],
            },
            lastname: {
                label: __('Last name'),
                validation: ['notEmpty', 'maxLength'],
            },
            telephone: {
                label: __('Phone number'),
                validation: ['notEmpty', 'maxLength'],
            },
            city: {
                label: __('City'),
                validation: ['notEmpty'],
            },
            country_id: {
                type: 'select',
                label: __('Country'),
                validation: ['notEmpty'],
                value: countryId,
                selectOptions: countries.map(({ id, label }) => ({ id, label, value: id })),
                onChange: this.onCountryChange,
            },
            ...this.getRegionFields(),
            postcode: {
                label: __('Zip/Postal code', 'maxLength'),
                validation: ['notEmpty'],
            },
            street: {
                label: __('Street address', 'maxLength'),
                value: street[0],
                validation: ['notEmpty'],
            },
        };
    }
}

export default MyAccountAddressForm;

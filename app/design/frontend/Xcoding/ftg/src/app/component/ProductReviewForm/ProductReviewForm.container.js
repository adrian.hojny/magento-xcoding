import { connect } from 'react-redux';

import {
    mapDispatchToProps,
    mapStateToProps,
    ProductReviewFormContainer as SourceProductReviewFormContainer,
} from 'SourceComponent/ProductReviewForm/ProductReviewForm.container';

import { COMFORT_FIELD_NAME, QUALITY_FIELD_NAME, RECOMMENDED, SIZE_FIELD_NAME } from 'Type/Rating';

export const REVIEW_TITLE_LENGTH = 25;

class ProductReviewFormContainer extends SourceProductReviewFormContainer {
    containerFunctions = {
        onReviewSubmitAttempt: this._onReviewSubmitAttempt.bind(this),
        onReviewSubmitSuccess: this._onReviewSubmitSuccess.bind(this),
        onStarRatingClick: this._onStarRatingClick.bind(this),
        handleComfortChange: this._handleFieldChange.bind(this, 'comfort'),
        handleSizeChange: this._handleFieldChange.bind(this, 'size'),
        handleQualityChange: this._handleFieldChange.bind(this, 'quality'),
        handleDetailChange: this._handleFieldChange.bind(this, 'detail'),
        handleRecommendationChange: this._handleFieldChange.bind(this, 'isRecommended'),
        onReviewError: this._onReviewError.bind(this),
    };

    constructor(props) {
        super(props);

        const reviewData = { isRecommended: RECOMMENDED };

        this.state = {
            isLoading: false,
            ratingData: {},
            reviewData,
        };
    }

    _onReviewSubmitAttempt(_, invalidFields) {
        const { showNotification, reviewRatings } = this.props;
        const { ratingData, reviewData } = this.state;

        const reviewsAreNotValid =
            invalidFields ||
            !reviewRatings.some(({ rating_id }) => ratingData[rating_id]) ||
            !reviewData.quality ||
            !reviewData.size ||
            !reviewData.comfort;

        if (reviewsAreNotValid) {
            showNotification('info', __('Incorrect data! Please check review fields.'));
        }

        this.setState({ isLoading: !reviewsAreNotValid });
    }

    _onReviewSubmitSuccess(fields) {
        const {
            product,
            addReview,
            hideActiveOverlay,
            goToPreviousHeaderState,
            customer: { firstname },
        } = this.props;

        const {
            isLoading,
            ratingData: rating_data,
            reviewData: { comfort, size, quality, isRecommended },
        } = this.state;

        const { detail } = fields;

        const { sku: product_sku } = product;

        const title = `${detail.substr(0, REVIEW_TITLE_LENGTH)}${detail.length > REVIEW_TITLE_LENGTH ? '...' : ''}`;

        if (Object.keys(rating_data).length && isLoading) {
            addReview({
                nickname: firstname || 'anonymous',
                title,
                detail,
                product_sku,
                rating_data,
                extended_rating_data: {
                    [COMFORT_FIELD_NAME]: comfort,
                    [QUALITY_FIELD_NAME]: quality,
                    [SIZE_FIELD_NAME]: size,
                    recommended_value: isRecommended,
                },
            }).then((success) => {
                if (success) {
                    this.setState({
                        ratingData: {},
                        reviewData: {},
                        isLoading: false,
                    });

                    goToPreviousHeaderState();
                    hideActiveOverlay();

                    return;
                }

                this.setState({ isLoading: false });
            });
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductReviewFormContainer);

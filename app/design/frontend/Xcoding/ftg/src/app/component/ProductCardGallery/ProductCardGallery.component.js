import React from 'react';
import { withRouter } from 'react-router';

import ProductCardGalleryImage from 'Component/ProductCardGalleryImage';
import { ProductGallery as SourceProductGallery } from 'Component/ProductGallery/ProductGallery.component';
import ProductLabels from 'Component/ProductLabels';
import VideoPopup from 'Component/VideoPopup/VideoPopup.container';

import './ProductCardGallery.style.scss';

export * from 'Component/ProductGallery/ProductGallery.component';

class ProductCardGalleryComponent extends SourceProductGallery {
    componentDidUpdate(prevProps) {
        const {
            productId,
            location: { pathname },
        } = this.props;
        const {
            productId: prevProductId,
            location: { pathname: prevPathname },
        } = prevProps;

        if (productId !== prevProductId) {
            this.updateSharedDestinationElement();
        }

        if (this.sliderRef && pathname !== prevPathname) {
            if (this.sliderRef.current) {
                CSS.setVariable(this.sliderRef.current.draggableRef, 'animation-speed', 0);
            }
        }
    }

    renderImage(mediaData, index) {
        const { onActiveImageChange, showGallery, updateActiveImageOverlayGallery } = this.props;

        return (
            <picture block="ProductCardGallery" elem="ImageWrapper" key={index}>
                <ProductCardGalleryImage
                    index={index}
                    onActiveImageChange={onActiveImageChange}
                    media={mediaData}
                    updateActiveImageOverlayGallery={updateActiveImageOverlayGallery}
                    showGallery={showGallery}
                />
            </picture>
        );
    }

    renderLabels() {
        const { attributes } = this.props;

        return <ProductLabels attributes={attributes} />;
    }

    render() {
        const { gallery } = this.props;

        return (
            <div block="ProductCardGallery">
                {this.renderLabels()}
                {gallery.map(this.renderSlide)}
                <VideoPopup />
            </div>
        );
    }
}

export default withRouter(ProductCardGalleryComponent);

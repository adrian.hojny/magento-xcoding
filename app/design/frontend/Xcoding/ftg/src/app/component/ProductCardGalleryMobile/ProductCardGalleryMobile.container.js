import React from 'react';
import PropTypes from 'prop-types';
import { Subscribe } from 'unstated';

import SourceProductGalleryContainer from 'SourceComponent/ProductGallery/ProductGallery.container';

import ProductCardGalleryMobile from 'Component/ProductCardGalleryMobile/ProductCardGalleryMobile.component';
import SharedTransitionContainer from 'Component/SharedTransition/SharedTransition.unstated';
import { ProductType } from 'Type/ProductList';

export * from 'SourceComponent/ProductGallery/ProductGallery.container';

class ProductCardGalleryMobileContainer extends SourceProductGalleryContainer {
    static propTypes = {
        product: ProductType.isRequired,
        areDetailsLoaded: PropTypes.bool,
        galleryType: PropTypes.string,
        showGallery: PropTypes.func,
    };

    containerProps = () => {
        const { activeImage, isZoomEnabled } = this.state;
        const {
            product: { id },
            galleryType,
            showGallery,
            attributes,
        } = this.props;

        return {
            gallery: this.getGalleryPictures(),
            productName: this._getProductName(),
            activeImage,
            isZoomEnabled,
            productId: id,
            galleryType,
            showGallery,
            attributes,
        };
    };

    render() {
        return (
            <Subscribe to={[SharedTransitionContainer]}>
                {({ registerSharedElementDestination }) => (
                    <ProductCardGalleryMobile
                        registerSharedElementDestination={registerSharedElementDestination}
                        {...this.containerProps()}
                        {...this.containerFunctions}
                    />
                )}
            </Subscribe>
        );
    }
}

export default ProductCardGalleryMobileContainer;

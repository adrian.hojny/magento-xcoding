import { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { toggleBreadcrumbs } from 'Store/Breadcrumbs/Breadcrumbs.action';
import { MyAccountDispatcher } from 'Store/MyAccount/MyAccount.dispatcher';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { isSignedIn } from 'Util/Auth';
import history from 'Util/History';
import { appendWithStoreCode } from 'Util/Url';

export const mapDisptachToProps = (dispatch) => ({
    requestCustomerData: () => MyAccountDispatcher.requestCustomerData(dispatch),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    toggleBreadcrumbs: (state) => dispatch(toggleBreadcrumbs(state)),
});

export const mapStateToProps = (state) => ({
    isSignedIn: state.MyAccountReducer.isSignedIn,
});

export class AccountAbstractContainer extends PureComponent {
    static propTypes = {
        requestCustomerData: PropTypes.func.isRequired,
        changeHeaderState: PropTypes.func.isRequired,
        isSignedIn: PropTypes.bool.isRequired,
        toggleBreadcrumbs: PropTypes.func.isRequired,
    };

    state = {
        isLoading: false,
    };

    componentDidMount() {
        const { toggleBreadcrumbs } = this.props;

        if (isSignedIn()) {
            history.push(appendWithStoreCode('/my-account/dashboard'));
        }

        toggleBreadcrumbs(false);
    }

    onSignIn() {
        history.push(appendWithStoreCode('/my-account/dashboard'));
    }

    stopLoading = () => this.setState({ isLoading: false });
}

export default connect(mapStateToProps, mapDisptachToProps)(AccountAbstractContainer);

import { connect } from 'react-redux';

import {
    CheckoutPaymentsContainer as SourceCheckoutPaymentsContainer,
    mapDispatchToProps as sourceMapDispatchToProps,
} from 'SourceComponent/CheckoutPayments/CheckoutPayments.container';

import { BRAINTREE_CONTAINER_ID } from 'Component/Braintree/Braintree.config';
import CheckoutPayments from 'Component/CheckoutPayments/CheckoutPayments.component';
import { BILLING_STEP } from 'Route/Checkout/Checkout.config';
import { purgeAllFormField } from 'Store/P24/P24.actions';
import BraintreeDropIn from 'Util/Braintree';
import { BRAINTREE, KLARNA, PRZELEWY24, STRIPE } from './CheckoutPayments.config';

export const mapStateToProps = (state) => ({
    p24FormValues: state.P24Reducer.formValues,
});

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    purgeAllFormField: (name) => dispatch(purgeAllFormField(name)),
});

export class CheckoutPaymentsContainer extends SourceCheckoutPaymentsContainer {
    containerFunctions = {
        initBraintree: this.initBraintree.bind(this),
        setStripeRef: this.setStripeRef.bind(this),
        selectPaymentMethod: this.selectPaymentMethod.bind(this),
    };

    braintree = new BraintreeDropIn(BRAINTREE_CONTAINER_ID);

    dataMap = {
        [BRAINTREE]: this.getBraintreeData.bind(this),
        [STRIPE]: this.getStripeData.bind(this),
        [KLARNA]: this.getKlarnaData.bind(this),
        [PRZELEWY24]: this.getPrzelewy24Data.bind(this),
    };

    componentWillUnmount() {
        const { purgeAllFormField } = this.props;
        purgeAllFormField();
        if (window.formPortalCollector) {
            window.formPortalCollector.unsubscribe(BILLING_STEP, 'CheckoutPaymentsContainer');
        }
    }

    collectAdditionalData = () => {
        const { selectedPaymentCode } = this.state;
        // debugger;
        const additionalDataGetter = this.dataMap[selectedPaymentCode];
        if (!additionalDataGetter) {
            return {};
        }

        const data = additionalDataGetter();

        return additionalDataGetter();
    };

    selectPaymentMethod({ code }) {
        const { onPaymentMethodSelect, setOrderButtonEnableStatus, purgeAllFormField } = this.props;

        this.setState({
            selectedPaymentCode: code,
        });

        purgeAllFormField();
        onPaymentMethodSelect(code);
        setOrderButtonEnableStatus(true);
    }

    getPrzelewy24Data() {
        const { p24FormValues } = this.props;
        return {
            inputValues: p24FormValues,
            invalidFields: [],
        };
    }

    getPrzelewy24SampleMockMethodsData() {
        return {
            methods: internalP24PaymentMethods,
        };
    }

    render() {
        return <CheckoutPayments {...this.props} {...this.containerFunctions} {...this.state} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPaymentsContainer);

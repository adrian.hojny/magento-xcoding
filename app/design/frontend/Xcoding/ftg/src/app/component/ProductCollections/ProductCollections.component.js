import React, { PureComponent } from 'react';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import PropTypes from 'prop-types';

import CarouselWrapper from 'Component/CarouselWrapper';
import ContentWrapper from 'Component/ContentWrapper';
import Link from 'Component/Link';
import ProductCard from 'Component/ProductCard/ProductCard.container';

import './ProductCollections.style';

const CAROUSEL_PRODUCTS_TO_RENDER = 10;
export const RENDER_TYPE = 'listing';

export default class ProductCollections extends PureComponent {
    static propTypes = {
        getProductCollections: PropTypes.func.isRequired,
        productCollections: PropTypes.any.isRequired,
        renderType: PropTypes.string,
        product: PropTypes.any,
    };

    static defaultProps = {
        renderType: 'carousel',
        product: {},
    };

    componentDidMount() {
        const { renderType } = this.props;

        if (renderType !== RENDER_TYPE) {
            const { getProductCollections } = this.props;
            const productCollectionId = this.getProductCollectionId();

            if (productCollectionId) {
                getProductCollections(productCollectionId);
            } else {
                getProductCollections(null);
            }
        }
    }

    componentDidUpdate(prevProps) {
        const { renderType } = this.props;

        if (renderType !== RENDER_TYPE) {
            const { getProductCollections, product } = this.props;
            const { product: prevProduct } = prevProps;
            const {
                product: { attributes },
            } = this.props;

            if (attributes && attributes.product_collection) {
                const collectionsValue = attributes.product_collection;

                if (collectionsValue.attribute_value) {
                    const productCollectionId = collectionsValue.attribute_value.split(',')[0];

                    if (!prevProduct.id || prevProduct.id !== product.id) {
                        if (productCollectionId) {
                            getProductCollections(productCollectionId);
                        } else {
                            getProductCollections(null);
                        }
                    }
                }
            }
        }
    }

    getProductCollectionId() {
        const {
            product: { attributes },
        } = this.props;

        if (attributes && attributes.product_collection) {
            const collectionsValue = attributes.product_collection;

            if (collectionsValue.attribute_value) {
                return collectionsValue.attribute_value.split(',')[0];
            }
        }

        return null;
    }

    renderHeader() {
        return (
            <ContentWrapper wrapperMix={{ block: 'ProductCollections', elem: 'Header' }} label="Product Collections">
                <h1 block="ProductCollections" elem="Heading">
                    {__('Products from Collection')}
                </h1>
                <Link block="ProductCollections" elem="ListingButton" to="/product-collections">
                    <span>{__('See all')}</span>
                </Link>
            </ContentWrapper>
        );
    }

    render() {
        const { productCollections, renderType } = this.props;

        if (renderType === RENDER_TYPE) {
            return (
                <div block="ProductCollections" elem="Container">
                    <h1 block="ProductCollections" elem="PageTitle">
                        {__('Products from Collection')}
                    </h1>
                    <ul block="ProductCollections" elem="List" mix={{ block: 'CategoryProductList', elem: 'Page' }}>
                        {productCollections.items.map((product) => (
                            <ProductCard product={product} key={`ProductCollectionsItem-${product.id}`} />
                        ))}
                    </ul>
                </div>
            );
        }
        const productCollectionsId = this.getProductCollectionId();

        if (productCollections.total_count < 1 || !productCollectionsId) {
            return <></>;
        }

        return (
            <div>
                {this.renderHeader()}
                <CarouselWrapper
                    label="Product Collections"
                    mix={{ block: 'ProductCollections' }}
                    wrapperMix={{ block: 'ProductCollections', elem: 'Wrapper' }}
                >
                    <ScrollMenu
                        data={productCollections.items.slice(0, CAROUSEL_PRODUCTS_TO_RENDER).map((item) => (
                            <ProductCard product={item} withoutItemProps key={`ProductCollectionsItem-${item.id}`} />
                        ))}
                        menuClass="CarouselSideImageMenu"
                        itemClass="CarouselSideImage-Item"
                        itemClassActive="CarouselSideImage-Item_active"
                        arrowClass="CarouselSideImage-ArrowWrapper"
                        arrowDisabledClass="CarouselSideImage-ArrowWrapper_hidden"
                        arrowLeft={<div block="CarouselSideImage" elem="Arrow" mods={{ left: true }} />}
                        arrowRight={<div block="CarouselSideImage" elem="Arrow" mods={{ right: true }} />}
                        scrollBy={1}
                        alignCenter={false}
                        wheel={false}
                    />
                </CarouselWrapper>
            </div>
        );
    }
}

import SourceProductReviews from 'SourceComponent/ProductReviews/ProductReviews.component';
import { REVIEW_POPUP_ID } from 'SourceComponent/ProductReviews/ProductReviews.config';

import ProductAddReviewButton from 'Component/ProductAddReviewButton';
import {
    COMFORT_OPTIONS,
    QUALITY_OPTIONS,
    SIZE_OPTIONS,
} from 'Component/ProductReviewForm/ProductReviewForm.component';
import { COMFORT_FIELD_NAME, QUALITY_FIELD_NAME, SIZE_FIELD_NAME } from 'Type/Rating';
import { getPluralForm } from 'Util/i18n/pl_PL';
import { getRating } from 'Util/Product/Review';

import './ProductReviews.style.scss';

export const REVIEW_SCHEMA_OBJECT = {
    itemType: 'http://schema.org/AggregateRating',
    itemProp: 'aggregateRating',
    itemScope: true,
};

export default class ProductReviews extends SourceProductReviews {
    renderNoRating() {
        return (
            <>
                <h4>{__('This product has no reviews yet.')}</h4>
                <p>{__('Be the first to share your opinion.')}</p>
            </>
        );
    }

    renderRatingSchema(percent, reviewCount) {
        return (
            <>
                <meta itemProp="ratingValue" content={percent} />
                <meta itemProp="worstRating" content={0} />
                <meta itemProp="bestRating" content={5} />
                <meta itemProp="reviewCount" content={reviewCount} />
            </>
        );
    }

    renderOverallRating(rating, reviewCount = 0) {
        return (
            <div block="ProductReviews" elem="SummaryRatingWrapper">
                <div block="ProductReviews" elem="SummaryHeading">
                    {__('Overall rating')}
                </div>
                <div block="ProductReviews" elem="SummaryRating">
                    {rating}
                </div>
                <div block="ProductReviews" elem="Count">
                    {getPluralForm(reviewCount, '%s review', '%s REVIEW_PLURAL_1', '%s REVIEW_PLURAL_2')}
                </div>
            </div>
        );
    }

    renderRatingDetails(name, id, options) {
        const {
            product: { review_summary: { extended_rating_summary } = {} },
        } = this.props;

        return (
            <div>
                <div block="ProductReviews" elem="SummaryHeading">
                    {name}
                </div>
                {options.map(({ label, value }) =>
                    this.renderDetailsLabel(label, extended_rating_summary[id][value] || 0),
                )}
            </div>
        );
    }

    renderDetailsLabel(label, value) {
        return (
            <div block="ProductReviews" elem="SummaryLabel" key={label}>
                <span>{label}</span>
                <span>{value}</span>
            </div>
        );
    }

    renderNoReviewsInfo() {
        const {
            product: { review_summary: { review_count } = {}, reviews },
        } = this.props;

        const hasReviews = reviews && Object.keys(reviews).length > 0;

        if (!hasReviews && !review_count) return this.renderNoRating();
    }

    renderSummary() {
        const {
            product: { review_summary: { rating_summary, review_count } = {} },
        } = this.props;

        const rating = getRating(rating_summary);
        const reviewSchemaObject = review_count ? REVIEW_SCHEMA_OBJECT : {};

        if (!review_count) return;

        return (
            <div block="ProductReviews" elem="SummaryWrapper">
                <div block="ProductReviews" elem="Summary" {...reviewSchemaObject}>
                    {this.renderRatingSchema(rating, review_count)}
                    {this.renderOverallRating(rating, review_count)}
                    {this.renderRatingDetails(__('Size'), SIZE_FIELD_NAME, SIZE_OPTIONS)}
                    {this.renderRatingDetails(__('Comfort'), COMFORT_FIELD_NAME, COMFORT_OPTIONS)}
                    {this.renderRatingDetails(__('Quality'), QUALITY_FIELD_NAME, QUALITY_OPTIONS)}
                </div>
            </div>
        );
    }

    renderReviewButton() {
        const { toggleReviewSidebar } = this.props;

        return <ProductAddReviewButton toggleReviewSidebar={toggleReviewSidebar} mods={{ isMobileOnly: true }} />;
    }

    render() {
        const { areDetailsLoaded } = this.props;

        if (!areDetailsLoaded) return null;

        return (
            <div block="ProductReviews">
                {this.renderNoReviewsInfo()}
                {this.renderSummary()}
                {this.renderList()}
                {this.renderReviewButton()}
            </div>
        );
    }
}

export { REVIEW_POPUP_ID };

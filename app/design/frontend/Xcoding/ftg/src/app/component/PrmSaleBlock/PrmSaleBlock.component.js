import React, { PureComponent } from 'react';

import CmsBlock from 'Component/CmsBlock/CmsBlock.container';
import StyledSlickSlider from 'Component/StyledSlickSlider';

import './PrmSaleBlock.style.scss';

const CMS_BLOCKS = [
    'prm-homepage-sale-block-1',
    'prm-homepage-sale-block-2',
    'prm-homepage-sale-block-3',
    'prm-homepage-sale-block-4',
];

export default class PrmSaleBlock extends PureComponent {
    render() {
        return (
            <div block="PrmSaleBlock">
                <div block="PrmSaleBlock" elem="wrapper">
                    <CmsBlock key="prm-homepage-sale-block-title" identifier="prm-homepage-sale-block-title" />
                    <StyledSlickSlider block="PrmSaleBlock-Carousel" arrows={false}>
                        {CMS_BLOCKS.map((key) => (
                            <CmsBlock key={key} identifier={key} />
                        ))}
                    </StyledSlickSlider>
                </div>
            </div>
        );
    }
}

import React from 'react';
import { withRouter } from 'react-router';

import Image from 'Component/Image';
import { ProductGallery as SourceProductGallery } from 'Component/ProductGallery/ProductGallery.component';
import ProductLabels from 'Component/ProductLabels';
import Slider from 'Component/Slider';

export * from 'Component/ProductGallery/ProductGallery.component';

export class ProductCardGalleryMobile extends SourceProductGallery {
    showGallery = () => {
        const { showGallery } = this.props;

        showGallery();
    };

    renderSlider() {
        const { gallery, activeImage, isZoomEnabled, onActiveImageChange } = this.props;

        return (
            <div ref={this.imageRef} onClick={this.showGallery}>
                <Slider
                    ref={this.sliderRef}
                    mix={{ block: 'ProductGallery', elem: 'Slider' }}
                    showCrumbs
                    showArrows
                    activeImage={activeImage}
                    onActiveImageChange={onActiveImageChange}
                    isInteractionDisabled={isZoomEnabled}
                >
                    {gallery.map(this.renderSlide)}
                </Slider>
                {this.renderGalleryNotice()}
            </div>
        );
    }

    renderImage(slide) {
        const { label, small, thumbnail } = slide;
        const src = small?.url || thumbnail?.url;

        return <Image src={src} alt={label} useWebp />;
    }

    renderProductLabels() {
        const { attributes } = this.props;

        return <ProductLabels attributes={attributes} />;
    }

    render() {
        return (
            <div block="ProductGallery">
                {this.renderProductLabels()}
                {this.renderSlider()}
            </div>
        );
    }
}

export default withRouter(ProductCardGalleryMobile);

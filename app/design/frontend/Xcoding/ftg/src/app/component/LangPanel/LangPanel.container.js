import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ConfigQuery from 'Query/Config.query';
import { getGuestCartId } from 'Util/Cart';
import { fetchMutation } from 'Util/Request';
import LangPanel from './LangPanel.component';
import { LANG_PANEL } from 'Component/Header/Header.component';

export const mapStateToProps = (state) => ({
    storeCode: state.ConfigReducer.code,
    storeList: state.ConfigReducer.storeList,
    currencies: state.ConfigReducer.currency,
    activeOverlay: state.OverlayReducer.activeOverlay
});

export class LangPanelContainer extends PureComponent {
    static propTypes = {
        activeOverlay: PropTypes.string,
        storeCode: PropTypes.string,
        storeList: PropTypes.array,
        currencies: PropTypes.object,
    };

    handleChangeCurrency = (currency) => {
        const guestCartId = getGuestCartId();

        return fetchMutation(ConfigQuery.getChangeCurrencyMutation(currency, guestCartId));
    };

    render() {
        const { storeCode, storeList, currencies, activeOverlay  } = this.props;

        return activeOverlay === LANG_PANEL && (
            <LangPanel
                storeCode={storeCode}
                storeList={storeList}
                currencies={currencies}
                handleChangeCurrency={this.handleChangeCurrency}
            />
        );
    }
}

export default connect(mapStateToProps)(LangPanelContainer);

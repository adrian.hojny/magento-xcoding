import React, { memo, useEffect } from 'react';

import Field from 'Component/Field';
import { BLIK_TYPE } from 'Component/Field/Field.config';
import { BuyButton } from '../P24BuyButton/P24BuyButton.component';
import { BLIK_CODE_NAME, BLIK_CODE_NAME_TOC, BLIK_LENGTH } from '../Przelewy24.config';
import { renderP24TocLabel } from '../Przelewy24.utils';

const P24Blik = memo(({ handleBlikChange, isPayBtnDisabled, blikCode, setBlikCode, purgeOtherFormGroups }) => {
    const blikPlaceholder = '000000';

    useEffect(() => {
        handleBlikChange({
            name: BLIK_CODE_NAME,
            value: blikCode,
        });
    }, [blikCode]);

    const handleBlikCodeChange = (code) => {
        if (code.length > BLIK_LENGTH) return;

        setBlikCode(code);
    };

    const handleTOCChange = (_, value) => {
        handleBlikChange({
            name: BLIK_CODE_NAME_TOC,
            value,
        });
    };

    // TODO: https://developers.przelewy24.pl/index.php?pl#section/Jak-umiescic-platnosci-BLIK-na-stronie-Merchanta-(BLIK-level-0)
    return (
        <div block="P24Blik">
            <img
                block="P24Blik"
                elem="logo"
                src="https://static.przelewy24.pl/payment-form-logo/svg/154"
                alt={__('Pay with Blik')}
            />
            <div block="P24Blik" elem="description">
                {__('Enter 6 digits code to bind blik transaction')}.
            </div>
            <Field
                max={999999}
                isControlled
                block="P24Blik"
                elem="input"
                type={BLIK_TYPE}
                id={BLIK_CODE_NAME}
                name={BLIK_CODE_NAME}
                value={blikCode}
                placeholder={blikPlaceholder}
                label={__('Blik code')}
                onChange={handleBlikCodeChange}
                // mix={ { block: 'CartCoupon', elem: 'Input' } }
            />
            <Field
                type="checkbox"
                label={renderP24TocLabel()}
                id={BLIK_CODE_NAME_TOC}
                mix={{ block: 'P24PaymentMethods', elem: 'Checkbox' }}
                name={BLIK_CODE_NAME_TOC}
                onChange={handleTOCChange}
                //    isControlled={ false }
            />

            <BuyButton
                isDisabled={isPayBtnDisabled}
                fieldsGroup={BLIK_CODE_NAME}
                purgeOtherFormGroups={purgeOtherFormGroups}
            />
        </div>
    );
});

export default P24Blik;

import React from 'react';

import SourceProductActions from 'SourceComponent/ProductActions/ProductActions.component';

import AddToCart from 'Component/AddToCart';
import CmsBlock from 'Component/CmsBlock';
import Link from 'Component/Link';
import ProductCardComingSoon from 'Component/ProductCardComingSoon/ProductCardComingSoon.component';
import ProductConfigurableAttributes from 'Component/ProductConfigurableAttributes';
import ProductPrice from 'Component/ProductPrice/ProductPrice.container';
import ProductReviewRating from 'Component/ProductReviewRating';
import { ROUTES } from 'Component/Router/Router.config';
import TextPlaceholder from 'Component/TextPlaceholder/TextPlaceholder.component';
import { getFormattedDeliveryDate } from 'Util/Product';
import { getRating } from 'Util/Product/Review';

export const CLUB_POINTS_INFO = 'club-points-info';
export const PRODUCT_INFO_LIST = 'product-info-list';

export default class ProductActions extends SourceProductActions {
    renderSku() {
        const {
            product,
            product: { variants },
            configurableVariantIndex,
            showOnlyIfLoaded,
        } = this.props;

        const productOrVariant =
            variants && variants[configurableVariantIndex] !== undefined ? variants[configurableVariantIndex] : product;

        const { sku } = productOrVariant;

        return showOnlyIfLoaded(
            sku,
            <div block="ProductActions" elem="Sku" itemProp="sku">
                <meta itemProp="mpn" content={sku} />
                {sku}
            </div>,
        );
    }

    renderReviews() {
        const {
            product: { review_summary: { rating_summary, review_count } = {}, reviews },
            toggleReviewSidebar,
        } = this.props;
        const hasReviews = reviews && Object.keys(reviews).length > 0;

        if (!rating_summary && !hasReviews) {
            return (
                <button onClick={toggleReviewSidebar} block="ProductActions" elem="ReviewButton">
                    {__('Be the first to review')}
                </button>
            );
        }

        return (
            rating_summary && (
                <div block="ProductActions" elem="Reviews">
                    <ProductReviewRating summary={rating_summary || 0} />
                    <p block="ProductActions" elem="ReviewLabel">
                        {getRating(rating_summary, 1)}
                        <span>{`(${review_count})`}</span>
                    </p>
                </div>
            )
        );
    }

    renderFindSize() {
        const { product, toggleOverlayByKey } = this.props;

        const { comming_soon, comming_soon_date, sizing_details } = product;

        if ((comming_soon && +new Date(comming_soon_date) - +new Date() > 0) || !sizing_details) {
            return null;
        }

        return (
            <div block="ProductActions" elem="FindSize">
                <button
                    onClick={() => {
                        toggleOverlayByKey(sizing_details.title, true);
                    }}
                    block="ProductActions"
                    elem="FindSizeButton"
                >
                    {__('Find my size')}
                </button>
            </div>
        );
    }

    renderPriceWithSchema() {
        const {
            product,
            product: { variants },
            configurableVariantIndex,
        } = this.props;

        // Product in props is updated before ConfigurableVariantIndex in props, when page is opened by clicking CartItem
        // As a result, we have new product, but old configurableVariantIndex, which may be out of range for variants
        let productOrVariant =
            variants && variants[configurableVariantIndex] !== undefined ? variants[configurableVariantIndex] : product;

        if (variants?.length && configurableVariantIndex === -1) {
            productOrVariant = variants[0];
        }

        const { price_range, special_to_date } = productOrVariant;

        const allVariantsOutOfStock = variants?.every(({ stock_status }) => stock_status === 'OUT_OF_STOCK');

        if (allVariantsOutOfStock) {
            price_range.minimum_price.regular_price = price_range.minimum_price.final_price;
        }

        return (
            <div>
                {this.renderSchema()}
                <ProductPrice
                    isSchemaRequired
                    price={price_range}
                    mix={{ block: 'ProductActions', elem: 'Price' }}
                    showDiscountInfo
                    specialToDate={special_to_date}
                />
            </div>
        );
    }

    renderSchema() {
        const { productName, stockMeta, metaLink } = this.props;

        return (
            <>
                <meta itemProp="availability" content={stockMeta} />
                <a block="ProductActions" elem="Schema-Url" itemProp="url" href={metaLink}>
                    {productName}
                </a>
            </>
        );
    }

    renderEstimatedDelivery() {
        const {
            productOrVariant: { delivery_date },
            product,
        } = this.props;

        if (!delivery_date || !delivery_date[0]) return null;

        const { comming_soon, comming_soon_date } = product;

        if (comming_soon && +new Date(comming_soon_date) - +new Date() > 0) {
            return null;
        }

        const valuesArr = delivery_date[0].value || [];

        return (
            <div block="ProductActions" elem="EstimatedDelivery">
                {__('Order now')}
                <strong>{` ${__('at your place')} ${getFormattedDeliveryDate(valuesArr)}`}</strong>
            </div>
        );
    }

    renderClubPoints() {
        const {
            productOrVariant: { clubPoints },
        } = this.props;

        if (!clubPoints) return null;

        const formattedClubPoints = clubPoints.toFixed(2).replace('.', ',');

        return (
            <div block="ProductActions" elem="ClubPoints">
                {`+${formattedClubPoints} pkt z SneakerClub`}
                <span block="ProductActions" elem="ClubPointsButton" />
                <span block="ProductActions" elem="ClubPointsInfo">
                    <CmsBlock identifier={CLUB_POINTS_INFO} withoutWrapper />
                </span>
            </div>
        );
    }

    renderBrand(brand_details) {
        const { storeViewCode } = this.props;
        const { title, url_alias } = brand_details;
        const brandUrl = `${url_alias}`.toLowerCase();

        return (
            <h4 block="ProductActions" elem="Brand" itemProp="brand">
                <Link block="ProductActions" elem="brandLink" to={`${ROUTES.brand[storeViewCode]}/${brandUrl}`}>
                    {title || url_alias}
                </Link>
            </h4>
        );
    }

    renderAddToCart() {
        const {
            configurableVariantIndex,
            product,
            quantity,
            groupedProductQuantity,
            productOptionsData,
            parameters,
        } = this.props;

        const { comming_soon, comming_soon_date } = product;

        if (comming_soon && +new Date(comming_soon_date) - +new Date() > 0) {
            return <ProductCardComingSoon product={product} />;
        }

        return (
            <AddToCart
                product={product}
                configurableVariantIndex={configurableVariantIndex}
                mix={{ block: 'ProductActions', elem: 'AddToCart' }}
                quantity={quantity}
                groupedProductQuantity={groupedProductQuantity}
                onProductValidationError={this.onProductValidationError}
                productOptionsData={productOptionsData}
                variant={parameters?.shoes_size}
            />
        );
    }

    renderNameAndBrand() {
        const {
            product: { name, brand_details = { title: '' } },
        } = this.props;

        return (
            <section block="ProductActions" elem="Section" mods={{ type: 'name' }}>
                {brand_details && this.renderBrand(brand_details)}
                <h1 block="ProductActions" elem="Title" itemProp="name">
                    <TextPlaceholder content={name} length="medium" />
                </h1>
            </section>
        );
    }

    renderInfoList() {
        return (
            <div block="ProductActions" elem="InfoList">
                <CmsBlock identifier={PRODUCT_INFO_LIST} withoutWrapper />
            </div>
        );
    }

    renderConfigurableAttributes() {
        const {
            getLink,
            updateConfigurableVariant,
            parameters,
            areDetailsLoaded,
            product,
            product: { configurable_options, type_id, sizing_details },
            getProductAvailability,
            toggleOverlayByKey,
        } = this.props;

        if (type_id !== 'configurable') return null;

        const { comming_soon, comming_soon_date } = product;

        if (comming_soon && +new Date(comming_soon_date) - +new Date() > 0) {
            return null;
        }

        return (
            <ProductConfigurableAttributes
                /* eslint-disable-next-line no-magic-numbers */
                numberOfPlaceholders={[2, 4]}
                mix={{ block: 'ProductActions', elem: 'Attributes' }}
                isReady={areDetailsLoaded}
                getLink={getLink}
                parameters={parameters}
                updateConfigurableVariant={updateConfigurableVariant}
                configurable_options={configurable_options}
                getProductAvailability={getProductAvailability}
                toggleOverlayByKey={toggleOverlayByKey}
                sizingDetails={sizing_details}
            />
        );
    }

    render() {
        return (
            <>
                <article block="ProductActions">
                    <div block="ProductActions" elem="SkuAndRating">
                        {this.renderSku()}
                        {this.renderReviews()}
                    </div>
                    {this.renderNameAndBrand()}
                    {this.renderPriceWithGlobalSchema()}
                    {this.renderFindSize()}
                    {this.renderConfigurableAttributes()}
                    {this.renderAddToCart()}
                    {this.renderEstimatedDelivery()}
                    {this.renderClubPoints()}
                    {this.renderInfoList()}
                    {this.renderGroupedItems()}
                    {/* disabled { this.renderTierPrices() } */}
                </article>
            </>
        );
    }
}

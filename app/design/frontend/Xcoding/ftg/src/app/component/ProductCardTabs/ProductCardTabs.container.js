import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ProductCardTabs from 'Component/ProductCardTabs/ProductCardTabs.component';

class ProductCardTabsContainer extends PureComponent {
    static propTypes = {
        defaultTab: PropTypes.string,
    };

    static defaultProps = {
        defaultTab: '',
    };

    containerFunctions = {
        setActiveTab: this.setActiveTab.bind(this),
    };

    constructor(props) {
        super(props);

        const { defaultTab } = props;

        this.state = {
            activeTab: defaultTab,
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.defaultTab !== this.props.defaultTab) {
            this.setState({
                activeTab: this.props.defaultTab,
            });
        }
    }

    setActiveTab(activeTab) {
        this.setState({ activeTab });
    }

    render() {
        return <ProductCardTabs {...this.props} {...this.state} {...this.containerFunctions} />;
    }
}

export default ProductCardTabsContainer;

import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { ChildrenType, MixType } from 'Type/Common';

import './CarouselWrapper.style';

/**
 * Carousel Wrapper
 * @class CarouselWrapper
 */
export default class CarouselWrapper extends PureComponent {
    static propTypes = {
        children: ChildrenType,
        mix: MixType,
        wrapperMix: PropTypes.shape({
            block: PropTypes.string,
            elem: PropTypes.string,
        }),
        label: PropTypes.string.isRequired,
    };

    static defaultProps = {
        mix: {},
        wrapperMix: {},
        children: null,
    };

    render() {
        const { children, mix, wrapperMix, label } = this.props;

        return (
            <section mix={mix} aria-label={label}>
                <div block="CarouselWrapper" mix={wrapperMix}>
                    {children}
                </div>
            </section>
        );
    }
}

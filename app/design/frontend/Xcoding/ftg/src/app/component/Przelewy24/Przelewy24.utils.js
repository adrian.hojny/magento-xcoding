import { Field as QueryField } from 'Util/Query';
import { fetchQuery } from 'Util/Request';
import { BLIK_CODE_NAME, BLIK_LENGTH } from './Przelewy24.config';

export const getP24PayMethods = async () => {
    const { przelewy24_methods } = await fetchQuery(
        new QueryField('przelewy24_methods')
            .addArgument('lang', 'String', 'pl')
            .addFieldList(['name', 'id', 'status', 'imgUrl', 'mobileImgUrl', 'mobile']),
    );

    return przelewy24_methods;
};

const TOC_SUFFIX = 'TOC';

export const getBaseFieldsGroupName = (propsName) => {
    const isNameATOC = propsName.endsWith(TOC_SUFFIX);
    return isNameATOC ? propsName.slice(0, -4) : propsName;
};

export const getCorrespondingTOCName = (name) => `${name}_${TOC_SUFFIX}`;

export const checkIfGroupIsValid = (propsName, p24formValues) => {
    const name = getBaseFieldsGroupName(propsName);

    const isBlik = name === BLIK_CODE_NAME;
    const inputValue = p24formValues[name];
    const TOCName = `${name}_TOC`;

    const blikValidator = (blikCode) => blikCode?.length >= BLIK_LENGTH;
    const paymentMethodsValidator = (paymentMethod) => paymentMethod >= 1;

    const isValidInputValue = isBlik ? blikValidator(inputValue) : paymentMethodsValidator(inputValue);

    const isTOCselected = p24formValues[TOCName];

    return isValidInputValue && isTOCselected;
};

export const renderP24TocLabel = () => (
    <span
        className="P24PaymentTOC"
        dangerouslySetInnerHTML={{
            __html: `
            ${__(
                'I hereby state that I have read the %s regulations %s and %s information obligation%s of Przelewy24',
                '<a href="https://www.przelewy24.pl/regulamin" target="_blank">',
                '</a>',
                '<a href="https://www.przelewy24.pl/obowiazekinformacyjny" target="_blank">',
                '</a>',
            )}
        `,
        }}
    />
);

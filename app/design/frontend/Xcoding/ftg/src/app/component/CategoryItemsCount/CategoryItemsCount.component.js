import React from 'react';

import SourceCategoryItemsCount from 'SourceComponent/CategoryItemsCount/CategoryItemsCount.component';

import TextPlaceholder from 'Component/TextPlaceholder';

export * from 'SourceComponent/CategoryItemsCount/CategoryItemsCount.component';

class CategoryItemsCount extends SourceCategoryItemsCount {
    render() {
        const { totalItems, isMatchingListFilter } = this.props;
        return (
            <p block="CategoryPage" elem="ItemsCount">
                <TextPlaceholder
                    content={!isMatchingListFilter ? __('Products are loading...') : __('%s products', totalItems)}
                />
            </p>
        );
    }
}

export default CategoryItemsCount;

import React from 'react';
import PropTypes from 'prop-types';

import SourceCartItemPrice from 'SourceComponent/CartItemPrice/CartItemPrice.component';

import { MixType } from 'Type/Common';
import { roundPrice } from 'Util/Price';

export * from 'SourceComponent/CartItemPrice/CartItemPrice.component';

class CartItemPrice extends SourceCartItemPrice {
    static propTypes = {
        row_total: PropTypes.number.isRequired,
        regular_price: PropTypes.number.isRequired,
        currency_code: PropTypes.string.isRequired,
        mix: MixType.isRequired,
    };

    calculateDiscount(specialPrice, regularPrice) {
        return (100 - (specialPrice * 100) / regularPrice).toFixed(2);
    }

    render() {
        const { currency_code, mix, regular_price, minimal_price, discountPercent } = this.props;

        const price = roundPrice(minimal_price);
        const regularPrice = roundPrice(regular_price);
        if (price === regularPrice) {
            return (
                <p block="ProductPrice" aria-label={__('Product Price')} mix={mix}>
                    <span aria-label={__('Current product price')}>
                        <data value={price}>{`${price} ${currency_code}`}</data>
                    </span>
                </p>
            );
        }

        return (
            <p block="ProductPrice" aria-label={__('Product Price')} mix={mix}>
                <span block="ProductPrice" aria-label={__('Current product price')}>
                    <data block="ProductPrice" elem="SpecialPrice" value={price}>
                        {`${price} ${currency_code}`}
                    </data>
                    <span block="ProductPrice" elem="RegularPrice">
                        {`${regularPrice} ${currency_code}`}
                    </span>
                    <span block="ProductPrice" elem="Discount">
                        {`-${Math.round(discountPercent)}%`}
                    </span>
                </span>
            </p>
        );
    }
}

export default CartItemPrice;

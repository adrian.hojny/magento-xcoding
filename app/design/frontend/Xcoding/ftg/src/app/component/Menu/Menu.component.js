import { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import BrandMenu from 'Component/BrandMenu';
import CmsBlock from 'Component/CmsBlock';
import Link from 'Component/Link';
import MenuItem from 'Component/MenuItem';
import StoreSwitcher from 'Component/StoreSwitcher';
import { MenuType } from 'Type/Menu';
import { getSortedItems } from 'Util/Menu';
import isMobile from 'Util/Mobile';

import './Menu.style';

const MEGAMENU_BRANDS = 'megamenu-brands';
const MENU_BRANDS_ITEM_CLASS = 'marki';
const HIDE_ON_DESKTOP = 'hide_on_desktop';

export class Menu extends PureComponent {
    static propTypes = {
        menu: MenuType.isRequired,
        activeMenuItemsStack: PropTypes.array.isRequired,
        handleSubcategoryClick: PropTypes.func.isRequired,
        closeMenu: PropTypes.func.isRequired,
        onCategoryHover: PropTypes.func.isRequired,
    };

    renderDesktopSubLevelItems(item, mods) {
        const { item_id, item_class } = item;
        const { closeMenu, activeMenuItemsStack } = this.props;

        const hideOnDesktop = item_class.includes(HIDE_ON_DESKTOP);

        if (hideOnDesktop) return null;

        return (
            <MenuItem
                activeMenuItemsStack={activeMenuItemsStack}
                item={item}
                itemMods={mods}
                closeMenu={closeMenu}
                isLink
                key={`${item_id}_desktop_sub_level`}
            />
        );
    }

    renderDesktopSubLevel(category) {
        const { children, item_class, item_id } = category;
        const childrenArray = getSortedItems(Object.values(children));

        if (isMobile.any() || !childrenArray.length) {
            return null;
        }

        const isBanner = item_class.includes('Menu-ItemFigure_type_banner');
        const isLogo = item_class.includes('Menu-ItemFigure_type_logo');
        const mods = {
            isBanner: !!isBanner,
            isLogo: !!isLogo,
        };

        return (
            <div block="Menu" elem="SubLevelDesktop" key={`${item_id}_sub_lvl_desktop`}>
                <div block="Menu" elem="ItemList" mods={{ ...mods }}>
                    {childrenArray.map((item) => this.renderDesktopSubLevelItems(item, mods))}
                </div>
            </div>
        );
    }

    renderSubLevelItems = (item) => {
        const { handleSubcategoryClick, activeMenuItemsStack, onCategoryHover, closeMenu } = this.props;

        const { item_id, children, item_class } = item;

        const isBanner = item_class.includes('Menu-ItemFigure_type_banner');
        const hideOnDesktop = item_class.includes(HIDE_ON_DESKTOP);
        const childrenArray = Object.values(children);
        const subcategoryMods = { type: 'subcategory' };

        if (hideOnDesktop) return null;

        if (childrenArray.length && isMobile.any()) {
            return (
                <div
                    key={`${item_id}_sub_level`}
                    block="Menu"
                    elem="main-link"
                    // TODO: split into smaller components
                    // eslint-disable-next-line react/jsx-no-bind
                    onClick={(e) => handleSubcategoryClick(e, item)}
                    tabIndex="0"
                    role="button"
                >
                    <MenuItem
                        activeMenuItemsStack={activeMenuItemsStack}
                        item={item}
                        itemMods={subcategoryMods}
                        onCategoryHover={onCategoryHover}
                        closeMenu={closeMenu}
                    />
                    {this.renderSubLevel(item)}
                </div>
            );
        }

        return (
            <div block="Menu" elem="SubItemWrapper" key={`${item_id}_sub_level`} mods={{ isBanner }}>
                <MenuItem activeMenuItemsStack={activeMenuItemsStack} item={item} closeMenu={closeMenu} isLink />
                {this.renderDesktopSubLevel(item)}
            </div>
        );
    };

    renderSubLevel(category) {
        const { activeMenuItemsStack } = this.props;
        const { item_id, children } = category;
        const childrenArray = getSortedItems(Object.values(children));
        const isVisible = activeMenuItemsStack.includes(item_id);
        const subcategoryMods = { type: 'subcategory' };

        return (
            <div block="Menu" elem="SubMenu" mods={{ isVisible }} key={`${item_id}_sub_lvl`}>
                <div block="Menu" elem="ItemList" mods={{ ...subcategoryMods }}>
                    {childrenArray.map(this.renderSubLevelItems)}
                </div>
            </div>
        );
    }

    renderPromotionCms() {
        const { closeMenu } = this.props;
        const { header_content: { header_cms } = {} } = window.contentConfiguration;

        if (header_cms) {
            return <CmsBlock identifier={header_cms} />;
        }

        return (
            <div block="Menu" elem="Promotion">
                <h3 block="Menu" elem="PageLink">
                    <Link to="/about-us" onClick={closeMenu} block="Menu" elem="Link">
                        {__('ABOUT US')}
                    </Link>
                </h3>
                <h3 block="Menu" elem="PageLink">
                    <Link to="/about-us" onClick={closeMenu} block="Menu" elem="Link">
                        {__('CONTACTS')}
                    </Link>
                </h3>
                <div block="Menu" elem="Social">
                    <CmsBlock identifier="social-links" />
                </div>
            </div>
        );
    }

    renderBrandsMenu(item) {
        const { title, url } = item;

        return (
            <div block="MegaMenu" elem="InnerWrapper">
                <div block="MegaMenu" elem="Content">
                    <div block="MegaMenu" elem="Categories">
                        <CmsBlock identifier="brand-menu-left" />
                        <div block="MegaMenu" elem="Featured-Brand">
                            <CmsBlock identifier="brand-menu-left-featured-brand" />
                        </div>
                    </div>
                    <div block="MegaMenu" elem="Brands">
                        <BrandMenu title={title} url={url} />
                    </div>
                </div>
            </div>
        );
    }

    renderSubMenuDesktopItems = (item) => {
        const { item_id, children, item_class } = item;
        const { activeMenuItemsStack, closeMenu } = this.props;
        const isVisible = activeMenuItemsStack.includes(item_id);
        const isMenuBrands = item_class.includes(MENU_BRANDS_ITEM_CLASS);

        if (!isVisible) {
            return null;
        }

        if (isMenuBrands) {
            return this.renderBrandsMenu(item);
        }

        if (!Object.keys(children).length) {
            return null;
        }

        return (
            <Fragment key={`${item_id}_sub_menu`}>
                <div block="MegaMenu" elem="InnerWrapper">
                    <div block="MegaMenu" elem="Content">
                        <div block="MegaMenu" elem="Categories">
                            <div block="MegaMenu" elem="SubCategoriesWrapper" mods={{ isVisible }}>
                                <div block="Menu" elem="SubCategoriesWrapperInner" mods={{ isVisible }}>
                                    <div block="Menu" elem="SubCategories">
                                        {this.renderSubLevel(item)}
                                    </div>
                                    {this.renderAdditionalInformation()}
                                </div>
                                <div block="Menu" elem="Overlay" mods={{ isVisible }} onMouseEnter={closeMenu} />
                            </div>
                        </div>
                        <div block="MegaMenu" elem="Brands" onClick={closeMenu}>
                            <CmsBlock identifier={`${MEGAMENU_BRANDS}-${item_id}`} />
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    };

    renderSubMenuDesktop(itemList) {
        if (isMobile.any() || isMobile.tablet()) {
            return null;
        }
        const childrenArray = getSortedItems(Object.values(itemList));
        return <>{childrenArray.map(this.renderSubMenuDesktopItems)}</>;
    }

    renderAdditionalInformation(checkMobile = false) {
        if (checkMobile && !isMobile.any()) {
            return null;
        }

        return (
            <>
                {this.renderStoreSwitcher()}
                {/* { this.renderPromotionCms() } */}
            </>
        );
    }

    renderFirstLevelItems(item) {
        const { activeMenuItemsStack, handleSubcategoryClick, onCategoryHover, closeMenu } = this.props;

        const { children } = item;
        const childrenArray = Object.values(children);
        const itemMods = { type: 'main' };

        if (childrenArray.length && (isMobile.any() || isMobile.tablet())) {
            return (
                <div
                    // TODO: split into smaller components
                    // eslint-disable-next-line react/jsx-no-bind
                    onClick={(e) => handleSubcategoryClick(e, item)}
                    tabIndex="0"
                    block="Menu"
                    elem="SubCatLink"
                    role="button"
                >
                    <MenuItem
                        activeMenuItemsStack={activeMenuItemsStack}
                        item={item}
                        itemMods={itemMods}
                        onCategoryHover={onCategoryHover}
                        closeMenu={closeMenu}
                    />
                    {this.renderSubLevel(item)}
                </div>
            );
        }

        return (
            <MenuItem
                activeMenuItemsStack={activeMenuItemsStack}
                item={item}
                itemMods={itemMods}
                onCategoryHover={onCategoryHover}
                closeMenu={closeMenu}
                isLink
            />
        );
    }

    renderFirstLevel = (item) => {
        const { item_id } = item;
        return (
            <li key={`${item_id}_first_level`} block="Menu" elem="CategoryItem">
                {this.renderFirstLevelItems(item)}
            </li>
        );
    };

    renderTopLevel() {
        const { menu } = this.props;
        const categoryArray = Object.values(menu);

        if (!categoryArray.length) {
            return null;
        }

        const [{ children, title: mainCategoriesTitle }] = categoryArray;
        const childrenArray = getSortedItems(Object.values(children));
        return (
            <>
                <div block="Menu" elem="MainCategories">
                    <ul block="Menu" elem="ItemList" mods={{ type: 'main' }} aria-label={mainCategoriesTitle}>
                        {childrenArray.map(this.renderFirstLevel)}
                    </ul>
                    {this.renderAdditionalInformation(true)}
                </div>
                {this.renderSubMenuDesktop(children)}
            </>
        );
    }

    renderStoreSwitcher() {
        if (!isMobile.any() && !isMobile.tablet()) {
            return null;
        }

        return <StoreSwitcher />;
    }

    render() {
        const { closeMenu } = this.props;

        return (
            <div block="Menu" elem="MenuWrapper" onMouseLeave={closeMenu}>
                {this.renderTopLevel()}
            </div>
        );
    }
}

export default Menu;

import SourceSearchOverlay from 'SourceComponent/SearchOverlay/SearchOverlay.component';

import './SearchOverlay.style';

export default class SearchOverlay extends SourceSearchOverlay {
    static defaultProps = {
        searchCriteria: '',
    };

    renderNoResults() {
        return <p>{__('Don’t give up! Check the spelling, or try something more specific.')}</p>;
    }
}

import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Image from 'Component/Image';

export default class ProductCardGalleryImage extends PureComponent {
    static propTypes = {
        index: PropTypes.number.isRequired,
        onActiveImageChange: PropTypes.func.isRequired,
        showGallery: PropTypes.func.isRequired,
        updateActiveImageOverlayGallery: PropTypes.func.isRequired,
        media: PropTypes.shape({
            label: PropTypes.string,
            file: PropTypes.string,
            small: PropTypes.shape({
                url: PropTypes.string,
            }),
        }).isRequired,
    };

    onActiveImageChange = () => {
        const { index, onActiveImageChange } = this.props;
        onActiveImageChange(index);
    };

    openGallery = () => {
        const { index, showGallery, updateActiveImageOverlayGallery } = this.props;

        updateActiveImageOverlayGallery(index);
        showGallery();
    };

    renderImage() {
        const {
            media: { label: alt, small: { url: baseUrl } = {} },
        } = this.props;

        return (
            <div onClick={this.openGallery} aria-hidden="true">
                <Image alt={alt} src={baseUrl} itemProp="image" useWebp />
            </div>
        );
    }

    render() {
        return this.renderImage();
    }
}

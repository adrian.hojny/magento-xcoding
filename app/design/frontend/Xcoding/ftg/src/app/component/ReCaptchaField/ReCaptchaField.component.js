import React, { PureComponent } from 'react';

import Loader from 'Component/Loader';
import { Field } from 'Util/Query';
import { fetchQuery } from 'Util/Request';

import './ReCaptchaField.style.scss';

export default class ReCaptchaField extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            storeConfig: {},
        };
    }

    componentDidMount() {
        this.getConfig().then((storeConfig) => {
            this.setState({ storeConfig });
        });
    }

    async getConfig() {
        const { storeConfig } = await fetchQuery(
            new Field('storeConfig').addFieldList([
                'recaptcha_public_key',
                'recaptcha_enabled',
                'recaptcha_enabled_create_user',
            ]),
        );

        return storeConfig;
    }

    getCaptchaScript() {
        const script = document.querySelector(
            'script[src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"]',
        );

        if (script) {
            return Promise.resolve(script);
        }

        return new Promise((resolve, reject) => {
            const script = document.createElement('script');
            script.src = 'https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit';
            window.onloadCallback = resolve;
            script.onerror = reject;
            document.head.appendChild(script);
        });
    }

    render() {
        const { storeConfig } = this.state;
        if (!Object.keys(storeConfig)) {
            return null;
        }
        const { recaptcha_enabled, recaptcha_public_key } = storeConfig;
        if (!recaptcha_enabled) {
            return null;
        }

        return (
            <div block="ReCaptchaField" elem="wrapper">
                <div
                    block="ReCaptchaField"
                    elem="checkbox"
                    ref={(ele) => {
                        if (ele !== null && recaptcha_public_key) {
                            const renderRecaptcha = async () => {
                                try {
                                    await this.getCaptchaScript();
                                    window.grecaptcha.render(ele, {
                                        sitekey: recaptcha_public_key,
                                    });
                                } catch (error) {
                                    setTimeout(renderRecaptcha, 200);
                                }
                            };

                            renderRecaptcha();
                        }
                    }}
                />
                <div block="ReCaptchaField" elem="loader">
                    <Loader isLoading />
                </div>
            </div>
        );
    }
}

import PropTypes from 'prop-types';

import { FieldSelect as SourceFieldSelect } from 'SourceComponent/FieldSelect/FieldSelect.component';

import { getStaticFilePath } from 'Util/Image/Image';

import './FieldSelect.style';

export class FieldSelect extends SourceFieldSelect {
    static propTypes = {
        ...this.propTypes,
        selectOptions: PropTypes.arrayOf(
            PropTypes.shape({
                id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
                value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
                disabled: PropTypes.bool,
                label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
                prefix: PropTypes.oneOfType([PropTypes.element, PropTypes.string, PropTypes.object]),
            }),
        ).isRequired,
    };

    renderOption = (option) => {
        const { id, label, prefix } = option;

        const { isSelectExpanded: isExpanded, handleSelectListOptionClick } = this.props;

        return (
            <li
                block="FieldSelect"
                elem="Option"
                mods={{ isExpanded }}
                key={id}
                /**
                 * Added 'o' as querySelector does not work with
                 * ids, that consist of numbers only
                 */
                id={`o${id}`}
                role="menuitem"
                // eslint-disable-next-line react/jsx-no-bind
                onClick={() => handleSelectListOptionClick(option)}
                // eslint-disable-next-line react/jsx-no-bind
                onKeyPress={() => handleSelectListOptionClick(option)}
                tabIndex={isExpanded ? '0' : '-1'}
            >
                {prefix && (
                    <img
                        block="LangPanel"
                        elem="Flag"
                        src={getStaticFilePath(`/assets/images/flags/${prefix}.png`)}
                        alt={prefix}
                    />
                )}{' '}
                {label}
            </li>
        );
    };
}

export default FieldSelect;

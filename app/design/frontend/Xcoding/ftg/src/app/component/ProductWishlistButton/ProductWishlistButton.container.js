import { connect } from 'react-redux';

import {
    mapDispatchToProps,
    mapStateToProps,
    ProductWishlistButtonContainer as SourceProductWishlistButtonContainer,
} from 'SourceComponent/ProductWishlistButton/ProductWishlistButton.container';

import ProductWishlistButton from './ProductWishlistButton.component';

class ProductWishlistButtonContainer extends SourceProductWishlistButtonContainer {
    render() {
        return <ProductWishlistButton {...this.props} {...this.containerProps()} {...this.containerFunctions()} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductWishlistButtonContainer);

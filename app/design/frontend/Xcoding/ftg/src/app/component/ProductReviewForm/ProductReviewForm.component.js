import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { CHECKBOX_TYPE, RADIO_TYPE, TEXTAREA_TYPE } from 'Component/Field/Field.config';
import Field from 'Component/Field/Field.container';
import Form from 'Component/Form';
import Image from 'Component/Image/Image.container';
import Loader from 'Component/Loader';
import ReviewStar from 'Component/ReviewStar';
import Sidebar from 'Component/Sidebar/Sidebar.container';
import { PRODUCT_REVIEW_OVERLAY_KEY } from 'Route/ProductPage/ProductPage.component';
import {
    COMFORTABLE,
    NOT_COMFORTABLE,
    NOT_RECOMMENDED,
    QUALITY_HIGH,
    QUALITY_LOW,
    QUALITY_OK,
    RatingItemsType,
    RECOMMENDED,
    SIZE_OK,
    SIZE_TOO_LARGE,
    SIZE_TOO_SMALL,
    VERY_COMFORTABLE,
} from 'Type/Rating';

import './ProductReviewForm.style';

export const COMFORT_OPTIONS = [
    { label: __('NOT_COMFORTABLE'), value: NOT_COMFORTABLE },
    { label: __('COMFORTABLE'), value: COMFORTABLE },
    { label: __('VERY_COMFORTABLE'), value: VERY_COMFORTABLE },
];

export const SIZE_OPTIONS = [
    { label: __('SIZE_TOO_SMALL'), value: SIZE_TOO_SMALL },
    { label: __('SIZE_OK'), value: SIZE_OK },
    { label: __('SIZE_TOO_LARGE'), value: SIZE_TOO_LARGE },
];

export const QUALITY_OPTIONS = [
    { label: __('QUALITY_LOW'), value: QUALITY_LOW },
    { label: __('QUALITY_OK'), value: QUALITY_OK },
    { label: __('QUALITY_HIGH'), value: QUALITY_HIGH },
];

class ProductReviewForm extends PureComponent {
    static propTypes = {
        handleComfortChange: PropTypes.func.isRequired,
        handleDetailChange: PropTypes.func.isRequired,
        handleQualityChange: PropTypes.func.isRequired,
        handleRecommendationChange: PropTypes.func.isRequired,
        handleSizeChange: PropTypes.func.isRequired,
        isLoading: PropTypes.bool.isRequired,
        onReviewError: PropTypes.func.isRequired,
        onReviewSubmitAttempt: PropTypes.func.isRequired,
        onReviewSubmitSuccess: PropTypes.func.isRequired,
        onStarRatingClick: PropTypes.func.isRequired,
        product: PropTypes.object.isRequired,
        ratingData: PropTypes.objectOf(PropTypes.number).isRequired,
        reviewData: PropTypes.object.isRequired,
        reviewRatings: RatingItemsType.isRequired,
    };

    ratingTitleMap = {
        1: __('Awful'),
        2: __('Bad'),
        3: __('Average'),
        4: __('Good'),
        5: __('Awesome'),
    };

    handleOptionClick = (onClick) => (event) => {
        onClick(event.target.value);
    };

    handleRecommendationCheck = (_, isChecked) => {
        const { handleRecommendationChange } = this.props;

        handleRecommendationChange(isChecked ? RECOMMENDED : NOT_RECOMMENDED);
    };

    renderRadioButtons() {
        const { handleSizeChange, handleComfortChange, handleQualityChange } = this.props;

        return (
            <div block="ProductReviewForm" elem="ReviewDetails">
                <div block="ProductReviewForm" elem="ReviewDetailsSection">
                    <h4>{__('Size')}</h4>
                    {this.renderOptions(SIZE_OPTIONS, handleSizeChange, 'size')}
                </div>
                <div block="ProductReviewForm" elem="ReviewDetailsSection">
                    <h4>{__('Comfort')}</h4>
                    {this.renderOptions(COMFORT_OPTIONS, handleComfortChange, 'comfort')}
                </div>
                <div block="ProductReviewForm" elem="ReviewDetailsSection">
                    <h4>{__('Quality')}</h4>
                    {this.renderOptions(QUALITY_OPTIONS, handleQualityChange, 'quality')}
                </div>
            </div>
        );
    }

    renderOptions(options = [], onClick, name) {
        const { reviewData } = this.props;

        return options.map(({ value, label }) => (
            <Field
                type={RADIO_TYPE}
                id={value}
                name={value}
                key={value}
                label={label}
                onClick={this.handleOptionClick(onClick)}
                value={value}
                checked={reviewData[name] === value}
            />
        ));
    }

    renderRemarks() {
        const { handleDetailChange } = this.props;

        return (
            <Field
                id="detail"
                name="detail"
                type={TEXTAREA_TYPE}
                rows={3}
                onChange={handleDetailChange}
                placeholder={__('RECOMMENDATION_PLACEHOLDER')}
                validation={['notEmpty']}
            />
        );
    }

    renderProductInfo() {
        const {
            product: { small_image = {}, sku, name },
        } = this.props;

        return (
            <div block="ProductReviewForm" elem="Product">
                <Image src={small_image.url} alt={name} ratio="square" width="150px" height="150px" />
                <div block="ProductReviewForm" elem="ProductDetails">
                    <div block="ProductReviewForm" elem="Sku">
                        {sku}
                    </div>
                    <div block="ProductReviewForm" elem="Name">
                        {name}
                    </div>
                    {this.renderReviewRating()}
                </div>
            </div>
        );
    }

    renderRecommendationCheckbox() {
        const { reviewData } = this.props;

        return (
            <Field
                type={CHECKBOX_TYPE}
                id="isRecommended"
                name="isRecommended"
                value
                label={__('RECOMMENDATION_CHECKBOX')}
                onChange={this.handleRecommendationCheck}
                checked={reviewData.isRecommended === RECOMMENDED}
                mix={{ block: 'ProductReviewForm', elem: 'RecommendationCheckbox' }}
            />
        );
    }

    renderReviewRating() {
        const { reviewRatings = [] } = this.props;

        const rating = reviewRatings[0];

        if (!rating) return null;

        const { rating_id, rating_options } = rating;

        return (
            <fieldset block="ProductReviewForm" elem="Rating" key={rating_id}>
                <legend block="ProductReviewForm" elem="Legend">
                    {__('My overall rating')}
                </legend>
                {rating_options
                    .sort(({ value }, { value: nextValue }) => nextValue - value)
                    .map((option) => this.renderReviewStar(option, rating_id))}
            </fieldset>
        );
    }

    renderReviewStar(options, rating_id) {
        const { ratingData, onStarRatingClick } = this.props;
        const { option_id, value } = options;
        const isChecked = !!ratingData[rating_id] && ratingData[rating_id] === option_id;

        return (
            <ReviewStar
                key={option_id}
                name={rating_id}
                value={value}
                title={this.ratingTitleMap[value]}
                isChecked={isChecked}
                option_id={option_id}
                rating_id={rating_id}
                onStarRatingClick={onStarRatingClick}
            />
        );
    }

    renderSubmitButton() {
        return (
            <button block="Button" mods={{ green: true, fullWidth: true }} type="submit">
                {__('Submit')}
            </button>
        );
    }

    render() {
        const { onReviewSubmitAttempt, onReviewSubmitSuccess, onReviewError, isLoading } = this.props;

        return (
            <Sidebar
                id={PRODUCT_REVIEW_OVERLAY_KEY}
                title={__('Add review')}
                subtitle={__('What do you think about this product?')}
                mix={{ block: 'ProductReviewForm' }}
            >
                <Form
                    key="product-review"
                    mix={{ block: 'ProductReviewForm' }}
                    onSubmit={onReviewSubmitAttempt}
                    onSubmitSuccess={onReviewSubmitSuccess}
                    onSubmitError={onReviewError}
                >
                    <Loader isLoading={isLoading} />
                    {this.renderProductInfo()}
                    {this.renderRadioButtons()}
                    {this.renderRemarks()}
                    {this.renderRecommendationCheckbox()}
                    {this.renderSubmitButton()}
                </Form>
            </Sidebar>
        );
    }
}

export default ProductReviewForm;

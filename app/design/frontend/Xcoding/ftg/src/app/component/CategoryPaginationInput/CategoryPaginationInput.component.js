import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './CategoryPaginationInput.style.scss';

class CategoryPaginationInput extends PureComponent {
    static propTypes = {
        currentPage: PropTypes.number,
        totalPages: PropTypes.number,
        getSearchQuery: PropTypes.func.isRequired,
        pathname: PropTypes.string.isRequired,
        history: PropTypes.object.isRequired,
        onPageSelect: PropTypes.func.isRequired,
    };

    static defaultProps = {
        currentPage: 1,
        totalPages: 1,
    };

    constructor(props) {
        super(props);
        this.state = { value: props.currentPage };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleEnter = this.handleEnter.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        const { value } = this.state;
        const { totalPages, history, getSearchQuery, pathname, currentPage, onPageSelect } = this.props;

        const newPage = parseInt(value, 10);

        if (newPage > 0 && newPage <= totalPages && newPage !== currentPage) {
            history.push({ pathname, search: getSearchQuery(newPage) });
            onPageSelect(newPage);
        } else {
            this.setState({ value: currentPage });
        }

        event.preventDefault();
    }

    handleEnter(event) {
        if (event.key === 'Enter') this.handleSubmit(event);
    }

    render() {
        const { value } = this.state;

        return (
            <input
                type="text"
                inputMode="numeric"
                pattern="[0-9]*"
                block="CategoryPagination"
                elem="Input"
                maxLength={2}
                value={value}
                onChange={this.handleChange}
                onKeyDown={this.handleEnter}
                onBlur={this.handleSubmit}
            />
        );
    }
}

export default CategoryPaginationInput;

import SourceField from 'SourceComponent/Field/Field.component';

import FieldInput from 'Component/FieldInput/FieldInput.container';
import FieldTextarea from 'Component/FieldTextarea';
import {
    BLIK_TYPE,
    CHECKBOX_TYPE,
    NUMBER_TYPE,
    PASSWORD_TYPE,
    RADIO_TYPE,
    SELECT_TYPE,
    TEXT_TYPE,
    TEXTAREA_TYPE,
} from './Field.config';

export { BLIK_TYPE, CHECKBOX_TYPE, NUMBER_TYPE, PASSWORD_TYPE, RADIO_TYPE, SELECT_TYPE, TEXT_TYPE, TEXTAREA_TYPE };

export default class Field extends SourceField {
    renderInputOfType(type) {
        switch (type) {
            case CHECKBOX_TYPE:
                return this.renderCheckbox();
            case RADIO_TYPE:
                return this.renderRadioButton();
            case NUMBER_TYPE:
                return this.renderTypeNumber();
            case TEXTAREA_TYPE:
                return this.renderTextarea();
            case PASSWORD_TYPE:
                return this.renderTypePassword();
            case SELECT_TYPE:
                return this.renderSelectWithOptions();
            case BLIK_TYPE:
                return this.renderTypeBlik();
            default:
                return this.renderTypeText();
        }
    }

    renderTextarea() {
        return <FieldTextarea {...this.props} />;
    }

    renderRadioButton() {
        const { id, checked } = this.props;

        return (
            <>
                <FieldInput
                    {...this.props}
                    type="radio"
                    checked={checked}
                    onChange={this.onClick}
                    onKeyPress={this.onKeyPress}
                />
                <label htmlFor={id} />
            </>
        );
    }

    renderTypeText() {
        const { value } = this.props;

        const className = value ? 'isDirty' : '';

        return (
            <FieldInput
                {...this.props}
                type="text"
                /* eslint-disable-next-line react/forbid-component-props */
                className={className}
            />
        );
    }

    renderTypePassword() {
        const { value } = this.props;

        const className = value ? 'isDirty' : '';

        return (
            <FieldInput
                {...this.props}
                type="password"
                autocomplete="current-password"
                /* eslint-disable-next-line react/forbid-component-props */
                className={className}
            />
        );
    }

    renderLabel(renderMapArray = []) {
        const { id, label, validation, type } = this.props;

        const isRequired = validation.includes('notEmpty');
        if (!label || !renderMapArray.includes(type)) {
            return null;
        }

        return (
            <label block="Field" elem="Label" mods={{ isRequired }} htmlFor={id}>
                {label}
            </label>
        );
    }

    renderTypeBlik() {
        return <FieldInput {...this.props} type="number" />;
    }

    render() {
        const { mix, type, message, clearField } = this.props;

        return (
            <div block="Field" mods={{ type, hasError: !!message }} mix={mix}>
                {this.renderLabel([NUMBER_TYPE, RADIO_TYPE, CHECKBOX_TYPE, TEXTAREA_TYPE])}
                {this.renderInputOfType(type)}
                {this.renderLabel([TEXT_TYPE, PASSWORD_TYPE])}
                {this.renderMessage()}
                {!!message && <div block="Field" elem="ClearButton" onClick={clearField} />}
            </div>
        );
    }
}

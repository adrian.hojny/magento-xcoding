import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import CmsBlock from 'Component/CmsBlock/CmsBlock.container';
import FieldSelect from 'Component/FieldSelect';
import { LANG_PANEL } from 'Component/Header/Header.component';
import Overlay from 'Component/Overlay/Overlay.container';
import Sidebar from 'Component/Sidebar/Sidebar.container';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { getStaticFilePath } from 'Util/Image/Image';
import { getLangPrefix } from 'Util/Lang/Lang';
import isMobile from 'Util/Mobile';

import './LangPanel.style.scss';

export const DEFAULT_CURERNCY = 'DEFAULT_CURERNCY';
const METHODS_ICONS = 'home-methods-icons';

const LangPanel = ({ storeCode, storeList, currencies, handleChangeCurrency }) => {
    const [currency, setCurrency] = useState(currencies.default_display_currency_code);
    const [currentLang, setCurrentLang] = useState(storeCode);

    useEffect(() => {
        const userPreferredCurrency = BrowserDatabase.getItem(DEFAULT_CURERNCY);
        setCurrency(userPreferredCurrency || currencies.default_display_currency_code);
    }, [currencies?.default_display_currency_code]);

    useEffect(() => {
        setCurrentLang(storeCode);
    }, [storeCode]);

    const changeLang = (e) => {
        const value = e?.target?.value || e;
        const store = storeList.find((store) => store.code === value);
        const path = window.location.pathname;
        const correctedPath = path.length > 0 ? path.replace(`/${storeCode}/`, '') : path;

        if (store?.base_link_url) {
            window.location.href = store.base_link_url + correctedPath;
        }
    };

    const changeCurrency = (e) => {
        const currency = e?.target?.value || e;
        BrowserDatabase.setItem(currency, DEFAULT_CURERNCY);

        handleChangeCurrency(currency).then((data) => {
            if (data?.changeCurrency) {
                setCurrency(currency);

                window.location.reload();
            }
        });
    };

    const renderLangs = () => {
        const langPrefix = getLangPrefix();

        const correctedList = storeList.filter(
            (item) => item.is_active && item.base_link_url.includes(window.location.host),
        );

        const options = correctedList.reduce((acc, item) => {
            acc.push({
                id: item.code,
                value: item.code,
                label: item.name,
                prefix: getLangPrefix(item?.base_link_url),
            });

            return acc;
        }, []);

        const renderedOptions = options.map((option) => (
            <option key={option.id} id={option.id} value={option.value}>
                {option.label}
            </option>
        ));

        return (
            <div block="LangPanel" elem="Langs">
                <span>{__('Language')}</span>
                {isMobile.any() ? (
                    <select block="FieldSelect" elem="Select" value={currentLang || ''} onChange={changeLang}>
                        {renderedOptions}
                    </select>
                ) : (
                    <FieldSelect
                        id="LangSelect"
                        name="LangSelect"
                        onChange={changeLang}
                        selectOptions={options}
                        value={currentLang}
                    />
                )}
                <img
                    block="LangPanel"
                    elem="Selected"
                    src={getStaticFilePath(`/assets/images/flags/${langPrefix}.png`)}
                    alt={langPrefix}
                />
            </div>
        );
    };

    const renderCurrencies = () => {
        const codes = currencies?.available_currency_codes || [];
        const options = codes.map((code) => {
            return {
                id: code,
                value: code,
                label: `${code}`,
            };
        });

        return (
            <div block="LangPanel" elem="Currencies">
                <span>{__('Currency')}</span>
                <FieldSelect
                    id="CurrencySelect"
                    name="CurrencySelect"
                    onChange={changeCurrency}
                    selectOptions={options}
                    value={currency}
                />
            </div>
        );
    };

    const renderContent = () => (
        <>
            {renderLangs()}
            <div block="LangPanel" elem="Cms">
                <CmsBlock identifier={METHODS_ICONS} />
            </div>
            {renderCurrencies()}
        </>
    );

    if (isMobile.tabletWidth() || isMobile.any()) {
        return (
            <Overlay
                id={LANG_PANEL}
                title={__('Preferences')}
                mix={{ block: 'MenuOverlay', elem: 'ItemList', mix: { block: 'LangPanel' } }}
            >
                {renderContent()}
            </Overlay>
        );
    }

    return (
        <Sidebar id={LANG_PANEL} title={__('Preferences')} mix={{ block: 'LangPanel' }}>
            {renderContent()}
        </Sidebar>
    );
};

LangPanel.propTypes = {
    storeCode: PropTypes.string,
    storeList: PropTypes.array,
    currencies: PropTypes.object,
    handleChangeCurrency: PropTypes.func.isRequired,
};

LangPanel.defaultProps = {
    currencies: {},
    storeCode: '',
    storeList: [],
};

export default LangPanel;

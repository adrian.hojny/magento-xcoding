import React from 'react';

import { ProductWishlistButton as SourceProductWishlistButton } from 'SourceComponent/ProductWishlistButton/ProductWishlistButton.component';

import './ProductWishlistButton.style';

export class ProductWishlistButton extends SourceProductWishlistButton {
    constructor(props) {
        super(props);
        this.buttonRef = React.createRef();
    }

    renderContent() {
        const { isInWishlist, isDisabled } = this.props;

        const click = () => {
            this.buttonRef.current.blur();
            this.onClick();
        };

        return (
            <div
                block="ProductWishlistButton"
                onClick={click}
                mods={{ isInWishlist, isDisabled }}
                title={this.getTitle()}
                ref={this.buttonRef}
            >
                <span>{__('Move to')}</span>
            </div>
        );
    }
}

export default ProductWishlistButton;

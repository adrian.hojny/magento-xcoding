import PropTypes from 'prop-types';

const SidebarDimmer = ({ isActive, onCloseButtonClick }) => (
    <div block="Dimmer" mods={{ isActive }} onClick={onCloseButtonClick} />
);

SidebarDimmer.propTypes = {
    isActive: PropTypes.bool.isRequired,
    onCloseButtonClick: PropTypes.func.isRequired,
};

export default SidebarDimmer;

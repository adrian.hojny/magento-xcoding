import React from 'react';

import SourceVideoThumbnail from 'SourceComponent/VideoThumbnail/VideoThumbnail.component';

import './VideoThumbnail.style';

export * from 'SourceComponent/VideoThumbnail/VideoThumbnail.component';

export default class VideoThumbnail extends SourceVideoThumbnail {
    renderPlayIcon() {
        return (
            <span block="VideoThumbnail" elem="PlayIcon">
                <span>Play video</span>
            </span>
        );
    }
}

import { connect } from 'react-redux';

import SourceSearchField from 'SourceComponent/SearchField/SearchField.component';

import ClickOutside from 'Component/ClickOutside';
import SearchOverlay from 'Component/SearchOverlay';
import history from 'Util/History';

export const mapStateToProps = (state) => ({
    code: state.ConfigReducer.code,
});

class SearchField extends SourceSearchField {
    onSearchEnterPress = (e) => {
        const { searchCriteria, hideActiveOverlay, onSearchBarChange, code } = this.props;
        const search = searchCriteria.trim().replace(/\s\s+/g, '%20');
        const trimmedSearch = searchCriteria.trim();

        if (e.key === 'Enter' && trimmedSearch !== '') {
            history.push(`/${code}/search/${search}`);
            hideActiveOverlay();
            onSearchBarChange({ target: { value: '' } });
            this.searchBarRef.current.blur();
            this.closeSearch();
        }
    };

    renderContent() {
        const { searchCriteria, onSearchBarFocus, isActive } = this.props;

        const { isPlaceholderVisible } = this.state;

        return (
            <>
                <input
                    id="search-field"
                    ref={this.searchBarRef}
                    block="SearchField"
                    elem="Input"
                    onFocus={onSearchBarFocus}
                    onChange={this.handleChange}
                    onKeyDown={this.onSearchEnterPress}
                    value={searchCriteria}
                    mods={{ isActive }}
                    autoComplete="off"
                />
                <div
                    block="SearchField"
                    elem="Placeholder"
                    mods={{
                        isActive,
                        isPlaceholderVisible,
                    }}
                >
                    <span>{__('Search')}</span>
                </div>
            </>
        );
    }

    render() {
        const { onSearchOutsideClick, searchCriteria, isVisible, isActive } = this.props;

        return (
            <div block="SearchField" mods={{ isVisible, isActive }}>
                <ClickOutside onClick={onSearchOutsideClick}>
                    <div block="SearchField" elem="Wrapper">
                        {this.renderContent()}
                        <SearchOverlay clearSearch={this.clearSearch} searchCriteria={searchCriteria} />
                    </div>
                </ClickOutside>
            </div>
        );
    }
}

export default connect(mapStateToProps)(SearchField);

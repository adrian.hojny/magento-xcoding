import React from 'react';

import SourceProductCard from 'SourceComponent/ProductCard/ProductCard.component';

import Image from 'Component/Image';
import Link from 'Component/Link';
import Loader from 'Component/Loader';
import ProductCardComingSoon from 'Component/ProductCardComingSoon/ProductCardComingSoon.component';
import TextPlaceholder from 'Component/TextPlaceholder';

import './ComingSoonProductCard.style.scss';

export * from 'SourceComponent/ProductCard/ProductCard.component';

class ComingSoonProductCard extends SourceProductCard {
    renderMainDetails() {
        const {
            product: { name },
        } = this.props;

        return (
            <p block="ComingSoonProductCard" elem="Name" mods={{ isLoaded: !!name }}>
                <TextPlaceholder content={name} length="medium" />
            </p>
        );
    }

    renderCardWrapper(children) {
        const { linkTo } = this.props;

        return (
            <Link block="ComingSoonProductCard" elem="Link" to={linkTo} onClick={this.registerSharedElement}>
                {children}
            </Link>
        );
    }

    renderPicture() {
        const {
            product: { name, thumbnail, small_image },
            thumbnail: sourceThumbnail,
        } = this.props;
        const src = small_image?.url || thumbnail?.url || sourceThumbnail;

        return <Image src={src} alt={name} ratio="custom" />;
    }

    render() {
        const {
            product: { sku },
            mix,
            isLoading,
            product,
        } = this.props;

        return (
            <div block="ComingSoonProductCard" itemScope itemType={sku && 'https://schema.org/Product'} mix={mix}>
                <Loader isLoading={isLoading} />
                <meta itemProp="sku" content={sku} />
                {this.renderCardWrapper(
                    <>
                        <figure block="ComingSoonProductCard" elem="Figure">
                            {this.renderPicture()}
                        </figure>
                        <div block="ComingSoonProductCard" elem="Content">
                            <ProductCardComingSoon product={product} comingSoonSection />
                            {this.renderMainDetails()}
                        </div>
                    </>,
                )}
            </div>
        );
    }
}

export default ComingSoonProductCard;

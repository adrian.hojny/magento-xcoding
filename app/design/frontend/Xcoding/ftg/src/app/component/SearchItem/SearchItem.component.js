import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Image from 'Component/Image';
import Link from 'Component/Link';
import ProductAttributeValue from 'Component/ProductAttributeValue';
import TextPlaceholder from 'Component/TextPlaceholder';
import { AttributeType, ProductType } from 'Type/ProductList';
import { roundPrice } from 'Util/Price';

import 'SourceComponent/SearchItem/SearchItem.style';

export class SearchItem extends PureComponent {
    static propTypes = {
        linkTo: PropTypes.oneOfType([PropTypes.string, PropTypes.shape({})]),
        imgSrc: PropTypes.string,
        customAttribute: AttributeType,
        product: ProductType,
        onClick: PropTypes.func.isRequired,
    };

    static defaultProps = {
        linkTo: {},
        imgSrc: '',
        customAttribute: null,
        product: {},
    };

    renderSearchItemPrice(price) {
        if (!price) {
            return null;
        }

        const { minimum_price: { final_price, regular_price } = {} } = price;

        return (
            <div block="SearchOverlay" elem="Price">
                <span>{roundPrice(final_price.value)}</span>
                <span>{final_price.currency}</span>

                {roundPrice(regular_price.value) !== roundPrice(final_price.value) && (
                    <span block="SearchOverlay" elem="OldPrice">
                        <span>{roundPrice(regular_price.value)}</span>
                        <span>{regular_price.currency}</span>
                    </span>
                )}
            </div>
        );
    }

    renderCustomAttribute() {
        const { customAttribute } = this.props;

        if (!customAttribute) {
            return null;
        }

        return (
            <div block="SearchItem" elem="CustomAttribute">
                <ProductAttributeValue attribute={customAttribute} isFormattedAsText />
            </div>
        );
    }

    renderContent() {
        const {
            product,
            product: { variants },
        } = this.props;

        let productOrVariant = product;

        if (variants?.length) {
            productOrVariant = variants[0];
        }

        const { name, price_range, sku } = productOrVariant;

        return (
            <figcaption block="SearchItem" elem="Content">
                <p block="SearchOverlay" elem="Sku">
                    <TextPlaceholder content={sku} />
                </p>
                {this.renderCustomAttribute()}
                <h4 block="SearchItem" elem="Title" mods={{ isLoaded: !!name }}>
                    <TextPlaceholder content={name} length="long" />
                </h4>
                {this.renderSearchItemPrice(price_range)}
            </figcaption>
        );
    }

    renderImage() {
        const {
            product: { name },
            imgSrc,
        } = this.props;

        if (name && !imgSrc) {
            return <div block="SearchItem" elem="Image" />;
        }

        return (
            <Image
                block="SearchItem"
                elem="Image"
                src={imgSrc}
                alt={__('Product %s thumbnail.', name)}
                isPlaceholder={!name}
            />
        );
    }

    renderLink() {
        const { linkTo, onClick } = this.props;

        return (
            <Link block="SearchItem" elem="Link" to={linkTo} onClick={onClick}>
                <figure block="SearchItem" elem="Wrapper">
                    {this.renderImage()}
                    {this.renderContent()}
                </figure>
            </Link>
        );
    }

    render() {
        return <li block="SearchItem">{this.renderLink()}</li>;
    }
}

export default SearchItem;

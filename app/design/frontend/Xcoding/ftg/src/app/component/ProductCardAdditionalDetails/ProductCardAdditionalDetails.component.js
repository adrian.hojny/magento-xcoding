import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import striptags from 'striptags';

import Html from 'Component/Html';

import './ProductCardAdditionalDetails.style.scss';

const BLOCK_NAME = 'ProductCardAdditionalDetails';

class ProductCardAdditionalDetails extends PureComponent {
    static propTypes = {
        content: PropTypes.object,
        elem: PropTypes.string,
        itemprop: PropTypes.string,
    };

    static defaultProps = {
        content: {},
        elem: undefined,
        itemprop: undefined,
    };

    render() {
        const {
            content: { html },
            elem,
            itemprop,
        } = this.props;

        if (!html) {
            return null;
        }

        return (
            <div block={BLOCK_NAME} mix={{ block: BLOCK_NAME, elem }} itemProp={itemprop} content={striptags(html)}>
                <Html content={html} />
            </div>
        );
    }
}

export default ProductCardAdditionalDetails;

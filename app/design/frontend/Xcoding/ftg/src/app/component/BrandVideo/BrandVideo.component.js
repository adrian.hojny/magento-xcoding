import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './BrandVideo.style.scss';

export default class BrandVideo extends PureComponent {
    static propTypes = {
        description: PropTypes.string.isRequired,
        logo: PropTypes.string.isRequired,
        videoPlaceholder: PropTypes.string.isRequired,
        videoSrc: PropTypes.string.isRequired,
        brandName: PropTypes.string.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            videoOverlay: false,
        };
    }

    handlePlay = () => {
        this.setState({ videoOverlay: true });
    };

    handleClose = () => {
        this.setState({ videoOverlay: false });
    };

    renderVideoOverlay() {
        const { videoSrc } = this.props;
        const { videoOverlay } = this.state;

        return (
            videoOverlay && (
                <div block="BrandVideo" elem="VideoOverlay">
                    <video width="800" height="600" autoPlay controls>
                        <source src={videoSrc} type="video/mp4" />
                        Your browser does not support the video tag.
                    </video>
                    <div block="BrandVideo" elem="Close" onClick={this.handleClose} />
                </div>
            )
        );
    }

    render() {
        const { description, logo, videoPlaceholder, videoSrc, brandName } = this.props;

        return (
            <div block="BrandVideo">
                <div block="BrandVideo" elem="Video">
                    <div block="BrandVideo" elem="VideoPlaceholder">
                        <img src={videoPlaceholder} alt={brandName} />
                        {videoSrc && <button onClick={this.handlePlay}>{__('Play video')}</button>}
                    </div>
                    {this.renderVideoOverlay()}
                </div>
                <div block="BrandVideo" elem="Content">
                    {logo && (
                        <div block="BrandVideo" elem="Logo">
                            <img src={logo} alt={brandName} />
                        </div>
                    )}
                    <p block="BrandVideo" elem="Desc">
                        {description}
                    </p>
                </div>
            </div>
        );
    }
}

import SourceCategoryPagination from 'SourceComponent/CategoryPagination/CategoryPagination.component';

import CategoryPaginationInput from 'Component/CategoryPaginationInput/CategoryPaginationInput.component';
import SourceCategoryPaginationLink from 'Component/CategoryPaginationLink/CategoryPaginationLink.component';
import TextPlaceholder from 'Component/TextPlaceholder';

export default class CategoryPagination extends SourceCategoryPagination {
    renderPreviousPageLink(page) {
        return this.renderPageLink(
            page,
            __('Previous page'),
            false,
            <div block="CategoryPagination" elem="Arrow" mods={{ left: true }} />,
        );
    }

    renderNextPageLink(page) {
        return this.renderPageLink(
            page,
            __('Next page'),
            false,
            <div block="CategoryPagination" elem="Arrow" mods={{ right: true }} />,
        );
    }

    renderPageLink(pageNumber, label, isCurrent, text) {
        const { pathname, onPageSelect, getSearchQuery } = this.props;

        return (
            <SourceCategoryPaginationLink
                label={label}
                url_path={pathname}
                getPage={onPageSelect}
                isCurrent={isCurrent}
                pageNumber={pageNumber}
                getSearchQueryForPage={getSearchQuery}
            >
                {text}
            </SourceCategoryPaginationLink>
        );
    }

    renderPlaceholder() {
        return <TextPlaceholder length="short" />;
    }

    render() {
        const { totalPages, currentPage, isLoading, hideOnMobile } = this.props;

        return (
            <nav aria-label={__('Product list navigation')} block="CategoryPagination" mods={{ hideOnMobile }}>
                {isLoading ? (
                    this.renderPlaceholder()
                ) : (
                    <>
                        {currentPage > 1 && this.renderPreviousPageLink(currentPage - 1)}
                        <CategoryPaginationInput {...this.props} key={currentPage} />
                        <span>{__('of')}</span>
                        <span block="CategoryPagination" elem="Total">
                            {totalPages}
                        </span>
                        {currentPage <= totalPages - 1 && this.renderNextPageLink(currentPage + 1)}
                    </>
                )}
            </nav>
        );
    }
}

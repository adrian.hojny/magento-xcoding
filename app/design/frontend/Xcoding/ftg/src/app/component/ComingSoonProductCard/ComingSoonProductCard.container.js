import React from 'react';
import { connect } from 'react-redux';
import { Subscribe } from 'unstated';

import {
    mapDispatchToProps,
    ProductCardContainer as SourceProductCardContainer,
} from 'SourceComponent/ProductCard/ProductCard.container';

import SharedTransitionContainer from 'Component/SharedTransition/SharedTransition.unstated';
import ComingSoonProductCard from './ComingSoonProductCard.component';

class ComingSoonProductCardContainer extends SourceProductCardContainer {
    render() {
        return (
            <Subscribe to={[SharedTransitionContainer]}>
                {({ registerSharedElement }) => (
                    <ComingSoonProductCard
                        {...{ ...this.props, registerSharedElement }}
                        {...this.containerFunctions}
                        {...this.containerProps()}
                    />
                )}
            </Subscribe>
        );
    }
}

export default connect(null, mapDispatchToProps)(ComingSoonProductCardContainer);

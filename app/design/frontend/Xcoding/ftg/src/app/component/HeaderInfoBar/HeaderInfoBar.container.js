import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { STATE_MAP } from 'Component/Header/Header.component';
import HeaderInfoBar from 'Component/HeaderInfoBar/HeaderInfoBar.component';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import isMobile from 'Util/Mobile';
import { debounce } from 'Util/Request';

export const mapStateToProps = (state) => ({
    navigationMode: state.NavigationReducer[TOP_NAVIGATION_TYPE].navigationState.name,
    navigationState: {},
});

const HIDDEN_CLASS_NAME = 'isHeaderInfoBarHidden';

class HeaderInfoBarContainer extends PureComponent {
    static propTypes = {
        navigationMode: PropTypes.string.isRequired,
    };

    state = {
        isHidden: false,
    };

    componentDidMount() {
        const SCROLL_DEBOUNCE_DELAY = 20;
        window.addEventListener('scroll', debounce(this.handleScroll, SCROLL_DEBOUNCE_DELAY));
    }

    componentDidUpdate(prevProps) {
        this.handleVisibleOnNavigationModeChange(prevProps);
    }

    handleScroll = () => {
        const { navigationMode } = this.props;
        if (!STATE_MAP[navigationMode]?.logo) return;

        const windowY = window.scrollY;
        this.handleNavVisibilityOnScroll(windowY);
        this.scrollPosition = windowY;
    };

    handleVisibleOnNavigationModeChange(prevProps) {
        const { navigationMode } = this.props;
        const { navigationMode: prevNavigationMode } = prevProps;

        if (navigationMode !== prevNavigationMode) {
            const isHidden = !STATE_MAP[navigationMode]?.infoBar && isMobile.width();
            this.setState({ isHidden });

            if (isHidden && isMobile.width()) {
                document.documentElement.classList.add('forceHeaderInfoBarHidden');
                document.documentElement.classList.add(HIDDEN_CLASS_NAME);
            } else {
                document.documentElement.classList.remove('forceHeaderInfoBarHidden');
                document.documentElement.classList.remove(HIDDEN_CLASS_NAME);
            }
        }
    }

    handleNavVisibilityOnScroll(windowY) {
        const ERROR_OFFSET = 5;
        const TOP_MIN_OFFSET = 100;
        const BOTTOM_MIN_OFFSET = 100;

        const doc = document.body;
        const offset = doc.scrollTop + window.innerHeight;
        const height = doc.offsetHeight;

        if (windowY < TOP_MIN_OFFSET) {
            // We are on top
            document.documentElement.classList.remove(HIDDEN_CLASS_NAME);
            return;
        }

        if (offset >= height - BOTTOM_MIN_OFFSET) {
            // We are on the bottom
            document.documentElement.classList.remove(HIDDEN_CLASS_NAME);
            return;
        }

        // Scroll is less then min offset
        if (Math.abs(windowY - this.scrollPosition) < ERROR_OFFSET) {
            return;
        }

        if (windowY < this.scrollPosition) {
            // Scrolling UP
            document.documentElement.classList.remove(HIDDEN_CLASS_NAME);
        } else {
            // Scrolling DOWN
            document.documentElement.classList.add(HIDDEN_CLASS_NAME);
        }
    }

    render() {
        return <HeaderInfoBar {...this.state} />;
    }
}

export default connect(mapStateToProps)(HeaderInfoBarContainer);

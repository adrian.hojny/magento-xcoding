import { connect } from 'react-redux';

import { changeNavigationState } from 'Store/Navigation/Navigation.action';
import TabbedRemoteNavi from './TabbedRemoteNavi.component';

export const mapStateToProps = (state) => ({
    getNavigationStateById: (id) =>
        (state.NavigationReducer[id] && state.NavigationReducer[id].navigationState) || null,
    navigationState: state.NavigationReducer, // to have subscription on this prop and react on new tabs
});

export const mapDispatchToProps = (dispatch) => ({
    changeNavigationStateWithId: (id, state) => dispatch(changeNavigationState(id, state)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TabbedRemoteNavi);

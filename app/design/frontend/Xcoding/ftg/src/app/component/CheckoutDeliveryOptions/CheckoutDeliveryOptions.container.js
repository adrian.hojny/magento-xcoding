import { CheckoutDeliveryOptionsContainer as SourceCheckoutDeliveryOptionsContainer } from 'SourceComponent/CheckoutDeliveryOptions/CheckoutDeliveryOptions.container';

class CheckoutDeliveryOptionsContainer extends SourceCheckoutDeliveryOptionsContainer {
    containerFunctions = {
        selectShippingMethod: this.selectShippingMethod.bind(this),
        selectInpostDeliveryPoint: this.selectInpostDeliveryPoint.bind(this),
        selectPickUpPoints: this.selectPickUpPoints.bind(this),
    };

    selectInpostDeliveryPoint(data) {
        const { onAdditionalInformationChange } = this.props;

        onAdditionalInformationChange({ inpost: data });
    }

    selectPickUpPoints(data) {
        const { onAdditionalInformationChange } = this.props;

        onAdditionalInformationChange({ pickup: data });
    }

    static getDerivedStateFromProps(props, state) {
        const { shippingMethods, selectedShippingMethod } = props;
        const { prevShippingMethods } = state;

        if (shippingMethods.length !== prevShippingMethods.length) {
            const selectedShippingMethodCode = CheckoutDeliveryOptionsContainer._getDefaultMethod(props);

            return {
                selectedShippingMethodCode,
                prevShippingMethods: shippingMethods,
            };
        }

        if (
            selectedShippingMethod?.method_code &&
            selectedShippingMethod?.method_code !== state.selectedShippingMethodCode
        ) {
            return {
                selectedShippingMethodCode: selectedShippingMethod.method_code,
            };
        }

        return null;
    }

    componentDidUpdate(_, prevState) {
        const { onShippingMethodSelect, shippingMethods, setShippingMethod } = this.props;
        const { selectedShippingMethodCode } = this.state;
        const { selectedShippingMethodCode: prevSelectedShippingMethodCode } = prevState;

        if (selectedShippingMethodCode !== prevSelectedShippingMethodCode) {
            const shippingMethod = shippingMethods.find(
                ({ method_code }) => method_code === selectedShippingMethodCode,
            );

            onShippingMethodSelect(shippingMethod);
            setShippingMethod(shippingMethod);
        }
    }
}

export default CheckoutDeliveryOptionsContainer;

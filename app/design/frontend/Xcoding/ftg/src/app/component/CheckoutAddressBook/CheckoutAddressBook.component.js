import { CheckoutAddressBook as SourceCheckoutAddressBook } from 'SourceComponent/CheckoutAddressBook/CheckoutAddressBook.component';

import Field from 'Component/Field';

export class CheckoutAddressBook extends SourceCheckoutAddressBook {
    renderCompanyCheckbox() {
        const {
            isCompany,
            onIsCompanyChange,
            companyDetails: { companyName, companyVatId, onCompanyNameChange, onCompanyTaxIdNumberChange },
        } = this.props;

        return (
            <div block="FieldForm">
                <Field
                    type="checkbox"
                    value={isCompany}
                    label={__('Buy as a company')}
                    id="isCompany"
                    name="isCompany"
                    validation={['isChecked']}
                    onChange={onIsCompanyChange}
                />
                {isCompany && (
                    <>
                        <Field
                            type="text"
                            label={__('Company name')}
                            value={companyName}
                            id="companyName"
                            name="companyName"
                            validation={['notEmpty', 'maxLength']}
                            onChange={onCompanyNameChange}
                        />
                        <Field
                            type="text"
                            label={__('Tax/VAT Number')}
                            value={companyVatId}
                            id="companyVatId"
                            name="companyVatId"
                            validation={['notEmpty', 'maxLength']}
                            onChange={onCompanyTaxIdNumberChange}
                        />
                    </>
                )}
            </div>
        );
    }

    render() {
        const { isBilling } = this.props;
        return (
            <div block="CheckoutAddressBook">
                {this.renderHeading()}
                {isBilling && this.renderCompanyCheckbox()}
                {this.renderContent()}
            </div>
        );
    }
}

export default CheckoutAddressBook;

import React from 'react';

import SourceFooter from 'SourceComponent/Footer/Footer.component';

import CmsBlockComponent from 'Component/CmsBlock/CmsBlock.component';
import ContentWrapper from 'Component/ContentWrapper';
import { LANG_PANEL } from 'Component/Header/Header.component';
import CmsBlockQuery from 'Query/CmsBlock.query';
import { getStaticFilePath } from 'Util/Image/Image';
import { getLangPrefix } from 'Util/Lang/Lang';
import isMobile from 'Util/Mobile';
import { makeCancelable } from 'Util/Promise';
import { prepareQuery } from 'Util/Query';
import { executeGet } from 'Util/Request';
import { hash } from 'Util/Request/Hash';
import { ONE_MONTH_IN_SECONDS } from 'Util/Request/QueryDispatcher';

import './Footer.extend.style.scss';

export * from 'SourceComponent/Footer/Footer.component';

const FOOTER_CONTACT = 'footer-contact';
const FOOTER_INFO = 'footer-info';
const FOOTER_HELP = 'footer-help';
const FOOTER_SOCIALS = 'footer-socials';
const FOOTER_ICONS = 'footer-icons';
const FOOTER_BOTTOM_LINKS = 'footer-bottom-links';

class Footer extends SourceFooter {
    constructor(props) {
        super(props);
        this.state = {
            blocks: [],
        };

        this.handleClick = this.handleClick.bind(this);
        this.getBlockData = this.getBlockData.bind(this);
    }

    componentDidMount() {
        this.getCmsBlock([FOOTER_CONTACT, FOOTER_INFO, FOOTER_HELP, FOOTER_SOCIALS, FOOTER_ICONS, FOOTER_BOTTOM_LINKS]);
    }

    getCmsBlock(identifier) {
        this.fetchData(
            [CmsBlockQuery.getQuery({ identifiers: Array.from(identifier) })],
            ({ cmsBlocks: { items } }) => {
                if (!items.length) {
                    return;
                }

                this.setState({ blocks: items });
            },
        );
    }

    fetchData(rawQueries, onSucces = () => {}, onError = () => {}) {
        const preparedQuery = prepareQuery(rawQueries);
        const { query, variables } = preparedQuery;
        const queryHash = hash(query + JSON.stringify(variables));
        if (!window.dataCache) {
            window.dataCache = {};
        }

        if (window.dataCache[queryHash]) {
            onSucces(window.dataCache[queryHash]);
            return;
        }

        this.promise = makeCancelable(executeGet(preparedQuery, this.dataModelName, ONE_MONTH_IN_SECONDS));

        this.promise.promise.then(
            (response) => {
                window.dataCache[queryHash] = response;
                onSucces(response);
            },
            (err) => {
                console.log({ err });
                return onError(err);
            },
        );
    }

    getBlockData(identifier) {
        const { blocks } = this.state;

        return blocks.find((block) => block.identifier === identifier);
    }

    handleOpenLangPanel = () => {
        const { showOverlay } = this.props;
        showOverlay(LANG_PANEL);
    };

    renderTop() {
        const lang = getLangPrefix();

        return (
            <ContentWrapper
                mix={{ block: 'Footer', elem: 'Top' }}
                wrapperMix={{ block: 'Footer', elem: 'WrapperTop' }}
                label={__('Footer Top Wrapper')}
            >
                {isMobile.any() && (
                    <div block="Footer" elem="Lang" onClick={this.handleOpenLangPanel}>
                        <span>{__('Language')}</span>
                        <img src={getStaticFilePath(`/assets/images/flags/${lang}.png`)} alt={lang} />
                        <span>{__('Change')}</span>
                    </div>
                )}
                <div block="Footer" elem="Block" mods={{ contact: true }}>
                    <CmsBlockComponent cmsBlock={this.getBlockData(FOOTER_CONTACT)} />
                </div>
                <div block="Footer" elem="AlignWrapper">
                    <div block="Footer" elem="AlignWrapperTop">
                        <div block="Footer" elem="Block" mods={{ info: true }} onClick={this.handleClick}>
                            <CmsBlockComponent cmsBlock={this.getBlockData(FOOTER_INFO)} />
                        </div>
                        <div block="Footer" elem="Block" mods={{ help: true }} onClick={this.handleClick}>
                            <CmsBlockComponent cmsBlock={this.getBlockData(FOOTER_HELP)} />
                        </div>
                        {!isMobile.any() && (
                            <div block="Footer" elem="Block" mods={{ socials: true }}>
                                <CmsBlockComponent cmsBlock={this.getBlockData(FOOTER_SOCIALS)} />
                            </div>
                        )}
                    </div>
                    <div block="Footer" elem="AlignWrapperBottom">
                        {isMobile.any() && (
                            <div block="Footer" elem="Block" mods={{ socials: true }}>
                                <CmsBlockComponent cmsBlock={this.getBlockData(FOOTER_SOCIALS)} />
                            </div>
                        )}
                        <CmsBlockComponent cmsBlock={this.getBlockData(FOOTER_BOTTOM_LINKS)} />
                    </div>
                </div>
            </ContentWrapper>
        );
    }

    handleClick = (event) => {
        const actionMobile = event.target.dataset.mobile;
        if (event.target.nextSibling) {
            const currentSiblingState = event.target.nextSibling.hidden;
            if (actionMobile) {
                const title = document.getElementById(event.target.id);
                title.nextSibling.hidden = !currentSiblingState;
                title.dataset.mobile = title.dataset.mobile === 'open' ? 'close' : 'open';
            }
        }
    };

    renderMid() {
        return (
            <ContentWrapper
                mix={{ block: 'Footer', elem: 'Mid' }}
                wrapperMix={{ block: 'Footer', elem: 'WrapperMid' }}
                label={__('Footer Mid Wrapper')}
            >
                <CmsBlockComponent cmsBlock={this.getBlockData(FOOTER_ICONS)} />
            </ContentWrapper>
        );
    }

    renderBottom() {
        const year = new Date().getFullYear();
        const copyright = __('Copyright © 2015 - %year SneakerStudio. All Rights Reserved');
        return (
            <ContentWrapper
                mix={{ block: 'Footer', elem: 'Bottom' }}
                wrapperMix={{ block: 'Footer', elem: 'WrapperBottom' }}
                label={__('Footer Bottom Wrapper')}
            >
                <p>{copyright.replace('%year', year)}</p>
            </ContentWrapper>
        );
    }

    render() {
        return (
            <footer block="Footer" aria-label="Footer">
                {this.renderTop()}
                {this.renderMid()}
                {this.renderBottom()}
            </footer>
        );
    }
}
export default Footer;

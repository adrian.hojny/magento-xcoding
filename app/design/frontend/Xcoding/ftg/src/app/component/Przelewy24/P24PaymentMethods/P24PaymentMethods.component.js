import React, { memo } from 'react';
import { useMediaPredicate } from 'react-media-hook';
import PropTypes from 'prop-types';

import Field from 'Component/Field';
import { BuyButton } from '../P24BuyButton/P24BuyButton.component';
import { P24_INNER_METHODS_NAME, P24_INNER_METHODS_NAME_TOC } from '../Przelewy24.config';
import { renderP24TocLabel } from '../Przelewy24.utils';

const P24PaymentMethod = memo(
    ({ name, imgUrl: desktopImgUrl, mobileImgUrl, mobile: isMethodVisibleOnMobile, isSelected, id, onSelected }) => {
        const isMobile = useMediaPredicate('(max-width: 767px)');

        if (isMobile && !isMethodVisibleOnMobile) return null;

        const imgUrl = isMobile ? mobileImgUrl : desktopImgUrl;

        const handleClick = () => {
            onSelected(id);
        };

        const mods = isSelected ? { isSelected } : {};

        return (
            <div block="P24PaymentMethod" mods={mods}>
                <input
                    type="radio"
                    id={id}
                    name={P24_INNER_METHODS_NAME}
                    value={id}
                    checked={isSelected}
                    onClick={handleClick}
                    readOnly
                />
                <label htmlFor={id}>
                    <img src={imgUrl} alt={name} />
                </label>
            </div>
        );
    },
);

const P24PaymentMethods = memo(
    ({
        methods,
        onPaymentMethodChange,
        isPayBtnDisabled,
        selectedPayment,
        setSelectedPayment,
        purgeOtherFormGroups,
    }) => {
        const handleSelected = (value) => {
            setSelectedPayment(value);
            onPaymentMethodChange({
                name: P24_INNER_METHODS_NAME,
                value,
            });
        };

        const handleTOCChange = (_, value) => {
            onPaymentMethodChange({
                name: P24_INNER_METHODS_NAME_TOC,
                value,
            });
        };

        return (
            <div block="P24PaymentMethods" elem="wrap">
                <div block="P24PaymentMethods">
                    {methods
                        .filter(({ status }) => !!status)
                        .map(({ name, imgUrl, mobileImgUrl, id, mobile }) => (
                            <P24PaymentMethod
                                key={id}
                                id={id}
                                name={name}
                                imgUrl={imgUrl}
                                mobileImgUrl={mobileImgUrl}
                                mobile={mobile}
                                isSelected={selectedPayment === id}
                                onSelected={handleSelected}
                            />
                        ))}
                </div>
                <div block="P24PaymentMethods" elem="submit-wrap">
                    <Field
                        type="checkbox"
                        label={renderP24TocLabel()}
                        id={P24_INNER_METHODS_NAME_TOC}
                        mix={{ block: 'P24PaymentMethods', elem: 'Checkbox' }}
                        name={P24_INNER_METHODS_NAME_TOC}
                        onChange={handleTOCChange}
                    />
                    <BuyButton
                        isDisabled={isPayBtnDisabled}
                        fieldsGroup={P24_INNER_METHODS_NAME}
                        purgeOtherFormGroups={purgeOtherFormGroups}
                    />
                </div>
            </div>
        );
    },
);

P24PaymentMethods.propTypes = {
    methods: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            id: PropTypes.number.isRequired,
            status: PropTypes.bool.isRequired,
            imgUrl: PropTypes.string.isRequired,
            mobileImgUrl: PropTypes.string,
            mobile: PropTypes.bool.isRequired,
        }),
    ).isRequired,
};

export default P24PaymentMethods;

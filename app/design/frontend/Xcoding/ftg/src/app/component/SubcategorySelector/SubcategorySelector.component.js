import React, { PureComponent } from 'react';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import PropTypes from 'prop-types';

import Link from 'Component/Link';

import './SubcategorySelector.style.scss';

export default class SubcategorySelector extends PureComponent {
    static propTypes = {
        category: PropTypes.object.isRequired,
    };

    getSubcategoryData = () => {
        const {
            category: { children = [], url, id, parent },
        } = this.props;

        const name = __('All');

        if (children.length === 0 && parent && parent.children && parent.children.length > 0) {
            return [{ name, url: parent.url, id: parent.id }, ...parent.children];
        }

        return [{ name, url, id }, ...children];
    };

    getSubcategoryLinks = (data) =>
        data.map(({ name, url, id }) => (
            <Link to={`${url}`} key={id} draggable={false}>
                {name}
            </Link>
        ));

    render() {
        const {
            category: { id },
        } = this.props;
        const subcategoryData = this.getSubcategoryData();

        if (!subcategoryData) return null;

        return (
            <ScrollMenu
                menuClass="SubcategorySelector"
                itemClass="SubcategorySelector-Item"
                itemClassActive="SubcategorySelector-Item_active"
                arrowClass="SubcategorySelector-ArrowWrapper"
                arrowDisabledClass="SubcategorySelector-ArrowWrapper_hidden"
                arrowLeft={<div block="SubcategorySelector" elem="Arrow" mods={{ left: true }} />}
                arrowRight={<div block="SubcategorySelector" elem="Arrow" mods={{ right: true }} />}
                scrollToSelected
                scrollBy={5}
                alignCenter={false}
                selected={id}
                data={this.getSubcategoryLinks(subcategoryData)}
                hideArrows
            />
        );
    }
}

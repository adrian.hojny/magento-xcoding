import PropTypes from 'prop-types';

import SourceProductReviewList from 'SourceComponent/ProductReviewList/ProductReviewList.component';

import ProductReviewRating from 'Component/ProductReviewRating';
import { RECOMMENDED } from 'Type/Rating';
import { isSignedIn } from 'Util/Auth';
import { getRating } from 'Util/Product/Review';

import './ProductReviewList.style.scss';

class ProductReviewList extends SourceProductReviewList {
    static propTypes = {
        ...super.propTypes,
        voteHelpfulness: PropTypes.func.isRequired,
    };

    renderReviewListItem = (reviewItem) => {
        const { review_id } = reviewItem;

        return (
            <li
                key={review_id}
                block="ProductReviewList"
                elem="Item"
                itemType="http://schema.org/Review"
                itemProp="review"
                itemScope
            >
                {this.renderMeta(reviewItem)}
                {this.renderItemContent(reviewItem)}
            </li>
        );
    };

    renderReviewListItemRating = (ratingVoteItem) => {
        const { vote_id, rating_code, percent } = ratingVoteItem;

        return (
            <div
                key={vote_id}
                block="ProductReviewList"
                elem="RatingSummaryItem"
                itemType="http://schema.org/Rating"
                itemScope
                itemProp="reviewRating"
            >
                <meta itemProp="ratingValue" content={percent} />
                <meta itemProp="worstRating" content={0} />
                <meta itemProp="bestRating" content={100} />
                <ProductReviewRating summary={percent} code={rating_code} />
                <span>{getRating(percent)}</span>
            </div>
        );
    };

    voteHelpfulness = (vote, reviewId) => () => {
        const { voteHelpfulness, showNotification } = this.props;

        if (!isSignedIn()) {
            return showNotification('info', __('Log in to be able to vote.'));
        }

        return voteHelpfulness({ vote, reviewId });
    };

    getFormattedDate(created_at) {
        const fixedDate = created_at.replace(/-/g, '/');
        const date = new Date(fixedDate);
        const day = String(date.getDate()).padStart(2, '0');
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const year = date.getFullYear();

        return `${day}.${month}.${year}`;
    }

    renderHelpfulnessInfo({ yes, no }, review_id) {
        return (
            <div block="ProductReviewList" elem="HelpfulnessInfo">
                {__('Was this review helpful?')}
                <button onClick={this.voteHelpfulness('VOTE_UP', review_id)}>
                    {__('Yes')}
                    <span>{`(${yes})`}</span>
                </button>
                <button onClick={this.voteHelpfulness('VOTE_DOWN', review_id)}>
                    {__('No')}
                    <span>{`(${no})`}</span>
                </button>
            </div>
        );
    }

    renderItemContent({ detail, helpfulness, extended_rating, review_id }) {
        return (
            <div block="ProductReviewList" elem="ItemContent">
                <div itemProp="reviewBody">{detail}</div>
                <div block="ProductReviewList" elem="ItemBottomContent">
                    {this.renderRecommendationInfo(extended_rating || {})}
                    {this.renderHelpfulnessInfo(helpfulness, review_id)}
                </div>
            </div>
        );
    }

    renderRecommendationInfo({ recommended_value }) {
        if (recommended_value === RECOMMENDED) {
            return (
                <div block="ProductReviewList" elem="RecommendationInfo">
                    {__('I recommend this product!')}
                </div>
            );
        }

        return null;
    }

    renderMeta({ nickname, created_at, rating_votes }) {
        return (
            <div block="ProductReviewList" elem="ItemMeta">
                <div block="ProductReviewList" elem="ItemAuthor" itemProp="author">
                    {nickname}
                </div>
                <div block="ProductReviewList" elem="ItemAuthorStatus">
                    {__('Logged in client')}
                </div>
                {rating_votes.length > 0 && this.renderReviewListItemRating(rating_votes[0])}
                <div block="ProductReviewList" elem="ItemDate">
                    {this.getFormattedDate(created_at)}
                </div>
                <meta itemProp="datePublished" content={this.getFormattedDate(created_at)} />
            </div>
        );
    }

    render() {
        const { product } = this.props;
        const hasReviews = product.reviews && Object.keys(product.reviews).length > 0;
        if (!hasReviews) return null;

        return (
            <div block="ProductReviewList" elem="Wrapper">
                <ul block="ProductReviewList">{this.renderReviews()}</ul>
            </div>
        );
    }
}

export default ProductReviewList;

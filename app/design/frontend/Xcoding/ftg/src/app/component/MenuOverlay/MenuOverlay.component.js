import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { MENU } from 'Component/Header/Header.config';
import Image from 'Component/Image/Image.container';
import Link from 'Component/Link';
import Overlay from 'Component/Overlay/Overlay.container';
import { MenuType } from 'Type/Menu';
import { getStaticFilePath } from 'Util/Image/Image';
import { getLangPrefix } from 'Util/Lang/Lang';
import media from 'Util/Media/Media';
import { getSortedItems } from 'Util/Menu';

import './MenuOverlay.style.scss';

export const MENU_OVERLAY_KEY = 'menu';

class MenuOverlay extends PureComponent {
    static propTypes = {
        menu: MenuType.isRequired,
        hideActiveOverlay: PropTypes.func.isRequired,
        changeHeaderState: PropTypes.func.isRequired,
    };

    componentDidMount() {
        handleResize();
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    handleResize() {
        const vh = window.clientHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
    }

    onVisible = () => {
        const { changeHeaderState } = this.props;
        changeHeaderState({ name: MENU });
    };

    renderTopLevel() {
        const { menu } = this.props;
        const categoryArray = Object.values(menu);

        if (!categoryArray.length) return null;

        const {
            0: { children: mainCategories, title: mainCategoriesTitle },
        } = categoryArray;

        const mainMods = { type: 'main' };

        return (
            <div block="MenuOverlay" elem="Menu">
                <ul block="MenuOverlay" elem="ItemList" mods={mainMods} aria-label={mainCategoriesTitle}>
                    {this.renderFirstLevel(mainCategories, mainMods)}
                    {this.renderAccountButton()}
                    {this.renderLangPanelButton()}
                </ul>
            </div>
        );
    }

    renderFirstLevel(itemList, itemMods) {
        const childrenArray = getSortedItems(Object.values(itemList));

        return childrenArray.map((item) => this.renderListItem(item, itemMods));
    }

    renderListItem(item, itemMods) {
        const { item_id, children, url, cms_page_identifier } = item;
        const childrenArray = Object.values(children);

        const path = cms_page_identifier ? `/${cms_page_identifier}` : url;

        return (
            <li key={item_id} block="MenuOverlay" elem="Item">
                {childrenArray.length ? (
                    <>
                        <div
                            onClick={(e) => this.props.handleSubcategoryClick(e, item)}
                            tabIndex="0"
                            role="button"
                            block="MenuOverlay"
                            elem="ItemButton"
                        >
                            {this.renderItemContent(item, itemMods)}
                        </div>
                        <div>{this.renderSubLevel(item)}</div>
                    </>
                ) : (
                    <Link to={path} onClick={this.props.closeMenuOverlay} block="MenuOverlay" elem="Link">
                        {this.renderItemContent(item, itemMods)}
                    </Link>
                )}
            </li>
        );
    }

    renderSubLevel(category) {
        const { activeMenuItemsStack } = this.props;
        const { item_id, children } = category;
        const childrenArray = getSortedItems(Object.values(children));
        const isVisible = activeMenuItemsStack.includes(item_id);
        const [firstItem] = activeMenuItemsStack;
        const activeElement = firstItem === item_id;
        const subcategoryMods = { type: 'subcategory' };

        return (
            <div block="MenuOverlay" elem="SubMenu" mods={{ isVisible, activeElement }}>
                <ul block="MenuOverlay" elem="ItemList" mods={{ ...subcategoryMods }}>
                    {childrenArray.map((item) => this.renderListItem(item))}
                </ul>
            </div>
        );
    }

    renderItemContent(item, mods = {}) {
        const { title, icon, item_class } = item;
        const itemMods = item_class.includes('MenuOverlay-ItemFigure_type_banner') ? { type: 'banner' } : mods;

        return (
            <>
                <div block="MenuOverlay" elem="ItemImage">
                    {this.renderItemContentImage(icon, itemMods)}
                </div>
                <div block="MenuOverlay" elem="ItemTitle">
                    {title}
                </div>
            </>
        );
    }

    renderItemContentImage(icon, itemMods) {
        if (!icon) return null;

        return (
            <Image
                mix={{ block: 'MenuOverlay', elem: 'Image', mods: itemMods }}
                src={icon && media(icon)}
                ratio="square"
                width="40px"
                height="40px"
            />
        );
    }

    renderAccountButton() {
        const { isSignedIn } = this.props;

        return (
            <li block="MenuOverlay" elem="Item">
                <Link
                    to={isSignedIn ? '/my-account/dashboard' : '/sign-in'}
                    onClick={this.props.closeMenuOverlay}
                    block="MenuOverlay"
                    elem="Link"
                >
                    <div block="MenuOverlay" elem="ItemImage" mods={{ type: 'account' }} />
                    <div block="MenuOverlay" elem="ItemTitle">
                        {isSignedIn ? __('Account') : __('Sign in')}
                    </div>
                </Link>
            </li>
        );
    }

    renderLangPanelButton() {
        const { handleOpenLangPanel } = this.props;
        const lang = getLangPrefix();

        return (
            <div block="MenuOverlay" elem="Item">
                <div block="MenuOverlay" elem="Link" onClick={handleOpenLangPanel}>
                    <div block="MenuOverlay" elem="ItemImage" mods={{ type: 'lang' }}>
                        <img src={getStaticFilePath(`/assets/images/flags/${lang}.png`)} alt={lang} />
                    </div>
                    <div block="MenuOverlay" elem="ItemTitle">
                        {__('Language')}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <Overlay id={MENU_OVERLAY_KEY} mix={{ block: 'MenuOverlay' }}>
                {this.renderTopLevel()}
            </Overlay>
        );
    }
}

export default MenuOverlay;

import { connect } from 'react-redux';

import { ProductsWithAttributeDispatcher } from 'Store/ProductsWithAttribute';
import CompleteLook from './CompleteLook.component';

export const LOOK_ATTRIBUTE_NAME = 'look';

export const mapStateToProps = (state) => ({
    completeLook: state.ProductsWithAttributeReducer.look,
});

export const mapDispatchToProps = (dispatch) => ({
    requestCompleteLook: (lookId) =>
        ProductsWithAttributeDispatcher.handleData(dispatch, {
            args: { filter: { attribute: { name: LOOK_ATTRIBUTE_NAME, value: lookId } } },
        }),
});

export default connect(mapStateToProps, mapDispatchToProps)(CompleteLook);

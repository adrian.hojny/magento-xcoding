import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { ProductConfigurableAttributesContainer as SourceProductConfigurableAttributesContainer } from 'SourceComponent/ProductConfigurableAttributes/ProductConfigurableAttributes.container';

import { goToPreviousNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { hideActiveOverlay } from 'Store/Overlay/Overlay.action';
import ProductConfigurableAttributes from './ProductConfigurableAttributes.component';

export const mapDispatchToProps = (dispatch) => ({
    hideActiveOverlay: () => dispatch(hideActiveOverlay()),
    goToPreviousNavigationState: () => dispatch(goToPreviousNavigationState(TOP_NAVIGATION_TYPE)),
});

export class ProductConfigurableAttributesContainer extends SourceProductConfigurableAttributesContainer {
    static propTypes = {
        getLink: PropTypes.func.isRequired,
        parameters: PropTypes.shape({}).isRequired,
        updateConfigurableVariant: PropTypes.func.isRequired,
        goToPreviousNavigationState: PropTypes.func.isRequired,
        hideActiveOverlay: PropTypes.func.isRequired,
        getIsConfigurableAttributeEnable: PropTypes.func,
    };

    containerFunctions = {
        handleOptionClick: this.handleOptionClick.bind(this),
        getSubHeading: this.getSubHeading.bind(this),
        isSelected: this.isSelected.bind(this),
        getLink: this.getLink.bind(this),
    };

    getLink({ attribute_code, attribute_value }) {
        const { getLink } = this.props;
        return getLink(attribute_code, attribute_value);
    }

    getSubHeading({ attribute_values, attribute_code, attribute_options }) {
        return attribute_values
            .reduce(
                (acc, attribute_value) =>
                    this.isSelected({ attribute_code, attribute_value })
                        ? [...acc, attribute_options[attribute_value].label]
                        : acc,
                [],
            )
            .join(', ');
    }

    handleOptionClick({ attribute_code, attribute_value }) {
        const { updateConfigurableVariant, hideActiveOverlay, goToPreviousNavigationState } = this.props;

        goToPreviousNavigationState();
        hideActiveOverlay();

        updateConfigurableVariant(attribute_code, attribute_value);
    }

    isSelected({ attribute_code, attribute_value }) {
        const { parameters = {} } = this.props;
        const parameter = parameters[attribute_code];

        if (parameter === undefined) {
            return false;
        }
        if (parameter.length !== undefined) {
            return parameter.includes(attribute_value);
        }

        return parameter === attribute_value;
    }

    render() {
        return <ProductConfigurableAttributes {...this.props} {...this.containerFunctions} />;
    }
}

export default connect(undefined, mapDispatchToProps)(ProductConfigurableAttributesContainer);

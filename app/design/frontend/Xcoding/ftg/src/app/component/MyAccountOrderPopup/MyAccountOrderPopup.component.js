import PropTypes from 'prop-types';

import { MyAccountOrderPopup as SourceMyAccountOrderPopup } from 'SourceComponent/MyAccountOrderPopup/MyAccountOrderPopup.component';

import Image from 'Component/Image';
import { orderType } from 'Type/Account';

import './MyAccountOrderPopup.style';

export class MyAccountOrderPopup extends SourceMyAccountOrderPopup {
    static propTypes = {
        order: orderType.isRequired,
        isLoading: PropTypes.bool.isRequired,
        currency_code: PropTypes.string.isRequired,
    };

    renderShipping() {
        const {
            order: {
                shipping_info,
                base_order_info: { order_currency_code },
            },
        } = this.props;

        const { shipping_description, shipping_amount, shipping_address } = shipping_info || {};

        if (!shipping_address) {
            return null;
        }

        return (
            <div block="MyAccountOrderPopup" elem="ShippingWrapper">
                <h4>{__('Shipping & Handling Information')}</h4>
                <dl>
                    <dt>{__('Description: ')}</dt>
                    <dd>{shipping_description}</dd>
                    <dt>{__('Price: ')}</dt>
                    <dd>{`${shipping_amount} ${order_currency_code}`}</dd>
                </dl>
                {this.renderShippingAddressTable()}
            </div>
        );
    }

    renderItems() {
        const {
            order: {
                order_products = [],
                base_order_info: { order_currency_code },
            },
        } = this.props;

        return order_products.map((product, i) => {
            const { name, thumbnail, id, qty, row_total } = product;

            const { url } = thumbnail || {};

            return (
                <tr key={id || i.toString()} block="MyAccountOrderPopup" elem="Row">
                    <td>
                        {url && (
                            <Image src={url} alt={name} mix={{ block: 'MyAccountOrderPopup', elem: 'Thumbnail' }} />
                        )}
                    </td>
                    <td>{name}</td>
                    <td>{qty}</td>
                    <td>{`${row_total} ${order_currency_code}`}</td>
                </tr>
            );
        });
    }

    renderTotals() {
        const {
            order: {
                base_order_info: { order_currency_code },
                base_order_info,
            },
        } = this.props;
        const { grand_total } = base_order_info || {};

        return (
            <div block="MyAccountOrderPopup" elem="OrderWrapper">
                <dl>
                    <dt>{__('Grand total: ')}</dt>
                    <dd>{`${grand_total} ${order_currency_code}`}</dd>
                </dl>
            </div>
        );
    }
}

export default MyAccountOrderPopup;

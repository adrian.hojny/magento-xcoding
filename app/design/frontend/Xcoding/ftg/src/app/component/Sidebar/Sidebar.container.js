import { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Sidebar from 'Component/Sidebar/Sidebar.component';
import { changeNavigationState, goToPreviousNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { hideActiveOverlay, showDimmer } from 'Store/Overlay/Overlay.action';

export const mapStateToProps = (state, { id }) => ({
    navigationState: state.NavigationReducer[TOP_NAVIGATION_TYPE],
    isActive: state.OverlayReducer.activeOverlay === id,
});

export const mapDispatchToProps = (dispatch) => ({
    hideActiveOverlay: () => dispatch(hideActiveOverlay()),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    goToPreviousNavigationState: () => dispatch(goToPreviousNavigationState(TOP_NAVIGATION_TYPE)),
    showDimmer: () => dispatch(showDimmer()),
});

export class SidebarContainer extends PureComponent {
    static propTypes = {
        navigationState: PropTypes.object.isRequired,
        hideActiveOverlay: PropTypes.func.isRequired,
        goToPreviousNavigationState: PropTypes.func.isRequired,
    };

    containerFunctions = {
        onCloseButtonClick: this.onCloseButtonClick.bind(this),
    };

    onCloseButtonClick() {
        const {
            navigationState: { onCloseClick },
        } = this.props;

        if (onCloseClick) return onCloseClick(this.defaultOnCloseClick);

        return this.defaultOnCloseClick();
    }

    defaultOnCloseClick = () => {
        const { hideActiveOverlay, goToPreviousNavigationState } = this.props;

        hideActiveOverlay();
        goToPreviousNavigationState();
    };

    render() {
        return <Sidebar {...this.props} {...this.containerFunctions} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SidebarContainer);

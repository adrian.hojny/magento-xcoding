import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './ProductLabels.style.scss';

export const LABEL_ATTRIBUTES = {
    is_new: { mod: 'isNew' },
    is_special: { mod: 'isNew' },
    comming_soon: { mod: 'isComming' },
    is_prm_website: { mod: 'isPremium' },
    is_on_sale: { mod: 'isSale' },
};

class ProductLabels extends PureComponent {
    static propTypes = {
        attributes: PropTypes.object,
    };

    static defaultProps = {
        attributes: undefined,
    };

    componentDidMount() {
        const timeLeft = this.calculateTimeLeftMs();

        if (timeLeft > 0) {
            setTimeout(() => {
                this.forceUpdate();
            }, timeLeft);
        }
    }

    calculateTimeLeftMs() {
        const { attributes } = this.props;
        if (!attributes) {
            return -1;
        }
        const { comming_soon_date, comming_soon } = attributes;

        if (!comming_soon_date || !comming_soon) {
            return -1;
        }

        if (!comming_soon_date.attribute_value || !comming_soon.attribute_value) {
            return -1;
        }

        const difference = +new Date(comming_soon_date.attribute_value) - +new Date();

        return difference;
    }

    renderLabel(name, attribute) {
        const { attribute_label, attribute_value } = attribute;

        if (name === 'comming_soon') {
            const timeLeft = this.calculateTimeLeftMs();

            if (timeLeft < 0) {
                return null;
            }
        }

        if (!LABEL_ATTRIBUTES[name] || attribute_value !== '1') return null;

        return (
            <div
                block="ProductLabels"
                elem="Label"
                aria-label={attribute_label}
                mods={{ [LABEL_ATTRIBUTES[name].mod]: true }}
                key={name}
            >
                {attribute_label}
            </div>
        );
    }

    render() {
        const { attributes } = this.props;

        if (!attributes) return null;

        const attributesArr = Object.keys(attributes);

        return (
            <div block="ProductLabels">
                {attributesArr.map((attrName) => this.renderLabel(attrName, attributes[attrName]))}
            </div>
        );
    }
}

export default ProductLabels;

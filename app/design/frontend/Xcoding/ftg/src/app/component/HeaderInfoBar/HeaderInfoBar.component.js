import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import CmsBlock from 'Component/CmsBlock';

import './HeaderInfoBar.style.scss';

export const HEADER_INFO_BAR = 'header-info-bar';

class HeaderInfoBar extends PureComponent {
    static propTypes = {
        isHidden: PropTypes.bool.isRequired,
    };

    render() {
        const { isHidden } = this.props;
        return (
            <div block="Header" elem="InfoBar" mods={{ isHidden }}>
                <CmsBlock identifier={HEADER_INFO_BAR} withoutWrapper />
            </div>
        );
    }
}

export default HeaderInfoBar;

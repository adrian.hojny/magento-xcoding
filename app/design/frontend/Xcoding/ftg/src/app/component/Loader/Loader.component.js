import PropTypes from 'prop-types';

import './Loader.style';

export const Loader = ({ isLoading }) => {
    if (!isLoading) {
        return null;
    }

    return (
        <div block="Loader">
            <div block="Loader" elem="Scale">
                <div block="Loader" elem="Main" />
            </div>
        </div>
    );
};

Loader.propTypes = {
    isLoading: PropTypes.bool.isRequired,
};

export default Loader;

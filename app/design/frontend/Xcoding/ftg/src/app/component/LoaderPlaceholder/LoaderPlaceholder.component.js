import './LoaderPlaceholder.style';

export const LoaderPlaceholder = () => (
    <div block="LoaderPlaceholder">
        <div block="LoaderPlaceholder" elem="Main" />
    </div>
);

export default LoaderPlaceholder;

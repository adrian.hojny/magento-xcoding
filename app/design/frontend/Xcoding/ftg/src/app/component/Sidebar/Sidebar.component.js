import React, { PureComponent } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';

import { POPUP } from 'Component/Header/Header.config';
import Overlay from 'Component/Overlay';
import { ChildrenType, MixType } from 'Type/Common';
import isMobile from 'Util/Mobile';

import './Sidebar.style.scss';

class Sidebar extends PureComponent {
    static propTypes = {
        isWithHeader: PropTypes.bool,
        title: PropTypes.string,
        onCloseButtonClick: PropTypes.func.isRequired,
        id: PropTypes.string.isRequired,
        changeHeaderState: PropTypes.func.isRequired,
        children: ChildrenType,
        showDimmer: PropTypes.func.isRequired,
        subtitle: PropTypes.string,
        mix: MixType,
    };

    static defaultProps = {
        isWithHeader: true,
        title: undefined,
        children: null,
        subtitle: undefined,
        mix: {},
    };

    onVisible = () => {
        const { changeHeaderState, title, showDimmer } = this.props;
        changeHeaderState({ name: POPUP, title });

        if (!isMobile.any()) {
            document.documentElement.classList.add('scrollDisabled');
        }

        showDimmer();
    };

    onHide = () => {
        document.documentElement.classList.remove('scrollDisabled');
    };

    renderHeader() {
        const { title, onCloseButtonClick } = this.props;

        return (
            <div block="Sidebar" elem="Header">
                <h4>{title}</h4>
                <button
                    key="close"
                    block="Header"
                    elem="Button"
                    mods={{ type: 'close', isVisible: true }}
                    onClick={onCloseButtonClick}
                    aria-label="Close"
                />
            </div>
        );
    }

    render() {
        const { isWithHeader, id, children, subtitle, mix } = this.props;

        return createPortal(
            <Overlay id={id} mix={{ block: 'Sidebar' }} onVisible={this.onVisible} onHide={this.onHide}>
                {isWithHeader && this.renderHeader()}
                <div block="Sidebar" elem="Content" mix={mix}>
                    {subtitle && (
                        <div block="Sidebar" elem="Subtitle">
                            {subtitle}
                        </div>
                    )}
                    {children && children}
                </div>
            </Overlay>,
            document.body,
        );
    }
}

export default Sidebar;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import CarouselWrapper from 'Component/CarouselWrapper';
import Link from 'Component/Link';
import ProductCard from 'Component/ProductCard/ProductCard.container';
import StyledSlickSlider from 'Component/StyledSlickSlider';
import RecentlyViewedUtil from 'Util/RecentlyViewed/RecentlyViewed';

import './RecentlyViewed.style.scss';

const CAROUSEL_PRODUCTS_TO_RENDER = 10;
const LISTING_RENDER_TYPE = 'listing';

export default class RecentlyViewed extends PureComponent {
    static propTypes = {
        getRecentlyViewed: PropTypes.func.isRequired,
        recentlyViewed: PropTypes.any.isRequired,
        renderType: PropTypes.string,
        title: PropTypes.string,
    };

    static defaultProps = {
        renderType: 'carousel',
    };

    componentDidMount() {
        const { getRecentlyViewed } = this.props;
        let productsSkus = [];
        const recentlySKUs = RecentlyViewedUtil.getLocalRecentlyViewed();

        if (recentlySKUs) {
            productsSkus = recentlySKUs.map((product) => product.sku);
        }

        productsSkus.length > 0 && getRecentlyViewed(productsSkus);
    }

    renderProducts(products) {
        const { renderType, title } = this.props;

        if (renderType === LISTING_RENDER_TYPE) {
            return (
                <div block="RecentlyViewed" elem="Container">
                    <h3 block="RecentlyViewed" elem="Header">
                        {title || __('Recently viewed products')}
                    </h3>
                    <ul block="RecentlyViewed" elem="List" mix={{ block: 'CategoryProductList', elem: 'Page' }}>
                        {products.map((product) => (
                            <ProductCard product={product} key={product.id} />
                        ))}
                    </ul>
                </div>
            );
        }

        return (
            <CarouselWrapper
                label="Recently Viewed Products"
                mix={{ block: 'RecentlyViewed', elem: 'Section' }}
                wrapperMix={{ block: 'RecentlyViewed', elem: 'Wrapper' }}
            >
                <div block="RecentlyViewed" elem="Carousel">
                    <h3 block="RecentlyViewed" elem="Header">
                        {title || __('Recently viewed products')}
                    </h3>
                    <Link block="RecentlyViewed" elem="ListingButton" to="/recently-viewed">
                        <span>{__('See all')}</span>
                    </Link>
                    <div block="CategoryProductList" elem="Page" mix={{ block: 'ProductListWidget', elem: 'Carousel' }}>
                        <StyledSlickSlider
                            alignCenter
                            block="ProductListCarousel"
                            infinite={false}
                            lazyLoad={false}
                            variableWidth
                        >
                            {products.slice(0, CAROUSEL_PRODUCTS_TO_RENDER).map((product) => (
                                <ProductCard product={product} withoutItemProps key={product.id} />
                            ))}
                        </StyledSlickSlider>
                    </div>
                </div>
            </CarouselWrapper>
        );
    }

    render() {
        const { recentlyViewed } = this.props;
        return (
            <>{recentlyViewed.recentlyViewed.length > 0 ? this.renderProducts(recentlyViewed.recentlyViewed) : ''}</>
        );
    }
}

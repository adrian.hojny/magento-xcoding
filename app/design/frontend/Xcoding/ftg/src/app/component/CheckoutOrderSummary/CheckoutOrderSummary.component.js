import SourceCheckoutOrderSummary from 'SourceComponent/CheckoutOrderSummary/CheckoutOrderSummary.component';

import CartItem from 'Component/CartItem';
import { SHIPPING_STEP } from 'Route/Checkout/Checkout.config';
import { roundPrice } from 'Util/Price';

class CheckoutOrderSummary extends SourceCheckoutOrderSummary {
    renderHeading() {
        const {
            totals: { items_qty },
        } = this.props;
        let text = '';
        const lastDigit = items_qty % 10;

        if (items_qty === 1) {
            text = __('%s product', items_qty);
        } else if (lastDigit > 1 && lastDigit < 5) {
            text = __('%s PRODUCT_PLURAL_1', items_qty);
        } else {
            text = __('%s PRODUCT_PLURAL_2', items_qty);
        }

        return (
            <div
                block="CheckoutOrderSummary"
                elem="Header"
                mix={{ block: 'CheckoutPage', elem: 'Heading', mods: { hasDivider: true } }}
            >
                <h3>{__('Order Summary')}</h3>
                {items_qty && (
                    <p block="CheckoutOrderSummary" elem="ItemsInCart">
                        {`${text} ${__('in cart')}`}
                    </p>
                )}
            </div>
        );
    }

    renderItem = (item) => {
        const {
            totals: { quote_currency_code },
            checkoutStep,
        } = this.props;

        const { item_id } = item;

        return <CartItem key={item_id} item={item} currency_code={quote_currency_code} checkoutStep={checkoutStep} />;
    };

    renderPriceLine(price, name, mods) {
        if (!price) {
            return null;
        }

        const {
            totals: { quote_currency_code },
        } = this.props;
        const priceString = quote_currency_code;

        return (
            <li block="CheckoutOrderSummary" elem="SummaryItem" mods={mods}>
                <strong block="CheckoutOrderSummary" elem="Text">
                    {name}
                </strong>
                <strong block="CheckoutOrderSummary" elem="Text">
                    {`${roundPrice(price)} ${priceString}`}
                </strong>
            </li>
        );
    }

    renderShippingInfo(price, name) {
        const {
            totals: { quote_currency_code },
        } = this.props;
        const priceString = quote_currency_code;

        return (
            <li block="CheckoutOrderSummary" elem="SummaryItem">
                <strong block="CheckoutOrderSummary" elem="Text">
                    {name}
                </strong>
                <strong block="CheckoutOrderSummary" elem="Text">
                    {price === 0 || price === null ? __('Free shipping') : `${roundPrice(price)} ${priceString}`}
                </strong>
            </li>
        );
    }

    renderTotals() {
        const {
            totals: { subtotal_incl_tax, grand_total },
            paymentTotals: {
                subtotal_incl_tax: payment_subtotal_incl_tax,
                grand_total: payment_grand_total,
                shipping_incl_tax,
                tax_amount,
            },
            checkoutStep,
            selectedShippingMethod,
        } = this.props;
        const isAvailableShippingMethod = !!selectedShippingMethod?.method_code;
        const shippingAmount = isAvailableShippingMethod ? selectedShippingMethod.amount : 0;

        return (
            <div block="CheckoutOrderSummary" elem="OrderTotals">
                <ul>
                    {this.renderPriceLine(
                        checkoutStep === SHIPPING_STEP
                            ? subtotal_incl_tax
                            : payment_subtotal_incl_tax || subtotal_incl_tax,
                        __('Cart Subtotal'),
                    )}
                    {checkoutStep !== SHIPPING_STEP
                        ? this.renderShippingInfo(shipping_incl_tax, __('Shipping'))
                        : isAvailableShippingMethod
                        ? this.renderShippingInfo(shippingAmount, __('Shipping'))
                        : null}
                    {this.renderCouponCode()}
                    {this.renderPriceLine(
                        checkoutStep === SHIPPING_STEP
                            ? subtotal_incl_tax + shippingAmount
                            : payment_grand_total || grand_total,
                        __('Order total'),
                    )}
                </ul>
            </div>
        );
    }
}

export default CheckoutOrderSummary;

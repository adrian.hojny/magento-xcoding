import { PureComponent } from 'react';
import Iframe from 'react-iframe';
import PropTypes from 'prop-types';

import { getStaticFilePath } from 'Util/Image/Image.js';

import './InpostGeowidget.style.scss';

class InpostGeowidget extends PureComponent {
    static propTypes = {
        onPointSelect: PropTypes.func.isRequired,
        selectedPoint: PropTypes.object,
    };

    static defaultProps = {
        selectedPoint: {},
    };

    state = { isDeliveryPointChanging: false };

    componentDidMount() {
        window.addEventListener('message', this.iframeEventListener);
    }

    componentWillUnmount() {
        window.removeEventListener('message', this.iframeEventListener);
    }

    iframeEventListener = ({ data }) => {
        const { onPointSelect } = this.props;

        if (data.source === 'inpost-widget' && data.payload.name) {
            onPointSelect(data.payload);
            this.setState({ isDeliveryPointChanging: false });
        }
    };

    onChangePointBtnClick = () => {
        this.setState({ isDeliveryPointChanging: true });
    };

    renderSelectedPoint() {
        const { selectedPoint } = this.props;
        const { isDeliveryPointChanging } = this.state;

        if (!selectedPoint || !selectedPoint.name) return null;

        const {
            name,
            address: { line1, line2 },
        } = selectedPoint;

        return (
            <div block="InpostGeowidget">
                <div block="InpostGeowidget" elem="Title">
                    {__('Selected delivery point')}
                </div>
                <div block="InpostGeowidget" elem="Point">
                    <div>
                        <strong>{name}</strong>
                        {`, ${line1}`}
                    </div>
                    <div>{line2}</div>
                </div>
                {!isDeliveryPointChanging && (
                    <button
                        block="Button"
                        mods={{ isHollow: true }}
                        mix={{ block: 'InpostGeowidget', elem: 'Button' }}
                        onClick={this.onChangePointBtnClick}
                    >
                        {__('Change delivery point')}
                    </button>
                )}
            </div>
        );
    }

    render() {
        const { isDeliveryPointChanging } = this.state;
        const { selectedPoint } = this.props;

        const showMap = isDeliveryPointChanging || !selectedPoint.name;

        return (
            <>
                {this.renderSelectedPoint()}
                {showMap && <Iframe url={getStaticFilePath('assets/iframe/inpost.html')} width="100%" height="470px" />}
            </>
        );
    }
}

export default InpostGeowidget;

import LazyLoad, { forceVisible } from 'react-lazyload';
import PropTypes from 'prop-types';

import { Image as SourceImage } from 'SourceComponent/Image/Image.component';
import { IMAGE_LOADED, IMAGE_LOADING, IMAGE_NOT_FOUND } from 'SourceComponent/Image/Image.config';

import LoaderPlaceholder from 'Component/LoaderPlaceholder';
import { getWebpImagePath } from 'Util/Image/Image';

export class Image extends SourceImage {
    static propTypes = {
        ...super.propTypes,
        itemProp: PropTypes.string,
        useWebp: PropTypes.bool,
        forceWithoutScrolling: PropTypes.bool,
    };

    static defaultProps = {
        ...super.defaultProps,
        itemProp: '',
        useWebp: false,
        forceWithoutScrolling: false,
    };

    renderSources() {
        const { src } = this.props;
        const { webpImagePath, sourceImageFormat } = getWebpImagePath(src);

        return (
            <>
                {webpImagePath && <source srcSet={webpImagePath} type="image/webp" />}
                <source srcSet={src} type={`image/${sourceImageFormat}`} />
            </>
        );
    }

    onLoad() {
        const { forceWithoutScrolling } = this.props;

        if (forceWithoutScrolling) {
            forceVisible();
        }

        this.setState({ imageStatus: IMAGE_LOADED });
    }

    renderImage() {
        const { useWebp, alt, src, isPlaceholder, style, itemProp } = this.props;
        const { imageStatus } = this.state;

        if (isPlaceholder) {
            return null;
        }

        switch (imageStatus) {
            case IMAGE_NOT_FOUND:
                return this.renderImageNotFound();
            case IMAGE_LOADED:
            case IMAGE_LOADING: {
                if (useWebp) {
                    return (
                        <>
                            {this.renderSources()}
                            <img
                                block="Image"
                                elem="Image"
                                src={src || ''}
                                alt={alt}
                                mods={{ isLoading: imageStatus === IMAGE_LOADING }}
                                style={style}
                                onLoad={this.onLoad}
                                onError={this.onError}
                                itemProp={itemProp}
                            />
                            {imageStatus === IMAGE_LOADING && <LoaderPlaceholder />}
                        </>
                    );
                }

                return (
                    <img
                        block="Image"
                        elem="Image"
                        src={src || ''}
                        alt={alt}
                        mods={{ isLoading: imageStatus === IMAGE_LOADING }}
                        style={style}
                        onLoad={this.onLoad}
                        onError={this.onError}
                        itemProp={itemProp}
                    />
                );
            }
            default:
                return null;
        }
    }

    render() {
        const { useWebp, ratio, mix, isPlaceholder, wrapperSize, src, imageRef, className } = this.props;

        const { imageStatus } = this.state;

        if (useWebp) {
            return (
                <LazyLoad once offset={200} placeholder={<LoaderPlaceholder />}>
                    <picture
                        block="Image"
                        ref={imageRef}
                        mods={{
                            ratio,
                            imageStatus,
                            isPlaceholder,
                            hasSrc: !!src,
                        }}
                        mix={mix}
                        style={wrapperSize}
                        // eslint-disable-next-line react/forbid-dom-props
                        className={className}
                    >
                        {this.renderImage()}
                    </picture>
                </LazyLoad>
            );
        }

        return (
            <LazyLoad once offset={200} placeholder={<LoaderPlaceholder />}>
                <div
                    block="Image"
                    ref={imageRef}
                    mods={{
                        ratio,
                        imageStatus,
                        isPlaceholder,
                        hasSrc: !!src,
                    }}
                    mix={mix}
                    style={wrapperSize}
                    // eslint-disable-next-line react/forbid-dom-props
                    className={className}
                >
                    {this.renderImage()}
                </div>
            </LazyLoad>
        );
    }
}

export default Image;

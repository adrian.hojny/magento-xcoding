import React from 'react';

import SourceCartItem from 'SourceComponent/CartItem/CartItem.component';

import CartItemPrice from 'Component/CartItemPrice';
import Image from 'Component/Image/Image.container';
import Link from 'Component/Link';
import Loader from 'Component/Loader';
import ProductWishlistButton from 'Component/ProductWishlistButton';
import ProgressBar from 'Component/ProgressBar';
import { BILLING_STEP, SHIPPING_STEP } from 'Route/Checkout/Checkout.config';

export const REMOVE_TIMEOUT = 8000;

export default class CartItem extends SourceCartItem {
    renderWrapper() {
        const { linkTo } = this.props;
        return (
            <figure block="CartItem" elem="Wrapper">
                <Link to={linkTo} block="CartItem" elem="Link">
                    {' '}
                    {this.renderImage()}{' '}
                </Link>
                {this.renderContent()}
                {this.renderActions()}
            </figure>
        );
    }

    renderProductName() {
        const {
            item: {
                product: { name },
            },
            linkTo,
        } = this.props;

        return (
            <p block="CartItem" elem="Heading">
                <Link to={linkTo} block="CartItem" elem="Link">
                    {' '}
                    {name}{' '}
                </Link>
            </p>
        );
    }

    renderImage() {
        const {
            item: {
                product: {
                    name,
                    small_image: { url },
                },
            },
        } = this.props;

        return (
            <>
                <Image
                    src={url}
                    alt={__('Product %s thumbnail.', name)}
                    mix={{
                        block: 'CartItem',
                        elem: 'Picture',
                    }}
                    ratio="custom"
                    useWebp
                />
                <img style={{ display: 'none' }} alt={name} src={url} itemProp="image" />
            </>
        );
    }

    renderProductPrice() {
        const {
            isLikeTable,
            currency_code,
            item: { row_total, discount_amount },
        } = this.props;

        return (
            <CartItemPrice
                row_total={row_total - discount_amount}
                currency_code={currency_code}
                regular_price={this.calculateRegularPrice()}
                minimal_price={this.calculateMinimalPrice()}
                discountPercent={this.getDiscountPercent()}
                mix={{
                    block: 'CartItem',
                    elem: 'Price',
                    mods: { isLikeTable },
                }}
            />
        );
    }

    renderProductDetails() {
        const {
            item: { customizable_options, bundle_options, sku },
        } = this.props;

        return (
            <>
                <p block="CartItem" elem="HeadingTop">
                    {sku}
                </p>
                {this.renderProductName()}

                <div block="CartItem" elem="OptionsWrapper">
                    <div block="CartItem" elem="OptionsSection">
                        {this.renderProductOptions(customizable_options)}
                        {this.renderProductOptions(bundle_options)}
                        {this.renderProductConfigurations()}
                        {this.renderQtyAction()}
                    </div>
                    <div block="CartItem" elem="PriceWrapper">
                        {this.renderProductPrice()}
                    </div>
                </div>
                <div>{this.renderLowStock()}</div>
            </>
        );
    }

    renderProductConfigurationOption = ([key, attribute]) => {
        const {
            item: {
                product: { configurable_options },
            },
        } = this.props;

        const { attribute_code, attribute_value } = attribute;

        if (!Object.keys(configurable_options).includes(key)) {
            return null;
        }
        const {
            [attribute_code]: {
                // configurable option attribute
                attribute_options: {
                    [attribute_value]: {
                        // attribute option value label
                        label,
                    },
                },
            },
        } = configurable_options;

        return (
            <li key={attribute_code} aria-label={attribute_code} block="CartItem" elem="Option">
                {`${attribute?.attribute_label || attribute_code}:`}
                <span>{label}</span>
            </li>
        );
    };

    renderProductConfigurations() {
        const {
            item: {
                product: { configurable_options, variants },
            },
            isLikeTable,
            getCurrentProduct,
        } = this.props;

        if (!variants || !configurable_options) {
            return null;
        }

        const { attributes = {} } = getCurrentProduct() || {};

        if (!Object.entries(attributes).length) {
            return null;
        }

        return (
            <ul block="CartItem" elem="Options" mods={{ isLikeTable }}>
                {Object.entries(attributes).map(this.renderProductConfigurationOption)}
            </ul>
        );
    }

    renderSelectElements() {
        const { getCurrentProduct } = this.props;
        const selectedProduct = getCurrentProduct();
        const stockItems = selectedProduct?.salable_qty || 0;
        const options = [];
        const maxElements = stockItems > 10 ? 10 : stockItems === 0 ? 1 : stockItems;

        for (let i = 1; i <= maxElements; i++) {
            options.push(
                <option key={i} value={i}>
                    {i}
                </option>,
            );
        }

        return options;
    }

    handleQtyChange = (event) => {
        const { handleChangeQuantity } = this.props;

        handleChangeQuantity(event.target.value);
    };

    renderQtyAction() {
        const {
            item: { qty },
            checkoutStep,
        } = this.props;
        const renderQtyField = !(checkoutStep === SHIPPING_STEP || checkoutStep === BILLING_STEP);

        return (
            <div
                block="CartItem"
                elem="QtyWrapper"
                mix={{ block: 'CartItem', elem: 'QtySelectWrapper' }}
                onClick={(e) => e.stopPropagation()}
            >
                <span block="CartItem" elem="QtyLabel">
                    {`${__('Number')}: `}
                </span>
                <div block="CartItem" elem="QtyField">
                    {renderQtyField ? (
                        <select
                            block="CartItem"
                            elem="QtyFieldSelect"
                            id="item_qty"
                            name="item_qty"
                            onChange={this.handleQtyChange}
                            defaultValue={qty}
                        >
                            {this.renderSelectElements()}
                        </select>
                    ) : (
                        qty
                    )}
                </div>
            </div>
        );
    }

    renderLowStock() {
        const variantStockFlag = this.getVariant().is_low_in_stock;

        if (variantStockFlag) {
            return (
                <span block="CartItem" elem="Last">
                    {__('Last item!')}
                </span>
            );
        }

        return null;
    }

    renderRemoveStatus() {
        const { addBackToCart } = this.props;

        return (
            <div block="CartItem" elem="RemoveStatus">
                <p>{__('The product has been removed from the cart.')}</p>
                <button onClick={addBackToCart}>{__('Undo')}</button>
            </div>
        );
    }

    renderContent() {
        const {
            isLikeTable,
            item: { removeStatus },
            isLoading,
        } = this.props;

        return (
            <figcaption block="CartItem" elem="Content" mods={{ isLikeTable }}>
                {removeStatus && !isLoading ? this.renderRemoveStatus() : this.renderProductDetails()}
            </figcaption>
        );
    }

    getVariant() {
        const {
            item: {
                sku,
                product: { variants },
                product,
            },
        } = this.props;

        if (variants.length === 0) {
            return product;
        }

        return variants.find((variant) => variant.sku === sku);
    }

    calculateRegularPrice() {
        const {
            item: { qty },
        } = this.props;

        return this.getVariant().price_range.minimum_price.regular_price.value * qty;
    }

    getDiscountPercent() {
        return this.getVariant().price_range.minimum_price.discount.percent_off;
    }

    calculateMinimalPrice() {
        const {
            item: { qty },
        } = this.props;

        return this.getVariant().price_range.minimum_price.final_price.value * qty;
    }

    renderActions() {
        const {
            isEditing,
            isLikeTable,
            handleRemoveItem,
            showActions,
            item: { removeStatus, product },
        } = this.props;

        if (removeStatus) return null;

        return (
            <div
                block="CartItem"
                elem="Actions"
                mods={{ isEditing, isLikeTable, showActions }}
                onClick={(e) => e.stopPropagation()}
            >
                <button
                    block="CartItem"
                    id="RemoveItem"
                    name="RemoveItem"
                    elem="Delete"
                    aria-label="Remove item from cart"
                    onClick={handleRemoveItem}
                >
                    <span>{__('Delete')}</span>
                </button>
                <ProductWishlistButton product={product} configurableVariantIndex={0} />
            </div>
        );
    }

    render() {
        const {
            isLoading,
            item: { removeStatus },
        } = this.props;

        return (
            <li block="CartItem">
                <Loader isLoading={isLoading} />
                <ProgressBar timeout={REMOVE_TIMEOUT} isVisible={!!removeStatus && !isLoading} />
                {this.renderWrapper()}
            </li>
        );
    }
}

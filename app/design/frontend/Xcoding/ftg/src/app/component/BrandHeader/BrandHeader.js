import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import BrandVideo from 'Component/BrandVideo';
import CategoryItemsCount from 'Component/CategoryItemsCount';
import Html from 'Component/Html';
import Image from 'Component/Image';
import Loader from 'Component/Loader';
import NewProductCarousel from 'Component/NewProductsCarousel';
import { PREMIUM } from 'Component/Router/Router.component';
import BrandsQuery from 'Query/Brands.query';
import NoMatch from 'Route/NoMatch';
import { LocationType, MatchType } from 'Type/Common';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import isMobile from 'Util/Mobile';
import { getIndexedProducts } from 'Util/Product';
import { getProductList } from 'Util/Product/ProductList';
import { fetchQuery } from 'Util/Request';

import './BrandHeader.style.scss';

class BrandHeader extends PureComponent {
    static propTypes = {
        location: LocationType.isRequired,
        match: MatchType.isRequired,
        children: PropTypes.node.isRequired,
        currentPage: PropTypes.number,
        brandDescriptionSuffix: PropTypes.string.isRequired,
        updateMetaFromCategory: PropTypes.func.isRequired,
        defaultTitle: PropTypes.string.isRequired,
        isMatchingListFilter: PropTypes.bool.isRequired,
        updateLinks: PropTypes.func.isRequired,
        removeLinks: PropTypes.func.isRequired,
    };

    static defaultProps = {
        currentPage: 1,
        brandDescriptionSuffix: '',
        defaultTitle: '',
    };

    constructor(props) {
        super(props);
        this.state = {
            brand: null,
            isLoading: true,
            noMatch: false,
            resizeFlag: false,
            newProducts: [],
        };
    }

    componentDidMount() {
        const {
            match: {
                params: { brandName },
            },
        } = this.props;

        window.addEventListener('resize', this.handleResize);
        this.updateData(brandName);
        this.getNewProducts();
    }

    componentDidUpdate(prevProps) {
        const {
            match: {
                params: { brandName },
            },
        } = this.props;
        const {
            match: {
                params: { brandName: prevBandName },
            },
        } = prevProps;

        if (brandName !== prevBandName) {
            this.updateData(brandName);
        }
    }

    componentWillUnmount() {
        const { removeLinks } = this.props;

        removeLinks();

        window.removeEventListener('resize', this.handleResize);
    }

    getNewProducts() {
        const {
            match: {
                params: { brandName },
            },
        } = this.props;
        const options = {
            args: {
                filter: {
                    brandName,
                },
                currentPage: 1,
                pageSize: 20,
            },
        };

        getProductList(options).then(({ products: { items } }) => {
            if (!items.length) {
                return;
            }

            // need to be indexed to properly work with ProductCard component
            const indexedProducts = getIndexedProducts(items);

            // sort desc by id
            const sortedProducts = indexedProducts.sort((a, b) => b.id - a.id);

            this.setState({ newProducts: sortedProducts });
        });
    }

    updateMeta = (brand) => {
        const { location, brandDescriptionSuffix, updateMetaFromCategory, defaultTitle, updateLinks } = this.props;

        const { meta_title, title, meta_description, description, meta_keywords, mf_alternate_urls } = brand;
        const customMetaDesc = __('Check the %s in the %s store', title || meta_title, defaultTitle);
        const metaDesc = meta_description === title ? customMetaDesc : meta_description;
        const canonical_url = `${window.location.origin}${location.pathname}`;

        const metaData = {
            meta_title,
            name: title,
            meta_description: `${metaDesc} ${brandDescriptionSuffix}`,
            description,
            meta_keyword: meta_keywords,
            canonical_url,
        };

        updateMetaFromCategory(metaData);
        updateLinks(mf_alternate_urls);
    };

    updateData = (brandName) => {
        fetchQuery(BrandsQuery.getBrandPageQuery(brandName)).then((data) => {
            const brand = data?.brands?.[0];
            if (brand) {
                this.setState({ brand, isLoading: false, noMatch: false }, () => {
                    this.updateMeta(brand);
                });
            } else {
                this.setState({ isLoading: false, noMatch: true });
            }
        });
    };

    handleResize = () => {
        this.setState(({ resizeFlag }) => ({ resizeFlag: !resizeFlag }));
    };

    renderBottomSection() {
        const { brand } = this.state;
        const { bottom_description, slider_image, bottom_image_url, bottom_video_url, title } = brand;

        if (!bottom_description && !bottom_image_url) {
            return null;
        }

        return (
            <BrandVideo
                description={bottom_description}
                logo={slider_image}
                videoPlaceholder={bottom_image_url}
                videoSrc={bottom_video_url}
                brandName={title}
            />
        );
    }

    renderItemsCount(isVisibleOnMobile = false) {
        const { isMatchingListFilter } = this.props;
        const isPrm = BrowserDatabase.getItem(PREMIUM);

        if (!isPrm && isVisibleOnMobile && !isMobile.any()) {
            return null;
        }

        if (!isPrm && !isVisibleOnMobile && isMobile.any()) {
            return null;
        }

        return <CategoryItemsCount isMatchingListFilter={isMatchingListFilter} />;
    }

    renderHeader() {
        const { currentPage } = this.props;
        const { brand } = this.state;
        const { description, image_url, title, slider_image, mobile_image_url, small_image_alt } = brand;
        const bannerUrl = isMobile.width() ? mobile_image_url : image_url;

        return (
            <>
                {bannerUrl && (
                    <div block="BrandHeader" elem="image">
                        <Image src={bannerUrl} alt={`${title} brand banner`} useWebp />
                        {slider_image ? (
                            <div block="BrandHeader" elem="logo">
                                <Image src={slider_image} alt={small_image_alt} useWebp />
                            </div>
                        ) : (
                            <div block="BrandHeader" elem="logo-empty" />
                        )}
                    </div>
                )}
                <div block="BrandHeader" elem="top" mods={{ withoutBanner: !bannerUrl }}>
                    <div block="BrandHeader" elem="title">
                        <h1>{title}</h1>
                    </div>
                    <div block="BrandHeader" elem="DetailsWrapper">
                        {this.renderItemsCount()}
                    </div>
                </div>
                {/* eslint-disable-next-line react/no-danger */}
                {currentPage === 1 && (
                    <div block="BrandHeader" elem="description">
                        {description && <Html content={description} />}
                    </div>
                )}
            </>
        );
    }

    renderGenderButton = ({ id, name, image_url, url }) => {
        return (
            <div key={id} block="GenderButtons" elem="Wrapper">
                <a href={url} block="GenderButtons" elem="Button">
                    <div block="GenderButtons" elem="Image">
                        <Image src={image_url} alt={name} />
                        {/* <img src={image_url} alt={name} /> */}
                    </div>
                    <span block="GenderButtons" elem="Title">
                        {name}
                    </span>
                    <span block="GenderButtons" elem="Icon" />
                </a>
            </div>
        );
    };

    renderSection1 = (key) => {
        const { brand } = this.state;
        const { box_1_name, box_2_name, box_3_name } = brand;

        if (!box_1_name && !box_2_name && !box_3_name) {
            return null;
        }

        const genderButtons = [
            {
                id: 0,
                name: box_1_name,
                image_url: brand.box_1_image_url ?? '',
                url: brand.box_1_url ?? '',
            },
            {
                id: 1,
                name: box_2_name,
                image_url: brand.box_2_image_url ?? '',
                url: brand.box_2_url ?? '',
            },
            {
                id: 2,
                name: box_3_name,
                image_url: brand.box_3_image_url ?? '',
                url: brand.box_3_url ?? '',
            },
        ];

        return (
            <div key={key} block="GenderButtons" elem="Container">
                {genderButtons.map((genderButton) => this.renderGenderButton(genderButton))}
            </div>
        );
    };

    renderSection2 = (key) => {
        const {
            brand: { s2_title },
            newProducts,
        } = this.state;

        return <NewProductCarousel key={key} products={newProducts} title={s2_title} />;
    };

    renderSection3 = (key) => {
        const {
            brand: { s3_html },
        } = this.state;

        if (!s3_html) {
            return null;
        }

        return (
            <div key={key} block="CarouselWrapper DiscoverBrand">
                <Html content={s3_html} />
            </div>
        );
    };

    renderSections() {
        const { brand } = this.state;
        let { s1_sort_order, s2_sort_order, s3_sort_order } = brand;

        if (s1_sort_order === 0 && s2_sort_order === 0 && s3_sort_order === 0) {
            s1_sort_order = 0;
            s2_sort_order = 1;
            s3_sort_order = 2;
        }

        if (s1_sort_order === 0 && s2_sort_order === 0 && s3_sort_order > 0) {
            s1_sort_order = 0;
            s2_sort_order = 1;
            s3_sort_order = 2;
        }

        if (s1_sort_order === 0 && s2_sort_order > 0 && s3_sort_order === 0) {
            s1_sort_order = 0;
            s2_sort_order = 2;
            s3_sort_order = 1;
        }

        if (s1_sort_order > 0 && s2_sort_order === 0 && s3_sort_order === 0) {
            s1_sort_order = 2;
            s2_sort_order = 0;
            s3_sort_order = 1;
        }

        // js sorts dynamic object keys automatically, sort order is always ascending
        const sectionRenderMap = {
            [s1_sort_order]: this.renderSection1,
            [s2_sort_order]: brand.s2_visible ? this.renderSection2 : null,
            [s3_sort_order]: this.renderSection3,
        };

        return Object.keys(sectionRenderMap).map((key) => {
            if (typeof sectionRenderMap[key] === 'function') {
                return sectionRenderMap[key](key);
            }

            return sectionRenderMap[key];
        });
    }

    render() {
        const { brand, isLoading, noMatch } = this.state;
        const { children, currentPage } = this.props;

        if (noMatch) {
            return <NoMatch {...this.props} />;
        }

        return (
            <div block="BrandHeader">
                <Loader isLoading={isLoading} />

                {brand && this.renderHeader()}

                {brand && this.renderSections()}

                {children}

                {brand && currentPage === 1 && this.renderBottomSection()}
            </div>
        );
    }
}

export default BrandHeader;

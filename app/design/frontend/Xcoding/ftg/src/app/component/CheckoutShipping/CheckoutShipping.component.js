import SourceCheckoutShipping from 'SourceComponent/CheckoutShipping/CheckoutShipping.component';

import CheckoutDeliveryOptions from 'Component/CheckoutDeliveryOptions/CheckoutDeliveryOptions.container';
import Field from 'Component/Field';
import Form from 'Component/Form';
import Loader from 'Component/Loader';
import { CLICKED_CATEGORIES } from 'Component/ProductList/ProductList.component';
import { SHIPPING_STEP } from 'Route/Checkout/Checkout.config';
import { TotalsType } from 'Type/MiniCart';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { getProductsForGTM } from 'Util/Product/Product';

import './CheckoutShipping.style';

class CheckoutShipping extends SourceCheckoutShipping {
    static propTypes = {
        ...super.propTypes,
        totals: TotalsType.isRequired,
    };

    renderDelivery() {
        const {
            shippingMethods,
            onShippingMethodSelect,
            onAdditionalInformationChange,
            shippingAdditionalInformation,
            selectedShippingMethod,
            setShippingMethod,
        } = this.props;

        return (
            <CheckoutDeliveryOptions
                shippingMethods={shippingMethods}
                onShippingMethodSelect={onShippingMethodSelect}
                onAdditionalInformationChange={onAdditionalInformationChange}
                shippingAdditionalInformation={shippingAdditionalInformation}
                selectedShippingMethod={selectedShippingMethod}
                setShippingMethod={setShippingMethod}
            />
        );
    }

    componentDidMount() {
        const { totals } = this.props;
        const categoriesData = BrowserDatabase.getItem(CLICKED_CATEGORIES) || {};

        const productsGTM = getProductsForGTM(totals.items, categoriesData);
        gtag('event', 'begin_checkout', {
            items: Array.from(productsGTM),
            checkout_step: 1,
        });
    }

    handleClick = () => {
        const { totals, selectedShippingMethod } = this.props;
        const categoriesData = BrowserDatabase.getItem(CLICKED_CATEGORIES) || {};

        // Step 2

        const productsGTM = getProductsForGTM(totals.items, categoriesData);
        gtag('event', 'checkout_progress', {
            items: Array.from(productsGTM),
            checkout_step: 2,
            checkout_option: selectedShippingMethod?.method_title || '',
        });
    };

    renderActions() {
        const { selectedShippingMethod } = this.props;

        return (
            <div block="Checkout" elem="StickyButtonWrapper">
                <button
                    type="submit"
                    block="Button"
                    mods={{ green: true }}
                    disabled={!selectedShippingMethod}
                    mix={{ block: 'CheckoutShipping', elem: 'Button' }}
                    onClick={this.handleClick}
                >
                    {__('Proceed to billing')}
                </button>
            </div>
        );
    }

    renderCheckbox() {
        const { subscribeNewsletter, onSubscribeNewsletterChange, termsData } = this.props;

        return (
            <div block="CheckoutShipping" elem="CheckboxContainer">
                {termsData?.content && (
                    <Field
                        type="checkbox"
                        label={<div dangerouslySetInnerHTML={{ __html: termsData.content }} />}
                        id="sneaker_terms"
                        name="sneaker_terms"
                        validation={['isChecked']}
                    />
                )}

                <Field
                    type="checkbox"
                    value={subscribeNewsletter}
                    label={__('Subscribe to newsletter')}
                    id="subscribe_newsletter"
                    name="subscribe_newsletter"
                    onChange={onSubscribeNewsletterChange}
                />
            </div>
        );
    }

    render() {
        const { onShippingSuccess, onShippingError, isLoading } = this.props;

        return (
            <Form
                id={SHIPPING_STEP}
                mix={{ block: 'CheckoutShipping' }}
                onSubmitError={onShippingError}
                onSubmitSuccess={onShippingSuccess}
            >
                {this.renderAddressBook()}
                {this.renderCheckbox()}
                <div>
                    <Loader isLoading={isLoading} />
                    {this.renderDelivery()}
                    {this.renderActions()}
                </div>
            </Form>
        );
    }
}

export default CheckoutShipping;

/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import { connect } from 'react-redux';

import {
    CheckoutBillingContainer as SourceCheckoutBillingContainer,
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
} from 'SourceComponent/CheckoutBilling/CheckoutBilling.container';

import {
    BRAINTREE,
    KLARNA,
    PRZELEWY24,
    PRZELEWY24_CARD,
    STRIPE,
} from 'Component/CheckoutPayments/CheckoutPayments.config';
import { CLICKED_CATEGORIES } from 'Component/ProductList/ProductList.component';
import { BLIK_CODE_NAME, P24_INNER_METHODS_NAME } from 'Component/Przelewy24/Przelewy24.config';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { getProductsForGTM } from 'Util/Product/Product';

export const TOTALS = 'totals';
export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    p24formValues: state.P24Reducer.formValues,
    p24formGroupsValidity: state.P24Reducer.formGroupsValidity,
});

export class CheckoutBillingContainer extends SourceCheckoutBillingContainer {
    containerFunctions = {
        onBillingSuccess: this.onBillingSuccess.bind(this),
        onBillingError: this.onBillingError.bind(this),
        onAddressSelect: this.onAddressSelect.bind(this),
        onSameAsShippingChange: this.onSameAsShippingChange.bind(this),
        onPaymentMethodSelect: this.onPaymentMethodSelect.bind(this),
        showPopup: this.showPopup.bind(this),
        onCompanyNameChange: this.onCompanyNameChange.bind(this),
        onCompanyTaxIdNumberChange: this.onCompanyTaxIdNumberChange.bind(this),
    };

    constructor(props) {
        super(props);

        const { paymentMethods, customer } = props;
        const [method] = paymentMethods;
        const { code: paymentMethod } = method || {};

        this.state = {
            isSameAsShipping: this.isSameShippingAddress(customer),
            selectedCustomerAddressId: 0,
            prevPaymentMethods: paymentMethods,
            companyDetails: {
                companyName: '',
                companyVatId: '',
            },
            paymentMethod,
        };
    }

    componentDidMount() {
        BrowserDatabase.setItem(this.props.totals, TOTALS);
    }

    saveCheckoutGAData = () => {
        const { totals, paymentMethods } = this.props;
        const { paymentMethod } = this.state;
        const categoriesData = BrowserDatabase.getItem(CLICKED_CATEGORIES) || {};
        const method = paymentMethods.find((method) => method.code === paymentMethod);

        const productsGTM = getProductsForGTM(totals.items, categoriesData);
        gtag('event', 'checkout_progress', {
            items: Array.from(productsGTM),
            checkout_step: 3,
            checkout_option: method?.title || '',
        });
    };

    onBillingSuccess(fields, asyncData) {
        const { savePaymentInformation } = this.props;
        const { companyDetails } = this.state;
        const address = this._getAddress(fields);
        const paymentMethod = this._getPaymentData(asyncData, fields);

        this.saveCheckoutGAData();

        savePaymentInformation({
            billing_address: {
                ...address,
                company: companyDetails.companyName,
                vat_id: companyDetails.companyVatId,
            },
            paymentMethod,
        });
    }

    onCompanyNameChange(value) {
        this.setState(({ companyDetails }) => ({
            companyDetails: {
                ...companyDetails,
                companyName: value,
            },
        }));
    }

    onCompanyTaxIdNumberChange(value) {
        this.setState(({ companyDetails }) => ({
            companyDetails: {
                ...companyDetails,
                companyVatId: value,
            },
        }));
    }

    _getPaymentData(asyncData, fields = {}) {
        const { paymentMethod: code } = this.state;
        const { p24formValues } = this.props;
        const { [BLIK_CODE_NAME]: blik, [P24_INNER_METHODS_NAME]: method } = p24formValues;

        switch (code) {
            case BRAINTREE:
                const [{ nonce }] = asyncData;

                return {
                    code,
                    additional_data: {
                        payment_method_nonce: nonce,
                        is_active_payment_token_enabler: false,
                    },
                };
            case STRIPE:
                const [{ token, handleAuthorization }] = asyncData;
                if (token === null) {
                    return false;
                }

                return {
                    code,
                    additional_data: {
                        cc_stripejs_token: token,
                        cc_save: false,
                    },
                    handleAuthorization,
                };
            case KLARNA:
                const [{ authorization_token }] = asyncData;
                return {
                    code,
                    additional_data: {
                        authorization_token,
                    },
                };
            case PRZELEWY24:
                return {
                    code,
                    additional_data: {
                        przelewy24: {
                            method: blik ? 154 : method,
                            blik,
                        },
                    },
                };
            case PRZELEWY24_CARD:
                return {
                    code,
                    additional_data: {
                        przelewy24: {
                            method,
                        },
                    },
                };
            default:
                return { code };
        }
    }
}

export default connect(mapStateToProps, sourceMapDispatchToProps)(CheckoutBillingContainer);

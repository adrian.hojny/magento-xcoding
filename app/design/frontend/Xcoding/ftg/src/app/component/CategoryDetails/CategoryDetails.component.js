import React from 'react';

import SourceCategoryDetails from 'SourceComponent/CategoryDetails/CategoryDetails.component';

import './CategoryDetails.extend.style';

export * from 'SourceComponent/CategoryDetails/CategoryDetails.component';

class CategoryDetails extends SourceCategoryDetails {
    render() {
        return (
            <div block="CategoryDetails" elem="Description">
                {this.renderCategoryName()}
            </div>
        );
    }
}

export default CategoryDetails;

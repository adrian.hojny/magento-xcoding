import { connect } from 'react-redux';

import ProductReviewList from 'Component/ProductReviewList/ProductReviewList.component';
import { showNotification } from 'Store/Notification/Notification.action';

export const ReviewDispatcher = import(
    /* webpackMode: "lazy", webpackChunkName: "dispatchers" */
    'Store/Review/Review.dispatcher'
);

export const mapDispatchToProps = (dispatch) => ({
    voteHelpfulness: (options) =>
        ReviewDispatcher.then(({ default: dispatcher }) => dispatcher.addHelpfulnessVote(dispatch, options)),
    showNotification: (type, message) => dispatch(showNotification(type, message)),
});

export default connect(undefined, mapDispatchToProps)(ProductReviewList);

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import TextPlaceholder from 'Component/TextPlaceholder/TextPlaceholder.component';

const ONE_DAY_SECONDS = 60 * 60 * 24;

class ProductCardComingSoon extends PureComponent {
    static propTypes = {
        product: PropTypes.any.isRequired,
        comingSoonSection: PropTypes.bool,
    };

    constructor(props) {
        super(props);
        this.state = {
            timeLeft: this.calculateTimeLeft(),
        };
    }

    componentDidMount() {
        const {
            product: { comming_soon_date, comming_soon },
        } = this.props;

        if (!comming_soon_date || !comming_soon) {
            return;
        }
        this.timer && clearInterval(this.timer);
        this.timer = setInterval(() => {
            this.setState(({ timeLeft }) => ({ timeLeft: timeLeft - 1 }));
        }, 1000);
    }

    componentWillUnmount() {
        this.timer && clearInterval(this.timer);
    }

    calculateTimeLeft() {
        const {
            product: { comming_soon_date, comming_soon },
        } = this.props;

        if (!comming_soon_date || !comming_soon) {
            return -1;
        }

        const difference = (+new Date(comming_soon_date) - +new Date()) / 1000;

        return difference;
    }

    renderMainDetails() {
        const {
            product: { name },
        } = this.props;

        return (
            <p block="ProductCard" elem="Name" mods={{ isLoaded: !!name }}>
                <TextPlaceholder content={name} length="medium" />
            </p>
        );
    }

    renderDate() {
        const { timeLeft } = this.state;

        const {
            product: { comming_soon_date, sku },
            comingSoonSection,
        } = this.props;

        if (!comming_soon_date) {
            return null;
        }

        if (timeLeft < 0) {
            return null;
        }
        const days = Math.floor(timeLeft / ONE_DAY_SECONDS);

        if ((days >= 3 && !comingSoonSection) || (comingSoonSection && days >= 100)) {
            const date = new Date(comming_soon_date);
            return (
                <div block="ProductCard" elem="Coming-soon-date">
                    <div>{__('Coming soon')}</div>
                    <div>
                        {`${`0${date.getDate()}`.slice(-2)}.${`0${date.getMonth() + 1}`.slice(
                            -2,
                        )}.${date.getFullYear()}`}
                    </div>
                </div>
            );
        }

        const hours = Math.floor((timeLeft / (60 * 60)) % 24);
        const minutes = Math.floor((timeLeft / 60) % 60);
        const seconds = Math.floor(timeLeft % 60);
        return (
            <div block="ProductCard" elem="Coming-soon-countDown">
                <div block="ProductCard" elem="Coming-soon-countDown-element">
                    {days}
                    <div block="ProductCard" elem="Coming-soon-countDown-text">
                        {__('Days')}
                    </div>
                </div>

                <div block="ProductCard" elem="Coming-soon-countDown-element">
                    {hours}
                    <div block="ProductCard" elem="Coming-soon-countDown-text">
                        {__('Hours')}
                    </div>
                </div>

                <div block="ProductCard" elem="Coming-soon-countDown-element">
                    {minutes}
                    <div block="ProductCard" elem="Coming-soon-countDown-text">
                        {__('Minutes')}
                    </div>
                </div>

                <div block="ProductCard" elem="Coming-soon-countDown-element">
                    {seconds}
                    <div block="ProductCard" elem="Coming-soon-countDown-text">
                        {__('Seconds')}
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return <>{this.renderDate()}</>;
    }
}

export default ProductCardComingSoon;

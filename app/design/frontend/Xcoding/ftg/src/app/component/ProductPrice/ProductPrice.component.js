import React from 'react';
import PropTypes from 'prop-types';

import SourceProductPrice from 'SourceComponent/ProductPrice/ProductPrice.component';

import { roundPrice } from 'Util/Price';

import './ProductPrice.extend.style.scss';

export * from 'SourceComponent/ProductPrice/ProductPrice.component';

const priceDate = new Date();
const nextMonday = (1 + 7 - priceDate.getDay()) % 7;
priceDate.setDate(priceDate.getDate() + nextMonday);

const defaultPriceDate = priceDate.toISOString();

class ProductPrice extends SourceProductPrice {
    static propTypes = {
        ...SourceProductPrice.propTypes,
        showPriceDiscount: PropTypes.bool,
        showDiscountInfo: PropTypes.bool,
    };

    static defaultProps = {
        ...SourceProductPrice.defaultProps,
        showPriceDiscount: false,
        showDiscountInfo: false,
    };

    renderPriceDiscount() {
        const { discountPercentage, showPriceDiscount } = this.props;

        if (!showPriceDiscount || discountPercentage <= 0) {
            return null;
        }

        return (
            <span block="ProductPrice" elem="DiscountPercentage">
                {`-${Math.round(discountPercentage)}%`}
            </span>
        );
    }

    renderOldPrice() {
        const { roundedRegularPrice, discountPercentage, currency } = this.props;

        return (
            <del
                block="ProductPrice"
                elem="HighPrice"
                mods={{ isVisible: discountPercentage > 0 }}
                aria-label={__('Old product price')}
            >
                {`${roundedRegularPrice} ${currency}`}
            </del>
        );
    }

    renderDiscountInfo() {
        const { showDiscountInfo, discountPercentage, priceDifference, currency } = this.props;

        if (!showDiscountInfo || discountPercentage <= 0) return null;

        return (
            <span block="ProductPrice" elem="DiscountInfo">
                {`${__('You save')} ${priceDifference} ${currency} (-${Math.round(discountPercentage)}%)`}
            </span>
        );
    }

    render() {
        const {
            price: { minimum_price: { final_price, regular_price } = {} } = {},
            isSchemaRequired,
            formatedCurrency,
            currency,
            mix,
            discountPercentage,
            specialToDate,
        } = this.props;

        const isDiscounted = discountPercentage > 0;

        let newFormatedCurrency = formatedCurrency;
        let newCurrency = currency;

        if (!final_price || !regular_price) {
            return this.renderPlaceholder();
        }

        if (final_price.value === 0) {
            newFormatedCurrency = roundPrice(regular_price.value);
            newCurrency = regular_price.currency;
        }

        if (regular_price.value === 0) {
            newFormatedCurrency = roundPrice(final_price.value);
            newCurrency = final_price.currency;
        }

        const [baseSplitFinalPrice, centsSplitFinalPrice] = newFormatedCurrency.split(',');

        // Use <ins></ins> <del></del> to represent new price and the old (deleted) one
        const PriceSemanticElementName = isDiscounted ? 'ins' : 'span';

        const schema = isSchemaRequired
            ? {
                  itemProp: 'price',
                  content: `${baseSplitFinalPrice}.${centsSplitFinalPrice}`,
              }
            : {};

        const schemaPriceDate = isSchemaRequired ? (
            <meta itemProp="priceValidUntil" content={specialToDate || defaultPriceDate} />
        ) : null;

        return (
            <p block="ProductPrice" mix={mix} aria-label={`Product price: ${newFormatedCurrency}${newCurrency}`}>
                <PriceSemanticElementName>
                    <data value={newFormatedCurrency} {...schema}>
                        {schemaPriceDate}
                        <span block="ProductPrice" elem="Price">
                            {`${baseSplitFinalPrice},`}
                        </span>
                        <span block="ProductPrice" elem="Cents">
                            {centsSplitFinalPrice}
                        </span>
                        <span block="ProductPrice" elem="Currency">
                            {newCurrency}
                        </span>
                    </data>
                </PriceSemanticElementName>

                {this.renderOldPrice()}
                {this.renderPriceDiscount()}
                {this.renderDiscountInfo()}
                {this.renderSchema()}
            </p>
        );
    }
}

export default ProductPrice;

import { createPortal } from 'react-dom';

import SourceOverlay from 'SourceComponent/Overlay/Overlay.component';

import isMobile from 'Util/Mobile';

import './Overlay.style.extend.scss';

class Overlay extends SourceOverlay {
    componentWillUnmount() {
        this.onHide();
    }

    renderInMobilePortal(content) {
        const { isStatic, isRenderInPortal } = this.props;

        if (!isStatic && (isMobile.any() || isMobile.tabletWidth()) && isRenderInPortal) {
            return createPortal(content, document.body);
        }

        return content;
    }
}

export default Overlay;

import SourceWishlistItem from 'SourceComponent/WishlistItem/WishlistItem.component';

import ProductCard from 'Component/ProductCard/ProductCard.container';

import './WishlistItem.style.scss';

class WishlistItem extends SourceWishlistItem {
    renderAddToCart() {
        const { addToCart } = this.props;

        return (
            <button
                block="Button"
                mods={{ green: true }}
                mix={{ block: 'WishlistItem', elem: 'AddToCart' }}
                onClick={addToCart}
            >
                {__('ADD_TO_CART_1')}
                <span>{__('ADD_TO_CART_2')}</span>
            </button>
        );
    }

    render() {
        const { product, parameters, isLoading } = this.props;

        return (
            <div block="WishlistItem" elem="ProductWrapper">
                <ProductCard
                    product={product}
                    selectedFilters={parameters}
                    mix={{ block: 'WishlistItem' }}
                    isLoading={isLoading}
                    withoutItemProps
                />
                {/* TEMP */}
                {/* { this.renderAddToCart() } */}
            </div>
        );
    }
}

export default WishlistItem;

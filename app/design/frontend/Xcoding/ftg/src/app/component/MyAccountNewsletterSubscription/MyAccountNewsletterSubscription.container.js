import React from 'react';
import { connect } from 'react-redux';

import {
    mapDispatchToProps,
    mapStateToProps,
    MyAccountNewsletterSubscriptionContainer as SourceNewsletterContainer,
} from 'SourceComponent/MyAccountNewsletterSubscription/MyAccountNewsletterSubscription.container';

import Loader from 'Component/Loader';
import MyAccountNewsletterSubscription from './MyAccountNewsletterSubscription.component';

export class MyAccountNewsletterSubscriptionContainer extends SourceNewsletterContainer {
    render() {
        const { isLoading } = this.state;

        return (
            <>
                <Loader isLoading={isLoading} />
                <MyAccountNewsletterSubscription {...this.props} {...this.containerFunctions} />
            </>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyAccountNewsletterSubscriptionContainer);

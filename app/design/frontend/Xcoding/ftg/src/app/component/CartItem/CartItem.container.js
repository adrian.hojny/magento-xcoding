import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
    CartItemContainer as SourceCartItemContainer,
    mapDispatchToProps as sourceMapDispatchToProps,
} from 'SourceComponent/CartItem/CartItem.container';

import { REMOVE_TIMEOUT } from 'Component/CartItem/CartItem.component';
import { CART_ITEM_REMOVED, removeCartItem, setCartItemRemoveStatus } from 'Store/Cart/Cart.action';
import { getConfigurableVariantIndex } from 'Util/Product/Product';

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    setRemoveStatus: (index, status) => dispatch(setCartItemRemoveStatus(index, status)),
    removeFromList: (itemId) => dispatch(removeCartItem(itemId)),
});

export class CartItemContainer extends SourceCartItemContainer {
    static propTypes = {
        ...super.propTypes,
        showActions: PropTypes.bool,
    };

    static defaultProps = {
        showActions: false,
    };

    containerFunctions = {
        handleChangeQuantity: this.handleChangeQuantity.bind(this),
        handleRemoveItem: this.handleRemoveItem.bind(this),
        getCurrentProduct: this.getCurrentProduct.bind(this),
        addBackToCart: this.addBackToCart.bind(this),
    };

    hideLoaderAfterPromise(promise) {
        return this.registerCancelablePromise(promise).promise.then(this.setStateNotLoading, this.setStateNotLoading);
    }

    addBackToCart() {
        const {
            addProduct,
            removeFromList,
            item: {
                product: { variants, type_id },
                product,
                qty,
                sku,
                item_id,
            },
        } = this.props;

        const configurableVariantIndex = getConfigurableVariantIndex(sku, variants);

        const productToAdd = type_id === 'configurable' ? { ...product, configurableVariantIndex } : product;

        this.setState({ isLoading: true }, () => {
            this.hideLoaderAfterPromise(addProduct({ product: productToAdd, quantity: qty })).then(() => {
                removeFromList(item_id);
            });
        });
    }

    handleRemoveItem() {
        this.setState({ isLoading: true }, () => {
            const {
                removeProduct,
                setRemoveStatus,
                removeFromList,
                item: { item_id },
            } = this.props;

            setRemoveStatus(item_id, CART_ITEM_REMOVED);

            this.hideLoaderAfterPromise(removeProduct(item_id)).then(() => {
                setTimeout(() => {
                    removeFromList(item_id);
                }, REMOVE_TIMEOUT);
            });
        });
    }
}

export default connect(null, mapDispatchToProps)(CartItemContainer);

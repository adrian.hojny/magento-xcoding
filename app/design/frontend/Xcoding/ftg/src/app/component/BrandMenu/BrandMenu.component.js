import React, { Fragment, useEffect, useRef, useState } from 'react';

import './BrandMenu.style.scss';

const CMS_BLOCK = 'brand-menu-right';

const BrandMenu = (props) => {
    const container = useRef(null);

    const [columnIndex, setColumnIndex] = useState(0);
    const [maxColumnIndex, setMaxColumnIndex] = useState(0);
    const [activeFilter, setActiveFiler] = useState('all');

    const { blocks, title, url } = props;

    const mapBrandMenu = (brandMenu) => {
        if (!brandMenu) {
            return null;
        }

        const content = document.createElement('div');
        content.innerHTML = brandMenu.content;
        const linksArray = Array.from(content.querySelectorAll('a'), (anhor) => {
            const mods = anhor.innerText.match(/\[(.*)\]/);

            return {
                text: anhor.innerText.trim().replace(/\[.*\]/, ''),
                href: anhor.href,
                group: anhor.innerText.replace(/[^a-z0-9]/gi, '').toUpperCase(),
                mods: (mods && mods[1]) || '',
            };
        }).sort((a, b) => {
            if (a.group < b.group) {
                return -1;
            }
            if (a.group > b.group) {
                return 1;
            }

            return 0;
        });

        return linksArray;
    };

    const handleClick = (i) => {
        if (container.current === null) {
            return;
        }

        container &&
            container.current.scroll({
                top: 0,
                left: (columnIndex + i) * 160,
                behavior: 'smooth',
            });
        setColumnIndex(columnIndex + i);
    };

    const countMaxIndex = () => {
        const allColumn = Math.floor(container.current.scrollWidth / 160);
        const visibleColumn = Math.floor(container.current.clientWidth / 160);
        return allColumn - visibleColumn;
    };

    useEffect(() => {
        setMaxColumnIndex(countMaxIndex());
    }, [activeFilter, blocks]);

    useEffect(() => {
        const handler = () => {
            setMaxColumnIndex(countMaxIndex());
        };

        document.addEventListener('resize', handler);
        return () => {
            document.removeEventListener('resize', handler);
        };
    }, []);

    const linksArray = mapBrandMenu(blocks);
    const groupLinksArray = linksArray
        ? linksArray.reduce((groups, element) => {
              const groupeName = element.group[0];
              groups[groupeName] = groups[groupeName] || [];
              groups[groupeName].push(element);

              return groups;
          }, {})
        : {};
    const allGroupsName = Object.keys(groupLinksArray);

    return (
        <div block="BrandMenu">
            <div block="BrandMenu" elem="header">
                <div block="MegaMenu" elem="BrandTitle">
                    {title}
                </div>
                <div block="BrandMenu" elem="filters">
                    {allGroupsName.map((group, i) => (
                        <span
                            key={i}
                            onClick={() => {
                                setActiveFiler(group);
                            }}
                            mix={{ block: 'BrandMenu', elem: 'filtersElem', mods: { active: activeFilter === group } }}
                        >
                            {group}
                        </span>
                    ))}
                    <span
                        onClick={() => {
                            setActiveFiler('all');
                        }}
                        mix={{ block: 'BrandMenu', elem: 'filtersElem', mods: { active: activeFilter === 'all' } }}
                    >
                        {__('All')}
                    </span>
                </div>
            </div>
            {columnIndex > 0 && <button className="BrandMenu-Arrow_left" onClick={() => handleClick(-1)} />}
            {maxColumnIndex !== columnIndex && (
                <button className="BrandMenu-Arrow_right" onClick={() => handleClick(1)} />
            )}
            <div block="BrandMenu" elem="wrapper" ref={container}>
                <div block="BrandMenu" elem="slider">
                    {allGroupsName
                        .filter((group) => group === activeFilter || activeFilter === 'all')
                        .map((group, i) => (
                            <Fragment key={group + i}>
                                {groupLinksArray[group].map((data, i) => (
                                    <span block="BrandMenu" elem="slider-item">
                                        {i === 0 && <strong>{group}</strong>}
                                        <a
                                            key={i}
                                            mix={{
                                                block: 'BrandMenu',
                                                elem: 'link',
                                                mods: { [data.mods]: !!data.mods },
                                            }}
                                            href={data.href}
                                        >
                                            <span>{data.text.trim()}</span>
                                        </a>
                                    </span>
                                ))}
                            </Fragment>
                        ))}
                </div>
            </div>
        </div>
    );
};

export default BrandMenu;

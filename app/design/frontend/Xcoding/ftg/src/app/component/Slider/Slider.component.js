import React from 'react';

import SourceSlider from 'SourceComponent/Slider/Slider.component';

import Draggable from 'Component/Draggable';
import CSS from 'Util/CSS';
import isMobile from 'Util/Mobile';
import { ANIMATION_DURATION } from './Slider.config';

export * from 'SourceComponent/Slider/Slider.component';

class Slider extends SourceSlider {
    renderNav = this.renderNav.bind(this);

    handleWidthResize = this.handleWidthResize.bind(this);

    componentDidMount() {
        const sliderChildren = this.draggableRef.current.children;
        const sliderWidth = this.draggableRef.current.offsetWidth;

        this.sliderWidth = sliderWidth;

        window.addEventListener('resize', this.handleWidthResize);

        if (!sliderChildren || !sliderChildren[0]) {
            return;
        }

        sliderChildren[0].onload = () => {
            CSS.setVariable(this.sliderRef, 'slider-height', `${sliderChildren[0].offsetHeight}px`);
        };

        setTimeout(() => {
            CSS.setVariable(this.sliderRef, 'slider-height', `${sliderChildren[0].offsetHeight}px`);
        }, ANIMATION_DURATION);
    }

    componentDidUpdate(prevProps) {
        const { activeImage: prevActiveImage } = prevProps;
        const { activeImage } = this.props;

        if (activeImage !== prevActiveImage) {
            const newTranslate = -activeImage * this.sliderWidth;

            CSS.setVariable(
                this.draggableRef,
                'animation-speed',
                `${Math.abs((prevActiveImage - activeImage) * ANIMATION_DURATION)}ms`,
            );

            CSS.setVariable(this.draggableRef, 'translateX', `${newTranslate}px`);
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWidthResize);
    }

    onClickChangeSlide(state, slideSize, lastTranslate, fullSliderSize) {
        const { originalX } = state;
        const { prevActiveImage: prevActiveSlider } = this.state;
        const { onActiveImageChange, slideLink } = this.props;

        const fullSliderPoss = Math.round(fullSliderSize / slideSize);
        const elementPossitionInDOM = this.draggableRef.current.getBoundingClientRect().x;

        const sliderPossition = -prevActiveSlider;
        const realElementPossitionInDOM = elementPossitionInDOM - lastTranslate;
        const mousePossitionInElement = originalX - realElementPossitionInDOM;

        if (isMobile.any() || slideLink) {
            return sliderPossition;
        }

        if (slideSize / 2 < mousePossitionInElement && -fullSliderPoss < sliderPossition) {
            const activeSlide = sliderPossition - 1;
            onActiveImageChange(-activeSlide);
            return activeSlide;
        }

        if (slideSize / 2 > mousePossitionInElement && lastTranslate) {
            const activeSlide = sliderPossition + 1;
            onActiveImageChange(-activeSlide);
            return activeSlide;
        }

        return sliderPossition;
    }

    handleDragStart() {
        CSS.setVariable(this.draggableRef, 'animation-speed', '0');
    }

    handleDragEnd(state, callback) {
        const activeSlide = this.calculateNextSlide(state);

        const slideSize = this.sliderWidth;

        const newTranslate = activeSlide * slideSize;

        CSS.setVariable(this.draggableRef, 'animation-speed', '300ms');

        CSS.setVariable(this.draggableRef, 'translateX', `${newTranslate}px`);

        setTimeout(() => {
            callback({
                originalX: newTranslate,
                lastTranslateX: newTranslate,
            });
        });
    }

    handleWidthResize() {
        const { activeImage } = this.props;

        this.sliderWidth = this.draggableRef.current.offsetWidth;

        const newTranslate = -activeImage * this.sliderWidth;

        CSS.setVariable(this.draggableRef, 'translateX', `${newTranslate}px`);
    }

    changeSlide(activeImage, allowed) {
        if (!allowed) {
            return;
        }
        const { onActiveImageChange } = this.props;
        onActiveImageChange(activeImage);
    }

    renderNav() {
        const { children } = this.props;
        const slidesCount = children.length;

        if (slidesCount <= 1) {
            return null;
        }

        const { activeImage } = this.props;
        const isLeftAllowed = activeImage > 0;
        const isRightAllowed = activeImage < slidesCount - 1;

        return (
            <>
                <button
                    block="Slider"
                    elem="Arrow"
                    mods={{ type: 'left', allowed: isLeftAllowed }}
                    onClick={() => this.changeSlide(activeImage - 1, isLeftAllowed)}
                >
                    <span>{__('Previous')}</span>
                </button>
                <button
                    block="Slider"
                    elem="Arrow"
                    mods={{ type: 'right', allowed: isRightAllowed }}
                    onClick={() => this.changeSlide(activeImage + 1, isRightAllowed)}
                >
                    <span>{__('Next')}</span>
                </button>
            </>
        );
    }

    handleClick = (state, callback, e) => {
        const { slideLink } = this.props;

        if (slideLink && !state.isDragging) {
            window.location.href = slideLink;
        }

        if (e.type === 'contextmenu') {
            this.handleDragEnd(state, callback);
        }
    };

    render() {
        const { showCrumbs, mix, activeImage, children } = this.props;

        return (
            <div block="Slider" elem="Container">
                <div block="Slider" mix={mix} ref={this.sliderRef}>
                    <Draggable
                        mix={{ block: 'Slider', elem: 'Wrapper' }}
                        draggableRef={this.draggableRef}
                        onDragStart={this.handleDragStart}
                        onDragEnd={this.handleDragEnd}
                        onDrag={this.handleDrag}
                        onClick={this.handleClick}
                        shiftX={-activeImage * this.sliderWidth}
                    >
                        {children}
                    </Draggable>
                    {showCrumbs && this.renderCrumbs()}
                    {showCrumbs && this.renderNav()}
                </div>
            </div>
        );
    }
}

export default Slider;

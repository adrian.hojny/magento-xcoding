import { TransformComponent } from 'react-zoom-pan-pinch';

import { ProductGalleryBaseImage as SourceProductGalleryBaseImage } from 'SourceComponent/ProductGalleryBaseImage/ProductGalleryBaseImage.component';

import Image from 'Component/Image';

export class ProductGalleryBaseImage extends SourceProductGalleryBaseImage {
    render() {
        const { src, alt } = this.props;

        return (
            <TransformComponent>
                <Image
                    src={src}
                    ratio="custom"
                    mix={{
                        block: 'ProductGallery',
                        elem: 'SliderImage',
                        mods: { isPlaceholder: !src },
                    }}
                    isPlaceholder={!src}
                    alt={alt}
                    useWebp
                    forceWithoutScrolling
                />
            </TransformComponent>
        );
    }
}

export default ProductGalleryBaseImage;

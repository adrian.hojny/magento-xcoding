/* eslint-disable func-names */
/* eslint-disable fp/no-let */
/* eslint-disable fp/no-loops */
import React, { PureComponent } from 'react';

import { ChildrenType } from 'Type/Common';

import './BrandSizing.style.scss';

class BrandSizing extends PureComponent {
    static propTypes = {
        children: ChildrenType.isRequired,
    };

    static clearCssHighLightedClasses(allTableDataElements) {
        for (let i = 0; i < allTableDataElements.length; i++) {
            const element = allTableDataElements[i];
            element.classList.remove('highlighted');
        }
    }

    static addCssHighLightedClasses(highlightedElements) {
        for (let i = 0; i < highlightedElements.length; i++) {
            const element = highlightedElements[i];
            element.classList.add('highlighted');
        }
    }

    constructor(props) {
        super(props);
        this.modifyTables = this.modifyTables.bind(this);
    }

    modifyTables(elem) {
        if (elem === null) {
            return;
        }

        const tables = elem.querySelectorAll('table');

        for (let i = 0; i < tables.length; i++) {
            const table = tables[i];
            this.modifyTable(table);
        }
    }

    modifyTable(table) {
        const trElements = table.querySelectorAll('tr');

        for (let i = 0; i < trElements.length; i++) {
            const trElement = trElements[i];

            const tdElements = trElement.querySelectorAll('td, th');

            for (let j = 0; j < tdElements.length; j++) {
                const tdElement = tdElements[j];

                tdElement.dataset.row = i;
                tdElement.dataset.col = j;

                tdElement.addEventListener('mouseover', function () {
                    const allTableDataElements = table.querySelectorAll('td, th');
                    BrandSizing.clearCssHighLightedClasses(allTableDataElements);

                    let highlightedSelector = `td[data-row="${this.dataset.row}"], th[data-row="${this.dataset.row}"]`;
                    for (let k = 0; k <= this.dataset.row; k++) {
                        highlightedSelector += `, td[data-row="${k}"][data-col="${this.dataset.col}"], 
                        th[data-col="${this.dataset.col}"][data-row="${k}"] `;
                    }

                    const highlighted = table.querySelectorAll(highlightedSelector);
                    BrandSizing.addCssHighLightedClasses(highlighted);
                });
            }
        }
    }

    render() {
        const { children } = this.props;
        return <div ref={this.modifyTables}>{children}</div>;
    }
}

export default BrandSizing;

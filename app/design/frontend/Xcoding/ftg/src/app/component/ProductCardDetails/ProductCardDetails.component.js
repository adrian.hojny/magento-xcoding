import { PureComponent } from 'react';

import ProductAttributeValue from 'Component/ProductAttributeValue';
import { AttributeType } from 'Type/ProductList';

import './ProductCardDetails.styles.scss';

const HIGHLIGHTED_ATTRIBUTE_CODE = 'material';

class ProductCardDetails extends PureComponent {
    static propTypes = {
        attributesWithValues: AttributeType.isRequired,
    };

    renderAttribute = ([attributeLabel, valueLabel]) => (
        <li
            key={attributeLabel}
            block="ProductCardDetails"
            elem="Attribute"
            mods={{ isHighlighted: valueLabel.attribute_code === HIGHLIGHTED_ATTRIBUTE_CODE }}
        >
            <span block="ProductCardDetails" elem="AttributeLabel">
                {attributeLabel}
            </span>
            <span block="ProductCardDetails" elem="ValueLabel">
                <ProductAttributeValue key={attributeLabel} attribute={valueLabel} isFormattedAsText />
            </span>
        </li>
    );

    renderAttributes() {
        const { attributesWithValues } = this.props;

        if (!Object.keys(attributesWithValues).length) {
            return null;
        }

        return (
            <ul block="ProductCardDetails" elem="AttributesList">
                {Object.entries(attributesWithValues).map(this.renderAttribute)}
            </ul>
        );
    }

    render() {
        return <>{this.renderAttributes()}</>;
    }
}

export default ProductCardDetails;

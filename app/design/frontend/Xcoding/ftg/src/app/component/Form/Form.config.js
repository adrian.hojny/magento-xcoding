import { MAX_TEXT_FIELD_LENGTH } from 'Component/Field/Field.config';

export const MIN_PASSWORD_LENGTH = 8;

export default {
    email: {
        validate: ({ value }) => value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i),
        message: __('Email is invalid.'),
    },
    password: {
        validate: ({ value }) => value.length >= MIN_PASSWORD_LENGTH,
        message: __('Password should be at least 8 characters long'),
    },
    telephone: {
        validate: ({ value }) => value.match(/^\+(?:[0-9-] ?){6,14}[0-9]$/),
        message: __('Phone number is invalid!'),
    },
    notEmpty: {
        validate: ({ value }) => value.length > 0,
        message: __('This field is required!'),
    },
    maxLength: {
        validate: ({ value }) => value.length <= MAX_TEXT_FIELD_LENGTH,
        message: __('Too long text'),
    },
    isChecked: {
        validate: ({ checked }) => checked,
        message: __('This field is required!'),
    },
    password_match: {
        validate: ({ value }, { password }) => {
            const {
                current: { value: passwordValue },
            } = password || { current: {} };

            return value === passwordValue;
        },
        message: __('Password does not match.'),
    },
};

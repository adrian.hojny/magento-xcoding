import React from 'react';
import { withRouter } from 'react-router';

import { ProductGallery as SourceProductGallery } from 'Component/ProductGallery/ProductGallery.component';
import VideoPopup from 'Component/VideoPopup/VideoPopup.container';

import './ProductGalleryOverlayMobile.style';

export * from 'Component/ProductGallery/ProductGallery.component';

class ProductGalleryOverlayMobileComponent extends SourceProductGallery {
    renderImage(mediaData, index) {
        return (
            <div block="ProductGalleryOverlayMobile" elem="ImageWrapper" key={index}>
                <img alt={mediaData.label} src={mediaData.base.url} itemProp="image" onClick={this.openGallery} />
            </div>
        );
    }

    render() {
        const { gallery } = this.props;

        return (
            <>
                <div block="ProductGalleryMobileContainer">{gallery.map(this.renderSlide)}</div>
                <VideoPopup />
            </>
        );
    }
}

export default withRouter(ProductGalleryOverlayMobileComponent);

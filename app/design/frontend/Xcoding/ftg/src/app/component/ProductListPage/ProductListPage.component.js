/**
 * ScandiPWA - Progressive Web App for Magento
 *
 * Copyright © Scandiweb, Inc. All rights reserved.
 * See LICENSE for license details.
 *
 * @license OSL-3.0 (Open Software License ("OSL") v. 3.0)
 * @package scandipwa/base-theme
 * @link https://github.com/scandipwa/base-theme
 */

import { withRouter } from 'react-router-dom';

import { ProductListPage as SourceProductListPage } from 'SourceComponent/ProductListPage/ProductListPage.component';

export class ProductListPage extends SourceProductListPage {
    render() {
        const { pageNumber, wrapperRef, mix, numberOfPlaceholders } = this.props;

        return (
            <ul
                block="ProductListPage"
                mix={{ ...mix, elem: 'Page', mods: { grid: numberOfPlaceholders } }}
                key={pageNumber}
                ref={wrapperRef}
            >
                {this.renderItems()}
            </ul>
        );
    }
}

export default withRouter(ProductListPage);

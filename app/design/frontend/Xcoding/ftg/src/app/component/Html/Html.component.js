import parser from 'html-react-parser';
import attributesToProps from 'html-react-parser/lib/attributes-to-props';
import domToReact from 'html-react-parser/lib/dom-to-react';
import PropTypes from 'prop-types';

import SourceHtml from 'SourceComponent/Html/Html.component';

import Image from 'Component/Image';
import Link from 'Component/Link';

class Html extends SourceHtml {
    static propTypes = {
        ...super.propTypes,
        ignoredRules: PropTypes.array,
    };

    static defaultProps = {
        ...super.defaultProps,
        ignoredRules: [],
    };

    rules = [
        {
            id: 'widget',
            query: { name: ['widget'] },
            replace: this.replaceWidget,
        },
        {
            id: 'a',
            query: { name: ['a'] },
            replace: this.replaceLinks,
        },
        {
            id: 'img',
            query: { name: ['img'] },
            replace: this.replaceImages,
        },
        {
            id: 'input',
            query: { name: ['input'] },
            replace: this.replaceInput,
        },
        {
            id: 'script',
            query: { name: ['script'] },
            replace: this.replaceScript,
        },
        {
            id: 'style',
            query: { name: ['style'] },
            replace: this.replaceStyle,
        },
        {
            id: 'table',
            query: { name: ['table'] },
            replace: this.wrapTable,
        },
    ];

    parserOptions = (ignoredRules = []) => ({
        replace: (domNode) => {
            const { data, name: domName, attribs: domAttrs } = domNode;

            // Let's remove empty text nodes
            if (data && !data.replace(/\u21b5/g, '').replace(/\s/g, '').length) {
                return <></>;
            }

            const rule = this.rules.find((rule) => {
                const {
                    query: { name, attribs },
                } = rule;

                if (name && domName && name.indexOf(domName) !== -1) {
                    return true;
                }
                if (attribs && domAttrs) {
                    // eslint-disable-next-line fp/no-loops, fp/no-let
                    for (let i = 0; i < attribs.length; i++) {
                        const attrib = attribs[i];

                        if (typeof attrib === 'object') {
                            const queryAttrib = Object.keys(attrib)[0];

                            if (Object.prototype.hasOwnProperty.call(domAttrs, queryAttrib)) {
                                return domAttrs[queryAttrib].match(Object.values(attrib)[0]);
                            }
                        } else if (Object.prototype.hasOwnProperty.call(domAttrs, attrib)) {
                            return true;
                        }
                    }
                }

                return false;
            });

            if (rule && !ignoredRules.includes(rule.id)) {
                const { replace } = rule;
                return replace.call(this, domNode);
            }
        },
    });

    /**
     * Replace links to native React Router links
     * @param  {{ attribs: Object, children: Array }}
     * @return {void|JSX} Return JSX if link is allowed to be replaced
     * @memberof Html
     */
    // eslint-disable-next-line consistent-return
    replaceLinks({ attribs, children }) {
        const { href, ...attrs } = attribs;
        const { ignoredRules } = this.props;

        if (href) {
            const isAbsoluteUrl = (value) => new RegExp('^(?:[a-z]+:)?//', 'i').test(value);
            const isSpecialLink = (value) => new RegExp('^(sms|tel|mailto):', 'i').test(value);

            if (!isAbsoluteUrl(href) && !isSpecialLink(href)) {
                return (
                    <Link {...attributesToProps({ ...attrs, to: href })}>
                        {domToReact(children, this.parserOptions(ignoredRules))}
                    </Link>
                );
            }
        }
    }

    /**
     * Replace img to React Images
     * @param  {{ attribs: Object }}
     * @return {void|JSX} Return JSX with image
     * @memberof Html
     */
    replaceImages({ attribs }) {
        const attributes = attributesToProps(attribs);

        if (attribs.src) {
            return <Image {...attributes} useWebp />;
        }
    }

    /**
     * Wrap table in container
     *
     * @param attribs
     * @param children
     * @returns {*}
     */
    wrapTable({ attribs, children }) {
        const { ignoredRules } = this.props;

        return (
            <div block="Table" elem="Wrapper">
                <table {...attributesToProps(attribs)}>{domToReact(children, this.parserOptions(ignoredRules))}</table>
            </div>
        );
    }

    render() {
        const { content, ignoredRules } = this.props;
        return parser(content, this.parserOptions(ignoredRules));
    }
}

export default Html;

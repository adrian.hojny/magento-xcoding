import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import Przelewy24Logo from '../../style/images/Przelewy24_logo.svg';
import P24Blik from './P24Blik/P24Blik.component';
import P24Card from './P24Card/P24Card.component';
import P24PaymentMethods from './P24PaymentMethods/P24PaymentMethods.component';
import { BLIK_CODE_NAME, P24_INNER_METHODS_NAME, P24CARD_CODE_NAME } from './Przelewy24.config';
import { getBaseFieldsGroupName, getP24PayMethods } from './Przelewy24.utils';

import './Przelewy24.style.scss';

const P24Header = () => (
    <h2 block="P24Header">
        <img block="P24Header" elem="Logo" src={Przelewy24Logo} alt="Przelewy24" />
    </h2>
);

const Przelewy24 = ({
    formValues = {},
    formGroupsValidity,
    setFormFieldValue,
    validateFieldGroup,
    purgeOtherFormGroups,
    isCard,
}) => {
    const [methods, setMethods] = useState(null);

    if (!formValues) return null;

    const { BLIK_CODE_NAME: blikCode, P24_INNER_METHODS_NAME: selectedPayment } = formValues;

    const {
        [BLIK_CODE_NAME]: blikVisible,
        [P24_INNER_METHODS_NAME]: otherMethodsVisible,
        [P24CARD_CODE_NAME]: p24CardVisible,
    } = formGroupsValidity;

    const setSelectedPayment = (value) => {
        setFormFieldValue(P24_INNER_METHODS_NAME, value);
    };

    const setBlikCode = (value) => {
        setFormFieldValue(BLIK_CODE_NAME, value);
    };

    useEffect(() => {
        (async () => {
            const methods = await getP24PayMethods();

            setMethods(methods);
        })();
    }, []);

    const handleP24InputChange = ({ name, value }) => {
        setFormFieldValue(name, value);

        const baseFieldsGroupName = getBaseFieldsGroupName(name);

        validateFieldGroup(baseFieldsGroupName);
    };

    if (isCard) {
        return (
            <div block="Przelewy24">
                <P24Card
                    onChange={handleP24InputChange}
                    isPayBtnDisabled={!p24CardVisible}
                    purgeOtherFormGroups={purgeOtherFormGroups}
                />
            </div>
        );
    }

    return (
        <div block="Przelewy24">
            <P24Header />
            <P24Blik
                handleBlikChange={handleP24InputChange}
                isPayBtnDisabled={!blikVisible}
                blikCode={blikCode}
                setBlikCode={setBlikCode}
                purgeOtherFormGroups={purgeOtherFormGroups}
            />
            <P24Header />
            {methods?.length > 0 && (
                <P24PaymentMethods
                    methods={methods}
                    onPaymentMethodChange={handleP24InputChange}
                    isPayBtnDisabled={!otherMethodsVisible}
                    selectedPayment={selectedPayment}
                    setSelectedPayment={setSelectedPayment}
                    purgeOtherFormGroups={purgeOtherFormGroups}
                />
            )}
        </div>
    );
};

P24PaymentMethods.propTypes = {
    methods: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string.isRequired,
            id: PropTypes.number.isRequired,
            status: PropTypes.bool.isRequired,
            imgUrl: PropTypes.string.isRequired,
            mobileImgUrl: PropTypes.string,
            mobile: PropTypes.bool.isRequired,
        }),
    ).isRequired,
};

export default Przelewy24;

import { Draggable as SourceDraggable } from 'SourceComponent/Draggable/Draggable.component';

export class Draggable extends SourceDraggable {
    handleTouchStart = () => {
        window.addEventListener('touchmove', this.handleTouchMove);
        window.addEventListener('touchend', this.handleTouchEnd);
    };

    handleMouseDown = (event) => {
        window.addEventListener('mousemove', this.handleMouseMove);
        window.addEventListener('mouseup', this.handleMouseUp);

        event.preventDefault();
    };

    handleMouseUp = () => {
        const { isDragging } = this.state;
        window.removeEventListener('mousemove', this.handleMouseMove);
        window.removeEventListener('mouseup', this.handleMouseUp);

        if (isDragging) {
            this._handleDragEnd();
        }
    };

    handleMouseMove = (event) => {
        const { isDragging } = this.state;
        const { clientX, clientY } = event;
        const { shiftX, shiftY } = this.props;

        if (!isDragging) {
            this._handleDragStart(event);
            return;
        }

        this.setState(
            ({ originalX, originalY }) => ({
                translateX: clientX - originalX + shiftX,
                translateY: clientY - originalY + shiftY,
            }),
            () => {
                const { onDrag } = this.props;
                if (onDrag) {
                    onDrag({ ...this.state, clientX, clientY });
                }
            },
        );
    };
}

export default Draggable;

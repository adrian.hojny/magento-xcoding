import { MyAccountNewsletterSubscription as SourceNewsletter } from 'SourceComponent/MyAccountNewsletterSubscription/MyAccountNewsletterSubscription.component';

import './MyAccountNewsletterSubscription.style.scss';

export class MyAccountNewsletterSubscription extends SourceNewsletter {
    constructor(props) {
        super(props);
        this.state = { hasChanged: false, checked: props.customer?.is_subscribed || false };
    }

    onFormSuccess = (_fields) => {
        const { onCustomerSave, customer } = this.props;
        const customField = {
            is_subscribed: this.state.checked,
        };

        onCustomerSave(customField);
    };

    handleSuscribeCheckbox = (_value, checked) => {
        this.setState({ hasChanged: true, checked });
    };

    get fieldMap() {
        const {
            customer: { is_subscribed },
        } = this.props;

        return {
            is_subscribed: {
                type: 'checkbox',
                label: __('General subscription'),
                value: 'is_subscribed',
                checked: is_subscribed,
                onChange: this.handleSuscribeCheckbox,
            },
        };
    }

    renderActions() {
        return (
            <button
                type="submit"
                block="Button"
                disabled={!this.state.hasChanged || this.state.checked === this.props.customer?.is_subscribed}
                mix={{ block: 'MyAccountNewsletterSubscription', elem: 'Button' }}
            >
                {__('Save changes')}
            </button>
        );
    }
}

export default MyAccountNewsletterSubscription;

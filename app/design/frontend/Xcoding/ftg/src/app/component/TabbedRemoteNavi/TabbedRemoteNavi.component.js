import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import { UidContext } from 'Context/UidContext';

import './TabbedRemoteNavi.style.scss';

const initialState = {
    tabs: [],
    name: null,
};

const TabsList = ({ tabs, current, handleTabChange }) =>
    tabs.map((tab, i) => {
        const isCurrent = current === tab;
        const handleOnclick = () => (!isCurrent ? handleTabChange(tab) : null);

        return (
            <div
                key={i}
                onClick={handleOnclick}
                block="TabbedRemoteNavi"
                elem="tab"
                mods={{ selected: current === tab }}
            >
                <div block="TabbedRemoteNavi" elem="inside_tab">
                    {tab}
                </div>
            </div>
        );
    });

const TabbedRemoteNavi = ({ changeNavigationStateWithId, getNavigationStateById, mix = {}, navigationState }) => {
    const { uid = 'single_carousel_per_page' } = useContext(UidContext) || {};

    if (typeof uid !== 'string' || uid.length < 1) {
        console.error(`TabbedRemoteNanavigationStatevi must be used withing UidContext. Missing uid: ${uid}`);
        return null;
    }

    const navState = getNavigationStateById(uid) || initialState;
    const { tabs, name: current } = navState;

    const handleTabChange = (tab) => {
        changeNavigationStateWithId(uid, {
            tabs,
            name: tab,
        });
    };

    return (
        <div block="TabbedRemoteNavi" mix={mix}>
            {tabs && <TabsList current={current} tabs={tabs} handleTabChange={handleTabChange} />}
        </div>
    );
};

TabbedRemoteNavi.propTypes = {
    // tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
    // handleTabSelect: PropTypes.func.isRequired,
    changeNavigationStateWithId: PropTypes.func.isRequired,
    getNavigationStateById: PropTypes.func.isRequired,
    mix: PropTypes.object,
    navigationState: PropTypes.object.isRequired,
};

TabbedRemoteNavi.defaultProps = {
    mix: null,
};

export default TabbedRemoteNavi;

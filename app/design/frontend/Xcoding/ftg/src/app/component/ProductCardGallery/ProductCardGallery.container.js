import React from 'react';
import PropTypes from 'prop-types';
import { Subscribe } from 'unstated';

import ProductGalleryContainer from 'Component/ProductGallery/ProductGallery.container';
import SharedTransitionContainer from 'Component/SharedTransition/SharedTransition.unstated';
import { ProductType } from 'Type/ProductList';
import ProductCardGallery from './ProductCardGallery.component';

export * from 'Component/ProductGallery/ProductGallery.container';

class ProductGalleryOverlaySliderContainer extends ProductGalleryContainer {
    static propTypes = {
        product: ProductType.isRequired,
        areDetailsLoaded: PropTypes.bool,
        galleryType: PropTypes.string,
        showGallery: PropTypes.func,
        updateActiveImageOverlayGallery: PropTypes.func,
    };

    containerProps = () => {
        const { activeImage, isZoomEnabled } = this.state;
        const {
            product: { id },
            galleryType,
            showGallery,
            updateActiveImageOverlayGallery,
            attributes,
        } = this.props;

        return {
            gallery: this.getGalleryPictures(),
            productName: this._getProductName(),
            activeImage,
            isZoomEnabled,
            productId: id,
            galleryType,
            showGallery,
            updateActiveImageOverlayGallery,
            attributes,
        };
    };

    render() {
        return (
            <Subscribe to={[SharedTransitionContainer]}>
                {({ registerSharedElementDestination }) => (
                    <ProductCardGallery
                        registerSharedElementDestination={registerSharedElementDestination}
                        {...this.containerProps()}
                        {...this.containerFunctions}
                    />
                )}
            </Subscribe>
        );
    }
}

export default ProductGalleryOverlaySliderContainer;

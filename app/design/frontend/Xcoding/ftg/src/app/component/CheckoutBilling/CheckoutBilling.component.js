import SourceCheckoutBilling from 'SourceComponent/CheckoutBilling/CheckoutBilling.component';

import CheckoutAddressBook from 'Component/CheckoutAddressBook';

class CheckoutBilling extends SourceCheckoutBilling {
    renderAddressBook() {
        const {
            onAddressSelect,
            isSameAsShipping,
            totals: { is_virtual },
            companyDetails,
            onCompanyTaxIdNumberChange,
            onCompanyNameChange,
        } = this.props;

        if (isSameAsShipping && !is_virtual) {
            return null;
        }

        return (
            <CheckoutAddressBook
                onAddressSelect={onAddressSelect}
                companyDetails={{ ...companyDetails, onCompanyTaxIdNumberChange, onCompanyNameChange }}
                isBilling
            />
        );
    }

    renderActions() {
        return null;
    }

    renderTermsAndConditions() {
        return null;
    }
}

export default CheckoutBilling;

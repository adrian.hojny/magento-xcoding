import { PureComponent } from 'react';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import PropTypes from 'prop-types';

import Image from 'Component/Image/Image.container';
import ProductCard from 'Component/ProductCard/ProductCard.container';

import './CarouselWithSideImage.style';

const CAROUSEL_PRODUCTS_TO_RENDER = 50;

export default class CarouselWithSideImage extends PureComponent {
    static propTypes = {
        sideImageUrl: PropTypes.string,
        products: PropTypes.any.isRequired,
        titleUp: PropTypes.string.isRequired,
        titleDown: PropTypes.string,
    };

    static defaultProps = {
        titleDown: null,
        sideImageUrl: '',
    };

    renderTitle() {
        const { titleUp, titleDown } = this.props;

        if (titleDown) {
            return (
                <div block="CarouselSideImage" elem="Title">
                    <div block="CarouselSideImage" elem="TitleUp">
                        <span>{titleUp}</span>
                    </div>
                    <div block="CarouselSideImage" elem="TitleDown">
                        <span>{titleDown}</span>
                    </div>
                </div>
            );
        }

        return (
            <div block="CarouselSideImage" elem="Title">
                <span>{titleUp}</span>
            </div>
        );
    }

    renderSideImage() {
        const { sideImageUrl } = this.props;

        if (sideImageUrl) {
            return <Image src={sideImageUrl} />;
        }

        return null;
    }

    renderCarousel() {
        const { products } = this.props;

        if (products.length > 0) {
            return (
                <ScrollMenu
                    menuClass="CarouselSideImageMenu"
                    itemClass="CarouselSideImage-Item"
                    itemClassActive="CarouselSideImage-Item_active"
                    arrowClass="CarouselSideImage-ArrowWrapper"
                    arrowDisabledClass="CarouselSideImage-ArrowWrapper_hidden"
                    arrowLeft={<div block="CarouselSideImage" elem="Arrow" mods={{ left: true }} />}
                    arrowRight={<div block="CarouselSideImage" elem="Arrow" mods={{ right: true }} />}
                    scrollBy={1}
                    alignCenter={false}
                    wheel={false}
                    data={products.slice(0, CAROUSEL_PRODUCTS_TO_RENDER).map((product) => (
                        <ProductCard product={product} key={product.id} />
                    ))}
                />
            );
        }

        return null;
    }

    render() {
        return (
            <div block="CarouselSideImage">
                <div block="CarouselSideImage" elem="ImageWrapper">
                    <div>{this.renderSideImage()}</div>
                </div>
                <div block="CarouselSideImage" elem="ContentWrapper">
                    <div block="CarouselSideImage" elem="TitleWrapper">
                        {this.renderTitle()}
                    </div>
                    <div block="CarouselSideImage" elem="CarouselWrapper">
                        {this.renderCarousel()}
                    </div>
                </div>
            </div>
        );
    }
}

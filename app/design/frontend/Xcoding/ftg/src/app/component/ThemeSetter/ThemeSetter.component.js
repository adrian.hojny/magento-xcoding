import { useContext } from 'react';

import { ThemeContext } from 'Context/ThemeContext';

const ThemeSetter = ({ children }) => {
    const {
        theme: { mods = [] },
    } = useContext(ThemeContext);

    const styledMods = mods.reduce(
        (acc, current) => ({
            ...acc,
            [current]: true,
        }),
        {},
    );

    return (
        <div block="Theme" mods={styledMods}>
            {children}
        </div>
    );
};

export default ThemeSetter;

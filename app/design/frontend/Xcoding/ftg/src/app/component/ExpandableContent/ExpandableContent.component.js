import PropTypes from 'prop-types';

import SourceExpandableContent from 'SourceComponent/ExpandableContent/ExpandableContent.component';

import TextPlaceholder from 'Component/TextPlaceholder';
import { ChildrenType } from 'Type/Common';

class ExpandableContent extends SourceExpandableContent {
    static propTypes = {
        ...super.propTypes,
        children: ChildrenType,
        heading: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
    };

    renderButton() {
        const { isContentExpanded } = this.state;
        const { heading, subHeading, mix } = this.props;

        return (
            <button
                block="ExpandableContent"
                elem="Button"
                mods={{ isContentExpanded }}
                mix={{ ...mix, elem: 'ExpandableContentButton' }}
                onClick={this.toggleExpand}
            >
                <span block="ExpandableContent" elem="Heading" mix={{ ...mix, elem: 'ExpandableContentHeading' }}>
                    {typeof heading === 'string' ? <TextPlaceholder content={heading} length="medium" /> : heading}
                </span>
                <span block="ExpandableContent" elem="SubHeading" mix={{ ...mix, elem: 'ExpandableContentSubHeading' }}>
                    {subHeading}
                </span>
            </button>
        );
    }
}

export default ExpandableContent;

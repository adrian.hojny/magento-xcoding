import React, { PureComponent } from 'react';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import PropTypes from 'prop-types';

import CarouselWrapper from 'Component/CarouselWrapper';
import Link from 'Component/Link';
import ProductCard from 'Component/ProductCard/ProductCard.container';

import './NewProductsCarousel.style.scss';

export default class NewProductsCarousel extends PureComponent {
    static propTypes = {
        products: PropTypes.any.isRequired,
        allProductsUrl: PropTypes.string,
        title: PropTypes.string,
    };

    static defaultProps = {
        title: '',
        allProductsUrl: '',
    };

    renderProducts(products) {
        const { title, allProductsUrl } = this.props;

        return (
            <CarouselWrapper
                label="New products"
                mix={{ block: 'NewProductsCarousel', elem: 'Section' }}
                wrapperMix={{ block: 'NewProductsCarousel', elem: 'Wrapper' }}
            >
                <div block="NewProductsCarousel" elem="Carousel">
                    <div block="NewProductsCarousel" elem="Header">
                        <h1 block="NewProductsCarousel" elem="Title">
                            {title || __('New Products')}
                        </h1>
                        {allProductsUrl && (
                            <Link block="NewProductsCarousel" elem="ListingButton" to={allProductsUrl}>
                                {__('See all')}
                            </Link>
                        )}
                    </div>

                    <ScrollMenu
                        menuClass="NewProductsCarouselMenu"
                        itemClass="NewProductsCarousel-Item"
                        itemClassActive="NewProductsCarousel-Item_active"
                        arrowClass="NewProductsCarousel-ArrowWrapper"
                        arrowDisabledClass="NewProductsCarousel-ArrowWrapper_hidden"
                        arrowLeft={<div block="NewProductsCarousel" elem="Arrow" mods={{ left: true }} />}
                        arrowRight={<div block="NewProductsCarousel" elem="Arrow" mods={{ right: true }} />}
                        scrollBy={5}
                        alignCenter={false}
                        wheel={false}
                        data={products.map((product) => (
                            <ProductCard product={product} key={product.id} />
                        ))}
                        hideArrows
                    />
                </div>
            </CarouselWrapper>
        );
    }

    render() {
        const { products } = this.props;

        if (!products.length) {
            return null;
        }

        return this.renderProducts(products);
    }
}

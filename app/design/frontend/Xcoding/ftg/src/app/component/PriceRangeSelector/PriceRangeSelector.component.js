import React from 'react';
import InputRange from 'react-input-range';

import FilterSelector from 'Component/FilterSelector/FilterSelector.component';
import PriceRangeSelectorInput from 'Component/PriceRangeSelectorInput';

import './PriceRangeSelector.style.scss';

class PriceRangeSelector extends FilterSelector {
    constructor(props) {
        super(props);
        const digitsAfterPeriod = 2;
        this.toIntegerMultiplyFix = 10 ** digitsAfterPeriod; // for floating point precision fix

        const grossInputMin = this.countGrossPrice(props.storeMinPriceValue);
        const grossInputMax = this.countGrossPrice(props.storeMaxPriceValue);

        this.state = {
            ...super.state,
            min: props.storeMinPriceValue,
            max: props.storeMaxPriceValue,
            inputMin: grossInputMin,
            inputMax: grossInputMax,
            isSliding: false,
        };
    }

    countGrossPrice(rawPrice) {
        const { priceSliderVatRate } = this.props;

        const rawPriceInt = rawPrice * this.toIntegerMultiplyFix;
        const vatRateInt = priceSliderVatRate * this.toIntegerMultiplyFix;

        return Math.ceil((rawPriceInt * vatRateInt) / this.toIntegerMultiplyFix ** 2);
    }

    countNettPrice(grossPrice) {
        const { priceSliderVatRate } = this.props;

        const grossPriceInt = grossPrice * this.toIntegerMultiplyFix;
        const vatRateInt = priceSliderVatRate * this.toIntegerMultiplyFix;

        return Math.floor(grossPriceInt / vatRateInt);
    }

    componentDidMount() {
        this.setRangeFromProps();
    }

    componentDidUpdate(prevProps) {
        const {
            storeMinPriceValue: prevStoreMinPriceValue,
            storeMaxPriceValue: prevStoreMaxPriceValue,
            currentFilterPriceRange: prevFilterPriceRange,
        } = prevProps;
        const { storeMinPriceValue, storeMaxPriceValue, currentFilterPriceRange } = this.props;

        if (
            prevStoreMinPriceValue !== storeMinPriceValue ||
            storeMaxPriceValue !== prevStoreMaxPriceValue ||
            prevFilterPriceRange !== currentFilterPriceRange
        ) {
            this.setRangeFromProps();
        }

        this.submitFiltersOnOverlayClose(prevProps);
    }

    setRangeFromProps = () => {
        const {
            storeMinPriceValue,
            storeMaxPriceValue,
            currentFilterPriceRange: { min: currentFilterMin, max: curentFilterMax },
        } = this.props;

        const minVal =
            currentFilterMin < storeMinPriceValue && currentFilterMin !== 0 ? currentFilterMin : storeMinPriceValue;
        const maxVal = curentFilterMax > storeMaxPriceValue ? curentFilterMax : storeMaxPriceValue;

        const grossInputMin = this.countGrossPrice(minVal);
        const grossInputMax = this.countGrossPrice(maxVal);

        this.setState({
            min: minVal,
            max: maxVal,
            inputMin: grossInputMin,
            inputMax: grossInputMax,
        });
    };

    onSliderChange = ({ min, max }) => {
        this.setState({
            inputMin: min,
            inputMax: max,
        });
    };

    onSliderStart = () => {
        this.setState({ isSliding: true });
    };

    onSliderComplete = () => {
        setImmediate(() => this.setState({ isSliding: false }));
    };

    dropdownClose = () => {
        const { isSliding } = this.state;
        if (!isSliding) super.dropdownClose();
    };

    submit = () => {
        const { updateFilter, storeMinPriceValue, storeMaxPriceValue } = this.props;
        const { inputMin, inputMax, min, max } = this.state;

        const nettMin = this.countNettPrice(inputMin);
        const nettMax = this.countNettPrice(inputMax);

        const currentInputsEqualToPriceRange = nettMin === storeMinPriceValue && nettMax === storeMaxPriceValue;

        const currentInputEqualToCurrenlySelectedFilters = nettMin === min && nettMax === max;

        if (currentInputEqualToCurrenlySelectedFilters) return;

        if (currentInputsEqualToPriceRange) {
            updateFilter({ min: '', max: '' });
        } else {
            updateFilter({ min: nettMin, max: nettMax });
        }
    };

    clear = () => {
        const { updateFilter } = this.props;

        updateFilter({ min: '', max: '' });
        this.setRangeFromProps();

        this.setState({
            isOpen: false,
            selectedOptions: {},
        });
    };

    checkIfSelected = () => {
        const {
            currentFilterPriceRange: { min, max },
        } = this.props;

        return min !== 0 && max !== 0;
    };

    changeInputValue = (name) => ({ target: { value } }) => {
        this.setState({ [name]: value });
    };

    submitInputValue = () => {
        const { inputMin, inputMax, min, max } = this.state;

        const { storeMinPriceValue, storeMaxPriceValue } = this.props;
        const minInputNettVal = this.countNettPrice(parseInt(inputMin, 10));
        const maxInputNettVal = this.countNettPrice(parseInt(inputMax, 10));

        if (minInputNettVal < max) {
            if (minInputNettVal < storeMinPriceValue) {
                this.setState({ min: storeMinPriceValue, inputMin: this.countGrossPrice(storeMinPriceValue) });
            }
        } else {
            this.setState({ inputMin: this.countGrossPrice(min) });
        }

        if (maxInputNettVal > min) {
            if (maxInputNettVal > storeMaxPriceValue) {
                this.setState({ max: storeMaxPriceValue, inputMax: this.countGrossPrice(storeMaxPriceValue) });
            }
        } else {
            this.setState({ inputMax: this.countGrossPrice(max) });
        }
    };

    renderPriceRangeInput(name) {
        const { currencyCode } = this.props;

        return (
            <PriceRangeSelectorInput
                currencyCode={currencyCode}
                value={this.state[name]}
                name="inputMin"
                changeValue={this.changeInputValue(name)}
                submit={this.submitInputValue}
                dropdownClose={this.dropdownClose}
            />
        );
    }

    renderOptions() {
        const { storeMinPriceValue, storeMaxPriceValue } = this.props;
        const { inputMin, inputMax } = this.state;

        return (
            <>
                <div block="PriceRangeSelector" elem="InputContainer">
                    {this.renderPriceRangeInput('inputMin')}
                    {this.renderPriceRangeInput('inputMax')}
                </div>
                <div block="RangeSelector">
                    <InputRange
                        minValue={storeMinPriceValue}
                        maxValue={storeMaxPriceValue}
                        value={{ min: parseInt(inputMin, 10), max: parseInt(inputMax, 10) }}
                        onChange={this.onSliderChange}
                        onChangeStart={this.onSliderStart}
                        onChangeComplete={this.onSliderComplete}
                    />
                </div>
            </>
        );
    }
}

export default PriceRangeSelector;

import { connect } from 'react-redux';

import { UPDATE_WISHLIST_FREQUENCY } from 'SourceComponent/WishlistItem/WishlistItem.config';
import {
    mapDispatchToProps,
    WishlistItemContainer as SourceWishlistItemContainer,
} from 'SourceComponent/WishlistItem/WishlistItem.container';

import history from 'Util/History';
import { objectToUri } from 'Util/Url';

export class WishlistItemContainer extends SourceWishlistItemContainer {
    containerProps = () => {
        const { isLoading } = this.state;
        const {
            product: { configurable_options = {} },
        } = this.props;
        const parameters = this._getParameters();

        return {
            changeQuantity: this.changeQuantity,
            changeDescription: this.changeDescription,
            parameters,
            isConfigured: Object.keys(parameters).length === Object.keys(configurable_options).length,
            isLoading,
        };
    };

    _getParameters = () => {
        const { product } = this.props;

        const {
            type_id,
            wishlist: { sku },
            variants,
            configurable_options,
        } = product;

        if (type_id !== 'configurable') return {};

        const options = Object.keys(configurable_options) || [];
        const configurableVariantIndex = this.getConfigurableVariantIndex(sku, variants);

        const { attributes = {} } = variants[configurableVariantIndex] || {};

        return Object.entries(attributes).reduce((acc, [code, { attribute_value }]) => {
            if (!options.includes(code)) return acc;

            return {
                ...acc,
                [code]: [attribute_value],
            };
        }, {});
    };

    addItemToCart() {
        const { product: item, addProductToCart, showNotification } = this.props;

        const {
            type_id,
            variants,
            wishlist: { id, sku, quantity },
        } = item;

        const configurableVariantIndex = this.getConfigurableVariantIndex(sku, variants);

        if (!configurableVariantIndex) {
            return history.push(this._getLinkTo());
        }

        const product = type_id === 'configurable' ? { ...item, configurableVariantIndex } : item;

        this.setState({ isLoading: true });

        return addProductToCart({ product, quantity })
            .then(
                () => this.removeItem(id),
                () => this.showNotification('error', __('Error Adding Product To Cart')),
            )
            .then(() => showNotification('success', __('Product Added To Cart')))
            .catch(() => this.showNotification('error', __('Error cleaning wishlist')));
    }

    _getLinkTo() {
        const {
            product: { url_key },
            product,
        } = this.props;

        if (!url_key) {
            return undefined;
        }
        const { parameters } = this._getParameters();

        return {
            pathname: `/product/${url_key}`,
            state: { product },
            search: objectToUri(parameters),
        };
    }
}

export { mapDispatchToProps, UPDATE_WISHLIST_FREQUENCY };
export default connect(null, mapDispatchToProps)(WishlistItemContainer);

import { connect } from 'react-redux';

import {
    CheckoutShippingContainer as SourceCheckoutShippingContainer,
    mapStateToProps,
} from 'SourceComponent/CheckoutShipping/CheckoutShipping.container';

import ConfigQuery from 'Query/Config.query';
import { setShippingMethod } from 'Store/Cart/Cart.action';
import { showNotification } from 'Store/Notification/Notification.action';
import { trimAddressFields } from 'Util/Address';
import { fetchQuery } from 'Util/Request';

const CHECKOUT_TERMS_NAME = 'CHECKOUT_TERMS';

export const mapDispatchToProps = (dispatch) => ({
    showNotification: (type, message) => dispatch(showNotification(type, message)),
    setShippingMethod: (method) => dispatch(setShippingMethod(method)),
});

class CheckoutShippingContainer extends SourceCheckoutShippingContainer {
    containerFunctions = {
        onShippingSuccess: this.onShippingSuccess.bind(this),
        onShippingError: this.onShippingError.bind(this),
        onAddressSelect: this.onAddressSelect.bind(this),
        onShippingMethodSelect: this.onShippingMethodSelect.bind(this),
        onSubscribeNewsletterChange: this.onSubscribeNewsletterChange.bind(this),
    };

    constructor(props) {
        super(props);

        const { shippingMethods } = props;
        const [selectedShippingMethod] = shippingMethods;

        this.state = {
            selectedCustomerAddressId: 0,
            subscribeNewsletter: false,
            selectedShippingMethod,
            termsData: {},
        };
    }

    componentDidMount() {
        this.getCheckboxData();
    }

    componentDidUpdate(prevProps) {
        const { shippingMethods, isLoading } = this.props;
        const { selectedShippingMethod } = this.state;
        const [defaultSelected] = shippingMethods;

        if (!isLoading && isLoading !== prevProps.isLoading && shippingMethods.length > 0) {
            const methodExistOnList = selectedShippingMethod?.carrier_code
                ? !!shippingMethods.find((method) => method.carrier_code === selectedShippingMethod.carrier_code)
                : false;

            if (!methodExistOnList) {
                this.setState({
                    selectedShippingMethod: defaultSelected,
                });
            }
        }
    }

    getCheckboxData() {
        fetchQuery(ConfigQuery.getCheckoutAgreements()).then(({ checkoutAgreements }) => {
            const data = checkoutAgreements.find((term) => term.name === CHECKOUT_TERMS_NAME);

            if (data) {
                this.setState({ termsData: data });
            }
        });
    }

    onShippingSuccess(fields) {
        const { saveAddressInformation, showNotification, shippingAdditionalInformation } = this.props;
        const { selectedCustomerAddressId, subscribeNewsletter, selectedShippingMethod } = this.state;

        const shippingAddress = selectedCustomerAddressId
            ? this._getAddressById(selectedCustomerAddressId)
            : trimAddressFields(fields);

        const { carrier_code: shipping_carrier_code, method_code: shipping_method_code } = selectedShippingMethod;

        const data = {
            billing_address: shippingAddress,
            shipping_address: shippingAddress,
            subscribe_newsletter: subscribeNewsletter,
            shipping_carrier_code,
            shipping_method_code,
        };

        if (shipping_method_code === 'inpostmachine') {
            const { inpost } = shippingAdditionalInformation;

            if (!inpost || !inpost.name) {
                return showNotification('info', __('Please, select delivery point first!'));
            }

            data.inpost_machine_id = inpost.name;
        }

        if (shipping_method_code === 'instorepickup') {
            const { pickup } = shippingAdditionalInformation;

            data.pickup_store_code = pickup.pickup_store_code;
        }

        return saveAddressInformation(data);
    }

    onSubscribeNewsletterChange(name, value) {
        this.setState({
            subscribeNewsletter: value,
        });
    }
}

export { mapStateToProps };

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutShippingContainer);

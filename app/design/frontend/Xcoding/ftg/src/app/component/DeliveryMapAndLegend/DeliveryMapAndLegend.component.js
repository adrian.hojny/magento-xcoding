import { useEffect } from 'react';
import { useMediaPredicate } from 'react-media-hook';

import CmsBlock from 'Component/CmsBlock';

import './DeliveryMapAndLegend.style';

const asyncLoadCss = (filename) =>
    new Promise((resolve) => {
        if (document.querySelector(`link[href='${filename}']`)) {
            resolve();
            return;
        }

        const link = document.createElement('link');
        link.rel = 'stylesheet';
        link.href = filename;
        link.onload = resolve;
        const h = document.getElementsByTagName('head')[0];
        h.parentNode.insertBefore(link, h);
    });

const enableCssMap = ($mapEl, mapSize = 750) => {
    $mapEl.CSSMap({
        size: mapSize,
        tooltips: 'floating-top-center',
        responsive: 'auto',
        disableClicks: true,
    });
};

const mediaQueryHandlers = {
    '(min-width: 767px)': '',
};

let cssMapWrap = null;

const getRegionsOnMap = (regionsIds) =>
    regionsIds.map((regionId) => cssMapWrap.querySelector(`.${regionId}`)).filter((region) => region !== null);

const getRegionsOnMapFromIdsDataAttr = (mapRegionsStr) => {
    const isArrayOfRegions = mapRegionsStr.charAt(0) === '[';
    const regionsIds = isArrayOfRegions ? JSON.parse(mapRegionsStr) : [mapRegionsStr];

    return getRegionsOnMap(regionsIds);
};

const selectMapArea = ({
    target: {
        dataset: { mapRegions },
    },
}) => {
    const regionsOnMap = getRegionsOnMapFromIdsDataAttr(mapRegions);

    regionsOnMap.map((region) => region.classList.add('active-region'));
};
const deselectMapArea = ({
    target: {
        dataset: { mapRegions },
    },
}) => {
    const regionsOnMap = getRegionsOnMapFromIdsDataAttr(mapRegions);

    regionsOnMap.map((region) => region.classList.remove('active-region'));
};

export default () => {
    const widthAbove1100 = useMediaPredicate('(min-width: 1100px)');
    const mapSize = widthAbove1100 ? 750 : 540;

    useEffect(() => {
        let trsWithCorrespondingMapSector = [];

        Promise.all([
            import(/* webpackMode: "lazy", webpackChunkName: "jquery" */ 'jquery'),
            asyncLoadCss('/media/cssmap-europe/cssmap-europe.css'),
        ]).then((values) => {
            const [jquery] = values;

            window.$ = jquery.default;
            window.jQuery = jquery.default;
            import(/* webpackMode: "lazy", webpackChunkName: "cssmap" */ './cssMapPluginES6').then((loadCssMap) => {
                loadCssMap.default();

                cssMapWrap = document.getElementById('map-europe');
                enableCssMap(window.$(cssMapWrap), mapSize);
                const mapTablesWrap = window.document
                    .getElementById('DeliveryMapAndLegend')
                    .querySelector('.CssMapTables');

                trsWithCorrespondingMapSector = Array.from(mapTablesWrap.querySelectorAll('tr[data-map-regions]'));

                trsWithCorrespondingMapSector.map((tr) => {
                    tr.addEventListener('mouseenter', selectMapArea);
                    tr.addEventListener('mouseleave', deselectMapArea);
                });
            });
        });

        return () => {
            trsWithCorrespondingMapSector.map((tr) => {
                tr.removeEventListener('mouseenter', selectMapArea);
                tr.removeEventListener('mouseleave', deselectMapArea);
            });
        };
    }, []);

    return (
        <div block="DeliveryMapAndLegend" id="DeliveryMapAndLegend">
            <CmsBlock identifier="qa-delivery-map-table-legend" withoutWrapper />
        </div>
    );
};

import { connect } from 'react-redux';

import { LANG_PANEL } from 'Component/Header/Header.component';
import { MENU_SUBCATEGORY } from 'Component/Header/Header.config';
import MenuOverlay from 'Component/MenuOverlay/MenuOverlay.component';
import { DEFAULT_STATE_NAME } from 'Component/NavigationAbstract/NavigationAbstract.config';
import MenuQuery from 'Query/Menu.query';
import { changeNavigationState, goToPreviousNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { hideActiveOverlay, toggleOverlayByKey } from 'Store/Overlay/Overlay.action';
import history from 'Util/History';
import MenuHelper from 'Util/Menu';
import DataContainer from 'Util/Request/DataContainer';

export const mapStateToProps = (state) => ({});

export const mapDispatchToProps = (dispatch) => ({
    hideActiveOverlay: () => dispatch(hideActiveOverlay()),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    goToPreviousHeaderState: (ignoredStates) =>
        dispatch(goToPreviousNavigationState(TOP_NAVIGATION_TYPE, ignoredStates)),
    showOverlay: (overlayKey) => dispatch(toggleOverlayByKey(overlayKey)),
});

export class MenuOverlayContainer extends DataContainer {
    state = {
        activeMenuItemsStack: [],
        menu: {},
    };

    containerFunctions = {
        closeMenuOverlay: this.closeMenuOverlay.bind(this),
        handleSubcategoryClick: this.handleSubcategoryClick.bind(this),
        handleOpenLangPanel: this.handleOpenLangPanel.bind(this),
    };

    componentDidMount() {
        this._getMenu();
    }

    _getMenuOptions() {
        const { header_content: { header_menu } = {} } = window.contentConfiguration;

        return {
            identifier: [header_menu || 'new-main-menu'],
        };
    }

    _getMenu() {
        this.fetchData([MenuQuery.getQuery(this._getMenuOptions())], ({ menu }) =>
            this.setState({
                menu: MenuHelper.reduce(menu),
            }),
        );
    }

    closeMenuOverlay() {
        const { hideActiveOverlay, changeHeaderState } = this.props;
        this.setState({ activeMenuItemsStack: [] });
        hideActiveOverlay();
        changeHeaderState({ name: DEFAULT_STATE_NAME, force: true });
    }

    handleOpenLangPanel() {
        const { showOverlay, changeHeaderState } = this.props;
        showOverlay(LANG_PANEL);

        changeHeaderState({
            name: LANG_PANEL,
            title: __('Preferences'),
            force: true,
            onCloseClick: () => {
                history.goBack();
            },
        });
    }

    handleSubcategoryClick(e, activeSubcategory) {
        const { activeMenuItemsStack } = this.state;
        const { changeHeaderState, goToPreviousHeaderState } = this.props;
        const { item_id, title } = activeSubcategory;

        const activeElement = document.querySelector('.MenuOverlay-SubMenu_activeElement');
        activeElement?.scrollTo({ top: 0, behavior: 'smooth' });

        e.stopPropagation();
        changeHeaderState({
            name: MENU_SUBCATEGORY,
            force: true,
            title,
            onBackClick: () => {
                this.setState(({ activeMenuItemsStack }) => ({ activeMenuItemsStack: activeMenuItemsStack.slice(1) }));
                goToPreviousHeaderState();
            },
            onCloseClick: () => {
                history.goBack();
                this.setState({ activeMenuItemsStack: [] });
            },
        });

        if (!activeMenuItemsStack.includes(item_id)) {
            this.setState({ activeMenuItemsStack: [item_id, ...activeMenuItemsStack] });
        }
    }

    render() {
        return <MenuOverlay {...this.props} {...this.state} {...this.containerFunctions} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuOverlayContainer);

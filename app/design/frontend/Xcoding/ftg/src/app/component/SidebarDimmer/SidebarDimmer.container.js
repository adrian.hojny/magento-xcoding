import { connect } from 'react-redux';

import {
    mapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
    SidebarContainer,
} from 'Component/Sidebar/Sidebar.container';
import SidebarDimmer from 'Component/SidebarDimmer/SidebarDimmer.component';

export const mapStateToProps = (state, { id }) => ({
    ...sourceMapStateToProps(state, { id }),
    isActive: state.OverlayReducer.isDimmerActive,
});

class SidebarDimmerContainer extends SidebarContainer {
    render() {
        return <SidebarDimmer {...this.containerFunctions} {...this.props} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SidebarDimmerContainer);

/* eslint-disable import/prefer-default-export */
export {
    BRAINTREE,
    CHECK_MONEY,
    KLARNA,
    PAYPAL_EXPRESS,
    PAYPAL_EXPRESS_CREDIT,
    STRIPE,
} from 'SourceComponent/CheckoutPayments/CheckoutPayments.config';

export const PRZELEWY24 = 'dialcom_przelewy';
export const PRZELEWY24_CARD = 'przelewy_card';

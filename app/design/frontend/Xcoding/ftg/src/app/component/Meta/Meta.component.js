import { Meta as SourceMeta } from 'SourceComponent/Meta/Meta.component';

export class Meta extends SourceMeta {
    renderTitle() {
        const { default_title, title_prefix, title_suffix, title } = this.props;

        const titlePrefix = title_prefix ? `${title_prefix} | ` : '';
        const titleSuffix = title_suffix ? ` - ${title_suffix}` : '';

        return <title>{`${titlePrefix}${title || default_title}${titleSuffix}`}</title>;
    }

    renderCanonical() {
        const { canonical_url } = this.props;

        if (!canonical_url) {
            return null;
        }
        const canonicalUrlPrefix = location.href.slice(0, location.href.indexOf(canonical_url));

        if (canonical_url.includes(canonicalUrlPrefix)) {
            return <link rel="canonical" href={canonical_url} />;
        }

        return <link rel="canonical" href={`${canonicalUrlPrefix}${canonical_url}`} />;
    }
}

export default Meta;

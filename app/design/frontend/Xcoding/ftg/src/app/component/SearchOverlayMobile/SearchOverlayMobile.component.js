import PropTypes from 'prop-types';

import SourceSearchOverlay from 'SourceComponent/SearchOverlay/SearchOverlay.component';

import Image from 'Component/Image';
import Link from 'Component/Link';
import Overlay from 'Component/Overlay';
import TextPlaceholder from 'Component/TextPlaceholder';
import media, { PRODUCT_MEDIA } from 'Util/Media';
import { roundPrice } from 'Util/Price';

import './SearchOverlayMobile.style.scss';

export const SEARCH_SELECTOR_OVERLAY_ID = 'search-mobile-details';

export default class SearchOverlayMobile extends SourceSearchOverlay {
    static propTypes = {
        ...super.propTypes,
        onSearchBarChange: PropTypes.func.isRequired,
    };

    static defaultProps = {
        searchCriteria: '',
    };

    state = {
        isActive: false,
    };

    handleChange = (e) => {
        const { onSearchBarChange } = this.props;
        onSearchBarChange(e);
    };

    renderSearchItemAdditionalContent(sku) {
        const { isLoading } = this.props;
        if (!isLoading && !sku) {
            return null;
        }

        return (
            <p block="SearchOverlayMobile" elem="Sku">
                <TextPlaceholder content={sku} />
            </p>
        );
    }

    renderSearchItemPrice(price) {
        if (!price) {
            return null;
        }

        const { minimum_price: { final_price, regular_price } = {} } = price;

        return (
            <div block="SearchOverlay" elem="Price">
                <span>{roundPrice(final_price.value)}</span>
                <span>{final_price.currency}</span>

                {roundPrice(regular_price.value) !== roundPrice(final_price.value) && (
                    <span block="SearchOverlay" elem="OldPrice">
                        <span>{roundPrice(regular_price.value)}</span>
                        <span>{regular_price.currency}</span>
                    </span>
                )}
            </div>
        );
    }

    renderSearchItemContent(name, sku, price) {
        return (
            <>
                {this.renderSearchItemAdditionalContent(sku)}
                <h4 block="SearchOverlayMobile" elem="Title" mods={{ isLoaded: !!name }}>
                    <TextPlaceholder content={name} length="long" />
                </h4>
                {this.renderSearchItemPrice(price)}
            </>
        );
    }

    renderSearchItem(product, i) {
        const { getProductLinkTo, handleItemClick } = this.props;

        let productOrVariant = product;

        if (product.variants?.length) {
            productOrVariant = product.variants[0];
        }

        const { name, sku, price_range } = productOrVariant;
        const { thumbnail: { path } = {} } = product;

        const imageSrc = path ? media(path, PRODUCT_MEDIA) : undefined;

        return (
            <li block="SearchOverlayMobile" elem="Item" key={i}>
                <Link block="SearchOverlayMobile" elem="Link" to={getProductLinkTo(product)} onClick={handleItemClick}>
                    <figure block="SearchOverlayMobile" elem="Wrapper">
                        <Image
                            block="SearchOverlayMobile"
                            elem="Image"
                            src={imageSrc}
                            alt={__('Product %s thumbnail.', name)}
                            isPlaceholder={!imageSrc}
                        />
                        <figcaption block="SearchOverlayMobile" elem="Content">
                            {this.renderSearchItemContent(name, sku, price_range)}
                        </figcaption>
                    </figure>
                </Link>
            </li>
        );
    }

    renderSearchCriteria() {
        const { searchCriteria } = this.props;

        return (
            <p block="SearchOverlayMobile" elem="Criteria" mods={{ isVisible: !!searchCriteria }}>
                {__('Results for:')}
                <strong>{searchCriteria}</strong>
            </p>
        );
    }

    renderNoSearchCriteria() {
        return null;
    }

    render() {
        const { searchCriteria, searchResults } = this.props;

        const { isActive } = this.state;

        return (
            <Overlay id={SEARCH_SELECTOR_OVERLAY_ID} mix={{ block: 'SearchOverlayMobile' }}>
                <div block="SearchOverlayMobile" elem="Container">
                    <input
                        id="search-field"
                        ref={this.searchBarRef}
                        block="SearchField"
                        elem="Input"
                        onFocus={() => this.setState({ isActive: true })}
                        onChange={this.handleChange}
                        onBlur={() => this.setState({ isActive: false })}
                        value={searchCriteria}
                        mods={{ isActive: isActive || !!searchCriteria }}
                        autoComplete="off"
                        minLength="1"
                    />
                    <div
                        block="SearchField"
                        elem="Placeholder"
                        mods={{
                            isActive: isActive || !!searchCriteria,
                            isPlaceholderVisible: !searchCriteria,
                        }}
                    >
                        <span>{__('Search')}</span>
                    </div>

                    {this.renderSearchCriteria()}
                    <article
                        block="SearchOverlayMobile"
                        elem={searchResults.length || searchCriteria ? 'Results' : 'NoResults'}
                        aria-label="Search results"
                    >
                        {this.renderSearchResults()}
                    </article>
                </div>
            </Overlay>
        );
    }
}

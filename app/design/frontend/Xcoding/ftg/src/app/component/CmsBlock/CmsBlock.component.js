import PropTypes from 'prop-types';

import SourceCmsBlock from 'SourceComponent/CmsBlock/CmsBlock.component';

import Html from 'Component/Html';

import './ProductCardDelivery.style.scss';

export default class CmsBlock extends SourceCmsBlock {
    static propTypes = {
        ...super.propTypes,
        withoutWrapper: PropTypes.bool,
        ignoredHtmlRules: PropTypes.array,
    };

    static defaultProps = {
        ...super.defaultProps,
        withoutWrapper: false,
    };

    render() {
        const {
            cmsBlock: { identifier, content, disabled },
        } = this.props;

        if (disabled) {
            return null;
        }

        if (identifier === undefined) {
            return this.renderPlaceholder();
        }

        const { withoutWrapper, ignoredHtmlRules } = this.props;

        if (withoutWrapper) return <Html content={content} />;

        return (
            <div block="CmsBlock" elem="Wrapper">
                <Html content={content} ignoredRules={ignoredHtmlRules} />
            </div>
        );
    }
}

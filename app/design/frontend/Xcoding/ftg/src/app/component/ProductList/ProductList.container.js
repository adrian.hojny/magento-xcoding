import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';

import {
    mapDispatchToProps as sourceMapDispatchToProps,
    ProductListContainer as SourceProductListContainer,
} from 'SourceComponent/ProductList/ProductList.container';

import ProductList from 'Component/ProductList/ProductList.component';
import { UidContext } from 'Context/UidContext';
import { changeNavigationState } from 'Store/Navigation/Navigation.action';

export const ADVERTISING_TILE_PREFIX = 'category-tile';

export const mapStateToProps = (state) => ({
    categoryUrlKey: state.CategoryReducer.category.url_key,
    categoryId: state.CategoryReducer.category.id,
    getNavigationStateById: (id) =>
        (state.NavigationReducer[id] && state.NavigationReducer[id].navigationState) || null,
    navigationState: state.NavigationReducer, // , // to have subscription on this prop and react on new tabs
    // tiles: (state.CmsBlocksAndSliderReducer.blocks
    //     .items[`${ADVERTISING_TILE_PREFIX}/${state.CategoryReducer.category.id}`] || {}).tiles
});

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    changeNavigationStateWithId: (id, state) => dispatch(changeNavigationState(id, state)),
});

class ProductListContainer extends SourceProductListContainer {
    requestPage = (currentPage = 1, isNext = false) => {
        const {
            sort,
            search,
            filter,
            pageSize,
            requestProductList,
            requestProductListInfo,
            noAttributes,
            noVariants,
        } = this.props;

        /**
         * In case the wrong category was passed down to the product list,
         * prevent it from being requested.
         */
        if (filter.categoryIds === -1) {
            return;
        }

        /**
         * Do not request page if there are no filters
         */
        if (!search && !this.isEmptyFilter()) {
            return;
        }

        // TODO: product list requests filters alongside the page
        // TODO: sometimes product list is requested more then once
        // TODO: the product list should not request itself, when coming from PDP

        const options = {
            isNext,
            noAttributes,
            noVariants,
            args: {
                sort,
                filter,
                search,
                pageSize,
                currentPage,
            },
        };

        const infoOptions = {
            args: {
                filter,
                search,
            },
        };

        window.scrollTo({ top: 0, behavior: 'smooth' });

        if (search) {
            options.args.sort = undefined;
        }

        requestProductList(options);
        requestProductListInfo(infoOptions);
    };

    render() {
        return (
            <UidContext.Consumer>
                {({ uid } = {}) => (
                    <ProductList {...this.props} {...this.containerFunctions} {...this.containerProps()} uid={uid} />
                )}
            </UidContext.Consumer>
        );
    }
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(ProductListContainer);

import { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { TOTALS } from 'Component/CheckoutBilling/CheckoutBilling.container';
import CmsBlock from 'Component/CmsBlock';
import { CLICKED_CATEGORIES } from 'Component/ProductList/ProductList.component';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { roundPrice } from 'Util/Price';
import { getProductsForGTM } from 'Util/Product/Product';
import OrderProducts from './OrderProducts/OrderProducts.component';

import './CheckoutSuccess.style.scss';

export const CHECKOUT_SUCCESS_MEESSAGE_ID = 'checkout-success-message';

export const mapStateToProps = (state) => ({
    config: state.ConfigReducer,
});

class CheckoutSuccess extends PureComponent {
    static propTypes = {
        orderData: PropTypes.object.isRequired,
        orderID: PropTypes.object.isRequired,
        config: PropTypes.object.isRequired,
    };

    sendGAEvent = true;

    componentDidMount() {
        this.handlePurchaseEvent();
    }

    componentDidUpdate() {
        if (this.sendGAEvent) {
            this.handlePurchaseEvent();
        }
    }

    handlePurchaseEvent = () => {
        const { orderData, config } = this.props;
        const cartTotals = BrowserDatabase.getItem(TOTALS) || {};
        const categoriesData = BrowserDatabase.getItem(CLICKED_CATEGORIES) || {};

        if (!!orderData && !!cartTotals && cartTotals?.items?.length) {
            const productsGTM = getProductsForGTM(cartTotals.items, categoriesData);

            gtag('event', 'purchase', {
                transaction_id: orderData?.base_order_info?.increment_id,
                affiliation: `Magento ${config?.code || ''}`,
                value: orderData?.base_order_info?.grand_total,
                currency: orderData?.currencyCode,
                tax: cartTotals?.tax_amount || 0,
                shipping: orderData?.shipping_info?.shipping_incl_tax || 0,
                coupon: cartTotals?.coupon_code || '',
                items: Array.from(productsGTM),
            });

            const content_ids = cartTotals.items.reduce((acc, product) => {
                return [...acc, product.sku];
            }, []);

            if (window?.fbq) {
                fbq('track', 'Purchase', {
                    content_ids,
                    content_type: 'product',
                    value: orderData?.base_order_info?.grand_total,
                    currency: orderData?.currencyCode,
                });
            }

            BrowserDatabase.deleteItem(CLICKED_CATEGORIES);
            BrowserDatabase.deleteItem(TOTALS);

            this.sendGAEvent = false;
        }
    };

    renderBottomCMSBlock() {
        return (
            <div block="CheckoutSuccess" elem="BottomBlock">
                <CmsBlock identifier="checkout-success-message" withoutWrapper />
            </div>
        );
    }

    renderOrderParams() {
        const { orderData: { base_order_info, order_number, isBlik, isP24 } = {} } = this.props;

        const { increment_id, status_label } = base_order_info || {};

        const status = isBlik
            ? __('Confirm payment on your bank account. You will receive payment status confirmation on email')
            : status_label;

        return (
            <>
                <div block="CheckoutSuccess" elem="Param" mods={{ isPrimary: true }}>
                    <div>{`${__('Order confirmation no')}:`}</div>
                    <div>{increment_id || order_number}</div>
                </div>
                <div block="CheckoutSuccess" elem="Param">
                    <div>{`${__('Order status')}:`}</div>
                    <div>{status}</div>
                </div>
            </>
        );
    }

    renderEmailInfo() {
        const {
            orderData: { email },
        } = this.props;

        return (
            <div block="CheckoutSuccess" elem="Section" mods={{ withEnvelope: true, fullWidth: true }}>
                <p>{`${__('The order confirmation and invoice have been sent to')} ${email}`}</p>
            </div>
        );
    }

    renderCustomerData() {
        const {
            orderData: {
                email,
                billing_address: { firstname, lastname } = {},
                shipping_info: { shipping_address: { telephone } = {} },
            },
        } = this.props;

        return (
            <div block="CheckoutSuccess" elem="Section" mods={{ withCheckmark: true }}>
                <h4>{__('Details of the ordering party')}</h4>
                <div>{email}</div>
                <div>{`${firstname} ${lastname}`}</div>
                <div>{telephone}</div>
            </div>
        );
    }

    renderShippingAddress() {
        const {
            orderData: {
                shipping_info: {
                    shipping_address: { street, house_number, apartment_number, postcode, city, company },
                },
            },
        } = this.props;

        return (
            <div block="CheckoutSuccess" elem="Section" mods={{ withCheckmark: true }}>
                <h4>{__('Delivery address')}</h4>
                {company && <div>{company}</div>}
                <div>{`${street} ${house_number || ''}${apartment_number ? ` / ${apartment_number}` : ''}`}</div>
                <div>{`${postcode} ${city}`}</div>
            </div>
        );
    }

    renderShippingMethod() {
        const {
            orderData: {
                shipping_info: { shipping_description },
            },
        } = this.props;

        return (
            <div block="CheckoutSuccess" elem="Section" mods={{ withCheckmark: true }}>
                <h4>{__('Shipping method')}</h4>
                {shipping_description}
            </div>
        );
    }

    renderDeliveryPoint() {
        const {
            orderData: { shippingAdditionalInformation },
        } = this.props;

        if (!shippingAdditionalInformation || !shippingAdditionalInformation.inpost) return null;

        const {
            inpost: {
                address: { line1, line2 },
                name,
            },
        } = shippingAdditionalInformation;

        return (
            <div block="CheckoutSuccess" elem="Section" mods={{ withCheckmark: true }}>
                <h4>{__('Delivery point')}</h4>
                {`${name}, ${line1}, ${line2}`}
            </div>
        );
    }

    renderPaymentInfo() {
        const {
            orderData: {
                payment_info: {
                    additional_information: { method_title },
                },
                paymentInstruction,
            },
        } = this.props;

        return (
            <div block="CheckoutSuccess" elem="Section" mods={{ withCheckmark: true, fullWidth: true }}>
                <h4>{__('Selected payment method')}</h4>
                {`${method_title}\n\n${paymentInstruction || ''}`}
            </div>
        );
    }

    renderCostSummary() {
        const {
            orderData: {
                base_order_info: { grand_total },
                currencyCode,
                shippingAmount,
                shipping_info,
            },
        } = this.props;
        const currencyString = currencyCode;
        const shippingCost = shippingAmount || shipping_info?.shipping_incl_tax;

        return (
            <>
                <div block="CheckoutSuccess" elem="Param">
                    <div>{`${__('Delivery cost')}:`}</div>
                    <div>{shippingCost ? `${roundPrice(shippingCost)} ${currencyString}` : __('FREE')}</div>
                </div>
                <div block="CheckoutSuccess" elem="Param" mods={{ isSecondary: true }}>
                    <div>{__('Grand total: ')}</div>
                    <div>{`${roundPrice(grand_total)} ${currencyString}`}</div>
                </div>
            </>
        );
    }

    renderOrderProducts() {
        const {
            orderData: {
                currencyCode,
                order_products,
                base_order_info: { total_qty_ordered },
            },
        } = this.props;
        const data = {
            order_products,
            total_qty_ordered,
            currencyCode,
        };

        return <OrderProducts data={data} />;
    }

    renderSideGallery = (right = false) => {
        const {
            orderData: { order_products },
        } = this.props;
        const gallery = order_products[0].media_gallery_entries || [];
        const filteredGallery = right ? gallery.slice(3, 6) : gallery.slice(0, 3);

        return filteredGallery.map((item) => <img src={item?.base?.url} key={item?.id} alt={item?.label} />);
    };

    render() {
        return (
            <>
                <div block="CheckoutSuccess" elem="Wrapper">
                    <div>{this.renderSideGallery()}</div>
                    <div>
                        <h2>{__('Thank you for your order')}</h2>
                        <div block="CheckoutSuccess" elem="Details">
                            {this.renderOrderParams()}
                            <div block="CheckoutSuccess" elem="SectionWrapper">
                                {this.renderEmailInfo()}
                                {this.renderCustomerData()}
                                {this.renderShippingAddress()}
                                {this.renderShippingMethod()}
                                {this.renderDeliveryPoint()}
                                {this.renderPaymentInfo()}
                            </div>
                            {this.renderOrderProducts()}
                            {this.renderCostSummary()}
                        </div>
                        {this.renderBottomCMSBlock()}
                    </div>
                    <div>{this.renderSideGallery(true)}</div>
                </div>
            </>
        );
    }
}

export default connect(mapStateToProps)(CheckoutSuccess);

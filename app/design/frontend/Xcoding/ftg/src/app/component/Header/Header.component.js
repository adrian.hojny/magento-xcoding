import React, { Suspense } from 'react';

import SourceHeader from 'SourceComponent/Header/Header.component';
import {
    CART,
    CART_EDITING,
    CART_OVERLAY,
    CATEGORY,
    CHECKOUT,
    CMS_PAGE,
    CUSTOMER_ACCOUNT,
    CUSTOMER_SUB_ACCOUNT,
    FILTER,
    MENU,
    MENU_SUBCATEGORY,
    PDP,
    POPUP,
    SEARCH,
} from 'SourceComponent/Header/Header.config';

import CartOverlay from 'Component/CartOverlay/CartOverlay.container';
import ClickOutside from 'Component/ClickOutside';
import HeaderInfoBar from 'Component/HeaderInfoBar';
import LangPanel from 'Component/LangPanel';
import Link from 'Component/Link';
import Menu from 'Component/Menu';
import { DEFAULT_STATE_NAME } from 'Component/NavigationAbstract/NavigationAbstract.config';
import OfflineNotice from 'Component/OfflineNotice/OfflineNotice.container';
import PopupSuspense from 'Component/PopupSuspense';
import { PREMIUM } from 'Component/Router/Router.component';
import SearchField from 'Component/SearchField';
import { SEARCH_SELECTOR_OVERLAY_ID } from 'Component/SearchOverlayMobile/SearchOverlayMobile.component';
import SearchOverlayMobile from 'Component/SearchOverlayMobile/SearchOverlayMobile.container';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { getStaticFilePath } from 'Util/Image/Image';
import { getLangPrefix } from 'Util/Lang/Lang';
import isMobile from 'Util/Mobile';
import Logo from '../../style/images/logo.svg';
// import LogoPrm from '../../style/images/logo_prm.svg';
import { ReactComponent as LogoPrm } from '../../style/images/logo_prm.svg';

import 'Component/MegaMenu/MegaMenu.style.scss';

export const FILTER_DETAIL = 'filter-detail';

export {
    CART,
    CART_EDITING,
    CART_OVERLAY,
    CATEGORY,
    CHECKOUT,
    CMS_PAGE,
    CUSTOMER_ACCOUNT,
    CUSTOMER_ACCOUNT_PAGE,
    CUSTOMER_SUB_ACCOUNT,
    FILTER,
    HOME_PAGE,
    MENU,
    MENU_SUBCATEGORY,
    MY_ACCOUNT,
    PDP,
    POPUP,
    SEARCH,
} from 'SourceComponent/Header/Header.config';

export const DEFAULT_HEADER_STATE = {
    logo: true,
    searchButton: true,
    wishlist: true,
    minicart: true,
    menu: true,
    infoBar: true,
};

export const LANG_PANEL = 'LANG_PANEL';

export const STATE_MAP = {
    [DEFAULT_STATE_NAME]: DEFAULT_HEADER_STATE,
    [PDP]: DEFAULT_HEADER_STATE,
    [CATEGORY]: DEFAULT_HEADER_STATE,
    [SEARCH]: DEFAULT_HEADER_STATE,
    [SEARCH_SELECTOR_OVERLAY_ID]: {
        title: true,
        close: true,
    },
    [POPUP]: {
        title: true,
        close: true,
    },
    [CUSTOMER_ACCOUNT]: {
        title: true,
        close: true,
    },
    [CUSTOMER_SUB_ACCOUNT]: {
        title: true,
        back: true,
    },
    [MENU]: {
        title: true,
        close: true,
    },
    [MENU_SUBCATEGORY]: {
        back: true,
        title: true,
        close: true,
    },
    [CART]: {
        title: true,
        close: true,
    },
    [CART_EDITING]: {
        title: true,
        cancel: true,
    },
    [CART_OVERLAY]: {
        title: true,
        close: true,
    },
    [FILTER]: {
        close: true,
        title: true,
    },
    [FILTER_DETAIL]: {
        close: true,
        title: true,
        back: true,
    },
    [CHECKOUT]: {
        logo: true,
        back: true,
        account: true,
        checkoutBack: true,
    },
    [CMS_PAGE]: DEFAULT_HEADER_STATE,
    [LANG_PANEL]: {
        title: true,
        close: true,
    },
};

export default class Header extends SourceHeader {
    static defaultProps = {
        logo_alt: 'SneakerStudio logo',
        showMyAccountLogin: false,
        header_logo_src: '',
        isLoading: true,
    };

    renderMap = {
        cancel: this.renderCancelButton.bind(this),
        checkoutBack: this.renderCheckoutBackButton.bind(this),
        back: this.renderBackButton.bind(this),
        menu: this.renderMenuButton.bind(this),
        lang: this.renderLanguageSwitch.bind(this),
        search: this.renderSearchField.bind(this),
        title: this.renderTitle.bind(this),
        close: this.renderCloseButton.bind(this),
        logo: this.renderLogo.bind(this),
        searchButton: this.renderSearchButton.bind(this),
        account: this.renderAccountButton.bind(this),
        wishlist: this.renderWishlistButton.bind(this),
        minicart: this.renderMinicart.bind(this),
        edit: this.renderEditButton.bind(this),
        ok: this.renderOkButton.bind(this),
    };

    stateMap = STATE_MAP;

    handleActiveMegaMenu = (id) => () => {
        const { setActiveMegaMenu } = this.props;
        setActiveMegaMenu(id);
    };

    renderTopMenu() {
        const { menu } = this.props;
        const categoryArray = Object.values(menu);

        return (
            <nav block="Header" elem="TopMenu">
                <ul block="Header" elem="TopMenuWrapper">
                    {this.renderFirstLevel(categoryArray)}
                </ul>
            </nav>
        );
    }

    renderMenu() {
        const { isCheckout } = this.props;

        if (isMobile.any() || isCheckout) {
            return null;
        }

        return <Menu />;
    }

    renderLogoImage() {
        const { logo_alt } = this.props;
        const isPrm = BrowserDatabase.getItem(PREMIUM);

        if (isPrm) {
            return <LogoPrm />;
        }

        return <img src={Logo} alt={logo_alt} />;
    }

    renderLogo(isVisible = false) {
        const { isLoading, onCloseButtonClick } = this.props;

        if (isLoading) return null;

        return (
            <Link
                to="/"
                aria-hidden={!isVisible}
                tabIndex={isVisible ? 0 : -1}
                block="Header"
                elem="LogoWrapper"
                mods={{ isVisible }}
                key="logo"
                onClick={onCloseButtonClick}
            >
                {this.renderLogoImage()}
            </Link>
        );
    }

    renderWishlistButton(isVisible = false) {
        return (
            <Link to="/stash" block="Header" elem="Button" mods={{ isVisible, type: 'wishlist' }} key="wishlist">
                {this.renderWishlistItemsQty()}
            </Link>
        );
    }

    renderWishlistItemsQty() {
        const { wishlistCount } = this.props;

        if (!wishlistCount) return null;

        return (
            <span block="Header" elem="WishlistItemCount" mix={{ block: 'Header', elem: 'Count' }}>
                {wishlistCount}
            </span>
        );
    }

    renderClubButton(isVisible = false) {
        return <button block="Header" elem="Button" mods={{ isVisible, type: 'club' }} key="club" />;
    }

    renderMinicartItemsQty() {
        const {
            cartTotals: { items_qty },
        } = this.props;

        if (!items_qty) return null;

        return (
            <span
                aria-label="Items in cart"
                block="Header"
                elem="MinicartItemCount"
                mix={{ block: 'Header', elem: 'Count' }}
            >
                {items_qty}
            </span>
        );
    }

    renderAccountButton(isVisible = false) {
        const { onMyAccountOutsideClick, onMyAccountButtonClick } = this.props;

        return (
            <ClickOutside onClick={onMyAccountOutsideClick} key="account">
                <div aria-label="My account" block="Header" elem="MyAccount">
                    <button
                        block="Header"
                        elem="Button"
                        mods={{ isVisible, type: 'account' }}
                        onClick={onMyAccountButtonClick}
                        aria-label="Open my account"
                    />
                </div>
            </ClickOutside>
        );
    }

    renderMinicartOverlayFallback() {
        return <PopupSuspense actualOverlayKey={CART_OVERLAY} />;
    }

    renderMinicartOverlay() {
        const { shouldRenderCartOverlay, hideActiveOverlay, goToPreviousNavigationState } = this.props;

        const closeOverlay = () => {
            hideActiveOverlay();
            goToPreviousNavigationState();
        };

        if (!shouldRenderCartOverlay) {
            return null;
        }

        return (
            <Suspense fallback={this.renderMinicartOverlayFallback()}>
                <CartOverlay closeOverlay={closeOverlay} />
            </Suspense>
        );
    }

    renderMinicartButton() {
        const {
            onMinicartButtonClick,
            navigationState: { name },
        } = this.props;

        return (
            <button
                block="Header"
                elem="MinicartButtonWrapper"
                mods={{ isOpen: name === CART_OVERLAY }}
                tabIndex="0"
                onClick={onMinicartButtonClick}
            >
                {this.renderMinicartItemsQty()}
            </button>
        );
    }

    renderMinicart(isVisible = false) {
        const {
            onMinicartOutsideClick,
            isCheckout,
            navigationState: { name },
        } = this.props;

        if (isCheckout) {
            return null;
        }

        return (
            <ClickOutside onClick={onMinicartOutsideClick} key="minicart">
                <div block="Header" elem="Button" mods={{ isVisible, type: 'minicart', isOpen: name === CART_OVERLAY }}>
                    {this.renderMinicartButton()}
                    {this.renderMinicartOverlay()}
                </div>
            </ClickOutside>
        );
    }

    handleOpenLangPanel = () => {
        const { showOverlay } = this.props;
        showOverlay(LANG_PANEL);
    };

    renderLanguageSwitch(isVisible = false) {
        const lang = getLangPrefix();

        return (
            <div aria-label="Language" block="Header" elem="Language" key="lang" onClick={this.handleOpenLangPanel}>
                <button block="Header" elem="Button" mods={{ isVisible, type: 'lang' }} aria-label="Change language">
                    <img src={getStaticFilePath(`/assets/images/flags/${lang}.png`)} alt={lang} />
                </button>
            </div>
        );
    }

    renderMenuButton(isVisible = false) {
        const { onMenuButtonClick } = this.props;

        return (
            <div block="Header" elem="MenuWrapper" key="menu_button">
                <button
                    block="Header"
                    elem="Button"
                    mods={{ isVisible, type: 'menu' }}
                    aria-label="Go to menu and search"
                    aria-hidden={!isVisible}
                    tabIndex={isVisible ? 0 : -1}
                    onClick={() => onMenuButtonClick()}
                />
            </div>
        );
    }

    renderSearchField(isSearchVisible = false) {
        const {
            searchCriteria,
            onSearchOutsideClick,
            onSearchBarFocus,
            onSearchBarChange,
            onClearSearchButtonClick,
            navigationState: { name },
            isCheckout,
            hideActiveOverlay,
        } = this.props;

        if (isCheckout) {
            return null;
        }

        return (
            <SearchField
                key="search"
                searchCriteria={searchCriteria}
                onSearchOutsideClick={onSearchOutsideClick}
                onSearchBarFocus={onSearchBarFocus}
                onSearchBarChange={onSearchBarChange}
                onClearSearchButtonClick={onClearSearchButtonClick}
                isVisible={isSearchVisible}
                isActive={name === SEARCH}
                hideActiveOverlay={hideActiveOverlay}
            />
        );
    }

    onSearchButtonClick() {
        const { showOverlay, setNavigationState } = this.props;

        showOverlay(SEARCH_SELECTOR_OVERLAY_ID);

        setNavigationState({ name: SEARCH_SELECTOR_OVERLAY_ID, title: __('Search title') });
    }

    renderSearchButton(isVisible = false) {
        const { searchCriteria, onSearchBarChange, onClearSearchButtonClick } = this.props;

        return (
            <React.Fragment key="searchButton">
                <button
                    block="Header"
                    elem="Button"
                    mods={{ isVisible, type: 'search' }}
                    key="searchButton"
                    onClick={() => this.onSearchButtonClick()}
                />
                <SearchOverlayMobile
                    onSearchBarChange={onSearchBarChange}
                    searchCriteria={searchCriteria}
                    clearSearch={onClearSearchButtonClick}
                />
            </React.Fragment>
        );
    }

    renderCheckoutBackButton(isVisible = false) {
        const { onCheckoutBackButtonClick } = this.props;

        return (
            <button
                block="Header"
                elem="Button"
                mods={{ isVisible, type: 'checkoutBack' }}
                key="checkoutBack"
                onClick={onCheckoutBackButtonClick}
            >
                {__('Go back')}
                <span>{__('to cart')}</span>
            </button>
        );
    }

    render() {
        const {
            navigationState: { name },
            isCheckout,
        } = this.props;

        return (
            <>
                <header block="Header" mods={{ name, isCheckout }}>
                    <HeaderInfoBar />
                    <nav block="Header" elem="Nav">
                        {this.renderNavigationState()}
                    </nav>
                    {this.renderMenu()}
                </header>
                <LangPanel />
                <OfflineNotice />
            </>
        );
    }
}

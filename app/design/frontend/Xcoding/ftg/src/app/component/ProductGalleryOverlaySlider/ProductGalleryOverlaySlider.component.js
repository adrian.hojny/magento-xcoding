import React from 'react';
import { withRouter } from 'react-router';

import { ProductGallery as SourceProductGallery } from 'Component/ProductGallery/ProductGallery.component';
import Slider from 'Component/Slider';
import { PRODUCT_GALLERY_OVERLAY_KEY } from 'Route/ProductPage/ProductPage.component';

import './ProductGalleryOverlaySlider.style';

export * from 'Component/ProductGallery/ProductGallery.component';

class ProductGalleryOverlaySliderComponent extends SourceProductGallery {
    handleLeftArrowClick = this.handleLeftArrowClick.bind(this);

    handleRightArrowClick = this.handleRightArrowClick.bind(this);

    handleKeyDown = this.handleKeyDown.bind(this);

    componentDidUpdate(prevProps) {
        const {
            productId,
            location: { pathname },
            activeOverlay,
        } = this.props;
        const {
            productId: prevProductId,
            location: { pathname: prevPathname },
        } = prevProps;

        if (productId !== prevProductId) {
            this.updateSharedDestinationElement();
        }

        if (this.sliderRef?.current?.draggableRef && pathname !== prevPathname) {
            setTimeout(() => {
                CSS.setVariable(this.sliderRef.current.draggableRef, 'animation-speed', 0);
            }, 0);
        }

        if (activeOverlay === PRODUCT_GALLERY_OVERLAY_KEY) {
            document.addEventListener('keydown', this.handleKeyDown);
        } else {
            document.removeEventListener('keydown', this.handleKeyDown);
        }
    }

    handleLeftArrowClick() {
        const { onActiveImageChange, activeImageOverlayGallery } = this.props;

        if (activeImageOverlayGallery > 0) {
            onActiveImageChange(activeImageOverlayGallery - 1);
        }
    }

    handleRightArrowClick() {
        const { onActiveImageChange, gallery, activeImageOverlayGallery } = this.props;

        if (activeImageOverlayGallery < gallery.length - 1) {
            onActiveImageChange(activeImageOverlayGallery + 1);
        }
    }

    handleKeyDown(e) {
        const { hideActiveOverlay } = this.props;

        switch (e.key) {
            case 'ArrowLeft':
                this.handleLeftArrowClick();
                break;
            case 'ArrowRight':
                this.handleRightArrowClick();
                break;
            case 'Escape':
                hideActiveOverlay();
                break;
            default:
                break;
        }
    }

    renderVideo(media, index) {
        const { video_content: { video_url } = {} } = media;
        const videoType = `video/${video_url.split('.').pop()}`;

        return (
            <div key={index}>
                <video width="100%" height="100%" controls>
                    <source src={video_url} type={videoType} />
                </video>
            </div>
        );
    }

    renderArrows() {
        return (
            <div block="Slider" elem="Arrows">
                <button block="Slider" elem="Arrow" mods={{ left: true }} onClick={this.handleLeftArrowClick} />
                <button block="Slider" elem="Arrow" mods={{ right: true }} onClick={this.handleRightArrowClick} />
            </div>
        );
    }

    renderAdditionalPictures() {
        const { gallery, productName } = this.props;

        if (gallery.length === 1) {
            return (
                <div block="ProductGallery" elem="AdditionalWrapper">
                    <div block="ProductGallery" elem="Additional" />
                    <div block="ProductGallery" elem="AdditionalProductInfo" />
                </div>
            );
        }

        return (
            <div block="ProductGallery" elem="AdditionalWrapper">
                {this.renderArrows()}
                <div block="ProductGallery" elem="Additional">
                    {gallery.map(this.renderAdditionalPicture)}
                </div>
                <div block="ProductGallery" elem="AdditionalProductInfo">
                    {productName}
                </div>
            </div>
        );
    }

    renderSlider() {
        const { gallery, isZoomEnabled, onActiveImageChange, activeImageOverlayGallery } = this.props;

        return (
            <div ref={this.imageRef}>
                <Slider
                    ref={this.sliderRef}
                    mix={{ block: 'ProductGallery', elem: 'Slider' }}
                    activeImage={activeImageOverlayGallery}
                    onActiveImageChange={onActiveImageChange}
                    isInteractionDisabled={isZoomEnabled}
                >
                    {gallery.map(this.renderSlide)}
                </Slider>
                {this.renderGalleryNotice()}
            </div>
        );
    }

    render() {
        return (
            <div block="ProductGallery">
                {this.renderSlider()}
                {this.renderAdditionalPictures()}
            </div>
        );
    }
}

export default withRouter(ProductGalleryOverlaySliderComponent);

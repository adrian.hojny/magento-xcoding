import { connect } from 'react-redux';

import {
    mapDispatchToProps as sourceMapDispatchToProps,
    mapStateToProps,
    SearchOverlayContainer as SourceSearchOverlayContainer,
} from 'SourceComponent/SearchOverlay/SearchOverlay.container';

import { hideActiveOverlay } from 'Store/Overlay/Overlay.action';
import SearchOverlayMobile from './SearchOverlayMobile.component';

export const mapDispatchToProps = (dispatch) => ({
    ...sourceMapDispatchToProps(dispatch),
    hideActiveOverlay: () => dispatch(hideActiveOverlay()),
});

export class SearchOverlayMobileContainer extends SourceSearchOverlayContainer {
    containerFunctions = {
        getProductLinkTo: this.getProductLinkTo.bind(this),
        makeSearchRequest: this.makeSearchRequest.bind(this),
        handleItemClick: this.handleItemClick.bind(this),
    };

    getProductLinkTo(product) {
        const { url } = product;

        if (!url) return {};

        return {
            pathname: url,
            state: { product },
        };
    }

    handleItemClick() {
        const { hideActiveOverlay, clearSearch } = this.props;

        clearSearch();
        hideActiveOverlay();
    }

    makeSearchRequest() {
        const { makeSearchRequest, searchCriteria } = this.props;

        if (searchCriteria) {
            makeSearchRequest({
                args: {
                    search: searchCriteria,
                    pageSize: 12,
                    currentPage: 1,
                },
            });
        }
    }

    render() {
        return <SearchOverlayMobile {...this.props} {...this.containerFunctions} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchOverlayMobileContainer);

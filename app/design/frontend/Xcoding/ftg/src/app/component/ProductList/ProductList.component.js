import React, { createRef } from 'react';
import debounceRender from 'react-debounce-render';
import PropTypes from 'prop-types';

import { ProductList as SourceProductList } from 'SourceComponent/ProductList/ProductList.component';

import CarouselWrapper from 'Component/CarouselWrapper';
import CategoryPagination from 'Component/CategoryPagination/CategoryPagination.container';
import ComingSoonProductCard from 'Component/ComingSoonProductCard';
import Html from 'Component/Html';
import Image from 'Component/Image';
import Link from 'Component/Link';
import ProductCard from 'Component/ProductCard';
import { DEFAULT_PLACEHOLDER_COUNT } from 'Component/ProductListPage/ProductListPage.config';
import StyledSlickSlider from 'Component/StyledSlickSlider';
import CmsBlockQuery from 'Query/CmsBlock.query';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import { makeCancelable } from 'Util/Promise';
import { prepareQuery } from 'Util/Query';
import { executeGet } from 'Util/Request';
import { hash } from 'Util/Request/Hash';
import { ONE_MONTH_IN_SECONDS } from 'Util/Request/QueryDispatcher';
import { ADVERTISING_TILE_PREFIX } from './ProductList.container';

import './ProductList.extend.style';

export const CLICKED_CATEGORIES = 'CLICKED_CATEGORIES';
const ONE_YEAR_IN_SECONDS = 2630000;
const DEFAULT_GRID = 4;
const GRID_OPTIONS_BUTTON1 = 3;
const GRID_OPTIONS_BUTTON2 = 4;

const LIST_PLACEHOLDER_COUNT = DEFAULT_PLACEHOLDER_COUNT;
const SLIDER_PLACEHOLDER_COUNT = 4;

class ProductList extends SourceProductList {
    static propTypes = {
        ...super.propTypes,
        categoryUrlKey: PropTypes.string,
    };

    state = {
        isDescriptionVisible: true,
        gridMode: this.getCurrentGridMode(),
        adTiles: null,
        notSticky: false,
    };

    handleDescriptionButtonSticky = this.handleDescriptionButtonSticky.bind(this);

    descriptionButtonRef = createRef();

    dataModelName = 'DataContainer';

    componentDidMount() {
        const {
            brandName,
            filter: { categoryIds },
        } = this.props;

        window.addEventListener('scroll', this.handleDescriptionButtonSticky);

        this.handleHeaderHeight();

        if (categoryIds) {
            this.getCmsBlock(`${ADVERTISING_TILE_PREFIX}/${categoryIds}`, categoryIds);
        }

        const isTabbed = brandName && brandName.length > 0;

        if (isTabbed) {
            this.handleTabbedList();
        }
    }

    componentDidUpdate(prevProps) {
        const {
            brandName,
            filter: { categoryIds },
        } = this.props;
        const { adTiles } = this.state;
        const tilesObj = !!adTiles && adTiles[categoryIds];
        const isTabbed = brandName && brandName.length > 0;

        if (isTabbed) {
            this.handleTabbedList();
        }

        if (categoryIds !== prevProps.filter?.categoryIds && !tilesObj) {
            this.getCmsBlock(`${ADVERTISING_TILE_PREFIX}/${categoryIds}`, categoryIds);
        }

        this.handleDescriptionButtonWidth();
    }

    componentWillUnmount() {
        if (this.observer && this.observer.disconnect) {
            this.observer.disconnect();
        }

        this.observer = null;

        window.removeEventListener('scroll', this.handleDescriptionButtonSticky);
    }

    getCurrentGridMode() {
        return BrowserDatabase.getItem('storageGridMode') || DEFAULT_GRID;
    }

    fetchData(rawQueries, onSucces = () => {}, onError = () => {}) {
        const preparedQuery = prepareQuery(rawQueries);
        const { query, variables } = preparedQuery;
        const queryHash = hash(query + JSON.stringify(variables));
        if (!window.dataCache) {
            window.dataCache = {};
        }

        if (window.dataCache[queryHash]) {
            onSucces(window.dataCache[queryHash]);
            return;
        }

        this.promise = makeCancelable(executeGet(preparedQuery, this.dataModelName, ONE_MONTH_IN_SECONDS));

        this.promise.promise.then(
            (response) => {
                window.dataCache[queryHash] = response;
                onSucces(response);
            },
            (err) => onError(err),
        );
    }

    getCmsBlock(identifier, category) {
        this.fetchData([CmsBlockQuery.getQuery({ identifiers: [identifier] })], ({ cmsBlocks: { items } }) => {
            if (!items.length) {
                return;
            }

            const adTiles = {
                [category]: {
                    cmsBlock: items[0],
                    tiles: this.mapHtmlTilesToObject(items[0].content),
                },
            };

            this.setState({ adTiles });
        });
    }

    handleHeaderHeight() {
        const header = document.querySelector('header.Header');

        document.documentElement.style.setProperty('--hide-description-top', `${header.offsetHeight}px`);
        this.setState({
            headerHeight: header.offsetHeight,
        });
    }

    handleDescriptionButtonWidth() {
        const descriptionButtonWidth = this.descriptionButtonRef.current?.clientWidth;

        document.documentElement.style.setProperty('--hide-description-height', `${descriptionButtonWidth}px`);
        this.setState({
            descriptionButtonWidth,
        });
    }

    handleDescriptionButtonSticky() {
        const { headerHeight, descriptionButtonWidth } = this.state;
        const { currentPage } = this.props;
        const listingHeight = this.nodes[currentPage]?.scrollHeight;
        const listingScroll = this.nodes[currentPage]?.getBoundingClientRect()?.y;
        const descriptionButtonBottom = descriptionButtonWidth + headerHeight;

        if (listingScroll + listingHeight <= descriptionButtonBottom) {
            this.setState({
                notSticky: true,
            });
        } else {
            this.setState({
                notSticky: false,
            });
        }
    }

    renderAdvertisingTile(position) {
        const {
            currentPage,
            filter: { categoryIds },
        } = this.props;
        const { adTiles } = this.state;
        const tilesObj = !!adTiles && adTiles[categoryIds];

        if (currentPage !== 1 || !tilesObj || !tilesObj?.tiles || !tilesObj?.tiles[position]) {
            return null;
        }

        const content = tilesObj.tiles[position];

        return (
            <li block="ProductCard" mix={{ block: 'CategoryProductList', elem: 'TileWrapper' }} key={content}>
                <Html content={content} />
            </li>
        );
    }

    handleProductClick = (product) => {
        const { search } = this.props;
        const data = BrowserDatabase.getItem(CLICKED_CATEGORIES) || {};
        const extendedData = {
            ...data,
            [product.id]: search ? 'Search Results' : this.props.categoryUrlKey,
        };

        BrowserDatabase.setItem(extendedData, CLICKED_CATEGORIES);
    };

    renderPages() {
        const {
            isLoading,
            pages,
            mix,
            addStaticImages,
            staticImages,
            currentPage,
            widget,
            brandName,
            uid = 'single_carousel_per_page',
            navigationState,
            totalItems,
            withoutSlider,
            productsCount,
        } = this.props;
        const { isDescriptionVisible } = this.state;
        const widgetPlaceholders = productsCount < SLIDER_PLACEHOLDER_COUNT ? productsCount : SLIDER_PLACEHOLDER_COUNT;

        if (isLoading) {
            const numberOfPlaceholders = widget ? widgetPlaceholders : LIST_PLACEHOLDER_COUNT;

            return this.renderPage({ numberOfPlaceholders });
        }

        if (widget) {
            if (addStaticImages && pages[currentPage].length <= totalItems) {
                const images = JSON.parse(atob(staticImages));

                images.forEach((image) => {
                    if (image.position) {
                        pages[currentPage].splice(parseInt(image.position, 10) - 1, 0, image);
                    }
                });
            }

            const isTabbed = brandName && brandName.length > 0;
            const isCurrent = isTabbed && navigationState[uid].navigationState.name === brandName;

            return Object.entries(pages).map(([pageNumber, items = []]) => (
                <ul
                    block="CategoryProductList"
                    elem="Page"
                    mix={{ ...mix, elem: 'Carousel' }}
                    mods={{ hideDescriptions: !isDescriptionVisible, isTabbed, isCurrent }}
                    key={`${pageNumber}`}
                    ref={(node) => {
                        this.nodes[pageNumber] = node;
                    }}
                >
                    {withoutSlider ? this.renderProductCard(items) : this.renderCarousel(items)}
                </ul>
            ));
        }

        return Object.entries(pages).map(([pageNumber, items = []]) => (
            <div key={pageNumber}>
                {this.renderDescriptionButton()}
                <ul
                    block="CategoryProductList"
                    elem="Page"
                    mix={{ ...mix, elem: 'Page' }}
                    mods={{ hideDescriptions: !isDescriptionVisible }}
                    ref={(node) => {
                        this.nodes[pageNumber] = node;
                    }}
                >
                    {this.renderProductCard(items)}
                </ul>
            </div>
        ));
    }

    renderCarousel(items) {
        const { selectedFilters, isComingSoon } = this.props;

        if (isComingSoon) {
            return (
                <div block="ComingSoon" elem="List">
                    {items.flatMap((product) => (
                        <ComingSoonProductCard product={product} key={product.id} selectedFilters={selectedFilters} />
                    ))}
                </div>
            );
        }

        return (
            <StyledSlickSlider alignCenter block="ProductListCarousel" infinite={false} lazyLoad={false} variableWidth>
                {this.renderProductCard(items, true)}
            </StyledSlickSlider>
        );
    }

    renderProductCard(items, isCarousel) {
        const { selectedFilters, simpleCard, withoutItemProps } = this.props;

        return items.flatMap((product) =>
            product.image_url
                ? [this.renderStaticImageTile(product)]
                : [
                      <ProductCard
                          product={product}
                          key={product.id}
                          selectedFilters={selectedFilters}
                          withoutItemProps={isCarousel || withoutItemProps}
                          simpleCard={simpleCard}
                          onProductClick={this.handleProductClick}
                      />,
                  ],
        );
    }

    renderStaticImageTile(image) {
        return (
            <li
                block="ProductCard"
                mix={{ block: 'ProductCard', elem: 'StaticImageWrapper' }}
                key={image.image_url + image.position}
            >
                <Link block="ProductCard" elem="Link" to={image.redirect_url} onClick={this.registerSharedElement}>
                    <Image mix={{ block: 'ProductCard', elem: 'StaticImage' }} src={image.image_url} alt={image.name} />
                </Link>
            </li>
        );
    }

    renderDescriptionButton() {
        const { notSticky } = this.state;
        const buttonText = this.state.isDescriptionVisible ? __('Hide descriptions') : __('Show descriptions');

        if (this.props.totalItems > 0) {
            return (
                <div block="CategoryProductList" elem="VerticalButtonLeft" mods={{ notSticky }}>
                    <button
                        ref={this.descriptionButtonRef}
                        onClick={() =>
                            this.setState((prevState) => ({ isDescriptionVisible: !prevState.isDescriptionVisible }))
                        }
                    >
                        {buttonText}
                    </button>
                </div>
            );
        }

        return null;
    }

    changeStorageGridMode(gridMode) {
        BrowserDatabase.setItem(gridMode, 'storageGridMode', ONE_YEAR_IN_SECONDS);
        this.setState({ gridMode });
    }

    renderSwitcherButton(gridType) {
        const elem = `Grid-${gridType}`;
        return <button block="CategoryProductList" elem={elem} onClick={() => this.changeStorageGridMode(gridType)} />;
    }

    renderGridSwitcher() {
        return (
            <div block="CategoryProductList" elem="GridSwitcher" mods={{ gridMode: this.state.gridMode }}>
                {this.renderSwitcherButton(GRID_OPTIONS_BUTTON1)}
                {this.renderSwitcherButton(GRID_OPTIONS_BUTTON2)}
            </div>
        );
    }

    renderPagination(hideOnMobile = false) {
        const { isLoading, totalPages, requestPage, isPaginationEnabled } = this.props;

        if (!isPaginationEnabled) return null;

        return (
            <CategoryPagination
                isLoading={isLoading}
                totalPages={totalPages}
                onPageSelect={requestPage}
                hideOnMobile={hideOnMobile}
            />
        );
    }

    renderTitle() {
        const { title, subtitle } = this.props;
        if (!title) {
            return null;
        }

        return (
            <div block="ProductListWidget" elem="TitleWrapper">
                <span block="ProductListWidget" elem="Title" mix={{ block: 'BlockTitleWhite' }}>
                    {title}
                </span>
                {subtitle ? (
                    <span block="ProductListWidget" elem="Subtitle" mix={{ block: 'BlockTitle' }}>
                        {subtitle}
                    </span>
                ) : (
                    ''
                )}
            </div>
        );
    }

    handleTabbedList() {
        const {
            brandName,
            uid = 'single_carousel_per_page',
            getNavigationStateById,
            changeNavigationStateWithId,
        } = this.props;

        const { tabs: prevTabs, name: prevName } = getNavigationStateById(uid) || { name: null, tabs: [] };

        const tabAlreadyExists = prevTabs.length > 0 && prevTabs.some((tab) => tab === brandName);

        const isNameSame = brandName === prevName;

        if (tabAlreadyExists || isNameSame) return;

        const tabs = tabAlreadyExists ? prevTabs : [...prevTabs, brandName];
        const name = prevName || brandName;

        changeNavigationStateWithId(uid, {
            tabs,
            name,
        });
    }

    mapHtmlTilesToObject(content) {
        const tempDiv = document.createElement('div');
        tempDiv.innerHTML = content;
        const elements = Array.from(tempDiv.querySelectorAll('[data-position]'));

        const tiles = elements.reduce((obj, element) => {
            obj[element.dataset.position] = element.innerHTML;
            return obj;
        }, {});

        return tiles;
    }

    render() {
        const { totalPages, isLoading, mix, widget, addStaticImages, brandName } = this.props;

        if (!isLoading && totalPages === 0) return this.renderNoProducts();

        if (addStaticImages) {
            return (
                <CarouselWrapper
                    wrapperMix={{ block: 'ProductListWidget', elem: 'Wrapper' }}
                    label="Product List Widget"
                >
                    {this.renderTitle()}
                    {this.renderLoadButton()}
                    {this.renderPages()}
                </CarouselWrapper>
            );
        }

        if (widget) {
            return (
                <>
                    {this.renderTitle()}
                    {this.renderLoadButton()}
                    {this.renderPages()}
                </>
            );
        }

        const isTabbed = brandName && brandName.length > 0;

        return (
            <>
                {this.renderPagination(true)}
                <div
                    block="CategoryProductList"
                    mods={{ isLoading, isTabbed, gridMode: this.state.gridMode, brandName }}
                    mix={mix}
                >
                    {this.renderGridSwitcher()}
                    {this.renderTitle()}
                    {this.renderLoadButton()}
                    {this.renderPages()}
                    {this.renderPagination()}
                </div>
            </>
        );
    }
}

const RENDER_PAGE_FREQUENCY = 10;

export { RENDER_PAGE_FREQUENCY };
export default debounceRender(ProductList, RENDER_PAGE_FREQUENCY, { leading: false });

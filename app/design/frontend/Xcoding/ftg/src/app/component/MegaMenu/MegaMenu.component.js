import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import BrandMenu from 'Component/BrandMenu';
import CmsBlock from 'Component/CmsBlock/CmsBlock.container';
import Link from 'Component/Link';

import './MegaMenu.style.scss';

const MEGAMENU_BRANDS = 'megamenu-brands';
const MENU_BRANDS_ITEM_CLASS = 'marki';

class MegaMenu extends PureComponent {
    static propTypes = {
        categories: PropTypes.array.isRequired,
        isActive: PropTypes.bool.isRequired,
        setActive: PropTypes.func.isRequired,
    };

    onMouseOver = () => {
        const { setActive } = this.props;
        setActive(true);
    };

    onMouseLeave = () => {
        const { setActive } = this.props;
        setActive(false);
    };

    renderCategoryColumns() {
        const { categories } = this.props;

        return categories.map(({ title, children, url, item_id }) => (
            <div block="MegaMenu" elem="Column" key={item_id}>
                <Link to={url} block="MegaMenu" elem="ColumnTitle" onClick={this.onMouseLeave}>
                    {title}
                </Link>
                {this.renderCategoryRows(children)}
            </div>
        ));
    }

    renderCategoryRows(categories = {}) {
        return Object.values(categories).map(({ title, item_id, url }) => (
            <Link to={url} block="MegaMenu" elem="ColumnElement" key={item_id} onClick={this.onMouseLeave}>
                {title}
            </Link>
        ));
    }

    renderBrandsMenu() {
        const { isActive, title, url } = this.props;

        return (
            <div
                block="MegaMenu"
                mods={{ isActive }}
                onMouseOver={this.onMouseOver}
                onMouseLeave={this.onMouseLeave}
                onFocus={this.onMouseOver}
                onBlur={this.onMouseLeave}
            >
                <div block="MegaMenu" elem="InnerWrapper">
                    <div block="MegaMenu" elem="Content">
                        <div block="MegaMenu" elem="Categories">
                            <CmsBlock identifier="brand-menu-left" />
                            <div block="MegaMenu" elem="Featured-Brand">
                                <CmsBlock identifier="brand-menu-left-featured-brand" />
                            </div>
                        </div>
                        <div block="MegaMenu" elem="Brands">
                            <BrandMenu title={title} url={url} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const { categories, item_class } = this.props;
        if (item_class.includes(MENU_BRANDS_ITEM_CLASS)) {
            return this.renderBrandsMenu();
        }

        if (!categories || !categories.length) {
            return null;
        }

        const parentID = categories[0].parent_id;

        return (
            <div block="MegaMenu" mods={{ isActive: true }}>
                <div block="MegaMenu" elem="InnerWrapper">
                    <div block="MegaMenu" elem="Content">
                        <div block="MegaMenu" elem="Categories">
                            {this.renderCategoryColumns()}
                        </div>
                        <div block="MegaMenu" elem="Brands">
                            <CmsBlock identifier={[`${MEGAMENU_BRANDS}-${parentID}`]} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MegaMenu;

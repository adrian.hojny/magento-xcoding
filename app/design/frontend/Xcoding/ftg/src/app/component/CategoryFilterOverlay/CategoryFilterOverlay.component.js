import React from 'react';

import SourceCategoryFilterOverlay from 'SourceComponent/CategoryFilterOverlay/CategoryFilterOverlay.component';
import { CATEGORY_FILTER_OVERLAY_ID } from 'SourceComponent/CategoryFilterOverlay/CategoryFilterOverlay.config';

import ActiveFilterButton from 'Component/ActiveFilterButton';
import CategoryConfigurableAttributes from 'Component/CategoryConfigurableAttributes/CategoryConfigurableAttributes.container';
import { DEFAULT_CURERNCY } from 'Component/LangPanel/LangPanel.component';
import Overlay from 'Component/Overlay/Overlay.container';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';

import './CategoryFilterOverlay.extend.style.scss';

export { CATEGORY_FILTER_OVERLAY_ID };

export default class CategoryFilterOverlay extends SourceCategoryFilterOverlay {
    static defaultProps = {
        ...super.defaultProps,
        priceValue: {
            min: 0,
            max: 0,
        },
    };

    renderDefaultFilters() {
        return (
            <>
                <div block="CategoryFilterOverlay" elem="CategoryConfigurableAttributes">
                    {this.renderFilters()}
                </div>
                {this.renderActiveFilters()}
                {this.renderSeeResults()}
            </>
        );
    }

    renderFilters() {
        const {
            availableFilters,
            customFiltersValues,
            toggleCustomFilter,
            getFilterUrl,
            updateFilter,
            updatePriceRange,
            priceValue,
            minPriceValue,
            maxPriceValue,
            sortFields,
            selectedSort,
            onSortChange,
            onVisible,
        } = this.props;

        const isLoaded = availableFilters && !!Object.keys(availableFilters).length;

        return (
            <CategoryConfigurableAttributes
                mix={{ block: 'CategoryFilterOverlay', elem: 'Attributes' }}
                isReady={isLoaded}
                configurable_options={availableFilters}
                getLink={getFilterUrl}
                parameters={customFiltersValues}
                updateConfigurableVariant={toggleCustomFilter}
                updateFilter={updateFilter}
                updatePriceRange={updatePriceRange}
                priceValue={priceValue}
                minPriceValue={minPriceValue}
                maxPriceValue={maxPriceValue}
                sortFields={sortFields}
                selectedSort={selectedSort}
                onSortChange={onSortChange}
                onFilterOverlayVisible={onVisible}
            />
        );
    }

    renderSeeResults() {
        const { onSeeResultsClick } = this.props;

        return (
            <div block="CategoryFilterOverlay" elem="SeeResults">
                <button
                    block="CategoryFilterOverlay"
                    elem="Button"
                    mix={{ block: 'Button' }}
                    onClick={onSeeResultsClick}
                >
                    {__('SEE RESULTS')}
                </button>
            </div>
        );
    }

    renderActiveFilters() {
        const {
            toggleCustomFilter,
            clearFilters,
            minPriceValue,
            maxPriceValue,
            currentArgs,
            updatePriceRange,
            currency_code,
        } = this.props;
        const userPreferredCurrency = BrowserDatabase.getItem(DEFAULT_CURERNCY);
        const currency = userPreferredCurrency || currency_code;

        const min = currentArgs?.filter?.priceRange?.min || 0;
        const max = currentArgs?.filter?.priceRange?.max || 0;

        const selectedFilters = this.getSelectedFiltersLabels();
        const isPriceFilterVisible = max !== 0;
        let numberOfSelectedFilters = selectedFilters.length;
        if (isPriceFilterVisible) {
            numberOfSelectedFilters++;
        }

        const countGrossPrice = (rawPrice) => {
            const { priceSliderVatRate } = this.props;

            const digitsAfterPeriod = 2;
            const toIntegerMultiplyFix = 10 ** digitsAfterPeriod; // for floating point precision fix

            const rawPriceInt = rawPrice * toIntegerMultiplyFix;
            const vatRateInt = priceSliderVatRate * toIntegerMultiplyFix;

            return Math.ceil((rawPriceInt * vatRateInt) / toIntegerMultiplyFix ** 2);
        };

        const minVal = countGrossPrice(Math.max(min, minPriceValue));
        const maxVal = countGrossPrice(Math.min(max, maxPriceValue));

        const onFilterClick = (key, value) => () => toggleCustomFilter(key, value);
        const onPriceClick = () => updatePriceRange({ min: '', max: '' });

        return (
            numberOfSelectedFilters > 0 && (
                <div block="ActiveFilters">
                    {selectedFilters.map(({ label, value, key }) => {
                        if (key === 'product_collection') {
                            return null;
                        }

                        return (
                            <ActiveFilterButton
                                label={label}
                                key={`${key}${value}`}
                                onClick={onFilterClick(key, value)}
                            />
                        );
                    })}
                    {isPriceFilterVisible && (
                        <ActiveFilterButton
                            label={`${minVal} - ${maxVal} ${currency}`}
                            key="price"
                            /* eslint-disable-next-line react/jsx-no-bind */
                            onClick={onPriceClick}
                        />
                    )}

                    {numberOfSelectedFilters > 1 && (
                        <ActiveFilterButton
                            label={__('Clear filters (%s)', numberOfSelectedFilters)}
                            key="clear"
                            onClick={clearFilters}
                        />
                    )}
                </div>
            )
        );
    }

    getSelectedFiltersLabels() {
        const { customFiltersValues, availableFilters } = this.props;

        if (Object.keys(availableFilters).length === 0) return [];

        return Object.keys(customFiltersValues).reduce(
            (prev, attributeKey) =>
                prev.concat(
                    customFiltersValues[attributeKey]
                        .map((value) => {
                            const { attribute_type, attribute_options, attribute_label } = availableFilters[
                                attributeKey
                            ];

                            if (!attribute_options[value]) return null;

                            const label =
                                attribute_type === 'boolean' ? attribute_label : attribute_options[value]?.label;

                            return { key: attributeKey, label, value };
                        })
                        .filter((value) => !!value),
                ),
            [],
        );
    }

    render() {
        const { onVisible, onHide, totalPages, isProductsLoading, isContentFiltered } = this.props;

        if (!isProductsLoading && totalPages === 0 && !isContentFiltered) {
            return <div block="CategoryFilterOverlay" />;
        }

        return (
            <Overlay
                onVisible={onVisible}
                onHide={onHide}
                mix={{ block: 'CategoryFilterOverlay' }}
                id={CATEGORY_FILTER_OVERLAY_ID}
                isRenderInPortal={false}
            >
                <div block="CategoryFilterOverlay" elem="Container">
                    {this.renderContent()}
                    {this.renderLoader()}
                </div>
            </Overlay>
        );
    }
}

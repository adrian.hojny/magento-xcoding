import { connect } from 'react-redux';

import {
    CategoryProductListContainer as SourceCategoryProductListContainer,
    mapDispatchToProps,
    mapStateToProps as sourceMapStateToProps,
} from 'SourceComponent/CategoryProductList/CategoryProductList.container';

import ProductList from 'Component/ProductList/ProductList.container';

import './CategoryProductList.extend.style.scss';

export const mapStateToProps = (state) => ({
    ...sourceMapStateToProps(state),
    category: state.CategoryReducer.category,
    currencyCode: state.ConfigReducer.default_display_currency_code,
    currentArgs: state.ProductListReducer.currentArgs,
});

class CategoryProductListContainer extends SourceCategoryProductListContainer {
    containerFunctions = {
        requestProductList: this.requestProductList.bind(this),
    };

    componentDidUpdate(prevProps) {
        const {
            totalItems,
            totalPages,
            currentArgs: { currentPage },
            pages,
            search,
            category,
        } = this.props;
        const changedItemsOnList =
            totalPages > 0 && (totalPages !== prevProps.totalPages || totalItems !== prevProps.totalItems);
        const products = pages[currentPage];

        if (changedItemsOnList && products) {
            const list = search ? 'Search Results' : category?.url_key;

            // Measure product impressions
            const productsGTM = products.map((product, index) => {
                return {
                    id: product?.sku,
                    name: product?.name || '',
                    list_name: list,
                    brand: product?.brand_details?.url_alias || '',
                    category: list,
                    variant: '',
                    list_position: index + 1,
                    price: product?.price_range?.minimum_price?.final_price?.value,
                };
            });

            gtag('event', 'view_item_list', {
                items: Array.from(productsGTM),
            });
        }
    }

    requestProductList(options) {
        const { requestProductList } = this.props;

        // eslint-disable-next-line no-param-reassign
        options.useProductListRequest = true;

        requestProductList(options);
    }

    render() {
        return (
            <ProductList
                {...this.props}
                {...this.containerProps()}
                {...this.containerFunctions}
                isInfiniteLoaderEnabled={false}
                withoutItemProps
            />
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryProductListContainer);

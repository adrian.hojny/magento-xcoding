import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import CarouselWithSideImage from 'Component/CarouselWithSideImage';
import CarouselWrapper from 'Component/CarouselWrapper';

export default class CompleteLook extends PureComponent {
    static propTypes = {
        requestCompleteLook: PropTypes.func.isRequired,
        completeLook: PropTypes.any.isRequired,
        product: PropTypes.any.isRequired,
    };

    componentDidMount() {
        const {
            requestCompleteLook,
            product: { attributes },
        } = this.props;

        if (attributes && attributes.look) {
            const lookValue = attributes.look.attribute_value;

            if (lookValue) {
                requestCompleteLook(lookValue.split(',')[0]);
            } else {
                requestCompleteLook(null);
            }
        }
    }

    componentDidUpdate(prevProps) {
        const {
            requestCompleteLook,
            product: { attributes },
            product,
        } = this.props;
        const { product: prevProduct } = prevProps;

        if (attributes && attributes.look) {
            const lookValue = attributes.look;

            if (lookValue.attribute_value) {
                const lookId = lookValue.attribute_value.split(',')[0];

                if (!prevProduct.id || prevProduct.id !== product.id) {
                    if (lookId) {
                        requestCompleteLook(lookId);
                    } else {
                        requestCompleteLook(null);
                    }
                }
            }
        }
    }

    getBaseImage(gallery) {
        if (gallery) return gallery.find((image) => image.types && image.types.includes('image')).base.url;

        return null;
    }

    render() {
        const { product, completeLook } = this.props;

        if (completeLook.total_count < 1) {
            return <></>;
        }

        return (
            <CarouselWrapper
                label="Complete Look"
                mix={{ block: 'CompleteLook' }}
                wrapperMix={{ block: 'CompleteLook', elem: 'Wrapper' }}
            >
                <CarouselWithSideImage
                    sideImageUrl={this.getBaseImage(product.media_gallery_entries)}
                    products={completeLook.items}
                    titleUp={__('Complete')}
                    titleDown={__('Look')}
                />
            </CarouselWrapper>
        );
    }
}

import { connect } from 'react-redux';

import SourceCategorySortContainer from 'SourceComponent/CategorySort/CategorySort.container';

import { changeNavigationState, goToPreviousNavigationState } from 'Store/Navigation/Navigation.action';
import { TOP_NAVIGATION_TYPE } from 'Store/Navigation/Navigation.reducer';
import { toggleOverlayByKey } from 'Store/Overlay/Overlay.action';

export const mapDispatchToProps = (dispatch) => ({
    toggleOverlayByKey: (key) => dispatch(toggleOverlayByKey(key)),
    changeHeaderState: (state) => dispatch(changeNavigationState(TOP_NAVIGATION_TYPE, state)),
    goToPreviousHeaderState: () => dispatch(goToPreviousNavigationState(TOP_NAVIGATION_TYPE)),
});

export const mapStateToProps = (state) => ({
    activeOverlay: state.OverlayReducer.activeOverlay,
});

export class CategorySortContainer extends SourceCategorySortContainer {
    containerProps = () => {
        const selectOptions = this._prepareOptions();

        return {
            selectOptions,
            label: this._getCurrentLabel(selectOptions),
            withButtons: false,
            name: 'sort',
        };
    };

    _getLabel(option) {
        const { id, label: pureLabel } = option;
        const [label] = pureLabel.split(' ');

        switch (id) {
            case 'price':
                return {
                    asc: __('lowest price'),
                    desc: __('highest price'),
                };
            case 'position':
                return {
                    asc: __('Popularity', label),
                };
            default:
                return {};
        }
    }

    _getCurrentLabel(selectOptions) {
        const { sortDirection, sortKey } = this.props;
        const { label } = selectOptions.find((el) => el.id === `${sortDirection} ${sortKey}`) || {};
        return label ? `${__('Sort:')} ${label}` : __('Sorting');
    }

    _prepareOptions() {
        const { sortFields } = this.props;

        if (!sortFields) return [];

        return sortFields.reduce((acc, option) => {
            const { id } = option;
            const label = this._getLabel(option);
            const { asc, desc } = label;
            const fields = Array.from(acc);

            if (!asc && !desc) return acc;

            if (asc) {
                fields.push({
                    id: `ASC ${id}`,
                    name: id,
                    value: `ASC ${id}`,
                    label: asc,
                    direction: 'ASC',
                });
            }
            if (desc) {
                fields.push({
                    id: `DESC ${id}`,
                    name: id,
                    value: `DESC ${id}`,
                    label: desc,
                    direction: 'DESC',
                });
            }

            return fields;
        }, []);
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategorySortContainer);

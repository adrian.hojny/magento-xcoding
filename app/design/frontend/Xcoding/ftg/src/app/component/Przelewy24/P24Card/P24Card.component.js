import React from 'react';

import Field from 'Component/Field';
import { BuyButton } from '../P24BuyButton/P24BuyButton.component';
import { P24CARD_CODE_NAME, P24CARD_CODE_NAME_TOC } from '../Przelewy24.config';
import { renderP24TocLabel } from '../Przelewy24.utils';

const P24Card = ({ isPayBtnDisabled, onChange }) => {
    const handleTOCChange = (_, value) => {
        // TODO refactor
        onChange({
            name: P24CARD_CODE_NAME_TOC,
            value,
        });
        onChange({
            name: P24CARD_CODE_NAME,
            value,
        });
    };

    return (
        <div block="Przelewy24Card">
            <Field
                type="checkbox"
                label={renderP24TocLabel()}
                id={P24CARD_CODE_NAME_TOC}
                mix={{ block: 'P24PaymentMethods', elem: 'Checkbox' }}
                name={P24CARD_CODE_NAME_TOC}
                onChange={handleTOCChange}
            />
            <BuyButton isDisabled={isPayBtnDisabled} />
        </div>
    );
};

export default P24Card;

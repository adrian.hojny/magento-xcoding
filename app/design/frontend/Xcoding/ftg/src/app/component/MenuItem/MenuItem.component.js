import PropTypes from 'prop-types';

import { MenuItem as SourceMenuItem } from 'SourceComponent/MenuItem/MenuItem.component';

import Link from 'Component/Link';

export class MenuItem extends SourceMenuItem {
    static propTypes = {
        ...super.propTypes,
        handleCategoryLeave: PropTypes.func.isRequired,
    };

    renderItemLinkContent() {
        const {
            activeMenuItemsStack,
            item,
            itemMods,
            handleCategoryHover,
            handleCategoryLeave,
            closeMenu,
        } = this.props;

        const { url, item_id } = item;

        const isHovered = activeMenuItemsStack.includes(item_id);

        return (
            <Link
                to={url}
                block="Menu"
                elem="Link"
                id={item_id}
                onMouseEnter={handleCategoryHover}
                onMouseLeave={handleCategoryLeave}
                mods={{ isHovered }}
                onClick={closeMenu}
            >
                {this.renderItemContent(item, itemMods)}
            </Link>
        );
    }
}

export default MenuItem;

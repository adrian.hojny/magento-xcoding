import React from 'react';

import SourceVideoPopup from 'SourceComponent/VideoPopup/VideoPopup.component';

import './VideoPopup.style.scss';

export * from 'SourceComponent/VideoPopup/VideoPopup.component';

export const VIMEO_FORMAT = new RegExp('(?:https?//)?vimeo.com[\\w/]*/(\\d+)$');
export const YOUTUBE_FORMAT = new RegExp('(?:https?//)?www.youtube.com/watch\\?v=(\\w+)');

export default class VideoPopup extends SourceVideoPopup {
    _renderVideoContent() {
        const {
            payload: { media: { video_content: { video_url } = {} } = {} },
        } = this.props;
        const {
            payload: { media: { media_type } = {} },
        } = this.props;

        if (media_type === 'internal-video') return this._renderInternalVideo(video_url);

        if (!video_url) return null;

        const [, vimeoId] = VIMEO_FORMAT.exec(video_url) || [];
        if (vimeoId) return this._renderVimeoVideo(vimeoId);

        const [, youtubeId] = YOUTUBE_FORMAT.exec(video_url);
        if (youtubeId) return this._renderYoutubeVideo(youtubeId);

        return null;
    }

    _renderInternalVideo(video_url) {
        const videoType = `video/${video_url.split('.').pop()}`;

        return (
            <div>
                <video width="100%" height="100%" controls>
                    <source src={video_url} type={videoType} />
                </video>
            </div>
        );
    }
}

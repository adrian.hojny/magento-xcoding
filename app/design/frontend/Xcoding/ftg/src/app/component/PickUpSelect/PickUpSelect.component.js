import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Field from 'Component/Field';

class PickUpSelect extends PureComponent {
    static propTypes = {
        onSelect: PropTypes.func.isRequired,
        pickupPoints: PropTypes.array,
    };

    constructor(props) {
        super(props);
        const { pickupPoints, onSelect } = props;

        if (Array.isArray(pickupPoints)) {
            const defaultCode = pickupPoints[0].code;

            this.state = {
                value: defaultCode,
            };

            onSelect({ pickup_store_code: defaultCode });
        } else {
            this.state = {};
        }
    }

    handleChange = (code) => {
        const { onSelect } = this.props;
        this.setState({
            value: code,
        });

        onSelect({ pickup_store_code: code });
    };

    render() {
        const { pickupPoints } = this.props;
        const { value } = this.state;
        if (!Array.isArray(pickupPoints)) {
            return null;
        }

        return (
            <div>
                <Field
                    type="select"
                    name="pickup_store_code"
                    id="pickup_store_code"
                    value={value}
                    selectOptions={pickupPoints.map((pickupPoint, index) => ({
                        id: index,
                        value: pickupPoint.code,
                        label: `${pickupPoint.address}`,
                    }))}
                    onChange={this.handleChange}
                />
            </div>
        );
    }
}

export default PickUpSelect;

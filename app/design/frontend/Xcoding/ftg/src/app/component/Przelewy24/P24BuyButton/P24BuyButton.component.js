import React, { memo } from 'react';

export const BuyButton = memo(({ isDisabled }) => (
    <div block="Przelewy24" elem="CompleteOrderButtonWrapper">
        <button
            type="submit"
            block="Button"
            mods={{ green: true }}
            disabled={isDisabled}
            mix={{ block: 'CheckoutBilling', elem: 'Button' }}
        >
            {__('Complete order')}
        </button>
    </div>
));

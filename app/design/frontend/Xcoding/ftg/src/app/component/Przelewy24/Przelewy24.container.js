import { connect } from 'react-redux';

import { purgeOtherFormGroups, setFormFieldValue, validateFieldGroup } from 'Store/P24/P24.actions';
import Przelewy24 from './Przelewy24.component';

export const mapStateToProps = (state) => ({
    formValues: state.P24Reducer.formValues,
    formGroupsValidity: state.P24Reducer.formGroupsValidity,
});

export const mapDispatchToProps = (dispatch) => ({
    setFormFieldValue: (name, value) => dispatch(setFormFieldValue(name, value)),
    validateFieldGroup: (name) => dispatch(validateFieldGroup(name)),
    purgeOtherFormGroups: (name) => dispatch(purgeOtherFormGroups(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Przelewy24);

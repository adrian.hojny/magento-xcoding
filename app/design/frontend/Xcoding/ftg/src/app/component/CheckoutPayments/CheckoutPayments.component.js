import PropTypes from 'prop-types';

import { CheckoutPayments as SourceCheckoutPayments } from 'SourceComponent/CheckoutPayments/CheckoutPayments.component';

import PayPal from 'Component/PayPal';
import Przelewy24 from 'Component/Przelewy24';
import {
    BRAINTREE,
    CHECK_MONEY,
    KLARNA,
    PAYPAL_EXPRESS,
    PAYPAL_EXPRESS_CREDIT,
    PRZELEWY24,
    PRZELEWY24_CARD,
    STRIPE,
} from './CheckoutPayments.config';

import './CheckoutPayments.style';

export class CheckoutPayments extends SourceCheckoutPayments {
    static propTypes = {
        ...super.propTypes,
        selectedPaymentCode: PropTypes.oneOf([
            KLARNA,
            BRAINTREE,
            CHECK_MONEY,
            PAYPAL_EXPRESS,
            PAYPAL_EXPRESS_CREDIT,
            CHECK_MONEY,
            STRIPE,
            PRZELEWY24,
            PRZELEWY24_CARD,
        ]).isRequired,
    };

    paymentRenderMap = {
        ...super.paymentRenderMap,
        [PRZELEWY24]: this.renderPrzelewy24.bind(this),
        [PRZELEWY24_CARD]: this.renderPrzelewy24Card.bind(this),
    };

    state = {
        hasError: false,
    };

    renderPrzelewy24() {
        return <Przelewy24 />;
    }

    renderPrzelewy24Card() {
        return <Przelewy24 isCard />;
    }

    renderPayments() {
        const { paymentMethods } = this.props;

        return paymentMethods.map(this.renderPaymentListEntryWithExpandlePayment);
    }

    renderSelectedPayment() {
        const { selectedPaymentCode } = this.props;
        const render = this.paymentRenderMap[selectedPaymentCode];
        if (!render) {
            return null;
        }

        return render();
    }

    renderBuy = () => {
        const { isPaymentButtonDisabled } = this.props;
        const { selectedPaymentCode } = this.props;

        return (
            selectedPaymentCode !== PAYPAL_EXPRESS && (
                <div block="Checkout" elem="StickyButtonWrapper2">
                    <button
                        // type="submit"
                        block="Button"
                        mods={{ green: true }}
                        disabled={isPaymentButtonDisabled}
                        mix={{ block: 'CheckoutBilling', elem: 'Button' }}
                    >
                        {__('Complete order')}
                    </button>
                </div>
            )
        );
    };

    renderPaymentListEntryWithExpandlePayment = (method) => {
        const { selectedPaymentCode } = this.props;
        const isSelected = method.code === selectedPaymentCode;
        const hasInternalPayBtn = selectedPaymentCode === PRZELEWY24 || selectedPaymentCode === PRZELEWY24_CARD;

        const conditionallyRenderExpandedPayment = () =>
            isSelected ? (
                <>
                    {this.renderSelectedPayment()}
                    {!hasInternalPayBtn && this.renderBuy()}
                </>
            ) : null;

        return (
            method.code !== PAYPAL_EXPRESS_CREDIT && (
                <div key={method.code}>
                    {this.renderPayment(method)}
                    {conditionallyRenderExpandedPayment()}
                </div>
            )
        );
    };

    renderHeading() {
        return (
            <h2 block="Checkout" elem="Heading">
                {__('Select payment method')}
            </h2>
        );
    }

    renderPayPal() {
        const { selectedPaymentCode, setLoading, setDetailsStep, isLoading, paymentMethods } = this.props;
        const isEnabled = paymentMethods.find((method) => method.code === PAYPAL_EXPRESS);

        return (
            isEnabled && (
                <PayPal
                    isLoading={isLoading}
                    setLoading={setLoading}
                    setDetailsStep={setDetailsStep}
                    selectedPaymentCode={selectedPaymentCode}
                />
            )
        );
    }

    renderContent() {
        const { hasError } = this.state;

        if (hasError) {
            return <p>{__('The error occurred during initializing payment methods. Please try again later!')}</p>;
        }

        return (
            <>
                {this.renderHeading()}
                <ul block="CheckoutPayments" elem="Methods">
                    {this.renderPayments()}
                </ul>
                {this.renderPayPal()}
            </>
        );
    }

    render() {
        return <div block="CheckoutPayments">{this.renderContent()}</div>;
    }
}

export default CheckoutPayments;

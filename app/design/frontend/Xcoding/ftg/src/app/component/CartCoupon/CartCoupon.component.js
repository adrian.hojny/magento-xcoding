import React from 'react';

import SourceCartCoupon from 'SourceComponent/CartCoupon/CartCoupon.component';

import Field from 'Component/Field';
import Loader from 'Component/Loader';

export const COUPON_IN_DETAILS = 'details';

export default class CartCoupon extends SourceCartCoupon {
    renderRemoveCoupon() {
        const { couponCode, renderType } = this.props;

        if (renderType === COUPON_IN_DETAILS) {
            return (
                <button block="CartCoupon" elem="RemoveCoupon" onClick={this.handleRemoveCoupon}>
                    {__('Delete')}
                </button>
            );
        }

        return (
            <>
                <p block="CartCoupon" elem="Message">
                    <strong>{couponCode.toUpperCase()}</strong>
                </p>
                <button
                    block="CartCoupon"
                    elem="Button"
                    type="button"
                    mix={{ block: 'Button' }}
                    onClick={this.handleRemoveCoupon}
                >
                    {__('Delete')}
                </button>
            </>
        );
    }

    renderApplyCoupon() {
        const { enteredCouponCode } = this.state;

        return (
            <>
                <Field
                    type="text"
                    id="couponCode"
                    name="couponCode"
                    value={enteredCouponCode}
                    placeholder={__('Discount Code')}
                    onChange={this.handleCouponCodeChange}
                    mix={{ block: 'CartCoupon', elem: 'Input' }}
                />
                <button
                    block="CartCoupon"
                    elem="Button"
                    type="button"
                    mix={{ block: 'Button' }}
                    disabled={!enteredCouponCode}
                    onClick={this.handleApplyCoupon}
                >
                    {__('Confirm')}
                </button>
            </>
        );
    }

    render() {
        const { isLoading, couponCode } = this.props;

        return (
            <>
                {!couponCode ? <span>{__('Enter your unique discount code below.')}</span> : ''}
                <form block="CartCoupon" onSubmit={this.handleFormSubmit}>
                    <Loader isLoading={isLoading} />
                    {couponCode ? this.renderRemoveCoupon() : this.renderApplyCoupon()}
                </form>
            </>
        );
    }
}

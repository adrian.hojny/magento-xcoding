import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Field from 'Component/Field';

import './FilterSelectorSearch.style.scss';

class FilterSelectorSearch extends PureComponent {
    static propTypes = {
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired,
    };

    render() {
        const { value, onChange } = this.props;

        return (
            <Field
                type="text"
                mix={{
                    block: 'FilterSelector',
                    elem: 'Search',
                }}
                name="OptionSearch"
                id="OptionSearch"
                placeholder={__('Search...')}
                value={value}
                onChange={onChange}
            />
        );
    }
}

export default FilterSelectorSearch;

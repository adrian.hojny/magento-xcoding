import { connect } from 'react-redux';

import RecentlyViewedDispatcher from 'Store/RecentlyViewed/RecentlyViewed.dispatcher';
import RecentlyViewed from './RecentlyViewed.component';

export const mapStateToProps = (state) => ({
    recentlyViewed: state.RecentlyViewedReducer,
});

export const mapDispatchToProps = (dispatch) => ({
    getRecentlyViewed: (options) => {
        RecentlyViewedDispatcher.handleData(dispatch, options);
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(RecentlyViewed);

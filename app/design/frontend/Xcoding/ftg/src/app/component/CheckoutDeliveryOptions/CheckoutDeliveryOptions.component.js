import PropTypes from 'prop-types';

import SourceCheckoutDeliveryOptions from 'SourceComponent/CheckoutDeliveryOptions/CheckoutDeliveryOptions.component';

import CheckoutDeliveryOption from 'Component/CheckoutDeliveryOption/CheckoutDeliveryOption.container';
import InpostGeowidget from 'Component/InpostGeowidget/InpostGeowidget.component';
import PickUpSelect from 'Component/PickUpSelect/PickUpSelect.component';

class CheckoutDeliveryOptions extends SourceCheckoutDeliveryOptions {
    static propTypes = {
        ...super.propTypes,
        selectInpostDeliveryPoint: PropTypes.func.isRequired,
    };

    shippingRenderMap = {
        inpostmachine: this.renderInpostGeowidget.bind(this),
    };

    renderInpostGeowidget() {
        const {
            selectInpostDeliveryPoint,
            shippingAdditionalInformation: { inpost },
        } = this.props;

        return <InpostGeowidget onPointSelect={selectInpostDeliveryPoint} selectedPoint={inpost} />;
    }

    renderDeliveryOption = (option) => {
        const { selectedShippingMethodCode, selectShippingMethod, selectPickUpPoints } = this.props;

        const { method_code } = option;
        const isSelected = selectedShippingMethodCode === method_code;

        return (
            <div key={method_code}>
                <CheckoutDeliveryOption isSelected={isSelected} option={option} onClick={selectShippingMethod} />

                {isSelected && <PickUpSelect onSelect={selectPickUpPoints} pickupPoints={option.pickup_points} />}
            </div>
        );
    };
}

export default CheckoutDeliveryOptions;

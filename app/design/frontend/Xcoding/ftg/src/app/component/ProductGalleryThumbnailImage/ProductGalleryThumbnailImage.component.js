import SourceProductGalleryThumbnailImage from 'SourceComponent/ProductGalleryThumbnailImage/ProductGalleryThumbnailImage.component';

import Image from 'Component/Image';
import { INTERNAL_VIDEO_TYPE } from 'Component/ProductGallery/ProductGallery.component';
import {
    IMAGE_TYPE,
    PLACEHOLDER_TYPE,
    THUMBNAIL_KEY,
    VIDEO_TYPE,
} from 'Component/ProductGallery/ProductGallery.config';
import media, { PRODUCT_MEDIA } from 'Util/Media';

export * from 'SourceComponent/ProductGalleryThumbnailImage/ProductGalleryThumbnailImage.component';

export default class ProductGalleryThumbnailImage extends SourceProductGalleryThumbnailImage {
    renderImage() {
        const {
            media: { label: alt, file, thumbnail: { url: thumbnailUrl } = {}, id },
        } = this.props;

        if (id === THUMBNAIL_KEY) {
            return this.renderPlaceholder();
        }

        const src = thumbnailUrl || media(file, PRODUCT_MEDIA);

        return (
            <Image
                src={src}
                alt={alt}
                ratio="custom"
                mix={{ block: 'ProductGalleryThumbnailImage' }}
                useWebp
                forceWithoutScrolling
            />
        );
    }

    renderMedia() {
        const {
            media: { media_type },
        } = this.props;

        switch (media_type) {
            case VIDEO_TYPE:
                return this.renderVideo();
            case INTERNAL_VIDEO_TYPE:
                return this.renderVideo();
            case IMAGE_TYPE:
                return this.renderImage();
            case PLACEHOLDER_TYPE:
                return this.renderPlaceholder();
            default:
                return null;
        }
    }
}

import { connect } from 'react-redux';

// eslint-disable-next-line max-len
import {
    mapDispatchToProps,
    ProductConfigurableAttributesContainer,
} from 'Component/ProductConfigurableAttributes/ProductConfigurableAttributes.container';
import CategoryConfigurableAttributes from './CategoryConfigurableAttributes.component';

export const mapStateToProps = (state) => ({
    currency_code: state.ConfigReducer.default_display_currency_code,
    minPriceValue: state.ProductListInfoReducer.minPrice,
    maxPriceValue: state.ProductListInfoReducer.maxPrice,
    currentArgs: state.ProductListReducer.currentArgs,
});

export class CategoryConfigurableAttributesContainer extends ProductConfigurableAttributesContainer {
    render() {
        return <CategoryConfigurableAttributes {...this.props} {...this.containerFunctions} />;
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryConfigurableAttributesContainer);

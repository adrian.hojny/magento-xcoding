import { cloneElement, lazy, PureComponent, Suspense } from 'react';
import { Router as ReactRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';

import Breadcrumbs from 'Component/Breadcrumbs';
import CookiePopup from 'Component/CookiePopup';
import DemoNotice from 'Component/DemoNotice';
import Footer from 'Component/Footer';
import Header from 'Component/Header';
import Loader from 'Component/Loader';
import Meta from 'Component/Meta';
import NewVersionPopup from 'Component/NewVersionPopup';
import NotificationList from 'Component/NotificationList';
import OfflineNotice from 'Component/OfflineNotice';
import ShowDimmer from 'Component/SidebarDimmer';
import ThemeSetter from 'Component/ThemeSetter';
import { ThemeProvider } from 'Context/ThemeContext';
import NoMatchHandler from 'Route/NoMatchHandler';
import ProductCollectionsPage from 'Route/ProductCollections';
import SomethingWentWrong from 'Route/SomethingWentWrong';
import UrlRewrites from 'Route/UrlRewrites';
import BrowserDatabase from 'Util/BrowserDatabase/BrowserDatabase';
import history from 'Util/History';
import isMobile from 'Util/Mobile';
import HreflinksProvider from './HreflinksProvider.component';
import { AFTER_ITEMS_TYPE, BEFORE_ITEMS_TYPE, getRouteUrl, SWITCH_ITEMS_TYPE, withStoreRegex } from './Router.config';

import 'slick-carousel/slick/slick.scss';

export const PREMIUM = 'PREMIUM';

/**
 * ACCOUNT
 */
export const ConfirmAccountPage = lazy(() =>
    import(/* webpackMode: "lazy", webpackChunkName: "account" */ 'Route/ConfirmAccountPage'),
);
export const MyAccount = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "account" */ 'Route/MyAccount'));
export const RegisterPage = lazy(() =>
    import(/* webpackMode: "lazy", webpackChunkName: "account" */ 'Route/RegisterPage'),
);
export const SignInPage = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "account" */ 'Route/SignInPage'));

/**
 * CART
 */
export const CartPage = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "cart" */ 'Route/CartPage'));

/**
 * CATEGORY
 */
export const BrandPage = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "category" */ 'Route/BrandPage'));
export const RecentlyViewedPage = lazy(() =>
    import(/* webpackMode: "lazy", webpackChunkName: "category" */ 'Route/RecentlyViewed'),
);
export const SearchPage = lazy(() =>
    import(/* webpackMode: "lazy", webpackChunkName: "category" */ 'Route/SearchPage'),
);

/**
 * CMS
 */
export const CmsPage = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "cms" */ 'Route/CmsPage'));
export const HomePage = lazy(() =>
    import(/* webpackMode: "lazy", webpackChunkName: "cms", webpackPreload: true */ 'Route/HomePage'),
);
export const MenuPage = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "cms" */ 'Route/MenuPage'));

/**
 * CONTACT
 */
export const Contact = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "contact" */ 'Route/Contact'));

/**
 * CHECKOUT
 */
export const Checkout = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "checkout" */ 'Route/Checkout'));

/**
 * MISC
 */
export const SizingPage = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "misc" */ 'Route/SizingPage'));
export const PasswordChangePage = lazy(() =>
    import(/* webpackMode: "lazy", webpackChunkName: "misc" */ 'Route/PasswordChangePage'),
);
export const RemindPasswordPage = lazy(() =>
    import(/* webpackMode: "lazy", webpackChunkName: "misc" */ 'Route/RemindPasswordPage'),
);
export const WishlistShared = lazy(() =>
    import(/* webpackMode: "lazy", webpackChunkName: "misc" */ 'Route/WishlistSharedPage'),
);

/**
 * STASH
 */
export const StashPage = lazy(() => import(/* webpackMode: "lazy", webpackChunkName: "stash" */ 'Route/StashPage'));

export class Router extends PureComponent {
    static propTypes = {
        isBigOffline: PropTypes.bool,
        storeViewCode: PropTypes.string,
    };

    static defaultProps = {
        isBigOffline: false,
        storeViewCode: '',
    };

    constructor(props) {
        super(props);

        this.state = {
            hasError: false,
            errorDetails: {},
        };

        this[BEFORE_ITEMS_TYPE] = [
            {
                component: <NotificationList />,
                position: 10,
            },
            {
                component: <DemoNotice />,
                position: 15,
            },
            {
                component: <Header />,
                position: 20,
            },
            {
                component: <Breadcrumbs />,
                position: 30,
            },
            {
                component: <NewVersionPopup />,
                position: 35,
            },
        ];

        this[AFTER_ITEMS_TYPE] = [
            {
                component: <Footer />,
                position: 10,
            },
            {
                component: <CookiePopup />,
                position: 20,
            },
        ];
    }

    _getRoutes = () => {
        const { storeViewCode } = this.props;

        return [
            {
                component: <Route path={withStoreRegex('/')} exact component={HomePage} />,
                position: 10,
            },
            {
                component: <Route path={withStoreRegex('/search/:query/')} component={SearchPage} />,
                position: 25,
            },
            {
                component: <Route path={withStoreRegex('/page')} component={CmsPage} />,
                position: 40,
            },
            {
                component: <Route path={withStoreRegex('/cart')} exact component={CartPage} />,
                position: 50,
            },
            {
                component: <Route path={withStoreRegex('/checkout/:step?')} component={Checkout} />,
                position: 55,
            },
            {
                component: <Route path={withStoreRegex('/:account*/createPassword/')} component={PasswordChangePage} />,
                position: 60,
            },
            {
                component: <Route path={withStoreRegex(`/:account*/confirm}`)} component={ConfirmAccountPage} />,
                position: 65,
            },
            {
                component: <Route path={withStoreRegex('/my-account/:tab?')} component={MyAccount} />,
                position: 70,
            },
            {
                component: <Route path={withStoreRegex('/forgot-password')} component={MyAccount} />,
                position: 71,
            },
            {
                component: <Route path={withStoreRegex('/menu')} component={MenuPage} />,
                position: 80,
            },
            {
                component: <Route path={withStoreRegex('/wishlist/shared/:code')} component={WishlistShared} />,
                position: 81,
            },
            {
                component: <Route path={withStoreRegex('/recently-viewed')} component={RecentlyViewedPage} />,
                position: 85,
            },
            {
                component: <Route path={withStoreRegex('/product-collections')} component={ProductCollectionsPage} />,
                position: 90,
            },
            {
                component: <Route path={withStoreRegex('/stash')} component={StashPage} />,
                position: 95,
            },
            {
                component: <Route path={withStoreRegex('/remind-password')} component={RemindPasswordPage} />,
                position: 100,
            },
            {
                component: <Route path={withStoreRegex('/sign-in')} component={SignInPage} />,
                position: 105,
            },
            {
                component: <Route path={withStoreRegex('/register')} component={RegisterPage} />,
                position: 110,
            },
            {
                component: <Route path={withStoreRegex('/contact')} component={Contact} />,
                position: 115,
            },
            {
                component: (
                    <Route
                        path={withStoreRegex(`${getRouteUrl('brand', storeViewCode)}/:brandName`)}
                        component={BrandPage}
                    />
                ),
                position: 120,
            },
            {
                component: (
                    <Route
                        path={withStoreRegex(`${getRouteUrl('sizing', storeViewCode)}/:brandName`)}
                        component={SizingPage}
                    />
                ),
                position: 121,
            },
            {
                component: <Route component={UrlRewrites} />,
                position: 1000,
            },
        ];
    };

    componentDidMount() {
        const { host } = window.location;
        const { storeViewCode } = this.props;

        // prevent zoom input on ios
        if (isMobile.iOS()) {
            this.handleMetaChange('viewport', 'width=device-width, initial-scale=1, maximum-scale=1');
        }

        if (host.includes('prm')) {
            this.setAppleTitleMetaPrm();
            BrowserDatabase.setItem(true, PREMIUM);
            document.body.classList.add('prm');

            storeViewCode !== '' && this.addBaseTag();
        } else {
            this.setAppleTitleMeta();
            BrowserDatabase.deleteItem(PREMIUM);
        }
    }

    componentDidUpdate(prevProps) {
        const { storeViewCode } = this.props;
        const { host } = window.location;

        if (host.includes('prm')) {
            this.setAppleTitleMetaPrm();
            prevProps.storeViewCode !== storeViewCode && this.addBaseTag();
        } else {
            this.setAppleTitleMeta();
        }
    }

    componentDidCatch(err, info) {
        this.setState({
            hasError: true,
            errorDetails: { err, info },
        });
    }

    handleMetaChange(name, content) {
        const metaTag = document.head.querySelector(`meta[name="${name}"]`);
        metaTag.content = content;
    }

    setAppleTitleMetaPrm() {
        this.handleMetaChange('apple-mobile-web-app-title', 'SneakerStudioPRM.com');
    }

    getSortedItems(type) {
        const items = type === SWITCH_ITEMS_TYPE ? this._getRoutes() : this[type];

        return items
            .sort((a, b) => a.position - b.position)
            .filter((entry) => {
                if (!entry.component) {
                    // eslint-disable-next-line no-console
                    console.warn('There is an item without a component property declared in main router.');
                    return false;
                }

                return true;
            });
    }

    setAppleTitleMeta() {
        this.handleMetaChange('apple-mobile-web-app-title', 'SneakerStudio.com');
    }

    addBaseTag = () => {
        const { storeViewCode } = this.props;
        const baseTag = document.createElement('base');
        const href = document.createAttribute('href');

        href.value =
            storeViewCode !== ''
                ? `${window.location.protocol}//${window.location.host}/${storeViewCode}/`
                : `${window.location.protocol}//${window.location.host}`;
        baseTag.setAttributeNode(href);
        document.head.appendChild(baseTag);
    };

    handleErrorReset = () => {
        this.setState({ hasError: false });
    };

    renderItemsOfType(type) {
        return this.getSortedItems(type).map(({ position, component }) => cloneElement(component, { key: position }));
    }

    renderMainItems() {
        const { isBigOffline } = this.props;

        if (!navigator.onLine && isBigOffline) {
            return <OfflineNotice isPage />;
        }

        return (
            <Suspense fallback={this.renderFallbackPage()}>
                <NoMatchHandler>
                    <Switch>{this.renderItemsOfType(SWITCH_ITEMS_TYPE)}</Switch>
                </NoMatchHandler>
            </Suspense>
        );
    }

    renderErrorRouterContent() {
        const { errorDetails } = this.state;

        return <SomethingWentWrong onClick={this.handleErrorReset} errorDetails={errorDetails} />;
    }

    renderFallbackPage() {
        return (
            <main style={{ height: '100vh' }}>
                <Loader isLoading />
            </main>
        );
    }

    renderDefaultRouterContent() {
        return (
            <>
                {this.renderItemsOfType(BEFORE_ITEMS_TYPE)}
                {this.renderMainItems()}
                {this.renderItemsOfType(AFTER_ITEMS_TYPE)}
            </>
        );
    }

    renderRouterContent() {
        const { hasError } = this.state;

        if (hasError) {
            return this.renderErrorRouterContent();
        }

        return this.renderDefaultRouterContent();
    }

    render() {
        return (
            <>
                <Meta />
                <ReactRouter history={history}>
                    <ThemeProvider key="themeProvider">
                        <ThemeSetter>{this.renderRouterContent()}</ThemeSetter>
                    </ThemeProvider>
                    <HreflinksProvider {...this.props} />
                </ReactRouter>
                <ShowDimmer />
            </>
        );
    }
}

export default Router;

import { connect } from 'react-redux';

import { ProductsWithAttributeDispatcher } from 'Store/ProductsWithAttribute';
import ProductCollections from './ProductCollections.component';

export const COLLECTION_ATTRIBUTE_NAME = 'product_collection';

export const mapStateToProps = (state) => ({
    productCollections: state.ProductsWithAttributeReducer.product_collection,
});

export const mapDispatchToProps = (dispatch) => ({
    getProductCollections: (collectionId) =>
        ProductsWithAttributeDispatcher.handleData(dispatch, {
            args: { filter: { attribute: { name: COLLECTION_ATTRIBUTE_NAME, value: collectionId } } },
        }),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductCollections);

import PropTypes from 'prop-types';

import SourceProductGalleryContainer from 'SourceComponent/ProductGallery/ProductGallery.container';

import { ProductType } from 'Type/ProductList';
import { AMOUNT_OF_PLACEHOLDERS, IMAGE_TYPE, THUMBNAIL_KEY } from './ProductGallery.config';

export * from 'SourceComponent/ProductGallery/ProductGallery.container';

class ProductGalleryContainer extends SourceProductGalleryContainer {
    static propTypes = {
        product: ProductType.isRequired,
        areDetailsLoaded: PropTypes.bool,
        galleryType: PropTypes.string,
        showGallery: PropTypes.func,
    };

    containerProps = () => {
        const { activeImage, isZoomEnabled } = this.state;
        const {
            product: { id },
            galleryType,
            showGallery,
        } = this.props;

        return {
            gallery: this.getGalleryPictures(),
            productName: this._getProductName(),
            activeImage,
            isZoomEnabled,
            productId: id,
            galleryType,
            showGallery,
        };
    };

    getGalleryPictures() {
        const {
            areDetailsLoaded,
            product: { media_gallery_entries: mediaGallery = [], [THUMBNAIL_KEY]: { url } = {}, name },
        } = this.props;

        if (mediaGallery.length) {
            return Object.values(
                mediaGallery.reduce((acc, srcMedia) => {
                    const { types, position, disabled } = srcMedia;

                    const canBeShown = !disabled;
                    if (!canBeShown) {
                        return acc;
                    }

                    const isThumbnail = types.includes(THUMBNAIL_KEY);
                    const key = isThumbnail ? 0 : position + 1;

                    return {
                        ...acc,
                        [key]: srcMedia,
                    };
                }, {}),
            );
        }

        if (!url) {
            return Array(AMOUNT_OF_PLACEHOLDERS + 1).fill({ media_type: 'placeholder' });
        }

        const placeholders = !areDetailsLoaded ? Array(AMOUNT_OF_PLACEHOLDERS).fill({ media_type: 'placeholder' }) : [];

        return [
            {
                thumbnail: { url },
                base: { url },
                small: { url },
                id: THUMBNAIL_KEY,
                label: name,
                media_type: IMAGE_TYPE,
            },
            ...placeholders,
        ];
    }
}

export default ProductGalleryContainer;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import Field from 'Component/Field';

import './FilterSelectorOption.style.scss';

class FilterSelectorOption extends PureComponent {
    static propTypes = {
        label: PropTypes.string.isRequired,
        isSelected: PropTypes.bool.isRequired,
        value: PropTypes.string.isRequired,
        selectOption: PropTypes.func.isRequired,
        isCheckbox: PropTypes.bool,
    };

    static defaultProps = {
        isCheckbox: true,
    };

    render() {
        const { isSelected, label, value, selectOption, isCheckbox } = this.props;

        if (!isCheckbox) {
            return (
                <div
                    mix={{
                        block: 'FilterSelector',
                        elem: 'Option',
                        mods: { isSelected, isRadio: true },
                    }}
                    onClick={selectOption}
                    role="button"
                >
                    {label}
                </div>
            );
        }

        return (
            <Field
                type="checkbox"
                label={label}
                value={value}
                id={value}
                checked={isSelected}
                onChange={selectOption}
                name={value}
                mix={{
                    block: 'FilterSelector',
                    elem: 'Option',
                    mods: { isSelected },
                }}
            />
        );
    }
}

export default FilterSelectorOption;

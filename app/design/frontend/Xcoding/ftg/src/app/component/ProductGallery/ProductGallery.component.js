import { withRouter } from 'react-router';

import { ProductGallery as SourceProductGallery } from 'SourceComponent/ProductGallery/ProductGallery.component';
import { IMAGE_TYPE, PLACEHOLDER_TYPE, VIDEO_TYPE } from 'SourceComponent/ProductGallery/ProductGallery.config';

import CSS from 'Util/CSS';

export * from 'SourceComponent/ProductGallery/ProductGallery.component';

export const INTERNAL_VIDEO_TYPE = 'internal-video';

export class ProductGallery extends SourceProductGallery {
    componentDidUpdate(prevProps) {
        const {
            productId,
            location: { pathname },
        } = this.props;
        const {
            productId: prevProductId,
            location: { pathname: prevPathname },
        } = prevProps;

        if (productId !== prevProductId) {
            this.updateSharedDestinationElement();
        }

        if (this.sliderRef?.current?.draggableRef && pathname !== prevPathname) {
            CSS.setVariable(this.sliderRef.current.draggableRef, 'animation-speed', 0);
        }
    }

    renderSlide(media, index) {
        const { media_type } = media;

        switch (media_type) {
            case IMAGE_TYPE:
                return this.renderImage(media, index);
            case VIDEO_TYPE:
                return this.renderVideo(media, index);
            case INTERNAL_VIDEO_TYPE:
                return this.renderVideo(media, index);
            case PLACEHOLDER_TYPE:
                return this.renderPlaceholder(index);
            default:
                return null;
        }
    }
}
export default withRouter(ProductGallery);

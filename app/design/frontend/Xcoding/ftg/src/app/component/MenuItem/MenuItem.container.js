import { PureComponent } from 'react';
import PropTypes from 'prop-types';

import MenuItem from './MenuItem.component';

export class MenuItemContainer extends PureComponent {
    static isHover = false;

    static propTypes = {
        onCategoryHover: PropTypes.func,
        item: PropTypes.object.isRequired,
    };

    static defaultProps = {
        onCategoryHover: () => {},
    };

    containerFunctions = {
        handleCategoryHover: this.handleCategoryHover.bind(this),
        handleCategoryLeave: this.handleCategoryLeave.bind(this),
    };

    handleCategoryLeave() {
        this.isHover = false;
    }

    handleCategoryHover() {
        const { onCategoryHover, item } = this.props;
        this.isHover = true;

        setTimeout(() => {
            this.isHover && onCategoryHover(item);
        }, 200);
    }

    render() {
        return <MenuItem {...this.props} {...this.containerFunctions} />;
    }
}

export default MenuItemContainer;

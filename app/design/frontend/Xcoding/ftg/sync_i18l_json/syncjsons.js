const sync = require('i18next-json-sync').default;

sync({
    files: './*.json',
    primary: 'pl_PL'
});

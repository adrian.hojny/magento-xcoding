// const folders = ['component', 'context', 'query', 'route', 'store', 'style', 'type', 'util'];
const folders = [
    'Style',
    'Component',
    'Context',
    'Route',
    'Store',
    'Util',
    'Query',
    'Type',
    // 'SourceStyle',
    // 'SourceComponent',
    // 'SourceRoute',
    // 'SourceStore',
    // 'SourceUtil',
    // 'SourceQuery',
    // 'SourceType',
];

module.exports = {
    extends: ['airbnb', 'plugin:array-func/recommended', 'plugin:prettier/recommended'],
    plugins: ['react', 'simple-import-sort', 'import', 'jest', 'babel', 'fp', 'prettier'],
    parser: 'babel-eslint',
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    env: {
        browser: true,
    },
    globals: {
        window: true,
        document: true,
        sessionStorage: true,
        localStorage: true,
        jest: true,
        __: true,
        workbox: true,
        importScripts: true,
    },
    rules: {
        'fp/no-let': 'off',
        'fp/no-arguments': 'error',
        'fp/no-loops': 'error',
        'fp/no-delete': 'error',
        'no-var': 'error',
        'css-rcurlyexpected': 0,
        'no-restricted-globals': ['error', 'isFinite', 'isNaN'],
        'max-len': [
            2,
            {
                ignoreComments: true,
                ignoreUrls: true,
                code: 120,
            },
        ],
        'jest/no-disabled-tests': 'warn',
        'jest/no-focused-tests': 'error',
        'jest/no-identical-title': 'error',
        'jest/prefer-to-have-length': 'warn',
        'jest/valid-expect': 'error',
        'no-shadow': 0,
        'class-methods-use-this': 0,
        camelcase: 0,
        'no-underscore-dangle': 'off',
        'template-curly-spacing': 'off',
        'computed-property-spacing': 'off',
        'import/no-unresolved': 0,
        'import/named': 0,
        'import/prefer-default-export': 0,
        'no-plusplus': 0,
        'react/react-in-jsx-scope': 0,
        'import/no-named-as-default': 0,
        'import/no-named-as-default-member': 0,
        'no-extra-semi': 'error',
        'simple-import-sort/imports': 'error',
        'simple-import-sort/exports': 'error',
        'sort-imports': 'off',
        'react/jsx-filename-extension': 0,
        'react/prefer-stateless-function': 0,
        'react/static-property-placement': 0,
        'react/button-has-type': 0,
        'react/jsx-no-bind': [
            2,
            {
                ignoreDOMComponents: true,
                ignoreRefs: true,
                allowArrowFunctions: false,
                allowFunctions: false,
                allowBind: false,
            },
        ],
        'react/forbid-prop-types': [
            2,
            {
                forbid: ['className'],
            },
        ],
        'react/forbid-component-props': [
            2,
            {
                forbid: ['className'],
            },
        ],
        'react/forbid-dom-props': [
            2,
            {
                forbid: ['className'],
            },
        ],
        'react/jsx-props-no-spreading': 'off',
        'react/no-deprecated': 2,
        'babel/semi': 1,
        'new-cap': [
            'error',
            {
                newIsCap: true,
            },
        ],
        'no-param-reassign': [
            'error',
            {
                props: true,
                ignorePropertyModificationsFor: ['acc', 'sum'],
            },
        ],
        'no-magic-numbers': [
            'error',
            {
                ignore: [1, 0, 2, -1],
            },
        ],
        'no-case-declarations': 'off',
        'jsx-a11y/label-has-for': 0,
        'padding-line-between-statements': [
            'error',
            {
                blankLine: 'always',
                prev: ['const', 'let', 'var'],
                next: '*',
            },
            {
                blankLine: 'any',
                prev: ['const', 'let', 'var'],
                next: ['const', 'let', 'var'],
            },
            {
                blankLine: 'always',
                prev: [
                    'block',
                    'block-like',
                    'multiline-block-like',
                    'multiline-expression',
                    'multiline-const',
                    'multiline-let',
                    'multiline-var',
                ],
                next: 'return',
            },
            {
                blankLine: 'any',
                prev: ['singleline-const', 'singleline-let', 'singleline-var'],
                next: '*',
            },
        ],
        'prefer-destructuring': [
            'error',
            {
                array: false,
                object: true,
            },
            {
                enforceForRenamedProperties: false,
            },
        ],
        'lines-between-class-members': ['error', 'always'],
    },
    overrides: [
        {
            files: ['*.js', '*.jsx'],
            rules: {
                'simple-import-sort/imports': [
                    'error',
                    {
                        groups: [
                            // Packages. `react` related packages come first.
                            // Things that start with a letter (or digit or underscore), or `@` followed by a letter.
                            ['^react', '^@?\\w'],
                            // Source components
                            ['^Source'],
                            // Absolute imports and relative imports.
                            [`^(${folders.join('|')})(/.*|$)`, '^\\.'],
                            // for scss imports.
                            ['^[^.]'],
                        ],
                    },
                ],
            },
        },
    ],
};
